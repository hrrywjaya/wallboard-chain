package com.mitrakreasindo.pemrek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.mitrakreasindo.pemrek.model.MasterDataTerputus;
import com.mitrakreasindo.pemrek.service.MasterDataTerputusService;

@RestController
@RequestMapping("/capi/masterdata/terputus")
public class MaterDataTerputusController {

	@Autowired
	private MasterDataTerputusService masterDataTerputusService;

	@GetMapping
	public List<MasterDataTerputus> getMaterDataTerputuses()
	{
		return masterDataTerputusService.findAll();
	}
	
}
