package com.mitrakreasindo.pemrek.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

@RestController
@RequestMapping("/capi/masterdata/status-pekerjaan")
public class MasterDataStatusPekerjaanController {

	@Autowired
	private MasterDataStatusPekerjaanRepository mdsttsPekerjaanRepo;
	
	@GetMapping("/all")
    public List<MasterDataStatusPekerjaan> getAllMasterDataStatusPekerjaan() {
        return mdsttsPekerjaanRepo.findAll();
    }
	
}
