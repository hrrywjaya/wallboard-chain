package com.mitrakreasindo.pemrek.controller;

import java.util.List;

import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.Transaksi;
// import com.mitrakreasindo.pemrek.model.TransaksiLogPerubahan;
import com.mitrakreasindo.pemrek.service.TransaksiService;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.model.CallLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/transaksi")
public class TransaksiController {

	@Autowired
	private TransaksiService transaksiService;
  @Autowired
  private CallLogService callLogService;
  

	// @GetMapping("/getTransaksiByRefnum")
	// public List<CallLog> getTransaksiByRefnum(@RequestParam String noRef)
	// {
	// 	return transaksiService.getTransaksi(noRef);
  // }
  
  @GetMapping("/getEdd")
  public List<CallLog> findAll(){
    return callLogService.findAllEdd();
  }

	
}