package com.mitrakreasindo.pemrek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.mitrakreasindo.pemrek.model.MasterDataServiceLevel;
import com.mitrakreasindo.pemrek.service.MasterDataServiceLevelService;

@RestController
@RequestMapping("/capi/masterdata/service-level")
public class MaterDataServiceLevelController {

	@Autowired
	private MasterDataServiceLevelService MaterDataServiceLevelService;

	@GetMapping
	public List<MasterDataServiceLevel> getMaterDataServiceLevels()
	{
		return MaterDataServiceLevelService.findAll();
	}
	
}
