package com.mitrakreasindo.pemrek.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.model.RingkasanTransaksiData;
import com.mitrakreasindo.pemrek.model.TransactionMessage.TransactionMessageStatus;
import com.mitrakreasindo.pemrek.service.RingkasanTransaksiService;

@RestController
@RequestMapping("/capi/ringkasan-transaksi")
public class RingkasanTransaksiController
{
	
	@Autowired
	private RingkasanTransaksiService ringkasanTransaksiService;
		
	@PostMapping("/{no-ref}/call-log/{callLogId}")
	public List<TransactionMessageStatus> process(@PathVariable("no-ref") String noRef, @PathVariable("callLogId") String callLogId, Principal principal) {
		RingkasanTransaksiData ringkasanTransaksiData = ringkasanTransaksiService.process(noRef, callLogId, principal.getName());
		ringkasanTransaksiService.processAsync(ringkasanTransaksiData, callLogId);
		return ringkasanTransaksiService.buildStatusProgress(
				noRef,
				ringkasanTransaksiData.getIsNasabahExisting(), 
				ringkasanTransaksiData.getIsNrt(), 
				ringkasanTransaksiData.getIsChangeProduct());
	}
	
}
