package com.mitrakreasindo.pemrek.controller;

import java.util.List;

import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.ReportLog;
import com.mitrakreasindo.pemrek.model.ReportLogPerubahan;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.service.ReportService;
import com.mitrakreasindo.pemrek.service.TransaksiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/report")
public class ReportController {
  
  @Autowired
	private ReportService reportService;
  @Autowired
  private CallLogService callLogService;

  @GetMapping("/edd-report")
  public List<CallLog> findApprovalReport(@RequestParam String startDate,@RequestParam String endDate,@RequestParam String channel,@RequestParam String rekening,@RequestParam String cabang, @RequestParam String agent, @RequestParam String status) {
      return callLogService.findApprovalReport(startDate, endDate, channel, rekening, cabang, agent, status);
  } 
  
	@GetMapping("/getReportLogByRefnum")
	public ReportLogPerubahan getReportLogRefnum(@RequestParam String refnum)
	{
		return reportService.getReport(refnum);
  }

  @GetMapping("/getReportLogByParam")
	public ReportLogPerubahan getReportLogByParam(@RequestParam String param, @RequestParam String startDate, @RequestParam String endDate)
	{
		return reportService.getReportByParam(param,startDate,endDate);
  }
  
  
  @GetMapping("/getReportLogByDate")
	public ReportLogPerubahan getReportLogRefnumByDate(@RequestParam String startDate, @RequestParam String endDate)
	{
		return reportService.getReportByDate(startDate,endDate);
  }

  // @GetMapping("/getEddReport)")
  // public List<CallLog> findApprovalReport(@RequestParam String startDate,@RequestParam String endDate,@RequestParam String channel,@RequestParam String rekening,@RequestParam String cabang,@RequestParam String agent,@RequestParam String status) {
  //     return callLogService.findApprovalReport(startDate, endDate, channel, rekening, cabang, agent, status);
  // }
  
 
	
}