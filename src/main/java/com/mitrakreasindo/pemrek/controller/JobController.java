package com.mitrakreasindo.pemrek.controller;

import com.mitrakreasindo.pemrek.scheduler.RequestBranchScheduler;
import com.mitrakreasindo.pemrek.scheduler.RequestCountryScheduler;
import com.mitrakreasindo.pemrek.scheduler.RequestPekerjaanScheduler;
import com.mitrakreasindo.pemrek.scheduler.SendEddEmailScheduler;
import com.mitrakreasindo.pemrek.scheduler.SendPendingTransactionStatusKafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/jobs")
public class JobController {
    
    @Autowired
    private RequestBranchScheduler branchScheduler;
    @Autowired
    private RequestPekerjaanScheduler pekerjaanScheduler;
    @Autowired
    private RequestCountryScheduler countryScheduler;
    @Autowired
    private SendEddEmailScheduler sendEddEmailScheduler; //testing
    @Autowired
    private SendPendingTransactionStatusKafka sendPendingTransactionStatusKafka; //testing

    @PostMapping("/restart")
    public void restartJob() {
//        branchScheduler.dailyJob();
        pekerjaanScheduler.dailyJob();
        countryScheduler.dailyJob();
        sendEddEmailScheduler.weeklyJob(); //testing
        sendPendingTransactionStatusKafka.dailyJobBatch1();//testing
        sendPendingTransactionStatusKafka.dailyJobBatch2();//testing
    }


}