package com.mitrakreasindo.pemrek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.mitrakreasindo.pemrek.model.MasterDataTolak;
import com.mitrakreasindo.pemrek.service.MasterDataTolakService;

@RestController
@RequestMapping("/capi/masterdata/tolak")
public class MaterDataTolakController {

	@Autowired
	private MasterDataTolakService masterDataTolakService;

	// @PostMapping
	// public MasterDataPemrek createMasterDataPemrek(@RequestBody MasterDataPemrek
	// dataPemrek)
	// {
	// return dataPredinService.save(dataPemrek);
	// }

	@GetMapping
	public List<MasterDataTolak> getMaterDataTolaks()
	{
		return masterDataTolakService.findAll();
	}
	
}
