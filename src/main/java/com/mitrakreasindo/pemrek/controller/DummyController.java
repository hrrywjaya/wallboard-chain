package com.mitrakreasindo.pemrek.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
// import com.mitrakreasindo.pemrek.service.EddApprovalService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.exception.ResourceNotFoundException;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.model.EmailRequest;
import com.mitrakreasindo.pemrek.service.EddApprovalService;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/capi/local")
public class DummyController {

	@Autowired
	private ModelMapper modelMapper;

  @Autowired
  private EddApprovalService eddApprovalService;


  @PostMapping("/send-email")
  public ErrorSchemaUpperCamelCase sendEmail(@RequestBody EmailRequest email){
    return eddApprovalService.sendEmail(email);
  }

	@GetMapping("eform/{refNumber}")
	public Object getEform(@PathVariable("refNumber") String refNumber) {
		try {
			// if (!refNumber.matches("AAAAAAAAAA-A1")) {
			// throw new ResourceNotFoundException(refNumber);
			// }
			// Resource resource = new ClassPathResource("dummy/eform.json");
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\eform.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\eform.json");
			// Resource resource = new ClassPathResource("dummy/eform.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("cis/search/rekening/{rek}")
	public Object getCisByRekening(@PathVariable("rek") String refNumber) {
		try {
			// Resource resource = new ClassPathResource("dummy/eform.json");
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\cis-by-rekening-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\cis-by-rekening-response.json");
			// Resource resource = new
			// ClassPathResource("dummy/cis-by-rekening-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("cis/search")
	public Object getCisByParam() {
		// if (isFound()) {
		// // do what you want
		// return new ResponseEntity<>(HttpStatus.OK);
		// }
		// else {
		// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		// return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		// }
		try {
			// Resource resource = new ClassPathResource("dummy/eform.json");
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\cis-baru.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\cis-baru.json");
			// Resource resource = new ClassPathResource("dummy/cis-baru.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("ektp")
	public Object getEktp(@RequestParam("nik") String nik) {
		try {
			// if (!nik.matches("1311032908900002")) {
			// throw new ResourceNotFoundException(nik);
			// }
			// Resource resource = new ClassPathResource("dummy/eform.json");
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\ektp.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\ektp.json");
			// Resource resource = new ClassPathResource("dummy/ektp.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("predin")
	public Object getPredin(@RequestParam("reference-number") String noRef) {
		try {
			// if (!noRef.matches("AAAAAAAAAA-A1")) {
			// throw new ResourceNotFoundException(noRef);
			// }
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\predin.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\predin.json");
			// Resource resource = new ClassPathResource("dummy/predin.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PutMapping("predin")
	public Object updatePredin() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\predin-update.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\predin-update.json");
			// Resource resource = new ClassPathResource("dummy/predin-update.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("customer/status")
	public Object getCustomerStatus() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\customer-status.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\customer-status.json");
			// Resource resource = new ClassPathResource("dummy/customer-status.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("din/{cisCustNumber}")
	public Object getDin(@PathVariable("cisCustNumber") String cisCustNumber) {

		// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		// return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		try {

			// // if (!cisCustNumber.matches("00000006335")) {
			// // throw new ResourceNotFoundException(cisCustNumber);
			// // }
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new ClassPathResource(".\\dummy\\din.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\din.json");
			// Resource resource = new ClassPathResource("dummy/din.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("halo-info/breaking-news")
	public Object getBreakingNews(@RequestParam("offset") String offset, @RequestParam("show") String show) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\halo-info-breaking-news.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\halo-info-breaking-news.json");
			// Resource resource = new
			// ClassPathResource("dummy/halo-info-breaking-news.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("halo-info/other-marketing-programs")
	public Object getOtherMarketingPrograms(@RequestParam("offset") String offset, @RequestParam("show") String show) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\halo-info-other-marketing.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\halo-info-other-marketing.json");
			// Resource resource = new
			// ClassPathResource("dummy/halo-info-other-marketing.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("eform/generate/reference-number")
	public Object getCreateNoRefBaru() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\generate-reference-number-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\generate-reference-number-response.json");
			// Resource resource = new
			// ClassPathResource("dummy/generate-reference-number-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("eform")
	public Object saveEform() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\eform-save-response-single-object.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\eform-save-response-single-object.json");
			// Resource resource = new
			// ClassPathResource("dummy/eform-save-response-single-object.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("cis/{cisCustNumber}")
	public Object updateCisIndividu() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\cis-update-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\cis-update-response.json");
			// Resource resource = new ClassPathResource("dummy/cis-update-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("registration/account/rekening")
	public Object openSecondAccount() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\create-rekening-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\create-rekening-response.json");
			// Resource resource = new
			// ClassPathResource("dummy/create-rekening-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("edd/{cisCustNumber}")
	public Object saveEdd() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\save-edd-response-00000005897.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\save-edd-response-00000005897.json");
			// Resource resource = new
			// ClassPathResource("dummy/save-edd-response-00000005897.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("eform/{noRef}/status")
	public Object updateStatusEform() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\eform-update-status-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\eform-update-status-response.json");
			// Resource resource = new
			// ClassPathResource("dummy/eform-update-status-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("property-value/users/{username}/property/{property}")
	public Object propertyValue(@PathVariable("username") String username, @PathVariable("property") String property) {
		// if (property.equalsIgnoreCase("sAMAccountName"))
		// 	return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// File file = new
			// ClassPathResource(".\\dummy\\eform-update-status-response.json").getFile();
			// Object object = objectMapper.readValue(file, Object.class);
			Resource resource = new ClassPathResource(".\\dummy\\property-value.json");
			// Resource resource = new
			// ClassPathResource("dummy/eform-update-status-response.json");
			Object object = objectMapper.readValue(resource.getInputStream(), Object.class);
			return object;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/ip")
	public String ip(HttpServletRequest request) {
		return "ip forwarded " + request.getHeader("X-FORWARDED-FOR")+
				"\nip remote address " + request.getRemoteAddr();
	}

}
