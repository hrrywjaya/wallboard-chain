// package com.mitrakreasindo.pemrek.controller;

// import org.modelmapper.ModelMapper;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.Pageable;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// import com.mitrakreasindo.pemrek.dto.TransactionLogDto;
// import com.mitrakreasindo.pemrek.dto.TransactionLogLastAgentDto;
// import com.mitrakreasindo.pemrek.dto.TransaksiBaruDto;
// import com.mitrakreasindo.pemrek.model.Referensi;
// import com.mitrakreasindo.pemrek.model.Transaksi;
// import com.mitrakreasindo.pemrek.model.TransaksiBaru;
// import com.mitrakreasindo.pemrek.repository.ReferensiRepository;
// import com.mitrakreasindo.pemrek.service.ReferensiService;
// import com.mitrakreasindo.pemrek.service.TransaksiBaruService;
// import com.mitrakreasindo.pemrek.service.TransaksiService;

// @RestController
// @RequestMapping("/capi/transaksi-baru")
// public class TransaksiBaruController {

// 	@Autowired
// 	private TransaksiBaruService transaksiBaruService;

// 	@Autowired
// 	private ModelMapper modelMapper;

// 	// @Autowired
// 	// private MasterDataLogRepository t;

// 	// @GetMapping
// 	// public Page<Referensi> getPage (Pageable pageable)
// 	// {
// 	// return referensiService.findAll(pageable);
// 	// }

// 	@GetMapping("/{id}")
// 	public TransaksiBaruDto get(@PathVariable("id") String id) {
// 		TransaksiBaru tb = transaksiBaruService.findById(id);
// 		TransaksiBaruDto tbDto = modelMapper.map(tb, TransaksiBaruDto.class);
// 		return tbDto;
// 	}

// 	@PostMapping
// 	public TransaksiBaruDto create(@RequestBody TransaksiBaruDto tbDto) {
// 		return transaksiBaruService.saveTransaksi(tbDto);
// 	}

// 	@GetMapping("/ref/{noRef}")
// 	public TransaksiBaruDto getByNoRef(@PathVariable("noRef") String noRef) {
// 		return transaksiBaruService.findOneByNoRef(noRef);
// 	}

// 	@GetMapping("/delete/{id}")
// 	public void delete(@PathVariable("id") String id) {
// 		// TransaksiBaru tb = transaksiBaruService.findById(id);
		
// 		transaksiBaruService.delete(id);
// 	}

// 	// @GetMapping("/ref/{refNumber}")
// 	// public TransaksiLastAgentDto
// 	// getLastTransaksiByRefNumber(@PathVariable("refNumber")String refNumber) {
// 	// return transaksiService.getLasTransaksiByNoRef(refNumber);
// 	// }

// 	// @PostMapping("/approved")
// 	// public TransaksiBaru setApproval (@RequestBody TransaksiBaru transaksiBaru)
// 	// {
// 	// return transaksibaruService.setApproved(transaksiBaru);
// 	// }

// }
