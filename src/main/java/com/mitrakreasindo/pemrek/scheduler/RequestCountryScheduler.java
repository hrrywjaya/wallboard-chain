package com.mitrakreasindo.pemrek.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.external.branch.service.BranchService;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.country.model.Country;
import com.mitrakreasindo.pemrek.external.country.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.country.service.CountryService;
import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataBranchRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataCountryRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataPekerjaanRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

import lombok.Data;
import lombok.Setter;
import lombok.Getter;

import com.mitrakreasindo.pemrek.model.MasterDataCountry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Data
@Getter
@Setter
public class RequestCountryScheduler {

    @Autowired
    private CountryService countryService;
    @Autowired
    private MasterDataCountryRepository masterDataCountryRepository;
    @Autowired
    private MasterDataStatusPekerjaanRepository masterDataStatusPekerjaanRepository;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private SchedulerConfig schedulerConfig;

    private LocalDateTime start;
    private LocalDateTime finish;
    private String runningStatus;
    private String jobName="Request Country";
    
    @Scheduled(cron = "0 0 0 * * ?")
    public void dailyJob() {
        
        if (!schedulerConfig.isDailyJobEnabled())
            return;

        System.out.println("=============================================================================");
        System.out.println("================== start running scheduled country job ====================");
        start = LocalDateTime.now();
        System.out.println("=============================================================================");
        requestCountry();
        System.out.println("=============================================================================");
        System.out.println("================= finish running scheduled country job ====================");
        
        System.out.println("=============================================================================");
        
    }
    
    private void createLog() {
    	MasterDataStatusPekerjaan mdsp = new MasterDataStatusPekerjaan();
    	mdsp.setJobName(getJobName());
    	mdsp.setLastRunning(getStart());
    	mdsp.setRunningPeriod(getStart(),getFinish());
    	mdsp.setStatus(getRunningStatus());
    	masterDataStatusPekerjaanRepository.save(mdsp);
    }

    private void requestCountry() {
        OutputSnakeCase<OutputSchema> output = countryService.getCountry();
               
        try {
        	List<MasterDataCountry> mdcs =  output.getOutputSchema().getCountries().getCountry().stream().map(country -> {
                MasterDataCountry mdc = new MasterDataCountry();
                mdc.setCountryCodeIso(country.getCountryCode().getIso());
                mdc.setCountryCodeLbu(country.getCountryCode().getLbu());
                mdc.setCountryNameShort(country.getCountryName().getShortName());
                mdc.setCountryNameLong(country.getCountryName().getLongName());
                mdc.setHightRiskIndicator(country.getHighRiskIndicator());
                return mdc;
            })
            .collect(Collectors.toList());

			if (mdcs != null) {
				if (masterDataCountryRepository.count() > 0 )
					masterDataCountryRepository.deleteAll();
				
				masterDataCountryRepository.saveAll(mdcs);
			}
			this.setRunningStatus(output.getErrorSchema().getErrorMessage().getIndonesian());
			finish = LocalDateTime.now();
			createLog();
		} catch (Exception e) {
			e.printStackTrace();
			this.setRunningStatus(output.getErrorSchema().getErrorMessage().getIndonesian());
			finish = LocalDateTime.now();
			createLog();
		}
        
    }

}