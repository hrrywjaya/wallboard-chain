package com.mitrakreasindo.pemrek.scheduler;

import com.mitrakreasindo.pemrek.external.kafka.serviceimpl.KafkaServiceImpl;
import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class SendPendingTransactionStatusKafka {

    @Autowired
    KafkaServiceImpl kafkaServiceImpl;

    @Autowired
    private SchedulerConfig schedulerConfig;
    
    @Autowired
    private MasterDataStatusPekerjaanRepository masterDataStatusPekerjaanRepository;
    
    private LocalDateTime start;
    private LocalDateTime finish;
    private String runningStatus;
    private String jobName;

    @Scheduled(cron = "0 0 12 * * ?")
//    private void dailyJobBatch1(){
    public void dailyJobBatch1(){ //testing
    
    if (!schedulerConfig.isDailyJobEnabled())
    return;

    this.setJobName("Kafka Daily Job Batch 1");
    System.out.println("===============================================================================================================");
    System.out.println("======================== start running scheduled SEND TO KAFKA PENDING TRANSACTION job batch 1========================");
    start = LocalDateTime.now();
    System.out.println("===============================================================================================================");
    kafkaServiceImpl.listTrxBatch1();
    runningStatus = kafkaServiceImpl.getRunningStatus() == null ? "Berhasil" : kafkaServiceImpl.getRunningStatus();
    System.out.println("===============================================================================================================");
    System.out.println("======================= finish running scheduled SEND TO KAFKA PENDING TRANSACTION job batch 1========================");
    finish = LocalDateTime.now();
    System.out.println("===============================================================================================================");
    createLog();
    }

    @Scheduled(cron = "0 0 23 * * ?")
//    private void dailyJobBatch2(){
    public void dailyJobBatch2(){ //testing
    
    if (!schedulerConfig.isDailyJobEnabled())
    return;

    this.setJobName("Kafka Daily Job Batch 2");
    System.out.println("===============================================================================================================");
    System.out.println("======================== start running scheduled SEND TO KAFKA PENDING TRANSACTION job batch 2========================");
    start = LocalDateTime.now();
    System.out.println("===============================================================================================================");
    kafkaServiceImpl.listTrxBatch2();
    runningStatus = kafkaServiceImpl.getRunningStatus() == null ? "Berhasil" : kafkaServiceImpl.getRunningStatus();
    System.out.println("===============================================================================================================");
    System.out.println("======================= finish running scheduled SEND TO KAFKA PENDING TRANSACTION job batch 2========================");
    finish = LocalDateTime.now();
    System.out.println("===============================================================================================================");
    createLog();
    }
    
    private void createLog() {
    	MasterDataStatusPekerjaan mdsp = new MasterDataStatusPekerjaan();
    	mdsp.setJobName(getJobName());
    	mdsp.setLastRunning(getStart());
    	mdsp.setRunningPeriod(getStart(), getFinish());
    	mdsp.setStatus(getRunningStatus());
    	masterDataStatusPekerjaanRepository.save(mdsp);
    }

}