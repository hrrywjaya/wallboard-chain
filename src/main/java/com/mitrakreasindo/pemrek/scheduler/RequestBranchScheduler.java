package com.mitrakreasindo.pemrek.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.external.branch.service.BranchService;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataBranchRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataPekerjaanRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Data
@Getter
@Setter
public class RequestBranchScheduler {

    @Autowired
    private BranchService branchService;
    @Autowired
    private MasterDataBranchRepository masterDataBranchRepository;
    @Autowired
    private MasterDataStatusPekerjaanRepository masterDataStatusPekerjaanRepository;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private SchedulerConfig schedulerConfig;

    private LocalDateTime start;
    private LocalDateTime finish;
    private String runningStatus;
    private String jobName="Request Branch";
    
    @Scheduled(cron = "0 0 0 * * ?")
    public void dailyJob() {
        
        if (!schedulerConfig.isDailyJobEnabled())
            return;
        
        System.out.println("=============================================================================");
        System.out.println("======================== start running scheduled branch job ========================");
        start = LocalDateTime.now();
        System.out.println("=============================================================================");
        requestBranch();
        System.out.println("=============================================================================");
        System.out.println("======================= finish running scheduled branch job ========================");
        
        System.out.println("=============================================================================");
        
    }
    
    private void createLog() {
    	MasterDataStatusPekerjaan mdsp = new MasterDataStatusPekerjaan();
    	mdsp.setJobName(getJobName());
    	mdsp.setLastRunning(getStart());
    	mdsp.setRunningPeriod(getStart(), getFinish());
    	mdsp.setStatus(getRunningStatus());
    	masterDataStatusPekerjaanRepository.save(mdsp);
    }

    private void requestBranch() {
        OutputSnakeCase<Object> branch = branchService.getBranch();
        String json;
        try {
            json = mapper.writeValueAsString(branch.getOutputSchema());
            JsonNode branchJson = mapper.readTree(json);
            JsonNode items =  branchJson.findValue("branch");
            
            if (items.isArray()) {
                List<MasterDataBranch> mbs = new ArrayList<>();
                items.forEach(item -> {
                    MasterDataBranch mb = new MasterDataBranch();
                    mb.setBranchCode(item.findValue("branch_code").asText(""));
                    mb.setBranchName(item.findValue("branch_name").asText(""));
                    mb.setBranchType(item.findValue("branch_type").asText(""));
                    mb.setBranchInitial(item.findValue("branch_initial").asText(""));
                    mb.setBranchStatus(item.findValue("branch_status").asText(""));
                    mb.setMainBranchCode(item.findValue("main_branch_code").asText(""));
                    mb.setMainBranchName(item.findValue("main_branch_name").asText(""));
                    mb.setMainBranchInitial(item.findValue("main_branch_initial").asText(""));
                    mb.setAddress(item.findValue("address").asText(""));
                    mb.setCity(item.findValue("city").asText(""));
                    mbs.add(mb);
                });
                masterDataBranchRepository.deleteAll();
                masterDataBranchRepository.saveAll(mbs);
            }
            this.setRunningStatus(branch.getErrorSchema().getErrorMessage().getIndonesian());
            finish = LocalDateTime.now();
            createLog();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            this.setRunningStatus(branch.getErrorSchema().getErrorMessage().getIndonesian());
            finish = LocalDateTime.now();
            createLog();
        }

    }

}