package com.mitrakreasindo.pemrek.scheduler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SchedulerConfig {

    @Value("${job-daily-enable}")
    private String schedulerStatus;

    /**
     * check running status for daily job
     * @return
     */
    public boolean isDailyJobEnabled() {
        return "true".equalsIgnoreCase(schedulerStatus);
    }

}