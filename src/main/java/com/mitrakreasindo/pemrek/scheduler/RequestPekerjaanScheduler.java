package com.mitrakreasindo.pemrek.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataPekerjaanRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Data
@Getter
@Setter
public class RequestPekerjaanScheduler {

    @Autowired
    private PekerjaanService pekerjaanService;
    @Autowired
    private MasterDataPekerjaanRepository masterDataPekerjaanRepository;
    @Autowired
    private MasterDataStatusPekerjaanRepository masterDataStatusPekerjaanRepository;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private SchedulerConfig schedulerConfig;
    
    private LocalDateTime start;
    private LocalDateTime finish;
    private String runningStatus;
    private String jobName="Request Pekerjaan";

    @Scheduled(cron = "0 0 0 * * ?")
    public void dailyJob() {
        
        if (!schedulerConfig.isDailyJobEnabled())
            return;

        System.out.println("=============================================================================");
        System.out.println("================== start running scheduled pekerjaan job ====================");
        start = LocalDateTime.now();
        System.out.println("=============================================================================");
        requestPekerjaan();
        System.out.println("=============================================================================");
        System.out.println("================= finish running scheduled pekerjaan job ====================");
        
        System.out.println("=============================================================================");
        
    }

    private void createLog() {
    	MasterDataStatusPekerjaan mdsp = new MasterDataStatusPekerjaan();
    	mdsp.setJobName(getJobName());
    	mdsp.setLastRunning(getStart());
    	mdsp.setRunningPeriod(getStart(),getFinish());
    	mdsp.setStatus(getRunningStatus());
    	masterDataStatusPekerjaanRepository.save(mdsp);
    }
    
    private void requestPekerjaan() {
        Object pekerjaanResult = pekerjaanService.getPekerjaanAll();
        try {
            String pekerjaanResultString = mapper.writeValueAsString(pekerjaanResult);
            JsonNode getPekerjaan = mapper.readTree(pekerjaanResultString);
            JsonNode status = getPekerjaan.findValue("indonesian");
            
            // masterDataPekerjaanRepository.findOneByDescription("MASTER_DATA").ifPresentOrElse(data -> {
            //     data.setRawPekerjaan(pekerjaanResultString);
            //     masterDataPekerjaanRepository.save(data);
            // }, () -> {
            //     MasterDataPekerjaan md = new MasterDataPekerjaan();
            //     md.setActiveFlag(Active.ACTIVE);
            //     md.setDescription("MASTER_DATA");
            //     md.setRawPekerjaan(pekerjaanResultString);
            //     masterDataPekerjaanRepository.save(md);
            // });
            Optional<MasterDataPekerjaan> mdp = masterDataPekerjaanRepository.findOneByDescription("MASTER_DATA");
            if (mdp.isPresent()) {
                mdp.ifPresent(data -> {
                    data.setRawPekerjaan(pekerjaanResultString);
                    masterDataPekerjaanRepository.save(data);
                });
            } else {
                MasterDataPekerjaan md = new MasterDataPekerjaan();
                md.setActiveFlag(Active.ACTIVE);
                md.setDescription("MASTER_DATA");
                md.setRawPekerjaan(pekerjaanResultString);
                masterDataPekerjaanRepository.save(md);
            }
            this.setRunningStatus(status.asText());
            finish = LocalDateTime.now();
            createLog();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            this.setRunningStatus("Gagal");
            finish = LocalDateTime.now();
            createLog();
        }
    }

}