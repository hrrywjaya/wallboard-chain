package com.mitrakreasindo.pemrek.scheduler;

import com.mitrakreasindo.pemrek.model.Email;
import com.mitrakreasindo.pemrek.model.EmailParam;
import com.mitrakreasindo.pemrek.model.EmailRequest;
import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataBranchRepository;
import com.mitrakreasindo.pemrek.repository.MasterDataStatusPekerjaanRepository;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.branch.service.BranchService;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatusUpperCamelCase;
import com.mitrakreasindo.pemrek.external.email.service.EmailService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Data
@Getter
@Setter
public class SendEddEmailScheduler {
  @Autowired
  private EmailService emailService;
  @Autowired
  private SchedulerConfig schedulerConfig;
  @Autowired
  private MasterDataStatusPekerjaanRepository masterDataStatusPekerjaanRepository;
  
  private LocalDateTime start;
  private LocalDateTime finish;
  private String runningStatus;
  private String jobName = "Send Edd Email";

  @Scheduled(cron = "0 0 6 * * MON")
//  private void weeklyJob(){
  public void weeklyJob(){ //testing
    
    if (!schedulerConfig.isDailyJobEnabled())
    return;

    System.out.println("=============================================================================");
    System.out.println("======================== start running scheduled email job ========================");
    start = LocalDateTime.now();
    System.out.println("=============================================================================");
    sendMail();
    System.out.println("=============================================================================");
    System.out.println("======================= finish running scheduled email job ========================");
    finish = LocalDateTime.now();
    System.out.println("=============================================================================");
    createLog();
  }
  
  private void createLog() {
	MasterDataStatusPekerjaan mdsp = new MasterDataStatusPekerjaan();
  	mdsp.setJobName(getJobName());
  	mdsp.setLastRunning(getStart());
  	mdsp.setRunningPeriod(getStart(), getFinish());
  	mdsp.setStatus(getRunningStatus());
  	masterDataStatusPekerjaanRepository.save(mdsp);
  }


  private void sendMail(){

    OutputStatusUpperCamelCase result =  emailService.sendEmail();
    this.setRunningStatus(result.getErrorSchema().getErrorMessage().getIndonesian());
    
  }


}