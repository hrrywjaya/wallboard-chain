// package com.mitrakreasindo.pemrek.scheduler;

// import java.util.Optional;

// import com.fasterxml.jackson.core.JsonProcessingException;
// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.mitrakreasindo.pemrek.core.model.Active;
// import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;
// import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
// import com.mitrakreasindo.pemrek.repository.MasterDataPekerjaanRepository;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.scheduling.annotation.Scheduled;
// import org.springframework.stereotype.Component;

// @Component
// public class RequestPekerjaanScheduler {

//     @Autowired
//     private PekerjaanService pekerjaanService;
//     @Autowired
//     private MasterDataPekerjaanRepository masterDataPekerjaanRepository;
//     @Autowired
//     private ObjectMapper mapper;

//     @Scheduled(cron = "0 0 0 * * ?")
//     private void dailyJob() {
//         System.out.println("=============================================================================");
//         System.out.println("======================== start running scheduled job ========================");
//         System.out.println("=============================================================================");
//         requestPekerjaan();
//         System.out.println("=============================================================================");
//         System.out.println("======================= finish running scheduled job ========================");
//         System.out.println("=============================================================================");
//     }

//     private void requestPekerjaan() {
//         Object pekerjaanResult = pekerjaanService.getPekerjaanAll();
//         try {
//             String pekerjaanResultString = mapper.writeValueAsString(pekerjaanResult);
//             // masterDataPekerjaanRepository.findOneByDescription("MASTER_DATA").ifPresentOrElse(data -> {
//             //     data.setRawPekerjaan(pekerjaanResultString);
//             //     masterDataPekerjaanRepository.save(data);
//             // }, () -> {
//             //     MasterDataPekerjaan md = new MasterDataPekerjaan();
//             //     md.setActiveFlag(Active.ACTIVE);
//             //     md.setDescription("MASTER_DATA");
//             //     md.setRawPekerjaan(pekerjaanResultString);
//             //     masterDataPekerjaanRepository.save(md);
//             // });
//             Optional<MasterDataPekerjaan> mdp = masterDataPekerjaanRepository.findOneByDescription("MASTER_DATA");
//             if (mdp.isPresent()) {
//                 mdp.ifPresent(data -> {
//                     data.setRawPekerjaan(pekerjaanResultString);
//                     masterDataPekerjaanRepository.save(data);
//                 });
//             } else {
//                 MasterDataPekerjaan md = new MasterDataPekerjaan();
//                 md.setActiveFlag(Active.ACTIVE);
//                 md.setDescription("MASTER_DATA");
//                 md.setRawPekerjaan(pekerjaanResultString);
//                 masterDataPekerjaanRepository.save(md);
//             }
//         } catch (JsonProcessingException e) {
//             e.printStackTrace();
//         }
//     }

// }