package com.mitrakreasindo.pemrek.serviceimpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import feign.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;
import com.mitrakreasindo.pemrek.core.exception.CommonResourceNotFoundException;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorMessage;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorSchema;
import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.common.model.OutputStatus;
import com.mitrakreasindo.pemrek.expose.transaction.TransactionErrorCode;
import com.mitrakreasindo.pemrek.expose.transaction.converter.TransactionStatusConverter;
import com.mitrakreasindo.pemrek.expose.transaction.dto.RetryOutputSchema;
import com.mitrakreasindo.pemrek.external.bcacoid.service.BcaCoidService;
import com.mitrakreasindo.pemrek.external.cis.converter.YAndTValueConverter;
import com.mitrakreasindo.pemrek.external.cis.model.ContactInformation;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerAddress;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerComplementData;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerRemark;
import com.mitrakreasindo.pemrek.external.cis.model.Handphone;
import com.mitrakreasindo.pemrek.external.cis.model.OutputCisSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaSearchItem;
import com.mitrakreasindo.pemrek.external.cis.model.Phone;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCis;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCustomerDemographicInformation;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCustomerMasterData;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCustomerNameAndPhone;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCustomerRemark;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateOutputSchema;
import com.mitrakreasindo.pemrek.external.cis.service.CisService;
import com.mitrakreasindo.pemrek.external.common.converter.DateConverter;
import com.mitrakreasindo.pemrek.external.common.converter.YesOrNoConverter;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaSnakeCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.AccountCustomerUpdateKeyValue;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request.AccountCustomerUpdateReqeust;
import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;
import com.mitrakreasindo.pemrek.external.customer.service.CustomerService;
import com.mitrakreasindo.pemrek.external.edd.model.request.EddRequest;
import com.mitrakreasindo.pemrek.external.edd.service.EddService;
import com.mitrakreasindo.pemrek.external.eform.coverter.UpdateReferenceNumberAccountTypeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.AccountTypeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.InterestPlanConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.JenisKartuPasporBcaConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.KodePembebananPajakConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.OdPlanConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.PejabatBankConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.PeriodeBiayaAdminConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.PeriodeBunganConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.PeriodeRkConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.ProdukMataUangConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.ServiceChargeCodeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.StatusTahapanGoldConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.TipeKartuPasporBcaConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.TransIdConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.UbahServiceChargeCodeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.UbahUserCodeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.UserCodeConverter;
import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.WitholdingPlanConverter;
import com.mitrakreasindo.pemrek.external.eform.model.EformFormData;
import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveDataRequest;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveRequest;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformGenerateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformSaveResponse;
import com.mitrakreasindo.pemrek.external.eform.service.EformService;
import com.mitrakreasindo.pemrek.external.ektp.service.EktpService;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.mbank.service.MbankService;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.AprovePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.predin.service.PredinService;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.AccountInformation;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateCisAndRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningAccountInformation;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CustomerInformation;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateCisAndRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationResponse;
import com.mitrakreasindo.pemrek.external.registration.service.RegistrationService;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.CompletData;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.model.RingkasanTransaksiData;
import com.mitrakreasindo.pemrek.model.TransactionMessage;
import com.mitrakreasindo.pemrek.model.TransactionMessage.Status;
import com.mitrakreasindo.pemrek.model.TransactionMessage.TransactionMessageStatus;
import com.mitrakreasindo.pemrek.repository.ReferensiRepository;
import com.mitrakreasindo.pemrek.model.TransactionStatus;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.AccountService;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.service.MasterDataBranchService;
import com.mitrakreasindo.pemrek.service.MasterDataTransactionCodeService;
import com.mitrakreasindo.pemrek.service.ReferenceNumberExtractor;
import com.mitrakreasindo.pemrek.service.ReferensiService;
import com.mitrakreasindo.pemrek.service.RingkasanTransaksiService;
import com.mitrakreasindo.pemrek.service.TransaksiService;
import com.mitrakreasindo.pemrek.service.ReferenceNumberExtractor.ChannelType;

import io.socket.client.Socket;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
// @Transactional
public class RingkasanTransaksiServiceImpl implements RingkasanTransaksiService {

	private final String SOCKET_TRANSACTION = "batch-notif";

	@Autowired
	private Socket socket;
	@Autowired
	private MbankService mBankService;
	@Autowired
	private ReferensiService referensiService;
	@Autowired
	private EformService eformService;
	@Autowired
	private PredinService predinService;
	@Autowired
	private CisService cisService;
	@Autowired
	private EddService eddService;
	@Autowired
	private RegistrationService registrationService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private TransaksiService transaksiService;
	@Autowired
	private EktpService ektpService;
	@Autowired
	private AccountService userService;
	@Autowired
	private BcaCoidService bcaCoIdService;
	@Autowired
	private ReferenceNumberExtractor referenceNumberExtractor;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private ObjectMapper objectMapper;
	@Value("${registration.user-id}")
	private String registrationUserId;
	@Value("${registration.spv-id}")
	private String registrationSpvId;
	@Value("${predin.user-id}")
	private String predinUserId;
	@Value("${predin.spv-id}")
	private String predinSpvId;
	@Value("${cis.user-id}")
	private String cisUserId;
	@Autowired
	private CallLogService callLogService;
	@Autowired
	private MasterDataBranchService masterDataBranchService;
	@Autowired
	private MasterDataTransactionCodeService masterDataTransactionCodeService;
	@Autowired
	private ReferensiRepository referensiRepository;
	@Value("${default.branch-code}")
	private String defaultBranchCode;

	int retryCreateSecondAccount =0;
	@Override
	public RingkasanTransaksiData process(String noRef, String callLogId, String username) {
		String noReferensi = noRef;

		// Referensi referensi = referensiService.findOneLastReferensiByNoRef(noRef);
//		Referensi referensi = referensiRepository.findLastByNoRef(noRef);
		Referensi referensi = referensiRepository.findLastByNoRef2(noRef);
		if (referensi == null) {
			log.debug("no ref {} : referensi not found", noReferensi);
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND,
					"reference number not found");
		}
		
		// set to last no ref
		noReferensi = referensi.getNoRef();
		log.debug("no ref {} setted to last no ref", noReferensi);
		
		Transaksi transaksi = referensi.getTransaksi();
		if (transaksi == null) {
			log.debug("no ref {} : transaksi not found", noReferensi);
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "transaction not found");
		}
		log.debug("no ref {} : transaksi id : {}", noReferensi, transaksi.getId());

		Account account = userService.findByUsername(username);

		// set data
		CompletData data = getData(transaksi, username);

		// check customer status
		CustomerStatus customerStatus = getCustomerStatus(username, account.getBranchCode() ,noReferensi, transaksi, data);

		Optional<CallLog> callLog = callLogService.findById(callLogId);

		boolean isNasabahExistingByAgent = callLog.filter(call -> "EXISTING".equalsIgnoreCase(call.getQrJenisNasabah())).isPresent();

		boolean isNasabahExisting = "Y".equalsIgnoreCase(data.getConfirmedEformData().getNasabahExisting()) || isNasabahExistingByAgent ? true : false;
		boolean isNrt = "T".equalsIgnoreCase(customerStatus.getCustomerNrtStatus()) || "TP".equalsIgnoreCase(customerStatus.getCustomerNrtStatus()) ? true : false;
		boolean isChangeProduct = data.getIsProdukEdited();

		

		RingkasanTransaksiData transaksiData = new RingkasanTransaksiData();
		transaksiData.setUsername(account.getUsername());
		transaksiData.setBranchCode(account.getBranchCode());
		transaksiData.setCurrentNoRef(noReferensi);
		// transaksiData.setReferensi(referensi);
		transaksiData.setTransaksi(transaksi);
		transaksiData.setCustomerStatus(customerStatus);
		transaksiData.setData(data);
		transaksiData.setIsNasabahExisting(isNasabahExisting);
		transaksiData.setIsNrt(isNrt);
		transaksiData.setIsChangeProduct(isChangeProduct);
		transaksiData.setCallLog(callLog.orElse(null));
		return transaksiData;
	}

	@Async
	@Override
	public void processAsync(RingkasanTransaksiData ringkasanTransaksiData, String callLogId) {
		doProcess(ringkasanTransaksiData, callLogId);
	}


	// retray from crm
	@Override
	public Output<RetryOutputSchema> processRetry(String noRef, String norRefCretedDate, String username) {
		// crm user authentication
		List<GrantedAuthority> authorities = new ArrayList<>();
		UsernamePasswordAuthenticationToken crmUser = new UsernamePasswordAuthenticationToken(username, "", authorities);
		
		// register authentication to current context
		SecurityContextHolder.getContext().setAuthentication(crmUser);		
		
		Output<RetryOutputSchema> output = new Output<RetryOutputSchema>();
		RetryOutputSchema outputSchema = new RetryOutputSchema();
		ErrorSchema errorSchema = new ErrorSchema();
		ErrorMessage errorMessage = new ErrorMessage();

		String noReferensi = noRef;
		
		Referensi referensi = referensiRepository.findLastByNoRef2(noRef);
		if (referensi == null) {
			log.debug("no ref {} : referensi not found", noReferensi);
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND,
					"reference number not found");
		}
		
		// set to last no ref
		noReferensi = referensi.getNoRef();

		Transaksi transaksi = null;
		transaksi = referensi.getTransaksi();
		
		if (!transaksi.getEformCreatedDate().equals(norRefCretedDate))
		{
			log.debug("no ref {} : referensi not found", noReferensi);
			errorMessage.setEnglish("Transaction not found");
			errorMessage.setIndonesian("Transaksi tidak ditemukan");
			errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_NOT_FOUND);
			errorSchema.setErrorMessage(errorMessage);
			output.setErrorSchema(errorSchema);
			return output;
		}

		Optional<CallLog> callLog = callLogService.findLastByNoRefAndNoRefCreatedDate(noRef, norRefCretedDate);
		if (callLog.isPresent()) {
//			transaksi = transaksiService.findById(callLog.get().getTransaksiId()).get();
			
			if (transaksi.getProcessRunning()) {
				errorMessage.setEnglish("Transaction status in progress");
				errorMessage.setIndonesian("Status transaksi dalam proses");
				errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_IN_PROCCESS);				
				errorSchema.setErrorMessage(errorMessage);
				output.setErrorSchema(errorSchema);
				return output;
			}

			TransactionStatusConverter.Status status = TransactionStatusConverter.convert(callLog.get().getTransaksiStatus(), callLog.get().getTolakMdId(), callLog.get().getWorkReadyDate());
			if (status == TransactionStatusConverter.Status.Terputus || status == TransactionStatusConverter.Status.Ditolak || status == TransactionStatusConverter.Status.Berhasil) {
				// check transaction status
				errorMessage.setEnglish("Transaction status not match");
				errorMessage.setIndonesian("Status transaksi tidak sesuai");
				errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_NOT_MATCH);				
				errorSchema.setErrorMessage(errorMessage);
				output.setErrorSchema(errorSchema);
				return output;
			}

			

		} else {
			log.debug("no ref {} : referensi not found", noReferensi);
			errorMessage.setEnglish("Transaction not found");
			errorMessage.setIndonesian("Transaksi tidak ditemukan");
			errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_NOT_FOUND);
			errorSchema.setErrorMessage(errorMessage);
			output.setErrorSchema(errorSchema);
			return output;
		}
		
		if (transaksi == null) {
			log.debug("no ref {} : transaksi not found", noReferensi);
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "transaction not found");
		}
		log.debug("no ref {} : transaksi id : {}", noReferensi, transaksi.getId());

//		Account account = userService.findByUsername(username);

		// set data
		CompletData data = getData(transaksi, username);

		// check customer status
		CustomerStatus customerStatus = getCustomerStatus(username, defaultBranchCode ,noReferensi, transaksi, data);

		boolean isNasabahExistingByAgent = callLog.filter(call -> "EXISTING".equalsIgnoreCase(call.getQrJenisNasabah())).isPresent();

		boolean isNasabahExisting = "Y".equalsIgnoreCase(data.getConfirmedEformData().getNasabahExisting()) || isNasabahExistingByAgent ? true : false;
		boolean isNrt = "T".equalsIgnoreCase(customerStatus.getCustomerNrtStatus()) || "TP".equalsIgnoreCase(customerStatus.getCustomerNrtStatus()) ? true : false;
		boolean isChangeProduct = data.getIsProdukEdited();

		

		RingkasanTransaksiData transaksiData = new RingkasanTransaksiData();
		transaksiData.setUsername(username);
		transaksiData.setBranchCode(defaultBranchCode);
		transaksiData.setCurrentNoRef(noReferensi);
		// transaksiData.setReferensi(referensi);
		transaksiData.setTransaksi(transaksi);
		transaksiData.setCustomerStatus(customerStatus);
		transaksiData.setData(data);
		transaksiData.setIsNasabahExisting(isNasabahExisting);
		transaksiData.setIsNrt(isNrt);
		transaksiData.setIsChangeProduct(isChangeProduct);
		transaksiData.setCallLog(callLog.orElse(null));

		// process transaction
		doProcess(transaksiData, callLog.get().getId());
		
		
		Optional<Transaksi> resultTransaction = transaksiService.findById(callLog.get().getTransaksiId());

		if (resultTransaction.isPresent()) {
			Transaksi trx = resultTransaction.get();
			if (trx.getTransactionStatus() == null && !trx.getIsTransactionStepFailed()) {
				errorSchema.setErrorCode(TransactionErrorCode.RETRY_SUCCESS);
				errorMessage.setEnglish("Success");
				errorMessage.setIndonesian("Berhasil");
			} else if (trx.getIsTransactionStepFailed()) {
				errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_FAILED);
				errorMessage.setEnglish("Proccess failed at "+trx.getTransactionStatus().name()+ " service");
				errorMessage.setIndonesian("Proses gagal pada service "+trx.getTransactionStatus().name());
			}
		} else {
			errorSchema.setErrorCode(TransactionErrorCode.RETRY_ERROR_TRANSACTION_NOT_FOUND);
			errorMessage.setEnglish("Transaction not found");
			errorMessage.setIndonesian("Transaksi tidak ditemukan");
		}
		
		outputSchema.setReferenceNumber(noRef);
		outputSchema.setStatus(TransactionErrorCode.RETRY_SUCCESS.equals(errorSchema.getErrorCode()) ? "Success" : "Failed");
		errorSchema.setErrorMessage(errorMessage);
		output.setErrorSchema(errorSchema);
		output.setOutputSchema(outputSchema);

		printLog(output);

		return output;
	}
	

	private void doProcess(RingkasanTransaksiData ringkasanTransaksiData, String callLogId) {
		String username = ringkasanTransaksiData.getUsername();
//		Account account = ringkasanTransaksiData.getAccount();
		String branchCode = ringkasanTransaksiData.getBranchCode();
		
		String noReferensi = ringkasanTransaksiData.getCurrentNoRef();
		// Referensi referensi = ringkasanTransaksiData.getReferensi();
		Transaksi transaksi = ringkasanTransaksiData.getTransaksi();
		CompletData data = ringkasanTransaksiData.getData();
		CustomerStatus customerStatus = ringkasanTransaksiData.getCustomerStatus();

		boolean isNasabahExisting = ringkasanTransaksiData.getIsNasabahExisting();
		boolean isNrt = ringkasanTransaksiData.getIsNrt();
		boolean isChangeProduct = ringkasanTransaksiData.getIsChangeProduct();

		CallLog callLog = ringkasanTransaksiData.getCallLog();

		// set process running true
		transaksi.setProcessRunning(true);
		transaksiService.update(transaksi);
		

		// notifikasi transaksi in progress
		if (referenceNumberExtractor.parseToChannel(noReferensi) == ChannelType.BCA_MOBILE) {
			if (isInTransaction(transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update status in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE, isNasabahExisting, isNrt, isChangeProduct, callLog);
				
				try {
					log.debug(
							"status ::::::::::::::::::::::::::::::::::::::::::::: update status in progress | bca mobile");
					UpdateStatusRequest updateStatus = new UpdateStatusRequest();
					updateStatus.setTransactionStatus(UpdateStatusRequest.TRANSACTION_STATUS_ON_LOCK);
					updateStatus.setReffNumber(noReferensi);
					updateStatus.setHpNumber(data.getConfirmedEformData().getKodeNegaraNomorHp() + data.getConfirmedEformData().getNomorHp());
					updateStatus.setUpdateOfficer(username);
					UpdateStatusResponse updateStatusResponse = mBankService.updateStatus(updateStatus);
					
					log.debug("no ref {} : mbank update status success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE, isNasabahExisting,
						isNrt, isChangeProduct, callLog);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : update status failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE, isNasabahExisting,
							isNrt, isChangeProduct, callLog);
					return;
				}
			}
		} else if (referenceNumberExtractor.parseToChannel(noReferensi) == ChannelType.BCA_CO_ID) {
			if (isInTransaction(transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update status in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					log.debug(
							"status ::::::::::::::::::::::::::::::::::::::::::::: update status in progress | bca co id");
					com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest request = new com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest();
					request.setTransactionStatus(
							com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest.TRANSACTION_STATUS_ON_LOCK);
					request.setReffNumber(noReferensi);
					request.setHpNumber(data.getConfirmedEformData().getKodeNegaraNomorHp() + data.getConfirmedEformData().getNomorHp());
					request.setUpdateOfficer(username);
					com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse response = bcaCoIdService
							.updateAccountStatus(request, username);

					log.debug("no ref {} : bca co id update status success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID, isNasabahExisting,
						isNrt, isChangeProduct, callLog);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : update status failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID, isNasabahExisting,
							isNrt, isChangeProduct, callLog);
					return;
				}
			}
		}

		// check data produk berubah ?
		if (isChangeProduct) {
			log.debug("status ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: product changed");

			// data produk berubah
			// generate reference number
			if (isInTransaction(transaksi, TransactionStatus.GENERATE_EFORM)) {
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt, isChangeProduct, callLog);

				// log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: generate reference number");
				// if (!data.getIsRefNumRegenerated()) {
				// 	EformGenerateReferenceNumberResponse generateRefNumber = null;
				// 	try {
				// 		generateRefNumber = eformService.generateReferenceNumber();
				// 		log.debug("no ref {} : eform generate reference number success", noReferensi);
				// 		log.debug("no ref {} : eform reference number change to {} ", noReferensi,
				// 				generateRefNumber.getRefNum());

				// 		// save generated ref number
				// 		transaksi.setRefNum(generateRefNumber.getRefNum());
				// 		transaksi.setIsRefNumRegenerated(true);
				// 		transaksiService.update(transaksi);

				// 		Referensi ref = new Referensi();
				// 		ref.setTransaksi(transaksi);
				// 		ref.setNoRef(generateRefNumber.getRefNum());
				// 		referensiService.create(ref);

				// 		noReferensi = generateRefNumber.getRefNum();

				// 		// update complete data
				// 		data = getData(transaksi, username);

				// 		log.debug("no ref {} : is current no ref", noReferensi);

				// 		EformSaveResponse response = saveEform(noReferensi, username, transaksi, data);
				// 		if (response == null)
				// 			return;

				// 		log.debug("no ref {} : generate reference number success", noReferensi);
				// 		nextTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
				// 				isChangeProduct, callLog);
				// 	} catch (Exception e) {
				// 		e.printStackTrace();
				// 		log.error("no ref {} : eform generate reference number failed : {} ", noReferensi,
				// 				e.getMessage());
				// 		stopTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
				// 				isChangeProduct, callLog);
				// 		return;
				// 	}
				// } else {
				// 	try {
				// 		EformSaveResponse response = saveEform(noReferensi, username, transaksi, data);
				// 		if (response == null)
				// 			return;
						
				// 		log.debug("no ref {} : generate reference number success", noReferensi);
				// 		nextTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
				// 				isChangeProduct, callLog);
				// 	} catch (Exception e) {
				// 		e.printStackTrace();
				// 		log.error("no ref {} : eform generate reference number failed : {} ", noReferensi,
				// 				e.getMessage());
				// 		stopTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
				// 				isChangeProduct, callLog);
				// 		return;
				// 	}

				// }

				try {
					EformSaveResponse response = saveEform(true, noReferensi, username, transaksi, data);
					if (response == null)
						return;
					
					// save new reference number from eform
					transaksi.setIsRefNumRegenerated(true);
					transaksi.setNewRefNumber(response.getRefNum());
					transaksiService.update(transaksi);

					Referensi ref = new Referensi();
					ref.setTransaksi(transaksi);
					ref.setNoRef(response.getRefNum());
					referensiService.create(ref);

					// generated no ref
					noReferensi = response.getRefNum();

					log.debug("no ref {} : generate new eform success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : generate new eform failed : {} ", noReferensi,
							e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.GENERATE_EFORM, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
					return;
				}

			}

			// update nomor referensi ke channel
			if (referenceNumberExtractor.parseToChannel(noReferensi) == ChannelType.BCA_MOBILE) {
				if (isInTransaction(transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE)) {
					try {
						log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update no ref to channel in progress | bca mobile");
						progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE, isNasabahExisting, isNrt, isChangeProduct, callLog);

						AccountTypeConverter.Type accountType = AccountTypeConverter.convert(
							data.getConfirmedEformData().getJenisRekening(),
							data.getConfirmedEformData().getStatusTahapanGold());

						UpdateReferenceNumberRequest updateReferenceNumber = new UpdateReferenceNumberRequest();
						updateReferenceNumber.setReffNumber(transaksi.getRefNum());
						updateReferenceNumber.setHpNumber(data.getConfirmedEformData().getKodeNegaraNomorHp() + data.getConfirmedEformData().getNomorHp());
						updateReferenceNumber.setNewReffNumber(transaksi.getNewRefNumber());
						updateReferenceNumber.setOpenaccType(isNasabahExisting ? "2" : "1");
						updateReferenceNumber.setAccountType(UpdateReferenceNumberAccountTypeConverter.convert(accountType));
						updateReferenceNumber.setUpdateOfficer(username);
						mBankService.updateReferenceNumber(updateReferenceNumber);
						
						log.debug("no ref {} : update nomor referensi ke channel success", noReferensi);
						nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE, isNasabahExisting,
								isNrt, isChangeProduct, callLog);
					} catch(Exception e) {
						log.error("no ref {} : update nomor referensi ke channel failed : {} ", noReferensi,
								e.getMessage());
						stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE, isNasabahExisting,
								isNrt, isChangeProduct, callLog);
						return;
					}
				}
			} else if (referenceNumberExtractor.parseToChannel(noReferensi) == ChannelType.BCA_CO_ID) {
				if (isInTransaction(transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID)) {
					try {
						log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update no ref to channel in progress | bca co id");
						progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID, isNasabahExisting, isNrt, isChangeProduct, callLog);

						AccountTypeConverter.Type accountType = AccountTypeConverter.convert(
							data.getConfirmedEformData().getJenisRekening(),
							data.getConfirmedEformData().getStatusTahapanGold());
						com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest request = new com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest();
						request.setReffNumber(transaksi.getRefNum());
						request.setHpNumber(data.getConfirmedEformData().getKodeNegaraNomorHp() + data.getConfirmedEformData().getNomorHp());
						request.setNewReffNumber(transaksi.getNewRefNumber());
						request.setOpenaccType(isNasabahExisting ? "2" : "1");
						request.setAccountType(UpdateReferenceNumberAccountTypeConverter.convert(accountType));
						request.setUpdateOfficer(username);
						bcaCoIdService.updateReferenceNumber(request, username);

						log.debug("no ref {} : update nomor referensi ke channel success", noReferensi);
						nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID, isNasabahExisting,
								isNrt, isChangeProduct, callLog);
					} catch(Exception e) {
						log.error("no ref {} : update nomor referensi ke channel failed : {} ", noReferensi,
								e.getMessage());
						stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID, isNasabahExisting,
								isNrt, isChangeProduct, callLog);
						return;
					}
				}
			}


			// update nomor referensi ke predin
			if (isInTransaction(transaksi, TransactionStatus.UPDATE_NO_REF_TO_PREDIN)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update no ref to predin in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_PREDIN, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					PredinUpdateReferenceNumberRequest predinUpdateNoRef = new PredinUpdateReferenceNumberRequest();
					predinUpdateNoRef.setOldReferenceNumber(transaksi.getRefNum());
					predinUpdateNoRef.setNewReferenceNumber(transaksi.getNewRefNumber());
					predinService.updateReferenceNumber(username, branchCode, predinUpdateNoRef);
					log.debug("no ref {} : update nomor referensi ke predin success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_PREDIN, isNasabahExisting,
							isNrt, isChangeProduct, callLog);
				} catch (Exception e) {
					log.error("no ref {} : update nomor referensi ke predin failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_NO_REF_TO_PREDIN, isNasabahExisting,
							isNrt, isChangeProduct, callLog);
					return;
				}
			}

		} else {
			log.debug("status ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: prouct not changed");
			// data produk tidak berubah
			// save eform
			if (isInTransaction(transaksi, TransactionStatus.SAVE_EFORM)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: save eform in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EFORM, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					EformSaveResponse response = saveEform(false, noReferensi, username, transaksi, data);
					if (response == null)
						return;
					log.debug("no ref {} : save efom success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EFORM, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : eform save failed", noReferensi);
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EFORM, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
					return;
				}
			}
		}

		// cek nasabah baru atau existing
		// if ("Y".equalsIgnoreCase(data.getConfirmedEformData().getNasabahExisting())) {
		if (isNasabahExisting) {
			// nasabah exsiting
			// update cis
			if (isInTransaction(transaksi, TransactionStatus.UPDATE_CIS)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update cis in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_CIS, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					
					OutputSchemaSearch rawCis = objectMapper.readValue(transaksi.getRawCis(), OutputSchemaSearch.class);
					OutputSchemaSearchItem cisItem = rawCis.getCisIndividu()
															.stream()
															.filter(item -> item.getCisCustomerNumber().equals(transaksi.getCisCustomerNumber()))
															.findFirst()
															.orElse(null);

					String jsonCisItem = objectMapper.writeValueAsString(cisItem);
															
					RequestUpdateCis requestUpdateCis = objectMapper.readValue(jsonCisItem, RequestUpdateCis.class);
					if (requestUpdateCis == null)
						requestUpdateCis = new RequestUpdateCis();

					requestUpdateCis.setOfficerUserId(branchCode+cisUserId);
					
					RequestUpdateCustomerMasterData cisMasterData = requestUpdateCis.getCustomerMasterData();
					if (cisMasterData == null)
						cisMasterData = new RequestUpdateCustomerMasterData();

					cisMasterData.setIdNumber(data.getConfirmedEformData().getNik());
					cisMasterData.setIdExpiredDate("31129999");
					cisMasterData.setBirthPlace(data.getConfirmedEformData().getTempatLahir());
					cisMasterData.setSex(data.getConfirmedEformData().getJenisKelamin());
					cisMasterData.setReligion(data.getConfirmedEformData().getAgama());
					cisMasterData.setTaxIndicator(data.getConfirmedEformData().getNpwp());
					cisMasterData.setTaxNo(data.getConfirmedEformData().getNomorNpwp());
					cisMasterData.setFatcaFormFlag(data.getConfirmedEformData().getFormPerpajakanFatca());
					cisMasterData.setAeoiFormFlag(data.getConfirmedEformData().getFormPerpajakanCrs());
					cisMasterData.setAeoiAssessable(data.getConfirmedEformData().getWajibPajakNegaraLain());
					cisMasterData.setCitizenshipCode(data.getConfirmedEformData().getNegara());
					cisMasterData.setFatcaAssessable(data.getConfirmedEformData().getWajibFatca());
					cisMasterData.setAeoiAssessable(data.getConfirmedEformData().getWajibPajakNegaraLain());
					cisMasterData.setProvideDataPermission(
							YAndTValueConverter.convert(data.getConfirmedEformData().getPersetujuanDataPihakKetiga()));
					cisMasterData.setSmsAdvertisement(
							YAndTValueConverter.convert(data.getConfirmedEformData().getPersetujuanSms()));
					cisMasterData.setEmailAdvertisement(
							YAndTValueConverter.convert(data.getConfirmedEformData().getPersetujuanEmail()));
					cisMasterData.setTelephoneAdvertisement(
							YAndTValueConverter.convert(data.getConfirmedEformData().getPersetujuanTelepon()));
					requestUpdateCis.setCustomerMasterData(cisMasterData);

					RequestUpdateCustomerNameAndPhone cisNameAndPhone = requestUpdateCis.getCustomerNameAndPhone();
					if (cisNameAndPhone == null) 
						cisNameAndPhone = new RequestUpdateCustomerNameAndPhone();

					cisNameAndPhone.setFullName(data.getConfirmedEformData().getNamaNasabah());
					
					Phone homePhone = cisNameAndPhone.getHomePhone();
					if (homePhone == null)
						homePhone = new Phone();
					homePhone.setCountryCode(data.getConfirmedEformData().getKodeNegaraTeleponRumah());
					homePhone.setAreaCode(data.getConfirmedEformData().getKodeAreaTeleponRumah());
					homePhone.setPhoneNumber(data.getConfirmedEformData().getNomorTeleponRumah());
					cisNameAndPhone.setHomePhone(homePhone);
					Phone officePhone = cisNameAndPhone.getOfficePhone();
					if (officePhone == null)
						officePhone = new Phone();
					officePhone.setCountryCode(data.getConfirmedEformData().getKodeNegaraTeleponKantor());
					officePhone.setAreaCode(data.getConfirmedEformData().getKodeAreaTeleponKantor());
					officePhone.setPhoneNumber(data.getConfirmedEformData().getNomorTeleponKantor());
					cisNameAndPhone.setOfficePhone(officePhone);
					requestUpdateCis.setCustomerNameAndPhone(cisNameAndPhone);

					CustomerAddress cisAddress = requestUpdateCis.getCustomerAddress();
					if (cisAddress == null)
						cisAddress = new CustomerAddress();
					cisAddress.setStreet(data.getConfirmedEformData().getNamaJalan());
					cisAddress.setBuilding(data.getConfirmedEformData().getNamaGedung());
					cisAddress.setZipCode(data.getConfirmedEformData().getKodePos());
					cisAddress.setCity(data.getConfirmedEformData().getKabupaten());
					cisAddress.setProvince(data.getConfirmedEformData().getProvinsi());
					cisAddress.setDistrict(data.getConfirmedEformData().getKecamatan());
					cisAddress.setSubDistrict(data.getConfirmedEformData().getKelurahan());
					cisAddress.setRt(data.getConfirmedEformData().getTransientRt());
					cisAddress.setRw(data.getConfirmedEformData().getTransientRw());
					cisAddress.setCountry(data.getConfirmedEformData().getNegaraAlamat());
					requestUpdateCis.setCustomerAddress(cisAddress);

					RequestUpdateCustomerDemographicInformation cisDemographic = requestUpdateCis.getCustomerDemographicInformation();
					if (cisDemographic == null)
						cisDemographic = new RequestUpdateCustomerDemographicInformation();
					cisDemographic.setIncome(data.getConfirmedEformData().getTotalPenghasilan());
					cisDemographic.setBirthDate(DateConverter.convert(data.getConfirmedEformData().getTanggalLahir()));
					cisDemographic.setMothersName(data.getConfirmedEformData().getNamaGadisIbuKandung());
					cisDemographic.setMaritalStatus(data.getConfirmedEformData().getStatusPerkawinan());
					cisDemographic.setOccupation(data.getConfirmedEformData().getPekerjaan());
					cisDemographic.setBirthCountryCode(data.getConfirmedEformData().getNegaraLahirFatca());
					requestUpdateCis.setCustomerDemographicInformation(cisDemographic);

					CustomerComplementData cisComplement = requestUpdateCis.getCustomerComplementData();
					if (cisComplement == null)
						cisComplement = new CustomerComplementData();

					ContactInformation cisContact = cisComplement.getContactInformation();
					if (cisContact == null)
						cisContact = new ContactInformation();

					List<Handphone> handphones = new ArrayList<>();
					if (cisContact.getHandphone() == null || cisContact.getHandphone().size() == 0 ) {
						Handphone handphone = new Handphone();
						handphone.setCountryCode(data.getConfirmedEformData().getKodeNegaraNomorHp());
						handphone.setPhoneNumber(data.getConfirmedEformData().getNomorHp());
						handphones.add(handphone);
						cisContact.setHandphone(handphones);
					}
					
					if (cisContact.getEmailAddress() == null || cisContact.getEmailAddress().isEmpty()){
						cisContact.setEmailAddress(data.getConfirmedEformData().getEmail());
					}
					cisContact.setStreet(data.getConfirmedEformData().getNamaJalanRekeningKoran());
					cisContact.setBuilding(data.getConfirmedEformData().getNamaGedungRekeningKoran());
					cisContact.setZipCode(data.getConfirmedEformData().getKodePosRekeningKoran());
					cisContact.setCity(data.getConfirmedEformData().getKabupatenRekeningKoran());
					cisContact.setDistrict(data.getConfirmedEformData().getKecamatanRekeningKoran());
					cisContact.setSubDistrict(data.getConfirmedEformData().getKelurahanRekeningKoran());
					cisContact.setRt(data.getConfirmedEformData().getTransientRtRekeningKoran());
					cisContact.setRw(data.getConfirmedEformData().getTransientRwRekeningKoran());
					cisContact.setProvince(data.getConfirmedEformData().getProvinsiRekeningKoran());
					cisContact.setCountry(data.getConfirmedEformData().getNegaraAlamatRekeningKoran());
					cisComplement.setContactInformation(cisContact);
					cisComplement.setBusinessSector(data.getConfirmedEformData().getBidangUsaha());
					cisComplement.setPosition(data.getConfirmedEformData().getJabatan());
					cisComplement.setIncome(data.getConfirmedEformData().getSumberPenghasilan());
					cisComplement.setCompanyName(data.getConfirmedEformData().getNamaTempatBekerja());
					cisComplement.setCompanyAddress(Arrays.asList(data.getConfirmedEformData().getAlamatKantor1(),
							data.getConfirmedEformData().getAlamatKantor2(),
							data.getConfirmedEformData().getAlamatKantor3()));
					cisComplement.setAccountPurpose(data.getConfirmedEformData().getTujuanPembukaanRekening());
					requestUpdateCis.setCustomerComplementData(cisComplement);

					RequestUpdateCustomerRemark customerRemark = requestUpdateCis.getCustomerRemark();
					if (customerRemark == null)
						customerRemark = new RequestUpdateCustomerRemark();
					
					customerRemark.setRemark("");
					customerRemark.setRemarkEffectiveDate(DateConverter.convert(new Date()));
					customerRemark.setRemarkExpiredDate(DateConverter.convert(new Date()));
					requestUpdateCis.setCustomerRemark(customerRemark);

					// replce 00001 to 1
					String customerNumber = transaksi.getCisCustomerNumber().replaceFirst("^0+(?!$)", "");
					
					OutputSnakeCase<RequestUpdateOutputSchema> response = cisService.updateCis(customerNumber, requestUpdateCis);
					if ("ESB-00-000".equals(response.getErrorSchema().getErrorCode())) {
						log.debug("no ref {} : update cis success", noReferensi);
						nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_CIS, isNasabahExisting, isNrt, isChangeProduct, callLog);
					} else {
						log.error("no ref {} : update cis failed : {} ", noReferensi, response.getErrorSchema().getErrorCode());
						stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_CIS, isNasabahExisting, isNrt, isChangeProduct, callLog);
						return;
					}
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : update cis failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_CIS, isNasabahExisting, isNrt, isChangeProduct, callLog);
					return;
				}
			}

			// create second account / create rekening
			if (isInTransaction(transaksi, TransactionStatus.SECOND_ACCOUNT)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: second account in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.SECOND_ACCOUNT, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					AccountTypeConverter.Type accountType = AccountTypeConverter.convert(
							data.getConfirmedEformData().getJenisRekening(),
							data.getConfirmedEformData().getStatusTahapanGold());
					TipeKartuPasporBcaConverter.Type tipeKartuPassporBca = TipeKartuPasporBcaConverter
							.convert(data.getConfirmedEformData().getTipeKartuPasporBca());
					JenisKartuPasporBcaConverter.Type jenisKartuPassporBca = JenisKartuPasporBcaConverter
							.convert(data.getConfirmedEformData().getJenisKartuPasporBca());

					boolean isTahapanGold = "Y".equals(data.getConfirmedEformData().getStatusTahapanGold());

					CreateRekeningRequest rekeningRequest = new CreateRekeningRequest();
					rekeningRequest.setCustomerNumber(transaksi.getCisCustomerNumber());
					rekeningRequest.setAccountNumber("");
					rekeningRequest.setReferenceNumber(Strings.nullToEmpty(noReferensi));
					rekeningRequest.setAction("");
					rekeningRequest.setSpvId(isTahapanGold ? registrationSpvId : "");
					rekeningRequest.setCustomerPekerjaanDesc1(Strings.nullToEmpty(data.getConfirmedEformData().getTransientPekerjaan()));
					rekeningRequest.setCustomerPekerjaanDesc2(Strings.nullToEmpty(data.getConfirmedEformData().getPekerjaanTier2()));
					rekeningRequest.setCustomerPekerjaanDesc3(Strings.nullToEmpty(data.getConfirmedEformData().getPekerjaanTier3()));
					CreateRekeningAccountInformation accountInformation = new CreateRekeningAccountInformation();
					accountInformation.setCustomerNoRekBaru("");
					accountInformation
							.setTahapanGoldStatus(Strings.nullToEmpty(StatusTahapanGoldConverter.convert(accountType)));
					accountInformation.setCostCenter(Strings.nullToEmpty(data.getConfirmedEformData().getCostCenter()));
					accountInformation.setCustomerRekeningUntuk(Strings.nullToEmpty(data.getConfirmedEformData().getRekeningUntuk()));
					accountInformation.setStatusRekeningGabungan("");
					accountInformation.setRekGabunganNama("");
					accountInformation.setRekGabunganNoCustomer("");
					accountInformation.setRekGabunganNamaTercetakPadaKartu("");
					accountInformation.setRekGabunganNoTandaPengenal("");
					accountInformation.setBukuTabungan(Strings.nullToEmpty(data.getConfirmedEformData().getDenganBuku()));
					accountInformation.setProdukPasporBcaJenis(Strings.nullToEmpty(data.getConfirmedEformData().getJenisKartuPasporBca()));
					accountInformation.setProdukPasporBcaTipe(Strings.nullToEmpty(data.getConfirmedEformData().getTipeKartuPasporBca()));
					accountInformation.setProdukPasporBcaInstantNo("");
					accountInformation
							.setProdukPasporBcaPetunjukLayar(Strings.nullToEmpty(data.getConfirmedEformData().getBahasaPetunjukLayarAtm()));
					accountInformation.setFasilitasKlikbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanKlikbca())));
					accountInformation.setFasilitasKeybcaIb(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanKeybca())));
					accountInformation.setFasilitasKeybcaSn("");
					accountInformation.setFasilitasKlikbcaHp("");
					accountInformation.setFasilitasMbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanMbca())));
					accountInformation.setFasilitasAktivasiFinMbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanFinMbca())));
					accountInformation.setFasilitasMbcaHp(Strings.nullToEmpty(data.getConfirmedEformData().getNomorHpMbca()));
					accountInformation.setCustomerCabang(Strings.nullToEmpty(data.getConfirmedEformData().getCabang()));
					accountInformation.setAccountType(Strings.nullToEmpty(data.getConfirmedEformData().getJenisRekening()));
					accountInformation.setKodeNegara("ID");
					accountInformation.setProdukMataUang(Strings.nullToEmpty(ProdukMataUangConverter.convert(accountType)));
					accountInformation
							.setServiceChargeCode(Strings.nullToEmpty(ServiceChargeCodeConverter.convert(accountType, tipeKartuPassporBca)));
					accountInformation.setBebanBiayaAdmin("1");
					accountInformation.setUserCode(Strings.nullToEmpty(
							UserCodeConverter.convert(accountType, tipeKartuPassporBca, jenisKartuPassporBca)));
					accountInformation.setPeriodeBiayaAdmin(Strings.nullToEmpty(PeriodeBiayaAdminConverter.convert(accountType)));
					accountInformation.setPeriodeRk(Strings.nullToEmpty(PeriodeRkConverter.convert(accountType)));
					accountInformation.setPeriodeBunga(Strings.nullToEmpty(PeriodeBunganConverter.convert(accountType)));
					accountInformation.setInterestPlan(Strings.nullToEmpty(InterestPlanConverter.convert(accountType)));
					accountInformation.setOdPlan(Strings.nullToEmpty(OdPlanConverter.convert(accountType)));
					accountInformation.setGolonganPemilik("886");
					accountInformation.setWitholdingPlan(Strings.nullToEmpty(WitholdingPlanConverter.convert(accountType)));
					accountInformation.setKodePembebananPajak(Strings.nullToEmpty(KodePembebananPajakConverter.convert(accountType)));
					accountInformation.setKodePeroranganBisnis("P");
					accountInformation.setKodePendudukNonpenduduk("R");
					accountInformation.setFasilitasBbp("N");

					rekeningRequest.setAccountInformation(accountInformation);

					CreateRekeningResponse createRekeningResponse = registrationService.createRekening(username, branchCode, rekeningRequest);

					if (createRekeningResponse != null) {
						// update rekening to callLog
						callLog.setQrNoRekening(createRekeningResponse.getAccountNumber());
						callLog.setQrCisCustomerNumber(createRekeningResponse.getCisCustomerNumber());
						// callLogService.save(callLog);

						// log call log
						CallLog callResponse = callLogService.save(callLog).get();
						ObjectMapper mapper = new ObjectMapper();
						try {
							String sResponse = mapper.writeValueAsString(callResponse);
							log.debug("callLog rps from second account : response {} ", sResponse);
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}

						// save account number and cis customer number
						transaksi.setPemrekAccountNumber(createRekeningResponse.getAccountNumber());
						transaksi.setPemrekCisCustomerNumber(createRekeningResponse.getCisCustomerNumber());
						transaksiService.update(transaksi);
					}

					log.debug("no ref {} : create second account success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.SECOND_ACCOUNT, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
				} catch (Exception e) {
					log.error("no ref {} : create second account failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.SECOND_ACCOUNT, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
					return;
				}
			}

		} else {
			// nasabah baru
			// create first account / create cis and rekening
			if (isInTransaction(transaksi, TransactionStatus.FIRST_ACCOUNT)) {
				log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: first account in progress");
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.FIRST_ACCOUNT, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					AccountTypeConverter.Type accountType = AccountTypeConverter.convert(
							data.getConfirmedEformData().getJenisRekening(),
							data.getConfirmedEformData().getStatusTahapanGold());
					TipeKartuPasporBcaConverter.Type tipeKartuPassporBca = TipeKartuPasporBcaConverter
							.convert(data.getConfirmedEformData().getTipeKartuPasporBca());
					JenisKartuPasporBcaConverter.Type jenisKartuPassporBca = JenisKartuPasporBcaConverter
							.convert(data.getConfirmedEformData().getJenisKartuPasporBca());
					boolean isTahapanGold = "Y".equals(data.getConfirmedEformData().getStatusTahapanGold());

					CreateCisAndRekeningRequest request = new CreateCisAndRekeningRequest();
					request.setReferenceNumber(noReferensi);
					request.setAction("");
					request.setSpvId(isTahapanGold ? registrationSpvId : "");
					CustomerInformation ci = new CustomerInformation();

					ci.setCustomerNama(Strings.nullToEmpty(data.getConfirmedEformData().getNamaNasabah()));
					ci.setCustomerAlias("");
					ci.setCustomerNamaRef("");
					ci.setCustomerAgama(Strings.nullToEmpty(data.getConfirmedEformData().getAgama()));
					ci.setCustomerTandaPengenalNo(Strings.nullToEmpty(data.getConfirmedEformData().getNik()));
					// ci.setCustomerTandaPengenalBerlakuSampai(data.getConfirmedEformData().getBerlakuSampaiDengan());
					ci.setCustomerTandaPengenalBerlakuSampai("31129999");
					ci.setCustomerTandaPengenal(
							Strings.nullToEmpty(data.getConfirmedEformData().getTipeIdentitasLainnya()));
					ci.setCustomerTandaPengenalNoLainnya("");
					ci.setCustomerTempatLahir(Strings.nullToEmpty(data.getConfirmedEformData().getTempatLahir()));
					ci.setCustomerTanggalLahir(Strings.nullToEmpty(data.getConfirmedEformData().getTanggalLahir()));
					ci.setNegaraLahir(Strings.nullToEmpty(data.getConfirmedEformData().getNegaraLahirFatca()));
					ci.setNegaraWn(Strings.nullToEmpty(data.getConfirmedEformData().getNegara()));
					ci.setCustomerJenisKelamin(Strings.nullToEmpty(data.getConfirmedEformData().getJenisKelamin()));
					ci.setCustomerNamaIbuKandung(
							Strings.nullToEmpty(data.getConfirmedEformData().getNamaGadisIbuKandung()));
					ci.setCustomerKewarganegaraan("1");
					ci.setCustomerKitasKitap("");
					ci.setCustomerKitasKitapNo("");
					ci.setCustomerKitasKitapBerlakuSampai("");
					ci.setIdKecamatan(Strings.nullToEmpty(data.getConfirmedEformData().getKecamatan()));
					ci.setIdKelurahan(Strings.nullToEmpty(data.getConfirmedEformData().getKelurahan()));
					ci.setIdNamaJalan(Strings.nullToEmpty(data.getConfirmedEformData().getNamaJalan()));
					ci.setIdKota(Strings.nullToEmpty(data.getConfirmedEformData().getKabupaten()));
					ci.setIdKodePos(Strings.nullToEmpty(data.getConfirmedEformData().getKodePos()));
					ci.setIdNegara(Strings.nullToEmpty(data.getConfirmedEformData().getNegaraAlamat()));
					ci.setIdProvinsi(Strings.nullToEmpty(data.getConfirmedEformData().getProvinsi()));
					ci.setIdKomplekGedung(Strings.nullToEmpty(data.getConfirmedEformData().getNamaGedung()));
					ci.setIdRt(Strings.nullToEmpty(data.getConfirmedEformData().getTransientRt()));
					ci.setIdRw(Strings.nullToEmpty(data.getConfirmedEformData().getTransientRw()));
					ci.setKodeNegaraTelpRumah(
							Strings.nullToEmpty(data.getConfirmedEformData().getKodeNegaraTeleponRumah()));
					ci.setKodeAreaTelpRumah(
							Strings.nullToEmpty(data.getConfirmedEformData().getKodeAreaTeleponRumah()));
					ci.setTelpRumah(Strings.nullToEmpty(data.getConfirmedEformData().getNomorTeleponRumah()));
					ci.setDomisiliKecamatan(
							Strings.nullToEmpty(data.getConfirmedEformData().getKecamatanRekeningKoran()));
					ci.setDomisiliKelurahan(
							Strings.nullToEmpty(data.getConfirmedEformData().getKelurahanRekeningKoran()));
					ci.setDomisiliNamaJalan(
							Strings.nullToEmpty(data.getConfirmedEformData().getNamaJalanRekeningKoran()));
					ci.setDomisiliKota(Strings.nullToEmpty(data.getConfirmedEformData().getKabupatenRekeningKoran()));
					ci.setDomisiliKodePos(Strings.nullToEmpty(data.getConfirmedEformData().getKodePosRekeningKoran()));
					ci.setDomisiliNegara(
							Strings.nullToEmpty(data.getConfirmedEformData().getNegaraAlamatRekeningKoran()));
					ci.setDomisiliProvinsi(
							Strings.nullToEmpty(data.getConfirmedEformData().getProvinsiRekeningKoran()));
					ci.setDomisiliKomplekGedung(
							Strings.nullToEmpty(data.getConfirmedEformData().getNamaGedungRekeningKoran()));
					ci.setDomisiliRt(Strings.nullToEmpty(data.getConfirmedEformData().getTransientRtRekeningKoran()));
					ci.setDomisiliRw(Strings.nullToEmpty(data.getConfirmedEformData().getTransientRwRekeningKoran()));
					ci.setCustomerPekerjaan(Strings.nullToEmpty(data.getConfirmedEformData().getPekerjaan()));
					ci.setCustomerPekerjaanLainnya("");
					ci.setNamaKantor(Strings.nullToEmpty(data.getConfirmedEformData().getNamaTempatBekerja()));
					ci.setJabatan(Strings.nullToEmpty(data.getConfirmedEformData().getJabatan()));
					ci.setBidangUsaha(Strings.nullToEmpty(data.getConfirmedEformData().getBidangUsaha()));
					ci.setEmail(Strings.nullToEmpty(data.getConfirmedEformData().getEmail()));
					ci.setAlamatKerja1(Strings.nullToEmpty(data.getConfirmedEformData().getAlamatKantor1()));
					ci.setAlamatKerja2(Strings.nullToEmpty(data.getConfirmedEformData().getAlamatKantor2()));
					ci.setAlamatKerja3(Strings.nullToEmpty(data.getConfirmedEformData().getAlamatKantor3()));
					ci.setKodeNegaraTelpKantor(
							Strings.nullToEmpty(data.getConfirmedEformData().getKodeNegaraTeleponKantor()));
					ci.setKodeAreaTelpKantor(
							Strings.nullToEmpty(data.getConfirmedEformData().getKodeAreaTeleponKantor()));
					ci.setTelpKantor(Strings.nullToEmpty(data.getConfirmedEformData().getNomorTeleponKantor()));
					ci.setKodeNegaraFaxKantor("");
					ci.setKodeAreaFaxKantor("");
					ci.setFaxKantor("");
					ci.setCustomerNpwp(Strings.nullToEmpty(data.getConfirmedEformData().getNpwp()));
					ci.setCustomerNpwpNo(Strings.nullToEmpty(data.getConfirmedEformData().getNomorNpwp()));
					ci.setCustomerSumberPenghasilan(
							Strings.nullToEmpty(data.getConfirmedEformData().getSumberPenghasilan()));
					ci.setCustomerSumberPenghasilanLainnya(
							Strings.nullToEmpty(data.getConfirmedEformData().getSumberPenghasilanLainnya()));
					ci.setCustomerGajiTotal(Strings.nullToEmpty(data.getConfirmedEformData().getTotalPenghasilan()));
					ci.setCustomerTujuanBukaRek(
							Strings.nullToEmpty(data.getConfirmedEformData().getTujuanPembukaanRekening()));
					ci.setCustomerTujuanBukaRekLainnya("");
					ci.setKdNegaraHp1(Strings.nullToEmpty(data.getConfirmedEformData().getKodeNegaraNomorHp()));
					ci.setHp1(Strings.nullToEmpty(data.getConfirmedEformData().getNomorHp()));
					ci.setKdNegaraHp2("");
					ci.setHp2("");
					ci.setCustomerStatusKawin(Strings.nullToEmpty(data.getConfirmedEformData().getStatusPerkawinan()));
					ci.setKodeCustomerResikoTinggi(Strings.nullToEmpty(customerStatus.getCustomerCode()));
					ci.setMailCode(Strings.nullToEmpty(data.getConfirmedEformData().getPengirimanRekeningKoran()));
					ci.setPersetujuan(
							Strings.nullToEmpty(data.getConfirmedEformData().getPersetujuanDataPihakKetiga()));
					ci.setMediaInfoSms(Strings.nullToEmpty(data.getConfirmedEformData().getPersetujuanSms()));
					ci.setMediaInfoMail(Strings.nullToEmpty(data.getConfirmedEformData().getPersetujuanEmail()));
					ci.setMediaInfoPhone(Strings.nullToEmpty(data.getConfirmedEformData().getPersetujuanTelepon()));
					ci.setFormFatca(Strings.nullToEmpty(data.getConfirmedEformData().getFormPerpajakanFatca()));
					ci.setWajibFatca(Strings.nullToEmpty(data.getConfirmedEformData().getWajibFatca()));
					ci.setTin("");
					ci.setPejabatBank(Strings.nullToEmpty(PejabatBankConverter.convert(accountType)));
					ci.setFormAeoi(Strings.nullToEmpty(data.getConfirmedEformData().getFormPerpajakanCrs()));
					ci.setWpNegaraLain(Strings.nullToEmpty(data.getConfirmedEformData().getWajibPajakNegaraLain()));

					AccountInformation ac = new AccountInformation();
					// account information
					ac.setCustomerNoRekBaru("");
					ac.setAccountType(Strings.nullToEmpty(data.getConfirmedEformData().getJenisRekening()));
					ac.setTahapanGoldStatus(Strings.nullToEmpty(StatusTahapanGoldConverter.convert(accountType)));
					ac.setCostCenter(Strings.nullToEmpty(data.getConfirmedEformData().getCostCenter()));
					ac.setKodeNegara("ID");
					ac.setProdukMataUang(Strings.nullToEmpty(ProdukMataUangConverter.convert(accountType)));
					ac.setServiceChargeCode(
							Strings.nullToEmpty(ServiceChargeCodeConverter.convert(accountType, tipeKartuPassporBca)));
					ac.setBebanBiayaAdmin("1");
					ac.setUserCode(Strings.nullToEmpty(
							UserCodeConverter.convert(accountType, tipeKartuPassporBca, jenisKartuPassporBca)));
					ac.setPeriodeBiayaAdmin(Strings.nullToEmpty(PeriodeBiayaAdminConverter.convert(accountType)));
					ac.setPeriodeRk(Strings.nullToEmpty(PeriodeRkConverter.convert(accountType)));
					ac.setPeriodeBunga(Strings.nullToEmpty(PeriodeBunganConverter.convert(accountType)));
					ac.setInterestPlan(Strings.nullToEmpty(InterestPlanConverter.convert(accountType)));
					ac.setOdPlan(Strings.nullToEmpty(OdPlanConverter.convert(accountType)));
					ac.setGolonganPemilik("886");
					ac.setWitholdingPlan(Strings.nullToEmpty(WitholdingPlanConverter.convert(accountType)));
					ac.setKodePembebananPajak(Strings.nullToEmpty(KodePembebananPajakConverter.convert(accountType)));
					ac.setKodePeroranganBisnis("P");
					ac.setKodePendudukNonpenduduk("R");
					ac.setCustomerRekeningUntuk(Strings.nullToEmpty(data.getConfirmedEformData().getRekeningUntuk()));
					ac.setStatusRekeningGabungan("");
					ac.setRekGabunganNama("");
					ac.setRekGabunganNoCustomer("");
					ac.setRekGabunganNamaTercetakPadaKartu("");
					ac.setRekGabunganNoTandaPengenal("");
					ac.setBukuTabungan(Strings.nullToEmpty(data.getConfirmedEformData().getDenganBuku()));
					ac.setProdukPasporBcaJenis(
							Strings.nullToEmpty(data.getConfirmedEformData().getJenisKartuPasporBca()));
					ac.setProdukPasporBcaTipe(
							Strings.nullToEmpty(data.getConfirmedEformData().getTipeKartuPasporBca()));
					ac.setProdukPasporBcaInstantNo("");
					ac.setProdukPasporBcaPetunjukLayar(
							Strings.nullToEmpty(data.getConfirmedEformData().getBahasaPetunjukLayarAtm()));
					ac.setFasilitasKlikbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanKlikbca())));
					ac.setFasilitasKeybcaIb(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanKeybca())));
					ac.setFasilitasKeybcaSn("");
					ac.setFasilitasKlikbcaHp("");
					ac.setFasilitasMbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanMbca())));
					ac.setFasilitasAktivasiFinMbca(Strings.nullToEmpty(YesOrNoConverter
							.convertFromNumber(data.getConfirmedEformData().getFasilitasYangDiinginkanFinMbca())));
					ac.setFasilitasMbcaHp(Strings.nullToEmpty(data.getConfirmedEformData().getNomorHpMbca()));
					ac.setFasilitasBbp("N");
					ac.setCustomerCabang(Strings.nullToEmpty(data.getConfirmedEformData().getCabang()));
					

					// ac.setCustomerGajiUtama("");
					request.setCustomerInformation(ci);
					request.setAccountInformation(ac);
					request.setCustomerPekerjaanDesc1(Strings.nullToEmpty(data.getConfirmedEformData().getTransientPekerjaan()));
					request.setCustomerPekerjaanDesc2(
							Strings.nullToEmpty(data.getConfirmedEformData().getPekerjaanTier2()));
					request.setCustomerPekerjaanDesc3(
							Strings.nullToEmpty(data.getConfirmedEformData().getPekerjaanTier3()));

					CreateCisAndRekeningResponse response = registrationService.createCisAndRekening(username, branchCode, request);
					// save cis customer number in transaksi
					// untuk mempermudah memanggil ulang save edd
					if (response != null) {
						// update rekening to callLog
						callLog.setQrNoRekening(response.getAccountNumber());
						callLog.setQrCisCustomerNumber(response.getCisCustomerNumber());
						// callLogService.save(callLog);

						// log call log
						CallLog callResponse = callLogService.save(callLog).get();
						ObjectMapper mapper = new ObjectMapper();
						try {
							String sResponse = mapper.writeValueAsString(callResponse);
							log.debug("callLog rps from first account : response {} ", sResponse);
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}

						// save account number and cis customer number
						transaksi.setPemrekCisCustomerNumber(Strings.nullToEmpty(response.getCisCustomerNumber()));
						transaksi.setPemrekAccountNumber(Strings.nullToEmpty(response.getAccountNumber()));
						transaksiService.update(transaksi);
					}
					log.debug("no ref {} : first account success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.FIRST_ACCOUNT, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("no ref {} : create first account failed : {}", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.FIRST_ACCOUNT, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
					return;
				}
			}

		}

		// custmer is nrt ?
		if (isInTransaction(transaksi, TransactionStatus.SAVE_EDD)) {
			if (isNrt) {
				progressTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EDD, isNasabahExisting, isNrt, isChangeProduct, callLog);

				try {
					log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: save edd in progress");
					saveEdd(noReferensi, username, transaksi, data);

					log.debug("no ref {} : save edd success", noReferensi);
					nextTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EDD, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
				} catch (Exception e) {
					log.error("no ref {} : save edd failed : {} ", noReferensi, e.getMessage());
					stopTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_EDD, isNasabahExisting, isNrt,
							isChangeProduct, callLog);
					return;
				}
			}
		}

		// save predin & dir / aprove predin
		if (isInTransaction(transaksi, TransactionStatus.SAVE_PREDIN_AND_DIR)) {
			log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: save predin and dir in progress");
			progressTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_PREDIN_AND_DIR, isNasabahExisting, isNrt, isChangeProduct, callLog);

			try {
				AprovePredinRequest aprovePredinRequest = new AprovePredinRequest();
				aprovePredinRequest.setCustomerNumber(transaksi.getPemrekCisCustomerNumber());
				aprovePredinRequest.setCustomerName(data.getConfirmedEformData().getNamaNasabah());
				aprovePredinRequest.setAccountNumber(transaksi.getPemrekAccountNumber());
				aprovePredinRequest.setReferenceNumber(noReferensi);
				aprovePredinRequest.setSpvId(predinSpvId);
				aprovePredinRequest.setSaveId(YesOrNoConverter.convertFromBooleanString(transaksi.getSpvApproveKtp()));
				aprovePredinRequest
						.setSavePhoto(YesOrNoConverter.convertFromBooleanString(transaksi.getSpvApproveFoto()));
				aprovePredinRequest
						.setSaveSignature(YesOrNoConverter.convertFromBooleanString(transaksi.getSpvApproveTtd()));
				aprovePredinRequest
						.setSaveSupphoto(YesOrNoConverter.convertFromBooleanString(transaksi.getSpvApproveNpwp())); // data
																													// npwp
																													// optional
				predinService.aprovePredin(username, branchCode, aprovePredinRequest);

				log.debug("no ref {} : save predin & dir failed success", noReferensi);
				nextTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_PREDIN_AND_DIR, isNasabahExisting, isNrt,
						isChangeProduct, callLog);
			} catch (Exception e) {
				log.error("no ref {} : save predin & dir failed : {} ", noReferensi, e.getMessage());
				stopTransaction(noReferensi, username, transaksi, TransactionStatus.SAVE_PREDIN_AND_DIR, isNasabahExisting, isNrt,
						isChangeProduct, callLog);
				return;
			}
		}

		// update cobrand, user code & service charge
		if (isInTransaction(transaksi, TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE)) {
			log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: update cobrand in progress");
			progressTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE, isNasabahExisting, isNrt, isChangeProduct, callLog);

			try {
				AccountCustomerUpdateReqeust accountCustomerUpdateReqeust = new AccountCustomerUpdateReqeust();
				List<AccountCustomerUpdateKeyValue> accountCustomerUpdateKeyValues = new ArrayList<>();

				AccountTypeConverter.Type accountType = AccountTypeConverter.convert(
						data.getConfirmedEformData().getJenisRekening(),
						data.getConfirmedEformData().getStatusTahapanGold());
				TipeKartuPasporBcaConverter.Type tipeKartuPassporBca = TipeKartuPasporBcaConverter
						.convert(data.getConfirmedEformData().getTipeKartuPasporBca());
				JenisKartuPasporBcaConverter.Type jenisKartuPassporBca = JenisKartuPasporBcaConverter
						.convert(data.getConfirmedEformData().getJenisKartuPasporBca());

				// cobrand
				String valCobrand = "";
				switch (accountType) {
					case TAHAPAN:
						valCobrand = "0000";
						break;
					case TAHAPAN_XPRESI:
						valCobrand = "0043";
						break;
					case TAHAPAN_GOLD:
						valCobrand = "0000";
						break;
					default:
						break;
				}

				String valServiceChargeCode = Strings.nullToEmpty(UbahServiceChargeCodeConverter.convert(accountType, tipeKartuPassporBca));

				String valUserCode5 = Strings.nullToEmpty(
					UbahUserCodeConverter.convert(accountType, tipeKartuPassporBca, jenisKartuPassporBca));
				

				AccountCustomerUpdateKeyValue cobrand = new AccountCustomerUpdateKeyValue();
				cobrand.setKey("DEPTNO");
				cobrand.setValue(valCobrand);
				accountCustomerUpdateKeyValues.add(cobrand);
				AccountCustomerUpdateKeyValue serviceChargeCode = new AccountCustomerUpdateKeyValue();
				serviceChargeCode.setKey("SCCODE");
				serviceChargeCode.setValue(valServiceChargeCode);
				accountCustomerUpdateKeyValues.add(serviceChargeCode);
				AccountCustomerUpdateKeyValue userCode = new AccountCustomerUpdateKeyValue();
				userCode.setKey("USRCD5");
				userCode.setValue(valUserCode5);
				accountCustomerUpdateKeyValues.add(userCode);

				accountCustomerUpdateReqeust.setUserId(predinUserId);
				accountCustomerUpdateReqeust.setData(accountCustomerUpdateKeyValues);

				customerService.accountCustomerUpdate(transaksi.getPemrekAccountNumber(), accountCustomerUpdateReqeust);

				log.debug("no ref {} : update user code  & service charge success", noReferensi);
				nextTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE, isNasabahExisting, isNrt,
						isChangeProduct, callLog);
			} catch (Exception e) {
				log.error("no ref {} : update user code  & service charge failed : {} ", noReferensi, e.getMessage());
				stopTransaction(noReferensi, username, transaksi, TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE, isNasabahExisting, isNrt,
						isChangeProduct, callLog);
				return;
			}
		}

		// digital card registration
		if (isInTransaction(transaksi, TransactionStatus.DIGITAL_CARD_REGISTRATION)) {
			log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: digital card registration in progress");
			progressTransaction(noReferensi, username, transaksi, TransactionStatus.DIGITAL_CARD_REGISTRATION, isNasabahExisting, isNrt, isChangeProduct, callLog);

			try {

				// String branchName = masterDataBranchService.findOneByBranchCode(branchCode).map(MasterDataBranch::getBranchName).orElse("");
				String branchName = masterDataBranchService.findOneByBranchCode(branchCode).map(MasterDataBranch::getBranchName).orElse("SENTRA LAYANAN DIGITAL");
				
				CardRegistrationRequest cardRegistrationRequest = new CardRegistrationRequest();
				cardRegistrationRequest.setAccountNumber(transaksi.getPemrekAccountNumber());
				cardRegistrationRequest.setUserId(registrationUserId);
				cardRegistrationRequest.setBranchCode(branchCode);
				cardRegistrationRequest.setBranchName(branchName);
				cardRegistrationRequest.setSpvId(registrationSpvId);
				registrationService.createDigitalCard(username, cardRegistrationRequest);
				
				log.debug("digital card registration success");
				nextTransaction(noReferensi, username, transaksi, TransactionStatus.DIGITAL_CARD_REGISTRATION, isNasabahExisting,
						isNrt, isChangeProduct, callLog);
			} catch (Exception e) {
				log.error("NC | no ref {} : digital card registration failed : {} ", noReferensi, e.getMessage());
				stopTransactionNotRetryAble(noReferensi, username, transaksi, TransactionStatus.DIGITAL_CARD_REGISTRATION, isNasabahExisting, isNrt, isChangeProduct, callLog);				
			}
		}
				
		
	}

	

	@Override
	public List<TransactionMessageStatus> buildStatusProgress(String noRef, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct) {
		List<TransactionMessageStatus> status = new ArrayList<>();
		if (!isNasabahExisting && !isNrt && !isChangeProduct) { // first account non nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.SAVE_EFORM).status(Status.NOT_RUNNING).description("Save e-form").build());
			status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.FIRST_ACCOUNT).status(Status.NOT_RUNNING).description("Open First Account").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (!isNasabahExisting && isNrt && !isChangeProduct) { // first account nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.SAVE_EFORM).status(Status.NOT_RUNNING).description("Save e-form").build());
			status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.FIRST_ACCOUNT).status(Status.NOT_RUNNING).description("Open First Account").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.SAVE_EDD).status(Status.NOT_RUNNING).description("Save EDD").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (!isNasabahExisting && !isNrt && isChangeProduct) { // first account change product non nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.GENERATE_EFORM).status(Status.NOT_RUNNING).description("Generate e-form").build());
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE).status(Status.NOT_RUNNING).description("Update no-reff to m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID).status(Status.NOT_RUNNING).description("Update no-reff to bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.UPDATE_NO_REF_TO_PREDIN).status(Status.NOT_RUNNING).description("Update no-reff to pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.FIRST_ACCOUNT).status(Status.NOT_RUNNING).description("Open First Account").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(8).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (!isNasabahExisting && isNrt && isChangeProduct) { // first account change product nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.GENERATE_EFORM).status(Status.NOT_RUNNING).description("Generate e-form").build());
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE).status(Status.NOT_RUNNING).description("Update no-reff to m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID).status(Status.NOT_RUNNING).description("Update no-reff to bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.UPDATE_NO_REF_TO_PREDIN).status(Status.NOT_RUNNING).description("Update no-reff to pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.FIRST_ACCOUNT).status(Status.NOT_RUNNING).description("Open First Account").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.SAVE_EDD).status(Status.NOT_RUNNING).description("Save EDD").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(8).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(9).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		
		if (isNasabahExisting && !isNrt && !isChangeProduct) { // second account non nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.SAVE_EFORM).status(Status.NOT_RUNNING).description("Save e-form").build());
			status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_CIS).status(Status.NOT_RUNNING).description("Update CIS").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.SECOND_ACCOUNT).status(Status.NOT_RUNNING).description("Open Second Account").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (isNasabahExisting && isNrt && !isChangeProduct) { // second account nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.SAVE_EFORM).status(Status.NOT_RUNNING).description("Save e-form").build());
			status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_CIS).status(Status.NOT_RUNNING).description("Update CIS").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.SECOND_ACCOUNT).status(Status.NOT_RUNNING).description("Open Second Account").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.SAVE_EDD).status(Status.NOT_RUNNING).description("Save EDD").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(8).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (isNasabahExisting && !isNrt && isChangeProduct) { // second account change product non nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.GENERATE_EFORM).status(Status.NOT_RUNNING).description("Generate e-form").build());
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE).status(Status.NOT_RUNNING).description("Update no-reff to m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID).status(Status.NOT_RUNNING).description("Update no-reff to bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.UPDATE_NO_REF_TO_PREDIN).status(Status.NOT_RUNNING).description("Update no-reff to pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.UPDATE_CIS).status(Status.NOT_RUNNING).description("Update CIS").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.SECOND_ACCOUNT).status(Status.NOT_RUNNING).description("Open Second Account").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(8).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(9).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		
		if (isNasabahExisting && isNrt && isChangeProduct) { // second account change product nrt
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_MOBILE).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(1).name(TransactionStatus.UPDATE_STATUS_IN_PROGRESS_BCA_CO_ID).status(Status.NOT_RUNNING).description("Kirim notifikasi transaksi ke bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(2).name(TransactionStatus.GENERATE_EFORM).status(Status.NOT_RUNNING).description("Generate e-form").build());
			if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_MOBILE)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_MOBILE).status(Status.NOT_RUNNING).description("Update no-reff to m-Bank").build());
			else if (referenceNumberExtractor.parseToChannel(noRef) == ChannelType.BCA_CO_ID)
				status.add(TransactionMessageStatus.builder().order(3).name(TransactionStatus.UPDATE_NO_REF_TO_CHANNEL_BCA_CO_ID).status(Status.NOT_RUNNING).description("Update no-reff to bca.co.id").build());
			status.add(TransactionMessageStatus.builder().order(4).name(TransactionStatus.UPDATE_NO_REF_TO_PREDIN).status(Status.NOT_RUNNING).description("Update no-reff to pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(5).name(TransactionStatus.UPDATE_CIS).status(Status.NOT_RUNNING).description("Update CIS").build());
			status.add(TransactionMessageStatus.builder().order(6).name(TransactionStatus.SECOND_ACCOUNT).status(Status.NOT_RUNNING).description("Open Second Account").build());
			status.add(TransactionMessageStatus.builder().order(7).name(TransactionStatus.SAVE_EDD).status(Status.NOT_RUNNING).description("Save EDD").build());
			status.add(TransactionMessageStatus.builder().order(8).name(TransactionStatus.SAVE_PREDIN_AND_DIR).status(Status.NOT_RUNNING).description("Approve Pre-DIN").build());
			status.add(TransactionMessageStatus.builder().order(9).name(TransactionStatus.UPDATE_COBRAND_SERVICE_CHARGE_AND_USER_CODE).status(Status.NOT_RUNNING).description("Ubah CoBrand, Service Charge & Usercode").build());
			status.add(TransactionMessageStatus.builder().order(10).name(TransactionStatus.DIGITAL_CARD_REGISTRATION).status(Status.NOT_RUNNING).description("Catat Instant Card").build());
			return status;
		}
		return status;
	}
	
	private CustomerStatus getCustomerStatus(String username, String branchCode, String noReferensi, Transaksi transaksi, CompletData data) {
		try
		{
			return customerService.getCustomerStatus(username, branchCode, data.getConfirmedEformData().getNik(), data.getConfirmedEformData().getNamaNasabah(), data.getConfirmedEformData().getTanggalLahir(), data.getConfirmedEformData().getNegara(), data.getConfirmedEformData().getTransientPekerjaan(), data.getConfirmedEformData().getPekerjaanTier2(), data.getConfirmedEformData().getPekerjaanTier3(), null, null);
		} catch (Exception e)
		{
			log.error("no ref {} : get customer status failed : {}", noReferensi, e.getMessage());
			e.printStackTrace();
			throw new CommonResourceNotFoundException("failed get customer status");
		}
	}

	
	/**
	 * save eform
	 * @param noRef
	 * @return
	 */
	private EformSaveResponse saveEform(boolean generatenewEform, String noRef, String username, Transaksi transaksi, CompletData data) {
//		Account account = userService.findByUsername(username);
//		
//		String jsonEform = new String(transaksi.getRawEform());
//		
//		OutputSchema eform = null;
//		OutputEktp ektp = null;
//		
//		try
//		{
//			
//			eform = objectMapper.readValue(jsonEform, OutputSchema.class);
//			String nik = eform.getFormData().getNik();
//			ektp = ektpService.getEktpByNik(nik, account.getBranchCode());
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
		
		EformSaveResponse response = null;
		EformSaveRequest req = new EformSaveRequest();
		EformSaveDataRequest reqData = new EformSaveDataRequest();
		
//		EformFormData formData = modelMapper.map(data, EformFormData.class);
		
//		Map confirmedData = objectMapper.convertValue(data, Map.class);
//		Map dataEform = objectMapper.convertValue(data.getEform().getFormData(), Map.class);
//
//		confirmedData.values().removeIf(Objects::isNull);
//		BeanUtils.copyProperties(data, dataEform);
//		
//		EformFormData formData = objectMapper.convertValue(dataEform, EformFormData.class);
		
		
		
		
//		EformFormData formData = new EformFormData();		
//		
//		formData.setNik(Stream.of(transaksi.getNik(), eform.getFormData().getNik()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaNasabah(Stream.of(transaksi.getNamaNasabah(), ektp.getOutputSchema().getPersonalInformation().getFullName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTempatLahir(Stream.of(transaksi.getTempatLahir(), ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthPlace()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalLahir(Stream.of(transaksi.getTanggalLahir(),ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthDate()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisKelamin(Stream.of(transaksi.getJenisKelamin(), DukcapilGenderConverter.convert(ektp.getOutputSchema().getPersonalInformation().getSex())).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGadisIbuKandung(Stream.of(transaksi.getNamaGadisIbuKandung(), eform.getFormData().getNamaGadisIbuKandung()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setStatusPerkawinan(Stream.of(transaksi.getStatusPerkawinan(), eform.getFormData().getStatusPerkawinan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAgama(Stream.of(transaksi.getAgama(), eform.getFormData().getAgama()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalan(Stream.of(transaksi.getNamaJalan() ,ektp.getOutputSchema().getAddress().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedung(Stream.of(transaksi.getNamaGedung(), eform.getFormData().getNamaGedung()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePos(Stream.of(transaksi.getKodePos(), ektp.getOutputSchema().getAddress().getPostCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupaten(Stream.of(transaksi.getKabupaten(), ektp.getOutputSchema().getAddress().getRegencyName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsi(Stream.of(transaksi.getProvinsi(), ektp.getOutputSchema().getAddress().getProvinceName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatan(Stream.of(transaksi.getKecamatan(), ektp.getOutputSchema().getAddress().getSubdistrictName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahan(Stream.of(transaksi.getKelurahan(), ektp.getOutputSchema().getAddress().getVillageName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRw(Stream.of(transaksi.getRtRw(), eform.getFormData().getRtRw()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamat(Stream.of(transaksi.getNegaraAlamat(), eform.getFormData().getNegaraAlamat()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponRumah(Stream.of(transaksi.getKodeNegaraTeleponRumah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponRumah(Stream.of(transaksi.getKodeAreaTeleponRumah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponRumah(Stream.of(transaksi.getNomorTeleponRumah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraNomorHp(Stream.of(transaksi.getKodeNegaraNomorHp(), eform.getFormData().getKodeNegaraNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorHp(Stream.of(transaksi.getNomorHp(), eform.getFormData().getNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setEmail(Stream.of(transaksi.getEmail(), eform.getFormData().getEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalanRekeningKoran(Stream.of(transaksi.getNamaJalanRekeningKoran() ,eform.getFormData().getNamaJalanRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedungRekeningKoran(Stream.of(transaksi.getNamaGedungRekeningKoran(), eform.getFormData().getNamaGedungRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosRekeningKoran(Stream.of(transaksi.getKodePosRekeningKoran(), eform.getFormData().getKodePosRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatanRekeningKoran(Stream.of(transaksi.getKecamatanRekeningKoran(),eform.getFormData().getKecamatanRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahanRekeningKoran(Stream.of(transaksi.getKelurahanRekeningKoran(),eform.getFormData().getKelurahanRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRwRekeningKoran(Stream.of(transaksi.getRtRwRekeningKoran(),eform.getFormData().getRtRwRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupatenRekeningKoran(Stream.of(transaksi.getKabupatenRekeningKoran(),eform.getFormData().getKabupatenRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsiRekeningKoran(Stream.of(transaksi.getProvinsiRekeningKoran(),eform.getFormData().getProvinsiRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamatRekeningKoran(Stream.of(transaksi.getNegaraAlamatRekeningKoran(),eform.getFormData().getNegaraAlamatRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaan(Stream.of(transaksi.getPekerjaan(), eform.getFormData().getPekerjaan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPekerjaanTier2(Stream.of(eform.getFormData().getPekerjaanTier2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaanTier3(Stream.of(eform.getFormData().getPekerjaanTier3()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBidangUsaha(Stream.of(transaksi.getBidangUsaha(), eform.getFormData().getBidangUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJabatan(Stream.of(transaksi.getJabatan(), eform.getFormData().getJabatan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setSumberPenghasilan(Stream.of(eform.getFormData().getSumberPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberPenghasilanLainnya(Stream.of(eform.getFormData().getSumberPenghasilanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setTotalPenghasilan(Stream.of(transaksi.getTotalPenghasilan(), eform.getFormData().getTotalPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaTempatBekerja(Stream.of(transaksi.getNamaTempatBekerja(), eform.getFormData().getNamaTempatBekerja()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor1(Stream.of(transaksi.getAlamatKantor1(), eform.getFormData().getAlamatKantor1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor2(Stream.of(transaksi.getAlamatKantor2(), eform.getFormData().getAlamatKantor2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor3(Stream.of(transaksi.getAlamatKantor3(), eform.getFormData().getAlamatKantor3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKotaKantor(Stream.of(transaksi.getKotaKantor(), eform.getFormData().getKotaKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosKantor(Stream.of(transaksi.getKodePosKantor(), eform.getFormData().getKodePosKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponKantor(Stream.of(transaksi.getKodeNegaraTeleponKantor(),eform.getFormData().getKodeNegaraTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponKantor(Stream.of(transaksi.getKodeAreaTeleponKantor(), eform.getFormData().getKodeAreaTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponKantor(Stream.of(transaksi.getNomorTeleponKantor(), eform.getFormData().getNomorTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNpwp(Stream.of(transaksi.getNpwp(), eform.getFormData().getNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorNpwp(Stream.of(transaksi.getNomorNpwp(), eform.getFormData().getNomorNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTujuanPembukaanRekening(Stream.of(transaksi.getTujuanPembukaanRekening(), eform.getFormData().getTujuanPembukaanRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisRekening(Stream.of(transaksi.getJenisRekening() , eform.getFormData().getJenisRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTipeKartuPasporBca(Stream.of(transaksi.getTipeKartuPasporBca(), eform.getFormData().getTipeKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setJenisKartuPasporBca(Stream.of(transaksi.getJenisKartuPasporBca(), eform.getFormData().getJenisKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setCabang(Stream.of(transaksi.getCabang(), eform.getFormData().getCabang()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBahasaPetunjukLayarAtm(Stream.of(eform.getFormData().getBahasaPetunjukLayarAtm()).filter(Objects::nonNull).findFirst().orElse(""));		
//		
//		formData.setFormPerpajakanFatca(Stream.of(transaksi.getFormPerpajakanFatca(), eform.getFormData().getFormPerpajakanFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setFormPerpajakanCrs(Stream.of(transaksi.getFormPerpajakanCrs(), eform.getFormData().getFormPerpajakanCrs()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraLahirFatca(Stream.of(transaksi.getNegaraLahirFatca(), eform.getFormData().getNegaraLahirFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegara(Stream.of(transaksi.getNegara(), eform.getFormData().getNegara()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setWajibFatca(Stream.of(transaksi.getWajibFatca(), eform.getFormData().getWajibFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setWajibPajakNegaraLain(Stream.of(eform.getFormData().getWajibPajakNegaraLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPersetujuanDataPihakKetiga(Stream.of(transaksi.getPersetujuanDataPihakKetiga(), eform.getFormData().getPersetujuanDataPihakKetiga()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanSms(Stream.of(transaksi.getPersetujuanSms(), eform.getFormData().getPersetujuanSms()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanEmail(Stream.of(transaksi.getPersetujuanEmail(), eform.getFormData().getPersetujuanEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanTelepon(Stream.of(transaksi.getPersetujuanTelepon(), eform.getFormData().getPersetujuanTelepon()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlasanResikoTinggi(Stream.of(transaksi.getAlasanResikoTinggi(), eform.getFormData().getAlasanResikoTinggi()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTinggalAlamatTerakhirSejak(Stream.of(transaksi.getTinggalAlamatTerakhirSejak(), eform.getFormData().getTinggalAlamatTerakhirSejak()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNasabahBankLain(Stream.of(transaksi.getNasabahBankLain(), eform.getFormData().getNasabahBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaBankLain(Stream.of(transaksi.getNamaBankLain(), eform.getFormData().getNamaBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalBergabungBankLain(Stream.of(transaksi.getTanggalBergabungBankLain(), eform.getFormData().getTanggalBergabungBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setHubunganUsahaLuarNegeri(Stream.of(transaksi.getHubunganUsahaLuarNegeri(), eform.getFormData().getHubunganUsahaLuarNegeri()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha1(Stream.of(transaksi.getNegaraBerhubunganUsaha1(), eform.getFormData().getNegaraBerhubunganUsaha1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha2(Stream.of(transaksi.getNegaraBerhubunganUsaha2(), eform.getFormData().getNegaraBerhubunganUsaha2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha3(Stream.of(transaksi.getNegaraBerhubunganUsaha3(), eform.getFormData().getNegaraBerhubunganUsaha3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanWarisan(Stream.of(transaksi.getSumberKekayaanWarisan(), eform.getFormData().getSumberKekayaanWarisan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanTabungan(Stream.of(transaksi.getSumberKekayaanTabungan(), eform.getFormData().getSumberKekayaanTabungan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHasilUsaha(Stream.of(transaksi.getSumberKekayaanHasilUsaha(), eform.getFormData().getSumberKekayaanHasilUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHibah(Stream.of(transaksi.getSumberKekayaanHibah(), eform.getFormData().getSumberKekayaanHibah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanGaji(Stream.of(transaksi.getSumberKekayaanGaji(), eform.getFormData().getSumberKekayaanGaji()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnya(Stream.of(transaksi.getSumberKekayaanLainnya(), eform.getFormData().getSumberKekayaanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnyaText(Stream.of(transaksi.getSumberKekayaanLainnyaText(), eform.getFormData().getSumberKekayaanLainnyaText()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setInformasiLainnya(Stream.of(transaksi.getInformasiLainnya(), eform.getFormData().getInformasiLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
		
// todo : jika produk berubah, refNum tidak diisi, (generate eform baru)
// update hasil noref ke channel
		AccountTypeConverter.Type accountType = AccountTypeConverter.convert(data.getConfirmedEformData().getJenisRekening(), data.getConfirmedEformData().getStatusTahapanGold()); 
    String accType = "";
		switch(accountType)
		{
			case TAHAPAN:
				accType = "TAHAPAN";
				break;
			case TAHAPAN_XPRESI:
				accType =  "XPRESI";
				break;
			case TAHAPAN_GOLD:
				accType = "TAHAPAN";
				break;
		}
		String transCode = masterDataTransactionCodeService.findCode(accType).getCode();
    reqData.setFormData(data.getConfirmedEformData());
		reqData.setRefNum(!generatenewEform ? noRef : "");
		reqData.setChannelId(referenceNumberExtractor.parseToChannelId(noRef));
    // reqData.setTransId(TransIdConverter.convert(accountType));
    reqData.setTransId(transCode);//added by yeos
		reqData.setTemplateVersion("");
		reqData.setStatus(data.getEform().getStatus());
		reqData.setCreatedBy(username);
		req.setFormDataList(Arrays.asList(reqData));
		
		response = eformService.saveEform(username, req);
		return response;
	}

//	private EformSaveResponse saveEformSecondAccount(String noRef, String username, TransaksiBaru transaksi, CompletData data) {
//		Account account = userService.findByUsername(username);
//		
//		String jsonEform = new String(transaksi.getRawEform());
//		String jsonCis = new String(transaksi.getRawCis());
//				
//		OutputSchema eform = null;
//		OutputSchemaSearch cis = null;
//		OutputEktp ektp = null;
//		
//		try
//		{
//			
//			eform = objectMapper.readValue(jsonEform, OutputSchema.class);
//			cis = objectMapper.readValue(jsonCis, OutputSchemaSearch.class);
//			String nik = transaksi.getNik() != null ? transaksi.getNik() : eform.getFormData().getNik();
//			ektp = ektpService.getEktpByNik(nik, account.getBranchCode());
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		
//		OutputSchemaSearchItem cisItem = null;
//		List<String> cisType = Arrays.asList("A", "B");
//		if (cisType.contains(transaksi.getCisType())) {
//			cisItem = cis.getCisIndividu().stream().filter(c -> c.getCisCustomerNumber() == transaksi.getCisCustomerNumber()).findFirst().orElse(null);
//		}
//		
//		EformSaveResponse response = null;
//		EformSaveRequest req = new EformSaveRequest();
//		EformSaveDataRequest reqData = new EformSaveDataRequest();
////		EformFormData formData = modelMapper.map(data, EformFormData.class);
//		EformFormData formData = new EformFormData();		
//		
//		formData.setNik(Stream.of(transaksi.getNik(), eform.getFormData().getNik()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaNasabah(Stream.of(transaksi.getNamaNasabah(), ektp.getOutputSchema().getPersonalInformation().getFullName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTempatLahir(Stream.of(transaksi.getTempatLahir(), ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthPlace()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalLahir(Stream.of(transaksi.getTanggalLahir(),ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthDate()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisKelamin(Stream.of(transaksi.getJenisKelamin(), DukcapilGenderConverter.convert(ektp.getOutputSchema().getPersonalInformation().getSex())).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGadisIbuKandung(Stream.of(transaksi.getNamaGadisIbuKandung(), cisItem.getCustomerDemographicInformation().getMothersName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setStatusPerkawinan(Stream.of(transaksi.getStatusPerkawinan(),  cisItem.getCustomerDemographicInformation().getMaritalStatus()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAgama(Stream.of(transaksi.getAgama(), cisItem.getCustomerMasterData().getReligion()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalan(Stream.of(transaksi.getNamaJalan(), ektp.getOutputSchema().getAddress().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedung(Stream.of(transaksi.getNamaGedung(), eform.getFormData().getNamaGedung()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePos(Stream.of(transaksi.getKodePos(), ektp.getOutputSchema().getAddress().getPostCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupaten(Stream.of(transaksi.getKabupaten(), ektp.getOutputSchema().getAddress().getRegencyName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsi(Stream.of(transaksi.getProvinsi(), ektp.getOutputSchema().getAddress().getProvinceName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatan(Stream.of(transaksi.getKecamatan(), ektp.getOutputSchema().getAddress().getSubdistrictName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahan(Stream.of(transaksi.getKelurahan(), ektp.getOutputSchema().getAddress().getVillageName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRw(Stream.of(transaksi.getRtRw(), ektp.getOutputSchema().getAddress().getRt()+"/"+ektp.getOutputSchema().getAddress().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamat(Stream.of(transaksi.getNegaraAlamat(), cisItem.getCustomerAddress().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponRumah(Stream.of(transaksi.getKodeNegaraTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponRumah(Stream.of(transaksi.getKodeAreaTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getAreaCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponRumah(Stream.of(transaksi.getNomorTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getPhoneNumber()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraNomorHp(Stream.of(transaksi.getKodeNegaraNomorHp(), eform.getFormData().getKodeNegaraNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorHp(Stream.of(transaksi.getNomorHp(), eform.getFormData().getNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setEmail(Stream.of(transaksi.getEmail(), eform.getFormData().getEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalanRekeningKoran(Stream.of(transaksi.getNamaJalanRekeningKoran() ,cisItem.getCustomerComplementData().getContactInformation().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedungRekeningKoran(Stream.of(transaksi.getNamaGedungRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getBuilding()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosRekeningKoran(Stream.of(transaksi.getKodePosRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getZipCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatanRekeningKoran(Stream.of(transaksi.getKecamatanRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahanRekeningKoran(Stream.of(transaksi.getKelurahanRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getSubDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRwRekeningKoran(Stream.of(transaksi.getRtRwRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getRt()+"/"+cisItem.getCustomerComplementData().getContactInformation().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupatenRekeningKoran(Stream.of(transaksi.getKabupatenRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCity()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsiRekeningKoran(Stream.of(transaksi.getProvinsiRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getProvince()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamatRekeningKoran(Stream.of(transaksi.getNegaraAlamatRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaan(Stream.of(transaksi.getPekerjaan(), eform.getFormData().getPekerjaan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPekerjaanTier2(Stream.of(eform.getFormData().getPekerjaanTier2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaanTier3(Stream.of(eform.getFormData().getPekerjaanTier3()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBidangUsaha(Stream.of(transaksi.getBidangUsaha(), eform.getFormData().getBidangUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJabatan(Stream.of(transaksi.getJabatan(), eform.getFormData().getJabatan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setSumberPenghasilan(Stream.of(eform.getFormData().getSumberPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberPenghasilanLainnya(Stream.of(eform.getFormData().getSumberPenghasilanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setTotalPenghasilan(Stream.of(transaksi.getTotalPenghasilan(), eform.getFormData().getTotalPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaTempatBekerja(Stream.of(transaksi.getNamaTempatBekerja(), eform.getFormData().getNamaTempatBekerja()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor1(Stream.of(transaksi.getAlamatKantor1(), eform.getFormData().getAlamatKantor1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor2(Stream.of(transaksi.getAlamatKantor2(), eform.getFormData().getAlamatKantor2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor3(Stream.of(transaksi.getAlamatKantor3(), eform.getFormData().getAlamatKantor3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKotaKantor(Stream.of(transaksi.getKotaKantor(), eform.getFormData().getKotaKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosKantor(Stream.of(transaksi.getKodePosKantor(), eform.getFormData().getKodePosKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponKantor(Stream.of(transaksi.getKodeNegaraTeleponKantor(),eform.getFormData().getKodeNegaraTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponKantor(Stream.of(transaksi.getKodeAreaTeleponKantor(), eform.getFormData().getKodeAreaTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponKantor(Stream.of(transaksi.getNomorTeleponKantor(), eform.getFormData().getNomorTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNpwp(Stream.of(transaksi.getNpwp(), eform.getFormData().getNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorNpwp(Stream.of(transaksi.getNomorNpwp(), eform.getFormData().getNomorNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTujuanPembukaanRekening(Stream.of(transaksi.getTujuanPembukaanRekening(), eform.getFormData().getTujuanPembukaanRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisRekening(Stream.of(transaksi.getJenisRekening() , eform.getFormData().getJenisRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTipeKartuPasporBca(Stream.of(transaksi.getTipeKartuPasporBca(), eform.getFormData().getTipeKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setJenisKartuPasporBca(Stream.of(transaksi.getJenisKartuPasporBca(), eform.getFormData().getJenisKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setCabang(Stream.of(transaksi.getCabang(), eform.getFormData().getCabang()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBahasaPetunjukLayarAtm(Stream.of(eform.getFormData().getBahasaPetunjukLayarAtm()).filter(Objects::nonNull).findFirst().orElse(""));		
//		
//		formData.setFormPerpajakanFatca(Stream.of(transaksi.getFormPerpajakanFatca(), eform.getFormData().getFormPerpajakanFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setFormPerpajakanCrs(Stream.of(transaksi.getFormPerpajakanCrs(), eform.getFormData().getFormPerpajakanCrs()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraLahirFatca(Stream.of(transaksi.getNegaraLahirFatca(), eform.getFormData().getNegaraLahirFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegara(Stream.of(transaksi.getNegara(), eform.getFormData().getNegara()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setWajibFatca(Stream.of(transaksi.getWajibFatca(), eform.getFormData().getWajibFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setWajibPajakNegaraLain(Stream.of(eform.getFormData().getWajibPajakNegaraLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPersetujuanDataPihakKetiga(Stream.of(transaksi.getPersetujuanDataPihakKetiga(), eform.getFormData().getPersetujuanDataPihakKetiga()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanSms(Stream.of(transaksi.getPersetujuanSms(), eform.getFormData().getPersetujuanSms()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanEmail(Stream.of(transaksi.getPersetujuanEmail(), eform.getFormData().getPersetujuanEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanTelepon(Stream.of(transaksi.getPersetujuanTelepon(), eform.getFormData().getPersetujuanTelepon()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlasanResikoTinggi(Stream.of(transaksi.getAlasanResikoTinggi(), eform.getFormData().getAlasanResikoTinggi()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTinggalAlamatTerakhirSejak(Stream.of(transaksi.getTinggalAlamatTerakhirSejak(), eform.getFormData().getTinggalAlamatTerakhirSejak()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNasabahBankLain(Stream.of(transaksi.getNasabahBankLain(), eform.getFormData().getNasabahBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaBankLain(Stream.of(transaksi.getNamaBankLain(), eform.getFormData().getNamaBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalBergabungBankLain(Stream.of(transaksi.getTanggalBergabungBankLain(), eform.getFormData().getTanggalBergabungBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setHubunganUsahaLuarNegeri(Stream.of(transaksi.getHubunganUsahaLuarNegeri(), eform.getFormData().getHubunganUsahaLuarNegeri()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha1(Stream.of(transaksi.getNegaraBerhubunganUsaha1(), eform.getFormData().getNegaraBerhubunganUsaha1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha2(Stream.of(transaksi.getNegaraBerhubunganUsaha2(), eform.getFormData().getNegaraBerhubunganUsaha2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha3(Stream.of(transaksi.getNegaraBerhubunganUsaha3(), eform.getFormData().getNegaraBerhubunganUsaha3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanWarisan(Stream.of(transaksi.getSumberKekayaanWarisan(), eform.getFormData().getSumberKekayaanWarisan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanTabungan(Stream.of(transaksi.getSumberKekayaanTabungan(), eform.getFormData().getSumberKekayaanTabungan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHasilUsaha(Stream.of(transaksi.getSumberKekayaanHasilUsaha(), eform.getFormData().getSumberKekayaanHasilUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHibah(Stream.of(transaksi.getSumberKekayaanHibah(), eform.getFormData().getSumberKekayaanHibah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanGaji(Stream.of(transaksi.getSumberKekayaanGaji(), eform.getFormData().getSumberKekayaanGaji()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnya(Stream.of(transaksi.getSumberKekayaanLainnya(), eform.getFormData().getSumberKekayaanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnyaText(Stream.of(transaksi.getSumberKekayaanLainnyaText(), eform.getFormData().getSumberKekayaanLainnyaText()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setInformasiLainnya(Stream.of(transaksi.getInformasiLainnya(), eform.getFormData().getInformasiLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		AccountTypeConverter.Type accountType = AccountTypeConverter.convert(data.getJenisRekening(), data.getStatusTahapanGold()); 
//		reqData.setFormData(formData);
//		reqData.setRefNum(data.getRefNum());
//		reqData.setChannelId("M");
//		reqData.setTransId(TransIdConverter.convert(accountType));
//		reqData.setTemplateVersion("");
//		reqData.setStatus(data.getStatus());
//		reqData.setCreatedBy(username);
//		req.setFormDataList(Arrays.asList(reqData));
//		
//		response = eformService.saveEform(req);
//		return response;
//	}
//	
//	private EformSaveResponse saveEformNewExisting(String noRef, String username, TransaksiBaru transaksi, CompletData data) {
//		Account account = userService.findByUsername(username);
//		
//		String jsonEform = new String(transaksi.getRawEform());
//		String jsonCis = new String(transaksi.getRawCis());
//				
//		OutputSchema eform = null;
//		OutputSchemaSearch cis = null;
//		OutputEktp ektp = null;
//		
//		try
//		{
//			
//			eform = objectMapper.readValue(jsonEform, OutputSchema.class);
//			cis = objectMapper.readValue(jsonCis, OutputSchemaSearch.class);
//			String nik = transaksi.getNik() != null ? transaksi.getNik() : eform.getFormData().getNik();
//			ektp = ektpService.getEktpByNik(nik, account.getBranchCode());
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		
//		OutputSchemaSearchItem cisItem = null;
//		List<String> cisType = Arrays.asList("A", "B");
//		if (cisType.contains(transaksi.getCisType())) {
//			cisItem = cis.getCisIndividu().stream().filter(c -> c.getCisCustomerNumber() == transaksi.getCisCustomerNumber()).findFirst().orElse(null);
//		}
//		
//		EformSaveResponse response = null;
//		EformSaveRequest req = new EformSaveRequest();
//		EformSaveDataRequest reqData = new EformSaveDataRequest();
////		EformFormData formData = modelMapper.map(data, EformFormData.class);
//		EformFormData formData = new EformFormData();		
//		
//		formData.setNik(Stream.of(transaksi.getNik(), eform.getFormData().getNik()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaNasabah(Stream.of(transaksi.getNamaNasabah(), ektp.getOutputSchema().getPersonalInformation().getFullName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTempatLahir(Stream.of(transaksi.getTempatLahir(), ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthPlace()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalLahir(Stream.of(transaksi.getTanggalLahir(),ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthDate()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisKelamin(Stream.of(transaksi.getJenisKelamin(), DukcapilGenderConverter.convert(ektp.getOutputSchema().getPersonalInformation().getSex())).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGadisIbuKandung(Stream.of(transaksi.getNamaGadisIbuKandung(), cisItem.getCustomerDemographicInformation().getMothersName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setStatusPerkawinan(Stream.of(transaksi.getStatusPerkawinan(),  cisItem.getCustomerDemographicInformation().getMaritalStatus()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAgama(Stream.of(transaksi.getAgama(), cisItem.getCustomerMasterData().getReligion()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalan(Stream.of(transaksi.getNamaJalan(), ektp.getOutputSchema().getAddress().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedung(Stream.of(transaksi.getNamaGedung(), eform.getFormData().getNamaGedung()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePos(Stream.of(transaksi.getKodePos(), ektp.getOutputSchema().getAddress().getPostCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupaten(Stream.of(transaksi.getKabupaten(), ektp.getOutputSchema().getAddress().getRegencyName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsi(Stream.of(transaksi.getProvinsi(), ektp.getOutputSchema().getAddress().getProvinceName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatan(Stream.of(transaksi.getKecamatan(), ektp.getOutputSchema().getAddress().getSubdistrictName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahan(Stream.of(transaksi.getKelurahan(), ektp.getOutputSchema().getAddress().getVillageName()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRw(Stream.of(transaksi.getRtRw(), ektp.getOutputSchema().getAddress().getRt()+"/"+ektp.getOutputSchema().getAddress().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamat(Stream.of(transaksi.getNegaraAlamat(), cisItem.getCustomerAddress().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponRumah(Stream.of(transaksi.getKodeNegaraTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponRumah(Stream.of(transaksi.getKodeAreaTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getAreaCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponRumah(Stream.of(transaksi.getNomorTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getPhoneNumber()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraNomorHp(Stream.of(transaksi.getKodeNegaraNomorHp(), eform.getFormData().getKodeNegaraNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorHp(Stream.of(transaksi.getNomorHp(), eform.getFormData().getNomorHp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setEmail(Stream.of(transaksi.getEmail(), eform.getFormData().getEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaJalanRekeningKoran(Stream.of(transaksi.getNamaJalanRekeningKoran() ,cisItem.getCustomerComplementData().getContactInformation().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaGedungRekeningKoran(Stream.of(transaksi.getNamaGedungRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getBuilding()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosRekeningKoran(Stream.of(transaksi.getKodePosRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getZipCode()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKecamatanRekeningKoran(Stream.of(transaksi.getKecamatanRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKelurahanRekeningKoran(Stream.of(transaksi.getKelurahanRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getSubDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setRtRwRekeningKoran(Stream.of(transaksi.getRtRwRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getRt()+"/"+cisItem.getCustomerComplementData().getContactInformation().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKabupatenRekeningKoran(Stream.of(transaksi.getKabupatenRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCity()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setProvinsiRekeningKoran(Stream.of(transaksi.getProvinsiRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getProvince()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraAlamatRekeningKoran(Stream.of(transaksi.getNegaraAlamatRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaan(Stream.of(transaksi.getPekerjaan(), eform.getFormData().getPekerjaan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPekerjaanTier2(Stream.of(eform.getFormData().getPekerjaanTier2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPekerjaanTier3(Stream.of(eform.getFormData().getPekerjaanTier3()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBidangUsaha(Stream.of(transaksi.getBidangUsaha(), eform.getFormData().getBidangUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJabatan(Stream.of(transaksi.getJabatan(), eform.getFormData().getJabatan()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setSumberPenghasilan(Stream.of(eform.getFormData().getSumberPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberPenghasilanLainnya(Stream.of(eform.getFormData().getSumberPenghasilanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setTotalPenghasilan(Stream.of(transaksi.getTotalPenghasilan(), eform.getFormData().getTotalPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaTempatBekerja(Stream.of(transaksi.getNamaTempatBekerja(), eform.getFormData().getNamaTempatBekerja()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor1(Stream.of(transaksi.getAlamatKantor1(), eform.getFormData().getAlamatKantor1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor2(Stream.of(transaksi.getAlamatKantor2(), eform.getFormData().getAlamatKantor2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlamatKantor3(Stream.of(transaksi.getAlamatKantor3(), eform.getFormData().getAlamatKantor3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKotaKantor(Stream.of(transaksi.getKotaKantor(), eform.getFormData().getKotaKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodePosKantor(Stream.of(transaksi.getKodePosKantor(), eform.getFormData().getKodePosKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeNegaraTeleponKantor(Stream.of(transaksi.getKodeNegaraTeleponKantor(),eform.getFormData().getKodeNegaraTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setKodeAreaTeleponKantor(Stream.of(transaksi.getKodeAreaTeleponKantor(), eform.getFormData().getKodeAreaTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorTeleponKantor(Stream.of(transaksi.getNomorTeleponKantor(), eform.getFormData().getNomorTeleponKantor()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNpwp(Stream.of(transaksi.getNpwp(), eform.getFormData().getNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNomorNpwp(Stream.of(transaksi.getNomorNpwp(), eform.getFormData().getNomorNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTujuanPembukaanRekening(Stream.of(transaksi.getTujuanPembukaanRekening(), eform.getFormData().getTujuanPembukaanRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setJenisRekening(Stream.of(transaksi.getJenisRekening() , eform.getFormData().getJenisRekening()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTipeKartuPasporBca(Stream.of(transaksi.getTipeKartuPasporBca(), eform.getFormData().getTipeKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setJenisKartuPasporBca(Stream.of(transaksi.getJenisKartuPasporBca(), eform.getFormData().getJenisKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setCabang(Stream.of(transaksi.getCabang(), eform.getFormData().getCabang()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setBahasaPetunjukLayarAtm(Stream.of(eform.getFormData().getBahasaPetunjukLayarAtm()).filter(Objects::nonNull).findFirst().orElse(""));		
//		
//		formData.setFormPerpajakanFatca(Stream.of(transaksi.getFormPerpajakanFatca(), eform.getFormData().getFormPerpajakanFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setFormPerpajakanCrs(Stream.of(transaksi.getFormPerpajakanCrs(), eform.getFormData().getFormPerpajakanCrs()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraLahirFatca(Stream.of(transaksi.getNegaraLahirFatca(), eform.getFormData().getNegaraLahirFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegara(Stream.of(transaksi.getNegara(), eform.getFormData().getNegara()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setWajibFatca(Stream.of(transaksi.getWajibFatca(), eform.getFormData().getWajibFatca()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setWajibPajakNegaraLain(Stream.of(eform.getFormData().getWajibPajakNegaraLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		formData.setPersetujuanDataPihakKetiga(Stream.of(transaksi.getPersetujuanDataPihakKetiga(), eform.getFormData().getPersetujuanDataPihakKetiga()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanSms(Stream.of(transaksi.getPersetujuanSms(), eform.getFormData().getPersetujuanSms()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanEmail(Stream.of(transaksi.getPersetujuanEmail(), eform.getFormData().getPersetujuanEmail()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setPersetujuanTelepon(Stream.of(transaksi.getPersetujuanTelepon(), eform.getFormData().getPersetujuanTelepon()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setAlasanResikoTinggi(Stream.of(transaksi.getAlasanResikoTinggi(), eform.getFormData().getAlasanResikoTinggi()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTinggalAlamatTerakhirSejak(Stream.of(transaksi.getTinggalAlamatTerakhirSejak(), eform.getFormData().getTinggalAlamatTerakhirSejak()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNasabahBankLain(Stream.of(transaksi.getNasabahBankLain(), eform.getFormData().getNasabahBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNamaBankLain(Stream.of(transaksi.getNamaBankLain(), eform.getFormData().getNamaBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setTanggalBergabungBankLain(Stream.of(transaksi.getTanggalBergabungBankLain(), eform.getFormData().getTanggalBergabungBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setHubunganUsahaLuarNegeri(Stream.of(transaksi.getHubunganUsahaLuarNegeri(), eform.getFormData().getHubunganUsahaLuarNegeri()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha1(Stream.of(transaksi.getNegaraBerhubunganUsaha1(), eform.getFormData().getNegaraBerhubunganUsaha1()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha2(Stream.of(transaksi.getNegaraBerhubunganUsaha2(), eform.getFormData().getNegaraBerhubunganUsaha2()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setNegaraBerhubunganUsaha3(Stream.of(transaksi.getNegaraBerhubunganUsaha3(), eform.getFormData().getNegaraBerhubunganUsaha3()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanWarisan(Stream.of(transaksi.getSumberKekayaanWarisan(), eform.getFormData().getSumberKekayaanWarisan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanTabungan(Stream.of(transaksi.getSumberKekayaanTabungan(), eform.getFormData().getSumberKekayaanTabungan()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHasilUsaha(Stream.of(transaksi.getSumberKekayaanHasilUsaha(), eform.getFormData().getSumberKekayaanHasilUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanHibah(Stream.of(transaksi.getSumberKekayaanHibah(), eform.getFormData().getSumberKekayaanHibah()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanGaji(Stream.of(transaksi.getSumberKekayaanGaji(), eform.getFormData().getSumberKekayaanGaji()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnya(Stream.of(transaksi.getSumberKekayaanLainnya(), eform.getFormData().getSumberKekayaanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setSumberKekayaanLainnyaText(Stream.of(transaksi.getSumberKekayaanLainnyaText(), eform.getFormData().getSumberKekayaanLainnyaText()).filter(Objects::nonNull).findFirst().orElse(""));
//		formData.setInformasiLainnya(Stream.of(transaksi.getInformasiLainnya(), eform.getFormData().getInformasiLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
//		
//		AccountTypeConverter.Type accountType = AccountTypeConverter.convert(data.getJenisRekening(), data.getStatusTahapanGold()); 
//		reqData.setFormData(formData);
//		reqData.setRefNum(data.getRefNum());
//		reqData.setChannelId("M");
//		reqData.setTransId(TransIdConverter.convert(accountType));
//		reqData.setTemplateVersion("");
//		reqData.setStatus(data.getStatus());
//		reqData.setCreatedBy(username);
//		req.setFormDataList(Arrays.asList(reqData));
//		
//		response = eformService.saveEform(req);
//		return response;
//	}
		
	
	/**
	 * save edd
	 * @param noRef
	 * @param username
	 * @param transaksi
	 * @param data
	 */
	private void saveEdd(String noRef, String username, Transaksi transaksi, CompletData data) {
		log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: save edd in progress");
		EddRequest edd = new EddRequest();
		edd.setUserId(username);
		edd.setReasonEdd(data.getConfirmedEformData().getAlasanResikoTinggi());
		edd.setCurrentAddressFromMonthyear(data.getConfirmedEformData().getTinggalAlamatTerakhirSejak());
		edd.setAdditionalAccountOrCreditcardInstitutionName(data.getConfirmedEformData().getNamaBankLain());
		edd.setAdditionalAccountOrCreditcardFromMonthyear(data.getConfirmedEformData().getTanggalBergabungBankLain());
		edd.setForeignCompanyCountryCode1(data.getConfirmedEformData().getNegaraBerhubunganUsaha1());
		edd.setForeignCompanyCountryCode2(data.getConfirmedEformData().getNegaraBerhubunganUsaha2());
		edd.setForeignCompanyCountryCode3(data.getConfirmedEformData().getNegaraBerhubunganUsaha3());
		edd.setWealthFromHeritance(data.getConfirmedEformData().getSumberKekayaanWarisan());
		edd.setWealthFromSaving(data.getConfirmedEformData().getSumberKekayaanTabungan());
		edd.setWealthFromBusiness(data.getConfirmedEformData().getSumberKekayaanHasilUsaha());
		edd.setWealthFromGift(data.getConfirmedEformData().getSumberKekayaanHibah());
		edd.setWealthFromSalary(data.getConfirmedEformData().getSumberKekayaanGaji());
		edd.setWealthFromOthers(data.getConfirmedEformData().getSumberKekayaanLainnya());
		edd.setWealthFromOthersDecription(data.getConfirmedEformData().getSumberKekayaanLainnyaText());
		edd.setAdditionalInformation(data.getConfirmedEformData().getInformasiLainnya());
		eddService.saveEdd(transaksi.getPemrekCisCustomerNumber(), edd);
	}

	// /**
	//  * update edd
	//  * @param noRef
	//  * @param username
	//  * @param transaksi
	//  * @param data
	//  */
	// private void updateEdd(String noRef, String username, Transaksi transaksi, CompletData data) {
	// 	log.debug("status ::::::::::::::::::::::::::::::::::::::::::::: save edd in progress");
	// 	EddRequest edd = new EddRequest();
	// 	edd.setUserId(username);
	// 	edd.setReasonEdd(data.getConfirmedEformData().getAlasanResikoTinggi());
	// 	edd.setCurrentAddressFromMonthyear(data.getConfirmedEformData().getTinggalAlamatTerakhirSejak());
	// 	edd.setAdditionalAccountOrCreditcardInstitutionName(data.getConfirmedEformData().getNamaBankLain());
	// 	edd.setAdditionalAccountOrCreditcardFromMonthyear(data.getConfirmedEformData().getTanggalBergabungBankLain());
	// 	edd.setForeignCompanyCountryCode1(data.getConfirmedEformData().getNegaraBerhubunganUsaha1());
	// 	edd.setForeignCompanyCountryCode2(data.getConfirmedEformData().getNegaraBerhubunganUsaha2());
	// 	edd.setForeignCompanyCountryCode3(data.getConfirmedEformData().getNegaraBerhubunganUsaha3());
	// 	edd.setWealthFromHeritance(data.getConfirmedEformData().getSumberKekayaanWarisan());
	// 	edd.setWealthFromSaving(data.getConfirmedEformData().getSumberKekayaanTabungan());
	// 	edd.setWealthFromBusiness(data.getConfirmedEformData().getSumberKekayaanHasilUsaha());
	// 	edd.setWealthFromGift(data.getConfirmedEformData().getSumberKekayaanHibah());
	// 	edd.setWealthFromSalary(data.getConfirmedEformData().getSumberKekayaanGaji());
	// 	edd.setWealthFromOthers(data.getConfirmedEformData().getSumberKekayaanLainnya());
	// 	edd.setWealthFromOthersDecription(data.getConfirmedEformData().getSumberKekayaanLainnyaText());
	// 	edd.setAdditionalInformation(data.getConfirmedEformData().getInformasiLainnya());
	// 	eddService.updateEdd(transaksi.getPemrekCisCustomerNumber(), edd);
	// }
	
	
	/**
	 * get full data 
	 * @param transaksi
	 * @return
	 */
	private CompletData getData(Transaksi transaksi, String username) {
		
		String jsonEform = new String(transaksi.getRawEform());
		String jsonCis = new String(transaksi.getRawCis());
		String confirmedData = new String(transaksi.getRawKonfirmasiData());
		
		log.debug("json eform raw : {} ", jsonEform);
		log.debug("json eform cis : {} ", jsonCis);
		
		OutputSchema eform = null;
		OutputSchemaSearch cis = null;
//		OutputEktp ektp = null;
		CompletData complete = null;
		EformFormData eformData = null;
		
		try
		{
			complete = objectMapper.readValue(confirmedData, CompletData.class);
			eform = objectMapper.readValue(jsonEform, OutputSchema.class);
			cis = objectMapper.readValue(jsonCis, OutputSchemaSearch.class);
//			String nik = transaksi.getNik() != null ? transaksi.getNik() : eform.getFormData().getNik();
//			ektp = ektpService.getEktpByNik(nik, account.getBranchCode());
			
//			EformFormData completeDate = objectMapper.readValue(confirmedData, EformFormData.class);



// ###############################################
// fix, mising data wehen request eform from expresi

			if (eform.getFormData().getDenganBuku() == null )
				eform.getFormData().setDenganBuku("N");
			if (eform.getFormData().getStatusTahapanGold() == null)
				eform.getFormData().setStatusTahapanGold("N");
			if (eform.getFormData().getTipeKartuPasporBca() == null)
				eform.getFormData().setTipeKartuPasporBca("1");

// ###############################################



			
			System.out.println("================ confirmed data "+confirmedData);
			
			Map confirmedDataMap = objectMapper.readValue(confirmedData, Map.class);
			Map dataEform = objectMapper.convertValue(eform.getFormData(), Map.class);
			
			System.out.println("================ confirmed data eform "+dataEform);
			System.out.println("================ confirmed map : "+confirmedDataMap);
			
			
			Map confirmedDataMapFormated = new HashMap<>();
			// filter null value
			confirmedDataMap.values().removeIf(Objects::isNull);
			// format camel case key to upper snake case
			confirmedDataMap.keySet().stream().forEach(key -> {
				String val = ((String)key);
				StringBuilder result = new StringBuilder(); 
				for(int x=0; x < val.length(); x++) {
					char strIndex = val.charAt(x);
					if (Character.isUpperCase(strIndex))
						result.append("_"+strIndex);
					else if (Character.isDigit(strIndex))
						result.append("_"+strIndex);
					else
						result.append(strIndex);
				}
				String resultString = result.toString().toUpperCase();
				confirmedDataMapFormated.put(resultString, confirmedDataMap.get(key));
				System.out.println("==== key name : "+resultString);
			});
			
			System.out.println("================ confirmed map after convert to upper : "+confirmedDataMapFormated);
			
			// update data
			dataEform.putAll(confirmedDataMapFormated);
			
			System.out.println("================ confirmed data eform after merging "+dataEform);
			
			eformData = objectMapper.convertValue(dataEform, EformFormData.class);
			
			
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		complete.setData(transaksi, eform, eformData, cis);
		
		return complete;
	}
	
	
	private boolean isInTransaction(Transaksi transaksi, TransactionStatus transactionStatus) {
		log.debug("isInTransaction | current transaction status {} ", transaksi.getTransactionStatus());
		if (transaksi.getIsTransactionStepFailed() == null || !transaksi.getIsTransactionStepFailed()) 
			return true;
		return (transaksi.getIsTransactionStepFailed() && transaksi.getTransactionStatus() == transactionStatus) ? true : false; 
	}

	private void progressTransaction(String noRef, String username, Transaksi transaksi, TransactionStatus transactionStatus, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct, CallLog callLog) {
		callLog.setTransaksiStatus("PENDING");
		callLogService.save(callLog);

		List<TransactionMessageStatus> stepMap = updateTransactionStepStatus(noRef, transaksi, isNasabahExisting, isNrt, isChangeProduct, transactionStatus, Status.ON_PROGRESS);
		socket.emit(SOCKET_TRANSACTION, socketMessageToString(new TransactionMessage(username, stepMap, Status.ON_PROGRESS)));
		log.debug("progressTransaction | current transaction status {} ", transaksi.getTransactionStatus());
	}
	
	private void nextTransaction(String noRef, String username, Transaksi transaksi, TransactionStatus transactionStatus, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct, CallLog callLog) {
		if (transactionStatus == TransactionStatus.DIGITAL_CARD_REGISTRATION) {
			callLog.setTransaksiStatus("SUCCESS");
			// callLogService.save(callLog);

			// logging call log
			CallLog response = callLogService.save(callLog).get();
			ObjectMapper mapper = new ObjectMapper();
			try {
				String sResponse = mapper.writeValueAsString(response);
				log.debug("callLog rps from {} : response {} ", transactionStatus.name(), sResponse);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		
		transaksi.setIsTransactionStepFailed(false);
		transaksi.setTransactionStatus(null);
		List<TransactionMessageStatus> stepMap = updateTransactionStepStatus(noRef, transaksi, isNasabahExisting, isNrt, isChangeProduct, transactionStatus, Status.SUCCESS);
			socket.emit(SOCKET_TRANSACTION, socketMessageToString(new TransactionMessage(username, stepMap, Status.SUCCESS)));
		log.debug("nextTransaction | success transaction status {} ", transactionStatus);
	}
	
	private void stopTransaction(String noRef, String username, Transaksi transaksi, TransactionStatus transactionStatus, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct, CallLog callLog) {
		callLog.setTransaksiStatus("PENDING");
		callLogService.save(callLog);

		transaksi.setIsTransactionStepFailed(true);
		transaksi.setTransactionStatus(transactionStatus);

		// set process running false
		transaksi.setProcessRunning(false);

		List<TransactionMessageStatus> stepMap = updateTransactionStepStatus(noRef, transaksi, isNasabahExisting, isNrt, isChangeProduct, transactionStatus, Status.FAILED_RETRAY_ABLE);
		socket.emit(SOCKET_TRANSACTION, socketMessageToString(new TransactionMessage(username, stepMap, Status.FAILED_RETRAY_ABLE)));
		log.debug("stopTransaction | current transaction status {} ", transaksi.getTransactionStatus());
	}

	private void stopTransactionNotRetryAble(String noRef, String username, Transaksi transaksi, TransactionStatus transactionStatus, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct, CallLog callLog) {
		
		if(transactionStatus == TransactionStatus.DIGITAL_CARD_REGISTRATION) {
			callLog.setTransaksiStatus("SUCCESS");
		}else {
			callLog.setTransaksiStatus("PENDING");
		}
		
		callLogService.save(callLog);

		transaksi.setIsTransactionStepFailed(true);
		transaksi.setTransactionStatus(transactionStatus);

		// set process running false
		transaksi.setProcessRunning(false);

		List<TransactionMessageStatus> stepMap = updateTransactionStepStatus(noRef, transaksi, isNasabahExisting, isNrt, isChangeProduct, transactionStatus, Status.FAILED_NOT_RETRAY_ABLE);
		socket.emit(SOCKET_TRANSACTION, socketMessageToString(new TransactionMessage(username, stepMap, Status.FAILED_NOT_RETRAY_ABLE)));
		log.debug("stopTransaction | current transaction status {} ", transaksi.getTransactionStatus());
	}
	
	private List<TransactionMessageStatus> updateTransactionStepStatus(String noRef, Transaksi transaksi, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct, TransactionStatus step, TransactionMessage.Status status) {		
		String val = transaksi.getTransactionStatusMap();
		List<TransactionMessageStatus> keyVal = null;
				
		if (val == null) {
			keyVal =  buildStatusProgress(noRef, isNasabahExisting, isNrt, isChangeProduct);			
		} else {
			try
			{
				keyVal = objectMapper.readValue(val, new TypeReference<List<TransactionMessageStatus>>(){});				
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		
		keyVal = keyVal.stream().map(k -> {
			if (k.getName() == step) {
				k.setStatus(status);
			}
			return k;
		}).collect(Collectors.toList());

		try
		{
			val = objectMapper.writeValueAsString(keyVal);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		
		transaksi.setTransactionStatusMap(val);
		transaksiService.update(transaksi);
		
		return keyVal;
	}
	
	private String socketMessageToString(TransactionMessage data) {
		try
		{
			return objectMapper.writeValueAsString(data);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	private void printLog(Object object) {
        try {
            String json = objectMapper.writeValueAsString(object);
            log.debug("response : "+json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
    }
		
}
