package com.mitrakreasindo.pemrek.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseServiceImpl;
import com.mitrakreasindo.pemrek.model.MasterDataTolak;
import com.mitrakreasindo.pemrek.repository.MasterDataTolakRepository;
import com.mitrakreasindo.pemrek.service.MasterDataTolakService;

/**
 * @author miftakhul
 *
 */
@Service
public class MasterDataTolakServiceImpl extends BaseServiceImpl<MasterDataTolak> implements MasterDataTolakService
{
    @Autowired
    private MasterDataTolakRepository masterDataTolakRepository;

    @Override
    public List<MasterDataTolak> findAll() {
        return masterDataTolakRepository.findAll();
    }

}
