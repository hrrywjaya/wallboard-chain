package com.mitrakreasindo.pemrek.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseServiceImpl;
import com.mitrakreasindo.pemrek.model.MasterDataServiceLevel;
import com.mitrakreasindo.pemrek.repository.MasterDataServiceLevelRepository;
import com.mitrakreasindo.pemrek.service.MasterDataServiceLevelService;

/**
 * @author miftakhul
 *
 */
@Service
public class MasterDataServiceLevelServiceImpl extends BaseServiceImpl<MasterDataServiceLevel> implements MasterDataServiceLevelService
{
    @Autowired
    private MasterDataServiceLevelRepository masterDataServiceLevelRepository;

    @Override
    public List<MasterDataServiceLevel> findAll() {
        return masterDataServiceLevelRepository.findAll();
    }

}
