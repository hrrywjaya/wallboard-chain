// package com.mitrakreasindo.pemrek.serviceimpl;

// import java.util.ArrayList;
// import java.util.List;
// import java.util.UUID;
// import java.util.stream.Collectors;

// import javax.persistence.EntityManager;
// import javax.persistence.EntityManagerFactory;
// import javax.persistence.PersistenceContext;
// import javax.transaction.Transactional;

// import org.modelmapper.ModelMapper;
// import org.springframework.beans.BeanUtils;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.context.SecurityContextHolder;
// import org.springframework.stereotype.Service;

// import com.fasterxml.jackson.core.JsonProcessingException;
// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.mitrakreasindo.pemrek.core.exception.IdNotMatchException;
// import com.mitrakreasindo.pemrek.core.exception.ResourceNotFoundException;
// import com.mitrakreasindo.pemrek.core.model.Account;
// import com.mitrakreasindo.pemrek.core.service.UserService;
// import com.mitrakreasindo.pemrek.core.service.base.BaseServiceImpl;
// import com.mitrakreasindo.pemrek.dto.CallHistoryDto;
// import com.mitrakreasindo.pemrek.dto.MasterDataLogDataDto;
// import com.mitrakreasindo.pemrek.dto.MasterDataLogDto;
// import com.mitrakreasindo.pemrek.dto.MasterDataTerputusDto;
// import com.mitrakreasindo.pemrek.dto.MasterDataTolakDto;
// import com.mitrakreasindo.pemrek.dto.ReferensiDto;
// import com.mitrakreasindo.pemrek.dto.RingkasanPanggilanDto;
// import com.mitrakreasindo.pemrek.dto.TerputusDto;
// import com.mitrakreasindo.pemrek.dto.TolakDto;
// import com.mitrakreasindo.pemrek.dto.TransactionLogDto;
// import com.mitrakreasindo.pemrek.dto.TransactionLogLastAgentDto;
// import com.mitrakreasindo.pemrek.dto.TransaksiBaruDto;
// import com.mitrakreasindo.pemrek.dto.VerificationIdentityLogDto;
// import com.mitrakreasindo.pemrek.dto.ViLogDto;
// import com.mitrakreasindo.pemrek.model.CallHistory;
// import com.mitrakreasindo.pemrek.model.ChatLog;
// import com.mitrakreasindo.pemrek.model.MasterDataLogChecksum;
// import com.mitrakreasindo.pemrek.model.MasterDataType;
// import com.mitrakreasindo.pemrek.model.Referensi;
// import com.mitrakreasindo.pemrek.model.RingkasanPanggilan;
// import com.mitrakreasindo.pemrek.model.ServiceLevel;
// import com.mitrakreasindo.pemrek.model.Tolak;
// import com.mitrakreasindo.pemrek.model.Transaksi;
// import com.mitrakreasindo.pemrek.model.TransaksiBaru;
// import com.mitrakreasindo.pemrek.model.ViLog;
// import com.mitrakreasindo.pemrek.repository.TransaksiBaruRepository;
// import com.mitrakreasindo.pemrek.repository.TransaksiRepository;
// import com.mitrakreasindo.pemrek.service.TransaksiBaruService;
// import com.mitrakreasindo.pemrek.service.TransaksiService;

// @Service
// public class TransaksiBaruServiceImpl extends BaseServiceImpl<TransaksiBaru> implements TransaksiBaruService {

//     @Autowired
//     private UserService userService;
//     @Autowired
//     private TransaksiBaruRepository transaksiBaruRepository;

//     @Autowired
//     private ModelMapper modelMapper;

//     @PersistenceContext
//     private EntityManager em;

//     // @Autowired
//     // private ViLogRepository viLogRepository;
//     // @Autowired
//     // private MasterDataLogRepository masterDataLogRepository;
//     // @Autowired
//     // private ObjectMapper mapper;

//     // @PersistenceContext
//     // private EntityManager em;

//     // @Autowired
//     // private EntityManagerFactory emf;

//     // /**
//     // * Configure the entity manager to be used.
//     // *
//     // * @param em the {@link EntityManager} to set.
//     // */
//     // public void setEntityManager(EntityManager em) {
//     // this.em = em;
//     // }

//     @Override
//     public TransaksiBaru save(TransaksiBaru transaksiBaru) {
//         if (!transaksiBaru.getId().isEmpty()) {
//             TransaksiBaru tb = transaksiBaruRepository.findById(transaksiBaru.getId());
//             return transaksiBaruRepository.save(tb);
//         }
//         return null;
//     }

//     // @Override
//     // public TransaksiBaruDto findOneByNoRef(String noRef) {
//     // TransaksiBaru tb = em.createQuery(
//     // "SELECT tb FROM TransaksiBaru AS tb LEFT JOIN Referensi AS r ON tb.id =
//     // r.transaksiBaru WHERE r.noRef = :NoRef",
//     // TransaksiBaru.class).setParameter("NoRef",
//     // noRef).getResultList().stream().findFirst().orElse(null);
//     // TransaksiBaruDto tbDto = null;
//     // if (tb == null) {
//     // TransaksiBaru createTb = new TransaksiBaru();
//     // createTb.setNoTransaksi(UUID.randomUUID().toString());
//     // List<Referensi> lr = new ArrayList<Referensi>();
//     // Referensi r = new Referensi();
//     // r.setNoRef(noRef);
//     // lr.add(r);
//     // // r.setTransaksiBaru(createTb);
//     // createTb.setReferensis(lr);
//     // tbDto = modelMapper.map(createTb, TransaksiBaruDto.class);
//     // return this.saveTransaksi(tbDto);
//     // }
//     // if (tb != null) {
//     // tbDto = modelMapper.map(tb, TransaksiBaruDto.class);
//     // }
//     // return tbDto;
//     // }

//     @Override
//     public TransaksiBaruDto findOneByNoRef(String noRefCallId) {
//         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//         Account user = userService.findByUsername(authentication.getName());

//         // EntityManagerFactory emf =
//         // Persistence.createEntityManagerFactory("PERSISTENCE");
//         // EntityManager em = emf.createEntityManager();
//         // em.getTransaction().begin();

//         String[] parts = noRefCallId.split("-");
//         if (parts.length != 2) {
//             throw new IdNotMatchException();
//         }
//         String noRef = parts[0]; // 004
//         String callId = parts[1]; // 034556

// //        TransaksiBaru tb = em.createQuery(
// //                "SELECT tb FROM TransaksiBaru AS tb LEFT JOIN Referensi AS r ON tb.id = r.transaksiBaru WHERE r.noRef = :NoRef",
// //                TransaksiBaru.class).setParameter("NoRef", noRef).getResultList().stream().findFirst().orElse(null);
        
//         TransaksiBaru tb = transaksiBaruRepository.findOneByNoRef(noRef);
//         TransaksiBaruDto tbDto = null;

//         if (tb == null) {
//             TransaksiBaru createTb = new TransaksiBaru();
//             createTb.setNoTransaksi(UUID.randomUUID().toString());
//             // List<Referensi> lr = new ArrayList<Referensi>();
//             createTb.setIsFinish(false);
//             Referensi r = new Referensi();
//             r.setNoRef(noRef);
//             r.setAgentName(user.getUsername());
//             // lr.add(r);
//             createTb.addReferensi(r);
//             // r.setTransaksiBaru(createTb);
//             // createTb.setReferensis(lr);
//             CallHistory ch = new CallHistory();
//             ch.setCallId(callId);
//             ch.setNoRef(noRef);
//             ch.setAgentName(user.getUsername());
//             createTb.addCallHistory(ch);
//             tbDto = modelMapper.map(createTb, TransaksiBaruDto.class);
// //            tbDto = toDto(createTb);
//             return this.saveTransaksi(tbDto);
//         }
//         if (tb != null) {
//             tbDto = modelMapper.map(tb, TransaksiBaruDto.class);
// //        	tbDto = toDto(tb);
//         }

//         // em.getTransaction().commit();
//         // em.close();
//         // emf.close();
//         return tbDto;
//     }

//     @Override
//     public TransaksiBaru getLastTransaksiByNoRef(String noRef) {
//         return null;
//     }

//     @Override
//     public TransaksiBaru setApproved(TransaksiBaru transaksiBaru) {
//         return null;
//     }

//     // @Override
//     // public TransaksiBaruDto saveTransaksi(TransaksiBaruDto tbDto) {

//     // Authentication authentication =
//     // SecurityContextHolder.getContext().getAuthentication();
//     // Account user = userService.findByUsername(authentication.getName());

//     // EntityManager em = emf.createEntityManager();
//     // EntityTransaction tx = em.getTransaction();
//     // tx.begin();

//     // TransaksiBaru tb = modelMapper.map(tbDto, TransaksiBaru.class);
//     // if (tb.getId() == null) {
//     // tb.setNoTransaksi(UUID.randomUUID().toString());
//     // }
//     // tb.setAgent(user);
//     // TransaksiBaru tb1 = em.merge(tb);

//     // if (tb.getReferensis() != null) {
//     // for (Referensi r : tb.getReferensis()) {
//     // r.setAgentName(user.getUsername());
//     // r.setTransaksiBaru(tb1);
//     // em.merge(r);
//     // }
//     // }
//     // if (tb.getCallHistories() != null) {
//     // for (CallHistory ch : tb.getCallHistories()) {
//     // ch.setAgentName(user.getUsername());
//     // ch.setTransaksiBaru(tb1);
//     // em.merge(ch);
//     // }
//     // }
//     // if (tb.getViLogs() != null) {
//     // for (ViLog vl : tb.getViLogs()) {
//     // if (vl.getId() == null) {
//     // vl.setAgentName(user.getUsername());
//     // vl.setTransaksiBaru(tb1);
//     // em.merge(vl);
//     // }
//     // }
//     // }
//     // if (tb.getTolaks() != null) {
//     // for (Tolak tolak : tb.getTolaks()) {
//     // // if (tolak.getId() == null) {
//     // tolak.setAgentName(user.getUsername());
//     // tolak.setTransaksiBaru(tb1);
//     // // tolak.setCallHistory(callHistory);
//     // em.merge(tolak);
//     // // }
//     // }
//     // }

//     // em.flush();
//     // em.refresh(tb1);

//     // // TransaksiBaru tb = modelMapper.map(tbDto, TransaksiBaru.class);
//     // // if (tb.getId() == null) {
//     // // tb.setNoTransaksi(UUID.randomUUID().toString());
//     // // em.persist(tb);
//     // // }

//     // // if (tb.getReferensis() != null) {
//     // // for (Referensi r : tb.getReferensis()) {
//     // // if (r.getId() != null) {
//     // // r.setId(null);
//     // // }
//     // // r.setTransaksiBaru(tb);
//     // // r.setAgent(user);
//     // // em.persist(r);
//     // // }
//     // // }
//     // // if (tb.getViLogs() != null) {
//     // // for (ViLog v : tb.getViLogs()) {
//     // // v.setTransaksiBaru(tb);
//     // // v.setAgentName(user.getUsername());
//     // // em.persist(v);
//     // // }
//     // // }

//     // tx.commit();
//     // // return tbDto;

//     // TransaksiBaruDto result = modelMapper.map(tb1, TransaksiBaruDto.class);
//     // return result;
//     // }

//     @Override
//     @Transactional
//     public TransaksiBaruDto saveTransaksi(TransaksiBaruDto tbDto) {

//         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//         Account user = userService.findByUsername(authentication.getName());

//         // EntityManager em = emf.createEntityManager();
//         // EntityTransaction tx = em.getTransaction();
//         // tx.begin();

//         // EntityManagerFactory emf =
//         // Persistence.createEntityManagerFactory("PERSISTENCE");
//         // EntityManager em = emf.createEntityManager();
//         // em.getTransaction().begin();

//         // TransaksiBaru tb = modelMapper.map(tbDto, TransaksiBaru.class);
//         // if (tb.getId() == null) {
//         // tb.setNoTransaksi(UUID.randomUUID().toString());
//         // }
//         // tb.setAgent(user);
//         // if (tb.getCallHistories().){

//         // }
//         // TransaksiBaru tb1 = this.transaksiBaruRepository.save(tb);
//         // TransaksiBaruDto tbDto1 = modelMapper.map(tb1, TransaksiBaruDto.class);
//         // return tbDto1;
//         // TransaksiBaru tb1 = em.merge(tb);

//         TransaksiBaru tb = modelMapper.map(tbDto, TransaksiBaru.class);
//         tb.setAgent(user);
//         CallHistory ch = new CallHistory();

//         for (Referensi r : tb.getReferensis()) {
//         }

//         for (CallHistory ch1 : tb.getCallHistories()) {
//             if (ch1.getId() == null) {
//                 ch1.setAgentName(user.getUsername());
//                 CallHistory ch2 = new CallHistory();
//                 ch2 = ch1;
//                 em.persist(ch2);
//                 ch = ch2;
//                 break;
//             }
//         }

//         for (Tolak t : tb.getTolaks()) {
//             if (t.getCallHistory() != null) {
//                 if (t.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//                     if (t.getCallHistory().getCallId().matches(ch.getCallId())) {
//                         t.setCallHistory(ch);
//                         t.setAgentName(user.getUsername());
//                         // tb.addTolak(t);
//                         // em.persist(t);
//                         break;
//                     }
//                 }
//             }
//         }

//         for (RingkasanPanggilan rp : tb.getRingkasanPanggilans()) {
//             if (rp.getCallHistory() != null) {
//                 if (rp.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//                     if (rp.getCallHistory().getCallId().matches(ch.getCallId())) {
//                         rp.setCallHistory(ch);
//                         rp.setAgentName(user.getUsername());
//                         // tb.addTolak(t);
//                         // em.persist(t);
//                         break;
//                     }
//                 }
//             }
//         }

//         for (ViLog v : tb.getViLogs()) {
//             if (v.getCallHistory() != null) {
//                 if (v.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//                     if (v.getCallHistory().getCallId().matches(ch.getCallId())) {
//                         v.setCallHistory(ch);
//                         v.setAgentName(user.getUsername());
//                         // tb.addTolak(t);
//                         // em.persist(t);
//                         break;
//                     }
//                 }
//             }
//         }

//         // ServiceLevel sl = new ServiceLevel();

//         // if (tb.getServiceLevel() != null) {
//         // if (tb.getServiceLevel().getId() == null) {
//         // ServiceLevel s2 = tb.getServiceLevel();
//         // em.persist(s2);
//         // // tb.setServiceLevel(sl);
//         // }
//         // }

//         // if (tb.getServiceLevel() != null) {
//         // ServiceLevel sl = tb.getServiceLevel();
//         // if (sl.getCallHistory().getCallId() != null) {
//         // if (sl.getCallHistory().getCallId().matches(ch.getCallId())) {
//         // v.setCallHistory(ch);
//         // v.setAgentName(user.getUsername());
//         // // tb.addTolak(t);
//         // // em.persist(t);
//         // sl.setTransaksiBaru(tb);
//         // }

//         // }
//         // // if (sl.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//         // // if (sl.getCallHistory().getCallId().matches(ch.getCallId())) {
//         // // sl.setCallHistory(ch);
//         // // sl.setAgentName(user.getUsername());
//         // // // tb.addTolak(t);
//         // // // em.persist(t);
//         // // }
//         // // }
//         // }

//         if (tb.getServiceLevel() != null) {
//             if (tb.getServiceLevel().getId() == null) {
//                 ServiceLevel sl = tb.getServiceLevel();
//                 sl.setTransaksiBaru(tb);
//                 if (sl.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//                     if (sl.getCallHistory().getCallId().matches(ch.getCallId())) {
//                         sl.setCallHistory(ch);
//                         sl.setAgentName(user.getUsername());
//                         sl.setTransaksiBaru(tb);
//                     }
//                 }
//             }
//         }

//         for (ChatLog c : tb.getChatLogs()) {
//             if (c.getCallHistory() != null) {
//                 if (c.getCallHistory().getCallId() != null && ch.getCallId() != null) {
//                     if (c.getCallHistory().getCallId().matches(ch.getCallId())) {
//                         c.setCallHistory(ch);
//                         // c.setAgentName(user.getUsername());
//                         // tb.addTolak(t);
//                         // em.persist(t);
//                         break;
//                     }
//                 }
//             }
//         }

//         // for (ChatLog c : tb.getChatLogs()) {
//         // if (c.getId() == null) {
//         // ChatLog c2 = new ChatLog();
//         // c2 = c;
//         // em.persist(c2);
//         // break;
//         // }
//         // }

//         TransaksiBaru tb1 = em.merge(tb);
//         // return tbDto;
//         TransaksiBaruDto result = modelMapper.map(tb1, TransaksiBaruDto.class);

//         // em.flush();
//         // em.refresh(tb1);
//         // tx.commit();
//         // em.close();
//         // emf.close();

//         // em.flush();
//         // em.getTransaction().commit();
//         // em.close();
//         // emf.close();
//         // TransaksiBaruDto result = modelMapper.map(tb1, TransaksiBaruDto.class);
//         return result;
//     }

//     @Override    
//     @Transactional
//     public void delete(String id) {
//         // super.delete(id);
//         // transaksiBaruRepository.findById(id);
//         TransaksiBaru tb = em.find(TransaksiBaru.class, id);

//         em.remove(tb);
//     }
    
    
//     private TransaksiBaruDto toDto(TransaksiBaru transaksi) {
//     	TransaksiBaruDto dto = new TransaksiBaruDto();
//     	dto.setId(transaksi.getId());
//     	dto.setCreatedDate(transaksi.getCreatedDate());
//     	dto.setCreatedBy(transaksi.getCreatedBy());
//     	dto.setUpdatedDate(transaksi.getUpdatedDate());
//     	dto.setUpdatedBy(transaksi.getUpdatedBy());
//     	dto.setActiveFlag(transaksi.getActiveFlag());
//     	dto.setNoTransaksi(transaksi.getNoTransaksi());
//     	dto.setCallId(transaksi.getCallId());
//     	dto.setLastStatus(transaksi.getLastStatus());
    	
//     	if (transaksi.getReferensis() != null) {
//     		List<ReferensiDto> referensiDtos = new ArrayList<>();
//     		transaksi.getReferensis().forEach(r -> {
//     			ReferensiDto rdto = new ReferensiDto();
    			    			
//     			rdto.setAgentName(r.getAgentName());
//     			rdto.setNoRef(r.getNoRef());
    			
//     			TransaksiBaruDto tbdto = null;
// 					if (r.getTransaksiBaru() != null) {
// 						tbdto = new TransaksiBaruDto();
// 	    			tbdto.setId(r.getTransaksiBaru().getId());
// 					}
    			
// 					rdto.setId(r.getId());
//     			rdto.setTransaksiBaru(tbdto);
    			
//     			referensiDtos.add(rdto);
    			
//     		});
//     		dto.setReferensis(referensiDtos);
//     	}
    	
//     	if (transaksi.getViLogs() != null) {
//     		List<ViLogDto> vilogDtos = new ArrayList<>();
//     		transaksi.getViLogs().forEach(v -> {
//     			ViLogDto vdto = new ViLogDto();
    			
//     			TransaksiBaruDto tbdto = null;
// 					if (v.getTransaksiBaru() != null) {
// 						tbdto = new TransaksiBaruDto();
// 	    			tbdto.setId(v.getTransaksiBaru().getId());
// 					}
    	    			
//     			CallHistoryDto cdto = null;
//     			if (v.getCallHistory() != null) {
//     				cdto = new CallHistoryDto();
//       			cdto.setId(v.getCallHistory().getId());
//       			cdto.setCallId(v.getCallHistory().getCallId());
//     			}
    			
//     			vdto.setId(v.getId());
//     			vdto.setTransaksiBaru(tbdto);
//     			vdto.setCallHistory(cdto);
//     			vdto.setNoRef(v.getNoRef());
//     			vdto.setAgentName(v.getAgentName());
//     			vdto.setPredinFoto(v.getPredinFoto());
//     			vdto.setPredinKtp(v.getPredinKtp());
//     			vdto.setPredinTtd(v.getPredinTtd());
//     			vdto.setPredinNpwp(v.getPredinNpwp());    			
    			
//     			vilogDtos.add(vdto);
//     		});
//     		dto.setViLogs(vilogDtos);
//     	}
    	
//     	if (transaksi.getTolaks() != null) {
//     		List<TolakDto> tolakDtos = new ArrayList<>();
// 				transaksi.getTolaks().forEach(t -> {
// 					TolakDto tdto = new TolakDto();
					
// 					TransaksiBaruDto tbdto = null;
// 					if (t.getTransaksiBaru() != null) {
// 						tbdto = new TransaksiBaruDto();
// 	    			tbdto.setId(t.getTransaksiBaru().getId());
// 					}
    			
//     			MasterDataTolakDto mdto = null;
//     			if (t.getMasterDataTolak() != null) {
//     				mdto = new MasterDataTolakDto();
//       			mdto.setId(t.getMasterDataTolak().getId());
//       			mdto.setTitle(t.getMasterDataTolak().getTitle());
//       			mdto.setDescription(t.getMasterDataTolak().getDescription());
//     			}
    			
//     			CallHistoryDto cdto = null;
//     			if (t.getCallHistory() != null) {
//     				cdto = new CallHistoryDto();
//       			cdto.setId(t.getCallHistory().getId());
//       			cdto.setCallId(t.getCallHistory().getCallId());
//     			}
					
//     			tdto.setId(t.getId());
//     			tdto.setTransaksiBaru(tbdto);
//     			tdto.setMasterDataTolak(mdto);
//     			tdto.setCallHistory(cdto);
//     			tdto.setNoRef(t.getNoRef());
//     			tdto.setAgentName(t.getAgentName());
//     			tdto.setMessage(t.getMessage());
					
// 					tolakDtos.add(tdto);					
// 				});
//     		dto.setTolaks(tolakDtos);
//     	}
    	
//     	if (transaksi.getTerputuses() != null) {
//     		List<TerputusDto> tdtos = new ArrayList<>();
    		
//     		transaksi.getTerputuses().forEach(t -> {
//     			TerputusDto tdto = new TerputusDto();
      		
//       		TransaksiBaruDto tbdto = null;
//   				if (t.getTransaksiBaru() != null) {
//   					tbdto = new TransaksiBaruDto();
//       			tbdto.setId(t.getTransaksiBaru().getId());
//   				}
    			
//   				MasterDataTerputusDto mdto = null;
//     			if (t.getMasterDataTerputus() != null) {
//     				mdto = new MasterDataTerputusDto();
//       			mdto.setId(t.getMasterDataTerputus().getId());
//       			mdto.setTitle(t.getMasterDataTerputus().getTitle());
//       			mdto.setDescription(t.getMasterDataTerputus().getDescription());
//     			}
    			
//     			CallHistoryDto cdto = null;
//     			if (t.getCallHistory() != null) {
//     				cdto = new CallHistoryDto();
//       			cdto.setId(t.getCallHistory().getId());
//       			cdto.setCallId(t.getCallHistory().getCallId());
//     			}
    			
//     			tdto.setId(t.getId());
//     			tdto.setTransaksiBaru(tbdto);
//     			tdto.setMasterDataTerputus(mdto);
//     			tdto.setCallHistory(cdto);
//     			tdto.setNoRef(t.getNoRef());
//     			tdto.setAgentName(t.getAgentName());
//     			tdto.setMessage(t.getMessage());
    			
//     			tdtos.add(tdto);
//     		});    		
//     		dto.setTerputuses(tdtos);
//     	}
    	
//     	if (transaksi.getCallHistories() != null) {
//     		List<CallHistoryDto> cdtos = new ArrayList<>();
//     		transaksi.getCallHistories().forEach(c -> {
//     			CallHistoryDto cdto = new CallHistoryDto();
//     			cdto.setId(c.getId());
//     			cdto.setCallId(c.getCallId());
//     			cdto.setNoRef(c.getNoRef());
//     			cdto.setAgentName(c.getAgentName());
    			
//     			cdtos.add(cdto);
//     		});    		
//     		dto.setCallHistories(cdtos);
//     	}
    	
//     	if (transaksi.getRingkasanPanggilans() != null) {
//     		List<RingkasanPanggilanDto> rdtos = new ArrayList<>();
//     		transaksi.getRingkasanPanggilans().forEach(r -> {
//     			RingkasanPanggilanDto rdto = new RingkasanPanggilanDto();
    			
//     			TransaksiBaruDto tbdto = null;
//   				if (r.getTransaksiBaru() != null) {
//   					tbdto = new TransaksiBaruDto();
//       			tbdto.setId(r.getTransaksiBaru().getId());
//   				}
    			
//     			CallHistoryDto cdto = null;
//     			if (r.getCallHistory() != null) {
//     				cdto = new CallHistoryDto();
//       			cdto.setId(r.getCallHistory().getId());
//       			cdto.setCallId(r.getCallHistory().getCallId());
//     			}
    			
//     			rdto.setId(r.getId());
//     			rdto.setTransaksiBaru(tbdto);
//     			rdto.setCallHistory(cdto);
//     			rdto.setFoto(r.getFoto());
//     			rdto.setAgentName(r.getAgentName());
//     			rdto.setMessage(r.getMessage());
    			
//     			rdtos.add(rdto);
//     		});
//     		dto.setRingkasanPanggilans(rdtos);
//     	}
    	
//     	dto.setRawEform(transaksi.getRawEform());
//     	dto.setRawCis(transaksi.getRawCis());
//     	dto.setPredinFoto(transaksi.getPredinFoto());
//     	dto.setPredinFotoChecked(transaksi.getPredinFotoChecked());
//     	dto.setPredinKtp(transaksi.getPredinKtp());
//     	dto.setPredinKtpChecked(transaksi.getPredinKtpChecked());
//     	dto.setPredinTtd(transaksi.getPredinTtd());
//     	dto.setPredinTtdChecked(transaksi.getPredinTtdChecked());
//     	dto.setPredinNpwp(transaksi.getPredinNpwp());
//     	dto.setPredinNpwpChecked(transaksi.getPredinNpwpChecked());
//     	dto.setDinFoto(transaksi.getDinFoto());
//     	dto.setDinFotoChecked(transaksi.getDinFotoChecked());
//     	dto.setDinKtp(transaksi.getDinKtp());
//     	dto.setDinKtpChecked(transaksi.getDinKtpChecked());
//     	dto.setDinTtd(transaksi.getPredinTtd());
//     	dto.setDinTtdChecked(transaksi.getDinTtdChecked());
//     	dto.setDinNpwp(transaksi.getPredinNpwp());
//     	dto.setDinNpwpChecked(transaksi.getDinNpwpChecked());
//     	dto.setCisIsExisting(transaksi.getCisIsExisting());
//     	dto.setCisCustomerNumber(transaksi.getCisCustomerNumber());
//     	dto.setCisTanggalLahir(transaksi.getCisTanggalLahir());
//     	dto.setCisTempatLahir(transaksi.getCisTempatLahir());
//     	dto.setCisNamaIbuKandung(transaksi.getCisNamaIbuKandung());
//     	dto.setCisStatusTempatTinggal(transaksi.getCisStatusTempatTinggal());
//     	dto.setCisNamaJalan(transaksi.getCisNamaJalan());
//     	dto.setCisKodePos(transaksi.getCisKodePos());
//     	dto.setCisProvinsi(transaksi.getCisProvinsi());
//     	dto.setCisKabupaten(transaksi.getCisKabupaten());
//     	dto.setCisKecamatan(transaksi.getCisKecamatan());
//     	dto.setCisKelurahan(transaksi.getCisKelurahan());
//     	dto.setEddUserId(transaksi.getEddUserId());
//     	dto.setEddReasonEdd(transaksi.getEddReasonEdd());
//     	dto.setEddCurrentAddressFromMonthyear(transaksi.getEddCurrentAddressFromMonthyear());
//     	dto.setEddAdditionalAccountOrCreditcardInstitutionName(transaksi.getEddAdditionalAccountOrCreditcardInstitutionName());
//     	dto.setEddAdditionalAccountOrCreditcardFromMonthyear(transaksi.getEddAdditionalAccountOrCreditcardFromMonthyear());
//     	dto.setEddForeignCompanyCountryCode1(transaksi.getEddForeignCompanyCountryCode1());
//     	dto.setEddForeignCompanyCountryCode2(transaksi.getEddForeignCompanyCountryCode2());
//     	dto.setEddForeignCompanyCountryCode3(transaksi.getEddForeignCompanyCountryCode3());
//     	dto.setEddWealthFromHeritance(transaksi.getEddWealthFromHeritance());
//     	dto.setEddWealthFromSaving(transaksi.getEddWealthFromSaving());
//     	dto.setEddWealthFromBusiness(transaksi.getEddWealthFromBusiness());
//     	dto.setEddWealthFromGift(transaksi.getEddWealthFromGift());
//     	dto.setEddWealthFromSalary(transaksi.getEddWealthFromSalary());
//     	dto.setEddWealthFromOthers(transaksi.getEddWealthFromOthers());
//     	dto.setEddWealthFromOthersDecription(transaksi.getEddWealthFromOthersDecription());
//     	dto.setEddAdditionalInformation(transaksi.getEddAdditionalInformation());
//     	dto.setIsFinish(transaksi.getIsFinish());
    	
//     	dto.setRefNum(transaksi.getRefNum());
//     	dto.setStatus(transaksi.getStatus());
//     	dto.setEformCreatedDate(transaksi.getEformCreatedDate());
//     	dto.setEformCreatedTime(transaksi.getEformCreatedTime());
//     	dto.setTipeKartuPasporBca(transaksi.getTipeKartuPasporBca());
//     	dto.setNamaTempatBekerja(transaksi.getNamaTempatBekerja());
//     	dto.setNoCustomerRekeningGabungan(transaksi.getNoCustomerRekeningGabungan());
//     	dto.setFasilitasYangDiinginkanFinMbca(transaksi.getFasilitasYangDiinginkanFinMbca());
//     	dto.setSerialNumberToken(transaksi.getSerialNumberToken());
//     	dto.setNamaJalan(transaksi.getNamaJalan());
//     	dto.setAlamatKantor3(transaksi.getAlamatKantor3());
//     	dto.setBerlakuSampaiDengan(transaksi.getBerlakuSampaiDengan());
//     	dto.setStatusPerkawinan(transaksi.getStatusPerkawinan());
//     	dto.setProvinsiRekeningKoran(transaksi.getProvinsiRekeningKoran());
//     	dto.setValidasiCatatanBankKategoriNasabah(transaksi.getValidasiCatatanBankKategoriNasabah());
//     	dto.setStatusRekening(transaksi.getStatusRekening());
//     	dto.setTipeIdentitasLainnya(transaksi.getTipeIdentitasLainnya());
//     	dto.setNoCustomer(transaksi.getNoCustomer());
//     	dto.setNomorTeleponRumah(transaksi.getNomorTeleponRumah());
//     	dto.setAgama(transaksi.getAgama());
//     	dto.setAlamatKantor1(transaksi.getAlamatKantor1());
//     	dto.setAlamatKantor2(transaksi.getAlamatKantor2());
//     	dto.setKdPenduduk(transaksi.getKdPenduduk());
//     	dto.setOdPlan(transaksi.getOdPlan());
//     	dto.setServiceChargeCode(transaksi.getServiceChargeCode());
//     	dto.setFormPerpajakanCrs(transaksi.getFormPerpajakanCrs());
//     	dto.setTempatLahir(transaksi.getTempatLahir());
//     	dto.setPernyataanFasilitasBca(transaksi.getPernyataanFasilitasBca());
//     	dto.setPersetujuanTelepon(transaksi.getPersetujuanTelepon());
//     	dto.setJenisRekening(transaksi.getJenisRekening());
//     	dto.setWitholdingPlan(transaksi.getWitholdingPlan());
//     	dto.setJabatan(transaksi.getJabatan());
//     	dto.setNomorHp(transaksi.getNomorHp());
//     	dto.setNikRekeningGabungan(transaksi.getNikRekeningGabungan());
//     	dto.setNamaGedung(transaksi.getNamaGedung());
//     	dto.setNpwp(transaksi.getNpwp());
//     	dto.setSumberPenghasilan(transaksi.getSumberPenghasilan());
//     	dto.setFasilitasYangDiinginkanMbca(transaksi.getFasilitasYangDiinginkanMbca());
//     	dto.setKodeAreaTeleponKantor(transaksi.getKodeAreaTeleponKantor());
//     	dto.setPersetujuanSms(transaksi.getPersetujuanSms());
//     	dto.setNamaGadisIbuKandung(transaksi.getNamaGadisIbuKandung());
//     	dto.setNamaDicetakDiKartu(transaksi.getNamaDicetakDiKartu());
//     	dto.setCostCenter(transaksi.getCostCenter());
//     	dto.setKabupatenRekeningKoran(transaksi.getKabupatenRekeningKoran());
//     	dto.setNamaRekeningGabungan(transaksi.getNamaRekeningGabungan());
//     	dto.setInterestPlan(transaksi.getInterestPlan());
//     	dto.setKodeNegaraFaksimilie(transaksi.getKodeNegaraFaksimilie());
//     	dto.setDenganBuku(transaksi.getDenganBuku());
//     	dto.setSumberPenghasilanLainnya(transaksi.getSumberPenghasilanLainnya());
//     	dto.setDalamHalIniBertindak(transaksi.getDalamHalIniBertindak());
//     	dto.setFasilitasYangDiinginkanKlikbca(transaksi.getFasilitasYangDiinginkanKlikbca());
//     	dto.setNomorNpwp(transaksi.getNomorNpwp());
//     	dto.setKitasKitap(transaksi.getKitasKitap());
//     	dto.setKodeNegaraNomorHp2(transaksi.getKodeNegaraNomorHp2());
//     	dto.setNomorFaksimilie(transaksi.getNomorFaksimilie());
//     	dto.setKodeNegaraNomorHp(transaksi.getKodeNegaraNomorHp());
//     	dto.setPernyataanProduk(transaksi.getPernyataanProduk());
//     	dto.setNomorRekeningBaru(transaksi.getNomorRekeningBaru());
//     	dto.setFormPerpajakanFatca(transaksi.getFormPerpajakanFatca());
//     	dto.setBerlakuSampaiDenganKitasKitap(transaksi.getBerlakuSampaiDenganKitasKitap());
//     	dto.setKecamatan(transaksi.getKecamatan());
//     	dto.setPersetujuanEmail(transaksi.getPersetujuanEmail());
//     	dto.setFasilitasYangDiinginkanKeybca(transaksi.getFasilitasYangDiinginkanKeybca());
//     	dto.setKelurahanRekeningKoran(transaksi.getKelurahanRekeningKoran());
//     	dto.setBahasaPetunjukLayarAtm(transaksi.getBahasaPetunjukLayarAtm());
//     	dto.setJenisKelamin(transaksi.getJenisKelamin());
//     	dto.setNegaraLahirFatca(transaksi.getNegaraLahirFatca());
//     	dto.setPersetujuanDataPihakKetiga(transaksi.getPersetujuanDataPihakKetiga());
//     	dto.setEmail(transaksi.getEmail());
//     	dto.setNamaGedungRekeningKoran(transaksi.getNamaGedungRekeningKoran());
//     	dto.setJenisKartuPasporBca(transaksi.getJenisKartuPasporBca());
//     	dto.setTotalPenghasilan(transaksi.getTotalPenghasilan());
//     	dto.setCabang(transaksi.getCabang());
//     	dto.setKodeAreaTeleponRumah(transaksi.getKodeAreaTeleponRumah());
//     	dto.setKodeNegaraDataRekening(transaksi.getKodeNegaraDataRekening());
//     	dto.setNomorHpMbca(transaksi.getNomorHpMbca());
//     	dto.setKodeNegaraTeleponKantor(transaksi.getKodeNegaraTeleponKantor());
//     	dto.setKecamatanRekeningKoran(transaksi.getKecamatanRekeningKoran());
//     	dto.setPernyataanPasporBca(transaksi.getPernyataanPasporBca());
//     	dto.setNomorRekeningExisting(transaksi.getNomorRekeningExisting());
//     	dto.setKodePosRekeningKoran(transaksi.getKodePosRekeningKoran());
//     	dto.setKelurahan(transaksi.getKelurahan());
//     	dto.setRekeningUntuk(transaksi.getRekeningUntuk());
//     	dto.setWajibPajakNegaraLain(transaksi.getWajibPajakNegaraLain());
//     	dto.setNamaNasabah(transaksi.getNamaNasabah());
//     	dto.setPekerjaan(transaksi.getPekerjaan());
//     	dto.setNomorKitasKitap(transaksi.getNomorKitasKitap());
//     	dto.setPeriodeBiayaAdmin(transaksi.getPeriodeBiayaAdmin());
//     	dto.setPernyataanFasilitasBcaDetail(transaksi.getPernyataanFasilitasBcaDetail());
//     	dto.setTinSsn(transaksi.getTinSsn());
//     	dto.setKotaKantor(transaksi.getKotaKantor());
//     	dto.setPejabatBank(transaksi.getPejabatBank());
//     	dto.setKabupaten(transaksi.getKabupaten());
//     	dto.setNegara(transaksi.getNegara());
//     	dto.setTujuanPembukaanRekening(transaksi.getTujuanPembukaanRekening());
//     	dto.setBiayaAdmin(transaksi.getBiayaAdmin());
//     	dto.setNegaraAlamat(transaksi.getNegaraAlamat());
//     	dto.setRtRw(transaksi.getRtRw());
//     	dto.setRtRwRekeningKoran(transaksi.getRtRwRekeningKoran());
//     	dto.setKodePeroranganBisnis(transaksi.getKodePeroranganBisnis());
//     	dto.setGolonganPemilik(transaksi.getGolonganPemilik());
//     	dto.setBidangUsaha(transaksi.getBidangUsaha());
//     	dto.setKodePos(transaksi.getKodePos());
//     	dto.setKodePenambahanPajak(transaksi.getKodePenambahanPajak());
//     	dto.setNomorHp2(transaksi.getNomorHp2());
//     	dto.setKodeAreaFaksimilie(transaksi.getKodeAreaFaksimilie());
//     	dto.setProvinsi(transaksi.getProvinsi());
//     	dto.setPeriodeRK(transaksi.getPeriodeRK());
//     	dto.setNegaraAlamatRekeningKoran(transaksi.getNegaraAlamatRekeningKoran());
//     	dto.setNil(transaksi.getNil());
//     	dto.setUserCode(transaksi.getUserCode());
//     	dto.setNik(transaksi.getNik());
//     	dto.setNamaJalanRekeningKoran(transaksi.getNamaJalanRekeningKoran());
//     	dto.setPeriodeBunga(transaksi.getPeriodeBunga());
//     	dto.setNomorTeleponKantor(transaksi.getNomorTeleponKantor());
//     	dto.setPernyataanProdukDetail(transaksi.getPernyataanProdukDetail());
//     	dto.setTanggalLahir(transaksi.getTanggalLahir());
//     	dto.setKodePosKantor(transaksi.getKodePosKantor());
//     	dto.setKodeNegaraTeleponRumah(transaksi.getKodeNegaraTeleponRumah());
//     	dto.setWajibFatca(transaksi.getWajibFatca());
//     	dto.setPengirimanRekeningKoran(transaksi.getPengirimanRekeningKoran());

//     	// *** ditanyakan
//     	dto.setFormAeoi(transaksi.getFormAeoi());
    	
//     	return dto;
//     }

// }
