package com.mitrakreasindo.pemrek.serviceimpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.model.EmailRequest;
import com.mitrakreasindo.pemrek.repository.EddApprovalRepository;
import com.mitrakreasindo.pemrek.service.EddApprovalService;
import com.mitrakreasindo.pemrek.repository.CallLogRepository;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EddApprovalServiceImpl {

  @Autowired
  private EddApprovalRepository eddApprovalRepository;
  @Autowired
  private CallLogRepository callLogRepository;
  @Value("${email.client-id}")
  private String clientId;
  @Autowired
	private ObjectMapper mapper;


  public ErrorSchemaUpperCamelCase sendEmail(EmailRequest email) {
    // String httpMethod = "POST";
    // String relativeUrl = "/generateEmailFromTemplate/initiate";	
    // String date = LocalDate.now().toString();
    // Long jml = callLogRepository.countByEddApprovalIsNotNull();
    String contentType = "application/json";
    String json = "";
    try
    {
			json = mapper.writeValueAsString(email);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}

    return eddApprovalRepository.sendEmail(clientId, email);
  }

}