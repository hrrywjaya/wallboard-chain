package com.mitrakreasindo.pemrek.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseServiceImpl;
import com.mitrakreasindo.pemrek.model.MasterDataTerputus;
import com.mitrakreasindo.pemrek.repository.MasterDataTerputusRepository;
import com.mitrakreasindo.pemrek.service.MasterDataTerputusService;

/**
 * @author miftakhul
 *
 */
@Service
public class MasterDataTerputusServiceImpl extends BaseServiceImpl<MasterDataTerputus> implements MasterDataTerputusService
{
    @Autowired
    private MasterDataTerputusRepository masterDataTerputusRepository;

    @Override
    public List<MasterDataTerputus> findAll() {
        return masterDataTerputusRepository.findAll();
    }

}
