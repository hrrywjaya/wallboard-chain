package com.mitrakreasindo.pemrek.external.finesse.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "finesse-repository", url = "${finesse.server.url}")
public interface FinesseRepository
{

	@RequestMapping(method = RequestMethod.GET, value = "/finesse/{path}")
	Object finesseGetFuntion(
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("Content-Type") String contentType,
			@PathVariable("path") String path);
	
	@RequestMapping(method = RequestMethod.PUT, value = "/finesse/{path}")
	Object finessePutFuntion(
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("Content-Type") String contentType,
			@PathVariable("path") String path);
	
}
