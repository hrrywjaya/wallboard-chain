package com.mitrakreasindo.pemrek.external.finesse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.finesse.repository.network.FinesseRepository;

@RestController
@RequestMapping("/finesse")
public class FinesseController
{
	
	@Autowired
	private FinesseRepository finesseRepository;
	
	@GetMapping("/{path}")
	public Object finesseGetFuntion(
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("Content-Type") String contentType,
			@PathVariable("path") String path) {
		return finesseRepository.finesseGetFuntion(authorization, contentType, path);
	}
	
	@PutMapping("/{path}")
	public Object finessePutFuntion(
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("Content-Type") String contentType,
			@PathVariable("path") String path) {
		return finesseRepository.finessePutFuntion(authorization, contentType, path);
	}
	
//	@GetMapping("/{path}")
//	public String finesseGetFuntion(@PathVariable("path") String path) {
//		return "get";
//	}
//	
//	@PutMapping("/{path}")
//	public String finessePutFuntion(@PathVariable("path") String path) {
//		return "put";
//	}

}
