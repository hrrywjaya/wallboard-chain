package com.mitrakreasindo.pemrek.external.gateway.repository.network;

import java.util.Map;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;

import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;

@FeignClient(name = "gateway-repository", url = "${gateway.server.url}", configuration = GatewayRepository.GatewayConfiguration.class)
public interface GatewayRepository
{
	
	/**
	 * request token
	 * @param authorization
	 * @param form
	 * @return
	 */
	 @RequestMapping(method = RequestMethod.POST, value = "/auth/realms/3scale-uat/protocol/openid-connect/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	//@RequestMapping(method = RequestMethod.POST, value = "/protocol/openid-connect/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	GatewayToken requestToken(
			@RequestHeader("Authorization") String authorization,
			@RequestBody Map<String, ?> form);
	
	
	class GatewayConfiguration {
		
		@Bean
		public Encoder encoder(ObjectFactory<HttpMessageConverters> converters) {
			return new SpringFormEncoder(new SpringEncoder(converters));
		}
		
	}
	
}
