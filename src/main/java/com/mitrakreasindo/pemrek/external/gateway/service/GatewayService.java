package com.mitrakreasindo.pemrek.external.gateway.service;

import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;

public interface GatewayService
{

	GatewayToken getToken();
		
}
