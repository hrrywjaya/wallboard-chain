package com.mitrakreasindo.pemrek.external.gateway.serviceimpl;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.repository.network.GatewayRepository;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;

@Service
public class GatewayServiceImpl implements GatewayService
{

	@Autowired
	private GatewayRepository gatewayRepository;
	@Value("${gateway.client-id}")
	private String gatewayId;
	@Value("${gateway.client-secret}")
	private String gatewaySecret;
	
	@Bean
	private GatewayToken gatewayToken() {
		return new GatewayToken();
	}
	
	@Override
	public GatewayToken getToken ()
	{
		if (gatewayToken().isTokenValid() && gatewayToken().isRefreshTokenValid())
			return gatewayToken();
		else if (!gatewayToken().isTokenValid() && !gatewayToken().isRefreshTokenValid())
			return requestToken();
		else if (!gatewayToken().isTokenValid() && gatewayToken().isRefreshTokenValid())
			return refreshToken();
		
		return requestToken();
	}
	
	private GatewayToken requestToken ()
	{
		String basicAuth = "Basic "+Base64.getEncoder().encodeToString(new String(gatewayId+":"+gatewaySecret).getBytes());
		Map<String, String> form = new HashMap<>();
		form.put("grant_type", "client_credentials");
		return gatewayRepository.requestToken(basicAuth, form);
	}
	
	private GatewayToken refreshToken ()
	{
		String basicAuth = "Basic "+Base64.getEncoder().encodeToString(new String(gatewayId+":"+gatewaySecret).getBytes());
		Map<String, String> form = new HashMap<>();
		form.put("grant_type", "client_credentials");
		return gatewayRepository.requestToken(basicAuth, form);
	}
	
}
