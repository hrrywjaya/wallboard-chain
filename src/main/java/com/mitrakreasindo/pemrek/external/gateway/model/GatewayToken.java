package com.mitrakreasindo.pemrek.external.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class GatewayToken
{

	private String accessToken;
	private long expiersIn;
	private long refreshExpiresIn;
	private String refreshToken;
	private String tokenType;
	private String idToken;
	@JsonProperty("not-before-policy")
	private long notBeforePolicy;
	private String sessionState;
	private long tokenCreated = System.currentTimeMillis();
	
	public boolean isTokenValid() {		
		return (tokenCreated + (expiersIn * 1000)) < System.currentTimeMillis();
	}
	
	public boolean isRefreshTokenValid() {		
		return (tokenCreated + (refreshExpiresIn * 1000)) < System.currentTimeMillis();
	}
	
}
