package com.mitrakreasindo.pemrek.external.din.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class SignatureInfo
{

	private String signatureImage;
	private String signatureUpdater;
	private String signatureUpdateDate;
	
}
