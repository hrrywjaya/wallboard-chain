package com.mitrakreasindo.pemrek.external.din.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.mitrakreasindo.pemrek.external.din.model.DinItem;
import com.mitrakreasindo.pemrek.external.din.model.Output;
import com.mitrakreasindo.pemrek.external.din.service.DinService;

@RestController
@RequestMapping("/capi/din")
public class DinController
{
	
	@Autowired
	private DinService dinService;
	
	@GetMapping("/{cisNumber}")
	public List<DinItem> getDin (@PathVariable("cisNumber") String cisNumber) {
		return dinService.getDin(cisNumber);
	}
	
}
