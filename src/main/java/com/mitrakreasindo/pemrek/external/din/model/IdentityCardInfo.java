package com.mitrakreasindo.pemrek.external.din.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class IdentityCardInfo
{

	private String identityCardImage;
	private String identityCardUpdater;
	private String identityCardUpdateDate;
		
}
