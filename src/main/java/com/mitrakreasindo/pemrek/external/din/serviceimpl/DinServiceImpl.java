package com.mitrakreasindo.pemrek.external.din.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.din.model.DinItem;
import com.mitrakreasindo.pemrek.external.din.model.Output;
import com.mitrakreasindo.pemrek.external.din.repository.network.DinRepository;
import com.mitrakreasindo.pemrek.external.din.service.DinService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class DinServiceImpl implements DinService
{

	@Autowired
	private DinRepository dinRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${din.user-id}")
	private String userId;
	@Autowired
	private AccountService userService;
	@Autowired
	private ObjectMapper mapper;
	
	// @Override
	// public Output getDin (String cisNumber)
	// {
	// 	return dinRepository.getDin(dinClientId, cisNumber, dinUserId, dinApplicationId);
	// }

	@Override
	public List<DinItem> getDin(String cisNumber) {
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/din/branches/"+account.getBranchCode()+"/customers/"+cisNumber;
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		
		return dinRepository.getDin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				cisNumber);
	}

}
