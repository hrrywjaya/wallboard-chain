package com.mitrakreasindo.pemrek.external.din.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import com.mitrakreasindo.pemrek.external.din.model.DinItem;
import com.mitrakreasindo.pemrek.external.din.model.Output;

@FeignClient(name = "din-repository", url = "${din.server.url}")
public interface DinRepository
{

	// /**
	//  * get din by cis number
	//  * @param clientId
	//  * @param cisNumber
	//  * @param userId
	//  * @param applicationId
	//  * @return
	//  */
	// @RequestMapping(value = "/DIN/{cisNumber}")
	// Output getDin(
	// 		@RequestHeader("ClientID") String clientId,
	// 		@PathVariable("cisNumber") String cisNumber,
	// 		@RequestParam("UserId") String userId,
	// 		@RequestParam("ApplicationId") String applicationId);
	
	/**
	 * get din
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param cisNumber
	 * @return
	 */
	@RequestMapping(value = "/bds/general/api/din/branches/{branch-code}/customers/{cis-number}")
	List<DinItem> getDin(
		@RequestHeader("app-id") String appId,
		@RequestHeader("user-domain") String userDomain,
		@RequestHeader("user-id") String userId,
		@RequestHeader("Origin") String Origin,
		@RequestHeader("Authorization") String authorization,
		@RequestHeader("X-BCA-Key") String xBcaKey,
		@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
		@RequestHeader("X-BCA-Signature") String xBcaSignature,
		@PathVariable("branch-code") String branchCode,
		@PathVariable("cis-number") String cisNumber);

}
