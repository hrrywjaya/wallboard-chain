package com.mitrakreasindo.pemrek.external.din.service;

import java.util.List;

import com.mitrakreasindo.pemrek.external.din.model.DinItem;
import com.mitrakreasindo.pemrek.external.din.model.Output;

public interface DinService
{

	List<DinItem> getDin(String cisNumber);
	
}
