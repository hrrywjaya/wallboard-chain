package com.mitrakreasindo.pemrek.external.din.model;

import lombok.Data;

@Data
public class DinItem {

    private String key;
    private String value;

}