package com.mitrakreasindo.pemrek.external.predin.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mitrakreasindo.pemrek.external.predin.model.Predin;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.AprovePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.SavePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.predin.model.response.AprovePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.PredinUpdateResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.SavePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.UpdateReferenceNumberResponse;

@FeignClient(name = "predin-repository", url = "${predin.server.url}")
public interface PredinRepository
{

	/**
	 * get predin
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branceCode
	 * @param referenceNumber
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bds/general/api/pre-din/branches/{branch-code}/references/{reference-number}")
	Predin getPredin(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@PathVariable("reference-number") String referenceNumber
			);
	
	/**
	 * save predin
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bds/general/api/pre-din/branches/{branch-code}")
	SavePredinResponse savePredin(
		@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@RequestBody SavePredinRequest request
	);


	/**
	 * update predin
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param referenceNumber
	 * @param predin
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/bds/general/api/pre-din/branches/{branch-code}/references/{reference-number}")
	PredinUpdateResponse updatePredin(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@PathVariable("reference-number") String referenceNumber,
			@RequestBody PredinUpdateRequest predin
			);
	
	/**
	 * approve predin
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param aprovePredinRequest
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bds/general/api/pre-din/branches/{branch-code}/approval")
	AprovePredinResponse aprovePredin(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@RequestBody AprovePredinRequest aprovePredinRequest
			);
	
	/**
	 * update reference number
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param updateReferenceNumberRequest
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/bds/general/api/pre-din/branches/{branch-code}/reference-numbers")
	UpdateReferenceNumberResponse updateReferencenNumber(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@RequestBody PredinUpdateReferenceNumberRequest updateReferenceNumberRequest
			);
	
}
