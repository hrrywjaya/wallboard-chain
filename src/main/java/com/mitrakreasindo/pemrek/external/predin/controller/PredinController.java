package com.mitrakreasindo.pemrek.external.predin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.predin.model.Predin;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.AprovePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.SavePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.predin.model.response.AprovePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.PredinUpdateResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.SavePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.predin.service.PredinService;

@RestController
@RequestMapping("/capi/predin")
public class PredinController
{

	@Autowired
	private PredinService predinService;

	@GetMapping
	public Predin getPredin (
			@RequestParam("reference-number") String referenceNumber){
		return predinService.getPredin(referenceNumber);
	}

	@PostMapping
	public SavePredinResponse savePredin (
			@RequestBody SavePredinRequest request){
		return predinService.savePredin(request);
	}
	
	@PutMapping
	public PredinUpdateResponse updatePredin (
			@RequestParam("reference-number") String referenceNumber,
			@RequestBody PredinUpdateRequest predin){
		return predinService.updatePredin(referenceNumber, predin);
	}
	
	@PostMapping("/approval")
	AprovePredinResponse aprovePredin(
			@RequestBody AprovePredinRequest aprovePredinRequest) {
		return predinService.aprovePredin(aprovePredinRequest);
	}
	
	@PutMapping("/reference-number")
	public UpdateReferenceNumberResponse updateReferenceNumber (
			@RequestBody PredinUpdateReferenceNumberRequest updateReferenceNumberRequest) {
		return predinService.updateReferenceNumber(updateReferenceNumberRequest);
	}

}
