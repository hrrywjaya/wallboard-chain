package com.mitrakreasindo.pemrek.external.predin.model.reqeust;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class SavePredinRequest {

    private String refNo;
    private String userId;
    private String accountNumber;
    private String customerNumber;
    private String customerName;
    private String photo;
    private String identity;
    private String sign;
    private String complementDocument;
    private String complementDocumentType;
    
}