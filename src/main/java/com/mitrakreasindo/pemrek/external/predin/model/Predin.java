package com.mitrakreasindo.pemrek.external.predin.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Predin
{

	private String refNo;
	private String userId;
	private String branchCode;
	private String photoPredin;
	private String photoOriPredin;
	private String signPredin;
	private String idPredin;
	private String idOriPredin;
	private String sup1Predin;
	private String sup1TypePredin;
	private String notesPredin;
	
}
