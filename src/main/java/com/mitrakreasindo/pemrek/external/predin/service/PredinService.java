package com.mitrakreasindo.pemrek.external.predin.service;

import com.mitrakreasindo.pemrek.external.predin.model.Predin;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.AprovePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.SavePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.predin.model.response.AprovePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.PredinUpdateResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.SavePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.UpdateReferenceNumberResponse;

public interface PredinService
{

	Predin getPredin(String referenceNumber);
	
	SavePredinResponse savePredin(SavePredinRequest request);

	PredinUpdateResponse updatePredin(String referenceNumber, PredinUpdateRequest predin);
	
	AprovePredinResponse aprovePredin(AprovePredinRequest aprovePredinRequest);

	AprovePredinResponse aprovePredin(String username, String branchCode, AprovePredinRequest aprovePredinRequest);
	
	UpdateReferenceNumberResponse updateReferenceNumber(PredinUpdateReferenceNumberRequest updateReferenceNumberRequest);

	UpdateReferenceNumberResponse updateReferenceNumber(String username, String branchCode, PredinUpdateReferenceNumberRequest updateReferenceNumberRequest);
	
}
