package com.mitrakreasindo.pemrek.external.predin.serviceimpl;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.predin.model.Predin;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.AprovePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.PredinUpdateRequest;
import com.mitrakreasindo.pemrek.external.predin.model.reqeust.SavePredinRequest;
import com.mitrakreasindo.pemrek.external.predin.model.response.AprovePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.PredinUpdateResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.SavePredinResponse;
import com.mitrakreasindo.pemrek.external.predin.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.predin.repository.network.PredinRepository;
import com.mitrakreasindo.pemrek.external.predin.service.PredinService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class PredinServiceImpl implements PredinService
{

	@Autowired
	private PredinRepository predinRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${predin.user-id}")
	private String userId;
	@Autowired
	private AccountService userService;
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	public Predin getPredin (String referenceNumber)
	{
		
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode()+"/references/"+referenceNumber;	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		
		return predinRepository.getPredin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				referenceNumber);
	}

	@Override
	public SavePredinResponse savePredin(SavePredinRequest request) {
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode();	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.savePredin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				request);
	}
	
	@Override
	public PredinUpdateResponse updatePredin (String referenceNumber, PredinUpdateRequest predin)
	{
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode()+"/references/"+referenceNumber;	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(predin);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.updatePredin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				referenceNumber,
				predin);
	}
	
	@Override
	public AprovePredinResponse aprovePredin (AprovePredinRequest aprovePredinRequest)
	{
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode()+"/approval";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(aprovePredinRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.aprovePredin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				aprovePredinRequest);
	}

	@Override
	public AprovePredinResponse aprovePredin (String username, String branchCode, AprovePredinRequest aprovePredinRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+branchCode+"/approval";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(aprovePredinRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.aprovePredin(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				username, 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				branchCode,
				aprovePredinRequest);
	}
	
	@Override
	public UpdateReferenceNumberResponse updateReferenceNumber (PredinUpdateReferenceNumberRequest updateReferenceNumberRequest)
	{
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode()+"/reference-numbers";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(updateReferenceNumberRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.updateReferencenNumber(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				account.getBranchCode(),
				updateReferenceNumberRequest);
	}

	@Override
	public UpdateReferenceNumberResponse updateReferenceNumber (String username, String branchCode, PredinUpdateReferenceNumberRequest updateReferenceNumberRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bds/general/api/pre-din/branches/"+branchCode+"/reference-numbers";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(updateReferenceNumberRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
		return predinRepository.updateReferencenNumber(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				username, 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				branchCode,
				updateReferenceNumberRequest);
	}

}
