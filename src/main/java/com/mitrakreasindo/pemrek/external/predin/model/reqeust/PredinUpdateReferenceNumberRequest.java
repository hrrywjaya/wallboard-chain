package com.mitrakreasindo.pemrek.external.predin.model.reqeust;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class PredinUpdateReferenceNumberRequest
{

	private String oldReferenceNumber;
	private String newReferenceNumber;
	
}
