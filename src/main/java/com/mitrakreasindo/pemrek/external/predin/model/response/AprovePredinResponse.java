package com.mitrakreasindo.pemrek.external.predin.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class AprovePredinResponse
{

	private String customerNumber;
	private String customerName;
	private String accountNumber;
	private String referenceNumber;
	private String spvId;
	private String saveId;
	private String savePhoto;
	private String saveSignature;
	private String saveSupphoto;
	
}
