package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class TransactionData {
	private String txnDate;
	private String txnCode;
	private String txnCurrencyValue;
	private String txnCurrencyCode;
	private String txnDescription;
	private String txnAmount;
	private String txnAdditionalData;
}
