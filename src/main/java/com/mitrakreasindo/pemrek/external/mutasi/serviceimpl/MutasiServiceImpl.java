package com.mitrakreasindo.pemrek.external.mutasi.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchemaV2;
import com.mitrakreasindo.pemrek.external.mutasi.repository.network.MutasiRepository;
import com.mitrakreasindo.pemrek.external.mutasi.repository.network.MutasiRepositoryV2;
import com.mitrakreasindo.pemrek.external.mutasi.service.MutasiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class MutasiServiceImpl implements MutasiService {
	
	@Autowired
	private MutasiRepository mutasiRepository;
	@Autowired
	private MutasiRepositoryV2 mutasiRepositoryV2;
	@Value("${mutasi.client-id}")
	private String mutasiClientId;
	@Value("${mutasi.server.url}")
	private String url;
	
	@Override
	public OutputSnakeCase<OutputSchema> inqTransactionData(String accountNo, String txnTotal) {
		
			return mutasiRepository.inqDataTransaction(mutasiClientId, accountNo, txnTotal);
		
	}

	@Override
	public ResponseEntity<?> inqTransactionDataRestTemplate(String accountNo, String txnTotal) throws JsonMappingException, JsonProcessingException {
		OutputSnakeCase<OutputSchema> outputMessage = new OutputSnakeCase<OutputSchema>();
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("client-id", mutasiClientId);
		HttpEntity<?> Entity = new HttpEntity<>(headers);
		try {
			ResponseEntity<String> response = restTemplate.exchange(url+"/deposits/account/"+accountNo+"/statement/"+txnTotal+"/txn", HttpMethod.GET, Entity, String.class );
			outputMessage = mapper.readValue(response.getBody(), outputMessage.getClass());
			return new ResponseEntity<>(outputMessage,response.getStatusCode());
		} catch (HttpServerErrorException e) {
			String responseBody = e.getResponseBodyAsString();
			outputMessage = mapper.readValue(responseBody, outputMessage.getClass());
			return new ResponseEntity<>(outputMessage,e.getStatusCode());
		} 
		
	}

	@Override
	public OutputSnakeCase<OutputSchemaV2> mutasiRekening(String accountNo, String startDate,
			String endDate) throws ParseException {
		String responseType = "stream";
		startDate = StringToDate(startDate);
		endDate = StringToDate(endDate);
		return mutasiRepositoryV2.inqDataTransaction(mutasiClientId, accountNo, startDate, endDate, responseType);
	}
	
	public static String StringToDate(String dob) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		Date date = formatter.parse(dob);
		String tanggal = new SimpleDateFormat("ddmmyyyy").format(date).toString();
		return tanggal;
	}

	@Override
	public OutputSnakeCase<OutputSchemaV2> mutasiRekening(String accountNo) throws ParseException {
		String responseType = "stream";
		String startDate = StringToDate(LocalDate.now().minusDays(5).toString());
		String endDate = StringToDate(LocalDate.now().toString());
//		accountNo = "0081100011";
//		String startDate = StringToDate("2020-08-26");
//		String endDate = StringToDate("2020-08-27");
//		System.out.println("start date "+startDate );
//		System.out.println("endDate "+endDate );
//		OutputSnakeCase<OutputSchemaV2> response = mutasiRepositoryV2.inqDataTransaction(mutasiClientId, accountNo, startDate, endDate, responseType);
//		response.getOutputSchema().getAccountStatement().getDetail();
//		int banyakData=response.getOutputSchema().getAccountStatement().getDetail().size();
		
		//dihapus dari index terakhir
//		for(int i=banyakData-1;i>=5;i--) {
//			response.getOutputSchema().getAccountStatement().getDetail().remove(i);
//		}
		return mutasiRepositoryV2.inqDataTransaction(mutasiClientId, accountNo, startDate, endDate, responseType);
	}
	

	
}
