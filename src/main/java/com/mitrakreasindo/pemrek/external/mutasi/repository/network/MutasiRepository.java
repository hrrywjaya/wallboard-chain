package com.mitrakreasindo.pemrek.external.mutasi.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchema;

@FeignClient(name = "mutasi-repository", url = "${mutasi.server.url}")
public interface MutasiRepository {
	
	/**
	 * mutasi
	 * @param account-no
	 * @param txn-total
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/deposits/account/{account-no}/statement/{txn-total}/txn")
	OutputSnakeCase<OutputSchema> inqDataTransaction(
			@RequestHeader("client-id") String clientId,
			@PathVariable("account-no") String accountNo,
			@PathVariable("txn-total") String txnTotal);
}
