package com.mitrakreasindo.pemrek.external.mutasi.service;

import java.text.ParseException;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchemaV2;

public interface MutasiService {

	OutputSnakeCase<OutputSchema> inqTransactionData(String accountNo,String txnTotal);
	
	ResponseEntity<?> inqTransactionDataRestTemplate(String accountNo,String txnTotal) throws JsonMappingException, JsonProcessingException;
	
	OutputSnakeCase<OutputSchemaV2> mutasiRekening(String accountNo,String startDate,String endDate) throws ParseException;
	
	OutputSnakeCase<OutputSchemaV2> mutasiRekening(String accountNo) throws ParseException;
	
}
