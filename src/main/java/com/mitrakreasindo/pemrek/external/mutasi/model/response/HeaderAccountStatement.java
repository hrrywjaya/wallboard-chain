package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class HeaderAccountStatement {
	private String beginningBalanceSign;
	private String currCode;
	private String startDate;
	private String beginningBalance;
	private String accountName;
}
