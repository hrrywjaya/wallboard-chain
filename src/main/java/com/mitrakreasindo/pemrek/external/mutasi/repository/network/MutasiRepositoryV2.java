package com.mitrakreasindo.pemrek.external.mutasi.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchemaV2;

@FeignClient(name = "mutasi-repository-v2", url = "${mutasi.server.url.v2}")
public interface MutasiRepositoryV2 {
	/**
	 * mutasi
	 * @param account-no
	 * @param start-date
	 * @param end-date
	 * @param response-type
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/deposits/account/{account-no}/statement/mq")
	OutputSnakeCase<OutputSchemaV2> inqDataTransaction(
			@RequestHeader("client_id") String clientId,
			@PathVariable("account-no") String accountNo,
			@RequestParam("start_date") String startDate,
			@RequestParam("end_date") String endDate,
			@RequestParam("response_type") String responseType);
	
}
