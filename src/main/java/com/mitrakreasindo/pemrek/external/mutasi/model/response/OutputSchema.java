package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchema {
	private Input input;
	private List<TransactionData> transactionData;
}
