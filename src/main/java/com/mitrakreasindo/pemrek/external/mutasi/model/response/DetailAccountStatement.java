package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class DetailAccountStatement {
	private String postingDate;
	private String txnName;
	private String txnCode;
	private String branch;
	private String userId;
	private String currentBalance;
	private String uniqueKey;
	private String detControl;
	private String transactionDate;
	private String currentSeq;
	private String amountTxn;
	private String debitCredit;
	private String reffNo;
	private String trailer1;
	private String trailer2;
	private String trailer3;
	private String trailer4;
	private String trailer5;
	private String trailer6;
	private String printedBackValue;
}
