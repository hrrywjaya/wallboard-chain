package com.mitrakreasindo.pemrek.external.mutasi.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.mutasi.model.response.OutputSchemaV2;
import com.mitrakreasindo.pemrek.external.mutasi.service.MutasiService;

@RestController
@RequestMapping("/capi/mutasi")
public class MutasiController {

	@Autowired
	private MutasiService mutasiService;

	@PostMapping("/r/{account-no}/{txn-total}")
	public OutputSnakeCase<OutputSchema> saveEdd(@PathVariable("account-no") String accountNo,
			@PathVariable("txn-total") String txnTotal) {
		return mutasiService.inqTransactionData(accountNo, txnTotal);
	}

	@PostMapping("/{account-no}/{txn-total}")
	public ResponseEntity<?> savemutasi(@PathVariable("account-no") String accountNo,
			@PathVariable("txn-total") String txnTotal) throws JsonMappingException, JsonProcessingException {

		return mutasiService.inqTransactionDataRestTemplate(accountNo, txnTotal);
	}

	/*
	 * @GetMapping("/{account-no}/statement/mq") public
	 * OutputSnakeCase<OutputSchemaV2> mutasiRekening(
	 * 
	 * @PathVariable("account-no") String accountNo,
	 * 
	 * @RequestParam("start_date") String startDate,
	 * 
	 * @RequestParam("end_date") String endDate){ return
	 * mutasiService.mutasiRekening(accountNo, startDate, endDate); }
	 * 
	 */
	
	@GetMapping("/{account-no}/statement/mq")
	public OutputSnakeCase<OutputSchemaV2> mutasiRekening(@PathVariable("account-no") String accountNo)
			throws ParseException {
		return mutasiService.mutasiRekening(accountNo);
	}
}
