package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.AccountCustomerUpdateKeyValue;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Input {
	private String requestCode;
	private String accountNo;
	private String inqIndicator;
}
