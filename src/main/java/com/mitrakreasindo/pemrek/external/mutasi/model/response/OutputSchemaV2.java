package com.mitrakreasindo.pemrek.external.mutasi.model.response;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaV2 {
	private String clientId;
	private String accountNo;
	private String startDate;
	private String endDate;
	private String responseType;
	private String requestId;
	private AccountStatement accountStatement;
}
