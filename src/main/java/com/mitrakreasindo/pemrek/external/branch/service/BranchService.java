package com.mitrakreasindo.pemrek.external.branch.service;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

public interface BranchService {

    OutputSnakeCase<Object> getBranch();

}