package com.mitrakreasindo.pemrek.external.branch.serviceimpl;

import com.mitrakreasindo.pemrek.external.branch.repository.network.BranchRepository;
import com.mitrakreasindo.pemrek.external.branch.service.BranchService;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BranchServiceImpl implements BranchService{

    @Autowired
	private BranchRepository branchRepository;
	@Value("${branch.client-id}")
	private String branchClientId;

    @Override
    public OutputSnakeCase<Object> getBranch() {
        return branchRepository.getBranch(branchClientId);
    }

}