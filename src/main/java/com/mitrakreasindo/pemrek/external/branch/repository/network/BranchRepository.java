package com.mitrakreasindo.pemrek.external.branch.repository.network;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "branch-repository", url = "${branch.server.url}")
public interface BranchRepository {

    @RequestMapping(method = RequestMethod.GET, value = "/branch/v2")
    OutputSnakeCase<Object> getBranch(
        @RequestHeader("client-id") String clientId
    );

}