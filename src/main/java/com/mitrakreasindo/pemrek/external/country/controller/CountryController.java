package com.mitrakreasindo.pemrek.external.country.controller;

import com.mitrakreasindo.pemrek.external.country.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.country.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/capi/countries")
public class CountryController {
    
    @Autowired
    private CountryService countryService;
    
    @GetMapping
    public OutputSnakeCase<OutputSchema> getCountry() {
        return countryService.getCountry();
    }

}