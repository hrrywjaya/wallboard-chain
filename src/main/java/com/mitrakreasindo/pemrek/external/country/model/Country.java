package com.mitrakreasindo.pemrek.external.country.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Country {

    private CountryCode countryCode;
    private CountryName countryName;
    private String highRiskIndicator;
    
}