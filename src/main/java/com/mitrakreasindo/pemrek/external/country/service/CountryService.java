package com.mitrakreasindo.pemrek.external.country.service;

import com.mitrakreasindo.pemrek.external.country.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

public interface CountryService {

    OutputSnakeCase<OutputSchema> getCountry();
    
}