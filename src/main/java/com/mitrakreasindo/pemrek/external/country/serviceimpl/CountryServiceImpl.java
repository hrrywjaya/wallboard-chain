package com.mitrakreasindo.pemrek.external.country.serviceimpl;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.country.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.country.repository.network.CountryRepository;
import com.mitrakreasindo.pemrek.external.country.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService {

    @Value("${country.client-id}")
    private String countryClientId;
    @Autowired
    private CountryRepository countryRepository;

    @Override
    public OutputSnakeCase<OutputSchema> getCountry() {
        return countryRepository.getCountry(countryClientId);
    }
    
}