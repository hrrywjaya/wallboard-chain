package com.mitrakreasindo.pemrek.external.country.model;

import lombok.Data;

@Data
public class CountryCode {

    private String iso;
    private String lbu;
    
}