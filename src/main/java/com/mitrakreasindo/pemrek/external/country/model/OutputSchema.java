package com.mitrakreasindo.pemrek.external.country.model;

import lombok.Data;

@Data
public class OutputSchema {

    private OutputCountry countries;
    
}