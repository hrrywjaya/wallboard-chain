package com.mitrakreasindo.pemrek.external.country.repository.network;

import com.mitrakreasindo.pemrek.external.country.model.OutputSchema;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

@FeignClient(name = "country-repository", url = "${country.server.url}")
public interface CountryRepository {

    @RequestMapping(method = RequestMethod.GET, value = "/countries/v2/")
    OutputSnakeCase<OutputSchema> getCountry(
        @RequestHeader("client-id") String clientId
    );
    
}