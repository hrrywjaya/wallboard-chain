package com.mitrakreasindo.pemrek.external.country.model;

import java.util.List;

import lombok.Data;

@Data
public class OutputCountry {

    private List<Country> country;
    
}