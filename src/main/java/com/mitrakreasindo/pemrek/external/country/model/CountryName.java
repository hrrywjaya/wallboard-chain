package com.mitrakreasindo.pemrek.external.country.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CountryName {

    @JsonProperty("short")
    private String shortName;
    @JsonProperty("long")
    private String longName;
    
}