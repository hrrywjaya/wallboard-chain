package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UpdateReferenceNumberAccountType {

    TAHAPAN_EXPRESI("X"), TAHAPAN("T"), TAHAPAN_GOLD("G");

    private final String value;

    private UpdateReferenceNumberAccountType(String value) {
        this.value = value;
    }

    @JsonCreator
    public static UpdateReferenceNumberAccountType forValue(String value) {
        if (value != null) {
            for (UpdateReferenceNumberAccountType type : UpdateReferenceNumberAccountType.values()) {
                if (type.equals(value))
                    return type;
            }
        }
        return null;
    }
    
    @JsonValue
    public String toString() {
        return value;
    }

}