package com.mitrakreasindo.pemrek.external.bcacoid.service;

import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;

public interface BcaCoidService {
    
    // MerchantTokenResponse generateMerchanToken(String credentialId,MerchantTokenRequest request);

    UpdateStatusResponse updateAccountStatus(UpdateStatusRequest request);

    UpdateStatusResponse updateAccountStatus(UpdateStatusRequest request, String username);

    UpdateReferenceNumberResponse updateReferenceNumber(UpdateReferenceNumberRequest request);

    UpdateReferenceNumberResponse updateReferenceNumber(UpdateReferenceNumberRequest request, String username);

}