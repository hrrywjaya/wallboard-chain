package com.mitrakreasindo.pemrek.external.bcacoid.controller;

import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.service.BcaCoidService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/bca-coid")
public class BcaCoIdController {

    @Autowired
    private BcaCoidService bcaCoidService;

    @PutMapping("/status")
    public UpdateStatusResponse updateAccountStatus(@RequestBody UpdateStatusRequest request) {
        return bcaCoidService.updateAccountStatus(request);
    }

    @PutMapping("/reference")
    UpdateReferenceNumberResponse updateReferenceNumber(@RequestBody UpdateReferenceNumberRequest request) {
        return bcaCoidService.updateReferenceNumber(request);
    }

}