package com.mitrakreasindo.pemrek.external.bcacoid.model.response;

import lombok.Data;

@Data
public class UpdateStatusResponse
{

	private String status;
	private String reason;
	
}
