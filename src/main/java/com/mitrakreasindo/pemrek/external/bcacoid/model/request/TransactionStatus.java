package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransactionStatus {

    REJECTED("0"), SUCCESS("1"), RETRY("2"), ONLOCK("3");

    private final String value;

    private TransactionStatus(String value) {
        this.value = value;
    }

    @JsonCreator
    public static TransactionStatus forValue(String value) {
        if (value != null) {
            for (TransactionStatus accountType : TransactionStatus.values()) {
                if (accountType.equals(value))
                    return accountType;
            }
        }
        return null;
    }
    
    @JsonValue
    public String toString() {
        return value;
    }

}