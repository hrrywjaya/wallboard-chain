package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(SnakeCaseStrategy.class)
public class MerchantTokenRequest {

    private String userId;
    private String applicationId;
    private String activityId;
    private String email;
    private String mobilePhone;
    private String fullName;

}