package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class UpdateStatusRequest
{

	public static String TRANSACTION_STATUS_DITOLAK = "0";
	public static String TRANSACTION_STATUS_BERHASIL = "1";
	public static String TRANSACTION_STATUS_ULANG = "2";
	public static String TRANSACTION_STATUS_ON_LOCK = "3";
	
	private String transactionStatus;
	private String reffNumber;
	private String hpNumber;
	private String accNumber;
	private String accType;
	private String debitcardName;
	private String debitcardNumber;
	private String debitcardExpdate;
	private String debitcardType;
	private String branchType;
	private String branchName;
	private String updateOfficer;
	
}
