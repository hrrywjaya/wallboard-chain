package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AccountType {

    TAHAPAN_EXPRESI("108"), TAHAPAN_OR_TAHAPAN_GOLD("110");

    private final String value;

    private AccountType(String value) {
        this.value = value;
    }
    
    @JsonCreator
    public static AccountType forValue(String value) {
        if (value != null) {
            for (AccountType accountType : AccountType.values()) {
                if (accountType.equals(value))
                    return accountType;
            }
        }
        return null;
    }
    
    @JsonValue
    public String toString() {
        return value;
    }

}