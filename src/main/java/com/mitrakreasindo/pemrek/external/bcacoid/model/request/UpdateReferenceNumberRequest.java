package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class UpdateReferenceNumberRequest
{

	private String reffNumber;
	private String hpNumber;
	private String newReffNumber;
	private String openaccType;
	private String accountType;
	private String updateOfficer;
	
}
