package com.mitrakreasindo.pemrek.external.bcacoid.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(SnakeCaseStrategy.class)
public class MerchantTokenResponse {

    private String verificationId;
    private String applicantId;
    private String urlPageview;

}