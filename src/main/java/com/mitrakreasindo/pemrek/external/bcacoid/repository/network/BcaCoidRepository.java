package com.mitrakreasindo.pemrek.external.bcacoid.repository.network;

import com.mitrakreasindo.pemrek.external.bcacoid.model.request.MerchantTokenRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.MerchantTokenResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "bcacoid-repository", url = "${bcacoid.server.url}")
public interface BcaCoidRepository {
    
    // @RequestMapping(method=RequestMethod.POST, value="/bca-co-id/open-account-marketplace/api/merchants/tokens/generate")
    // MerchantTokenResponse generateMerchanToken(
    //     @RequestHeader("credential-id") String credentialId,
    //     @RequestBody MerchantTokenRequest request
    // );

    @RequestMapping(method=RequestMethod.PUT, value="/bca-co-id/open-account-marketplace/api/status")
    UpdateStatusResponse updateAccountStatus(
        @RequestHeader("app-id") String appId,
        @RequestHeader("user-domain") String userDomain,
        @RequestHeader("user-id") String userId,
        @RequestHeader("Origin") String Origin,
        @RequestHeader("Authorization") String authorization,
        @RequestHeader("X-BCA-Key") String xBcaKey,
        @RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
        @RequestHeader("X-BCA-Signature") String xBcaSignature,
        @RequestHeader("client-id") String clientId,
        @RequestBody UpdateStatusRequest request
    );


    @RequestMapping(method=RequestMethod.PUT, value="/bca-co-id/open-account-marketplace/api/references")
    UpdateReferenceNumberResponse updateReferenceNumber(
        @RequestHeader("app-id") String appId,
        @RequestHeader("user-domain") String userDomain,
        @RequestHeader("user-id") String userId,
        @RequestHeader("Origin") String Origin,
        @RequestHeader("Authorization") String authorization,
        @RequestHeader("X-BCA-Key") String xBcaKey,
        @RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
        @RequestHeader("X-BCA-Signature") String xBcaSignature,
        @RequestHeader("client-id") String clientId,
        @RequestBody UpdateReferenceNumberRequest request
    );

}