package com.mitrakreasindo.pemrek.external.bcacoid.serviceimpl;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.repository.network.BcaCoidRepository;
import com.mitrakreasindo.pemrek.external.bcacoid.service.BcaCoidService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BcaCoidServiceImpl implements BcaCoidService {

    @Autowired
    private BcaCoidRepository bcaCoidRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${din.user-id}")
	private String userId;
	@Value("${bcacoid.client-id}")
	private String clientId;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private AccountService accountService;

    // @Override
    // public MerchantTokenResponse generateMerchanToken(String credentialId, MerchantTokenRequest request) {
    //     Account account = userService.getCurrentUser();
	// 	GatewayToken token = gatewayService.getToken();		
	// 	String httpMethod = "POST";
	// 	String relativeUrl = "/bds/general/api/pre-din/branches/"+account.getBranchCode();	
	// 	String time = OffsetDateTime.now().toString();
	// 	String json = "";
	// 	try
	// 	{
	// 		json = mapper.writeValueAsString(request);
	// 	} catch (JsonProcessingException e)
	// 	{
	// 		e.printStackTrace();
	// 	}		
	// 	String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
	// 	return predinRepository.savePredin(
	// 			"014D2CF3CDC3AF6F4F92C09190860E33", 
	// 			userService.getCurrentUser().getUsername(), 
	// 			userId, 
	// 			"localhost.com", 
	// 			"Bearer "+token.getAccessToken(), 
	// 			gatewayApiKey, 
	// 			time, 
	// 			signature, 
	// 			account.getBranchCode(),
	// 			request);
    //     return bcaCoidRepository.generateMerchanToken(credentialId, request);
    // }
	
	@Override
    public UpdateStatusResponse updateAccountStatus(UpdateStatusRequest request) {
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bca-co-id/open-account-marketplace/api/status";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
        return bcaCoidRepository.updateAccountStatus(
            "014D2CF3CDC3AF6F4F92C09190860E33", 
            accountService.getCurrentUser().getUsername(), 
            userId, 
            "localhost.com", 
            "Bearer "+token.getAccessToken(), 
            gatewayApiKey, 
            time, 
			signature,
			clientId, 
            request
            );
    }

    @Override
    public UpdateStatusResponse updateAccountStatus(UpdateStatusRequest request, String username) {
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bca-co-id/open-account-marketplace/api/status";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
        return bcaCoidRepository.updateAccountStatus(
            "014D2CF3CDC3AF6F4F92C09190860E33", 
            username, 
            userId, 
            "localhost.com", 
            "Bearer "+token.getAccessToken(), 
            gatewayApiKey, 
            time, 
            signature, 
			clientId,
            request
            );
    }

	@Override
    public UpdateReferenceNumberResponse updateReferenceNumber(UpdateReferenceNumberRequest request) {
        GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bca-co-id/open-account-marketplace/api/references";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
        return bcaCoidRepository.updateReferenceNumber(
            "014D2CF3CDC3AF6F4F92C09190860E33", 
			accountService.getCurrentUser().getUsername(),
            userId, 
            "localhost.com", 
            "Bearer "+token.getAccessToken(), 
            gatewayApiKey, 
            time, 
            signature, 
			clientId,
            request
            );
    }

    @Override
    public UpdateReferenceNumberResponse updateReferenceNumber(UpdateReferenceNumberRequest request, String username) {
        GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/bca-co-id/open-account-marketplace/api/references";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
		
        return bcaCoidRepository.updateReferenceNumber(
            "014D2CF3CDC3AF6F4F92C09190860E33", 
			username,
            userId, 
            "localhost.com", 
            "Bearer "+token.getAccessToken(), 
            gatewayApiKey, 
            time, 
            signature, 
			clientId,
            request
            );
    }


}