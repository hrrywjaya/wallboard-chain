package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UpdateReferenceNumberOpenAccountType {

    FIRST_ACCOUNT("1"), SECOND_ACCOUNT("2");

    private final String value;

    private UpdateReferenceNumberOpenAccountType(String value) {
        this.value = value;
    }

    @JsonCreator
    public static UpdateReferenceNumberOpenAccountType forValue(String value) {
        if (value != null) {
            for (UpdateReferenceNumberOpenAccountType type : UpdateReferenceNumberOpenAccountType.values()) {
                if (type.equals(value))
                    return type;
            }
        }
        return null;
    }
    
    @JsonValue
    public String toString() {
        return value;
    }

}