package com.mitrakreasindo.pemrek.external.bcacoid.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DebitCardType {

    SILVER("S"), GOLD("G"), PLATINUM("M");

    private final String value;

    private DebitCardType(String value) {
        this.value = value;
    }

    @JsonCreator
    public static DebitCardType forValue(String value) {
        if (value != null) {
            for (DebitCardType type : DebitCardType.values()) {
                if (type.equals(value))
                    return type;
            }
        }
        return null;
    }
    
    @JsonValue
    public String toString() {
        return value;
    }

}