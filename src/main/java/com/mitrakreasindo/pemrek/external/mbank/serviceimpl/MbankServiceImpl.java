package com.mitrakreasindo.pemrek.external.mbank.serviceimpl;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.mbank.respository.network.MbankRepository;
import com.mitrakreasindo.pemrek.external.mbank.service.MbankService;

@Service
public class MbankServiceImpl implements MbankService
{

	@Autowired
	private MbankRepository mbankRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	public UpdateReferenceNumberResponse updateReferenceNumber (UpdateReferenceNumberRequest updateReferenceNumberRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/mobile-banking/open-account-data/api/reference-numbers";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(updateReferenceNumberRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}	
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());		
		
		return mbankRepository.updateReferenceNumber(
				"localhost", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				updateReferenceNumberRequest);
	}
	
	@Override
	public UpdateStatusResponse updateStatus (UpdateStatusRequest updateStatusRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/mobile-banking/open-account-data/api/status";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(updateStatusRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}	
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());		
		
		return mbankRepository.updateStatus(
				"localhost", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				updateStatusRequest);
	}

}
