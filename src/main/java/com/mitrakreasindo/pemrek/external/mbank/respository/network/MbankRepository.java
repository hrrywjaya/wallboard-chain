package com.mitrakreasindo.pemrek.external.mbank.respository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateStatusResponse;

@FeignClient(name = "mbank-repository", url = "${mbank.server.url}")
public interface MbankRepository
{

	/**
	 * update reference number
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param updateReferenceNumberRequest
	 * @return
	 */
	@RequestMapping(method=RequestMethod.PUT, value="/mobile-banking/open-account-data/api/reference-numbers")
	UpdateReferenceNumberResponse updateReferenceNumber (
			@RequestHeader("Origin") String Origin, 
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey, 
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestBody UpdateReferenceNumberRequest updateReferenceNumberRequest);
	
	
	@RequestMapping(method=RequestMethod.PUT, value="/mobile-banking/open-account-data/api/status")
	UpdateStatusResponse updateStatus(
			@RequestHeader("Origin") String Origin, 
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey, 
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestBody UpdateStatusRequest updateStatusRequest);

}
