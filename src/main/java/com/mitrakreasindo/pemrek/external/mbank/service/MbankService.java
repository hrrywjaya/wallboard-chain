package com.mitrakreasindo.pemrek.external.mbank.service;

import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateStatusResponse;

public interface MbankService
{

	UpdateReferenceNumberResponse updateReferenceNumber(UpdateReferenceNumberRequest updateReferenceNumberRequest);
	
	UpdateStatusResponse updateStatus(UpdateStatusRequest updateStatusRequest);
	
}
