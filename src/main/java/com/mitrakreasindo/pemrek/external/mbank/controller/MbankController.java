package com.mitrakreasindo.pemrek.external.mbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.mbank.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.mbank.service.MbankService;

@RestController
@RequestMapping("/capi/m-bank")
public class MbankController
{

	@Autowired
	private MbankService mbankService;
	
	@PutMapping("/reference-number")
	public UpdateReferenceNumberResponse updateReferenceNumber (@RequestBody UpdateReferenceNumberRequest updateReferenceNumberRequest) {
		return mbankService.updateReferenceNumber(updateReferenceNumberRequest);
	}
	
	@PutMapping("/status")
	public UpdateStatusResponse updateStatus (@RequestBody UpdateStatusRequest updateStatusRequest) {
		return mbankService.updateStatus(updateStatusRequest);
	}
	
}
