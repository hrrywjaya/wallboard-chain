package com.mitrakreasindo.pemrek.external.kafka.serviceimpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.acme.avro.pemol_updatestatus_avro;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.kafka.model.KafkaTransactionStatus;
import com.mitrakreasindo.pemrek.external.kafka.model.KafkaTrxModelProjection;
import com.mitrakreasindo.pemrek.external.kafka.service.KafkaService;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.repository.CallLogRepository;

import lombok.Getter;
import lombok.Setter;


@Service
@Getter
@Setter
public class KafkaServiceImpl implements KafkaService {

	@Autowired
	private CallLogRepository callLogRepository;

    @Autowired
	private JdbcTemplate jdbcChainTemplate;
	
	@Autowired
    private ObjectMapper mapper;
   
    @Autowired
	private KafkaTemplate<String, pemol_updatestatus_avro> kafkaTemplate;
	
	@Value("${service.kafka.topic.dest}")
	private String destTopic;

	@Value("${kafka.sendTransactionPendingFirstBatch}")
	private String kafkaFirstBatch;

	@Value("${kafka.sendTransactionPendingSecondBatch}")
	private String kafkaSecondBatch;
	
	private String runningStatus;

	@Override
	public void listTrxBatch1() {
		String today = getCurrentDate();
		String[] time = kafkaFirstBatch.split("-");
		sendMessageKafka(today, time[0], time[1]);
	}

	@Override
	public void listTrxBatch2() {
		String today = getCurrentDate();
		String[] time = kafkaSecondBatch.split("-");
		sendMessageKafka(today, time[0], time[1]);
	}
	
	public String getCurrentDate(){
		String date = LocalDate.now().minusDays(1).toString();
      	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      	try {
			Date tanggal = formatter.parse(date);
			date = new SimpleDateFormat("yyyy-MM-dd").format(tanggal).toString();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		System.out.println("ini tanggal sekarang : "+ date);
		return date;
	}
    
    public List<KafkaTransactionStatus> findPendingTransaction(String date,String startDate,String endDate){
        String sqlQuery = "SELECT cl.id AS callLogId,\n"+ 
        "tr.id AS transactionId,\n" + 
        "cl.NO_REF AS noRef,\n" + 
        "cl.TRANSAKSI_STATUS AS transactionStatus,\n" + 
		"cl.QR_NOMOR_HP AS noHp,\n" +
		"cl.old_raw_konfirmasi_data AS oldRawKonfirmasiData,\n" + 
        "cl.raw_konfirmasi_data AS rawKonfirmasiData,\n" + 
        "cl.UPDATED_BY AS updateBy,\n" + 
        "cl.CREATED_DATE AS createdDate\n" + 
        "FROM CALL_LOG cl\n" + 
        "INNER JOIN TRANSAKSI tr \n" + 
        "ON cl.TRANSAKSI_ID = tr.ID \n" + 
        "AND cl.CREATED_DATE >= TO_DATE('"+date+" "+startDate+"','yyyy-mm-dd hh24:mi:ss') \n" + 
        "AND cl.CREATED_DATE < TO_DATE('"+date+" "+endDate+"','yyyy-mm-dd hh24:mi:ss')\n" + 
        "WHERE cl.TRANSAKSI_STATUS = 'PENDING' \n";;

        List<KafkaTrxModelProjection> result = jdbcChainTemplate.query(
            sqlQuery, 
            (rs,rowNum) -> new KafkaTrxModelProjection(
            	rs.getString("callLogId"),	
                rs.getString("transactionId"), 
                rs.getString("noRef"), 
                rs.getString("transactionStatus"), 
				rs.getString("noHp"),
				rs.getString("oldRawKonfirmasiData"),
				rs.getString("rawKonfirmasiData"),
                rs.getString("updateBy"), 
                rs.getTimestamp("createdDate")));
        
        List<KafkaTransactionStatus> resultList = result.stream().map(trx -> {
			JsonNode konfirmasiData = parseKonfirmasiData(trx.getOldRawKonfirmasiData(), trx.getRawKonfirmasiData());
            KafkaTransactionStatus trxStatus = new KafkaTransactionStatus(
            	trx.getCallLogId(),	
                "0",
                trx.getNoRef(),
                getStringContent(konfirmasiData, "nomorHp"),
                trx.getUpdateBy(), 
                checkTypeChannel(trx.getNoRef()));
            return trxStatus;
        }).collect(Collectors.toList());

        return resultList;
    }

    public String checkTypeChannel(String noRef){
        if ('A' == noRef.charAt(0)) {
            return "bcacoid";
        }else{
            return "bcamobile";
        }
    }
    public void updateTransaction(String id){

		CallLog callLog = callLogRepository.findById(id).orElse(null);
		if(callLog.getId() != null){
			callLog.setTransaksiStatus("Ditolak");
			callLogRepository.save(callLog);
		}

	}
    public void sendMessageKafka(String date,String startDate,String endDate) {
    	
    	int timeOutMillis = 10000;
		List<KafkaTransactionStatus> result = findPendingTransaction(date,startDate,endDate);
		ListenableFuture<SendResult<String, pemol_updatestatus_avro>> future;
		SendResult<String, pemol_updatestatus_avro> resultSend;
		StringBuffer sbf = new StringBuffer();
    	if(!result.isEmpty()){
			for(KafkaTransactionStatus trx: result) {
				/**
				 * generating message
				 */
				pemol_updatestatus_avro msgs = pemol_updatestatus_avro.newBuilder()
												.setTransactionStatus(trx.getTransactionStatus())
												.setReffNumber(trx.getReffNumber())
												.setHpNumber(trx.getHpNumber())
												.setUpdateOfficer(trx.getUpdateOfficer())
												.setChannel(trx.getChannel())
												.build();
				/**
				 * send message
				 */
				try {
					future = kafkaTemplate.send(destTopic, msgs) ;
					resultSend = future.get(timeOutMillis, TimeUnit.MILLISECONDS);
					
					sbf.append("##  SUCCESS ##" + "\n")
					
						.append("Topic")
							.append(": " + resultSend.getRecordMetadata().topic() + "\n")
							
						.append("transaction status")
							.append(": " + trx.getTransactionStatus() + "\n")
						
						.append("reff number")
							.append(": " + trx.getReffNumber() + "\n")
						
						.append("hp number")
							.append(": " + trx.getHpNumber() + "\n")
						
						.append("update officer")
							.append(": " + trx.getUpdateOfficer() + "\n")
						
						.append("channel")
							.append(": " + trx.getChannel() + "\n")
							
						.append("partition")
							.append(": " + resultSend.getRecordMetadata().partition() + "\n")
							
						.append("offset")
							.append(": " + resultSend.getRecordMetadata().offset() + "\n");
					
					System.out.println(sbf.toString());
					sbf.delete(0, sbf.length());
					this.setRunningStatus("Berhasil");
					updateTransaction(trx.getCallLogId());
				} catch (InterruptedException | ExecutionException | TimeoutException ex) {
					ex.printStackTrace();

					sbf.append("##  FAILED ##" + "\n")
					
						.append("LocalizedMessage")
							.append(": " + ex.getLocalizedMessage() + "\n")
							
						.append("Message")
							.append(": " + ex.getMessage() + "\n")
						
						.append("reff number")
							.append(": " + trx.getReffNumber() + "\n")
							
						.append("Exception")
							.append(": " + ex.toString() + "\n");
					
					System.out.println(sbf.toString());
					sbf.delete(0, sbf.length());
					this.setRunningStatus("Tidak Berhasil");
				}
				
			}
		}
    }

	@Override
	public List<KafkaTransactionStatus> listTrxBatchTesting(String tanggal) {
		String[] time = kafkaFirstBatch.split("-");
		List<KafkaTransactionStatus> res = findPendingTransaction(tanggal, time[0], time[1]);
		sendMessageKafka(tanggal, time[0], time[1]);
		return res;
	}

	private JsonNode parseKonfirmasiData(String oldRawKonfirmasiData, String rawKonfirmasiData) {
    	if (oldRawKonfirmasiData == null && oldRawKonfirmasiData == null)
    		return null;
    	try {
            return mapper.readTree(rawKonfirmasiData != null ? rawKonfirmasiData : oldRawKonfirmasiData);
        } catch (JsonMappingException e) {
            System.out.println("failed parsing konfirmasi data");
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            System.out.println("failed parsing konfirmasi data");
            e.printStackTrace();
        }
        return null;        
	}

	private String getStringContent(JsonNode jsonNode, String field) {
    	return jsonNode == null ? "" : jsonNode.get(field).asText();
    }

}