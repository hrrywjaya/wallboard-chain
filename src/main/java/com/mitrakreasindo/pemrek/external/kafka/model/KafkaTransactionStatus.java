package com.mitrakreasindo.pemrek.external.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaTransactionStatus {
	private String callLogId;
    private String transactionStatus;
    private String reffNumber;
    private String hpNumber;
    private String updateOfficer;
    private String channel;

}