package com.mitrakreasindo.pemrek.external.kafka.service;

import java.util.List;

import com.mitrakreasindo.pemrek.external.kafka.model.KafkaTransactionStatus;

public interface KafkaService {
    List<KafkaTransactionStatus> listTrxBatchTesting(String tanggal);
    void listTrxBatch1();
    void listTrxBatch2();
}