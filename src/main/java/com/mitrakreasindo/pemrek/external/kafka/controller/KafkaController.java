package com.mitrakreasindo.pemrek.external.kafka.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.mitrakreasindo.pemrek.external.kafka.model.KafkaTransactionStatus;
import com.mitrakreasindo.pemrek.external.kafka.service.KafkaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/kafka")
public class KafkaController {

    @Autowired
    KafkaService kafkaService;

    @GetMapping("/trxData")
    public List<KafkaTransactionStatus> getTrxData(@RequestParam("date") String waktu){
        String date = LocalDate.now().minusDays(1).toString();
      	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      	try {
			Date tanggal = formatter.parse(date);
			date = new SimpleDateFormat("yyyy-MM-dd").format(tanggal).toString();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		//System.out.println("ini tanggal sekarang : "+ date);
        return kafkaService.listTrxBatchTesting(waktu);

    }
}