package com.mitrakreasindo.pemrek.external.kafka.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaTrxModelProjection {
	private String callLogId;
    private String transactionId;
    private String noRef;
    private String transactionStatus;
    private String nomorHp;
    private String oldRawKonfirmasiData;
    private String rawKonfirmasiData;
    private String updateBy;
    private Timestamp createDate;
}