package com.mitrakreasindo.pemrek.external.ektp.service;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;

public interface PekerjaanService
{
	
	// OutputEktp getEktpByNik (String nik);
	
	/**
	 * 
	 * @param nik
	 * @param location / branch code
	 * @return
	 */
  // OutputEktp getEktpByNik (String nik, String location);
  
  Object getPekerjaan(String categoryCode);

  Object getPekerjaanAll();

}
