package com.mitrakreasindo.pemrek.external.ektp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputEktp
{
	
	private ErrorSchema errorSchema;
	private OutputSchema outputSchema;

}
