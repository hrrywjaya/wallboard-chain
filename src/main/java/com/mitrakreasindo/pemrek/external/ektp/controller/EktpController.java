package com.mitrakreasindo.pemrek.external.ektp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;
import com.mitrakreasindo.pemrek.external.ektp.service.EktpService;

@RestController
@RequestMapping("/capi/ektp")
public class EktpController
{
	
	@Autowired
	private EktpService ektpService;

	@GetMapping
	public OutputEktp getEktp(
			@RequestParam("nik") String nik) {
		return ektpService.getEktpByNik(nik);
	}
		
}
