package com.mitrakreasindo.pemrek.external.ektp.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;

@FeignClient(name = "ekt-repository", url = "${ektp.server.url}")
public interface EktpRepository
{
	
	/**
	 * get ektp data
	 * @param clientId
	 * @param idNumber
	 * @param applicationId
	 * @param userId
	 * @param location
	 * @param ipSource
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/ektp/v2/{id-number}")
	OutputEktp getEktp (
			@RequestHeader("client-id") String clientId, 
			@PathVariable("id-number") String idNumber, 
			@RequestParam("user-id") String userId,
			@RequestParam("location") String location, 
			@RequestParam("ip-source") String ipSource);
	
}
