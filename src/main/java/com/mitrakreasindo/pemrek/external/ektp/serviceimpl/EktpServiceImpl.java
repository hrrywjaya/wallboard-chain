package com.mitrakreasindo.pemrek.external.ektp.serviceimpl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;
import com.mitrakreasindo.pemrek.external.ektp.repository.network.EktpRepository;
import com.mitrakreasindo.pemrek.external.ektp.repository.network.PekerjaanRepository;
import com.mitrakreasindo.pemrek.external.ektp.service.EktpService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class EktpServiceImpl implements EktpService
{
	
	@Autowired
  private EktpRepository ektpRepoitory;
	@Autowired
	private AccountService userService;
	@Value("${ektp.client-id}")
	private String ektpClientId;
	@Value("${ektp.user-id}")
	private String ektpUserId;

	@Override
	public OutputEktp getEktpByNik (String nik)
	{
		String ipAddress = "";
		try
		{
			InetAddress inet = InetAddress.getLocalHost();
			ipAddress = inet.getHostAddress();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		return ektpRepoitory.getEktp(ektpClientId, nik, ektpUserId, userService.getCurrentUser().getBranchCode(), ipAddress);
	}
	
	@Override
	public OutputEktp getEktpByNik (String nik, String location)
	{
		String ipAddress = "";
		try
		{
			InetAddress inet = InetAddress.getLocalHost();
			ipAddress = inet.getHostAddress();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		return ektpRepoitory.getEktp(ektpClientId, nik, ektpUserId, location, ipAddress);
	}

  // @Override
  // public Object getPekerjaan(String categoryCode) {
  //   String ipAddress = "";
	// 	try
	// 	{
	// 		InetAddress inet = InetAddress.getLocalHost();
	// 		ipAddress = inet.getHostAddress();
	// 	} catch (UnknownHostException e)
	// 	{
	// 		e.printStackTrace();
	// 	}
	// 	return pekerjaanRepository.getPekerjaan(categoryCode);
  // }

}
