package com.mitrakreasindo.pemrek.external.ektp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchema
{

	private String idNumber;
	private Address address;
	private PersonalInformation personalInformation;
	private MaritalInformation maritalInformation;
	private String ektpCreated;
	private String ektpLocalId;
	private String ektpStatus;
	private String photo;
	private String hostAddress;
	
}
