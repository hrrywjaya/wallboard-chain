package com.mitrakreasindo.pemrek.external.ektp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class PersonalInformation
{

	private String fullName;
	private String religion;
	private String latestEducation;
	private String sex;
	private String occupation;
	private String kkNumber;
	private String motherName;
	private BirthInformation birthInformation; 
	
}
