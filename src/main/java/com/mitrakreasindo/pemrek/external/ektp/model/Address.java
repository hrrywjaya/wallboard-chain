package com.mitrakreasindo.pemrek.external.ektp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Address
{

	private String street;
	private String backwoods;
	private String rt;
	private String rw;
	private String villageNumber;
	private String villageName;
	private String subdistrictNumber;
	private String subdistrictName;
	private String provinceNumber;
	private String provinceName;
	private String regencyNumber;
	private String regencyName;
	private String postCode;
	
}
