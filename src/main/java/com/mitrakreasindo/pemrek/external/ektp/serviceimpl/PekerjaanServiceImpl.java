package com.mitrakreasindo.pemrek.external.ektp.serviceimpl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;
import com.mitrakreasindo.pemrek.external.ektp.repository.network.EktpRepository;
import com.mitrakreasindo.pemrek.external.ektp.repository.network.PekerjaanRepository;
import com.mitrakreasindo.pemrek.external.ektp.service.EktpService;
import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class PekerjaanServiceImpl implements PekerjaanService
{
	
	@Autowired
  private EktpRepository ektpRepoitory;
  @Autowired
  private PekerjaanRepository pekerjaanRepository;
	@Autowired
	private AccountService userService;
	@Value("${ektp.client-id}")
	private String ektpClientId;
	@Value("${ektp.user-id}")
	private String ektpUserId;

  @Override
  public Object getPekerjaan(String categoryCode) {
    String ipAddress = "";
		try
		{
			InetAddress inet = InetAddress.getLocalHost();
			ipAddress = inet.getHostAddress();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		return pekerjaanRepository.getPekerjaan(ektpClientId, categoryCode);
  }

  @Override
  public Object getPekerjaanAll() {
    String ipAddress = "";
		try
		{
			InetAddress inet = InetAddress.getLocalHost();
			ipAddress = inet.getHostAddress();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		return pekerjaanRepository.getPekerjaanAll(ektpClientId);
  }

}
