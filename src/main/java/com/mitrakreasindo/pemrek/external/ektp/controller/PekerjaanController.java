package com.mitrakreasindo.pemrek.external.ektp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;
import com.mitrakreasindo.pemrek.external.ektp.service.EktpService;
import com.mitrakreasindo.pemrek.external.ektp.service.PekerjaanService;

@RestController
@RequestMapping("/capi/pekerjaan")
public class PekerjaanController
{
  @Autowired
  private ObjectMapper obj;
	@Autowired
	private PekerjaanService pekerjaanService;

	@GetMapping("/{categoryCode}")
	public Object getPekerjaan(
			@PathVariable("categoryCode") String categoryCode) {
		return pekerjaanService.getPekerjaan(categoryCode);
	}
  @GetMapping("/all")
	public Object getPekerjaanAll()
			{
		return pekerjaanService.getPekerjaanAll();
  }
}
