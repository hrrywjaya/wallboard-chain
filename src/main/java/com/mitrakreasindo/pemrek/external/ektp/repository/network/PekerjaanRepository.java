package com.mitrakreasindo.pemrek.external.ektp.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.ektp.model.OutputEktp;

@FeignClient(name = "pekerjaan-repository", url = "${daftar.pekerjaan.url}")
public interface PekerjaanRepository
{
	
	/**
	 * get ektp data
	 * @param clientId
	 * @param idNumber
	 * @param applicationId
	 * @param userId
	 * @param location
	 * @param ipSource
	 * @return
	 */
  
	@RequestMapping(method = RequestMethod.GET, value = "/form-category/v2/category-code/{category-code}")
	Object getPekerjaan (
			@RequestHeader("client-id") String clientId, 
			@PathVariable("category-code") String categoryCode);
			// @RequestParam("user-id") String userId,
			// @RequestParam("location") String location, 
      // @RequestParam("ip-source") String ipSource);
      

    @RequestMapping(method = RequestMethod.GET, value = "/form-category/v2")
	Object getPekerjaanAll (
			@RequestHeader("client-id") String clientId);
	
}
