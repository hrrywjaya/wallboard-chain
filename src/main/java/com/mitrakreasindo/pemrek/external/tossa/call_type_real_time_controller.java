package com.mitrakreasindo.pemrek.external.tossa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

@RestController
@RequestMapping("/capi/calltyperealtime")
public class call_type_real_time_controller {
	static final String Path_expr="jdbc:sqlserver://10.20.234.84;databaseName=pcce_awdb";
	static final String service_user = "cuicview";
	static final String service_password = "1234567890";
	
	
	
	@GetMapping("/getcallready")
	public int gettimeready()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code="); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timeready=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timeready;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timeready=e.getErrorCode();
	        	
	        	
	        	return mc.timeready;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timeready=0;
   	
   	
   	return mc.timeready;
	}
	
	@GetMapping("/getmeeting")
	public int getmeeting()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code="); //meetingcode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timejobrutin=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timejobrutin;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timejobrutin=e.getErrorCode();
	        	
	        	
	        	return mc.timejobrutin;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timejobrutin=0;
   	
   	
   	return mc.timejobrutin;
	}
	
	
	@GetMapping("/getotherreason")
	public int getotherreason()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code=5737"); //meetingcode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timeotherreason=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timeotherreason;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timeotherreason=e.getErrorCode();
	        	
	        	
	        	return mc.timeotherreason;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timeotherreason=0;
   	
   	
   	return mc.timeotherreason;
	}
	
	@GetMapping("/gethold")
	public Iterable<model_call_type_realtime> gethold()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(HandleTimeTo5) as countas, count(HandleTimeToday) as countas2  from Agent_Real_Time "); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        List<model_call_type_realtime> mc1 = new ArrayList<model_call_type_realtime>();
		        	mc.holdtimeto5=Cursor1.getInt("countas");
		        	mc.holdtimetoday=Cursor1.getInt("countas2");
		        	mc1.add(mc);
		        	
		        	
		        	return mc1;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    		model_call_type_realtime mc = new model_call_type_realtime();
		        List<model_call_type_realtime> mc1 = new ArrayList<model_call_type_realtime>();
		        	mc.holdtimeto5=e.getErrorCode();
		        	mc.holdtimetoday=e.getErrorCode();
		        	mc1.add(mc);
		        	
		        	
		        	return mc1;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
        List<model_call_type_realtime> mc1 = new ArrayList<model_call_type_realtime>();
        	mc.holdtimeto5=0;
        	mc.holdtimetoday=0;
        	mc1.add(mc);
        	
        	
        	return mc1;
	}
	
	
	@GetMapping("/getjobrutin")
	public int getjobrutin()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code=55228"); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timejobrutin=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timejobrutin;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timejobrutin=e.getErrorCode();
	        	
	        	
	        	return mc.timejobrutin;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timejobrutin=0;
   	
   	
   	return mc.timejobrutin;
	}
	
	@GetMapping("/getcallistirahat")
	public int gettimeistirahat()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code=5098"); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timeistirahat=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timeistirahat;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timeistirahat=e.getErrorCode();
	        	
	        	
	        	return mc.timeistirahat;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timeistirahat=0;
   	
   	
   	return mc.timeistirahat;
	}
	
	
	@GetMapping("/getcallibadah")
	public int gettimeibadah()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code=6138"); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timeibadah=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timeibadah;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timeibadah=e.getErrorCode();
	        	
	        	
	        	return mc.timeibadah;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timeibadah=0;
   	
   	
   	return mc.timeibadah;
	}
	
	
	@GetMapping("/getcalltoilet")
	public int gettimetoilet()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(DateTimeLastStateChange) as countas from Agent_Real_Time where reason_code=45560"); //readyreasoncode?
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.timetoilet=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.timetoilet;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	    	  mc.timetoilet=e.getErrorCode();
	        	
	        	
	        	return mc.timetoilet;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
       
		mc.timetoilet=0;
   	
   	
   	return mc.timetoilet;
	}
	
	
	
	@GetMapping("/getcallofered")
	public int getcalloefered()
	{
		
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(CallsOfferedToday) as countas from Call_Type_Real_Time where cast([DateTime] as Date) = cast(getdate() as Date) ");
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.callofered=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.callofered;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  model_call_type_realtime mc = new model_call_type_realtime();
		        
	        	mc.callofered=e.getErrorCode();
	        	
	        	
	        	return mc.callofered;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
        
    	mc.callofered=0;
    	
    	
    	return mc.callofered;
	}
	
	@GetMapping("/getnoanscall")
	public int getnoanscall()
	{
		
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("select count(RedirectNoAnsCallsTo5) as countas from Skill_Group_Real_Time where cast([DateTime] as Date) = cast(getdate() as Date) ");
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.redirectnoanscall=Cursor1.getInt("countas");
		        	
		        	
		        	return mc.redirectnoanscall;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
     		model_call_type_realtime mc = new model_call_type_realtime();
mc.redirectnoanscall=e.getErrorCode();
    	return mc.redirectnoanscall;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
		mc.redirectnoanscall=0;
		     	return mc.redirectnoanscall;
	}
	
	
	@GetMapping("/getabandon")
	public int abandongrincall()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(Path_expr, service_user, service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          (" SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" + 
	          		"SELECT  Interval = SGI.DateTime,\r\n" + 
	          		"		DATE = CONVERT(char(10),SGI.DateTime,101), \r\n" + 
	          		"		FullName = Skill_Group.EnterpriseName,\r\n" + 
	          		"		SkillGroupSkillID = Skill_Group.SkillTargetID,\r\n" + 
	          		"		CallbackMessages = SUM(ISNULL(SGI.CallbackMessages,0)),\r\n" + 
	          		"		CallbackMessagesTime = SUM(ISNULL(SGI.CallbackMessagesTime,0)),\r\n" + 
	          		"		AvgHandledCallsTalkTime = AVG(ISNULL(SGI.AvgHandledCallsTalkTime,0)),\r\n" + 
	          		"		HoldTime = SUM(ISNULL(SGI.HoldTime,0)),\r\n" + 
	          		"		HandledCallsTalkTime = SUM(ISNULL(SGI.HandledCallsTalkTime,0)),\r\n" + 
	          		"		InternalCalls = SUM(ISNULL(SGI.InternalCalls,0)),\r\n" + 
	          		"		InternalCallsTime = SUM(ISNULL(SGI.InternalCallsTime,0)),\r\n" + 
	          		"		CallsHandled = SUM(ISNULL(SGI.CallsHandled,0)),\r\n" + 
	          		"		SupervAssistCalls = SUM(ISNULL(SGI.SupervAssistCalls,0)),\r\n" + 
	          		"		AvgHandledCallsTime = AVG(ISNULL(SGI.AvgHandledCallsTime,0)),\r\n" + 
	          		"		SupervAssistCallsTime = SUM(ISNULL(SGI.SupervAssistCallsTime,0)),\r\n" + 
	          		"		HandledCallsTime = SUM(ISNULL(SGI.HandledCallsTime,0)),\r\n" + 
	          		"		AgentOutCallsTime = SUM(ISNULL(SGI.AgentOutCallsTime,0)),\r\n" + 
	          		"		TalkInTime = SUM(ISNULL(SGI.TalkInTime,0)),\r\n" + 
	          		"		LoggedOnTime = SUM(ISNULL(SGI.LoggedOnTime,0)),\r\n" + 
	          		"		ExternalOut = SUM(ISNULL(SGI.AgentOutCalls,0)),\r\n" + 
	          		"		TalkOutTime = SUM(ISNULL(SGI.TalkOutTime,0)),\r\n" + 
	          		"		TalkOtherTime = SUM(ISNULL(SGI.TalkOtherTime,0)),\r\n" + 
	          		"		AvailTime = SUM(ISNULL(SGI.AvailTime,0)),\r\n" + 
	          		"		NotReadyTime = SUM(ISNULL(SGI.NotReadyTime,0)),\r\n" + 
	          		"		TransferInCalls = SUM(ISNULL(SGI.TransferInCalls,0)),\r\n" + 
	          		"		TalkTime = SUM(ISNULL(SGI.TalkTime,0)),\r\n" + 
	          		"		TransferInCallsTime = SUM(ISNULL(SGI.TransferInCallsTime,0)),\r\n" + 
	          		"		WorkReadyTime = SUM(ISNULL(SGI.WorkReadyTime,0)),\r\n" + 
	          		"		TransferOutCalls = SUM(ISNULL(SGI.TransferOutCalls,0)),\r\n" + 
	          		"		WorkNotReadyTime = SUM(ISNULL(SGI.WorkNotReadyTime,0)),\r\n" + 
	          		"		BusyOtherTime = SUM(ISNULL(SGI.BusyOtherTime,0)),\r\n" + 
	          		"		CallsAnswered = SUM(ISNULL(SGI.CallsAnswered,0)),\r\n" + 
	          		"		ReservedStateTime = SUM(ISNULL(SGI.ReservedStateTime,0)),\r\n" + 
	          		"		AnswerWaitTime = SUM(ISNULL(SGI.AnswerWaitTime,0)),\r\n" + 
	          		"		AbandonRingCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)),\r\n" + 
	          		"		AbandonRingTime = SUM(ISNULL(SGI.AbandonRingTime,0)),\r\n" + 
	          		"		AbandonHoldCalls = SUM(ISNULL(SGI.AbandonHoldCalls,0)),\r\n" + 
	          		"		AgentOutCallsTalkTime = SUM(ISNULL(SGI.AgentOutCallsTalkTime,0)),\r\n" + 
	          		"		AgentOutCallsOnHold = SUM(ISNULL(SGI.AgentOutCallsOnHold,0)),\r\n" + 
	          		"		AgentOutCallsOnHoldTime = SUM(ISNULL(SGI.AgentOutCallsOnHoldTime,0)),\r\n" + 
	          		"		AgentTerminatedCalls = SUM(ISNULL(SGI.AgentTerminatedCalls,0)),\r\n" + 
	          		"		ConsultativeCalls = SUM(ISNULL(SGI.ConsultativeCalls,0)),\r\n" + 
	          		"		ConsultativeCallsTime = SUM(ISNULL(SGI.ConsultativeCallsTime,0)),\r\n" + 
	          		"		ConferencedInCalls = SUM(ISNULL(SGI.ConferencedInCalls,0)),\r\n" + 
	          		"		ConferencedInCallsTime = SUM(ISNULL(SGI.ConferencedInCallsTime,0)),\r\n" + 
	          		"		ConferencedOutCalls = SUM(ISNULL(SGI.ConferencedOutCalls,0)),\r\n" + 
	          		"		ConferencedOutCallsTime = SUM(ISNULL(SGI.ConferencedOutCallsTime,0)),\r\n" + 
	          		"		IncomingCallsOnHoldTime = SUM(ISNULL(SGI.IncomingCallsOnHoldTime,0)),\r\n" + 
	          		"		IncomingCallsOnHold = SUM(ISNULL(SGI.IncomingCallsOnHold,0)),\r\n" + 
	          		"		InternalCallsOnHoldTime = SUM(ISNULL(SGI.InternalCallsOnHoldTime,0)),\r\n" + 
	          		"		InternalCallsOnHold = SUM(ISNULL(SGI.InternalCallsOnHold,0)),\r\n" + 
	          		"		InternalCallsRcvdTime = SUM(ISNULL(SGI.InternalCallsRcvdTime,0)),\r\n" + 
	          		"		InternalCallsRcvd = SUM(ISNULL(SGI.InternalCallsRcvd,0)),\r\n" + 
	          		"		RedirectNoAnsCalls = SUM(ISNULL(SGI.RedirectNoAnsCalls,0)),\r\n" + 
	          		"		RedirectNoAnsCallsTime = SUM(ISNULL(SGI.RedirectNoAnsCallsTime,0)),\r\n" + 
	          		"		ShortCalls = SUM(ISNULL(SGI.ShortCalls,0)),\r\n" + 
	          		"		RouterCallsAbandQ = SUM(ISNULL(SGI.RouterCallsAbandQ,0)),\r\n" + 
	          		"		RouterQueueCalls = SUM(ISNULL(SGI.RouterQueueCalls,0)),\r\n" + 
	          		"		AutoOutCalls = SUM(ISNULL(SGI.AutoOutCalls,0)),\r\n" + 
	          		"		AutoOutCallsTime = SUM(ISNULL(SGI.AutoOutCallsTime,0)),\r\n" + 
	          		"		AutoOutCallsTalkTime = SUM(ISNULL(SGI.AutoOutCallsTalkTime,0)),\r\n" + 
	          		"		AutoOutCallsOnHold = SUM(ISNULL(SGI.AutoOutCallsOnHold,0)),\r\n" + 
	          		"		AutoOutCallsOnHoldTime = SUM(ISNULL(SGI.AutoOutCallsOnHoldTime,0)),\r\n" + 
	          		"		PreviewCalls = SUM(ISNULL(SGI.PreviewCalls,0)),\r\n" + 
	          		"		PreviewCallsTime = SUM(ISNULL(SGI.PreviewCallsTime,0)),\r\n" + 
	          		"		PreviewCallsTalkTime = SUM(ISNULL(SGI.PreviewCallsTalkTime,0)),\r\n" + 
	          		"		PreviewCallsOnHold = SUM(ISNULL(SGI.PreviewCallsOnHold,0)),\r\n" + 
	          		"		PreviewCallsOnHoldTime = SUM(ISNULL(SGI.PreviewCallsOnHoldTime,0)),\r\n" + 
	          		"		ReserveCalls = SUM(ISNULL(SGI.ReserveCalls,0)),\r\n" + 
	          		"		ReserveCallsTime = SUM(ISNULL(SGI.ReserveCallsTime,0)),\r\n" + 
	          		"		ReserveCallsTalkTime = SUM(ISNULL(SGI.ReserveCallsTalkTime,0)),\r\n" + 
	          		"		ReserveCallsOnHold = SUM(ISNULL(SGI.ReserveCallsOnHold,0)),\r\n" + 
	          		"		ReserveCallsOnHoldTime = SUM(ISNULL(SGI.ReserveCallsOnHoldTime,0)),\r\n" + 
	          		"		TalkAutoOutTime = SUM(ISNULL(SGI.TalkAutoOutTime,0)),\r\n" + 
	          		"		TalkPreviewTime = SUM(ISNULL(SGI.TalkPreviewTime,0)),\r\n" + 
	          		"		TalkReserveTime = SUM(ISNULL(SGI.TalkReserveTime,0)),\r\n" + 
	          		"		BargeInCalls = SUM(ISNULL(SGI.BargeInCalls,0)),\r\n" + 
	          		"		InterceptCalls = SUM(ISNULL(SGI.InterceptCalls,0)),\r\n" + 
	          		"		MonitorCalls = SUM(ISNULL(SGI.MonitorCalls,0)),\r\n" + 
	          		"		WhisperCalls = SUM(ISNULL(SGI.WhisperCalls,0)),\r\n" + 
	          		"		EmergencyAssists = SUM(ISNULL(SGI.EmergencyAssists,0)),\r\n" + 
	          		"		CallsOffered = SUM(ISNULL(SGI.CallsHandled,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)) + SUM(ISNULL(SGI.AbandonRingCalls,0))+ SUM(ISNULL(SGI.RedirectNoAnsCalls,0)),\r\n" + 
	          		"		CallsQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)), \r\n" + 
	          		"		InterruptedTime = SUM(ISNULL(SGI.InterruptedTime,0)),\r\n" + 
	          		"		TimeZone = SGI.TimeZone,\r\n" + 
	          		"		RouterCallsOffered = SUM(ISNULL(SGI.RouterCallsOffered,  0)),  \r\n" + 
	          		"		RouterCallsAgentAbandons = SUM(ISNULL(SGI.RouterCallsAbandToAgent,0)),   \r\n" + 
	          		"		RouterCallsDequeued = SUM(ISNULL(SGI.RouterCallsDequeued,0)),   \r\n" + 
	          		"		RouterError = SUM(ISNULL(SGI.RouterError,0)),  \r\n" + 
	          		"		DoNotUseSLTop = CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
	          		"			WHEN 1 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 \r\n" + 
	          		"			WHEN 2 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0\r\n" + 
	          		"			WHEN 3 THEN (sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 \r\n" + 
	          		"			ELSE 0 END,\r\n" + 
	          		"		DoNotUseSLBottom = CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
	          		"			WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END\r\n" + 
	          		"			WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
	          		"			WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0))- sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
	          		"			ELSE 0 END,\r\n" + 
	          		"		ServicelLevel=CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
	          		"			WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE	\r\n" + 
	          		"			sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 /\r\n" + 
	          		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END\r\n" + 
	          		"			WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE\r\n" + 
	          		"			sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 /\r\n" + 
	          		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
	          		"			WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE\r\n" + 
	          		"			(sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 /\r\n" + 
	          		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
	          		"			ELSE 0 END, \r\n" + 
	          		"		ServiceLevelCalls = SUM(ISNULL(SGI.ServiceLevelCalls,0)),   \r\n" + 
	          		"		ServiceLevelCallsAband = SUM(ISNULL(SGI.ServiceLevelCallsAband,  0)),  \r\n" + 
	          		"		ServiceLevelCallsDequeue = SUM(ISNULL(SGI.ServiceLevelCallsDequeue,  0)),  \r\n" + 
	          		"		ServiceLevelError =  SUM(ISNULL(SGI.ServiceLevelError,0)),   \r\n" + 
	          		"		ServiceLevelRONA = SUM(ISNULL(SGI.ServiceLevelRONA,0)),   \r\n" + 
	          		"		ServiceLevelCallsOffered = SUM(ISNULL(SGI.ServiceLevelCallsOffered,0)),\r\n" + 
	          		"		NetConsultativeCalls = sum(isnull(SGI.NetConsultativeCalls,   0)),\r\n" + 
	          		"		NetConsultativeCallsTime = SUM(ISNULL(SGI.NetConsultativeCallsTime,0)), \r\n" + 
	          		"		NetConferencedOutCalls = sum(isnull( SGI.NetConferencedOutCalls,0)),\r\n" + 
	          		"		NetConfOutCallsTime = SUM(ISNULL(SGI.NetConfOutCallsTime,0)),\r\n" + 
	          		"		NetTransferredOutCalls = sum(isnull(SGI.NetTransferOutCalls,0)),\r\n" + 
	          		"		DbDateTime = SGI.DbDateTime,\r\n" + 
	          		"                ReportingInterval = SUM(ISNULL(SGI.ReportingInterval,0))*60,\r\n" + 
	          		"		fte_AgentsLogonTotal = SUM(ISNULL(SGI.LoggedOnTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsNotReady = SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsNotActive = SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsActive = SUM(ISNULL(SGI.TalkTime,0)) * 1.0 /(SUM(ISNULL(SGI.ReportingInterval,0))*60),\r\n" + 
	          		"		fte_AgentsWrapup = SUM(ISNULL(SGI.WorkReadyTime,0)+ISNULL(SGI.WorkNotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsOther = SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsHold = SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
	          		"		fte_AgentsReserved = SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),\r\n" + 
	          		"		ast_PercentNotActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.TalkTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentHoldTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentWrapTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE (SUM(ISNULL(SGI.WorkNotReadyTime + SGI.WorkReadyTime,0))) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentReservedTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentNotReadyTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		ast_PercentUtilization = \r\n" + 
	          		"		CASE WHEN (SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) = 0 THEN 0		\r\n" + 
	          		"				ELSE (CASE WHEN SUM(ISNULL(SGI.TalkTime,0)) = 0 THEN 0 ELSE (SUM(ISNULL(SGI.TalkInTime,0)) + \r\n" + 
	          		"					 SUM(ISNULL(SGI.TalkOutTime,0)) + \r\n" + 
	          		"					 SUM(ISNULL(SGI.TalkOtherTime,  0)) + \r\n" + 
	          		"					 SUM(ISNULL(SGI.WorkReadyTime,0)) + \r\n" + 
	          		"					 SUM(ISNULL(SGI.WorkNotReadyTime,0))) * 1.0 /\r\n" + 
	          		"			  		(SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) END) END,\r\n" + 
	          		"		asa =  CASE WHEN SUM(ISNULL(SGI.CallsAnswered,0)) = 0 THEN 0 \r\n" + 
	          		"					ELSE SUM(ISNULL(SGI.AnswerWaitTime,0)) * 1.0 / SUM(ISNULL(SGI.CallsAnswered,0)) END,\r\n" + 
	          		"		CompletedTasks_AHT =  (CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.HandledCallsTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END),\r\n" + 
	          		"		CompletedTasks_AvgActiveTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0 \r\n" + 
	          		"										ELSE SUM(ISNULL(SGI.HandledCallsTalkTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END,\r\n" + 
	          		"		CompletedTasks_AvgWrapTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0 \r\n" + 
	          		"										ELSE (SUM(ISNULL(SGI.WorkReadyTime,0)) + SUM(ISNULL(SGI.WorkNotReadyTime,0))) / SUM(ISNULL(SGI.CallsHandled,0)) END, \r\n" + 
	          		"		ast_ActiveTime = SUM(ISNULL(SGI.TalkTime,0)),\r\n" + 
	          		"		TotalQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)) + SUM(ISNULL(SGI.CallsQueued,0)),\r\n" + 
	          		"		ast_PerBusyOtherTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
	          		"			   ELSE SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
	          		"		Media = Media_Routing_Domain.EnterpriseName,\r\n" + 
	          		"		MRDomainID = Media_Routing_Domain.MRDomainID,\r\n" + 
	          		"		AbandCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)),\r\n" + 
	          		"		RouterMaxCallsQueued = MAX(SGI.RouterMaxCallsQueued),\r\n" + 
	          		"		RouterMaxCallWaitTime = MAX(SGI.RouterMaxCallWaitTime)\r\n" + 
	          		"   FROM Skill_Group (nolock), \r\n" + 
	          		"		Skill_Group_Interval SGI (nolock), \r\n" + 
	          		"		Media_Routing_Domain (nolock)  \r\n" + 
	          		"  WHERE (DATEPART(dw, SGI.DateTime) in(2,3,4,5,6,7,1) and SGI.DateTime between '2019-10-31 00:00:00' and '2019-10-31 23:59:59' and convert([char], SGI.DateTime, 108) between '00:00:00' and '23:59:59') and (Skill_Group.SkillTargetID IN (5029)) and (Skill_Group.SkillTargetID = SGI.SkillTargetID)  \r\n" + 
	          		"         and (Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID)    \r\n" + 
	          		"GROUP BY Skill_Group.EnterpriseName,   \r\n" + 
	          		"		 Skill_Group.SkillTargetID,\r\n" + 
	          		"		 Media_Routing_Domain.EnterpriseName,\r\n" + 
	          		"		 Media_Routing_Domain.MRDomainID,\r\n" + 
	          		"		 SGI.DateTime,\r\n" + 
	          		"		 CONVERT(char(10),SGI.DateTime,101),\r\n" + 
	          		"		 SGI.TimeZone,\r\n" + 
	          		"		 SGI.DbDateTime\r\n" + 
	          		"ORDER BY Media_Routing_Domain.EnterpriseName,\r\n" + 
	          		"		 Skill_Group.EnterpriseName, \r\n" + 
	          		"		 SGI.DateTime  ");
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		model_call_type_realtime mc = new model_call_type_realtime();
		        
		        	mc.abandonringcall=Cursor1.getInt("AbandonRingCalls");
		        	
		        	
		        	return mc.abandonringcall;
		        	
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
      		model_call_type_realtime mc = new model_call_type_realtime();
mc.abandonringcall=e.getErrorCode();
     	return mc.abandonringcall;
	    
	      }
	
		model_call_type_realtime mc = new model_call_type_realtime();
		mc.abandonringcall=0;
		     	return mc.abandonringcall;
	}
	
	
	
	
	
}
