package com.mitrakreasindo.pemrek.external.tossa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

@RestController
@RequestMapping("/capi/callserviceagenrealtime")
public class AGENTREALTIMECONTROLLER {
	SERVICE_CONNECTION SQLCONNECTION = new SERVICE_CONNECTION();

	@GetMapping("/getcountagentrealtime")
	public int getcountagentrealtime()
	{int hasilcount;
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(SQLCONNECTION.Path_expr, SQLCONNECTION.service_user, SQLCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("SELECT count(SkillTargetID)AS callcount FROM Agent_Real_Time" );
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		hasilcount=Cursor1.getInt("callcount");
	        		return hasilcount;
		        	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	 hasilcount=e.getErrorCode();
	    
	      }
	
		hasilcount=0;
		return hasilcount;
	}
	
	@GetMapping("/getagenttextcode")
	public Iterable <MODELAGENTREALTIME> getagenttextcode()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(SQLCONNECTION.Path_expr, SQLCONNECTION.service_user, SQLCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("   SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON\r\n" + 
	          		";WITH AgentNotReadyDetails(SkillTargetID, MRDomainID, LoginDateTime, Event, Duration, ReasonCode, DateTime, DbDateTime, TimeZone) AS\r\n" + 
	          		"(Select aed.SkillTargetID, \r\n" + 
	          		"	   aed.MRDomainID, \r\n" + 
	          		"	  CONVERT(varchar, aed.LoginDateTime,120),  \r\n" + 
	          		"	   aed.Event, \r\n" + 
	          		"	   aed.Duration, \r\n" + 
	          		"	   aed.ReasonCode, 	\r\n" + 
	          		"	   DateTime,			\r\n" + 
	          		"	   aed.DbDateTime,\r\n" + 
	          		"	   aed.TimeZone	   \r\n" + 
	          		"From Agent_Event_Detail aed  (nolock)\r\n" + 
	          		"Where CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime) \r\n" + 
	          		"					ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \r\n" + 
	          		"					+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\r\n" + 
	          		"					+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" + 
	          		"					\r\n" + 
	          		"						+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" + 
	          		"								THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2)\r\n" + 
	          		"								 ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END)\r\n" + 
	          		"						+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" + 
	          		"									  WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" + 
	          		"									  WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" + 
	          		"									  ELSE '30' END )\r\n" + 
	          		"					    + ':00' END >= '2019-10-31 00:00:00'\r\n" + 
	          		"  AND CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime) \r\n" + 
	          		"					ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,aed.DateTime)) \r\n" + 
	          		"						+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\r\n" + 
	          		"						+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" + 
	          		"						+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" + 
	          		"								 THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2)\r\n" + 
	          		"								 ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END)\r\n" + 
	          		"						+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" + 
	          		"									  WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" + 
	          		"									  WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" + 
	          		"									  ELSE '30' END )\r\n" + 
	          		"					    + ':00' END < '2019-10-31 23:59:59'\r\n" + 
	          		"  AND SkillTargetID IN (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)),\r\n" + 
	          		"  \r\n" + 
	          		"  AgentLogOutDetails(ALODSkillTargetID, ALODLoginDateTime, TotalLoginTime, ALODEvent) AS\r\n" + 
	          		"(Select aed.SkillTargetID,  \r\n" + 
	          		"	   aed.LoginDateTime, \r\n" + 
	          		"	   TotalLoginTime = DATEDIFF(ss,LoginDateTime, DateTime),\r\n" + 
	          		"	   aed.Event\r\n" + 
	          		"From AgentNotReadyDetails aed \r\n" + 
	          		"WHERE Event = 2),\r\n" + 
	          		"		 \r\n" + 
	          		"AgentNRDuration(SkillTargetID, MRDomainID, LoginDateTime, ReasonCodeDuration, ReasonCode) AS\r\n" + 
	          		"(Select ANRD.SkillTargetID, \r\n" + 
	          		"	    ANRD.MRDomainID, \r\n" + 
	          		"		ANRD.LoginDateTime, \r\n" + 
	          		"		ReasonCodeDuration = SUM (ANRD.Duration),\r\n" + 
	          		"		ANRD.ReasonCode\r\n" + 
	          		"   From AgentNotReadyDetails ANRD \r\n" + 
	          		"  Where ANRD.Event = 3\r\n" + 
	          		"  Group By ANRD.SkillTargetID, \r\n" + 
	          		"		   ANRD.MRDomainID, \r\n" + 
	          		"		   ANRD.LoginDateTime,\r\n" + 
	          		"		   ANRD.ReasonCode),  \r\n" + 
	          		"		   \r\n" + 
	          		"AgentNotReadyTotal(SkillTargetID, NotReadyTime,LoginDateTime) AS\r\n" + 
	          		"(Select SkillTargetID,\r\n" + 
	          		"		SUM(ReasonCodeDuration),\r\n" + 
	          		"		LoginDateTime\r\n" + 
	          		"	From AgentNRDuration\r\n" + 
	          		"	GROUP BY SkillTargetID,\r\n" + 
	          		"		LoginDateTime)\r\n" + 
	          		"\r\n" + 
	          		"SELECT SkillTargetID = ANRDU.SkillTargetID, \r\n" + 
	          		"	   LoginDateTime = ANRDU.LoginDateTime,\r\n" + 
	          		"           LoginDate = cast(ANRDU.LoginDateTime as date),\r\n" + 
	          		"	   	  \r\n" + 
	          		"	   	   TotalLoginTime = CASE WHEN ALOD.ALODEvent = 2 THEN ALOD.TotalLoginTime \r\n" + 
	          		"	   							 ELSE 0 END,\r\n" + 
	          		"	   TotalNotReadyTime = AgentNotReadyTotal.NotReadyTime,\r\n" + 
	          		"	   ReasonCode = ANRDU.ReasonCode, \r\n" + 
	          		"	   textReasonCode = ISNULL(Reason_Code.ReasonText, ' ')+'['+convert(varchar, ANRDU.ReasonCode)+']', \r\n" + 
	          		"	   ReasonCodeDuration = ANRDU.ReasonCodeDuration, \r\n" + 
	          		"           FullName = Person.LastName + ', ' + Person.FirstName,\r\n" + 
	          		"	   perNotReady = CASE WHEN ISNULL(AgentNotReadyTotal.NotReadyTime,0) = 0 THEN 0*1.0 ELSE ISNULL(ANRDU.ReasonCodeDuration,0)*1.0/ISNULL(AgentNotReadyTotal.NotReadyTime,0) END,\r\n" + 
	          		"	   perLogon = CASE WHEN ALOD.ALODEvent = 2 THEN (ANRDU.ReasonCodeDuration*1.0/ALOD.TotalLoginTime) ELSE 0 END, TotalAgentLoginTime=(select sum(DATEDIFF(ss,LoginDateTime, DateTime)) from AgentNotReadyDetails aed\r\n" + 
	          		"WHERE Event = 2 AND aed.SkillTargetID=ANRDU.SkillTargetID)\r\n" + 
	          		"FROM AgentNRDuration ANRDU \r\n" + 
	          		"	 LEFT JOIN Reason_Code ON ANRDU.ReasonCode=Reason_Code.ReasonCode\r\n" + 
	          		"     LEFT JOIN  AgentLogOutDetails ALOD ON (ANRDU.SkillTargetID = ALOD.ALODSkillTargetID\r\n" + 
	          		"										AND ANRDU.LoginDateTime <= DATEADD(ss, 2, ALOD.ALODLoginDateTime) AND ANRDU.LoginDateTime >= DATEADD(ss, -2, ALOD.ALODLoginDateTime))\r\n" + 
	          		"	 LEFT JOIN AgentNotReadyTotal ON (ANRDU.SkillTargetID = AgentNotReadyTotal.SkillTargetID AND ANRDU.LoginDateTime = AgentNotReadyTotal.LoginDateTime),\r\n" + 
	          		"	 Person (nolock),\r\n" + 
	          		"	 Agent (nolock),\r\n" + 
	          		"	 Media_Routing_Domain\r\n" + 
	          		"WHERE Agent.PersonID = Person.PersonID \r\n" + 
	          		"  AND Agent.SkillTargetID = ANRDU.SkillTargetID\r\n" + 
	          		"  AND Media_Routing_Domain.MRDomainID = ANRDU.MRDomainID \r\n" + 
	          		"ORDER BY FullName, \r\n" + 
	          		"		 Media_Routing_Domain.EnterpriseName, \r\n" + 
	          		"		 LoginDateTime, \r\n" + 
	          		"		 textReasonCode" );
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	        		MODELAGENTREALTIME c = new MODELAGENTREALTIME();
		        	c.textreasoncode=Cursor1.getString("textReasonCode");
		        	c.reasoncode=Cursor1.getInt("ReasonCode");
		        	c1.add(c);
		        	return c1; 	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1

	      }
	      catch (SQLException e)
	      {
	    	  MODELAGENTREALTIME c = new MODELAGENTREALTIME();
     	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
     	c.textreasoncode=e.getMessage();
     	System.out.print(e.getErrorCode());
     	c1.add(c);
     	return c1;
	    
	      }
	
		MODELAGENTREALTIME c = new MODELAGENTREALTIME();
	      	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	     	c.textreasoncode="";
	      	c1.add(c);
	      	return c1;
	}
	

	@GetMapping("/gettime")
	public Iterable<MODELAGENTREALTIME>gettimes()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(SQLCONNECTION.Path_expr, SQLCONNECTION.service_user, SQLCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)	
	          ("SELECT datetimelaststatechange,datetimelogin from Agent_Real_Time " );
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	        		MODELAGENTREALTIME c = new MODELAGENTREALTIME();
		        	c.login=Cursor1.getDate("datetimelaststatechange");
		        	c.ganti=Cursor1.getDate("datetimelogin");
		        	c1.add(c);
		        	return c1;	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        
	      }
	      catch (SQLException e)
	      {
	    	  MODELAGENTREALTIME c = new MODELAGENTREALTIME();
    	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
    	c.login=null;
    	c.ganti=null;
    	System.out.print(e.getMessage());
    	c1.add(c);
    	return c1;

	      }
	
		 MODELAGENTREALTIME c = new MODELAGENTREALTIME();
	    	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	    	c.login=null;
	    	c.ganti=null;
	      	c1.add(c);
	      	return c1;
	}
	
	
	@GetMapping("/getagentrealtime")
	public Iterable <MODELAGENTREALTIME> getagentrealtime()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(SQLCONNECTION.Path_expr, SQLCONNECTION.service_user, SQLCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	        	
	          ("SELECT count(SkillTargetID)AS callcount FROM Agent_Real_Time" );
	        
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        		List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	        		MODELAGENTREALTIME c = new MODELAGENTREALTIME();
		        	c.SkillTargetID=Cursor1.getInt("callcount");
		        	c1.add(c);
		        	return c1;	
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	      }
	      catch (SQLException e)
	      {
	    	  MODELAGENTREALTIME c = new MODELAGENTREALTIME();
     	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
     	c.SkillTargetID=e.getErrorCode();
     	System.out.print(e.getErrorCode());
     	c1.add(c);
     	return c1;
	    
	      }
	
		MODELAGENTREALTIME c = new MODELAGENTREALTIME();
	      	List<MODELAGENTREALTIME> c1 = new ArrayList<MODELAGENTREALTIME>();
	      	c.SkillTargetID=0;
	      	c1.add(c);
	      	return c1;
	}
	
}
