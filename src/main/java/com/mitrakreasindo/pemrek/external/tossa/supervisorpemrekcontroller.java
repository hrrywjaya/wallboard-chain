package com.mitrakreasindo.pemrek.external.tossa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/callpemrekspv")
public class supervisorpemrekcontroller {
	//oracleserviceconnection
	SERVICE_CONNECTION_CHAIN CHAINCONNECTION = new SERVICE_CONNECTION_CHAIN();
	MODEL_PARAMETER_RONA rona = new MODEL_PARAMETER_RONA();
	
	@GetMapping("/getcounttransaksiberhasilspv")
	public int getcountcalltransactionberhasilspv ()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT count(status) as totalstatus from pemrek where status='BERHASIL' AND created_date=CURRENT_DATE ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  int hasil = Cursor1.getInt("totalstatus");
	           return hasil;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	
	@GetMapping("/getcounttransaksiditolakspv")
	public int getcountcalltransactionditolakspv ()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT count(status) as totalstatus from pemrek where status='DITOLAK' AND created_date=CURRENT_DATE ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  int hasil = Cursor1.getInt("totalstatus");
	           return hasil;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	
	public int getcountavgtime(int count_transaction_id)
	{
		int totalcount, start, end,  selisih, rumus;
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("select COUNT(start_time) as totstart,COUNT(end_time) as totend  from Call_Transaction where created_date=CURRENT_DATE  ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  
	        	  start=Cursor1.getInt("totstart");
	        	  end=Cursor1.getInt("totend");
	        	  
	        	  selisih = end-start;
	        	  rumus = count_transaction_id/selisih;
	           return rumus;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	@GetMapping("/getavgtime")
	public int getcountaverage()
	{
		int totalcount, start, end,  selisih, rumus;
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT SUM(call_transaction_id) as tot from Call_Transaction where created_date=CURRENT_DATE  ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  totalcount = Cursor1.getInt("tot");
	        	
	        	  getcountavgtime(totalcount);
	        	  
	        	 
	           return totalcount;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	
	@GetMapping("/getcountpanggilanterputus")
	public int getcountpanggilanterputusspv()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT count(call_history_id) as totalabandon from call_history where created_date=CURRENT_DATE ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  int hasil = Cursor1.getInt("totalabandon");
	        	  rona.param_rona=hasil;
	           return hasil;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	
	
	@GetMapping("/getcounttransaksiterputusspv")
	public int getcountcalltransactionterputusspv()
	{
		try
	     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
	        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT count(status) as totalstatus from pemrek where status='TERPUTUS' AND created_date=CURRENT_DATE ");
	        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      
	        	  int hasil = Cursor1.getInt("totalstatus");
	           return hasil;
	          }
	          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
	      }
	      catch (SQLException e)
	      {
	    	  int hasil;
	    	  hasil = e.getErrorCode();
	    	  return hasil;
	      }
		int hasil =0;
		return hasil;
	}
	
	

}
