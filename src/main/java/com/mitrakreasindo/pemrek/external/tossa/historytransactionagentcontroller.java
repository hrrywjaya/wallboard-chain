package com.mitrakreasindo.pemrek.external.tossa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/callhistorytransactionagent")
public class historytransactionagentcontroller {
//oracleserviceconnection
	SERVICE_CONNECTION_CHAIN CHAINCONNECTION = new SERVICE_CONNECTION_CHAIN();
	public int param_rona;

	@GetMapping("/loadalldata")
	public Iterable <modelhistory> gettransaksiberhasil ()
	{
		
		try
     {
	        Connection Connection1 = DriverManager.getConnection(CHAINCONNECTION.Path_expr, CHAINCONNECTION.service_user, CHAINCONNECTION.service_password);
        Statement Connected_Expression1 = Connection1.createStatement(); // Create_Expression (Connection1);
	        ResultSet Cursor1 = Connected_Expression1.executeQuery // Evaluate (Connected_Expression1)
	          ("SELECT pemrek.reference_Number,call_transaction.Start_Time,call_transaction.End_Time, pemrek.cis_number, pemrek.account_no,pemrek.transaction_type,pemrek.product,pemrek.status,pemrek.note,account.username,account.branch_code from pemrek, call_transaction,account where pemrek.reference_number=call_transaction.reference_number and   pemrek.account_no=account.id");
        {
	          while (Cursor1.next()) // while there_is_next_record_in (Cursor1)
	          {      int durasi =Cursor1.getInt("call_transaction.End_Time")-Cursor1.getInt("call_transaction.Start_Time");
	        	  List<modelhistory> collectionpemrek= new ArrayList<modelhistory>();
	        	  modelhistory RecordObjectpemrek= new modelhistory(); //deklarasi RecordObject1
	        	  RecordObjectpemrek.reference_number=Cursor1.getString("pemrek.reference_Number");
	        	  RecordObjectpemrek.start_time=Cursor1.getDate("call_transaction.Start_Time");
	        	  RecordObjectpemrek.end_time=Cursor1.getDate("call_transaction.End_Time");
	        	  RecordObjectpemrek.cis_number=Cursor1.getInt("pemrek.cis_number");
	        	  RecordObjectpemrek.account_no=Cursor1.getString("pemrek.account_no");
	        	  RecordObjectpemrek.transaction_type=Cursor1.getString("pemrek.transaction_type");
	        	  RecordObjectpemrek.product=Cursor1.getString("pemrek.product");
	        	  RecordObjectpemrek.status=Cursor1.getString("pemrek.status");
	        	  RecordObjectpemrek.note=Cursor1.getString("pemrek.note");
	        	  RecordObjectpemrek.username=Cursor1.getString("account.username");
	        	  RecordObjectpemrek.branch_code=Cursor1.getString("account.branch_code");
	        	  RecordObjectpemrek.durasi=durasi;
	        	  collectionpemrek.add(RecordObjectpemrek);
	        	return collectionpemrek;
	          }
          Connected_Expression1.close(); // close (Connected_Expression1) -> close Cursor1
	        }
      }
	      catch (SQLException e)
	      {
	    	  List<modelhistory> collectionpemrek= new ArrayList<modelhistory>();
	    	  modelhistory RecordObjectpemrek= new modelhistory(); //deklarasi RecordObject1
	    	  RecordObjectpemrek.status=e.getMessage();
        	  collectionpemrek.add(RecordObjectpemrek);
	        	return collectionpemrek;
	      }
		List<modelhistory> collectionpemrek= new ArrayList<modelhistory>();
		modelhistory RecordObjectpemrek= new modelhistory(); //deklarasi RecordObject1
      	collectionpemrek.add(RecordObjectpemrek);
      	return collectionpemrek;
		
	}
	
}
