package com.mitrakreasindo.pemrek.external.registration.serviceimpl;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateCisAndRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateCisAndRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationResponse;
import com.mitrakreasindo.pemrek.external.registration.repository.network.RegistrationRepository;
import com.mitrakreasindo.pemrek.external.registration.service.RegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService
{

	@Autowired
	private RegistrationRepository registrationRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${registration.user-id}")
	private String userId;
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	public CreateCisAndRekeningResponse createCisAndRekening (String username, String branchCode, CreateCisAndRekeningRequest request)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/customers/mdm/branches/"+branchCode+"/accounts/registration";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
				
		return registrationRepository.createCisAndRekening(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				username, 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				branchCode,
				request);
	}
	
	@Override
	public CreateRekeningResponse createRekening (String username, String branchCode, CreateRekeningRequest request)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/customers/mdm/branches/"+branchCode+"/second-accounts/registration";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(request);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
				
		return registrationRepository.createRekening(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				username, 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				branchCode,
				request);
	}
	
	@Override
	public CardRegistrationResponse createDigitalCard (String username, CardRegistrationRequest card)
	{		
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/digital-card/registration";	
		String time = OffsetDateTime.now().toString();
		String json = "";
		try
		{
			json = mapper.writeValueAsString(card);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
				
		return registrationRepository.digitalCardRegistration(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				username, 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				card);
	}
	
}
