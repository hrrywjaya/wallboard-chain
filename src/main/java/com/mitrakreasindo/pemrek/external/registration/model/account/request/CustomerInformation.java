package com.mitrakreasindo.pemrek.external.registration.model.account.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerInformation
{

	private String customerNama;
	private String customerAlias;
	private String customerNamaRef;
	private String customerAgama;
	private String customerTandaPengenalNo;
	private String customerTandaPengenalBerlakuSampai;
	private String customerTandaPengenal;
	private String customerTandaPengenalNoLainnya;
	private String customerTempatLahir;
	private String customerTanggalLahir;
	private String negaraLahir;
	private String negaraWn;
	private String customerJenisKelamin;
	private String customerNamaIbuKandung;
	private String customerKewarganegaraan;
	private String customerKitasKitap;
	private String customerKitasKitapNo;
	private String customerKitasKitapBerlakuSampai;
	private String idKecamatan;
	private String idKelurahan;
	private String idNamaJalan;
	private String idKota;
	private String idKodePos;
	private String idNegara;
	private String idProvinsi;
	private String idKomplekGedung;
	private String idRt;
	private String idRw;
	private String kodeNegaraTelpRumah;
	private String kodeAreaTelpRumah;
	private String telpRumah;
	private String domisiliKecamatan;
	private String domisiliKelurahan;
	private String domisiliNamaJalan;
	private String domisiliKota;
	private String domisiliNegara;
	private String domisiliProvinsi;
	private String domisiliKomplekGedung;
	private String domisiliRw;
	private String domisiliRt;
	private String domisiliKodePos;
	private String customerPekerjaan;
	private String customerPekerjaanLainnya;
	private String namaKantor;
	private String jabatan;
	private String bidangUsaha;
	private String email;
	@JsonProperty("alamat_kerja_1")
	private String alamatKerja1;
	@JsonProperty("alamat_kerja_2")
	private String alamatKerja2;
	@JsonProperty("alamat_kerja_3")
	private String alamatKerja3;
	private String kodeNegaraTelpKantor;
	private String kodeAreaTelpKantor;
	private String telpKantor;
	private String kodeNegaraFaxKantor;
	private String kodeAreaFaxKantor;
	private String faxKantor;
	private String customerNpwp;
	private String customerNpwpNo;
	private String customerSumberPenghasilan;
	private String customerGajiTotal;
	private String customerSumberPenghasilanLainnya;
	private String customerTujuanBukaRek;
	private String customerTujuanBukaRekLainnya;
	@JsonProperty("kd_negara_hp_1")
	private String kdNegaraHp1;
	@JsonProperty("hp_1")
	private String hp1;
	@JsonProperty("kd_negara_hp_2")
	private String kdNegaraHp2;
	@JsonProperty("hp_2")
	private String hp2;
	private String customerStatusKawin;
	private String kodeCustomerResikoTinggi;
	private String mailCode;
	private String persetujuan;
	private String mediaInfoSms;
	private String mediaInfoMail;
	private String mediaInfoPhone;
	private String formFatca;
	private String formAeoi;
	private String wajibFatca;
	private String wpNegaraLain;
	private String tin;
	private String pejabatBank;
	
}
