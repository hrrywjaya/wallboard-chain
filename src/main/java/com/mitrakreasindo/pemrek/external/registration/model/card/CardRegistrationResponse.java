package com.mitrakreasindo.pemrek.external.registration.model.card;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CardRegistrationResponse
{

	private String accountNumber;
	private String customerName;
	private String cardNumber;
	private String companyName;
	private String cardType;
	private String companyBrandCode;
	
}
