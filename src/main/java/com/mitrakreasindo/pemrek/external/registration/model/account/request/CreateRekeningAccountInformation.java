package com.mitrakreasindo.pemrek.external.registration.model.account.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CreateRekeningAccountInformation
{

	private String customerCabang;
	private String customerNoRekBaru;
	private String accountType;
	private String tahapanGoldStatus;
	private String costCenter;
	private String kodeNegara;
	private String produkMataUang;
	private String serviceChargeCode;
	private String bebanBiayaAdmin;
	private String userCode;
	private String periodeBiayaAdmin;
	private String periodeRk;
	private String periodeBunga;
	private String interestPlan;
	private String odPlan;
	private String golonganPemilik;
	private String witholdingPlan;
	private String kodePembebananPajak;
	private String kodePeroranganBisnis;
	private String kodePendudukNonpenduduk;
	private String customerRekeningUntuk;
	private String statusRekeningGabungan;
	private String rekGabunganNama;
	private String rekGabunganNoCustomer;
	private String rekGabunganNamaTercetakPadaKartu;
	private String rekGabunganNoTandaPengenal;
	private String bukuTabungan;
	private String produkPasporBcaJenis;
	private String produkPasporBcaTipe;
	private String produkPasporBcaInstantNo;
	private String produkPasporBcaPetunjukLayar;
	private String fasilitasKlikbca;
	private String fasilitasKeybcaIb;
	private String fasilitasKeybcaSn;
	private String fasilitasKlikbcaHp;
	private String fasilitasMbca;
	private String fasilitasAktivasiFinMbca;
	private String fasilitasMbcaHp;
	private String fasilitasBbp;
	
}
