package com.mitrakreasindo.pemrek.external.registration.service;

import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateCisAndRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateCisAndRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationResponse;

public interface RegistrationService
{

	CreateCisAndRekeningResponse createCisAndRekening(String username, String branchCode, CreateCisAndRekeningRequest request);
	
	CreateRekeningResponse createRekening(String username, String branchCode, CreateRekeningRequest request);
	
	CardRegistrationResponse createDigitalCard(String username, CardRegistrationRequest card);
	
}
