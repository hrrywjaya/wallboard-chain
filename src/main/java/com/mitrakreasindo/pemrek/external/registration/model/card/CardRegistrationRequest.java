package com.mitrakreasindo.pemrek.external.registration.model.card;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CardRegistrationRequest
{

	private String accountNumber;
	private String userId;
	private String branchCode;
	private String branchName;
	private String spvId;
	
}
