package com.mitrakreasindo.pemrek.external.registration.model.account.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CreateRekeningRequest
{

	private String customerNumber;
	private String accountNumber;
	private String referenceNumber;
	private String action;
	private String spvId;
	@JsonProperty("customer_pekerjaan_desc_1")
	private String customerPekerjaanDesc1;
	@JsonProperty("customer_pekerjaan_desc_2")
	private String customerPekerjaanDesc2;
	@JsonProperty("customer_pekerjaan_desc_3")
	private String customerPekerjaanDesc3;
	private CreateRekeningAccountInformation accountInformation;
	
}
