package com.mitrakreasindo.pemrek.external.registration.model.account.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CreateCisAndRekeningResponse
{

	private String accountNumber;
	private String cisCustomerNumber;
	
}
