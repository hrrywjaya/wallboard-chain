package com.mitrakreasindo.pemrek.external.registration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateCisAndRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateCisAndRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationResponse;
import com.mitrakreasindo.pemrek.external.registration.service.RegistrationService;

@RestController
@RequestMapping("/capi/registration")
public class RegistrationController
{

	@Autowired
	private RegistrationService registrationService;

	// @PostMapping("/account/cis-rekening")
	// public CreateCisAndRekeningResponse createCisAndRekening(
	// 		@RequestBody CreateCisAndRekeningRequest request) {
	// 	return registrationService.createCisAndRekening(request);
	// }
	
	// @PostMapping("/account/rekening")
	// public CreateRekeningResponse createRekening( 
	// 		@RequestBody CreateRekeningRequest request) {
	// 	return registrationService.createRekening(request);
	// }
	
	// @PostMapping("/card/digital")
	// public CardRegistrationResponse createDigitalCard (@RequestBody CardRegistrationRequest card)
	// {
	// 	return registrationService.createDigitalCard(card);
	// }

}
