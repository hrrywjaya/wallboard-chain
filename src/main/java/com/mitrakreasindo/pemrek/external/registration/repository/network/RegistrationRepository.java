package com.mitrakreasindo.pemrek.external.registration.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateCisAndRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.request.CreateRekeningRequest;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateCisAndRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.account.response.CreateRekeningResponse;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationResponse;

@FeignClient(name = "registration-repository", url = "${registration.server.url}")
public interface RegistrationRepository
{

	/**
	 * create cis and rekening
	 * first account
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param pemrek
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bds/general/api/customers/mdm/branches/{branch-code}/accounts/registration")
	CreateCisAndRekeningResponse createCisAndRekening(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@RequestBody CreateCisAndRekeningRequest request);
	
	/**
	 * create rekening
	 * second account
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param branchCode
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bds/general/api/customers/mdm/branches/{branch-code}/second-accounts/registration")
	CreateRekeningResponse createRekening(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("branch-code") String branchCode,
			@RequestBody CreateRekeningRequest request);
	
	/**
	 * create digital card registration
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param card
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bds/general/api/digital-card/registration")
	CardRegistrationResponse digitalCardRegistration(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestBody CardRegistrationRequest card);
	
}
