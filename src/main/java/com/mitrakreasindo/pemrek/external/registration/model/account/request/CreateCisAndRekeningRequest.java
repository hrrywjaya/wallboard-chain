package com.mitrakreasindo.pemrek.external.registration.model.account.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CreateCisAndRekeningRequest
{

	private String referenceNumber;
	private String action;
	private String spvId;
	private CustomerInformation customerInformation;
	private AccountInformation accountInformation;
	@JsonProperty("customer_pekerjaan_desc_1")
	private String customerPekerjaanDesc1;
	@JsonProperty("customer_pekerjaan_desc_2")
	private String customerPekerjaanDesc2;
	@JsonProperty("customer_pekerjaan_desc_3")
	private String customerPekerjaanDesc3;
	
}
