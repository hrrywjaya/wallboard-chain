package com.mitrakreasindo.pemrek.external.common.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;


/*
* Dibuat untuk kebutuhan get error messagenya yang mana kalau 
* pakai errorSchemaUpperCamelCase dan outputStatus tidak bisa
*/
@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class OutputStatusUpperCamelCase {

	private ErrorSchemaUpperCamelCase errorSchema;
	private OutputStatusScema outputSchema;
	
}
