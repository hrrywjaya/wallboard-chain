package com.mitrakreasindo.pemrek.external.common.converter;

public class YesOrNoConverter
{

	public static String convertFromNumber(String data) {
		if (data == null)
			throw new RuntimeException("data can't null");
		switch (data)
		{
		case "0":
			return "N";
		case "1":
			return "Y";
		default:
			return "";
		}
	}

	public static String convertFromBooleanString(String data) {
		if (data == null)
			return "N";
			
		switch (data)
		{
		case "false":
			return "N";
		case "true":
			return "Y";
		default:
			return "";
		}
	}
	
}
