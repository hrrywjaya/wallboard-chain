package com.mitrakreasindo.pemrek.external.common.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class Output<T>
{

	private ErrorSchema errorSchema;
	private T outputSchema;
	
}
