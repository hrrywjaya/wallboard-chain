package com.mitrakreasindo.pemrek.external.common.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.ErrorMessage;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class ErrorSchema
{
	
	private String errorCode;
	private ErrorMessage errorMessage;
	
}
