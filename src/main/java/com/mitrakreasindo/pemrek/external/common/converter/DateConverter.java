package com.mitrakreasindo.pemrek.external.common.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

	public static String convert(String data) {
		if (data == null)
			throw new RuntimeException("data can't null");
		
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sf.parse(data);
			sf.applyPattern("ddMMyyyy");
			return sf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String convert(String data, String fromFormat, String toFormat) {
		if (data == null)
			throw new RuntimeException("data can't null");
		
		try {
			SimpleDateFormat sf = new SimpleDateFormat(fromFormat);
			Date date = sf.parse(data);
			sf.applyPattern(toFormat);
			return sf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String convert(Date data) {
		if (data == null)
			throw new RuntimeException("data can't null");
		
		SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
		return sf.format(data);
	}

}
