package com.mitrakreasindo.pemrek.external.common.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.ErrorMessage;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class ErrorSchemaSnakeCase
{
	
	private String errorCode;
	private ErrorMessage errorMessage;
	
}
