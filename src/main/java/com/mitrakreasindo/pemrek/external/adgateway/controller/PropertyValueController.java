package com.mitrakreasindo.pemrek.external.adgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.adgateway.model.OutputProperty;
import com.mitrakreasindo.pemrek.external.adgateway.model.PropertyValueModel;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;

@RestController
@RequestMapping("/capi/property-value")
public class PropertyValueController {

	@Autowired
	private AdGatewayService adGatewayService;
	
	@PostMapping
	public OutputProperty getPropertyValue (@RequestBody PropertyValueModel propertyValueModel) {
		return adGatewayService.propertyValue(propertyValueModel.getUserId(), propertyValueModel.getPassword(), propertyValueModel.getPropertyName(), propertyValueModel.getPath());
	}
	
	@PostMapping("/users/{username}/property/{property}")
	public OutputProperty checkProperty (@PathVariable("username") String username, @PathVariable("property") String property) {
		return adGatewayService.checkProperty(username, property);
	}
	
}
