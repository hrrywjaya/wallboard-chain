package com.mitrakreasindo.pemrek.external.adgateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PropertyValueModelWithoutPassword
{

	@JsonProperty("UserID")
	private String userId;
	@JsonProperty("PropertyName")
	private String propertyName;
	@JsonProperty("Path")
	private String path;
	
	
	
}
