package com.mitrakreasindo.pemrek.external.adgateway.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class PropertyOutputSchema {

	private List<String> value;
	
}
