package com.mitrakreasindo.pemrek.external.adgateway.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.mitrakreasindo.pemrek.external.adgateway.model.LoginModel;
import com.mitrakreasindo.pemrek.external.adgateway.model.OutputProperty;
import com.mitrakreasindo.pemrek.external.adgateway.model.PropertyValueModel;
import com.mitrakreasindo.pemrek.external.adgateway.model.PropertyValueModelWithoutPassword;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;

@FeignClient(name = "adgateway-repository", url = "${adgateway.server.url}")
public interface AdGatewayRepository
{

	/**
	 * login
	 * @param clientId
	 * @param loginModel
	 * @return
	 */
	@PostMapping("/ad-gateways/verify1")
	OutputStatus login(
			@RequestHeader("ClientId") String clientId,
			@RequestBody LoginModel loginModel);
	

	/**
	 * property-value
	 * @param clientId
	 * @param propertyValueModel
	 * @return
	 */
	@PostMapping("/ad-gateways/property-value")
	OutputProperty propertyValue(
			@RequestHeader("ClientId") String clientId,
			@RequestBody PropertyValueModel propertyValueModel);
	
	/**
	 * get property-value without password
	 * used to check user domain availability
	 * @param clientId
	 * @param propertyValueModel
	 * @return
	 */
	@PostMapping("/ad-gateways/property-value2")
	OutputProperty propertyValue2(
			@RequestHeader("ClientId") String clientId,
			@RequestBody PropertyValueModelWithoutPassword propertyValueModel);
}
