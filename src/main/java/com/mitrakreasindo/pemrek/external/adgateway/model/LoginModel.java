package com.mitrakreasindo.pemrek.external.adgateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LoginModel
{

	@JsonProperty("ApplicationID")
	private String applicationId;
	@JsonProperty("UserID")
	private String userId;
	@JsonProperty("Password")
	private String password;
	
}
