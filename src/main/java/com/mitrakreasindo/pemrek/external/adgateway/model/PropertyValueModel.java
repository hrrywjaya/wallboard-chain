package com.mitrakreasindo.pemrek.external.adgateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PropertyValueModel
{

	@JsonProperty("ApplicationID")
	private String applicationId;
	@JsonProperty("UserID")
	private String userId;
	@JsonProperty("Password")
	private String password;
	@JsonProperty("PropertyName")
	private String propertyName;
	@JsonProperty("Path")
	private String path;
		
}
