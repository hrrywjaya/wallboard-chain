package com.mitrakreasindo.pemrek.external.adgateway.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.core.security.TripleDesEncription;
import com.mitrakreasindo.pemrek.external.adgateway.model.LoginModel;
import com.mitrakreasindo.pemrek.external.adgateway.model.OutputProperty;
import com.mitrakreasindo.pemrek.external.adgateway.model.PropertyValueModel;
import com.mitrakreasindo.pemrek.external.adgateway.model.PropertyValueModelWithoutPassword;
import com.mitrakreasindo.pemrek.external.adgateway.repository.network.AdGatewayRepository;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;

@Service
public class AdGatewayServiceImpl implements AdGatewayService {

	@Autowired
	private AdGatewayRepository adGatewayRepository;
	@Value("${adgateway.client-id}")
	private String adGatewayClientId;
	@Autowired
	private TripleDesEncription tripleDes;

	@Override
	public OutputStatus login(String username, String password) {
		LoginModel model = new LoginModel();
		model.setApplicationId("CHAIN");
		model.setUserId(username);
		try {
			model.setPassword(tripleDes.bcaEencrypt(password));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adGatewayRepository.login(adGatewayClientId, model);
	}

	@Override
	public OutputProperty propertyValue(String username, String password, String propertyName, String path) {
		try {
			PropertyValueModel model = new PropertyValueModel();
			model.setApplicationId("CHAIN");
			model.setUserId(username);
			model.setPassword(tripleDes.bcaEencrypt(password));
			model.setPropertyName(propertyName);
			model.setPath(path);
			return adGatewayRepository.propertyValue(adGatewayClientId, model);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public OutputProperty checkProperty(String username, String property) {
		PropertyValueModelWithoutPassword model = new PropertyValueModelWithoutPassword();
		model.setUserId(username);
		model.setPropertyName(property);
		return adGatewayRepository.propertyValue2(adGatewayClientId, model);
	}

}
