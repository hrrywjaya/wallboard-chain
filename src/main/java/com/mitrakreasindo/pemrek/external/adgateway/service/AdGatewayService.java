package com.mitrakreasindo.pemrek.external.adgateway.service;

import com.mitrakreasindo.pemrek.external.adgateway.model.OutputProperty;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;

public interface AdGatewayService
{

	OutputStatus login(String username, String password);
	
	OutputProperty propertyValue(String username, String password,String propertyName, String path);
	
	OutputProperty checkProperty(String username, String property);
	
}
