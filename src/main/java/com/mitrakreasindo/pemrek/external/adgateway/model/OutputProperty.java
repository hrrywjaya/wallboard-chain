package com.mitrakreasindo.pemrek.external.adgateway.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class OutputProperty
{

	private ErrorSchemaUpperCamelCase errorSchema;
	private PropertyOutputSchema outputSchema;
	
}
