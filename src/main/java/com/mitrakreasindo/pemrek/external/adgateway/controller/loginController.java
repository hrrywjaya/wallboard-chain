package com.mitrakreasindo.pemrek.external.adgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;

@RestController
@RequestMapping("/testcapi/login")
public class loginController {

	@Autowired
	private AdGatewayService adGatewayService;
	
	@PostMapping
	public OutputStatus login(@RequestParam String username, @RequestParam String password) {
		return adGatewayService.login(username, password);
	}
}
