package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AgentTodayRowMapper implements RowMapper<DaftarAgent>{

	@Override
	public DaftarAgent mapRow(ResultSet rs, int rowNum) throws SQLException {
		DaftarAgent ch = new DaftarAgent();
    ch.setUsername(rs.getString("USERNAME"));
    // ch.setBerhasil(rs.getString("berhasil"));
    // ch.setJumlah(rs.getString("jumlah"));
		return ch;
	}

}
