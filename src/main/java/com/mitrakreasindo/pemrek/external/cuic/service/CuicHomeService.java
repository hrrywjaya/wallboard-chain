package com.mitrakreasindo.pemrek.external.cuic.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.mitrakreasindo.pemrek.external.cuic.model.AuxTime;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTimeOutput;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLog;
import com.mitrakreasindo.pemrek.external.cuic.model.DaftarAgentPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.HistoryAux;
import com.mitrakreasindo.pemrek.external.cuic.model.PerformanceReview;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilan;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilanAgent;;

public interface CuicHomeService {

	List<AuxTime> getAuxTimes();
  
  StatistikPanggilan getStatistikPanggilan (String startPeriode, String endPeriode);

  StatistikPanggilanAgent getStatistikPanggilanAgent (String startPeriode, String endPeriode, String agentLoginName);

  List<AuxTime> getStateReadyAux ();

  AuxTimeOutput getAgetRealTimeStatus(String state);

  List<AuxTime> getAuxByState (String state);
  
  List<CallLog> getcalllog (String startPeriode, String endPeriode);

  PerformanceReview agentPR(String startPeriode, String endPeriode, String agentLoginName);

  PerformanceReview spvPR(String startPeriode, String endPeriode);

  HistoryAux agentPRAux(String startPeriode, String endPeriode, String agentLoginName);

  HistoryAux spvPRAux(String startPeriode, String endPeriode);

  DaftarAgentPaging listDaftarAgent (String startPeriode, String endPeriode, String agent, int pageNumber, int rowPerPage);
  
}
