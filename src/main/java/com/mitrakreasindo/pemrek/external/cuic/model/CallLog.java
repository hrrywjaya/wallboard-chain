package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class CallLog {
  private String id;
  private String active_flag;
  private String username;
  private String answer_date;
  private String answer_duration;
  private String call_id;
  private String transaksi_status;
  private String terputus_md_id;
  private String tolak_md_id;
  private String no_ref;
  private int transaksi_duration;
} 