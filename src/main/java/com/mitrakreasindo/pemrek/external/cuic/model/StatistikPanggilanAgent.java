package com.mitrakreasindo.pemrek.external.cuic.model;

import java.util.List;

import lombok.Data;

@Data
public class StatistikPanggilanAgent {
  private long panggilanDiterima;
  private long transaksiBerhasil;
  private long transaksiDitolak;
  private long panggilanTerputus;

  private List<DateDiff> datediff;
  
  private int ServisLevelTarget;
  private int averageHandlingTime;
  private int transaksiPending;
}
