package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class CallHandle {
	private int callsHandled;
}
