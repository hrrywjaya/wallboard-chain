package com.mitrakreasindo.pemrek.external.cuic.model;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DurationHoldPaging {
  private List<TransactionSummary> sum;
  private List<DurationHold> dh;
  private int currentPage;
  private long totalPage;
  private int rowPerPage;
  private int totalData;
  private boolean first;
  private boolean last;
}
