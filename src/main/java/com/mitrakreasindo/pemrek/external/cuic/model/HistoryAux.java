package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class HistoryAux {
	private int ready;
	private int toilet;
  private int ibadah;
  private int istirahat;
  private int jobRutin;
  private int otherReason;
  private int totalProduktivitas;
  private int meeting;
  private int maintenance;


}
