package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DurationHoldRowMapper implements RowMapper<DurationHold>{

	@Override
	public DurationHold mapRow(ResultSet rs, int rowNum) throws SQLException {
		DurationHold dh = new DurationHold();
		dh.setLoginName(rs.getString("LoginName"));
		dh.setDuration(rs.getInt("Duration"));
    dh.setHoldTime(rs.getInt("HoldTime"));
    dh.setNoRef(rs.getString("NoRef"));
		return dh;
	}

}
