package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RedirectCallRowMapper implements RowMapper<RedirectCall>{

	@Override
	public RedirectCall mapRow(ResultSet rs, int rowNum) throws SQLException {
		RedirectCall rc = new RedirectCall();
    rc.setRedirectCalls(rs.getInt("RedirectCalls"));
    rc.setLoginname(rs.getString("loginname"));
		return rc;
	}

}
