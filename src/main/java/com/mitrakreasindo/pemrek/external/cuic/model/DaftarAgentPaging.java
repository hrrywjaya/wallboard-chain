package com.mitrakreasindo.pemrek.external.cuic.model;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DaftarAgentPaging {
  // private List<DaftarAgent> da;
  private List<AuxTime> da;
  private int currentPage;
  private long totalPage;
  private int rowPerPage;
  private int totalData;
  private boolean first;
  private boolean last;
}
