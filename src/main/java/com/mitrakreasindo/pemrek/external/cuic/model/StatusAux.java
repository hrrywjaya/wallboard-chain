package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class StatusAux {
	// private int ready;
	// private int toilet;
  // private int ibadah;
  // private int istirahat;
  // private int jobRutin;
  // private int otherReason;
  // private int totalProduktivitas;

  private String ReasonCode;
  private String textReasonCode;
  private String loginName;
  private int totalLoginTime;
  private int totalNotReadyTime;
  private int totalCodeDuration;
  private int agentStateCode;
  private int agentReady;

}
