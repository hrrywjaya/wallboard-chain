package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class PerformanceReview {
  private int panggilanDiterima;
  private int transaksiBerhasil;
  private int panggilanTerputus;
  private int panggilanDitolak;
  private int rona;
  private int panggilanLebihDariSL;
  private int averageHandlingTime;
  private float serviceLevel;
  private int queue;
  private int panggilanMasuk;
  private int abandon;
  private int durasiBerhasil;
}
