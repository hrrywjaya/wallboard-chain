package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class AgentToday {

  private int berhasil;
  private int jumlah;
  private String username;
	
}
