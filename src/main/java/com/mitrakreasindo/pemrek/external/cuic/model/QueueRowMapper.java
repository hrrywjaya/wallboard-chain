package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class QueueRowMapper implements RowMapper<Queue>{

	@Override
	public Queue mapRow(ResultSet rs, int rowNum) throws SQLException {
		Queue q = new Queue();
		q.setRouterCallsQnow(rs.getInt("RouterCallsQNow"));
		return q;
	}

}
