package com.mitrakreasindo.pemrek.external.cuic.serviceimpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.cuic.model.AbandonCall;
import com.mitrakreasindo.pemrek.external.cuic.model.AbandonCallRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.Queue;
import com.mitrakreasindo.pemrek.external.cuic.model.QueueRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.RedirectCall;
import com.mitrakreasindo.pemrek.external.cuic.model.RedirectCallRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.StatusAux;
import com.mitrakreasindo.pemrek.external.cuic.model.StatusAuxRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.StatusAuxRowNewQueryMapper;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicMonitoringService;

@Service
public class CuicMonitoringServiceImpl implements CuicMonitoringService{


	@Autowired
	@Qualifier("cuicJdbc")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<AbandonCall> getAbandonCall(String startPeriode, String endPeriode) {
    String sql = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" +
    "SELECT	Media = ASGI.Media, \r\n" +
    "Interval	 = AI.DateTime, \r\n" +
        "Date		 = CONVERT(char(10),AI.DateTime,101), \r\n" +
               "FullName = Person.LastName + ', ' + Person.FirstName, \r\n" +
               "loginname = Person.LoginName, \r\n" +
         "AgentSkillID  = Agent.SkillTargetID, \r\n" +
         "SkillGroupName = ASGI.SGEnterpriseName, \r\n" +
         "SkillGroupSkillTargetID = ASGI.SGSkillTargetID, \r\n" +
       "AgentLoggedOnTime = SUM(ISNULL( AI.LoggedOnTime,0)), \r\n" +
      "AgentSGLoggedOnTime = SUM(ISNULL(ASGI.LoggedOnTime,0)), \r\n" +
           "AgentAvailTime = SUM(ISNULL(AI.AvailTime, 0)), \r\n" +
           "AgentNotReady = SUM(ISNULL(AI.NotReadyTime, 0)) , \r\n" +
      "AgentBusyOtherTime = SUM(ISNULL(AI.LoggedOnTime - AI.AvailTime,0)), \r\n" +
         "--Need to 're-sum' in order to make SQL think these are aggregate functions \r\n" +
            "CallsAnswered  = sum(ASGI.CallsAnswered), \r\n" +
            "CallsHandled = sum(ASGI.CallsHandled), \r\n" +
            "AbandRingCalls = sum(ASGI.AbandRingCalls), \r\n" +
          "AbandRingCallsTime = sum(ASGI.AbandRingCallsTime), \r\n" +
             "RedirectCalls = sum(ASGI.RedirectCalls), \r\n" +
           "RedirectCallsTime = sum(ASGI.RedirectCallsTime), \r\n" +
          "AbandonHoldCalls = sum(ASGI.AbandonHoldCalls), \r\n" +
           "TransferInCalls = sum(ASGI.TransferInCalls), \r\n" +
          "TransferOutCalls = sum(ASGI.TransferOutCalls), \r\n" +
           "ConsultativeCalls = sum(ASGI.ConsultativeCalls), \r\n" +
           "ConferenceInCalls = sum(ASGI.ConferenceInCalls), \r\n" +
          "ConferenceOutCalls = sum(ASGI.ConferenceOutCalls),\r\n" +
            "OutExtnCalls = sum(ASGI.OutExtnCalls), \r\n" +
              "ShortCalls = sum(ASGI.ShortCalls),\r\n" +
            "SupAssistCalls = sum(ASGI.SupAssistCalls), \r\n" +
            "BargeInCalls = sum(ASGI.BargeInCalls), \r\n" +
            "InterceptCalls = sum(ASGI.InterceptCalls), \r\n" +
            "MonitorCalls = sum(ASGI.MonitorCalls), \r\n" +
            "WhisperCalls = sum(ASGI.WhisperCalls), \r\n" +
        "EmergencyAssistCalls = sum(ASGI.EmergencyAssistCalls), \r\n" +
          "SupAssistCallsTime = sum(ASGI.SupAssistCallsTime), \r\n" +
        "AgentOutCallsOnHoldTime = sum(ASGI.AgentOutCallsOnHoldTime), \r\n" +
             "InCallsOnHold = sum(ASGI.InCallsOnHold), \r\n" +
           "InCallsOnHoldTime = sum(ASGI.InCallsOnHoldTime), \r\n" +
            "IntCallsOnHold = sum(ASGI.IntCallsOnHold), \r\n" +
          "IntCallsOnHoldTime = sum(ASGI.IntCallsOnHoldTime), \r\n" +
              "TalkTime = sum(ASGI.TalkTime), \r\n" +
          "HandledCallsTime = sum(ASGI.HandledCallsTime), \r\n" +
              "HoldTime = sum(ASGI.HoldTime), \r\n" +
            "ReservedTime = sum(ASGI.ReservedTime), \r\n" +
              "WrapTime = sum(ASGI.WrapTime), \r\n" +
            "AnswerWaitTime = sum(ASGI.AnswerWaitTime), \r\n" +
            "AutoOutCalls = sum(ASGI.AutoOutCalls), \r\n" +
          "AutoOutCallsTime = sum(ASGI.AutoOutCallsTime), \r\n" +
        "AutoOutCallsTalkTime = sum(ASGI.AutoOutCallsTalkTime), \r\n" +
          "AutoOutCallsOnHold = sum(ASGI.AutoOutCallsOnHold), \r\n" +
        "AutoOutCallsOnHoldTime = sum(ASGI.AutoOutCallsOnHoldTime), \r\n" +
            "PreviewCalls = sum(ASGI.PreviewCalls), \r\n" +
          "PreviewCallsTime = sum(ASGI.PreviewCallsTime), \r\n" +
        "PreviewCallsTalkTime = sum(ASGI.PreviewCallsTalkTime), \r\n" +
          "PreviewCallsOnHold = sum(ASGI.PreviewCallsOnHold), \r\n" +
        "PreviewCallsOnHoldTime = sum(ASGI.PreviewCallsOnHoldTime), \r\n" +
            "ReserveCalls = sum(ASGI.ReserveCalls), \r\n" +
          "ReserveCallsTime = sum(ASGI.ReserveCallsTime), \r\n" +
        "ReserveCallsTalkTime = sum(ASGI.ReserveCallsTalkTime), \r\n" +
          "ReserveCallsOnHold = sum(ASGI.ReserveCallsOnHold), \r\n" +
        "ReserveCallsOnHoldTime = sum(ASGI.ReserveCallsOnHoldTime), \r\n" +
           "TalkAutoOutTime = sum(ASGI.TalkAutoOutTime), \r\n" +
           "TalkPreviewTime = sum(ASGI.TalkPreviewTime), \r\n" +
           "TalkReserveTime = sum(ASGI.TalkReserveTime), \r\n" +
    "AgentOutCallsTime= SUM(ISNULL(ASGI.AgentOutCallsTime, 0)), \r\n" +
        "AgentOutCallsTalkTime = sum(ASGI.AgentOutCallsTalkTime), \r\n" +
        "AgentTerminatedCalls = sum(ASGI.AgentTerminatedCalls), \r\n" +
          "CallbackMessages = sum(ASGI.CallbackMessages), \r\n" +
        "CallbackMessagesTime = sum(ASGI.CallbackMessagesTime), \r\n" +
        "ConsultativeCallsTime = sum(ASGI.ConsultativeCallsTime), \r\n" +
        "ConferencedInCallsTime = sum(ASGI.ConferencedInCallsTime), \r\n" +
        "ConferencedOutCallsTime = sum(ASGI.ConferencedOutCallsTime),\r\n" +
        "HandledCallsTalkTime = sum(ASGI.HandledCallsTalkTime), \r\n" +
           "InternalCallsRcvd = sum(ASGI.InternalCallsRcvd), \r\n" +
        "InternalCallsRcvdTime = sum(ASGI.InternalCallsRcvdTime), \r\n" +
             "InternalCalls = sum(ASGI.InternalCalls),\r\n" +
           "InternalCallsTime = sum(ASGI.InternalCallsTime), \r\n" +
        "TransferredInCallsTime = sum(ASGI.TransferredInCallsTime),\r\n" +
             "TalkOtherTime = sum(ASGI.TalkOtherTime), \r\n" +
             "TalkOutTime = sum(ASGI.TalkOutTime), \r\n" +
              "TimeZone = sum(ASGI.TimeZone), \r\n" +
           "InterruptedTime = sum(ASGI.InterruptedTime), \r\n" +
          "WorkNotReadyTime = sum(ASGI.WorkNotReadyTime),\r\n" +
             "WorkReadyTime = sum(ASGI.WorkReadyTime), \r\n" +
        "NetConsultativeCalls = sum(ASGI.NetConsultativeCalls), \r\n" +
        "NetConsultativeCallsTime = sum(ASGI.NetConsultativeCallsTime),\r\n" +
        "NetConferencedOutCalls = sum(ASGI.NetConferencedOutCalls), \r\n" +
        "NetConfOutCallsTime = sum(ASGI.NetConfOutCallsTime), \r\n" +
        "NetTransferredOutCalls = sum(ASGI.NetTransferredOutCalls), \r\n" +
              "DbDateTime = max(ASGI.DbDateTime), \r\n" +
                "Assists = SUM(ASGI.Assists), \r\n" +
               "TransOut = SUM(ASGI.TransOut), \r\n" +
        "AHT = ISNULL(SUM(ASGI.HandledCallsTime) / SUM(ASGI.CallsHandled),0), \r\n" +
       "AHoldT = ISNULL(SUM(ASGI.InCallsOnHoldTime) / SUM(ASGI.InCallsOnHold),0), \r\n" +
       "perActiveTime = SUM(ASGI.TalkTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perHoldTime  = sum(ASGI.HoldTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perNotActive = ISNULL(SUM(AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
          "perNotReady = ISNULL(SUM(AI.NotReadyTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
         "perReserved = sum(ASGI.ReservedTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
           "perWrapup = sum(ASGI.WrapTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perBusyOther = ISNULL(SUM(AI.LoggedOnTime - AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
        "AACW = ISNULL((SUM(ASGI.WrapTime) / SUM(ASGI.CallsHandled)),0), \r\n" +
        "perWACW = ISNULL((SUM(ASGI.WrapTime) + SUM(ASGI.TalkTime) + SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0)) )* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0), \r\n" +
        "perWOACW = ISNULL((SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0))+ SUM(ASGI.TalkTime))* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0) \r\n" +
        
      "FROM Agent (nolock), \r\n" +
         "Agent_Interval AI (nolock), \r\n" +
         "Person (nolock), \r\n" +
         "--This nested Select statement is necessary in order to make AgentLoggedOnTime, AgentAvailTime, and AgentNotReadyTime work correctly  \r\n" +
        "(Select   Media		 = A.Media, \r\n" +
    "MediaID = A.MediaID,\r\n" +
             "A.DateTime, \r\n" +
             "A.SkillTargetID, \r\n" +
             "A.SGEnterpriseName, \r\n" +
             "A.SGSkillTargetID, \r\n" +
         "A.SGPeripheralID, \r\n" +
             "CallsAnswered  = SUM(ISNULL(A.CallsAnswered,0)), \r\n" +
              "LoggedOnTime = SUM(ISNULL(A.LoggedOnTime,0)), \r\n" +
            "CallsHandled = SUM(ISNULL(A.CallsHandled,0)), \r\n" +
            "AbandRingCalls = SUM(ISNULL(A.AbandonRingCalls,0)), \r\n" +
          "AbandRingCallsTime = SUM(ISNULL(A.AbandonRingTime,0)), \r\n" +
             "RedirectCalls = SUM(ISNULL(A.RedirectNoAnsCalls,0)), \r\n" +
           "RedirectCallsTime = SUM(ISNULL(A.RedirectNoAnsCallsTime,0)), \r\n" +
          "AbandonHoldCalls = SUM(ISNULL(A.AbandonHoldCalls,0)),\r\n" +
           "TransferInCalls = SUM(ISNULL(A.TransferredInCalls,0)), \r\n" +
          "TransferOutCalls = SUM(ISNULL(A.TransferredOutCalls,0)), \r\n" +
           "ConsultativeCalls = SUM(ISNULL(A.ConsultativeCalls,0)), \r\n" +
           "ConferenceInCalls = SUM(ISNULL(A.ConferencedInCalls,0)), \r\n" +
          "ConferenceOutCalls = SUM(ISNULL(A.ConferencedOutCalls,0)), \r\n" +
            "OutExtnCalls = SUM(ISNULL(A.AgentOutCalls,0)), \r\n" +
              "ShortCalls = SUM(ISNULL(A.ShortCalls,0)), \r\n" +
            "SupAssistCalls = SUM(ISNULL(A.SupervAssistCalls,0)), \r\n" +
            "BargeInCalls = SUM(ISNULL(A.BargeInCalls,0)), \r\n" +
            "InterceptCalls = SUM(ISNULL(A.InterceptCalls,0)), \r\n" +
            "MonitorCalls = SUM(ISNULL(A.MonitorCalls,0)),\r\n" +
            "WhisperCalls = SUM(ISNULL(A.WhisperCalls,0)), \r\n" +
        "EmergencyAssistCalls = SUM(ISNULL(A.EmergencyAssists,0)), \r\n" +
          "SupAssistCallsTime = SUM(ISNULL(A.SupervAssistCallsTime,0)), \r\n" +
        "AgentOutCallsOnHoldTime = SUM(ISNULL(A.AgentOutCallsOnHoldTime,0)), \r\n" +
             "InCallsOnHold = SUM(ISNULL(A.IncomingCallsOnHold,0)), \r\n" +
           "InCallsOnHoldTime = SUM(ISNULL(A.IncomingCallsOnHoldTime,0)), \r\n" + 
            "IntCallsOnHold = SUM(ISNULL(A.InternalCallsOnHold,0)), \r\n" +
          "IntCallsOnHoldTime = SUM(ISNULL(A.InternalCallsOnHoldTime,0)), \r\n" +
              "TalkTime = sum(isnull(A.TalkInTime,0)) +  \r\n" +
                     "sum(isnull(A.TalkOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkOtherTime,0)) + \r\n" +
                     "sum(isnull(A.TalkAutoOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkPreviewTime,0)) + \r\n" +
                     "sum(isnull(A.TalkReserveTime,0)), \r\n" +
          "HandledCallsTime = SUM(ISNULL(A.HandledCallsTime,0)), \r\n" +
              "HoldTime = SUM(ISNULL(A.HoldTime,0)), \r\n" +
            "ReservedTime = SUM(ISNULL(A.ReservedStateTime,0)), \r\n" +
              "WrapTime = SUM(ISNULL(A.WorkNotReadyTime + A.WorkReadyTime,0)), \r\n" +
           "AnswerWaitTime = SUM(ISNULL(A.AnswerWaitTime,0)), \r\n" +
            "AutoOutCalls = SUM(ISNULL(A.AutoOutCalls,0)), \r\n" +
          "AutoOutCallsTime = SUM(ISNULL(A.AutoOutCallsTime,0)), \r\n" +
        "AutoOutCallsTalkTime = SUM(ISNULL(A.AutoOutCallsTalkTime,0)), \r\n" +
          "AutoOutCallsOnHold = SUM(ISNULL(A.AutoOutCallsOnHold,0)), \r\n" +
        "AutoOutCallsOnHoldTime = SUM(ISNULL(A.AutoOutCallsOnHoldTime,0)), \r\n" +
            "PreviewCalls = SUM(ISNULL(A.PreviewCalls,0)), \r\n" +
          "PreviewCallsTime = SUM(ISNULL(A.PreviewCallsTime,0)), \r\n" +
        "PreviewCallsTalkTime = SUM(ISNULL(A.PreviewCallsTalkTime,0)), \r\n" +
          "PreviewCallsOnHold = SUM(ISNULL(A.PreviewCallsOnHold,0)), \r\n" +
        "PreviewCallsOnHoldTime = SUM(ISNULL(A.PreviewCallsOnHoldTime,0)), \r\n" +
            "ReserveCalls = SUM(ISNULL(A.ReserveCalls,0)), \r\n" +
          "ReserveCallsTime = SUM(ISNULL(A.ReserveCallsTime,0)), \r\n" +
        "ReserveCallsTalkTime = SUM(ISNULL(A.ReserveCallsTalkTime,0)), \r\n" +
          "ReserveCallsOnHold = SUM(ISNULL(A.ReserveCallsOnHold,0)), \r\n" +
        "ReserveCallsOnHoldTime = SUM(ISNULL(A.ReserveCallsOnHoldTime,0)), \r\n" +
           "TalkAutoOutTime = SUM(ISNULL(A.TalkAutoOutTime,0)), \r\n" +
           "TalkPreviewTime = SUM(ISNULL(A.TalkPreviewTime,0)), \r\n" +
           "TalkReserveTime = SUM(ISNULL(A.TalkReserveTime,0)), \r\n" +
    "AgentOutCallsTime = SUM(ISNULL(A.AgentOutCallsTime,0)), \r\n" +
        "AgentOutCallsTalkTime = SUM(ISNULL(A.AgentOutCallsTalkTime,0)), \r\n" +
        "AgentTerminatedCalls = SUM(ISNULL(A.AgentTerminatedCalls,0)), \r\n" +
          "CallbackMessages = SUM(ISNULL(A.CallbackMessages,0)), \r\n" +
        "CallbackMessagesTime = SUM(ISNULL(A.CallbackMessagesTime,0)), \r\n" +
        "ConsultativeCallsTime = SUM(ISNULL(A.ConsultativeCallsTime,0)), \r\n" +
        "ConferencedInCallsTime = SUM(ISNULL(A.ConferencedInCallsTime,0)), \r\n" +
        "ConferencedOutCallsTime = SUM(ISNULL(A.ConferencedOutCallsTime,0)), \r\n" +
        "HandledCallsTalkTime = SUM(ISNULL(A.HandledCallsTalkTime,0)), \r\n" +
           "InternalCallsRcvd = SUM(ISNULL(A.InternalCallsRcvd,0)), \r\n" +
        "InternalCallsRcvdTime = SUM(ISNULL(A.InternalCallsRcvdTime,0)), \r\n" +
             "InternalCalls = SUM(ISNULL(A.InternalCalls,0)), \r\n" +
           "InternalCallsTime = SUM(ISNULL(A.InternalCallsTime,0)), \r\n" +
        "TransferredInCallsTime = SUM(ISNULL(A.TransferredInCallsTime,0)), \r\n" +
             "TalkOtherTime = SUM(ISNULL(A.TalkOtherTime,0)), \r\n" +
             "TalkOutTime = SUM(ISNULL(A.TalkOutTime,0)), \r\n" +
              "TimeZone = MAX(A.TimeZone), \r\n" +
           "InterruptedTime = SUM(ISNULL(A.InterruptedTime,0)), \r\n" +
          "WorkNotReadyTime = SUM(ISNULL(A.WorkNotReadyTime,0)), \r\n" +
             "WorkReadyTime = SUM(ISNULL(A.WorkReadyTime,0)), \r\n" +
        "NetConsultativeCalls = SUM(ISNULL(A.NetConsultativeCalls,0)), \r\n" +
        "NetConsultativeCallsTime = SUM(ISNULL(A.NetConsultativeCallsTime,0)), \r\n" +
        "NetConferencedOutCalls = SUM(ISNULL(A.NetConferencedOutCalls,0)), \r\n" +
        "NetConfOutCallsTime = SUM(ISNULL(A.NetConfOutCallsTime,0)), \r\n" +
        "NetTransferredOutCalls = SUM(ISNULL(A.NetTransferredOutCalls,0)), \r\n" +
              "DbDateTime = MAX(A.DbDateTime), \r\n" +
            "TransOut = SUM(ISNULL(A.TransferredOutCalls, 0) + ISNULL(A.NetTransferredOutCalls, 0)), \r\n" +
            "Assists = SUM(ISNULL(A.EmergencyAssists, 0) + ISNULL(A.SupervAssistCallsTime, 0)) \r\n" +
            "FROM (Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Skill_Group.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
              "FROM Skill_Group(nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock) \r\n" +
      "WHERE Skill_Group.SkillTargetID = Agent_Skill_Group_Interval.SkillGroupSkillTargetID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID \r\n" +
      "AND (Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID FROM Skill_Group (nolock) WHERE (Priority > 0) AND (Deleted <> 'Y'))) \r\n" +
      "UNION ALL \r\n" +
        "Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Precision_Queue.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
        "FROM Skill_Group (nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock), Precision_Queue(nolock) \r\n" +
      "WHERE Skill_Group.PrecisionQueueID = Agent_Skill_Group_Interval.PrecisionQueueID \r\n" +
      "AND Skill_Group.PrecisionQueueID = Precision_Queue.PrecisionQueueID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID)A \r\n" +
            "GROUP BY A.SGEnterpriseName, \r\n" +
            "A.SGSkillTargetID, \r\n" +
            "A.SkillTargetID, \r\n" +
            "A.Media, \r\n" +
    "A.MediaID, \r\n" +
            "A.DateTime, \r\n" +		 
            "A.SGPeripheralID) ASGI \r\n" +
       "WHERE Agent.SkillTargetID = ASGI.SkillTargetID \r\n" + 
       "AND Agent.PersonID = Person.PersonID  \r\n" +
       "AND Agent.SkillTargetID =  AI.SkillTargetID \r\n" +
        "AND Agent.PeripheralID = ASGI.SGPeripheralID \r\n" +
    "AND AI.MRDomainID = ASGI.MediaID \r\n" +
       "AND ASGI.DateTime = AI.DateTime \r\n" +
       "AND Agent.SkillTargetID  in (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5096,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5111,5112,5113,5114,5115,5116,5117,5118,5119,5120,5121,5122,5123,5124,5125,5126,5127,5128,5129,5130,5131,5132,5133,5134,5135,5136,5137,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457) \r\n" +
      " AND AI.DateTime >= '"+startPeriode+" 00:00:00' \r\n" +
       "AND AI.DateTime < '"+endPeriode+" 23:59:59' \r\n" +
     "GROUP BY Agent.SkillTargetID, ASGI.SGEnterpriseName, \r\n" +
          "ASGI.SGSkillTargetID, \r\n" +
          "Person.LastName, \r\n" +
          "Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +
          "AI.DateTime \r\n" +
     "ORDER BY Person.LastName + ',' + Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +
          "Agent.SkillTargetID, \r\n" +
          "ASGI.SGEnterpriseName, \r\n" +
          "AI.DateTime"
    ;
		// String sql = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" + 
		// 		"SELECT  Interval = SGI.DateTime,\r\n" + 
		// 		"		DATE = CONVERT(char(10),SGI.DateTime,101), \r\n" + 
		// 		"		FullName = Skill_Group.EnterpriseName,\r\n" + 
		// 		"		SkillGroupSkillID = Skill_Group.SkillTargetID,\r\n" + 
		// 		"		CallbackMessages = SUM(ISNULL(SGI.CallbackMessages,0)),\r\n" + 
		// 		"		CallbackMessagesTime = SUM(ISNULL(SGI.CallbackMessagesTime,0)),\r\n" + 
		// 		"		AvgHandledCallsTalkTime = AVG(ISNULL(SGI.AvgHandledCallsTalkTime,0)),\r\n" + 
		// 		"		HoldTime = SUM(ISNULL(SGI.HoldTime,0)),\r\n" + 
		// 		"		HandledCallsTalkTime = SUM(ISNULL(SGI.HandledCallsTalkTime,0)),\r\n" + 
		// 		"		InternalCalls = SUM(ISNULL(SGI.InternalCalls,0)),\r\n" + 
		// 		"		InternalCallsTime = SUM(ISNULL(SGI.InternalCallsTime,0)),\r\n" + 
		// 		"		CallsHandled = SUM(ISNULL(SGI.CallsHandled,0)),\r\n" + 
		// 		"		SupervAssistCalls = SUM(ISNULL(SGI.SupervAssistCalls,0)),\r\n" + 
		// 		"		AvgHandledCallsTime = AVG(ISNULL(SGI.AvgHandledCallsTime,0)),\r\n" + 
		// 		"		SupervAssistCallsTime = SUM(ISNULL(SGI.SupervAssistCallsTime,0)),\r\n" + 
		// 		"		HandledCallsTime = SUM(ISNULL(SGI.HandledCallsTime,0)),\r\n" + 
		// 		"		AgentOutCallsTime = SUM(ISNULL(SGI.AgentOutCallsTime,0)),\r\n" + 
		// 		"		TalkInTime = SUM(ISNULL(SGI.TalkInTime,0)),\r\n" + 
		// 		"		LoggedOnTime = SUM(ISNULL(SGI.LoggedOnTime,0)),\r\n" + 
		// 		"		ExternalOut = SUM(ISNULL(SGI.AgentOutCalls,0)),\r\n" + 
		// 		"		TalkOutTime = SUM(ISNULL(SGI.TalkOutTime,0)),\r\n" + 
		// 		"		TalkOtherTime = SUM(ISNULL(SGI.TalkOtherTime,0)),\r\n" + 
		// 		"		AvailTime = SUM(ISNULL(SGI.AvailTime,0)),\r\n" + 
		// 		"		NotReadyTime = SUM(ISNULL(SGI.NotReadyTime,0)),\r\n" + 
		// 		"		TransferInCalls = SUM(ISNULL(SGI.TransferInCalls,0)),\r\n" + 
		// 		"		TalkTime = SUM(ISNULL(SGI.TalkTime,0)),\r\n" + 
		// 		"		TransferInCallsTime = SUM(ISNULL(SGI.TransferInCallsTime,0)),\r\n" + 
		// 		"		WorkReadyTime = SUM(ISNULL(SGI.WorkReadyTime,0)),\r\n" + 
		// 		"		TransferOutCalls = SUM(ISNULL(SGI.TransferOutCalls,0)),\r\n" + 
		// 		"		WorkNotReadyTime = SUM(ISNULL(SGI.WorkNotReadyTime,0)),\r\n" + 
		// 		"		BusyOtherTime = SUM(ISNULL(SGI.BusyOtherTime,0)),\r\n" + 
		// 		"		CallsAnswered = SUM(ISNULL(SGI.CallsAnswered,0)),\r\n" + 
		// 		"		ReservedStateTime = SUM(ISNULL(SGI.ReservedStateTime,0)),\r\n" + 
		// 		"		AnswerWaitTime = SUM(ISNULL(SGI.AnswerWaitTime,0)),\r\n" + 
		// 		"		AbandonRingCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)),\r\n" + 
		// 		"		AbandonRingTime = SUM(ISNULL(SGI.AbandonRingTime,0)),\r\n" + 
		// 		"		AbandonHoldCalls = SUM(ISNULL(SGI.AbandonHoldCalls,0)),\r\n" + 
		// 		"		AgentOutCallsTalkTime = SUM(ISNULL(SGI.AgentOutCallsTalkTime,0)),\r\n" + 
		// 		"		AgentOutCallsOnHold = SUM(ISNULL(SGI.AgentOutCallsOnHold,0)),\r\n" + 
		// 		"		AgentOutCallsOnHoldTime = SUM(ISNULL(SGI.AgentOutCallsOnHoldTime,0)),\r\n" + 
		// 		"		AgentTerminatedCalls = SUM(ISNULL(SGI.AgentTerminatedCalls,0)),\r\n" + 
		// 		"		ConsultativeCalls = SUM(ISNULL(SGI.ConsultativeCalls,0)),\r\n" + 
		// 		"		ConsultativeCallsTime = SUM(ISNULL(SGI.ConsultativeCallsTime,0)),\r\n" + 
		// 		"		ConferencedInCalls = SUM(ISNULL(SGI.ConferencedInCalls,0)),\r\n" + 
		// 		"		ConferencedInCallsTime = SUM(ISNULL(SGI.ConferencedInCallsTime,0)),\r\n" + 
		// 		"		ConferencedOutCalls = SUM(ISNULL(SGI.ConferencedOutCalls,0)),\r\n" + 
		// 		"		ConferencedOutCallsTime = SUM(ISNULL(SGI.ConferencedOutCallsTime,0)),\r\n" + 
		// 		"		IncomingCallsOnHoldTime = SUM(ISNULL(SGI.IncomingCallsOnHoldTime,0)),\r\n" + 
		// 		"		IncomingCallsOnHold = SUM(ISNULL(SGI.IncomingCallsOnHold,0)),\r\n" + 
		// 		"		InternalCallsOnHoldTime = SUM(ISNULL(SGI.InternalCallsOnHoldTime,0)),\r\n" + 
		// 		"		InternalCallsOnHold = SUM(ISNULL(SGI.InternalCallsOnHold,0)),\r\n" + 
		// 		"		InternalCallsRcvdTime = SUM(ISNULL(SGI.InternalCallsRcvdTime,0)),\r\n" + 
		// 		"		InternalCallsRcvd = SUM(ISNULL(SGI.InternalCallsRcvd,0)),\r\n" + 
		// 		"		RedirectNoAnsCalls = SUM(ISNULL(SGI.RedirectNoAnsCalls,0)),\r\n" + 
		// 		"		RedirectNoAnsCallsTime = SUM(ISNULL(SGI.RedirectNoAnsCallsTime,0)),\r\n" + 
		// 		"		ShortCalls = SUM(ISNULL(SGI.ShortCalls,0)),\r\n" + 
		// 		"		RouterCallsAbandQ = SUM(ISNULL(SGI.RouterCallsAbandQ,0)),\r\n" + 
		// 		"		RouterQueueCalls = SUM(ISNULL(SGI.RouterQueueCalls,0)),\r\n" + 
		// 		"		AutoOutCalls = SUM(ISNULL(SGI.AutoOutCalls,0)),\r\n" + 
		// 		"		AutoOutCallsTime = SUM(ISNULL(SGI.AutoOutCallsTime,0)),\r\n" + 
		// 		"		AutoOutCallsTalkTime = SUM(ISNULL(SGI.AutoOutCallsTalkTime,0)),\r\n" + 
		// 		"		AutoOutCallsOnHold = SUM(ISNULL(SGI.AutoOutCallsOnHold,0)),\r\n" + 
		// 		"		AutoOutCallsOnHoldTime = SUM(ISNULL(SGI.AutoOutCallsOnHoldTime,0)),\r\n" + 
		// 		"		PreviewCalls = SUM(ISNULL(SGI.PreviewCalls,0)),\r\n" + 
		// 		"		PreviewCallsTime = SUM(ISNULL(SGI.PreviewCallsTime,0)),\r\n" + 
		// 		"		PreviewCallsTalkTime = SUM(ISNULL(SGI.PreviewCallsTalkTime,0)),\r\n" + 
		// 		"		PreviewCallsOnHold = SUM(ISNULL(SGI.PreviewCallsOnHold,0)),\r\n" + 
		// 		"		PreviewCallsOnHoldTime = SUM(ISNULL(SGI.PreviewCallsOnHoldTime,0)),\r\n" + 
		// 		"		ReserveCalls = SUM(ISNULL(SGI.ReserveCalls,0)),\r\n" + 
		// 		"		ReserveCallsTime = SUM(ISNULL(SGI.ReserveCallsTime,0)),\r\n" + 
		// 		"		ReserveCallsTalkTime = SUM(ISNULL(SGI.ReserveCallsTalkTime,0)),\r\n" + 
		// 		"		ReserveCallsOnHold = SUM(ISNULL(SGI.ReserveCallsOnHold,0)),\r\n" + 
		// 		"		ReserveCallsOnHoldTime = SUM(ISNULL(SGI.ReserveCallsOnHoldTime,0)),\r\n" + 
		// 		"		TalkAutoOutTime = SUM(ISNULL(SGI.TalkAutoOutTime,0)),\r\n" + 
		// 		"		TalkPreviewTime = SUM(ISNULL(SGI.TalkPreviewTime,0)),\r\n" + 
		// 		"		TalkReserveTime = SUM(ISNULL(SGI.TalkReserveTime,0)),\r\n" + 
		// 		"		BargeInCalls = SUM(ISNULL(SGI.BargeInCalls,0)),\r\n" + 
		// 		"		InterceptCalls = SUM(ISNULL(SGI.InterceptCalls,0)),\r\n" + 
		// 		"		MonitorCalls = SUM(ISNULL(SGI.MonitorCalls,0)),\r\n" + 
		// 		"		WhisperCalls = SUM(ISNULL(SGI.WhisperCalls,0)),\r\n" + 
		// 		"		EmergencyAssists = SUM(ISNULL(SGI.EmergencyAssists,0)),\r\n" + 
		// 		"		CallsOffered = SUM(ISNULL(SGI.CallsHandled,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)) + SUM(ISNULL(SGI.AbandonRingCalls,0))+ SUM(ISNULL(SGI.RedirectNoAnsCalls,0)),\r\n" + 
		// 		"		CallsQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)), \r\n" + 
		// 		"		InterruptedTime = SUM(ISNULL(SGI.InterruptedTime,0)),\r\n" + 
		// 		"		TimeZone = SGI.TimeZone,\r\n" + 
		// 		"		RouterCallsOffered = SUM(ISNULL(SGI.RouterCallsOffered,  0)),  \r\n" + 
		// 		"		RouterCallsAgentAbandons = SUM(ISNULL(SGI.RouterCallsAbandToAgent,0)),   \r\n" + 
		// 		"		RouterCallsDequeued = SUM(ISNULL(SGI.RouterCallsDequeued,0)),   \r\n" + 
		// 		"		RouterError = SUM(ISNULL(SGI.RouterError,0)),  \r\n" + 
		// 		"		DoNotUseSLTop = CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
		// 		"			WHEN 1 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 \r\n" + 
		// 		"			WHEN 2 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0\r\n" + 
		// 		"			WHEN 3 THEN (sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 \r\n" + 
		// 		"			ELSE 0 END,\r\n" + 
		// 		"		DoNotUseSLBottom = CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
		// 		"			WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END\r\n" + 
		// 		"			WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
		// 		"			WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0))- sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
		// 		"			ELSE 0 END,\r\n" + 
		// 		"		ServicelLevel=CASE min(isnull(Skill_Group.ServiceLevelType,0))\r\n" + 
		// 		"			WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE	\r\n" + 
		// 		"			sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 /\r\n" + 
		// 		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END\r\n" + 
		// 		"			WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE\r\n" + 
		// 		"			sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 /\r\n" + 
		// 		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
		// 		"			WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE\r\n" + 
		// 		"			(sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 /\r\n" + 
		// 		"                        (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END\r\n" + 
		// 		"			ELSE 0 END, \r\n" + 
		// 		"		ServiceLevelCalls = SUM(ISNULL(SGI.ServiceLevelCalls,0)),   \r\n" + 
		// 		"		ServiceLevelCallsAband = SUM(ISNULL(SGI.ServiceLevelCallsAband,  0)),  \r\n" + 
		// 		"		ServiceLevelCallsDequeue = SUM(ISNULL(SGI.ServiceLevelCallsDequeue,  0)),  \r\n" + 
		// 		"		ServiceLevelError =  SUM(ISNULL(SGI.ServiceLevelError,0)),   \r\n" + 
		// 		"		ServiceLevelRONA = SUM(ISNULL(SGI.ServiceLevelRONA,0)),   \r\n" + 
		// 		"		ServiceLevelCallsOffered = SUM(ISNULL(SGI.ServiceLevelCallsOffered,0)),\r\n" + 
		// 		"		NetConsultativeCalls = sum(isnull(SGI.NetConsultativeCalls,   0)),\r\n" + 
		// 		"		NetConsultativeCallsTime = SUM(ISNULL(SGI.NetConsultativeCallsTime,0)), \r\n" + 
		// 		"		NetConferencedOutCalls = sum(isnull( SGI.NetConferencedOutCalls,0)),\r\n" + 
		// 		"		NetConfOutCallsTime = SUM(ISNULL(SGI.NetConfOutCallsTime,0)),\r\n" + 
		// 		"		NetTransferredOutCalls = sum(isnull(SGI.NetTransferOutCalls,0)),\r\n" + 
		// 		"		DbDateTime = SGI.DbDateTime,\r\n" + 
		// 		"                ReportingInterval = SUM(ISNULL(SGI.ReportingInterval,0))*60,\r\n" + 
		// 		"		fte_AgentsLogonTotal = SUM(ISNULL(SGI.LoggedOnTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsNotReady = SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsNotActive = SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsActive = SUM(ISNULL(SGI.TalkTime,0)) * 1.0 /(SUM(ISNULL(SGI.ReportingInterval,0))*60),\r\n" + 
		// 		"		fte_AgentsWrapup = SUM(ISNULL(SGI.WorkReadyTime,0)+ISNULL(SGI.WorkNotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsOther = SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsHold = SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),   \r\n" + 
		// 		"		fte_AgentsReserved = SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60),\r\n" + 
		// 		"		ast_PercentNotActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.TalkTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentHoldTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentWrapTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE (SUM(ISNULL(SGI.WorkNotReadyTime + SGI.WorkReadyTime,0))) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentReservedTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentNotReadyTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		ast_PercentUtilization = \r\n" + 
		// 		"		CASE WHEN (SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) = 0 THEN 0		\r\n" + 
		// 		"				ELSE (CASE WHEN SUM(ISNULL(SGI.TalkTime,0)) = 0 THEN 0 ELSE (SUM(ISNULL(SGI.TalkInTime,0)) + \r\n" + 
		// 		"					 SUM(ISNULL(SGI.TalkOutTime,0)) + \r\n" + 
		// 		"					 SUM(ISNULL(SGI.TalkOtherTime,  0)) + \r\n" + 
		// 		"					 SUM(ISNULL(SGI.WorkReadyTime,0)) + \r\n" + 
		// 		"					 SUM(ISNULL(SGI.WorkNotReadyTime,0))) * 1.0 /\r\n" + 
		// 		"			  		(SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) END) END,\r\n" + 
		// 		"		asa =  CASE WHEN SUM(ISNULL(SGI.CallsAnswered,0)) = 0 THEN 0 \r\n" + 
		// 		"					ELSE SUM(ISNULL(SGI.AnswerWaitTime,0)) * 1.0 / SUM(ISNULL(SGI.CallsAnswered,0)) END,\r\n" + 
		// 		"		CompletedTasks_AHT =  (CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.HandledCallsTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END),\r\n" + 
		// 		"		CompletedTasks_AvgActiveTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0 \r\n" + 
		// 		"										ELSE SUM(ISNULL(SGI.HandledCallsTalkTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END,\r\n" + 
		// 		"		CompletedTasks_AvgWrapTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0 \r\n" + 
		// 		"										ELSE (SUM(ISNULL(SGI.WorkReadyTime,0)) + SUM(ISNULL(SGI.WorkNotReadyTime,0))) / SUM(ISNULL(SGI.CallsHandled,0)) END, \r\n" + 
		// 		"		ast_ActiveTime = SUM(ISNULL(SGI.TalkTime,0)),\r\n" + 
		// 		"		TotalQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)) + SUM(ISNULL(SGI.CallsQueued,0)),\r\n" + 
		// 		"		ast_PerBusyOtherTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0\r\n" + 
		// 		"			   ELSE SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END),\r\n" + 
		// 		"		Media = Media_Routing_Domain.EnterpriseName,\r\n" + 
		// 		"		MRDomainID = Media_Routing_Domain.MRDomainID,\r\n" + 
		// 		"		AbandCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)),\r\n" + 
		// 		"		RouterMaxCallsQueued = MAX(SGI.RouterMaxCallsQueued),\r\n" + 
		// 		"		RouterMaxCallWaitTime = MAX(SGI.RouterMaxCallWaitTime)\r\n" + 
		// 		"   FROM Skill_Group (nolock), \r\n" + 
		// 		"		Skill_Group_Interval SGI (nolock), \r\n" + 
		// 		"		Media_Routing_Domain (nolock)  \r\n" + 
		// 		"  WHERE (DATEPART(dw, SGI.DateTime) in(2,3,4,5,6,7,1) and SGI.DateTime between '"+startPeriode.toString()+" 00:00:00' and '"+endPeriode.toString()+" 23:59:59' and convert([char], SGI.DateTime, 108) between '00:00:00' and '23:59:59') and (Skill_Group.SkillTargetID IN (5029)) and (Skill_Group.SkillTargetID = SGI.SkillTargetID)  \r\n" + 
		// 		"         and (Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID)    \r\n" + 
		// 		"GROUP BY Skill_Group.EnterpriseName,   \r\n" + 
		// 		"		 Skill_Group.SkillTargetID,\r\n" + 
		// 		"		 Media_Routing_Domain.EnterpriseName,\r\n" + 
		// 		"		 Media_Routing_Domain.MRDomainID,\r\n" + 
		// 		"		 SGI.DateTime,\r\n" + 
		// 		"		 CONVERT(char(10),SGI.DateTime,101),\r\n" + 
		// 		"		 SGI.TimeZone,\r\n" + 
		// 		"		 SGI.DbDateTime\r\n" + 
		// 		"ORDER BY Media_Routing_Domain.EnterpriseName,\r\n" + 
		// 		"		 Skill_Group.EnterpriseName, \r\n" + 
		// 		"		 SGI.DateTime";
		return jdbcTemplate.query(sql, new AbandonCallRowMapper());
  }
  
  @Override
	public List<Queue> getCallsQueue() {
		String sql = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" +
    "SELECT Call_Type.EnterpriseName, \r\n" +
      "CTRT.CallTypeID, \r\n" +
      "CTRT.DateTime, \r\n" +
      "Date =CONVERT(char(10),CTRT.DateTime,101), \r\n" +
      "CTRT.AvgRouterDelayQHalf, \r\n" +
      "CTRT.AvgRouterDelayQNow, \r\n" +
      "CTRT.AvgRouterDelayQTo5, \r\n" +
      "CTRT.AvgRouterDelayQToday, \r\n" +
      "CTRT.CallsRoutedToday, \r\n" +
      "CTRT.CallsRoutedToHalf, \r\n" +
      "CTRT.ErrorCountToday, \r\n" +
      "CTRT.ErrorCountToHalf, \r\n" +
      "CTRT.ICRDefaultRoutedToday, \r\n" +
      "CTRT.ICRDefaultRoutedToHalf, \r\n" +
      "CTRT.MasterScriptID, \r\n" +
      "CTRT.NetworkDefaultRoutedToday, \r\n" +
      "CTRT.NetworkDefaultRoutedToHalf, \r\n" +
      "CTRT.ReturnBusyToday, \r\n" +
      "CTRT.ReturnBusyToHalf, \r\n" +
      "CTRT.ReturnRingToday, \r\n" +
      "CTRT.ReturnRingToHalf, \r\n" +
      "CTRT.RouterCallsAbandQHalf, \r\n" +
      "CTRT.RouterCallsAbandQTo5, \r\n" +
      "CTRT.RouterCallsAbandQToday, \r\n" +
      "CTRT.RouterCallsQNow, \r\n" +
      "CTRT.RouterCallsQNowTime, \r\n" +
      "routerlongestcallq=ISNULL(DATEDIFF(ss, CTRT.RouterLongestCallQ, (SELECT NowTime from Controller_Time)),0), \r\n" +
      "CTRT.RouterQueueCallsHalf, \r\n" + 
      "CTRT.RouterQueueCallsTo5, \r\n" +
      "CTRT.RouterQueueCallsToday, \r\n" +
      "CTRT.RouterQueueWaitTimeHalf, \r\n" +
      "CTRT.RouterQueueWaitTimeTo5, \r\n" +
      "CTRT.RouterQueueWaitTimeToday, \r\n" +
      "CTRT.ScriptID, \r\n" +
      "CTRT.NetworkAnnouncementToHalf, \r\n" +
      "CTRT.NetworkAnnouncementToday, \r\n" +
      "CTRT.AnswerWaitTimeTo5, \r\n" +
      "CTRT.CallsHandledTo5, \r\n" +
      "CTRT.CallsLeftQTo5, \r\n" +
      "CTRT.CallsOfferedTo5, \r\n" +
      "CTRT.DelayQAbandTimeTo5, \r\n" +
      "CTRT.DelayQAbandTimeToday, \r\n" +
      "CTRT.DelayQAbandTimeHalf, \r\n" +
      "CTRT.HandleTimeTo5, \r\n" +
      "CTRT.ServiceLevelAbandTo5, \r\n" + 
      "CTRT.ServiceLevelCallsOfferedTo5, \r\n" + 
      "CTRT.ServiceLevelCallsTo5, \r\n" +
      "DoNotUseSLTopTo5 = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN isnull(CTRT.ServiceLevelCallsTo5,0) * 1.0 \r\n" +
                         "WHEN 2 THEN isnull(CTRT.ServiceLevelCallsTo5,0) * 1.0 \r\n" +
                         "WHEN 3 THEN (isnull(CTRT.ServiceLevelCallsTo5,0) + isnull(CTRT.ServiceLevelAbandTo5,0)) * 1.0  \r\n" +
                         "ELSE 0 END, \r\n" +
        "DoNotUseSLBottomTo5 = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0) - isnull(CTRT.ServiceLevelAbandTo5,0)) <= 0 THEN 0 ELSE (isnull(CTRT.ServiceLevelCallsOfferedTo5,0) - isnull(CTRT.ServiceLevelAbandTo5,0)) END \r\n" +
                         "WHEN 2 THEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0)) \r\n" +
                         "WHEN 3 THEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0)) \r\n" +
                         "ELSE 0 END, \r\n" +
        "ServicelLevelTo5 = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0) - isnull(CTRT.ServiceLevelAbandTo5,0)) <= 0 THEN 0 \r\n" +
                        "ELSE isnull(CTRT.ServiceLevelCallsTo5,0) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedTo5,0) - isnull(CTRT.ServiceLevelAbandTo5,0))END) \r\n" +
                         "WHEN 2 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0)) <= 0 THEN 0 \r\n" +
                        "ELSE isnull(CTRT.ServiceLevelCallsTo5,0) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedTo5,0))END) \r\n" +
                         "WHEN 3 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedTo5,0)) <= 0 THEN 0 \r\n" +
                        "ELSE (isnull(CTRT.ServiceLevelCallsTo5,0) + isnull(CTRT.ServiceLevelAbandTo5,0)) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedTo5,0))END) \r\n" +
                         "ELSE 0 END, \r\n" +
      "CTRT.TalkTimeTo5, \r\n" +  
      "CTRT.ServiceLevelCallsQHeld, \r\n" +
      "CTRT.AnswerWaitTimeToday, \r\n" +
      "CTRT.CallsHandledToday, \r\n" +
      "CTRT.CallsOfferedToday, \r\n" +
      "CTRT.HandleTimeToday, \r\n" +
      "CTRT.ServiceLevelAbandToday, \r\n" + 
      "CTRT.ServiceLevelCallsOfferedToday, \r\n" + 
      "CTRT.ServiceLevelCallsToday, \r\n" +
      "DoNotUseSLTopToday = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN (isnull(CTRT.ServiceLevelCallsToday,0)) * 1.0 \r\n" + 
                         "WHEN 2 THEN (isnull(CTRT.ServiceLevelCallsToday,0)) * 1.0 \r\n" +
                         "WHEN 3 THEN (isnull(CTRT.ServiceLevelCallsToday,0) + isnull(CTRT.ServiceLevelAbandToday,0)) * 1.0 \r\n" +
                         "ELSE 0 END, \r\n" +
        "DoNotUseSLBottomToday = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0) - isnull(CTRT.ServiceLevelAbandToday,0)) <= 0 THEN 0 ELSE (isnull(CTRT.ServiceLevelCallsOfferedToday,0) - isnull(CTRT.ServiceLevelAbandToday,0)) END \r\n" +
                         "WHEN 2 THEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0)) \r\n" +
                         "WHEN 3 THEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0)) \r\n" +
                         "ELSE 0 END, \r\n" +
        "ServicelLevelToday = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0) - isnull(CTRT.ServiceLevelAbandToday,0)) <= 0 THEN 0 \r\n" +
                        "ELSE isnull(CTRT.ServiceLevelCallsToday,0) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedToday,0) - isnull(CTRT.ServiceLevelAbandToday,0))END) \r\n" +
                         "WHEN 2 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0)) <= 0 THEN 0 \r\n" +
                        "ELSE isnull(CTRT.ServiceLevelCallsToday,0) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedToday,0))END) \r\n" +
                         "WHEN 3 THEN (CASE WHEN (isnull(CTRT.ServiceLevelCallsOfferedToday,0)) <= 0 THEN 0 \r\n" +
                        "ELSE (isnull(CTRT.ServiceLevelCallsToday,0) + isnull(CTRT.ServiceLevelAbandToday,0)) * 1.0 / \r\n" +
                                    "(isnull(CTRT.ServiceLevelCallsOfferedToday,0))END) \r\n" +
                         "ELSE 0 END, \r\n" +
      "CTRT.TalkTimeToday, \r\n" +
      "CTRT.AnswerWaitTimeHalf, \r\n" +
      "CTRT.CallsHandledHalf, \r\n" +
      "CTRT.CallsOfferedHalf, \r\n" +
      "CTRT.HandleTimeHalf, \r\n" +
      "CTRT.ServiceLevelAbandHalf, \r\n" + 
      "CTRT.ServiceLevelCallsHalf, \r\n" +
      "CTRT.ServiceLevelCallsOfferedHalf, \r\n" + 	
      "DoNotUseSLTopHalf = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN (isnull(CTRT.ServiceLevelCallsHalf,0)) * 1.0 \r\n" + 
                         "WHEN 2 THEN (isnull(CTRT.ServiceLevelCallsHalf,0)) * 1.0 \r\n" +
                         "WHEN 3 THEN ((isnull(CTRT.ServiceLevelCallsHalf,0)) + (isnull(CTRT.ServiceLevelAbandHalf,0))) * 1.0 \r\n" +
                         "ELSE 0 END, \r\n" +
        "DoNotUseSLBottomHalf = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN CASE WHEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)) - (isnull(CTRT.ServiceLevelAbandHalf,0))) <= 0 THEN 0 ELSE ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)) - (isnull(CTRT.ServiceLevelAbandHalf,0))) END \r\n" +
                         "WHEN 2 THEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0))) \r\n" +
                         "WHEN 3 THEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0))) \r\n" +
                         "ELSE 0 END, \r\n" +
        "ServicelLevelHalf = CASE isnull(Call_Type.ServiceLevelType,0) \r\n" +
                         "WHEN 1 THEN (CASE WHEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)) - (isnull(CTRT.ServiceLevelAbandHalf,0))) <= 0 THEN 0 \r\n" +
                        "ELSE (isnull(CTRT.ServiceLevelCallsHalf,0)) * 1.0 / \r\n" +
                                    "((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)) - (isnull(CTRT.ServiceLevelAbandHalf,0)))END) \r\n" +
                         "WHEN 2 THEN (CASE WHEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0))) <= 0 THEN 0 \r\n" +
                        "ELSE (isnull(CTRT.ServiceLevelCallsHalf,0)) * 1.0 / \r\n" +
                                    "((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)))END) \r\n" +
                         "WHEN 3 THEN (CASE WHEN ((isnull(CTRT.ServiceLevelCallsOfferedHalf,0))) <= 0 THEN 0 \r\n" +
                        "ELSE ((isnull(CTRT.ServiceLevelCallsHalf,0)) + (isnull(CTRT.ServiceLevelAbandHalf,0))) * 1.0 / \r\n" +
                                    "((isnull(CTRT.ServiceLevelCallsOfferedHalf,0)))END) \r\n" +
                         "ELSE 0 END, \r\n" +
      "CTRT.TalkTimeHalf, \r\n" +   
      "CTRT.HoldTimeTo5, \r\n" +  
      "CTRT.HoldTimeHalf, \r\n " +
      "CTRT.HoldTimeToday, \r\n" +
      "CTRT.OverflowOutHalf, \r\n" +
      "CTRT.OverflowOutTo5, \r\n" +
      "CTRT.OverflowOutToday, \r\n" +
      "CTRT.CallsAnsweredTo5, \r\n" +
      "CTRT.CallsAnsweredHalf, \r\n" +
      "CTRT.CallsAnsweredToday,\r\n" +
      "CTRT.CallsRoutedNonAgentTo5, \r\n" + 
      "CTRT.CallsRoutedNonAgentHalf, \r\n" +
      "CTRT.CallsRoutedNonAgentToday, \r\n" +
      "CTRT.CallsRONATo5, \r\n" +
      "CTRT.CallsRONAHalf, \r\n" +
      "CTRT.CallsRONAToday, \r\n" +
      "CTRT.ReturnReleaseToday, \r\n" +
      "CTRT.ReturnReleaseHalf, \r\n" +
      "CTRT.RouterCallsAbandToAgentTo5, \r\n" +
      "CTRT.RouterCallsAbandToAgentHalf, \r\n" +
      "CTRT.RouterCallsAbandToAgentToday, \r\n" +
      "CTRT.TotalCallsAbandTo5, \r\n" +
      "CTRT.TotalCallsAbandToday, \r\n" +
      "CTRT.TotalCallsAbandHalf, \r\n" +
      "CTRT.DelayAgentAbandTimeToday, \r\n" +
      "CTRT.DelayAgentAbandTimeTo5, \r\n" +
      "CTRT.DelayAgentAbandTimeHalf, \r\n" +
      "CTRT.CallDelayAbandTimeToday, \r\n" +
      "CTRT.CallDelayAbandTimeTo5, \r\n" +
      "CTRT.CallDelayAbandTimeHalf, \r\n" +
      "CTRT.CTDelayAbandTimeToday, \r\n" +
      "CTRT.CTDelayAbandTimeTo5, \r\n" +
      "CTRT.CTDelayAbandTimeHalf, \r\n" +
      "CTRT.ServiceLevelErrorHalf, \r\n" +
      "CTRT.ServiceLevelErrorToday, \r\n" +
      "CTRT.ServiceLevelRONATo5, \r\n" +
      "CTRT.ServiceLevelRONAHalf, \r\n" +
      "CTRT.ServiceLevelRONAToday, \r\n" +
      "CTRT.AgentErrorCountHalf, \r\n" +
      "CTRT.AgentErrorCountToday, \r\n" +
      "CTRT.CallsAtVRUNow, \r\n" +
      "CTRT.CallsAtAgentNow, \r\n" +
      "ActiveCalls = isnull(CTRT.CallsAtVRUNow,0) + ISNULL(CTRT.CallsAtAgentNow,0), \r\n" +
      "asa=ISNULL(CTRT.AnswerWaitTimeTo5/CTRT.CallsAnsweredTo5,0), \r\n" +
      "asatoday=ISNULL(CTRT.AnswerWaitTimeToday/CTRT.CallsAnsweredToday,0), \r\n" +
      "tasks_at_vru= (ISNULL(CTRT.CallsAtVRUNow,0) - isnull(CTRT.RouterCallsQNow,0)) , \r\n" + 
      "aad=ISNULL(CTRT.CallDelayAbandTimeTo5 / CTRT.TotalCallsAbandTo5, 0), \r\n" +
      "aht=ISNULL(CTRT.HandleTimeTo5 / CTRT.CallsHandledTo5, 0), \r\n" +
      "aadtoday=ISNULL(CTRT.CallDelayAbandTimeToday / CTRT.TotalCallsAbandToday, 0), \r\n" + 
      "ahttoday=ISNULL(CTRT.HandleTimeToday / CTRT.CallsHandledToday, 0), \r\n" +
      "other=ISNULL(CTRT.ReturnBusyToHalf,0) \r\n" +
              "+ ISNULL(CTRT.ReturnRingToHalf,0) \r\n" +
              "+ ISNULL(CTRT.ReturnReleaseHalf,0) \r\n" +
              "+ ISNULL(CTRT.CallsRONAHalf,0) \r\n" +
              "+ ISNULL(CTRT.CallsRoutedNonAgentHalf,0) \r\n" +
    "FROM Call_Type (nolock), \r\n" +
      "Call_Type_Real_Time CTRT(nolock)  \r\n" +
    "WHERE CTRT.CallTypeID IN (5000) and Call_Type.CallTypeID = CTRT.CallTypeID \r\n" +
    "Order BY Call_Type.EnterpriseName, \r\n" +
      "CTRT.DateTime";
		return jdbcTemplate.query(sql, new QueueRowMapper());
	}

  @Override
	public List<RedirectCall> getRedirectCalls(String startPeriode, String endPeriode) {
    String sql =  "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" +
    "SELECT	Media = ASGI.Media, \r\n" +
    "Interval	 = AI.DateTime, \r\n" +
        "Date		 = CONVERT(char(10),AI.DateTime,101), \r\n" +
               "FullName = Person.LastName + ', ' + Person.FirstName, \r\n" +
               "loginname = Person.LoginName, \r\n" +
         "AgentSkillID  = Agent.SkillTargetID, \r\n" +
         "SkillGroupName = ASGI.SGEnterpriseName, \r\n" +
         "SkillGroupSkillTargetID = ASGI.SGSkillTargetID, \r\n" +
       "AgentLoggedOnTime = SUM(ISNULL( AI.LoggedOnTime,0)), \r\n" +
      "AgentSGLoggedOnTime = SUM(ISNULL(ASGI.LoggedOnTime,0)), \r\n" +
           "AgentAvailTime = SUM(ISNULL(AI.AvailTime, 0)), \r\n" +
           "AgentNotReady = SUM(ISNULL(AI.NotReadyTime, 0)) , \r\n" +
      "AgentBusyOtherTime = SUM(ISNULL(AI.LoggedOnTime - AI.AvailTime,0)), \r\n" +
         "--Need to 're-sum' in order to make SQL think these are aggregate functions \r\n" +
            "CallsAnswered  = sum(ASGI.CallsAnswered), \r\n" +
            "CallsHandled = sum(ASGI.CallsHandled), \r\n" +
            "AbandRingCalls = sum(ASGI.AbandRingCalls), \r\n" +
          "AbandRingCallsTime = sum(ASGI.AbandRingCallsTime), \r\n" +
             "RedirectCalls = sum(ASGI.RedirectCalls), \r\n" +
           "RedirectCallsTime = sum(ASGI.RedirectCallsTime), \r\n" +
          "AbandonHoldCalls = sum(ASGI.AbandonHoldCalls), \r\n" +
           "TransferInCalls = sum(ASGI.TransferInCalls), \r\n" +
          "TransferOutCalls = sum(ASGI.TransferOutCalls), \r\n" +
           "ConsultativeCalls = sum(ASGI.ConsultativeCalls), \r\n" +
           "ConferenceInCalls = sum(ASGI.ConferenceInCalls), \r\n" +
          "ConferenceOutCalls = sum(ASGI.ConferenceOutCalls),\r\n" +
            "OutExtnCalls = sum(ASGI.OutExtnCalls), \r\n" +
              "ShortCalls = sum(ASGI.ShortCalls),\r\n" +
            "SupAssistCalls = sum(ASGI.SupAssistCalls), \r\n" +
            "BargeInCalls = sum(ASGI.BargeInCalls), \r\n" +
            "InterceptCalls = sum(ASGI.InterceptCalls), \r\n" +
            "MonitorCalls = sum(ASGI.MonitorCalls), \r\n" +
            "WhisperCalls = sum(ASGI.WhisperCalls), \r\n" +
        "EmergencyAssistCalls = sum(ASGI.EmergencyAssistCalls), \r\n" +
          "SupAssistCallsTime = sum(ASGI.SupAssistCallsTime), \r\n" +
        "AgentOutCallsOnHoldTime = sum(ASGI.AgentOutCallsOnHoldTime), \r\n" +
             "InCallsOnHold = sum(ASGI.InCallsOnHold), \r\n" +
           "InCallsOnHoldTime = sum(ASGI.InCallsOnHoldTime), \r\n" +
            "IntCallsOnHold = sum(ASGI.IntCallsOnHold), \r\n" +
          "IntCallsOnHoldTime = sum(ASGI.IntCallsOnHoldTime), \r\n" +
              "TalkTime = sum(ASGI.TalkTime), \r\n" +
          "HandledCallsTime = sum(ASGI.HandledCallsTime), \r\n" +
              "HoldTime = sum(ASGI.HoldTime), \r\n" +
            "ReservedTime = sum(ASGI.ReservedTime), \r\n" +
              "WrapTime = sum(ASGI.WrapTime), \r\n" +
            "AnswerWaitTime = sum(ASGI.AnswerWaitTime), \r\n" +
            "AutoOutCalls = sum(ASGI.AutoOutCalls), \r\n" +
          "AutoOutCallsTime = sum(ASGI.AutoOutCallsTime), \r\n" +
        "AutoOutCallsTalkTime = sum(ASGI.AutoOutCallsTalkTime), \r\n" +
          "AutoOutCallsOnHold = sum(ASGI.AutoOutCallsOnHold), \r\n" +
        "AutoOutCallsOnHoldTime = sum(ASGI.AutoOutCallsOnHoldTime), \r\n" +
            "PreviewCalls = sum(ASGI.PreviewCalls), \r\n" +
          "PreviewCallsTime = sum(ASGI.PreviewCallsTime), \r\n" +
        "PreviewCallsTalkTime = sum(ASGI.PreviewCallsTalkTime), \r\n" +
          "PreviewCallsOnHold = sum(ASGI.PreviewCallsOnHold), \r\n" +
        "PreviewCallsOnHoldTime = sum(ASGI.PreviewCallsOnHoldTime), \r\n" +
            "ReserveCalls = sum(ASGI.ReserveCalls), \r\n" +
          "ReserveCallsTime = sum(ASGI.ReserveCallsTime), \r\n" +
        "ReserveCallsTalkTime = sum(ASGI.ReserveCallsTalkTime), \r\n" +
          "ReserveCallsOnHold = sum(ASGI.ReserveCallsOnHold), \r\n" +
        "ReserveCallsOnHoldTime = sum(ASGI.ReserveCallsOnHoldTime), \r\n" +
           "TalkAutoOutTime = sum(ASGI.TalkAutoOutTime), \r\n" +
           "TalkPreviewTime = sum(ASGI.TalkPreviewTime), \r\n" +
           "TalkReserveTime = sum(ASGI.TalkReserveTime), \r\n" +
    "AgentOutCallsTime= SUM(ISNULL(ASGI.AgentOutCallsTime, 0)), \r\n" +
        "AgentOutCallsTalkTime = sum(ASGI.AgentOutCallsTalkTime), \r\n" +
        "AgentTerminatedCalls = sum(ASGI.AgentTerminatedCalls), \r\n" +
          "CallbackMessages = sum(ASGI.CallbackMessages), \r\n" +
        "CallbackMessagesTime = sum(ASGI.CallbackMessagesTime), \r\n" +
        "ConsultativeCallsTime = sum(ASGI.ConsultativeCallsTime), \r\n" +
        "ConferencedInCallsTime = sum(ASGI.ConferencedInCallsTime), \r\n" +
        "ConferencedOutCallsTime = sum(ASGI.ConferencedOutCallsTime),\r\n" +
        "HandledCallsTalkTime = sum(ASGI.HandledCallsTalkTime), \r\n" +
           "InternalCallsRcvd = sum(ASGI.InternalCallsRcvd), \r\n" +
        "InternalCallsRcvdTime = sum(ASGI.InternalCallsRcvdTime), \r\n" +
             "InternalCalls = sum(ASGI.InternalCalls),\r\n" +
           "InternalCallsTime = sum(ASGI.InternalCallsTime), \r\n" +
        "TransferredInCallsTime = sum(ASGI.TransferredInCallsTime),\r\n" +
             "TalkOtherTime = sum(ASGI.TalkOtherTime), \r\n" +
             "TalkOutTime = sum(ASGI.TalkOutTime), \r\n" +
              "TimeZone = sum(ASGI.TimeZone), \r\n" +
           "InterruptedTime = sum(ASGI.InterruptedTime), \r\n" +
          "WorkNotReadyTime = sum(ASGI.WorkNotReadyTime),\r\n" +
             "WorkReadyTime = sum(ASGI.WorkReadyTime), \r\n" +
        "NetConsultativeCalls = sum(ASGI.NetConsultativeCalls), \r\n" +
        "NetConsultativeCallsTime = sum(ASGI.NetConsultativeCallsTime),\r\n" +
        "NetConferencedOutCalls = sum(ASGI.NetConferencedOutCalls), \r\n" +
        "NetConfOutCallsTime = sum(ASGI.NetConfOutCallsTime), \r\n" +
        "NetTransferredOutCalls = sum(ASGI.NetTransferredOutCalls), \r\n" +
              "DbDateTime = max(ASGI.DbDateTime), \r\n" +
                "Assists = SUM(ASGI.Assists), \r\n" +
               "TransOut = SUM(ASGI.TransOut), \r\n" +
        "AHT = ISNULL(SUM(ASGI.HandledCallsTime) / SUM(ASGI.CallsHandled),0), \r\n" +
       "AHoldT = ISNULL(SUM(ASGI.InCallsOnHoldTime) / SUM(ASGI.InCallsOnHold),0), \r\n" +
       "perActiveTime = SUM(ASGI.TalkTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perHoldTime  = sum(ASGI.HoldTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perNotActive = ISNULL(SUM(AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
          "perNotReady = ISNULL(SUM(AI.NotReadyTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
         "perReserved = sum(ASGI.ReservedTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
           "perWrapup = sum(ASGI.WrapTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perBusyOther = ISNULL(SUM(AI.LoggedOnTime - AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
        "AACW = ISNULL((SUM(ASGI.WrapTime) / SUM(ASGI.CallsHandled)),0), \r\n" +
        "perWACW = ISNULL((SUM(ASGI.WrapTime) + SUM(ASGI.TalkTime) + SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0)) )* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0), \r\n" +
        "perWOACW = ISNULL((SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0))+ SUM(ASGI.TalkTime))* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0) \r\n" +
        
      "FROM Agent (nolock), \r\n" +
         "Agent_Interval AI (nolock), \r\n" +
         "Person (nolock), \r\n" +
         "--This nested Select statement is necessary in order to make AgentLoggedOnTime, AgentAvailTime, and AgentNotReadyTime work correctly  \r\n" +
        "(Select   Media		 = A.Media, \r\n" +
    "MediaID = A.MediaID,\r\n" +
             "A.DateTime, \r\n" +
             "A.SkillTargetID, \r\n" +
             "A.SGEnterpriseName, \r\n" +
             "A.SGSkillTargetID, \r\n" +
         "A.SGPeripheralID, \r\n" +
             "CallsAnswered  = SUM(ISNULL(A.CallsAnswered,0)), \r\n" +
              "LoggedOnTime = SUM(ISNULL(A.LoggedOnTime,0)), \r\n" +
            "CallsHandled = SUM(ISNULL(A.CallsHandled,0)), \r\n" +
            "AbandRingCalls = SUM(ISNULL(A.AbandonRingCalls,0)), \r\n" +
          "AbandRingCallsTime = SUM(ISNULL(A.AbandonRingTime,0)), \r\n" +
             "RedirectCalls = SUM(ISNULL(A.RedirectNoAnsCalls,0)), \r\n" +
           "RedirectCallsTime = SUM(ISNULL(A.RedirectNoAnsCallsTime,0)), \r\n" +
          "AbandonHoldCalls = SUM(ISNULL(A.AbandonHoldCalls,0)),\r\n" +
           "TransferInCalls = SUM(ISNULL(A.TransferredInCalls,0)), \r\n" +
          "TransferOutCalls = SUM(ISNULL(A.TransferredOutCalls,0)), \r\n" +
           "ConsultativeCalls = SUM(ISNULL(A.ConsultativeCalls,0)), \r\n" +
           "ConferenceInCalls = SUM(ISNULL(A.ConferencedInCalls,0)), \r\n" +
          "ConferenceOutCalls = SUM(ISNULL(A.ConferencedOutCalls,0)), \r\n" +
            "OutExtnCalls = SUM(ISNULL(A.AgentOutCalls,0)), \r\n" +
              "ShortCalls = SUM(ISNULL(A.ShortCalls,0)), \r\n" +
            "SupAssistCalls = SUM(ISNULL(A.SupervAssistCalls,0)), \r\n" +
            "BargeInCalls = SUM(ISNULL(A.BargeInCalls,0)), \r\n" +
            "InterceptCalls = SUM(ISNULL(A.InterceptCalls,0)), \r\n" +
            "MonitorCalls = SUM(ISNULL(A.MonitorCalls,0)),\r\n" +
            "WhisperCalls = SUM(ISNULL(A.WhisperCalls,0)), \r\n" +
        "EmergencyAssistCalls = SUM(ISNULL(A.EmergencyAssists,0)), \r\n" +
          "SupAssistCallsTime = SUM(ISNULL(A.SupervAssistCallsTime,0)), \r\n" +
        "AgentOutCallsOnHoldTime = SUM(ISNULL(A.AgentOutCallsOnHoldTime,0)), \r\n" +
             "InCallsOnHold = SUM(ISNULL(A.IncomingCallsOnHold,0)), \r\n" +
           "InCallsOnHoldTime = SUM(ISNULL(A.IncomingCallsOnHoldTime,0)), \r\n" + 
            "IntCallsOnHold = SUM(ISNULL(A.InternalCallsOnHold,0)), \r\n" +
          "IntCallsOnHoldTime = SUM(ISNULL(A.InternalCallsOnHoldTime,0)), \r\n" +
              "TalkTime = sum(isnull(A.TalkInTime,0)) +  \r\n" +
                     "sum(isnull(A.TalkOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkOtherTime,0)) + \r\n" +
                     "sum(isnull(A.TalkAutoOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkPreviewTime,0)) + \r\n" +
                     "sum(isnull(A.TalkReserveTime,0)), \r\n" +
          "HandledCallsTime = SUM(ISNULL(A.HandledCallsTime,0)), \r\n" +
              "HoldTime = SUM(ISNULL(A.HoldTime,0)), \r\n" +
            "ReservedTime = SUM(ISNULL(A.ReservedStateTime,0)), \r\n" +
              "WrapTime = SUM(ISNULL(A.WorkNotReadyTime + A.WorkReadyTime,0)), \r\n" +
           "AnswerWaitTime = SUM(ISNULL(A.AnswerWaitTime,0)), \r\n" +
            "AutoOutCalls = SUM(ISNULL(A.AutoOutCalls,0)), \r\n" +
          "AutoOutCallsTime = SUM(ISNULL(A.AutoOutCallsTime,0)), \r\n" +
        "AutoOutCallsTalkTime = SUM(ISNULL(A.AutoOutCallsTalkTime,0)), \r\n" +
          "AutoOutCallsOnHold = SUM(ISNULL(A.AutoOutCallsOnHold,0)), \r\n" +
        "AutoOutCallsOnHoldTime = SUM(ISNULL(A.AutoOutCallsOnHoldTime,0)), \r\n" +
            "PreviewCalls = SUM(ISNULL(A.PreviewCalls,0)), \r\n" +
          "PreviewCallsTime = SUM(ISNULL(A.PreviewCallsTime,0)), \r\n" +
        "PreviewCallsTalkTime = SUM(ISNULL(A.PreviewCallsTalkTime,0)), \r\n" +
          "PreviewCallsOnHold = SUM(ISNULL(A.PreviewCallsOnHold,0)), \r\n" +
        "PreviewCallsOnHoldTime = SUM(ISNULL(A.PreviewCallsOnHoldTime,0)), \r\n" +
            "ReserveCalls = SUM(ISNULL(A.ReserveCalls,0)), \r\n" +
          "ReserveCallsTime = SUM(ISNULL(A.ReserveCallsTime,0)), \r\n" +
        "ReserveCallsTalkTime = SUM(ISNULL(A.ReserveCallsTalkTime,0)), \r\n" +
          "ReserveCallsOnHold = SUM(ISNULL(A.ReserveCallsOnHold,0)), \r\n" +
        "ReserveCallsOnHoldTime = SUM(ISNULL(A.ReserveCallsOnHoldTime,0)), \r\n" +
           "TalkAutoOutTime = SUM(ISNULL(A.TalkAutoOutTime,0)), \r\n" +
           "TalkPreviewTime = SUM(ISNULL(A.TalkPreviewTime,0)), \r\n" +
           "TalkReserveTime = SUM(ISNULL(A.TalkReserveTime,0)), \r\n" +
    "AgentOutCallsTime = SUM(ISNULL(A.AgentOutCallsTime,0)), \r\n" +
        "AgentOutCallsTalkTime = SUM(ISNULL(A.AgentOutCallsTalkTime,0)), \r\n" +
        "AgentTerminatedCalls = SUM(ISNULL(A.AgentTerminatedCalls,0)), \r\n" +
          "CallbackMessages = SUM(ISNULL(A.CallbackMessages,0)), \r\n" +
        "CallbackMessagesTime = SUM(ISNULL(A.CallbackMessagesTime,0)), \r\n" +
        "ConsultativeCallsTime = SUM(ISNULL(A.ConsultativeCallsTime,0)), \r\n" +
        "ConferencedInCallsTime = SUM(ISNULL(A.ConferencedInCallsTime,0)), \r\n" +
        "ConferencedOutCallsTime = SUM(ISNULL(A.ConferencedOutCallsTime,0)), \r\n" +
        "HandledCallsTalkTime = SUM(ISNULL(A.HandledCallsTalkTime,0)), \r\n" +
           "InternalCallsRcvd = SUM(ISNULL(A.InternalCallsRcvd,0)), \r\n" +
        "InternalCallsRcvdTime = SUM(ISNULL(A.InternalCallsRcvdTime,0)), \r\n" +
             "InternalCalls = SUM(ISNULL(A.InternalCalls,0)), \r\n" +
           "InternalCallsTime = SUM(ISNULL(A.InternalCallsTime,0)), \r\n" +
        "TransferredInCallsTime = SUM(ISNULL(A.TransferredInCallsTime,0)), \r\n" +
             "TalkOtherTime = SUM(ISNULL(A.TalkOtherTime,0)), \r\n" +
             "TalkOutTime = SUM(ISNULL(A.TalkOutTime,0)), \r\n" +
              "TimeZone = MAX(A.TimeZone), \r\n" +
           "InterruptedTime = SUM(ISNULL(A.InterruptedTime,0)), \r\n" +
          "WorkNotReadyTime = SUM(ISNULL(A.WorkNotReadyTime,0)), \r\n" +
             "WorkReadyTime = SUM(ISNULL(A.WorkReadyTime,0)), \r\n" +
        "NetConsultativeCalls = SUM(ISNULL(A.NetConsultativeCalls,0)), \r\n" +
        "NetConsultativeCallsTime = SUM(ISNULL(A.NetConsultativeCallsTime,0)), \r\n" +
        "NetConferencedOutCalls = SUM(ISNULL(A.NetConferencedOutCalls,0)), \r\n" +
        "NetConfOutCallsTime = SUM(ISNULL(A.NetConfOutCallsTime,0)), \r\n" +
        "NetTransferredOutCalls = SUM(ISNULL(A.NetTransferredOutCalls,0)), \r\n" +
              "DbDateTime = MAX(A.DbDateTime), \r\n" +
            "TransOut = SUM(ISNULL(A.TransferredOutCalls, 0) + ISNULL(A.NetTransferredOutCalls, 0)), \r\n" +
            "Assists = SUM(ISNULL(A.EmergencyAssists, 0) + ISNULL(A.SupervAssistCallsTime, 0)) \r\n" +
            "FROM (Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Skill_Group.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
              "FROM Skill_Group(nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock) \r\n" +
      "WHERE Skill_Group.SkillTargetID = Agent_Skill_Group_Interval.SkillGroupSkillTargetID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID \r\n" +
      "AND (Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID FROM Skill_Group (nolock) WHERE (Priority > 0) AND (Deleted <> 'Y'))) \r\n" +
      "UNION ALL \r\n" +
        "Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Precision_Queue.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
        "FROM Skill_Group (nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock), Precision_Queue(nolock) \r\n" +
      "WHERE Skill_Group.PrecisionQueueID = Agent_Skill_Group_Interval.PrecisionQueueID \r\n" +
      "AND Skill_Group.PrecisionQueueID = Precision_Queue.PrecisionQueueID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID)A \r\n" +
            "GROUP BY A.SGEnterpriseName, \r\n" +
            "A.SGSkillTargetID, \r\n" +
            "A.SkillTargetID, \r\n" +
            "A.Media, \r\n" +
    "A.MediaID, \r\n" +
            "A.DateTime, \r\n" +		 
            "A.SGPeripheralID) ASGI \r\n" +
       "WHERE Agent.SkillTargetID = ASGI.SkillTargetID \r\n" + 
       "AND Agent.PersonID = Person.PersonID  \r\n" +
       "AND Agent.SkillTargetID =  AI.SkillTargetID \r\n" +
        "AND Agent.PeripheralID = ASGI.SGPeripheralID \r\n" +
    "AND AI.MRDomainID = ASGI.MediaID \r\n" +
       "AND ASGI.DateTime = AI.DateTime \r\n" +
       "AND Agent.SkillTargetID  in (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5096,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5111,5112,5113,5114,5115,5116,5117,5118,5119,5120,5121,5122,5123,5124,5125,5126,5127,5128,5129,5130,5131,5132,5133,5134,5135,5136,5137,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457) \r\n" +
      " AND AI.DateTime >= '"+startPeriode+" 00:00:00' \r\n" +
       "AND AI.DateTime < '"+endPeriode+" 23:59:59' \r\n" +
     "GROUP BY Agent.SkillTargetID, ASGI.SGEnterpriseName, \r\n" +
          "ASGI.SGSkillTargetID, \r\n" +
          "Person.LastName, \r\n" +
          "Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +
          "AI.DateTime \r\n" +
     "ORDER BY Person.LastName + ',' + Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +
          "Agent.SkillTargetID, \r\n" +
          "ASGI.SGEnterpriseName, \r\n" +
          "AI.DateTime"
    ;

    return jdbcTemplate.query(sql, new RedirectCallRowMapper());
  }

  @Override
	public List<RedirectCall> getRedirectCallsByAgent(String startPeriode, String endPeriode,String agentLoginName) {
    String sql =  "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON \r\n" +
    "SELECT	Media = ASGI.Media, \r\n" +
    "Interval	 = AI.DateTime, \r\n" +
        "Date		 = CONVERT(char(10),AI.DateTime,101), \r\n" +
               "FullName = Person.LastName + ', ' + Person.FirstName, \r\n" +
               "loginname = Person.LoginName, \r\n" +
         "AgentSkillID  = Agent.SkillTargetID, \r\n" +
         "SkillGroupName = ASGI.SGEnterpriseName, \r\n" +
         "SkillGroupSkillTargetID = ASGI.SGSkillTargetID, \r\n" +
       "AgentLoggedOnTime = SUM(ISNULL( AI.LoggedOnTime,0)), \r\n" +
      "AgentSGLoggedOnTime = SUM(ISNULL(ASGI.LoggedOnTime,0)), \r\n" +
           "AgentAvailTime = SUM(ISNULL(AI.AvailTime, 0)), \r\n" +
           "AgentNotReady = SUM(ISNULL(AI.NotReadyTime, 0)) , \r\n" +
      "AgentBusyOtherTime = SUM(ISNULL(AI.LoggedOnTime - AI.AvailTime,0)), \r\n" +
         "--Need to 're-sum' in order to make SQL think these are aggregate functions \r\n" +
            "CallsAnswered  = sum(ASGI.CallsAnswered), \r\n" +
            "CallsHandled = sum(ASGI.CallsHandled), \r\n" +
            "AbandRingCalls = sum(ASGI.AbandRingCalls), \r\n" +
          "AbandRingCallsTime = sum(ASGI.AbandRingCallsTime), \r\n" +
             "RedirectCalls = sum(ASGI.RedirectCalls), \r\n" +
           "RedirectCallsTime = sum(ASGI.RedirectCallsTime), \r\n" +
          "AbandonHoldCalls = sum(ASGI.AbandonHoldCalls), \r\n" +
           "TransferInCalls = sum(ASGI.TransferInCalls), \r\n" +
          "TransferOutCalls = sum(ASGI.TransferOutCalls), \r\n" +
           "ConsultativeCalls = sum(ASGI.ConsultativeCalls), \r\n" +
           "ConferenceInCalls = sum(ASGI.ConferenceInCalls), \r\n" +
          "ConferenceOutCalls = sum(ASGI.ConferenceOutCalls),\r\n" +
            "OutExtnCalls = sum(ASGI.OutExtnCalls), \r\n" +
              "ShortCalls = sum(ASGI.ShortCalls),\r\n" +
            "SupAssistCalls = sum(ASGI.SupAssistCalls), \r\n" +
            "BargeInCalls = sum(ASGI.BargeInCalls), \r\n" +
            "InterceptCalls = sum(ASGI.InterceptCalls), \r\n" +
            "MonitorCalls = sum(ASGI.MonitorCalls), \r\n" +
            "WhisperCalls = sum(ASGI.WhisperCalls), \r\n" +
        "EmergencyAssistCalls = sum(ASGI.EmergencyAssistCalls), \r\n" +
          "SupAssistCallsTime = sum(ASGI.SupAssistCallsTime), \r\n" +
        "AgentOutCallsOnHoldTime = sum(ASGI.AgentOutCallsOnHoldTime), \r\n" +
             "InCallsOnHold = sum(ASGI.InCallsOnHold), \r\n" +
           "InCallsOnHoldTime = sum(ASGI.InCallsOnHoldTime), \r\n" +
            "IntCallsOnHold = sum(ASGI.IntCallsOnHold), \r\n" +
          "IntCallsOnHoldTime = sum(ASGI.IntCallsOnHoldTime), \r\n" +
              "TalkTime = sum(ASGI.TalkTime), \r\n" +
          "HandledCallsTime = sum(ASGI.HandledCallsTime), \r\n" +
              "HoldTime = sum(ASGI.HoldTime), \r\n" +
            "ReservedTime = sum(ASGI.ReservedTime), \r\n" +
              "WrapTime = sum(ASGI.WrapTime), \r\n" +
            "AnswerWaitTime = sum(ASGI.AnswerWaitTime), \r\n" +
            "AutoOutCalls = sum(ASGI.AutoOutCalls), \r\n" +
          "AutoOutCallsTime = sum(ASGI.AutoOutCallsTime), \r\n" +
        "AutoOutCallsTalkTime = sum(ASGI.AutoOutCallsTalkTime), \r\n" +
          "AutoOutCallsOnHold = sum(ASGI.AutoOutCallsOnHold), \r\n" +
        "AutoOutCallsOnHoldTime = sum(ASGI.AutoOutCallsOnHoldTime), \r\n" +
            "PreviewCalls = sum(ASGI.PreviewCalls), \r\n" +
          "PreviewCallsTime = sum(ASGI.PreviewCallsTime), \r\n" +
        "PreviewCallsTalkTime = sum(ASGI.PreviewCallsTalkTime), \r\n" +
          "PreviewCallsOnHold = sum(ASGI.PreviewCallsOnHold), \r\n" +
        "PreviewCallsOnHoldTime = sum(ASGI.PreviewCallsOnHoldTime), \r\n" +
            "ReserveCalls = sum(ASGI.ReserveCalls), \r\n" +
          "ReserveCallsTime = sum(ASGI.ReserveCallsTime), \r\n" +
        "ReserveCallsTalkTime = sum(ASGI.ReserveCallsTalkTime), \r\n" +
          "ReserveCallsOnHold = sum(ASGI.ReserveCallsOnHold), \r\n" +
        "ReserveCallsOnHoldTime = sum(ASGI.ReserveCallsOnHoldTime), \r\n" +
           "TalkAutoOutTime = sum(ASGI.TalkAutoOutTime), \r\n" +
           "TalkPreviewTime = sum(ASGI.TalkPreviewTime), \r\n" +
           "TalkReserveTime = sum(ASGI.TalkReserveTime), \r\n" +
    "AgentOutCallsTime= SUM(ISNULL(ASGI.AgentOutCallsTime, 0)), \r\n" +
        "AgentOutCallsTalkTime = sum(ASGI.AgentOutCallsTalkTime), \r\n" +
        "AgentTerminatedCalls = sum(ASGI.AgentTerminatedCalls), \r\n" +
          "CallbackMessages = sum(ASGI.CallbackMessages), \r\n" +
        "CallbackMessagesTime = sum(ASGI.CallbackMessagesTime), \r\n" +
        "ConsultativeCallsTime = sum(ASGI.ConsultativeCallsTime), \r\n" +
        "ConferencedInCallsTime = sum(ASGI.ConferencedInCallsTime), \r\n" +
        "ConferencedOutCallsTime = sum(ASGI.ConferencedOutCallsTime),\r\n" +
        "HandledCallsTalkTime = sum(ASGI.HandledCallsTalkTime), \r\n" +
           "InternalCallsRcvd = sum(ASGI.InternalCallsRcvd), \r\n" +
        "InternalCallsRcvdTime = sum(ASGI.InternalCallsRcvdTime), \r\n" +
             "InternalCalls = sum(ASGI.InternalCalls),\r\n" +
           "InternalCallsTime = sum(ASGI.InternalCallsTime), \r\n" +
        "TransferredInCallsTime = sum(ASGI.TransferredInCallsTime),\r\n" +
             "TalkOtherTime = sum(ASGI.TalkOtherTime), \r\n" +
             "TalkOutTime = sum(ASGI.TalkOutTime), \r\n" +
              "TimeZone = sum(ASGI.TimeZone), \r\n" +
           "InterruptedTime = sum(ASGI.InterruptedTime), \r\n" +
          "WorkNotReadyTime = sum(ASGI.WorkNotReadyTime),\r\n" +
             "WorkReadyTime = sum(ASGI.WorkReadyTime), \r\n" +
        "NetConsultativeCalls = sum(ASGI.NetConsultativeCalls), \r\n" +
        "NetConsultativeCallsTime = sum(ASGI.NetConsultativeCallsTime),\r\n" +
        "NetConferencedOutCalls = sum(ASGI.NetConferencedOutCalls), \r\n" +
        "NetConfOutCallsTime = sum(ASGI.NetConfOutCallsTime), \r\n" +
        "NetTransferredOutCalls = sum(ASGI.NetTransferredOutCalls), \r\n" +
              "DbDateTime = max(ASGI.DbDateTime), \r\n" +
                "Assists = SUM(ASGI.Assists), \r\n" +
               "TransOut = SUM(ASGI.TransOut), \r\n" +
        "AHT = ISNULL(SUM(ASGI.HandledCallsTime) / SUM(ASGI.CallsHandled),0), \r\n" +
       "AHoldT = ISNULL(SUM(ASGI.InCallsOnHoldTime) / SUM(ASGI.InCallsOnHold),0), \r\n" +
       "perActiveTime = SUM(ASGI.TalkTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perHoldTime  = sum(ASGI.HoldTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perNotActive = ISNULL(SUM(AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
          "perNotReady = ISNULL(SUM(AI.NotReadyTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
         "perReserved = sum(ASGI.ReservedTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
           "perWrapup = sum(ASGI.WrapTime) * 1.0 / SUM(ISNULL(AI.LoggedOnTime, 0)), \r\n" +
        "perBusyOther = ISNULL(SUM(AI.LoggedOnTime - AI.AvailTime) * 1.0 / SUM(AI.LoggedOnTime),0), \r\n" +
        "AACW = ISNULL((SUM(ASGI.WrapTime) / SUM(ASGI.CallsHandled)),0), \r\n" +
        "perWACW = ISNULL((SUM(ASGI.WrapTime) + SUM(ASGI.TalkTime) + SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0)) )* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0), \r\n" +
        "perWOACW = ISNULL((SUM(ASGI.HoldTime) + SUM(ISNULL(AI.NotReadyTime,0))+ SUM(ASGI.TalkTime))* 1.0 / SUM(ISNULL(AI.LoggedOnTime,0)),0) \r\n" +
        
      "FROM Agent (nolock), \r\n" +
         "Agent_Interval AI (nolock), \r\n" +
         "Person (nolock), \r\n" +
         "--This nested Select statement is necessary in order to make AgentLoggedOnTime, AgentAvailTime, and AgentNotReadyTime work correctly  \r\n" +
        "(Select   Media		 = A.Media, \r\n" +
    "MediaID = A.MediaID,\r\n" +
             "A.DateTime, \r\n" +
             "A.SkillTargetID, \r\n" +
             "A.SGEnterpriseName, \r\n" +
             "A.SGSkillTargetID, \r\n" +
         "A.SGPeripheralID, \r\n" +
             "CallsAnswered  = SUM(ISNULL(A.CallsAnswered,0)), \r\n" +
              "LoggedOnTime = SUM(ISNULL(A.LoggedOnTime,0)), \r\n" +
            "CallsHandled = SUM(ISNULL(A.CallsHandled,0)), \r\n" +
            "AbandRingCalls = SUM(ISNULL(A.AbandonRingCalls,0)), \r\n" +
          "AbandRingCallsTime = SUM(ISNULL(A.AbandonRingTime,0)), \r\n" +
             "RedirectCalls = SUM(ISNULL(A.RedirectNoAnsCalls,0)), \r\n" +
           "RedirectCallsTime = SUM(ISNULL(A.RedirectNoAnsCallsTime,0)), \r\n" +
          "AbandonHoldCalls = SUM(ISNULL(A.AbandonHoldCalls,0)),\r\n" +
           "TransferInCalls = SUM(ISNULL(A.TransferredInCalls,0)), \r\n" +
          "TransferOutCalls = SUM(ISNULL(A.TransferredOutCalls,0)), \r\n" +
           "ConsultativeCalls = SUM(ISNULL(A.ConsultativeCalls,0)), \r\n" +
           "ConferenceInCalls = SUM(ISNULL(A.ConferencedInCalls,0)), \r\n" +
          "ConferenceOutCalls = SUM(ISNULL(A.ConferencedOutCalls,0)), \r\n" +
            "OutExtnCalls = SUM(ISNULL(A.AgentOutCalls,0)), \r\n" +
              "ShortCalls = SUM(ISNULL(A.ShortCalls,0)), \r\n" +
            "SupAssistCalls = SUM(ISNULL(A.SupervAssistCalls,0)), \r\n" +
            "BargeInCalls = SUM(ISNULL(A.BargeInCalls,0)), \r\n" +
            "InterceptCalls = SUM(ISNULL(A.InterceptCalls,0)), \r\n" +
            "MonitorCalls = SUM(ISNULL(A.MonitorCalls,0)),\r\n" +
            "WhisperCalls = SUM(ISNULL(A.WhisperCalls,0)), \r\n" +
        "EmergencyAssistCalls = SUM(ISNULL(A.EmergencyAssists,0)), \r\n" +
          "SupAssistCallsTime = SUM(ISNULL(A.SupervAssistCallsTime,0)), \r\n" +
        "AgentOutCallsOnHoldTime = SUM(ISNULL(A.AgentOutCallsOnHoldTime,0)), \r\n" +
             "InCallsOnHold = SUM(ISNULL(A.IncomingCallsOnHold,0)), \r\n" +
           "InCallsOnHoldTime = SUM(ISNULL(A.IncomingCallsOnHoldTime,0)), \r\n" + 
            "IntCallsOnHold = SUM(ISNULL(A.InternalCallsOnHold,0)), \r\n" +
          "IntCallsOnHoldTime = SUM(ISNULL(A.InternalCallsOnHoldTime,0)), \r\n" +
              "TalkTime = sum(isnull(A.TalkInTime,0)) +  \r\n" +
                     "sum(isnull(A.TalkOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkOtherTime,0)) + \r\n" +
                     "sum(isnull(A.TalkAutoOutTime,0)) + \r\n" +
                     "sum(isnull(A.TalkPreviewTime,0)) + \r\n" +
                     "sum(isnull(A.TalkReserveTime,0)), \r\n" +
          "HandledCallsTime = SUM(ISNULL(A.HandledCallsTime,0)), \r\n" +
              "HoldTime = SUM(ISNULL(A.HoldTime,0)), \r\n" +
            "ReservedTime = SUM(ISNULL(A.ReservedStateTime,0)), \r\n" +
              "WrapTime = SUM(ISNULL(A.WorkNotReadyTime + A.WorkReadyTime,0)), \r\n" +
           "AnswerWaitTime = SUM(ISNULL(A.AnswerWaitTime,0)), \r\n" +
            "AutoOutCalls = SUM(ISNULL(A.AutoOutCalls,0)), \r\n" +
          "AutoOutCallsTime = SUM(ISNULL(A.AutoOutCallsTime,0)), \r\n" +
        "AutoOutCallsTalkTime = SUM(ISNULL(A.AutoOutCallsTalkTime,0)), \r\n" +
          "AutoOutCallsOnHold = SUM(ISNULL(A.AutoOutCallsOnHold,0)), \r\n" +
        "AutoOutCallsOnHoldTime = SUM(ISNULL(A.AutoOutCallsOnHoldTime,0)), \r\n" +
            "PreviewCalls = SUM(ISNULL(A.PreviewCalls,0)), \r\n" +
          "PreviewCallsTime = SUM(ISNULL(A.PreviewCallsTime,0)), \r\n" +
        "PreviewCallsTalkTime = SUM(ISNULL(A.PreviewCallsTalkTime,0)), \r\n" +
          "PreviewCallsOnHold = SUM(ISNULL(A.PreviewCallsOnHold,0)), \r\n" +
        "PreviewCallsOnHoldTime = SUM(ISNULL(A.PreviewCallsOnHoldTime,0)), \r\n" +
            "ReserveCalls = SUM(ISNULL(A.ReserveCalls,0)), \r\n" +
          "ReserveCallsTime = SUM(ISNULL(A.ReserveCallsTime,0)), \r\n" +
        "ReserveCallsTalkTime = SUM(ISNULL(A.ReserveCallsTalkTime,0)), \r\n" +
          "ReserveCallsOnHold = SUM(ISNULL(A.ReserveCallsOnHold,0)), \r\n" +
        "ReserveCallsOnHoldTime = SUM(ISNULL(A.ReserveCallsOnHoldTime,0)), \r\n" +
           "TalkAutoOutTime = SUM(ISNULL(A.TalkAutoOutTime,0)), \r\n" +
           "TalkPreviewTime = SUM(ISNULL(A.TalkPreviewTime,0)), \r\n" +
           "TalkReserveTime = SUM(ISNULL(A.TalkReserveTime,0)), \r\n" +
    "AgentOutCallsTime = SUM(ISNULL(A.AgentOutCallsTime,0)), \r\n" +
        "AgentOutCallsTalkTime = SUM(ISNULL(A.AgentOutCallsTalkTime,0)), \r\n" +
        "AgentTerminatedCalls = SUM(ISNULL(A.AgentTerminatedCalls,0)), \r\n" +
          "CallbackMessages = SUM(ISNULL(A.CallbackMessages,0)), \r\n" +
        "CallbackMessagesTime = SUM(ISNULL(A.CallbackMessagesTime,0)), \r\n" +
        "ConsultativeCallsTime = SUM(ISNULL(A.ConsultativeCallsTime,0)), \r\n" +
        "ConferencedInCallsTime = SUM(ISNULL(A.ConferencedInCallsTime,0)), \r\n" +
        "ConferencedOutCallsTime = SUM(ISNULL(A.ConferencedOutCallsTime,0)), \r\n" +
        "HandledCallsTalkTime = SUM(ISNULL(A.HandledCallsTalkTime,0)), \r\n" +
           "InternalCallsRcvd = SUM(ISNULL(A.InternalCallsRcvd,0)), \r\n" +
        "InternalCallsRcvdTime = SUM(ISNULL(A.InternalCallsRcvdTime,0)), \r\n" +
             "InternalCalls = SUM(ISNULL(A.InternalCalls,0)), \r\n" +
           "InternalCallsTime = SUM(ISNULL(A.InternalCallsTime,0)), \r\n" +
        "TransferredInCallsTime = SUM(ISNULL(A.TransferredInCallsTime,0)), \r\n" +
             "TalkOtherTime = SUM(ISNULL(A.TalkOtherTime,0)), \r\n" +
             "TalkOutTime = SUM(ISNULL(A.TalkOutTime,0)), \r\n" +
              "TimeZone = MAX(A.TimeZone), \r\n" +
           "InterruptedTime = SUM(ISNULL(A.InterruptedTime,0)), \r\n" +
          "WorkNotReadyTime = SUM(ISNULL(A.WorkNotReadyTime,0)), \r\n" +
             "WorkReadyTime = SUM(ISNULL(A.WorkReadyTime,0)), \r\n" +
        "NetConsultativeCalls = SUM(ISNULL(A.NetConsultativeCalls,0)), \r\n" +
        "NetConsultativeCallsTime = SUM(ISNULL(A.NetConsultativeCallsTime,0)), \r\n" +
        "NetConferencedOutCalls = SUM(ISNULL(A.NetConferencedOutCalls,0)), \r\n" +
        "NetConfOutCallsTime = SUM(ISNULL(A.NetConfOutCallsTime,0)), \r\n" +
        "NetTransferredOutCalls = SUM(ISNULL(A.NetTransferredOutCalls,0)), \r\n" +
              "DbDateTime = MAX(A.DbDateTime), \r\n" +
            "TransOut = SUM(ISNULL(A.TransferredOutCalls, 0) + ISNULL(A.NetTransferredOutCalls, 0)), \r\n" +
            "Assists = SUM(ISNULL(A.EmergencyAssists, 0) + ISNULL(A.SupervAssistCallsTime, 0)) \r\n" +
            "FROM (Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Skill_Group.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
              "FROM Skill_Group(nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock) \r\n" +
      "WHERE Skill_Group.SkillTargetID = Agent_Skill_Group_Interval.SkillGroupSkillTargetID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID \r\n" +
      "AND (Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID FROM Skill_Group (nolock) WHERE (Priority > 0) AND (Deleted <> 'Y'))) \r\n" +
      "UNION ALL \r\n" +
        "Select Agent_Skill_Group_Interval.*, SGPeripheralID = Skill_Group.PeripheralID, SGEnterpriseName = Precision_Queue.EnterpriseName, SGSkillTargetID = Skill_Group.SkillTargetID, Media = Media_Routing_Domain.EnterpriseName, MediaID = Media_Routing_Domain.MRDomainID \r\n" +
        "FROM Skill_Group (nolock), Agent_Skill_Group_Interval(nolock), Media_Routing_Domain(nolock), Precision_Queue(nolock) \r\n" +
      "WHERE Skill_Group.PrecisionQueueID = Agent_Skill_Group_Interval.PrecisionQueueID \r\n" +
      "AND Skill_Group.PrecisionQueueID = Precision_Queue.PrecisionQueueID \r\n" +
      "AND Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID)A \r\n" +
            "GROUP BY A.SGEnterpriseName, \r\n" +
            "A.SGSkillTargetID, \r\n" +
            "A.SkillTargetID, \r\n" +
            "A.Media, \r\n" +
    "A.MediaID, \r\n" +
            "A.DateTime, \r\n" +		 
            "A.SGPeripheralID) ASGI \r\n" +
       "WHERE Agent.SkillTargetID = ASGI.SkillTargetID \r\n" + 
       "AND Agent.PersonID = Person.PersonID  \r\n" +
       "AND Agent.SkillTargetID =  AI.SkillTargetID \r\n" +
        "AND Agent.PeripheralID = ASGI.SGPeripheralID \r\n" +
    "AND AI.MRDomainID = ASGI.MediaID \r\n" +
       "AND ASGI.DateTime = AI.DateTime \r\n" +
       "AND Agent.SkillTargetID  in (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5096,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5111,5112,5113,5114,5115,5116,5117,5118,5119,5120,5121,5122,5123,5124,5125,5126,5127,5128,5129,5130,5131,5132,5133,5134,5135,5136,5137,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457) \r\n" +
       " AND AI.DateTime >= '"+startPeriode+" 00:00:00' \r\n" +
       "AND AI.DateTime < '"+endPeriode+" 23:59:59' \r\n" +
       "AND Person.LoginName like '"+agentLoginName+"' \r\n" +
     "GROUP BY Agent.SkillTargetID, ASGI.SGEnterpriseName, \r\n" +
          "ASGI.SGSkillTargetID, \r\n" +
          "Person.LastName, \r\n" +
          "Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +
          "AI.DateTime \r\n" +
     "ORDER BY Person.LastName + ',' + Person.FirstName, \r\n" +
          "Person.LoginName, \r\n" +
          "ASGI.Media, \r\n" +          
          "Agent.SkillTargetID, \r\n" +
          "ASGI.SGEnterpriseName, \r\n" +
          "AI.DateTime"
    ;

    return jdbcTemplate.query(sql, new RedirectCallRowMapper());
  }

  @Override
	public List<StatusAux> statusAuxAgent(String startPeriode, String endPeriode,String agentLoginName) {
    String sql =  "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON; \r\n" +
    "WITH AgentNotReadyDetails(SkillTargetID, MRDomainID, LoginDateTime, Event, Duration, ReasonCode, DateTime, DbDateTime, TimeZone) AS \r\n" +
    "(Select aed.SkillTargetID, \r\n" +
        "aed.MRDomainID, \r\n" +
        "CONVERT(varchar, aed.LoginDateTime,120), \r\n" + 
        "aed.Event, \r\n" +
        "aed.Duration,  \r\n" +
        "aed.ReasonCode, \r\n" +
        "DateTime, \r\n" +
        "aed.DbDateTime, \r\n" +
        "aed.TimeZone \r\n" +
    "From Agent_Event_Detail aed  (nolock) \r\n" +
    "Where CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime)  \r\n" +
              "ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" +              
              "+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" +
                    "THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2) \r\n" +
                    "ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END) \r\n" +
              "+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" +
                      "WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" +
                      "WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" +
                      "ELSE '30' END ) \r\n" +
              "+ ':00' END >= '"+startPeriode+" 00:00:00' \r\n" +
    "AND CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime) \r\n" +
              "ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,aed.DateTime)) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" +
              "+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" +
                    "THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2) \r\n" +
                    "ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END) \r\n" +
              "+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" +
                    "WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" +
                    "WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" +
                    "ELSE '30' END ) \r\n" +
              "+ ':00' END < '"+endPeriode+" 23:59:59' \r\n" +
    "AND SkillTargetID IN (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)), \r\n" +      
    "AgentLogOutDetails(ALODSkillTargetID, ALODLoginDateTime, TotalLoginTime, ALODEvent) AS \r\n" +
    "(Select aed.SkillTargetID, \r\n" +
         "aed.LoginDateTime,  \r\n" +
         "TotalLoginTime = DATEDIFF(ss,LoginDateTime, DateTime), \r\n" +
         "aed.Event \r\n" +
    "From AgentNotReadyDetails aed \r\n" +
    "WHERE Event = 2), \r\n" +        
    "AgentNRDuration(SkillTargetID, MRDomainID, LoginDateTime, ReasonCodeDuration, ReasonCode) AS \r\n" +
    "(Select ANRD.SkillTargetID,  \r\n" +
        "ANRD.MRDomainID, \r\n" +
        "ANRD.LoginDateTime, \r\n" +
        "ReasonCodeDuration = SUM (ANRD.Duration), \r\n" +
        "ANRD.ReasonCode \r\n" +
      "From AgentNotReadyDetails ANRD \r\n" +
      "Where ANRD.Event = 3 \r\n" +
      "Group By ANRD.SkillTargetID, \r\n" +
           "ANRD.MRDomainID, \r\n" +
           "ANRD.LoginDateTime, \r\n" +
           "ANRD.ReasonCode), \r\n" +           
    "AgentNotReadyTotal(SkillTargetID, NotReadyTime,LoginDateTime) AS \r\n" +
    "(Select SkillTargetID, \r\n" +
        "SUM(ReasonCodeDuration), \r\n" +
        "LoginDateTime \r\n" +
      "From AgentNRDuration \r\n" +
      "GROUP BY SkillTargetID, \r\n" +
        "LoginDateTime) \r\n" +
    "SELECT SkillTargetID = ANRDU.SkillTargetID, \r\n" +
         "LoginDateTime = ANRDU.LoginDateTime, \r\n" +
              "LoginDate = cast(ANRDU.LoginDateTime as date), \r\n" +             
              "TotalLoginTime = CASE WHEN ALOD.ALODEvent = 2 THEN ALOD.TotalLoginTime \r\n" +
                        "ELSE 0 END, \r\n" +
         "TotalNotReadyTime = AgentNotReadyTotal.NotReadyTime, \r\n" +
         "ReasonCode = ANRDU.ReasonCode, \r\n" +
         "textReasonCode = ISNULL(Reason_Code.ReasonText, ' ')+'['+convert(varchar, ANRDU.ReasonCode)+']', \r\n" +
         "ReasonCodeDuration = ANRDU.ReasonCodeDuration, \r\n" +
               "FullName = Person.LastName + ', ' + Person.FirstName, \r\n" +
               "LoginName = Person.LoginName, \r\n" +
         "perNotReady = CASE WHEN ISNULL(AgentNotReadyTotal.NotReadyTime,0) = 0 THEN 0*1.0 ELSE ISNULL(ANRDU.ReasonCodeDuration,0)*1.0/ISNULL(AgentNotReadyTotal.NotReadyTime,0) END, \r\n" +
         "perLogon = CASE WHEN ALOD.ALODEvent = 2 THEN (ANRDU.ReasonCodeDuration*1.0/ALOD.TotalLoginTime) ELSE 0 END, TotalAgentLoginTime=(select sum(DATEDIFF(ss,LoginDateTime, DateTime)) from AgentNotReadyDetails aed \r\n" +
    "WHERE Event = 2 AND aed.SkillTargetID=ANRDU.SkillTargetID) \r\n" +
    "FROM AgentNRDuration ANRDU \r\n" +
       "LEFT JOIN Reason_Code ON ANRDU.ReasonCode=Reason_Code.ReasonCode \r\n" +
      "LEFT JOIN  AgentLogOutDetails ALOD ON (ANRDU.SkillTargetID = ALOD.ALODSkillTargetID \r\n" +
          "AND ANRDU.LoginDateTime <= DATEADD(ss, 2, ALOD.ALODLoginDateTime) AND ANRDU.LoginDateTime >= DATEADD(ss, -2, ALOD.ALODLoginDateTime)) \r\n" +
      "LEFT JOIN AgentNotReadyTotal ON (ANRDU.SkillTargetID = AgentNotReadyTotal.SkillTargetID AND ANRDU.LoginDateTime = AgentNotReadyTotal.LoginDateTime), \r\n" +
      "Person (nolock), \r\n" +
      "Agent (nolock), \r\n" +
      "Media_Routing_Domain \r\n" +
    "WHERE Agent.PersonID = Person.PersonID \r\n" + 
      "AND Agent.SkillTargetID = ANRDU.SkillTargetID \r\n" +
      "AND Media_Routing_Domain.MRDomainID = ANRDU.MRDomainID \r\n" + 
      "AND Person.LoginName = '"+agentLoginName+"' \r\n" +
      "ORDER BY FullName, \r\n" + 
         "Media_Routing_Domain.EnterpriseName, \r\n" + 
         "LoginDateTime, \r\n" +
         "textReasonCode";

    return jdbcTemplate.query(sql, new StatusAuxRowMapper());
  }

  @Override
	public List<StatusAux> statusAux(String startPeriode, String endPeriode) {
    String sql =  "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON; \r\n" +
    "WITH AgentNotReadyDetails(SkillTargetID, MRDomainID, LoginDateTime, Event, Duration, ReasonCode, DateTime, DbDateTime, TimeZone) AS \r\n" +
    "(Select aed.SkillTargetID, \r\n" +
        "aed.MRDomainID, \r\n" +
        "CONVERT(varchar, aed.LoginDateTime,120), \r\n" + 
        "aed.Event, \r\n" +
        "aed.Duration,  \r\n" +
        "aed.ReasonCode, \r\n" +
        "DateTime, \r\n" +
        "aed.DbDateTime, \r\n" +
        "aed.TimeZone \r\n" +
    "From Agent_Event_Detail aed  (nolock) \r\n" +
    "Where CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime)  \r\n" +
              "ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" +              
              "+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" +
                    "THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2) \r\n" +
                    "ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END) \r\n" +
              "+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" +
                      "WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" +
                      "WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" +
                      "ELSE '30' END ) \r\n" +
              "+ ':00' END >= '"+startPeriode+" 00:00:00' \r\n" +
    "AND CASE WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' and DATEPART(hh,aed.DateTime) = '00' THEN DATEADD(mi,-30,DateTime) \r\n" +
              "ELSE CONVERT(VARCHAR(30), DATEPART(yyyy,aed.DateTime)) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2) \r\n" +
              "+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \r\n" +
              "+ ' ' +	(CASE WHEN DATEPART(mi, aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' \r\n" +
                    "THEN RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)-1),2) \r\n" +
                    "ELSE RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) END) \r\n" +
              "+ ':' + (CASE WHEN DATEPART(mi,aed.DateTime) = '30' and DATEPART(ss,aed.DateTime) = '00' THEN '00' \r\n" +
                    "WHEN DATEPART(mi,aed.DateTime) = '00' and DATEPART(ss,aed.DateTime) = '00' THEN '30' \r\n" +
                    "WHEN DATEPART(mi,aed.DateTime) < 30 THEN '00'  \r\n" +
                    "ELSE '30' END ) \r\n" +
              "+ ':00' END < '"+endPeriode+" 23:59:59' \r\n" +
    "AND SkillTargetID IN (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)), \r\n" +      
    "AgentLogOutDetails(ALODSkillTargetID, ALODLoginDateTime, TotalLoginTime, ALODEvent) AS \r\n" +
    "(Select aed.SkillTargetID, \r\n" +
         "aed.LoginDateTime,  \r\n" +
         "TotalLoginTime = DATEDIFF(ss,LoginDateTime, DateTime), \r\n" +
         "aed.Event \r\n" +
    "From AgentNotReadyDetails aed \r\n" +
    "WHERE Event = 2), \r\n" +        
    "AgentNRDuration(SkillTargetID, MRDomainID, LoginDateTime, ReasonCodeDuration, ReasonCode) AS \r\n" +
    "(Select ANRD.SkillTargetID,  \r\n" +
        "ANRD.MRDomainID, \r\n" +
        "ANRD.LoginDateTime, \r\n" +
        "ReasonCodeDuration = SUM (ANRD.Duration), \r\n" +
        "ANRD.ReasonCode \r\n" +
      "From AgentNotReadyDetails ANRD \r\n" +
      "Where ANRD.Event = 3 \r\n" +
      "Group By ANRD.SkillTargetID, \r\n" +
           "ANRD.MRDomainID, \r\n" +
           "ANRD.LoginDateTime, \r\n" +
           "ANRD.ReasonCode), \r\n" +           
    "AgentNotReadyTotal(SkillTargetID, NotReadyTime,LoginDateTime) AS \r\n" +
    "(Select SkillTargetID, \r\n" +
        "SUM(ReasonCodeDuration), \r\n" +
        "LoginDateTime \r\n" +
      "From AgentNRDuration \r\n" +
      "GROUP BY SkillTargetID, \r\n" +
        "LoginDateTime) \r\n" +
    "SELECT SkillTargetID = ANRDU.SkillTargetID, \r\n" +
         "LoginDateTime = ANRDU.LoginDateTime, \r\n" +
              "LoginDate = cast(ANRDU.LoginDateTime as date), \r\n" +             
              "TotalLoginTime = CASE WHEN ALOD.ALODEvent = 2 THEN ALOD.TotalLoginTime \r\n" +
                        "ELSE 0 END, \r\n" +
         "TotalNotReadyTime = AgentNotReadyTotal.NotReadyTime, \r\n" +
         "ReasonCode = ANRDU.ReasonCode, \r\n" +
         "textReasonCode = ISNULL(Reason_Code.ReasonText, ' ')+'['+convert(varchar, ANRDU.ReasonCode)+']', \r\n" +
         "ReasonCodeDuration = ANRDU.ReasonCodeDuration, \r\n" +
               "FullName = Person.LastName + ', ' + Person.FirstName, \r\n" +
               "LoginName = Person.LoginName, \r\n" +
         "perNotReady = CASE WHEN ISNULL(AgentNotReadyTotal.NotReadyTime,0) = 0 THEN 0*1.0 ELSE ISNULL(ANRDU.ReasonCodeDuration,0)*1.0/ISNULL(AgentNotReadyTotal.NotReadyTime,0) END, \r\n" +
         "perLogon = CASE WHEN ALOD.ALODEvent = 2 THEN (ANRDU.ReasonCodeDuration*1.0/ALOD.TotalLoginTime) ELSE 0 END, TotalAgentLoginTime=(select sum(DATEDIFF(ss,LoginDateTime, DateTime)) from AgentNotReadyDetails aed \r\n" +
    "WHERE Event = 2 AND aed.SkillTargetID=ANRDU.SkillTargetID) \r\n" +
    "FROM AgentNRDuration ANRDU \r\n" +
       "LEFT JOIN Reason_Code ON ANRDU.ReasonCode=Reason_Code.ReasonCode \r\n" +
      "LEFT JOIN  AgentLogOutDetails ALOD ON (ANRDU.SkillTargetID = ALOD.ALODSkillTargetID \r\n" +
          "AND ANRDU.LoginDateTime <= DATEADD(ss, 2, ALOD.ALODLoginDateTime) AND ANRDU.LoginDateTime >= DATEADD(ss, -2, ALOD.ALODLoginDateTime)) \r\n" +
      "LEFT JOIN AgentNotReadyTotal ON (ANRDU.SkillTargetID = AgentNotReadyTotal.SkillTargetID AND ANRDU.LoginDateTime = AgentNotReadyTotal.LoginDateTime), \r\n" +
      "Person (nolock), \r\n" +
      "Agent (nolock), \r\n" +
      "Media_Routing_Domain \r\n" +
    "WHERE Agent.PersonID = Person.PersonID \r\n" + 
      "AND Agent.SkillTargetID = ANRDU.SkillTargetID \r\n" +
      "AND Media_Routing_Domain.MRDomainID = ANRDU.MRDomainID \r\n" + 
      "ORDER BY FullName, \r\n" + 
         "Media_Routing_Domain.EnterpriseName, \r\n" + 
         "LoginDateTime, \r\n" +
         "textReasonCode";

    return jdbcTemplate.query(sql, new StatusAuxRowMapper());
  }

@Override
public List<StatusAux> statusAuxAgentNewQuery(String startPeriode, String endPeriode, String agentLoginName) {
	String sql = "WITH AgentDetail(SkillTargetID,AgentId,AgentName) \n" + 
			  "AS\n" + 
			  "(\n" + 
			  "   SELECT A.SkillTargetID,\n" + 
			  "\t   B.LoginName,\n" + 
			  "\t   (B.LastName +', ' +B.FirstName)\n" + 
			  "   FROM Agent A, \n" + 
			  "\t   Person B WHERE A.SkillTargetID IN (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)\n" + 
			  "  AND A.PersonID = B.PersonID  \n" + 
			  " ),\n" + 
			  "AgentLogAvailNotReady(SkillTargetID,Date,LoggedOnTime,AvailTime,NotReadyTime)\n" + 
			  "AS\n" + 
			  "(\n" + 
			  "SELECT AI.SkillTargetID,\n" + 
			  "\tDateTime = CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ' ' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ':' + CASE WHEN DATEPART(mi,DateTime) < 30 THEN '00' ELSE '30' END \n" + 
			  "\t\t\t\t\t+ ':00',\n" + 
			  "\tLoggedOnTime =sum( AI.LoggedOnTime),  \n" + 
			  "\tAvailTime = SUM(ISNULL(AI.AvailTime, 0)),\n" + 
			  "\tNotReadyTime = SUM(ISNULL(AI.NotReadyTime, 0))\n" + 
			  "FROM Agent_Interval AI (nolock)\n" + 
			  "WHERE AI.SkillTargetID IN (5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)\n" + 
			  "  AND AI.DateTime  >= '"+startPeriode+" 00:00:00' \n" + 
			  "  AND AI.DateTime < '"+endPeriode+" 23:59:59'\n" + 
			  " GROUP BY AI.SkillTargetID,\n" + 
			  "\t CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ' ' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ':' + CASE WHEN DATEPART(mi,DateTime) < 30 THEN '00' ELSE '30' END \n" + 
			  "\t\t\t\t\t+ ':00'\n" + 
			  "),\n" + 
			  "AuxReason(Interval,SkillTargetID,ReasonCode,Duration)\n" + 
			  "AS\n" + 
			  "( \n" + 
			  "      SELECT Interval= CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ' ' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ':' + CASE WHEN DATEPART(mi,DateTime) < 30 THEN '00' ELSE '30' END \n" + 
			  "\t\t\t\t\t+ ':00',\n" + 
			  "\t\t  SkillTargetID,\n" + 
			  "\t\t  ReasonCode,\n" + 
			  "\t\t  Duration=SUM(Duration) \n" + 
			  " FROM Agent_Event_Detail\n" + 
			  "      WHERE Event = 3 \n" + 
			  "\t\t  AND DateTime  >=  '"+startPeriode+"  00:00:00'  \n" + 
			  "\t\t  and DateTime <= '"+endPeriode+"  23:59:59' \n" + 
			  "\t\t  AND SkillTargetID IN(5055,5056,5065,5066,5067,5073,5075,5076,5077,5078,5079,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5113,5114,5116,5118,5121,5122,5124,5125,5126,5129,5130,5132,5135,5138,5139,5140,5142,5143,5144,5149,5150,5151,5152,5153,5154,5155,5156,5157,5159,5160,5161,5162,5163,5164,5165,5166,5167,5168,5220,5226,5227,5228,5229,5230,5231,5232,5233,5234,5235,5236,5237,5238,5239,5240,5251,5252,5253,5255,5256,5257,5258,5259,5260,5261,5262,5264,5265,5266,5267,5268,5269,5270,5271,5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,5293,5294,5295,5296,5297,5298,5299,5300,5301,5302,5303,5304,5305,5306,5307,5308,5309,5310,5311,5312,5313,5314,5315,5316,5317,5318,5319,5320,5321,5322,5372,5373,5374,5375,5376,5377,5378,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5394,5395,5396,5397,5398,5399,5400,5401,5402,5403,5404,5405,5406,5407,5408,5409,5410,5411,5412,5413,5414,5415,5416,5417,5418,5419,5420,5421,5422,5423,5424,5425,5426,5427,5428,5429,5430,5431,5432,5433,5434,5435,5436,5437,5438,5439,5440,5441,5442,5443,5444,5445,5446,5447,5448,5449,5450,5451,5452,5453,5454,5455,5456,5457,5458)\n" + 
			  "  GROUP BY CONVERT(VARCHAR(30), DATEPART(yyyy,DateTime)) \n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(mm,DateTime)),2)\n" + 
			  "\t\t\t\t\t+ '-' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(dd,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ' ' + RIGHT('0'+CONVERT(VARCHAR(30),DATEPART(hh,DateTime)),2) \n" + 
			  "\t\t\t\t\t+ ':' + CASE WHEN DATEPART(mi,DateTime) < 30 THEN '00' ELSE '30' END \n" + 
			  "\t\t\t\t\t+ ':00',\n" + 
			  "\t\t  SkillTargetID,\n" + 
			  "\t\t  ReasonCode\n" + 
			  "),\n" + 
			  "TotalAuxTime(Interval,SkillTargetID,Duration)\n" + 
			  "AS\n" + 
			  "(\n" + 
			  "      SELECT Interval,\n" + 
			  "\t\t  SkillTargetID,\n" + 
			  "\t\t  SUM(Duration)\n" + 
			  "      FROM AuxReason\n" + 
			  "      GROUP BY Interval,\n" + 
			  "\t\t  SkillTargetID\n" + 
			  "\t\t  ),\n" + 
			  "\t\t  \n" + 
			  "AuxDetails(Interval,SkillTargetID,Aux53734,Aux32767,Aux50002,Aux50003,Aux29727,Aux63421,Aux14337,Aux55963,Aux58283,Aux7542,Aux48439,Aux10035,Aux22973,Aux999,Aux0)\n" + 
			  "AS\n" + 
			  "(\n" + 
			  "      SELECT Interval,\n" + 
			  "\t\t  SkillTargetID,\n" + 
			  "\t\t  ISNULL([53734],0),ISNULL([32767],0),ISNULL([50002],0),ISNULL([50003],0),ISNULL([29727],0),ISNULL([63421],0),ISNULL([14337],0),ISNULL([55963],0),ISNULL([58283],0),ISNULL([7542],0),\n" + 
			  "\t\t  ISNULL([48439],0),ISNULL([10035],0),ISNULL([22973],0),ISNULL([999],0),ISNULL([0],0)      \n" + 
			  "      FROM (SELECT Interval,\n" + 
			  "\t\t\t\tSkillTargetID,\n" + 
			  "\t\t\t\tReasonCode,\n" + 
			  "\t\t\t\tDuration \n" + 
			  "            FROM AuxReason)PS\n" + 
			  "      PIVOT\n" + 
			  "      (SUM(Duration)\n" + 
			  "            FOR ReasonCode IN ([53734],[32767],[50002],[50003],[29727],[63421],[14337],[55963],[58283],[7542],[48439],[10035],[22973],[999],[0])\n" + 
			  "      )AS PVT\n" + 
			  ")\n" + 
			  "SELECT A.Interval,\n" + 
			  "\tA.SkillTargetID,\n" + 
			  "\tC.AgentId,\n" + 
			  "\tC.AgentName,\n" + 
			  "\tB.Duration,\n" + 
			  "\tAgentLoggedOnTime = E.LoggedOnTime,\n" + 
			  "\tAgentAvailTime = E.AvailTime,\n" + 
			  "\tAgentNotReadyTime = E.NotReadyTime,\n" + 
			  "\tA.Aux53734,A.Aux32767,A.Aux50002,A.Aux50003,A.Aux29727,A.Aux63421,A.Aux14337,A.Aux55963,A.Aux58283,A.Aux7542,A.Aux48439,A.Aux10035,A.Aux22973,A.Aux999,A.Aux0,\n" + 
			  "\tRC53734= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 53734),53734),\n" + 
			  "\tRC32767= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 32767),32767),\n" + 
			  "\tRC50002= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 50002),50002),\n" + 
			  "\tRC50003= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 50003),50003),\n" + 
			  "\tRC29727= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 29727),29727),\n" + 
			  "\tRC63421= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 63421),63421),\n" + 
			  "\tRC14337= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 14337),14337),\n" + 
			  "\tRC55963= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 55963),55963),\n" + 
			  "\tRC58283= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 58283),58283),\n" + 
			  "\tRC7542= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 7542),7542),\n" + 
			  "\tRC48439= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 48439),48439),\n" + 
			  "\tRC10035= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 10035),10035),\n" + 
			  "\tRC22973= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 22973),22973),\n" + 
			  "\tRC999= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 999),999),\n" + 
			  "\tRC0= ISNULL((Select ReasonText From Reason_Code RC Where RC.ReasonCode = 0),0)\n" + 
			  "From AuxDetails A,\n" + 
			  "\tTotalAuxTime B,\n" + 
			  "\tAgentDetail C,\n" + 
			  "\tAgentLogAvailNotReady E\n" + 
			  "WHERE A.Interval =B.Interval \n" + 
			  "\tAND A.SkillTargetID = B.SkillTargetID \n" + 
			  "\tAND C.SkillTargetID = A.SkillTargetID \n" + 
			  "\tAND A.SkillTargetID = E.SkillTargetID \n" + 
			  "\tAND E.Date= A.Interval\n" + 
			  "\tAND C.AgentId = '"+agentLoginName+"'\n" + 
			  "ORDER BY A.Interval, C.AgentId;\n";

			return jdbcTemplate.query(sql, new StatusAuxRowNewQueryMapper());
}







}
