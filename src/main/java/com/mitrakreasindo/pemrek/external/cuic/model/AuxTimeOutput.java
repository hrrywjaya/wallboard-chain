package com.mitrakreasindo.pemrek.external.cuic.model;

import java.util.List;

import lombok.Data;

@Data
public class AuxTimeOutput{
  private List<AuxTime> output_schema;
}
