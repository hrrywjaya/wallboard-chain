package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DateDiffRowMapper implements RowMapper<DateDiff>{

	@Override
	public DateDiff mapRow(ResultSet rs, int rowNum) throws SQLException {
    DateDiff rc = new DateDiff();
      // rc.setDropped_date(rs.getString("DROPPED_DATE"));
      rc.setDiff_hours(rs.getInt("diff_hours"));
		return rc;
	}

}
