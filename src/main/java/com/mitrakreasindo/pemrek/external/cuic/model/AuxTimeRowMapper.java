package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AuxTimeRowMapper implements RowMapper<AuxTime>{

	@Override
	public AuxTime mapRow(ResultSet rs, int rowNum) throws SQLException {
		AuxTime at = new AuxTime();
    at.setDuration(rs.getInt("Duration"));
    at.setAgentState(rs.getString("AgentState"));
    at.setReasonCode(rs.getInt("ReasonCode"));
    at.setLoginName(rs.getString("LoginName"));
    at.setAgentStateCode(rs.getInt("AgentStateCode"));
    at.setReason(rs.getString("Reason"));
    at.setLoginDate(rs.getString("DateTimeLogin"));
    at.setFirstName(rs.getString("FirstName"));
    at.setLastName(rs.getString("LastName"));
		return at;
	}

}
