package com.mitrakreasindo.pemrek.external.cuic.model;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StatusAuxRowNewQueryMapper implements RowMapper<StatusAux>{
	@Override
	public StatusAux mapRow(ResultSet rs, int rowNum) throws SQLException {
    StatusAux rc = new StatusAux();
    rc.setLoginName(rs.getString("AgentId"));
    rc.setAgentReady(rs.getInt("Aux0"));
    return rc;
	}
}
