package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DurationHoldPagingRowMapper implements RowMapper<DurationHoldPaging>{

	@Override
	public DurationHoldPaging  mapRow(ResultSet rs, int rowNum) throws SQLException {
    DurationHoldPaging dhp = new DurationHoldPaging();
    dhp.setTotalPage(rs.getInt("totalPage"));
    return null;
	}

}
