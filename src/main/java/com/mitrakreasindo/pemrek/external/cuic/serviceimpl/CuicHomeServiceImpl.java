package com.mitrakreasindo.pemrek.external.cuic.serviceimpl;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.cuic.model.AbandonCall;
import com.mitrakreasindo.pemrek.external.cuic.model.AgentToday;
import com.mitrakreasindo.pemrek.external.cuic.model.AgentTodayRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTime;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTimeOutput;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTimeRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLog;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLogRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.DaftarAgent;
import com.mitrakreasindo.pemrek.external.cuic.model.DaftarAgentPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.DateDiff;
import com.mitrakreasindo.pemrek.external.cuic.model.DateDiffRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.HistoryAux;
import com.mitrakreasindo.pemrek.external.cuic.model.PerformanceReview;
import com.mitrakreasindo.pemrek.external.cuic.model.Queue;
import com.mitrakreasindo.pemrek.external.cuic.model.RedirectCall;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilan;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilanAgent;
import com.mitrakreasindo.pemrek.external.cuic.model.StatusAux;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicHomeService;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicMonitoringService;

@Service
public class CuicHomeServiceImpl implements CuicHomeService{

  @Autowired
	private CuicMonitoringService cuicMonitoringService;
	@Autowired
	@Qualifier("cuicJdbc")
  private JdbcTemplate jdbcCuicTemplate;
  @Autowired
  private JdbcTemplate jdbcChainTemplate;
  @Value("${service.level}")
	private int serviceLevel;
  
  @Override
  public List<AuxTime> getStateReadyAux() {
    String sql = " SELECT EnterpriseName = Agent.EnterpriseName, Person.LastName + ', ' + Person.FirstName as AgentName, \r\n" +
          "LastName = Person.LastName, \r\n" +
          "FirstName = Person.FirstName, \r\n" +
          "LoginName = Person.LoginName, \r\n" +
          "PeripheralNumber = Agent.PeripheralNumber, \r\n" +
          "SkillName = Skill_Group.EnterpriseName, \r\n" +   
          "SkillTargetID = Skill_Group.SkillTargetID, \r\n" +
          "DateTime = ART.DateTime, \r\n" +
          "Date=CONVERT(char(10),ART.DateTime,101), \r\n" +            
          "AgentSkillTargetID = ART.SkillTargetID, \r\n" +           
          "ServiceName=Service.EnterpriseName, \r\n" +
          "Media= MRD.EnterpriseName, \r\n" +
          "AgentStateCode= ASGRT.AgentState, \r\n" +          
          "AgentState=CASE ASGRT.AgentState \r\n" +  
              "WHEN 0 THEN 'Logged Out'  \r\n" +
              "WHEN 1 THEN 'Logged On' \r\n" + 
              "WHEN 2 THEN 'Not Ready' \r\n" + 
              "WHEN 3 THEN 'Ready' \r\n" +
              "WHEN 4 THEN 'Talking' \r\n" + 
              "WHEN 5 THEN 'Work Not Ready' \r\n" + 
              "WHEN 6 THEN 'Work Ready' \r\n" + 
              "WHEN 7 THEN 'Busy Other' \r\n" + 
              "WHEN 8 THEN 'Reserved' \r\n" +  
              "WHEN 9 THEN 'Unknown' \r\n" + 
              "WHEN 10 THEN 'Hold' \r\n" + 
              "WHEN 11 THEN 'Active' \r\n" +  
              "WHEN 12 THEN 'Paused' \r\n" + 
              "WHEN 13 THEN 'Interrupted' \r\n" + 
              "WHEN 14 THEN 'Not Active' \r\n" + 
              "ELSE CONVERT(VARCHAR, ASGRT.AgentState) END, \r\n" +
          "PhoneTypeText=(CASE ART.PhoneType \r\n" + 
              "When 0 THEN 'Not Mobile' \r\n" + 
              "WHEN 1 THEN 'Call By Call' \r\n" + 
              "WHEN 2 THEN 'Nailed Connection' \r\n" +
              "Else 'Not Applicable' END),  ART.RemotePhoneNumber as RemotePhoneNumber, \r\n" + 
          "Reason=(CASE WHEN ASGRT.ReasonCode = 0 THEN 'NONE' \r\n" +
              "ELSE ISNULL((select ReasonText from Reason_Code where ReasonCode=ASGRT.ReasonCode),ASGRT.ReasonCode)  END), \r\n" +
          "ReasonCode = ASGRT.ReasonCode, \r\n"+
          "Extension = ART.Extension, \r\n" +
          "DateTimeLastStateChange = ASGRT.DateTimeLastStateChange, \r\n" +
          "DateTimeLogin = ASGRT.DateTimeLogin, \r\n" +
          "RequestedSupervisorAssist=(CASE WHEN RequestedSupervisorAssist = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "Destination=CASE ART.Destination \r\n" + 
              "WHEN 1 THEN 'ACD' \r\n" +
              "WHEN 2 THEN 'Direct' \r\n" +
              "WHEN 3 THEN 'Auto Out' \r\n" +
              "WHEN 4 THEN 'Reserve' \r\n" +
              "WHEN 5 THEN 'Preview' \r\n" +
              "ELSE 'Not Applicable' END, \r\n" +
          "DirectionText=CASE \r\n" +
              "WHEN ASGRT.AgentState = 7 THEN 'Not Applicable' \r\n" +
              "WHEN ART.Direction = 1 THEN 'In' \r\n" +
              "WHEN ART.Direction = 2 THEN 'Out' \r\n" +
              "WHEN ART.Direction = 3 THEN 'Other In' \r\n" +
              "WHEN ART.Direction = 4 THEN 'Other Out' \r\n" +
              "WHEN ART.Direction = 5 THEN 'Out Reserve' \r\n" +
              "WHEN ART.Direction = 6 THEN 'Out Preview' \r\n" +
              "WHEN ART.Direction = 7 THEN 'Out Predictive' \r\n" +
              "ELSE 'Not Applicable' END,Direction = ART.Direction, \r\n" +
          "OnHold= (CASE WHEN ART.OnHold = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "NetworkTargetID = ART.NetworkTargetID, \r\n" +
          "AgentStatus = ART.AgentStatus, \r\n" +
          "CustomerPhoneNumber = ART.CustomerPhoneNumber, \r\n" +
          "CustomerAccountNumber = ART.CustomerAccountNumber, \r\n" +       
          "CampaignID = ART.CampaignID, \r\n" +
          "QueryRuleID = ART.QueryRuleID,\r\n" +
          "Duration = DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
              "CASE WHEN (DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
                "(SELECT NowTime \r\n" +
                "from Controller_Time (nolock))) <=0) \r\n" + 
              "THEN ASGRT.DateTimeLastStateChange \r\n" +
              "ELSE (SELECT NowTime FROM Controller_Time (nolock))END), \r\n" +
          "RoutableText=(CASE WHEN ART.Routable = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "DateTimeLastModeChange = ART.DateTimeLastModeChange, \r\n" +
          "CallsInProgress = ART.CallInProgress, \r\n" +
          "MaxTasks = ART.MaxTasks, \r\n" +
          "AvailInMRDText=(CASE \r\n" +
              "WHEN ART.AvailableInMRD = 0 THEN 'No' \r\n" +
              "WHEN ART.AvailableInMRD = 1 THEN 'Yes_ICM' \r\n" +
              "WHEN ART.AvailableInMRD = 2 THEN 'Yes_APP' \r\n" +
              "ELSE 'No' END), \r\n" +
          "DateTimeTaskLevelChange = ART.DateTimeTaskLevelChange, \r\n" +
          "RouterCallsQNow = Skill_Group_Real_Time.RouterCallsQNow, \r\n" +
          "RouterCallsQueueNow = ART.RouterCallsQueueNow, \r\n" +
          "RouterLongestCallQ = ART.RouterLongestCallQ, \r\n" +
          "totalrouterqueuednow=isnull(Skill_Group_Real_Time.RouterCallsQNow,0) + isnull(Skill_Group_Real_Time.CallsQueuedNow,0) \r\n" +
        "FROM  Agent (nolock), \r\n" +
        "Person (nolock), \r\n" +
        "Agent_Skill_Group_Real_Time ASGRT, \r\n" +
        "Agent_Real_Time ART (nolock) LEFT OUTER JOIN \r\n" +
        "Service (nolock) ON ART.ServiceSkillTargetID = Service.SkillTargetID, \r\n" +
        "Skill_Group (nolock), \r\n" +
        "Media_Routing_Domain MRD (nolock), \r\n" +
        "Skill_Group_Real_Time (nolock) \r\n" +
        "WHERE Skill_Group.SkillTargetID IN (5104) and Agent.PersonID=Person.PersonID \r\n" +
        "AND Agent.SkillTargetID = ART.SkillTargetID \r\n" +
        "AND ART.SkillTargetID = ASGRT.SkillTargetID \r\n" +
        "AND ART.MRDomainID = MRD.MRDomainID \r\n" +
        "AND Skill_Group.MRDomainID = ART.MRDomainID \r\n" +
        "AND Skill_Group.SkillTargetID = ASGRT.SkillGroupSkillTargetID \r\n" + 
        "AND Skill_Group.SkillTargetID= Skill_Group_Real_Time.SkillTargetID \r\n" +
        "AND Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID  FROM Skill_Group WHERE Priority > 0 AND Deleted != 'Y') \r\n"
        ;
		return jdbcCuicTemplate.query(sql, new AuxTimeRowMapper());
  }

  // @Override
  public List<AuxTime> getStateReadyAuxbyAgent(String agent) {
    String sql = " SELECT EnterpriseName = Agent.EnterpriseName, Person.LastName + ', ' + Person.FirstName as AgentName, \r\n" +
          "LastName = Person.LastName, \r\n" +
          "FirstName = Person.FirstName, \r\n" +
          "LoginName = Person.LoginName, \r\n" +
          "PeripheralNumber = Agent.PeripheralNumber, \r\n" +
          "SkillName = Skill_Group.EnterpriseName, \r\n" +   
          "SkillTargetID = Skill_Group.SkillTargetID, \r\n" +
          "DateTime = ART.DateTime, \r\n" +
          "Date=CONVERT(char(10),ART.DateTime,101), \r\n" +            
          "AgentSkillTargetID = ART.SkillTargetID, \r\n" +           
          "ServiceName=Service.EnterpriseName, \r\n" +
          "Media= MRD.EnterpriseName, \r\n" +
          "AgentStateCode= ASGRT.AgentState, \r\n" +          
          "AgentState=CASE ASGRT.AgentState \r\n" +  
              "WHEN 0 THEN 'Logged Out'  \r\n" +
              "WHEN 1 THEN 'Logged On' \r\n" + 
              "WHEN 2 THEN 'Not Ready' \r\n" + 
              "WHEN 3 THEN 'Ready' \r\n" +
              "WHEN 4 THEN 'Talking' \r\n" + 
              "WHEN 5 THEN 'Work Not Ready' \r\n" + 
              "WHEN 6 THEN 'Work Ready' \r\n" + 
              "WHEN 7 THEN 'Busy Other' \r\n" + 
              "WHEN 8 THEN 'Reserved' \r\n" +  
              "WHEN 9 THEN 'Unknown' \r\n" + 
              "WHEN 10 THEN 'Hold' \r\n" + 
              "WHEN 11 THEN 'Active' \r\n" +  
              "WHEN 12 THEN 'Paused' \r\n" + 
              "WHEN 13 THEN 'Interrupted' \r\n" + 
              "WHEN 14 THEN 'Not Active' \r\n" + 
              "ELSE CONVERT(VARCHAR, ASGRT.AgentState) END, \r\n" +
          "PhoneTypeText=(CASE ART.PhoneType \r\n" + 
              "When 0 THEN 'Not Mobile' \r\n" + 
              "WHEN 1 THEN 'Call By Call' \r\n" + 
              "WHEN 2 THEN 'Nailed Connection' \r\n" +
              "Else 'Not Applicable' END),  ART.RemotePhoneNumber as RemotePhoneNumber, \r\n" + 
          "Reason=(CASE WHEN ASGRT.ReasonCode = 0 THEN 'NONE' \r\n" +
              "ELSE ISNULL((select ReasonText from Reason_Code where ReasonCode=ASGRT.ReasonCode),ASGRT.ReasonCode)  END), \r\n" +
          "ReasonCode = ASGRT.ReasonCode, \r\n"+
          "Extension = ART.Extension, \r\n" +
          "DateTimeLastStateChange = ASGRT.DateTimeLastStateChange, \r\n" +
          "DateTimeLogin = ASGRT.DateTimeLogin, \r\n" +
          "RequestedSupervisorAssist=(CASE WHEN RequestedSupervisorAssist = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "Destination=CASE ART.Destination \r\n" + 
              "WHEN 1 THEN 'ACD' \r\n" +
              "WHEN 2 THEN 'Direct' \r\n" +
              "WHEN 3 THEN 'Auto Out' \r\n" +
              "WHEN 4 THEN 'Reserve' \r\n" +
              "WHEN 5 THEN 'Preview' \r\n" +
              "ELSE 'Not Applicable' END, \r\n" +
          "DirectionText=CASE \r\n" +
              "WHEN ASGRT.AgentState = 7 THEN 'Not Applicable' \r\n" +
              "WHEN ART.Direction = 1 THEN 'In' \r\n" +
              "WHEN ART.Direction = 2 THEN 'Out' \r\n" +
              "WHEN ART.Direction = 3 THEN 'Other In' \r\n" +
              "WHEN ART.Direction = 4 THEN 'Other Out' \r\n" +
              "WHEN ART.Direction = 5 THEN 'Out Reserve' \r\n" +
              "WHEN ART.Direction = 6 THEN 'Out Preview' \r\n" +
              "WHEN ART.Direction = 7 THEN 'Out Predictive' \r\n" +
              "ELSE 'Not Applicable' END,Direction = ART.Direction, \r\n" +
          "OnHold= (CASE WHEN ART.OnHold = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "NetworkTargetID = ART.NetworkTargetID, \r\n" +
          "AgentStatus = ART.AgentStatus, \r\n" +
          "CustomerPhoneNumber = ART.CustomerPhoneNumber, \r\n" +
          "CustomerAccountNumber = ART.CustomerAccountNumber, \r\n" +       
          "CampaignID = ART.CampaignID, \r\n" +
          "QueryRuleID = ART.QueryRuleID,\r\n" +
          "Duration = DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
              "CASE WHEN (DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
                "(SELECT NowTime \r\n" +
                "from Controller_Time (nolock))) <=0) \r\n" + 
              "THEN ASGRT.DateTimeLastStateChange \r\n" +
              "ELSE (SELECT NowTime FROM Controller_Time (nolock))END), \r\n" +
          "RoutableText=(CASE WHEN ART.Routable = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "DateTimeLastModeChange = ART.DateTimeLastModeChange, \r\n" +
          "CallsInProgress = ART.CallInProgress, \r\n" +
          "MaxTasks = ART.MaxTasks, \r\n" +
          "AvailInMRDText=(CASE \r\n" +
              "WHEN ART.AvailableInMRD = 0 THEN 'No' \r\n" +
              "WHEN ART.AvailableInMRD = 1 THEN 'Yes_ICM' \r\n" +
              "WHEN ART.AvailableInMRD = 2 THEN 'Yes_APP' \r\n" +
              "ELSE 'No' END), \r\n" +
          "DateTimeTaskLevelChange = ART.DateTimeTaskLevelChange, \r\n" +
          "RouterCallsQNow = Skill_Group_Real_Time.RouterCallsQNow, \r\n" +
          "RouterCallsQueueNow = ART.RouterCallsQueueNow, \r\n" +
          "RouterLongestCallQ = ART.RouterLongestCallQ, \r\n" +
          "totalrouterqueuednow=isnull(Skill_Group_Real_Time.RouterCallsQNow,0) + isnull(Skill_Group_Real_Time.CallsQueuedNow,0) \r\n" +
        "FROM  Agent (nolock), \r\n" +
        "Person (nolock), \r\n" +
        "Agent_Skill_Group_Real_Time ASGRT, \r\n" +
        "Agent_Real_Time ART (nolock) LEFT OUTER JOIN \r\n" +
        "Service (nolock) ON ART.ServiceSkillTargetID = Service.SkillTargetID, \r\n" +
        "Skill_Group (nolock), \r\n" +
        "Media_Routing_Domain MRD (nolock), \r\n" +
        "Skill_Group_Real_Time (nolock) \r\n" +
        "WHERE Skill_Group.SkillTargetID IN (5104) and Agent.PersonID=Person.PersonID \r\n" +
        "AND Agent.SkillTargetID = ART.SkillTargetID \r\n" +
        "AND ART.SkillTargetID = ASGRT.SkillTargetID \r\n" +
        "AND ART.MRDomainID = MRD.MRDomainID \r\n" +
        "AND Skill_Group.MRDomainID = ART.MRDomainID \r\n" +
        "AND Skill_Group.SkillTargetID = ASGRT.SkillGroupSkillTargetID \r\n" + 
        "AND Skill_Group.SkillTargetID= Skill_Group_Real_Time.SkillTargetID \r\n" +
        "AND Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID  FROM Skill_Group WHERE Priority > 0 AND Deleted != 'Y') \r\n"+
        "AND upper(Person.FirstName) like upper('%"+agent+"%')";
		return jdbcCuicTemplate.query(sql, new AuxTimeRowMapper());
  }


  @Override
  public List<AuxTime> getAuxByState(String state) {
  String sql;
    if(state.equals("0")){
       sql = " SELECT EnterpriseName = Agent.EnterpriseName, Person.LastName + ', ' + Person.FirstName as AgentName, \r\n" +
          "LastName = Person.LastName, \r\n" +
          "FirstName = Person.FirstName, \r\n" +
          "LoginName = Person.LoginName, \r\n" +
          "PeripheralNumber = Agent.PeripheralNumber, \r\n" +
          "SkillName = Skill_Group.EnterpriseName, \r\n" +   
          "SkillTargetID = Skill_Group.SkillTargetID, \r\n" +
          "DateTime = ART.DateTime, \r\n" +
          "Date=CONVERT(char(10),ART.DateTime,101), \r\n" +            
          "AgentSkillTargetID = ART.SkillTargetID, \r\n" +           
          "ServiceName=Service.EnterpriseName, \r\n" +
          "Media= MRD.EnterpriseName, \r\n" +
          "AgentStateCode= ASGRT.AgentState, \r\n" +          
          "AgentState=CASE ASGRT.AgentState \r\n" +  
              "WHEN 0 THEN 'Logged Out'  \r\n" +
              "WHEN 1 THEN 'Logged On' \r\n" + 
              "WHEN 2 THEN 'Not Ready' \r\n" + 
              "WHEN 3 THEN 'Ready' \r\n" +
              "WHEN 4 THEN 'Talking' \r\n" + 
              "WHEN 5 THEN 'Work Not Ready' \r\n" + 
              "WHEN 6 THEN 'Work Ready' \r\n" + 
              "WHEN 7 THEN 'Busy Other' \r\n" + 
              "WHEN 8 THEN 'Reserved' \r\n" +  
              "WHEN 9 THEN 'Unknown' \r\n" + 
              "WHEN 10 THEN 'Hold' \r\n" + 
              "WHEN 11 THEN 'Active' \r\n" +  
              "WHEN 12 THEN 'Paused' \r\n" + 
              "WHEN 13 THEN 'Interrupted' \r\n" + 
              "WHEN 14 THEN 'Not Active' \r\n" + 
              "ELSE CONVERT(VARCHAR, ASGRT.AgentState) END, \r\n" +
          "PhoneTypeText=(CASE ART.PhoneType \r\n" + 
              "When 0 THEN 'Not Mobile' \r\n" + 
              "WHEN 1 THEN 'Call By Call' \r\n" + 
              "WHEN 2 THEN 'Nailed Connection' \r\n" +
              "Else 'Not Applicable' END),  ART.RemotePhoneNumber as RemotePhoneNumber, \r\n" + 
          "Reason=(CASE WHEN ASGRT.ReasonCode = 0 THEN 'NONE' \r\n" +
              "ELSE ISNULL((select ReasonText from Reason_Code where ReasonCode=ASGRT.ReasonCode),ASGRT.ReasonCode)  END), \r\n" +
              "ReasonCode = ASGRT.ReasonCode, \r\n"+
          "Extension = ART.Extension, \r\n" +
          "DateTimeLastStateChange = ASGRT.DateTimeLastStateChange, \r\n" +
          "DateTimeLogin = ASGRT.DateTimeLogin, \r\n" +
          "RequestedSupervisorAssist=(CASE WHEN RequestedSupervisorAssist = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "Destination=CASE ART.Destination \r\n" + 
              "WHEN 1 THEN 'ACD' \r\n" +
              "WHEN 2 THEN 'Direct' \r\n" +
              "WHEN 3 THEN 'Auto Out' \r\n" +
              "WHEN 4 THEN 'Reserve' \r\n" +
              "WHEN 5 THEN 'Preview' \r\n" +
              "ELSE 'Not Applicable' END, \r\n" +
          "DirectionText=CASE \r\n" +
              "WHEN ASGRT.AgentState = 7 THEN 'Not Applicable' \r\n" +
              "WHEN ART.Direction = 1 THEN 'In' \r\n" +
              "WHEN ART.Direction = 2 THEN 'Out' \r\n" +
              "WHEN ART.Direction = 3 THEN 'Other In' \r\n" +
              "WHEN ART.Direction = 4 THEN 'Other Out' \r\n" +
              "WHEN ART.Direction = 5 THEN 'Out Reserve' \r\n" +
              "WHEN ART.Direction = 6 THEN 'Out Preview' \r\n" +
              "WHEN ART.Direction = 7 THEN 'Out Predictive' \r\n" +
              "ELSE 'Not Applicable' END,Direction = ART.Direction, \r\n" +
          "OnHold= (CASE WHEN ART.OnHold = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "NetworkTargetID = ART.NetworkTargetID, \r\n" +
          "AgentStatus = ART.AgentStatus, \r\n" +
          "CustomerPhoneNumber = ART.CustomerPhoneNumber, \r\n" +
          "CustomerAccountNumber = ART.CustomerAccountNumber, \r\n" +       
          "CampaignID = ART.CampaignID, \r\n" +
          "QueryRuleID = ART.QueryRuleID,\r\n" +
          "Duration = DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
              "CASE WHEN (DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
                "(SELECT NowTime \r\n" +
                "from Controller_Time (nolock))) <=0) \r\n" + 
              "THEN ASGRT.DateTimeLastStateChange \r\n" +
              "ELSE (SELECT NowTime FROM Controller_Time (nolock))END), \r\n" +
          "RoutableText=(CASE WHEN ART.Routable = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "DateTimeLastModeChange = ART.DateTimeLastModeChange, \r\n" +
          "CallsInProgress = ART.CallInProgress, \r\n" +
          "MaxTasks = ART.MaxTasks, \r\n" +
          "AvailInMRDText=(CASE \r\n" +
              "WHEN ART.AvailableInMRD = 0 THEN 'No' \r\n" +
              "WHEN ART.AvailableInMRD = 1 THEN 'Yes_ICM' \r\n" +
              "WHEN ART.AvailableInMRD = 2 THEN 'Yes_APP' \r\n" +
              "ELSE 'No' END), \r\n" +
          "DateTimeTaskLevelChange = ART.DateTimeTaskLevelChange, \r\n" +
          "RouterCallsQNow = Skill_Group_Real_Time.RouterCallsQNow, \r\n" +
          "RouterCallsQueueNow = ART.RouterCallsQueueNow, \r\n" +
          "RouterLongestCallQ = ART.RouterLongestCallQ, \r\n" +
          "totalrouterqueuednow=isnull(Skill_Group_Real_Time.RouterCallsQNow,0) + isnull(Skill_Group_Real_Time.CallsQueuedNow,0) \r\n" +
        "FROM  Agent (nolock), \r\n" +
        "Person (nolock), \r\n" +
        "Agent_Skill_Group_Real_Time ASGRT, \r\n" +
        "Agent_Real_Time ART (nolock) LEFT OUTER JOIN \r\n" +
        "Service (nolock) ON ART.ServiceSkillTargetID = Service.SkillTargetID, \r\n" +
        "Skill_Group (nolock), \r\n" +
        "Media_Routing_Domain MRD (nolock), \r\n" +
        "Skill_Group_Real_Time (nolock) \r\n" +
        "WHERE Skill_Group.SkillTargetID IN (5104) and Agent.PersonID=Person.PersonID \r\n" +
        "AND Agent.SkillTargetID = ART.SkillTargetID \r\n" +
        "AND ART.SkillTargetID = ASGRT.SkillTargetID \r\n" +
        "AND ART.MRDomainID = MRD.MRDomainID \r\n" +
        "AND Skill_Group.MRDomainID = ART.MRDomainID \r\n" +
        "AND Skill_Group.SkillTargetID = ASGRT.SkillGroupSkillTargetID \r\n" + 
        "AND Skill_Group.SkillTargetID= Skill_Group_Real_Time.SkillTargetID \r\n" +
        "AND Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID  FROM Skill_Group WHERE Priority > 0 AND Deleted != 'Y') \r\n" +
        "AND ASGRT.AgentState=3";
    } else {
       sql = " SELECT EnterpriseName = Agent.EnterpriseName, Person.LastName + ', ' + Person.FirstName as AgentName, \r\n" +
          "LastName = Person.LastName, \r\n" +
          "FirstName = Person.FirstName, \r\n" +
          "LoginName = Person.LoginName, \r\n" +
          "PeripheralNumber = Agent.PeripheralNumber, \r\n" +
          "SkillName = Skill_Group.EnterpriseName, \r\n" +   
          "SkillTargetID = Skill_Group.SkillTargetID, \r\n" +
          "DateTime = ART.DateTime, \r\n" +
          "Date=CONVERT(char(10),ART.DateTime,101), \r\n" +            
          "AgentSkillTargetID = ART.SkillTargetID, \r\n" +           
          "ServiceName=Service.EnterpriseName, \r\n" +
          "Media= MRD.EnterpriseName, \r\n" +
          "AgentStateCode= ASGRT.AgentState, \r\n" +          
          "AgentState=CASE ASGRT.AgentState \r\n" +  
              "WHEN 0 THEN 'Logged Out'  \r\n" +
              "WHEN 1 THEN 'Logged On' \r\n" + 
              "WHEN 2 THEN 'Not Ready' \r\n" + 
              "WHEN 3 THEN 'Ready' \r\n" +
              "WHEN 4 THEN 'Talking' \r\n" + 
              "WHEN 5 THEN 'Work Not Ready' \r\n" + 
              "WHEN 6 THEN 'Work Ready' \r\n" + 
              "WHEN 7 THEN 'Busy Other' \r\n" + 
              "WHEN 8 THEN 'Reserved' \r\n" +  
              "WHEN 9 THEN 'Unknown' \r\n" + 
              "WHEN 10 THEN 'Hold' \r\n" + 
              "WHEN 11 THEN 'Active' \r\n" +  
              "WHEN 12 THEN 'Paused' \r\n" + 
              "WHEN 13 THEN 'Interrupted' \r\n" + 
              "WHEN 14 THEN 'Not Active' \r\n" + 
              "ELSE CONVERT(VARCHAR, ASGRT.AgentState) END, \r\n" +
          "PhoneTypeText=(CASE ART.PhoneType \r\n" + 
              "When 0 THEN 'Not Mobile' \r\n" + 
              "WHEN 1 THEN 'Call By Call' \r\n" + 
              "WHEN 2 THEN 'Nailed Connection' \r\n" +
              "Else 'Not Applicable' END),  ART.RemotePhoneNumber as RemotePhoneNumber, \r\n" + 
          "Reason=(CASE WHEN ASGRT.ReasonCode = 0 THEN 'NONE' \r\n" +
              "ELSE ISNULL((select ReasonText from Reason_Code where ReasonCode=ASGRT.ReasonCode),ASGRT.ReasonCode)  END), \r\n" +
          "ReasonCode = ASGRT.ReasonCode, \r\n"+
          "Extension = ART.Extension, \r\n" +
          "DateTimeLastStateChange = ASGRT.DateTimeLastStateChange, \r\n" +
          "DateTimeLogin = ASGRT.DateTimeLogin, \r\n" +
          "RequestedSupervisorAssist=(CASE WHEN RequestedSupervisorAssist = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "Destination=CASE ART.Destination \r\n" + 
              "WHEN 1 THEN 'ACD' \r\n" +
              "WHEN 2 THEN 'Direct' \r\n" +
              "WHEN 3 THEN 'Auto Out' \r\n" +
              "WHEN 4 THEN 'Reserve' \r\n" +
              "WHEN 5 THEN 'Preview' \r\n" +
              "ELSE 'Not Applicable' END, \r\n" +
          "DirectionText=CASE \r\n" +
              "WHEN ASGRT.AgentState = 7 THEN 'Not Applicable' \r\n" +
              "WHEN ART.Direction = 1 THEN 'In' \r\n" +
              "WHEN ART.Direction = 2 THEN 'Out' \r\n" +
              "WHEN ART.Direction = 3 THEN 'Other In' \r\n" +
              "WHEN ART.Direction = 4 THEN 'Other Out' \r\n" +
              "WHEN ART.Direction = 5 THEN 'Out Reserve' \r\n" +
              "WHEN ART.Direction = 6 THEN 'Out Preview' \r\n" +
              "WHEN ART.Direction = 7 THEN 'Out Predictive' \r\n" +
              "ELSE 'Not Applicable' END,Direction = ART.Direction, \r\n" +
          "OnHold= (CASE WHEN ART.OnHold = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "NetworkTargetID = ART.NetworkTargetID, \r\n" +
          "AgentStatus = ART.AgentStatus, \r\n" +
          "CustomerPhoneNumber = ART.CustomerPhoneNumber, \r\n" +
          "CustomerAccountNumber = ART.CustomerAccountNumber, \r\n" +       
          "CampaignID = ART.CampaignID, \r\n" +
          "QueryRuleID = ART.QueryRuleID,\r\n" +
          "Duration = DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
              "CASE WHEN (DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
                "(SELECT NowTime \r\n" +
                "from Controller_Time (nolock))) <=0) \r\n" + 
              "THEN ASGRT.DateTimeLastStateChange \r\n" +
              "ELSE (SELECT NowTime FROM Controller_Time (nolock))END), \r\n" +
          "RoutableText=(CASE WHEN ART.Routable = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "DateTimeLastModeChange = ART.DateTimeLastModeChange, \r\n" +
          "CallsInProgress = ART.CallInProgress, \r\n" +
          "MaxTasks = ART.MaxTasks, \r\n" +
          "AvailInMRDText=(CASE \r\n" +
              "WHEN ART.AvailableInMRD = 0 THEN 'No' \r\n" +
              "WHEN ART.AvailableInMRD = 1 THEN 'Yes_ICM' \r\n" +
              "WHEN ART.AvailableInMRD = 2 THEN 'Yes_APP' \r\n" +
              "ELSE 'No' END), \r\n" +
          "DateTimeTaskLevelChange = ART.DateTimeTaskLevelChange, \r\n" +
          "RouterCallsQNow = Skill_Group_Real_Time.RouterCallsQNow, \r\n" +
          "RouterCallsQueueNow = ART.RouterCallsQueueNow, \r\n" +
          "RouterLongestCallQ = ART.RouterLongestCallQ, \r\n" +
          "totalrouterqueuednow=isnull(Skill_Group_Real_Time.RouterCallsQNow,0) + isnull(Skill_Group_Real_Time.CallsQueuedNow,0) \r\n" +
        "FROM  Agent (nolock), \r\n" +
        "Person (nolock), \r\n" +
        "Agent_Skill_Group_Real_Time ASGRT, \r\n" +
        "Agent_Real_Time ART (nolock) LEFT OUTER JOIN \r\n" +
        "Service (nolock) ON ART.ServiceSkillTargetID = Service.SkillTargetID, \r\n" +
        "Skill_Group (nolock), \r\n" +
        "Media_Routing_Domain MRD (nolock), \r\n" +
        "Skill_Group_Real_Time (nolock), \r\n" +
        "Reason_Code rc (nolock) \r\n" +
        "WHERE Skill_Group.SkillTargetID IN (5104) and Agent.PersonID=Person.PersonID \r\n" +
        "AND Agent.SkillTargetID = ART.SkillTargetID \r\n" +
        "AND ART.SkillTargetID = ASGRT.SkillTargetID \r\n" +
        "AND ART.MRDomainID = MRD.MRDomainID \r\n" +
        "AND Skill_Group.MRDomainID = ART.MRDomainID \r\n" +
        "AND Skill_Group.SkillTargetID = ASGRT.SkillGroupSkillTargetID \r\n" + 
        "AND Skill_Group.SkillTargetID= Skill_Group_Real_Time.SkillTargetID \r\n" +
        "AND Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID  FROM Skill_Group WHERE Priority > 0 AND Deleted != 'Y') \r\n"+
        "AND rc.ReasonCode= ASGRT.ReasonCode \r\n"+
        "AND rc.ReasonCode = '"+state+"'";
    }
           
		return jdbcCuicTemplate.query(sql, new AuxTimeRowMapper());
  }

	@Override
	public List<AuxTime> getAuxTimes() {
		String sql = " SELECT EnterpriseName = Agent.EnterpriseName, Person.LastName + ', ' + Person.FirstName as AgentName, \r\n" +
          "LastName = Person.LastName, \r\n" +
          "FirstName = Person.FirstName, \r\n" +
          "LoginName = Person.LoginName, \r\n" +
          "PeripheralNumber = Agent.PeripheralNumber, \r\n" +
          "SkillName = Skill_Group.EnterpriseName, \r\n" +   
          "SkillTargetID = Skill_Group.SkillTargetID, \r\n" +
          "DateTime = ART.DateTime, \r\n" +
          "Date=CONVERT(char(10),ART.DateTime,101), \r\n" +            
          "AgentSkillTargetID = ART.SkillTargetID, \r\n" +           
          "ServiceName=Service.EnterpriseName, \r\n" +
          "Media= MRD.EnterpriseName, \r\n" +
          "AgentStateCode= ASGRT.AgentState, \r\n" +          
          "AgentState=CASE ASGRT.AgentState \r\n" +  
              "WHEN 0 THEN 'Logged Out'  \r\n" +
              "WHEN 1 THEN 'Logged On' \r\n" + 
              "WHEN 2 THEN 'Not Ready' \r\n" + 
              "WHEN 3 THEN 'Ready' \r\n" +
              "WHEN 4 THEN 'Talking' \r\n" + 
              "WHEN 5 THEN 'Work Not Ready' \r\n" + 
              "WHEN 6 THEN 'Work Ready' \r\n" + 
              "WHEN 7 THEN 'Busy Other' \r\n" + 
              "WHEN 8 THEN 'Reserved' \r\n" +  
              "WHEN 9 THEN 'Unknown' \r\n" + 
              "WHEN 10 THEN 'Hold' \r\n" + 
              "WHEN 11 THEN 'Active' \r\n" +  
              "WHEN 12 THEN 'Paused' \r\n" + 
              "WHEN 13 THEN 'Interrupted' \r\n" + 
              "WHEN 14 THEN 'Not Active' \r\n" + 
              "ELSE CONVERT(VARCHAR, ASGRT.AgentState) END, \r\n" +
          "PhoneTypeText=(CASE ART.PhoneType \r\n" + 
              "When 0 THEN 'Not Mobile' \r\n" + 
              "WHEN 1 THEN 'Call By Call' \r\n" + 
              "WHEN 2 THEN 'Nailed Connection' \r\n" +
              "Else 'Not Applicable' END),  ART.RemotePhoneNumber as RemotePhoneNumber, \r\n" + 
          "Reason=(CASE WHEN ASGRT.ReasonCode = 0 THEN 'NONE' \r\n" +
              "ELSE ISNULL((select ReasonText from Reason_Code where ReasonCode=ASGRT.ReasonCode),ASGRT.ReasonCode)  END), \r\n" +
          "ReasonCode = ASGRT.ReasonCode, \r\n"+
          "Extension = ART.Extension, \r\n" +
          "DateTimeLastStateChange = ASGRT.DateTimeLastStateChange, \r\n" +
          "DateTimeLogin = ASGRT.DateTimeLogin, \r\n" +
          "RequestedSupervisorAssist=(CASE WHEN RequestedSupervisorAssist = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "Destination=CASE ART.Destination \r\n" + 
              "WHEN 1 THEN 'ACD' \r\n" +
              "WHEN 2 THEN 'Direct' \r\n" +
              "WHEN 3 THEN 'Auto Out' \r\n" +
              "WHEN 4 THEN 'Reserve' \r\n" +
              "WHEN 5 THEN 'Preview' \r\n" +
              "ELSE 'Not Applicable' END, \r\n" +
          "DirectionText=CASE \r\n" +
              "WHEN ASGRT.AgentState = 7 THEN 'Not Applicable' \r\n" +
              "WHEN ART.Direction = 1 THEN 'In' \r\n" +
              "WHEN ART.Direction = 2 THEN 'Out' \r\n" +
              "WHEN ART.Direction = 3 THEN 'Other In' \r\n" +
              "WHEN ART.Direction = 4 THEN 'Other Out' \r\n" +
              "WHEN ART.Direction = 5 THEN 'Out Reserve' \r\n" +
              "WHEN ART.Direction = 6 THEN 'Out Preview' \r\n" +
              "WHEN ART.Direction = 7 THEN 'Out Predictive' \r\n" +
              "ELSE 'Not Applicable' END,Direction = ART.Direction, \r\n" +
          "OnHold= (CASE WHEN ART.OnHold = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "NetworkTargetID = ART.NetworkTargetID, \r\n" +
          "AgentStatus = ART.AgentStatus, \r\n" +
          "CustomerPhoneNumber = ART.CustomerPhoneNumber, \r\n" +
          "CustomerAccountNumber = ART.CustomerAccountNumber, \r\n" +       
          "CampaignID = ART.CampaignID, \r\n" +
          "QueryRuleID = ART.QueryRuleID,\r\n" +
          "Duration = DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
              "CASE WHEN (DATEDIFF(ss, ASGRT.DateTimeLastStateChange, \r\n" +
                "(SELECT NowTime \r\n" +
                "from Controller_Time (nolock))) <=0) \r\n" + 
              "THEN ASGRT.DateTimeLastStateChange \r\n" +
              "ELSE (SELECT NowTime FROM Controller_Time (nolock))END), \r\n" +
          "RoutableText=(CASE WHEN ART.Routable = 1 THEN 'Yes' ELSE 'No' END), \r\n" +
          "DateTimeLastModeChange = ART.DateTimeLastModeChange, \r\n" +
          "CallsInProgress = ART.CallInProgress, \r\n" +
          "MaxTasks = ART.MaxTasks, \r\n" +
          "AvailInMRDText=(CASE \r\n" +
              "WHEN ART.AvailableInMRD = 0 THEN 'No' \r\n" +
              "WHEN ART.AvailableInMRD = 1 THEN 'Yes_ICM' \r\n" +
              "WHEN ART.AvailableInMRD = 2 THEN 'Yes_APP' \r\n" +
              "ELSE 'No' END), \r\n" +
          "DateTimeTaskLevelChange = ART.DateTimeTaskLevelChange, \r\n" +
          "RouterCallsQNow = Skill_Group_Real_Time.RouterCallsQNow, \r\n" +
          "RouterCallsQueueNow = ART.RouterCallsQueueNow, \r\n" +
          "RouterLongestCallQ = ART.RouterLongestCallQ, \r\n" +
          "totalrouterqueuednow=isnull(Skill_Group_Real_Time.RouterCallsQNow,0) + isnull(Skill_Group_Real_Time.CallsQueuedNow,0) \r\n" +
        "FROM  Agent (nolock), \r\n" +
        "Person (nolock), \r\n" +
        "Agent_Skill_Group_Real_Time ASGRT, \r\n" +
        "Agent_Real_Time ART (nolock) LEFT OUTER JOIN \r\n" +
        "Service (nolock) ON ART.ServiceSkillTargetID = Service.SkillTargetID, \r\n" +
        "Skill_Group (nolock), \r\n" +
        "Media_Routing_Domain MRD (nolock), \r\n" +
        "Skill_Group_Real_Time (nolock) \r\n" +
        "WHERE Skill_Group.SkillTargetID IN (5104) and Agent.PersonID=Person.PersonID \r\n" +
        "AND Agent.SkillTargetID = ART.SkillTargetID \r\n" +
        "AND ART.SkillTargetID = ASGRT.SkillTargetID \r\n" +
        "AND ART.MRDomainID = MRD.MRDomainID \r\n" +
        "AND Skill_Group.MRDomainID = ART.MRDomainID \r\n" +
        "AND Skill_Group.SkillTargetID = ASGRT.SkillGroupSkillTargetID \r\n" + 
        "AND Skill_Group.SkillTargetID= Skill_Group_Real_Time.SkillTargetID \r\n" +
        "AND Skill_Group.SkillTargetID NOT IN (SELECT BaseSkillTargetID  FROM Skill_Group WHERE Priority > 0 AND Deleted != 'Y') \r\n" +
        "AND ASGRT.AgentState=2"
        ;
		return jdbcCuicTemplate.query(sql, new AuxTimeRowMapper());
  }
  

  
	public List<CallLog> getcalllog(String startPeriode, String endPeriode) {
    String sql = "SELECT ID, NO_REF, USERNAME, TRANSAKSI_STATUS, TERPUTUS_MD_ID, TOLAK_MD_ID, TRANSAKSI_DURATION \r\n" +
    "FROM CALL_LOG \r\n" +
    "WHERE CREATED_DATE BETWEEN TO_DATE('"+startPeriode+" 00:00:00','YYYY-MM-DD HH24:MI:SS') AND TO_DATE('"+endPeriode+" 23:59:00','YYYY-MM-DD HH24:MI:SS') \r\n";
		return jdbcChainTemplate.query(sql, new CallLogRowMapper());
  }


  public List<DaftarAgent> getAgentToday (String startPeriode, String endPeriode) {
    String sql = "SELECT USERNAME, \r\n" +
    "SUM(CASE WHEN TRANSAKSI_STATUS = 'SUCCESS' THEN 1 ELSE 0 END) AS berhasil, \r\n" +
    "count(TRANSAKSI_STATUS) AS jumlah FROM call_log \r\n" +
    "WHERE CREATED_DATE BETWEEN TO_DATE('"+startPeriode+" 00:00:00','YYYY-MM-DD HH24:MI:SS') AND TO_DATE('"+endPeriode+" 23:59:59','YYYY-MM-DD HH24:MI:SS') \r\n"+
    "GROUP BY USERNAME";
		return jdbcChainTemplate.query(sql, new AgentTodayRowMapper());

  }

  public List<CallLog> getcalllogAgent(String startPeriode, String endPeriode, String agentLoginName) {
    String sql = "SELECT ID, NO_REF, USERNAME, TRANSAKSI_STATUS, TERPUTUS_MD_ID, TOLAK_MD_ID, TRANSAKSI_DURATION \r\n" +
    "FROM CALL_LOG \r\n" +
    "WHERE CREATED_DATE BETWEEN TO_DATE('"+startPeriode+" 00:00:00','YYYY-MM-DD HH24:MI:SS') AND TO_DATE('"+endPeriode+" 23:59:59','YYYY-MM-DD HH24:MI:SS') \r\n" +
    "AND USERNAME ='"+agentLoginName+"'";
		return jdbcChainTemplate.query(sql, new CallLogRowMapper());
  }

  public List<DateDiff> getDateDiff(String startPeriode, String endPeriode) {
    String sql = "SELECT TO_NUMBER(TO_DATE(DROPPED_DATE, 'YYYY-MM-DD HH24:MI:SS') - TO_DATE(ANSWER_DATE, 'YYYY-MM-DD HH24:MI:SS')) * 86400 AS diff_hours \r\n" +
    "FROM CALL_LOG \r\n"+
    "WHERE CREATED_DATE BETWEEN TO_DATE('"+startPeriode+" 00:00:00','YYYY-MM-DD HH24:MI:SS') AND TO_DATE('"+endPeriode+" 23:59:59','YYYY-MM-DD HH24:MI:SS') AND TRANSAKSI_STATUS = 'SUCCESS'";
    return jdbcChainTemplate.query(sql, new DateDiffRowMapper());
  }

  public List<DateDiff> getDateDiffAgent(String startPeriode, String endPeriode, String agentLoginName) {
    String sql = "SELECT TO_NUMBER(TO_DATE(DROPPED_DATE, 'YYYY-MM-DD HH24:MI:SS') - TO_DATE(ANSWER_DATE, 'YYYY-MM-DD HH24:MI:SS')) * 86400 AS diff_hours \r\n" +
    "FROM CALL_LOG \r\n"+
    "WHERE CREATED_DATE BETWEEN TO_DATE('"+startPeriode+" 00:00:00','YYYY-MM-DD HH24:MI:SS') AND TO_DATE('"+endPeriode+" 23:59:59','YYYY-MM-DD HH24:MI:SS') AND USERNAME ='"+agentLoginName+"' AND TRANSAKSI_STATUS = 'SUCCESS'";
    return jdbcChainTemplate.query(sql, new DateDiffRowMapper());
  }



  public AuxTimeOutput getAgetRealTimeStatus (String state){
    List<AuxTime> aux = getAuxByState(state);

    AuxTimeOutput output = new AuxTimeOutput();
    
    output.setOutput_schema(aux);

    return output;
  }

  public DaftarAgentPaging listDaftarAgent (String startPeriode, String endPeriode, String agent, int pageNumber, int rowPerPage) {
    List<DaftarAgent> at = getAgentToday(startPeriode, endPeriode);
    List<RedirectCall> rc = cuicMonitoringService.getRedirectCalls(startPeriode, endPeriode);
    List<AuxTime> rd = null;
    if("all".equalsIgnoreCase(agent)){
      rd = getStateReadyAux();
    }else{
      rd = getStateReadyAuxbyAgent(agent);
    }
    List<StatusAux> sa = cuicMonitoringService.statusAux(startPeriode,  endPeriode);
    
    DaftarAgentPaging dap = new DaftarAgentPaging();
    int totalpage=0;
    

    List<AuxTime> ast = rd.stream().map(r -> {
      DaftarAgent da = at.stream().filter(a -> a.getUsername().equals(r.getLoginName())).findFirst()
      .orElse(null);
      if(da != null){
        r.setJumlah(da.getJumlah());
        r.setBerhasil(da.getBerhasil());
      }
      // rc.stream().mapToInt(RedirectCall:: getRedirectCalls).sum();
      //  RedirectCall rona = rc.stream().filter(d -> d.getLoginname().equals(r.getLoginName())).findFirst()
      int rona =(int) rc.stream().filter(d -> d.getLoginname().equals(r.getLoginName())).mapToInt(RedirectCall:: getRedirectCalls).sum();

      // .orElse(null);
      // if(rona != null) {
      //   r.setRona(rona.getRedirectCalls());
      // }
      if(rona != 0) {
        r.setRona(rona);
      }

      return r;
    }).collect(Collectors.toList());
   
    List<AuxTime> resultAux = ast.stream()
    .skip(pageNumber * rowPerPage)
    .limit(rowPerPage).collect(Collectors.toList());
    
    dap.setDa(resultAux);

    int totalData = ast.size();
    totalpage = totalData/rowPerPage;
    dap.setTotalPage(totalpage);
    dap.setTotalData(totalData);
    dap.setRowPerPage(rowPerPage);
    dap.setCurrentPage(pageNumber);
    if((pageNumber+1) == totalpage){
      dap.setLast(true);
    } else {
      dap.setLast(false);
    }    
    if ((pageNumber+1) == 1){
      dap.setFirst(true);
    } else {
      dap.setFirst(false);
    }

    return dap;
  } 

  public StatistikPanggilanAgent getStatistikPanggilanAgent(String startPeriode, String endPeriode, String agentLoginName){
    List<CallLog> cl = getcalllogAgent(startPeriode, endPeriode, agentLoginName); 
    List<DateDiff> dd = getDateDiffAgent(startPeriode, endPeriode, agentLoginName);

    int totalDiterima = (int)cl.stream().filter(c -> c.getNo_ref() != null).count();
    int totalBerhasil = (int)cl.stream().filter(c-> "SUCCESS".equals(c.getTransaksi_status())).count();
    int totalPending = (int)cl.stream().filter(c-> "PENDING".equals(c.getTransaksi_status())).count();
    int totalTolak= (int)cl.stream().filter(c->c.getTolak_md_id() != null).count();
    int totalTerputus = (int)cl.stream().filter(c-> c.getTerputus_md_id() != null ).count();
    int durasi = dd.stream().mapToInt(DateDiff:: getDiff_hours).sum();
    int average = 0;
    if(durasi != 0 && totalBerhasil != 0){
      average = (int)Math.ceil((double)durasi/totalBerhasil);
      // average = durasi/totalBerhasil;
    }

    StatistikPanggilanAgent sp = new StatistikPanggilanAgent();
    sp.setAverageHandlingTime(average);
    sp.setTransaksiBerhasil(totalBerhasil);
    sp.setPanggilanTerputus(totalTerputus);
    sp.setTransaksiDitolak(totalTolak);
    sp.setTransaksiPending(totalPending);
    sp.setPanggilanDiterima(totalDiterima);
    sp.setServisLevelTarget(serviceLevel);
    
    return sp;
  }
  
  public StatistikPanggilan getStatistikPanggilan(String startPeriode, String endPeriode){
    List<AbandonCall> ac = cuicMonitoringService.getAbandonCall(startPeriode, endPeriode);
    List<RedirectCall> rc = cuicMonitoringService.getRedirectCalls(startPeriode, endPeriode);
    List<CallLog> cl = getcalllog(startPeriode, endPeriode);    
    List<Queue> q = cuicMonitoringService.getCallsQueue();
    List<DateDiff> dd = getDateDiff(startPeriode, endPeriode);

    int totalAbandon = ac.stream().mapToInt(AbandonCall:: getAbandon).sum();
    int totalRona = rc.stream().mapToInt(RedirectCall:: getRedirectCalls).sum();
    long totalTerputus = cl.stream().filter(c -> c.getTerputus_md_id() != null).count();
    long totalTolak = cl.stream().filter(c -> c.getTolak_md_id() != null).count();
    long totalBerhasil = cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).count();
    int durasi = dd.stream().mapToInt(DateDiff:: getDiff_hours).sum();
    int callberhasil = (int) totalBerhasil;
    int average = 0;
    if(durasi != 0 && callberhasil != 0){
      average = (int)Math.ceil((double)durasi/callberhasil);
      // average = durasi/callberhasil;
    }
    int queue = q.get(0).getRouterCallsQnow();


    StatistikPanggilan sp = new StatistikPanggilan();
    sp.setAbandonCall(totalAbandon);
    sp.setRona(totalRona);
    sp.setPanggilanTerputus(totalTerputus); 
    sp.setPanggilanDitolak(totalTolak);
    sp.setTransaksiBerhasil(totalBerhasil);
    sp.setAverageHandlingTime(average);
    sp.setQueue(queue);
    return sp;
  }
  
  public PerformanceReview spvPR(String startPeriode, String endPeriode) {

    List<RedirectCall> rc = cuicMonitoringService.getRedirectCalls(startPeriode, endPeriode);
    List<CallLog> cl = getcalllog(startPeriode, endPeriode);
    List<Queue> q = cuicMonitoringService.getCallsQueue();
    List<AbandonCall> ac = cuicMonitoringService.getAbandonCall(startPeriode, endPeriode);
    List<DateDiff> dd = getDateDiff(startPeriode, endPeriode);

    int totalDitolak = (int)cl.stream().filter(c -> c.getTolak_md_id() != null).count();
    int totalRona = rc.stream().mapToInt(RedirectCall:: getRedirectCalls).sum();
    int totalBerhasil = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).count();
    int totalTerputus = (int)cl.stream().filter(c -> c.getTerputus_md_id() != null).count();
    int totalDiterima = (int)cl.stream().filter(c -> c.getNo_ref() != null).count();
    int durasi = dd.stream().mapToInt(DateDiff:: getDiff_hours).sum();
    int queue = q.get(0).getRouterCallsQnow();
    // int abandon = ac.get(0).getAbandon();
    int abandon = ac.stream().mapToInt(AbandonCall::getAbandon).sum();
    
    int average = 0;
    // int panggilanLbhSl = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).filter(a -> a.getTransaksi_duration() > serviceLevel).count();
    int panggilanLbhSl = (int)dd.stream().filter(a -> a.getDiff_hours() > serviceLevel).count();

    float serviceLevelPerc = 0;

    // int trxBerhasilkrgSl = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).filter(a -> a.getTransaksi_duration() <= serviceLevel).count();
    int trxBerhasilkrgSl = (int)dd.stream().filter(a -> a.getDiff_hours() <= serviceLevel).count();

    if(totalBerhasil > 0){
      serviceLevelPerc = ((float)trxBerhasilkrgSl/(float)totalBerhasil) * 100;
    }

    if(durasi != 0 && totalBerhasil != 0){
      average = (int)Math.ceil((double)durasi/totalBerhasil);

      // average = durasi/totalBerhasil;//Average Handling Time	Formula = Durasi / Jumlah Panggilan Diterima

    }

    PerformanceReview pr = new PerformanceReview();
    pr.setPanggilanMasuk(totalDiterima + totalRona);
    pr.setPanggilanDiterima(totalDiterima);
    pr.setPanggilanDitolak(totalDitolak);
    pr.setRona(totalRona);
    pr.setTransaksiBerhasil(totalBerhasil);
    pr.setPanggilanTerputus(totalTerputus);
    pr.setAverageHandlingTime(average);
    pr.setQueue(queue);
    pr.setAbandon(abandon);
    pr.setPanggilanLebihDariSL(panggilanLbhSl);
    pr.setServiceLevel(serviceLevelPerc);
    pr.setDurasiBerhasil(durasi);

    return pr;
  }

  public PerformanceReview agentPR(String startPeriode, String endPeriode, String agentLoginName) {

    List<RedirectCall> rc = cuicMonitoringService.getRedirectCallsByAgent(startPeriode, endPeriode, agentLoginName);
    List<CallLog> cl = getcalllogAgent(startPeriode, endPeriode, agentLoginName);    
    List<Queue> q = cuicMonitoringService.getCallsQueue();
    List<AbandonCall> ac = cuicMonitoringService.getAbandonCall(startPeriode, endPeriode);
    List<DateDiff> dd = getDateDiffAgent(startPeriode, endPeriode, agentLoginName);

    int totalDitolak = (int)cl.stream().filter(c -> c.getTolak_md_id() != null).count();
    int totalRona = rc.stream().mapToInt(RedirectCall:: getRedirectCalls).sum();
    int totalBerhasil = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).count();
    int totalTerputus = (int)cl.stream().filter(c -> c.getTerputus_md_id() != null).count();
    int totalDiterima = (int)cl.stream().filter(c -> c.getNo_ref() != null).count();
    int durasi = dd.stream().mapToInt(DateDiff:: getDiff_hours).sum();
    int queue = q.get(0).getRouterCallsQnow();
    int abandon = ac.get(0).getAbandon();

    // int panggilanLbhSl = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).filter(a -> a.getTransaksi_duration() > serviceLevel).count();
    int panggilanLbhSl = (int)dd.stream().filter(a -> a.getDiff_hours() > serviceLevel).count();

    // int trxBerhasilkrgSl = (int)cl.stream().filter(c -> "SUCCESS".equals(c.getTransaksi_status())).filter(a -> a.getTransaksi_duration() <= serviceLevel).count();
    int trxBerhasilkrgSl = (int)dd.stream().filter(a -> a.getDiff_hours() <= serviceLevel).count();

    float serviceLevelPerc = 0;

    if(totalBerhasil > 0){
      serviceLevelPerc = ((float)trxBerhasilkrgSl/(float)totalBerhasil) * 100;
    }

    int average = 0;
    if(durasi != 0 && totalBerhasil != 0){
      average = (int)Math.ceil((double)durasi/totalBerhasil);

      // average = durasi/totalBerhasil;//Average Handling Time	Formula = Durasi / Jumlah Panggilan Diterima

    }
    PerformanceReview pr = new PerformanceReview();
    pr.setPanggilanMasuk(totalDiterima + totalRona);
    pr.setPanggilanDiterima(totalDiterima);
    pr.setPanggilanDitolak(totalDitolak);
    pr.setRona(totalRona);
    pr.setTransaksiBerhasil(totalBerhasil);
    pr.setPanggilanTerputus(totalTerputus);
    pr.setAverageHandlingTime(average);
    pr.setQueue(queue);
    pr.setAbandon(abandon);
    pr.setPanggilanLebihDariSL(panggilanLbhSl);
    pr.setServiceLevel(serviceLevelPerc);

    return pr;
  }

  public HistoryAux agentPRAux(String startPeriode, String endPeriode, String agentLoginName) {
    
    List<StatusAux> sa = cuicMonitoringService.statusAuxAgent(startPeriode,  endPeriode, agentLoginName);
    List<StatusAux> san = cuicMonitoringService.statusAuxAgentNewQuery(startPeriode,  endPeriode, agentLoginName);
    List<AuxTime> rd = getStateReadyAux();
    HistoryAux ha = new HistoryAux();

    //for dev
//    int totalIbadah = (int)sa.stream().filter(s -> "6138".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalIstirahat = (int)sa.stream().filter(s -> "5098".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalJobRutin = (int)sa.stream().filter(s -> "55228".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalOtherReason = (int)sa.stream().filter(s -> "5737".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalToilet = (int)sa.stream().filter(s -> "45560".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalMeeting = (int)sa.stream().filter(s -> "34982".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    int totalMaintenance = (int)sa.stream().filter(s -> "6242".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
//    // int ready = (int)rd.stream().filter(s -> s.getAgentStateCode() == 3).mapToInt(AuxTime::getDuration).sum();
    
    //for prod
    int totalIbadah = (int)sa.stream().filter(s -> "29727".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalIstirahat = (int)sa.stream().filter(s -> "63421".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalJobRutin = (int)sa.stream().filter(s -> "14337".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalOtherReason = (int)sa.stream().filter(s -> "48439".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalToilet = (int)sa.stream().filter(s -> "55963".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalMeeting = (int)sa.stream().filter(s -> "58283".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    int totalMaintenance = (int)sa.stream().filter(s -> "7542".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int ready = (int)rd.stream().filter(s -> s.getAgentStateCode() == 3).mapToInt(AuxTime::getDuration).sum();
 
    int totalLoginTime = (int)sa.stream().mapToInt(StatusAux::getTotalLoginTime).sum();
    int totalNotReady = (int)sa.stream().mapToInt(StatusAux::getTotalNotReadyTime).sum();

    int ready = 0;
    if(totalLoginTime > totalNotReady){
     ready = totalLoginTime - totalNotReady;
    }
    ready = (int)san.stream().mapToInt(StatusAux::getAgentReady).sum();
    // int readyDuration = 0;
    // if(rd.size() != 0){
    //    readyDuration = rd.get(0).getDuration();
    // }
    
    ha.setIbadah(totalIbadah);
    ha.setIstirahat(totalIstirahat);
    ha.setJobRutin(totalJobRutin);
    ha.setOtherReason(totalOtherReason);
    ha.setMeeting(totalMeeting);
    ha.setReady(ready);
    ha.setToilet(totalToilet);
    ha.setMaintenance(totalMaintenance);
    ha.setTotalProduktivitas(ready + totalJobRutin + totalMeeting);

    return ha;
  }

  public HistoryAux spvPRAux(String startPeriode, String endPeriode) {
    
    List<StatusAux> sa = cuicMonitoringService.statusAux(startPeriode,  endPeriode);
    List<AuxTime> rd = getStateReadyAux();
    HistoryAux ha = new HistoryAux();

    // list status reason code prod
    // int totalIbadah = (int)sa.stream().filter(s -> "29727".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalIstirahat = (int)sa.stream().filter(s -> "64318".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalJobRutin = (int)sa.stream().filter(s -> "14337".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalOtherReason = (int)sa.stream().filter(s -> "48439".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalToilet = (int)sa.stream().filter(s -> "55963".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalMeeting = (int)sa.stream().filter(s -> "58283".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();
    // int totalMaintenance = (int)sa.stream().filter(s -> "7542".equals(s.getReasonCode())).mapToInt(StatusAux::getTotalCodeDuration).sum();

    //for dev
//    int totalMaintenance = (int)rd.stream().filter(s -> s.getReasonCode() == 6242).count();
//    int totalIbadah = (int)rd.stream().filter(s -> s.getReasonCode() == 6138).count();
//    int totalIstirahat = (int)rd.stream().filter(s -> s.getReasonCode() == 5098).count();
//    int totalJobRutin = (int)rd.stream().filter(s -> s.getReasonCode() == 55228).count();
//    int totalOtherReason = (int)rd.stream().filter(s -> s.getReasonCode() == 5737).count();
//    int totalToilet = (int)rd.stream().filter(s -> s.getReasonCode() == 45560).count();
//    int totalMeeting = (int)rd.stream().filter(s -> s.getReasonCode() == 34982).count();//keperluan khusus
//    int ready = (int)rd.stream().filter(s -> s.getAgentStateCode() == 3).count();
    
    //for prod
    int totalMaintenance = (int)rd.stream().filter(s -> s.getReasonCode() == 7542).count();
    int totalIbadah = (int)rd.stream().filter(s -> s.getReasonCode() == 29727).count();
    int totalIstirahat = (int)rd.stream().filter(s -> s.getReasonCode() == 63421).count();
    int totalJobRutin = (int)rd.stream().filter(s -> s.getReasonCode() == 14337).count();
    int totalOtherReason = (int)rd.stream().filter(s -> s.getReasonCode() == 48439).count();
    int totalToilet = (int)rd.stream().filter(s -> s.getReasonCode() == 55963).count();
    int totalMeeting = (int)rd.stream().filter(s -> s.getReasonCode() == 58283).count();//keperluan khusus
    int ready = (int)rd.stream().filter(s -> s.getAgentStateCode() == 3).count();

    ha.setIbadah(totalIbadah);
    ha.setIstirahat(totalIstirahat);
    ha.setJobRutin(totalJobRutin);
    ha.setOtherReason(totalOtherReason);
    ha.setMeeting(totalMeeting);
    ha.setReady(ready);
    ha.setToilet(totalToilet);
    ha.setMaintenance(totalMaintenance);
    // ha.setTotalProduktivitas(ready + totalJobRutin + totalMeeting);

    return ha;
  }

}
