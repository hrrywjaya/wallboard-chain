package com.mitrakreasindo.pemrek.external.cuic.model;

import java.util.List;

import lombok.Data;

@Data
public class AuxTime {
  private int duration;
  private String agentState;
  private int agentStateCode;
  private int reasonCode;
  private String reason;
  private String loginName;
  private int jumlah;
  private int berhasil;
  private int totalLogin;
  private int rona;
  private String loginDate;
  private String firstName;
  private String lastName;
}


