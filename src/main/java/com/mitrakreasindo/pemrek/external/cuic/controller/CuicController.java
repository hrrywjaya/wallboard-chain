package com.mitrakreasindo.pemrek.external.cuic.controller;

import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.AbandonCall;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTime;
import com.mitrakreasindo.pemrek.external.cuic.model.AuxTimeOutput;
import com.mitrakreasindo.pemrek.external.cuic.model.CallHandle;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLog;
import com.mitrakreasindo.pemrek.external.cuic.model.DaftarAgentPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHold;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHoldPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.HistoryAux;
import com.mitrakreasindo.pemrek.external.cuic.model.PerformanceReview;
import com.mitrakreasindo.pemrek.external.cuic.model.Queue;
import com.mitrakreasindo.pemrek.external.cuic.model.RedirectCall;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilan;
import com.mitrakreasindo.pemrek.external.cuic.model.StatistikPanggilanAgent;
import com.mitrakreasindo.pemrek.external.cuic.model.TransactionSummary;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicHistoryTransactionService;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicHomeService;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicMonitoringService;

@RestController
@RequestMapping("/capi/cuic")
public class CuicController {

	@Autowired
	private CuicHistoryTransactionService cuicService;
	@Autowired
	private CuicMonitoringService cuicMonitoringService;
  @Autowired
  private CuicHomeService cuicHomeService;

	@GetMapping("/transactionSummary")
	public DurationHoldPaging testTs(
    @RequestParam String startDate,     // "2019-10-31 00:00:00"
    @RequestParam String endDate,     // "2019-10-31 23:59:59"
    @RequestParam(required = false) String agent,
    @RequestParam(required = false) String dataNasabah,
    @RequestParam(required = false) String dataNasabah2,
    @RequestParam(required = false) String namaCabang,
    @RequestParam(required = false) String trxStatus,
    @RequestParam(required = false) String trxType,
    @RequestParam(required = false) String trxType2,
    @RequestParam(required = false) String role,
    @RequestParam(required = false) String username,
    @RequestParam(required = false) String title,
    
    @RequestParam int pageNumber,
    @RequestParam int rowPerPage
  ) {
    return cuicService.summary(startDate,endDate,agent, dataNasabah, dataNasabah2, namaCabang, trxStatus, trxType, trxType2, role, username, pageNumber,rowPerPage, title);
  }

	@GetMapping("/transactionSummaryAgent/{agent}")
  public DurationHoldPaging testDh(
      @PathVariable("agent") String agent,
      @RequestParam String startDate, 
      @RequestParam String endDate,
      @RequestParam int pageNumber,
      @RequestParam int rowPerPage
  ) {
    // "2019-10-31 00:00:00"
    // "2019-10-31 23:59:59"
		return cuicService.summaryByAgent(startDate,endDate,pageNumber,rowPerPage, agent);
	}
	
  @GetMapping("/test")
  public StatistikPanggilan test (){
    return cuicHomeService.getStatistikPanggilan("2020-03-06", "2020-03-06");
  }  

  @GetMapping("/spvPr")
  public PerformanceReview spvPR (
    @RequestParam String startDate, 
    @RequestParam String endDate
  ){
    return cuicHomeService.spvPR(startDate, endDate);
  }  

  @GetMapping("/spvPrAux")
  public HistoryAux spvPRAux (
    @RequestParam String startDate, 
    @RequestParam String endDate
  ){
    return cuicHomeService.spvPRAux(startDate, endDate);
  }  
  
  @GetMapping("/agentPr/{agent}")
  public PerformanceReview agentPR (
    @PathVariable("agent") String agent,
    @RequestParam String startDate, 
    @RequestParam String endDate
  ){
    return cuicHomeService.agentPR(startDate, endDate, agent);
  }  
  
  @GetMapping("/agentPRAux/{agent}")
  public HistoryAux agentPRAux (
    @PathVariable("agent") String agent,
    @RequestParam String startDate, 
    @RequestParam String endDate
  ){
    return cuicHomeService.agentPRAux(startDate, endDate, agent);
  }  
  
  @GetMapping("/getAgetRealTimeStatus")
  public AuxTimeOutput getAgetRealTimeStatus(
    @RequestParam String state
  ){
    return cuicHomeService.getAgetRealTimeStatus(state);
  }

  @GetMapping("/getSpvHome")//spv home
  public StatistikPanggilan getSpvHome(
    @RequestParam String startDate, 
    @RequestParam String endDate
  ){   
    return cuicHomeService.getStatistikPanggilan(startDate, endDate);
  }  
  
  @GetMapping("/getAgentHome/{agent}")//agent home
  public StatistikPanggilanAgent getAgentHome(
    @RequestParam String startDate, 
    @RequestParam String endDate,
    @PathVariable("agent") String agent
  ){   
    return cuicHomeService.getStatistikPanggilanAgent(startDate, endDate, agent);
  }  
  

	@GetMapping("/daftarAgentMonitoring")
	public DaftarAgentPaging dap(
    @RequestParam String startDate,     // "2019-10-31 00:00:00"
    @RequestParam String endDate,     // "2019-10-31 23:59:59"
    @RequestParam String agent,
    @RequestParam int pageNumber,
    @RequestParam int rowPerPage
  ) {
    return cuicHomeService.listDaftarAgent(startDate,endDate,agent,pageNumber,rowPerPage);
  }




	@GetMapping("/abandon")
	public List<AbandonCall> testAbandon() {
		return cuicMonitoringService.getAbandonCall("2019-10-31 00:00:00","2019-10-31 23:59:59");
	}
  
  @GetMapping("/spvHome")
  public List<CallLog> getDataSpv(){
    List<AuxTime> auxCuic = cuicHomeService.getAuxTimes();

    try{

      ObjectMapper objectMapper = new ObjectMapper();
      Resource resource = new ClassPathResource(".\\dummy\\call_log.json");
      CallLog callLog = objectMapper.readValue(resource.getInputStream(), CallLog.class);
    } catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
  }

  @GetMapping("/auxtime")
  public List<AuxTime> testAux(){
    return cuicHomeService.getAuxTimes();
  }
  
  @GetMapping("/callHandle")
  public List<CallHandle> testCh(){
    return cuicService.getCallHandle(Timestamp.valueOf("2019-11-01 00:00:00"), Timestamp.valueOf("2019-11-01 23:59:59"));
  }

  @GetMapping("/routerCallsQnow")
  public List<Queue> testQ(){
    return cuicMonitoringService.getCallsQueue();
  }

  @GetMapping("/redirectCalls")
  public List<RedirectCall> tesrc(){
    return cuicMonitoringService.getRedirectCalls("2019-11-01 00:00:00", "2019-11-01 23:59:59");
  }

}
