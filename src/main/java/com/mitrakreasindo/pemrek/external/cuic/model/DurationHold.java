package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class DurationHold {

	private String loginName;
	private int holdTime;
  private int duration;
  private int totalData;
  private String noRef;
}
