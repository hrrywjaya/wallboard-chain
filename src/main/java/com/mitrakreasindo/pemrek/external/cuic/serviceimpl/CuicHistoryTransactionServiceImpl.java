package com.mitrakreasindo.pemrek.external.cuic.serviceimpl;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
//import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import ch.qos.logback.core.util.Duration;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;

import com.mitrakreasindo.pemrek.external.cuic.model.CallHandle;
import com.mitrakreasindo.pemrek.external.cuic.model.CallHandleRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLog;
import com.mitrakreasindo.pemrek.external.cuic.model.CallLogRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHold;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHoldPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHoldPagingRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHoldRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.model.TransactionSummary;
import com.mitrakreasindo.pemrek.external.cuic.model.TransactionSummaryRowMapper;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicHistoryTransactionService;
import com.mitrakreasindo.pemrek.external.cuic.service.CuicHomeService;

@Service
public class CuicHistoryTransactionServiceImpl implements CuicHistoryTransactionService{

	@Autowired
	@Qualifier("cuicJdbc")
  private JdbcTemplate jdbcCuicTemplate;
  @Autowired
  private JdbcTemplate jdbcChainTemplate;
  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
	private CuicHomeService cuicHomeService;
  
  


	@Override
	public List<DurationHold> getDurationHold(String startPeriode, String endPeriode, int pageNumber, int rowPerPage) {

    String sql = "SELECT \r\n" + 
				"CONCAT((SUBSTRING(TCD.ANI,8,6)),LastName) as Concate,\r\n" + 
				"FirstName = Person.FirstName, \r\n" + 
				"LastName = Person.LastName, \r\n" + 
				"LoginName = Person.LoginName,\r\n" + 
				"SUBSTRING(TCD.ANI,8,6) as NoRef,\r\n" + 
				"Queue = Variable3,\r\n" + 
				"DateTime, DateTime as Date,\r\n" + 
				"Agent.EnterpriseName as Agent,\r\n" + 
				"Skill_Group.EnterpriseName as SkillGroup,\r\n" + 
				"CASE TCD.CallDisposition \r\n" + 
				"WHEN 1 THEN 'Abandoned in Network'\r\n" + 
				"WHEN 2 THEN 'Abandoned in Local Queue'\r\n" + 
				"WHEN 3 THEN 'Abandoned Ring'\r\n" + 
				"WHEN 4 THEN 'Abandoned Delay'\r\n" + 
				"WHEN 5 THEN 'Abandoned Interflow'\r\n" + 
				"WHEN 6 THEN 'Abandoned Agent Terminal'\r\n" + 
				"WHEN 7 THEN 'Short'\r\n" + 
				"WHEN 8 THEN 'Busy'\r\n" + 
				"WHEN 9 THEN 'Forced Busy'\r\n" + 
				"WHEN 10 THEN 'Disconnect / Drop No Answer'\r\n" + 
				"WHEN 11 THEN 'Disconnect / Drop Busy'\r\n" + 
				"WHEN 12 THEN 'Disconnect / Drop Reorder'\r\n" + 
				"WHEN 13 THEN 'Disconnect / Drop Handled Primary Route'\r\n" + 
				"WHEN 14 THEN 'Disconnect / Drop Handled Other'\r\n" + 
				"WHEN 15 THEN 'Redirected'\r\n" + 
				"WHEN 16 THEN 'Cut Through'\r\n" + 
				"WHEN 17 THEN 'Intraflow'\r\n" + 
				"WHEN 18 THEN 'Interflow'\r\n" + 
				"WHEN 19 THEN 'Ring No Answer'\r\n" + 
				"WHEN 20 THEN 'Intercept Reorder'\r\n" + 
				"WHEN 21 THEN 'Intercept Denial'\r\n" + 
				"WHEN 22 THEN 'Time Out'\r\n" + 
				"WHEN 23 THEN 'Voice Energy'\r\n" + 
				"WHEN 24 THEN 'Non-Classified Energy Detected'\r\n" + 
				"WHEN 25 THEN 'No Cut Through'\r\n" + 
				"WHEN 26 THEN 'U-Abort'\r\n" + 
				"WHEN 27 THEN 'Failed Software'\r\n" + 
				"WHEN 28 THEN 'Blind Transfer'\r\n" + 
				"WHEN 29 THEN 'Announced Transfer'\r\n" + 
				"WHEN 30 THEN 'Conferenced'\r\n" + 
				"WHEN 31 THEN 'Duplicate Transfer'\r\n" + 
				"WHEN 32 THEN 'Unmonitored Device'\r\n" + 
				"WHEN 33 THEN 'Answering Machine'\r\n" + 
				"WHEN 34 THEN 'Network Blind Transfer'\r\n" + 
				"WHEN 35 THEN 'Task Abandoned in Router'\r\n" + 
				"WHEN 36 THEN 'Task Abandoned Before Offered'\r\n" + 
				"WHEN 37 THEN 'Task Abandoned While Offered'\r\n" + 
				"WHEN 38 THEN 'Normal Task End'\r\n" + 
				"WHEN 39 THEN 'Cant Obtain Task ID'\r\n" + 
				"WHEN 40 THEN 'Agent Logged Out During Task'\r\n" + 
				"WHEN 41 THEN 'Maximum Task Lifetime Exceeded'\r\n" + 
				"WHEN 42 THEN 'Application Path Went Down'\r\n" + 
				"WHEN 51 THEN 'Task Ended During Application Init'\r\n" + 
				"WHEN 53 THEN 'Partial Call'\r\n" + 
				"ELSE 'Unknown'  \r\n" + 
				"END as CallDisposition,\r\n" + 
				"TCD.DigitsDialed,\r\n" + 
				"TCD.DNIS,\r\n" + 
				"TCD.ANI,\r\n" + 
				"TCD.TalkTime,\r\n" + 
				"TCD.RingTime,\r\n" + 
				"TCD.TimeToAband,\r\n" + 
				"TCD.Duration,\r\n" + 
				"TCD.HoldTime,\r\n" + 
				"TCD.LocalQTime,\r\n" + 
				"TCD.WorkTime,\r\n" + 
				"TCD.WrapupData,\r\n" + 
				"CASE TCD.CallDispositionFlag\r\n" + 
				"WHEN 1 THEN 'Handled'\r\n" + 
				"WHEN 2 THEN 'Abandoned'\r\n" + 
				"WHEN 3 THEN 'Short'\r\n" + 
				"WHEN 4 THEN 'Disconnect'\r\n" + 
				"WHEN 5 THEN 'Redirected'\r\n" + 
				"WHEN 6 THEN 'Requery'\r\n" + 
				"WHEN 7 THEN 'Incomplete'\r\n" + 
				"ELSE 'Unknown'  \r\n" + 
				"END as CallDispositionFlag,\r\n" + 
				"Agent.SkillTargetID\r\n" + 
				"FROM \r\n" + 
				"Termination_Call_Detail TCD JOIN\r\n" + 
				"Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\r\n" + 
				"Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID \r\n" + 
				"LEFT JOIN Person ON Agent.PersonID = Person.PersonID\r\n" + 
				"WHERE (DATEPART(dw, DateTime) in(2,3,4,5,6,7,1) and DateTime between '"+startPeriode.toString()+" 00:00:00' and '"+endPeriode.toString()+" 23:59:59' and convert([char], DateTime, 108) between '00:00:00' and '23:59:59') and\r\n" + 
				"Agent.EnterpriseName IS NOT NULL\r\n" + 
				"ORDER BY \r\n" + 
				"Agent.EnterpriseName ASC,\r\n" + 
        "DateTime ASC\r\n" + 
        // "OFFSET ( " + pageNumber +" - " + 1  +") * " + rowPerPage + " ROWS \r\n" +
        // "FETCH NEXT " + rowPerPage + " ROWS ONLY \r\n" +
				"";
		return jdbcCuicTemplate.query(sql, new DurationHoldRowMapper());
  }
  
	public List<TransactionSummary> getCallLog(String startPeriode, String endPeriode, String agent, String dataNasabah, String dataNasabah2, String cdCabang, String trxStatus, String trxType, String trxType2, String role, String username,String title) {

    String agentParam = " ";
    // " AND cl.FULL_NAME = '"+agent+"' \r\n";
    String cdCabangParam = "";
    String trxTypeParam = "";
    String trxType2Param = "";//produk
    // " AND cl.QR_JENIS_REKENING ='"+trxType+"' \r\n";
    String dataNasabah2Param = " ";
    String trxStatusParam = " ";
    String roleParam ="";
    String titleParam ="";

    if("Karyawan Kontrak".equalsIgnoreCase(title)){
      titleParam = "AND cl.QR_TIPE_KARTU_PASPOR_BCA_TEMP = 'Blue' AND cl.QR_JENIS_REKENING <>  'Tahapan Gold'";
    } 
    //if agent only display the agent's summary
    if("agent".equalsIgnoreCase(role)){
      roleParam = "AND cl.USERNAME = '"+username+"' \r\n";
    }

    if("No Rekening".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " AND cl.QR_NO_REKENING = '"+dataNasabah2+"' \r\n";
    } else if("No Referensi".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " AND cl.NO_REF = '"+dataNasabah2+"' \r\n";
    } else if("Nama Nasabah".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " AND upper(cl.QR_NAMA_NASABAH) like upper('%"+dataNasabah2+"%') \r\n";
    } else if("No Handphone".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " AND cl.QR_NOMOR_HP = '"+dataNasabah2+"' \r\n";
    } else if("No CIS".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " AND cl.QR_CIS_CUSTOMER_NUMBER = '"+dataNasabah2+"' \r\n";
    } else {
      dataNasabah2Param = " ";
    }


    if("".equalsIgnoreCase(dataNasabah2) &&  "No Rekening".equalsIgnoreCase(dataNasabah)){
      dataNasabah2Param = " ";
    }

    if("".equalsIgnoreCase(dataNasabah2)){
      dataNasabah2Param = " ";
    }

    if(dataNasabah2 == null || dataNasabah2.length() == 0){
      dataNasabah2Param = " ";
    }

    // " AND cl.QR_JENIS_REKENING ='"+trxType+"' \r\n";

    if(trxType == null || "".equalsIgnoreCase(trxType) ){
      trxTypeParam = " ";
    }
    if("semua".equalsIgnoreCase(trxType2) || "".equalsIgnoreCase(trxType2) || trxType2 == null){
      trxType2Param = " ";
      // if("Karyawan Kontrak".equalsIgnoreCase(title)){
      //     trxType2="Tahapan";
      //     String trxType3="Tahapan Xpresi";
      //     System.out.println("INI PRODUK karyawan kontrak "+trxType2);
      //     trxType2Param = "AND cl.QR_JENIS_REKENING in ('"+trxType2+"','"+trxType3+"') \r\n";
      //   }
    }else {
    	// if("Karyawan Kontrak".equalsIgnoreCase(title) && "Tahapan Gold".equalsIgnoreCase(trxType2)){
      //       trxType2 = "xxxx";
      //       trxType2Param = "AND cl.QR_JENIS_REKENING = '"+trxType2+"' \r\n";
      //     }else{
            trxType2Param = "AND cl.QR_JENIS_REKENING = '"+trxType2+"' \r\n";
          // }
    }

    if("semua".equalsIgnoreCase(trxStatus)){
      trxStatusParam = " ";
    } else if ("tolak".equalsIgnoreCase(trxStatus)){
      trxStatusParam = "AND cl.TOLAK_MD_ID IS NOT null \r\n";
    } else if ("berhasil".equalsIgnoreCase(trxStatus)){
      trxStatusParam = "AND cl.TRANSAKSI_STATUS = 'SUCCESS' \r\n";
    } else if ("terputus".equalsIgnoreCase(trxStatus) ){
      trxStatusParam = "AND cl.TRANSAKSI_STATUS IS null AND cl.TOLAK_MD_ID IS null and cl.WORK_READY_DATE is not null \r\n";
    } else if ("pending".equalsIgnoreCase(trxStatus)) {
      trxStatusParam = "AND cl.TERPUTUS_MD_ID IS NOT null AND cl.TRANSAKSI_STATUS = 'PENDING' \r\n";
    }

    if("".equalsIgnoreCase(agent) || agent == null){
      agentParam = " ";
    } else {
      agentParam = "AND upper(cl.FULL_NAME) like upper('%"+agent+"%') \r\n";
    }

    if("0000".equalsIgnoreCase(cdCabang) || cdCabang == null){
      cdCabangParam = " ";
    }else{
      cdCabangParam = " AND cl.QR_CABANG = '"+cdCabang+"' \r\n";
    }

    String sql = "SELECT \r\n" +
    "cl.id AS calllogid, \r\n" +
    "cl.QR_CUSTOMER_PEP_STATUS AS pep_status, \r\n" +
    "cl.QR_CUSTOMER_NRT_STATUS AS nrt_status, \r\n" +
    "cl.QR_NAMA_NASABAH AS nama_nasabah, \r\n" +
    "cl.QR_JENIS_NASABAH AS jenis_nasabah, \r\n" +
    "cl.QR_STATUS_IDENTITAS AS status_identitas, \r\n" +
    "cl.QR_CHANNEL AS channel, \r\n" +
    "cl.QR_JENIS_TRANSAKSI AS jenis_transaksi, \r\n" +
    "cl.RAW_KONFIRMASI_DATA AS rawKonfirmasiData, \r\n" +
    "t.RAW_EFORM AS eform, \r\n" +
    "cl.JABBER_EXTENSION AS jabber_ext, \r\n" +
    "cl.QR_NO_REKENING, \r\n"+
    "cl.QR_NOMOR_HP AS no_hp1, \r\n" +//rawkonfirmasidata
    "t.NOMOR_HP2 AS no_hp2, \r\n" +//rawkonfirmasidata  
    "t.NOMOR_HP_MBCA AS no_hp_mbca, \r\n" +
    "t.NOMOR_REKENING_BARU AS no_rekening_baru, \r\n" +
    "t.PEMREK_ACCOUNT_NUMBER AS no_rekening, \r\n" +
    "t.AGAMA, \r\n" +
    "t.NEGARA AS negara_asal, \r\n" +
    "t.NEGARA_LAHIR_FATCA AS negara_lahir, \r\n" +
    "t.PEKERJAAN, \r\n" +
    "t.PEKERJAAN_TIER2, \r\n" +
    "t.PEKERJAAN_TIER3, \r\n" +
    "t.BIDANG_USAHA, \r\n" +
    "t.JABATAN, \r\n" +
    "t.ALAMAT_KANTOR1 AS alamat_usaha, \r\n" +
    "t.TINGGAL_ALAMAT_TERAKHIR_SEJAK, \r\n" +
    "t.NASABAH_BANK_LAIN, \r\n" +
    "t.NAMA_BANK_LAIN, \r\n" +
    "t.HUBUNGAN_USAHA_LUAR_NEGERI, \r\n" +
    "t.NEGARA_BERHUBUNGAN_USAHA1, \r\n" +
    "t.NEGARA_BERHUBUNGAN_USAHA2, \r\n" +
    "t.NEGARA_BERHUBUNGAN_USAHA3, \r\n" +
    "cl.QR_CIS_CUSTOMER_NUMBER, \r\n" +    
    "t.INFORMASI_LAINNYA AS keterangan_edd, \r\n" +
    "cl.FULL_NAME AS agent_name, \r\n" +
    "cl.QR_JENIS_REKENING AS produk, \r\n" +
    "cl.NO_REF AS no_ref, \r\n" +
    "cl.USERNAME AS username, \r\n" +
    "cl.TRANSAKSI_STATUS AS transaksi_status, \r\n" +
    "cl.TERPUTUS_MESSAGE, \r\n" +
    "cl.TERPUTUS_MD_ID, \r\n" +
    "cl.TERPUTUS_MD_TITLE, \r\n" +
    "cl.TOLAK_MESSAGE,  \r\n" +
    "cl.TOLAK_MD_ID,  \r\n" +
    "cl.WORK_READY_DATE,  \r\n" +
    "cl.TOLAK_MD_TITLE,  \r\n" +
    "cl.SERVICE_LEVEL_MESSAGE,  \r\n"  +
    "cl.SERVICE_LEVEL_MD_TITLE,  \r\n"  +
    "cl.APPROVAL_USERNAME, \r\n" +
    "TRANSAKSI_DURATION, \r\n" +
    "ANSWER_DATE, \r\n" +
    "DROPPED_DATE, \r\n " +
    "cl.QR_CABANG, \r\n " +
    // "cl.QR_NAMA_CABANG AS nama_cabang \r\n" +
    "br.branch_name AS nama_cabang \r\n" +
    "FROM CALL_LOG cl \r\n" +
    "LEFT JOIN TRANSAKSI t \r\n" +
    "ON cl.TRANSAKSI_ID = t.ID \r\n" +
    "LEFT JOIN MD_PEMREK_BRANCH br \r\n"+
    "ON cl.QR_CABANG = br.branch_code \r\n"+
    "LEFT JOIN ACCOUNT a \r\n" +
    "ON cl.USERNAME = a.USERNAME \r\n" +
    "WHERE cl.ACTIVE_DATE BETWEEN '"+startPeriode+" 00:00:00' AND '"+endPeriode+" 23:59:00' \r\n"+
    cdCabangParam+
    agentParam+
    trxStatusParam+
    dataNasabah2Param+
    roleParam+
    trxType2Param+
    trxTypeParam+
    titleParam;
    
    return jdbcChainTemplate.query(sql, new TransactionSummaryRowMapper());    
  }


  public DurationHoldPaging summary (String startPeriode, String endPeriode,String agent, String dataNasabah, String dataNasabah2, String cdCabang, String trxStatus, String trxType, String trxType2, String role, String username, int pageNumber, int rowPerPage, String title){
   
    List<TransactionSummary> ts = getCallLog(startPeriode, endPeriode, agent, dataNasabah, dataNasabah2, cdCabang, trxStatus, trxType, trxType2, role,  username, title);
//    List<DurationHold> dh = getDurationHold(startPeriode, endPeriode, pageNumber, rowPerPage);
   
//    Map<String,Integer> rdh = dh.stream().collect(Collectors.toMap(DurationHold::getLoginName, DurationHold::getDuration, Integer:: sum));
    
	/*
	 * List<DurationHold> dhs = rdh.entrySet().stream().map(entry -> { DurationHold
	 * d = new DurationHold(); d.setLoginName(entry.getKey());
	 * d.setDuration(entry.getValue()); return d; }).collect(Collectors.toList());
	 */

    DurationHoldPaging finaldata = new DurationHoldPaging();
    int totalpage=0;
    //map
//    List<TransactionSummary> result = ts.stream().map(r -> {
//      DurationHold holdTime = dhs.stream().filter(d -> d.getLoginName().equals(r.getUsername())).findFirst()
//      .orElse(null);
//			if (holdTime != null) {
//				r.setHoldtime(holdTime.getHoldTime());
//      }     
//			return r; 
//    }).collect(Collectors.toList());
    //paging
    
    List<TransactionSummary> resultpaged = ts.stream()
    .skip(pageNumber * rowPerPage)
    .limit(rowPerPage).collect(Collectors.toList());
    
    finaldata.setSum(resultpaged);
    int totalData = ts.size();
    if (ts != null){
      totalpage = totalData / rowPerPage ; 
    }
    finaldata.setTotalPage(totalpage);
    finaldata.setTotalData(totalData);
    finaldata.setRowPerPage(rowPerPage);
    finaldata.setCurrentPage(pageNumber);
    if((pageNumber+1) == totalpage){
      finaldata.setLast(true);
    } else {
      finaldata.setLast(false);
    }    
    if ((pageNumber+1) == 1){
      finaldata.setFirst(true);
    } else {
      finaldata.setFirst(false);
    }
    return finaldata;
  }


// by agent
	@Override
	public List<DurationHold> getDurationHoldByAgent(String startPeriode, String endPeriode, int pageNumber, int rowPerPage, String agentLoginName) {
		String sql = "SELECT \r\n" + 
				"CONCAT((SUBSTRING(TCD.ANI,8,6)),LastName) as Concate,\r\n" + 
				"FirstName = Person.FirstName, \r\n" + 
				"LastName = Person.LastName, \r\n" + 
				"LoginName = Person.LoginName,\r\n" + 
				"SUBSTRING(TCD.ANI,8,6) as NoRef,\r\n" + 
				"Queue = Variable3,\r\n" + 
				"DateTime, DateTime as Date,\r\n" + 
				"Agent.EnterpriseName as Agent,\r\n" + 
				"Skill_Group.EnterpriseName as SkillGroup,\r\n" + 
				"CASE TCD.CallDisposition \r\n" + 
				"WHEN 1 THEN 'Abandoned in Network'\r\n" + 
				"WHEN 2 THEN 'Abandoned in Local Queue'\r\n" + 
				"WHEN 3 THEN 'Abandoned Ring'\r\n" + 
				"WHEN 4 THEN 'Abandoned Delay'\r\n" + 
				"WHEN 5 THEN 'Abandoned Interflow'\r\n" + 
				"WHEN 6 THEN 'Abandoned Agent Terminal'\r\n" + 
				"WHEN 7 THEN 'Short'\r\n" + 
				"WHEN 8 THEN 'Busy'\r\n" + 
				"WHEN 9 THEN 'Forced Busy'\r\n" + 
				"WHEN 10 THEN 'Disconnect / Drop No Answer'\r\n" + 
				"WHEN 11 THEN 'Disconnect / Drop Busy'\r\n" + 
				"WHEN 12 THEN 'Disconnect / Drop Reorder'\r\n" + 
				"WHEN 13 THEN 'Disconnect / Drop Handled Primary Route'\r\n" + 
				"WHEN 14 THEN 'Disconnect / Drop Handled Other'\r\n" + 
				"WHEN 15 THEN 'Redirected'\r\n" + 
				"WHEN 16 THEN 'Cut Through'\r\n" + 
				"WHEN 17 THEN 'Intraflow'\r\n" + 
				"WHEN 18 THEN 'Interflow'\r\n" + 
				"WHEN 19 THEN 'Ring No Answer'\r\n" + 
				"WHEN 20 THEN 'Intercept Reorder'\r\n" + 
				"WHEN 21 THEN 'Intercept Denial'\r\n" + 
				"WHEN 22 THEN 'Time Out'\r\n" + 
				"WHEN 23 THEN 'Voice Energy'\r\n" + 
				"WHEN 24 THEN 'Non-Classified Energy Detected'\r\n" + 
				"WHEN 25 THEN 'No Cut Through'\r\n" + 
				"WHEN 26 THEN 'U-Abort'\r\n" + 
				"WHEN 27 THEN 'Failed Software'\r\n" + 
				"WHEN 28 THEN 'Blind Transfer'\r\n" + 
				"WHEN 29 THEN 'Announced Transfer'\r\n" + 
				"WHEN 30 THEN 'Conferenced'\r\n" + 
				"WHEN 31 THEN 'Duplicate Transfer'\r\n" + 
				"WHEN 32 THEN 'Unmonitored Device'\r\n" + 
				"WHEN 33 THEN 'Answering Machine'\r\n" + 
				"WHEN 34 THEN 'Network Blind Transfer'\r\n" + 
				"WHEN 35 THEN 'Task Abandoned in Router'\r\n" + 
				"WHEN 36 THEN 'Task Abandoned Before Offered'\r\n" + 
				"WHEN 37 THEN 'Task Abandoned While Offered'\r\n" + 
				"WHEN 38 THEN 'Normal Task End'\r\n" + 
				"WHEN 39 THEN 'Cant Obtain Task ID'\r\n" + 
				"WHEN 40 THEN 'Agent Logged Out During Task'\r\n" + 
				"WHEN 41 THEN 'Maximum Task Lifetime Exceeded'\r\n" + 
				"WHEN 42 THEN 'Application Path Went Down'\r\n" + 
				"WHEN 51 THEN 'Task Ended During Application Init'\r\n" + 
				"WHEN 53 THEN 'Partial Call'\r\n" + 
				"ELSE 'Unknown'  \r\n" + 
				"END as CallDisposition,\r\n" + 
				"TCD.DigitsDialed,\r\n" + 
				"TCD.DNIS,\r\n" + 
				"TCD.ANI,\r\n" + 
				"TCD.TalkTime,\r\n" + 
				"TCD.RingTime,\r\n" + 
				"TCD.TimeToAband,\r\n" + 
				"TCD.Duration,\r\n" + 
				"TCD.HoldTime,\r\n" + 
				"TCD.LocalQTime,\r\n" + 
				"TCD.WorkTime,\r\n" + 
				"TCD.WrapupData,\r\n" + 
				"CASE TCD.CallDispositionFlag\r\n" + 
				"WHEN 1 THEN 'Handled'\r\n" + 
				"WHEN 2 THEN 'Abandoned'\r\n" + 
				"WHEN 3 THEN 'Short'\r\n" + 
				"WHEN 4 THEN 'Disconnect'\r\n" + 
				"WHEN 5 THEN 'Redirected'\r\n" + 
				"WHEN 6 THEN 'Requery'\r\n" + 
				"WHEN 7 THEN 'Incomplete'\r\n" + 
				"ELSE 'Unknown'  \r\n" + 
				"END as CallDispositionFlag,\r\n" + 
				"Agent.SkillTargetID\r\n" + 
				"FROM \r\n" + 
				"Termination_Call_Detail TCD JOIN\r\n" + 
				"Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\r\n" + 
				"Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\r\n" + 
				"LEFT JOIN Person ON Agent.PersonID = Person.PersonID\r\n" + 
				"WHERE (DATEPART(dw, DateTime) in(2,3,4,5,6,7,1) and DateTime between '"+startPeriode+"' and '"+endPeriode+"' and convert([char], DateTime, 108) between '00:00:00' and '23:59:59') and\r\n" + 
				"Agent.EnterpriseName IS NOT NULL and Person.LoginName = '"+agentLoginName+"'\r\n" + 
				"ORDER BY \r\n" + 
				"Agent.EnterpriseName ASC,\r\n" + 
        "DateTime ASC\r\n" + 
        // "OFFSET ( " + pageNumber +" - " + 1  +") * " + rowPerPage + " ROWS \r\n" +
        // "FETCH NEXT " + rowPerPage + " ROWS ONLY \r\n" +
				"";
		return jdbcCuicTemplate.query(sql, new DurationHoldRowMapper());
	}


  public List<TransactionSummary> getCallLogByAgent(String startPeriode, String endPeriode, String agentLoginName) {
    String sql = "SELECT ID, NO_REF, USERNAME, TRANSAKSI_STATUS,\r\n" +
    "TERPUTUS_MD_ID, TOLAK_MD_ID, TRANSAKSI_DURATION, ANSWER_DATE, DROPPED_DATE, TRANSAKSI_STATUS \r\n" +
    "FROM CALL_LOG \r\n" +
    "WHERE CREATED_DATE BETWEEN TO_TIMESTAMP('"+startPeriode+"','YYYY-MM-DD HH24:MI:SS,FF1') AND TO_TIMESTAMP('"+endPeriode+"','YYYY-MM-DD HH24:MI:SS,FF1') \r\n"+
    "AND USERNAME = '"+agentLoginName+"'";
    return jdbcChainTemplate.query(sql, new TransactionSummaryRowMapper());   
  }

  //merge by agent next
  public DurationHoldPaging summaryByAgent (String startPeriode, String endPeriode, int pageNumber, int rowPerPage, String agentLoginName){
    List<TransactionSummary> ts = getCallLogByAgent(startPeriode, endPeriode, agentLoginName);
    List<DurationHold> dh = getDurationHoldByAgent(startPeriode, endPeriode, pageNumber, rowPerPage, agentLoginName);
    
    Map<String,Integer> rdh = dh.stream().collect(Collectors.toMap(DurationHold::getLoginName, DurationHold::getDuration, Integer:: sum));

    List<DurationHold> dhs = rdh.entrySet().stream().map(entry -> {
      DurationHold d = new DurationHold();
      d.setLoginName(entry.getKey());
      d.setDuration(entry.getValue());
      return d;
    }).collect(Collectors.toList());

    DurationHoldPaging finaldata = new DurationHoldPaging();
    int totalpage=0;
    //map
    List<TransactionSummary> result = ts.stream().map(r -> {
      DurationHold holdTime = dhs.stream().filter(d -> d.getLoginName().equals(r.getUsername())).findFirst()
      .orElse(null);
			if (holdTime != null) {
				r.setHoldtime(holdTime.getHoldTime());
      }     
			return r; 
    }).collect(Collectors.toList());
    //paging
    List<TransactionSummary> resultpaged = result.stream()
    .skip(pageNumber * rowPerPage)
    .limit(rowPerPage).collect(Collectors.toList());
    
    finaldata.setSum(resultpaged);
    int totalData = result.size();
    if (dh != null){
      totalpage = totalData / rowPerPage ; 
    }
    finaldata.setTotalPage(totalpage);
    finaldata.setTotalData(totalData);
    finaldata.setRowPerPage(rowPerPage);
    finaldata.setCurrentPage(pageNumber);
    if((pageNumber+1) == totalpage){
      finaldata.setLast(true);
    } else {
      finaldata.setLast(false);
    }    
    if ((pageNumber+1) == 1){
      finaldata.setFirst(true);
    } else {
      finaldata.setFirst(false);
    }
    return finaldata;
  }


  @Override
	public List<CallHandle> getCallHandle(Timestamp startPeriode, Date endPeriode) {
		String sql = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF SET NOCOUNT ON SELECT  Interval = SGI.DateTime,\r\n" +
        "DATE = CONVERT(char(10),SGI.DateTime,101),\r\n" + 
        "FullName = Skill_Group.EnterpriseName,\r\n" +
        "SkillGroupSkillID = Skill_Group.SkillTargetID,\r\n" +
        "CallbackMessages = SUM(ISNULL(SGI.CallbackMessages,0)), \r\n" +
        "CallbackMessagesTime = SUM(ISNULL(SGI.CallbackMessagesTime,0)), \r\n" +
        "AvgHandledCallsTalkTime = AVG(ISNULL(SGI.AvgHandledCallsTalkTime,0)), \r\n" +
        "HoldTime = SUM(ISNULL(SGI.HoldTime,0)), \r\n" +
        "HandledCallsTalkTime = SUM(ISNULL(SGI.HandledCallsTalkTime,0)),\r\n" +
        "InternalCalls = SUM(ISNULL(SGI.InternalCalls,0)), \r\n" +
        "InternalCallsTime = SUM(ISNULL(SGI.InternalCallsTime,0)), \r\n" +
        "CallsHandled = SUM(ISNULL(SGI.CallsHandled,0)), \r\n" +
        "SupervAssistCalls = SUM(ISNULL(SGI.SupervAssistCalls,0)), \r\n" +
        "AvgHandledCallsTime = AVG(ISNULL(SGI.AvgHandledCallsTime,0)), \r\n" +
        "SupervAssistCallsTime = SUM(ISNULL(SGI.SupervAssistCallsTime,0)), \r\n" +
        "HandledCallsTime = SUM(ISNULL(SGI.HandledCallsTime,0)), \r\n" +
        "AgentOutCallsTime = SUM(ISNULL(SGI.AgentOutCallsTime,0)), \r\n" +
        "TalkInTime = SUM(ISNULL(SGI.TalkInTime,0)), \r\n" +
        "LoggedOnTime = SUM(ISNULL(SGI.LoggedOnTime,0)), \r\n" +
        "ExternalOut = SUM(ISNULL(SGI.AgentOutCalls,0)), \r\n" +
        "TalkOutTime = SUM(ISNULL(SGI.TalkOutTime,0)), \r\n" +
        "TalkOtherTime = SUM(ISNULL(SGI.TalkOtherTime,0)), \r\n" +
        "AvailTime = SUM(ISNULL(SGI.AvailTime,0)), \r\n" +
        "NotReadyTime = SUM(ISNULL(SGI.NotReadyTime,0)), \r\n" +
        "TransferInCalls = SUM(ISNULL(SGI.TransferInCalls,0)), \r\n" +
        "TalkTime = SUM(ISNULL(SGI.TalkTime,0)), \r\n" +
        "TransferInCallsTime = SUM(ISNULL(SGI.TransferInCallsTime,0)), \r\n" +
        "WorkReadyTime = SUM(ISNULL(SGI.WorkReadyTime,0)), \r\n" +
        "TransferOutCalls = SUM(ISNULL(SGI.TransferOutCalls,0)), \r\n" +
        "WorkNotReadyTime = SUM(ISNULL(SGI.WorkNotReadyTime,0)), \r\n" +
        "BusyOtherTime = SUM(ISNULL(SGI.BusyOtherTime,0)), \r\n" +
        "CallsAnswered = SUM(ISNULL(SGI.CallsAnswered,0)), \r\n" +
        "ReservedStateTime = SUM(ISNULL(SGI.ReservedStateTime,0)), \r\n" +
        "AnswerWaitTime = SUM(ISNULL(SGI.AnswerWaitTime,0)), \r\n" +
        "AbandonRingCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)), \r\n" +
        "AbandonRingTime = SUM(ISNULL(SGI.AbandonRingTime,0)), \r\n" +
        "AbandonHoldCalls = SUM(ISNULL(SGI.AbandonHoldCalls,0)), \r\n" +
        "AgentOutCallsTalkTime = SUM(ISNULL(SGI.AgentOutCallsTalkTime,0)), \r\n" +
        "AgentOutCallsOnHold = SUM(ISNULL(SGI.AgentOutCallsOnHold,0)), \r\n" +
        "AgentOutCallsOnHoldTime = SUM(ISNULL(SGI.AgentOutCallsOnHoldTime,0)), \r\n" +
        "AgentTerminatedCalls = SUM(ISNULL(SGI.AgentTerminatedCalls,0)), \r\n" +
        "ConsultativeCalls = SUM(ISNULL(SGI.ConsultativeCalls,0)), \r\n" +
        "ConsultativeCallsTime = SUM(ISNULL(SGI.ConsultativeCallsTime,0)), \r\n" +
        "ConferencedInCalls = SUM(ISNULL(SGI.ConferencedInCalls,0)), \r\n" +
        "ConferencedInCallsTime = SUM(ISNULL(SGI.ConferencedInCallsTime,0)), \r\n" +
        "ConferencedOutCalls = SUM(ISNULL(SGI.ConferencedOutCalls,0)), \r\n" +
        "ConferencedOutCallsTime = SUM(ISNULL(SGI.ConferencedOutCallsTime,0)), \r\n" +
        "IncomingCallsOnHoldTime = SUM(ISNULL(SGI.IncomingCallsOnHoldTime,0)), \r\n" +
        "IncomingCallsOnHold = SUM(ISNULL(SGI.IncomingCallsOnHold,0)), \r\n" +
        "InternalCallsOnHoldTime = SUM(ISNULL(SGI.InternalCallsOnHoldTime,0)), \r\n" +
        "InternalCallsOnHold = SUM(ISNULL(SGI.InternalCallsOnHold,0)),\r\n" +
        "InternalCallsRcvdTime = SUM(ISNULL(SGI.InternalCallsRcvdTime,0)), \r\n" +
        "InternalCallsRcvd = SUM(ISNULL(SGI.InternalCallsRcvd,0)), \r\n" +
        "RedirectNoAnsCalls = SUM(ISNULL(SGI.RedirectNoAnsCalls,0)), \r\n" +
        "RedirectNoAnsCallsTime = SUM(ISNULL(SGI.RedirectNoAnsCallsTime,0)), \r\n" +
        "ShortCalls = SUM(ISNULL(SGI.ShortCalls,0)), \r\n" +
        "RouterCallsAbandQ = SUM(ISNULL(SGI.RouterCallsAbandQ,0)), \r\n" +
        "RouterQueueCalls = SUM(ISNULL(SGI.RouterQueueCalls,0)), \r\n" +
        "AutoOutCalls = SUM(ISNULL(SGI.AutoOutCalls,0)), \r\n" +
        "AutoOutCallsTime = SUM(ISNULL(SGI.AutoOutCallsTime,0)), \r\n" +
        "AutoOutCallsTalkTime = SUM(ISNULL(SGI.AutoOutCallsTalkTime,0)), \r\n" +
        "AutoOutCallsOnHold = SUM(ISNULL(SGI.AutoOutCallsOnHold,0)), \r\n" +
        "AutoOutCallsOnHoldTime = SUM(ISNULL(SGI.AutoOutCallsOnHoldTime,0)), \r\n" +
        "PreviewCalls = SUM(ISNULL(SGI.PreviewCalls,0)), \r\n" +
        "PreviewCallsTime = SUM(ISNULL(SGI.PreviewCallsTime,0)), \r\n" +
        "PreviewCallsTalkTime = SUM(ISNULL(SGI.PreviewCallsTalkTime,0)), \r\n" +
        "PreviewCallsOnHold = SUM(ISNULL(SGI.PreviewCallsOnHold,0)), \r\n" +
        "PreviewCallsOnHoldTime = SUM(ISNULL(SGI.PreviewCallsOnHoldTime,0)), \r\n" +
        "ReserveCalls = SUM(ISNULL(SGI.ReserveCalls,0)), \r\n" +
        "ReserveCallsTime = SUM(ISNULL(SGI.ReserveCallsTime,0)), \r\n" +
        "ReserveCallsTalkTime = SUM(ISNULL(SGI.ReserveCallsTalkTime,0)), \r\n" +
        "ReserveCallsOnHold = SUM(ISNULL(SGI.ReserveCallsOnHold,0)), \r\n" +
        "ReserveCallsOnHoldTime = SUM(ISNULL(SGI.ReserveCallsOnHoldTime,0)), \r\n" +
        "TalkAutoOutTime = SUM(ISNULL(SGI.TalkAutoOutTime,0)), \r\n" +
        "TalkPreviewTime = SUM(ISNULL(SGI.TalkPreviewTime,0)), \r\n" +
        "TalkReserveTime = SUM(ISNULL(SGI.TalkReserveTime,0)), \r\n" +
        "BargeInCalls = SUM(ISNULL(SGI.BargeInCalls,0)), \r\n" +
        "InterceptCalls = SUM(ISNULL(SGI.InterceptCalls,0)), \r\n" +
        "MonitorCalls = SUM(ISNULL(SGI.MonitorCalls,0)), \r\n" +
        "WhisperCalls = SUM(ISNULL(SGI.WhisperCalls,0)), \r\n" +
        "EmergencyAssists = SUM(ISNULL(SGI.EmergencyAssists,0)), \r\n" +
        "CallsOffered = SUM(ISNULL(SGI.CallsHandled,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)) + SUM(ISNULL(SGI.AbandonRingCalls,0))+ SUM(ISNULL(SGI.RedirectNoAnsCalls,0)), \r\n" +
        "CallsQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)), \r\n" +
        "InterruptedTime = SUM(ISNULL(SGI.InterruptedTime,0)), \r\n" +
        "TimeZone = SGI.TimeZone, \r\n" +
        "RouterCallsOffered = SUM(ISNULL(SGI.RouterCallsOffered,  0)), \r\n" +  
        "RouterCallsAgentAbandons = SUM(ISNULL(SGI.RouterCallsAbandToAgent,0)), \r\n" +   
        "RouterCallsDequeued = SUM(ISNULL(SGI.RouterCallsDequeued,0)), \r\n" +   
        "RouterError = SUM(ISNULL(SGI.RouterError,0)), \r\n" +
        "DoNotUseSLTop = CASE min(isnull(Skill_Group.ServiceLevelType,0)) \r\n" +
          "WHEN 1 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0  \r\n" +
          "WHEN 2 THEN sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 \r\n" +
          "WHEN 3 THEN (sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 \r\n" + 
          "ELSE 0 END, \r\n" +
        "DoNotUseSLBottom = CASE min(isnull(Skill_Group.ServiceLevelType,0)) \r\n" +
          "WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END \r\n" +
          "WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END \r\n" +
          "WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0))- sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END  \r\n" +
          "ELSE 0 END, \r\n" +
        "ServicelLevel=CASE min(isnull(Skill_Group.ServiceLevelType,0)) \r\n" +
          "WHEN 1 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) <= 0 THEN 0 ELSE	\r\n" +
          "sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 / \r\n" +
                            "(sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0)) - sum(isnull(SGI.ServiceLevelCallsAband,0))) END \r\n" +
          "WHEN 2 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE \r\n" +
          "sum(isnull(SGI.ServiceLevelCalls,0)) * 1.0 / \r\n" +
                            "(sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END \r\n" +
          "WHEN 3 THEN CASE WHEN (sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) <= 0 THEN 0 ELSE \r\n" +
          "(sum(isnull(SGI.ServiceLevelCalls,0)) + sum(isnull(SGI.ServiceLevelCallsAband,0))) * 1.0 / \r\n" +
                            "(sum(isnull(SGI.ServiceLevelCallsOffered,0)) - sum(isnull(SGI.RouterCallsDequeued,0)) - sum(isnull(SGI.RouterCallsAbandDequeued,0))) END \r\n" +
          "ELSE 0 END, \r\n" +
        "ServiceLevelCalls = SUM(ISNULL(SGI.ServiceLevelCalls,0)), \r\n" + 
        "ServiceLevelCallsAband = SUM(ISNULL(SGI.ServiceLevelCallsAband,  0)), \r\n" + 
        "ServiceLevelCallsDequeue = SUM(ISNULL(SGI.ServiceLevelCallsDequeue,  0)), \r\n" +
        "ServiceLevelError =  SUM(ISNULL(SGI.ServiceLevelError,0)), \r\n" +
        "ServiceLevelRONA = SUM(ISNULL(SGI.ServiceLevelRONA,0)), \r\n" +
        "ServiceLevelCallsOffered = SUM(ISNULL(SGI.ServiceLevelCallsOffered,0)), \r\n" +
        "NetConsultativeCalls = sum(isnull(SGI.NetConsultativeCalls,   0)), \r\n" +
        "NetConsultativeCallsTime = SUM(ISNULL(SGI.NetConsultativeCallsTime,0)), \r\n" +
        "NetConferencedOutCalls = sum(isnull( SGI.NetConferencedOutCalls,0)), \r\n" +
        "NetConfOutCallsTime = SUM(ISNULL(SGI.NetConfOutCallsTime,0)), \r\n" +
        "NetTransferredOutCalls = sum(isnull(SGI.NetTransferOutCalls,0)), \r\n" +
        "DbDateTime = SGI.DbDateTime, \r\n" +
                    "ReportingInterval = SUM(ISNULL(SGI.ReportingInterval,0))*60, \r\n" +
        "fte_AgentsLogonTotal = SUM(ISNULL(SGI.LoggedOnTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +   
        "fte_AgentsNotReady = SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +  
        "fte_AgentsNotActive = SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +
        "fte_AgentsActive = SUM(ISNULL(SGI.TalkTime,0)) * 1.0 /(SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +
        "fte_AgentsWrapup = SUM(ISNULL(SGI.WorkReadyTime,0)+ISNULL(SGI.WorkNotReadyTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +   
        "fte_AgentsOther = SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +
        "fte_AgentsHold = SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +
        "fte_AgentsReserved = SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / (SUM(ISNULL(SGI.ReportingInterval,0))*60), \r\n" +
        "ast_PercentNotActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.AvailTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentActiveTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.TalkTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentHoldTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.HoldTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentWrapTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE (SUM(ISNULL(SGI.WorkNotReadyTime + SGI.WorkReadyTime,0))) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentReservedTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.ReservedStateTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentNotReadyTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.NotReadyTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "ast_PercentUtilization =  \r\n" +
        "CASE WHEN (SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) = 0 THEN 0 \r\n" +		
            "ELSE (CASE WHEN SUM(ISNULL(SGI.TalkTime,0)) = 0 THEN 0 ELSE (SUM(ISNULL(SGI.TalkInTime,0)) + \r\n" +
               "SUM(ISNULL(SGI.TalkOutTime,0)) +  \r\n" +
               "SUM(ISNULL(SGI.TalkOtherTime,  0)) + \r\n" +
               "SUM(ISNULL(SGI.WorkReadyTime,0)) +  \r\n" +
               "SUM(ISNULL(SGI.WorkNotReadyTime,0))) * 1.0 / " +
                "(SUM(ISNULL(SGI.LoggedOnTime,0)) - SUM(ISNULL(SGI.NotReadyTime,0))) END) END, \r\n" +
        "asa =  CASE WHEN SUM(ISNULL(SGI.CallsAnswered,0)) = 0 THEN 0  \r\n" +
              "ELSE SUM(ISNULL(SGI.AnswerWaitTime,0)) * 1.0 / SUM(ISNULL(SGI.CallsAnswered,0)) END, \r\n" +
        "CompletedTasks_AHT =  (CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.HandledCallsTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END), \r\n" +
        "CompletedTasks_AvgActiveTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0  \r\n" +
                        "ELSE SUM(ISNULL(SGI.HandledCallsTalkTime,0)) / SUM(ISNULL(SGI.CallsHandled,0)) END, \r\n" +
        "CompletedTasks_AvgWrapTime = CASE WHEN SUM(ISNULL(SGI.CallsHandled,0)) = 0 THEN 0  \r\n" +
                        "ELSE (SUM(ISNULL(SGI.WorkReadyTime,0)) + SUM(ISNULL(SGI.WorkNotReadyTime,0))) / SUM(ISNULL(SGI.CallsHandled,0)) END, \r\n" + 
        "ast_ActiveTime = SUM(ISNULL(SGI.TalkTime,0)), \r\n" +
        "TotalQueued = SUM(ISNULL(SGI.RouterQueueCalls,0)) + SUM(ISNULL(SGI.CallsQueued,0)), \r\n" +
        "ast_PerBusyOtherTime = (CASE WHEN SUM(ISNULL(SGI.LoggedOnTime,0)) = 0 THEN 0 \r\n" +
             "ELSE SUM(ISNULL(SGI.BusyOtherTime,0)) * 1.0 / SUM(ISNULL(SGI.LoggedOnTime,0)) END), \r\n" +
        "Media = Media_Routing_Domain.EnterpriseName, \r\n" +
        "MRDomainID = Media_Routing_Domain.MRDomainID, \r\n" +
        "AbandCalls = SUM(ISNULL(SGI.AbandonRingCalls,0)) + SUM(ISNULL(SGI.RouterCallsAbandQ,0)), \r\n" +
        "RouterMaxCallsQueued = MAX(SGI.RouterMaxCallsQueued), \r\n" +
        "RouterMaxCallWaitTime = MAX(SGI.RouterMaxCallWaitTime) \r\n" +
       "FROM Skill_Group (nolock),  \r\n" +
        "Skill_Group_Interval SGI (nolock), \r\n" +
        "Media_Routing_Domain (nolock) \r\n" +
      "WHERE Skill_Group.SkillTargetID IN (5562) and (DATEPART(dw, SGI.DateTime) in(2,3,4,5,6,7,1) and SGI.DateTime between '"+startPeriode.toString()+"' and '"+endPeriode.toString()+"' and convert([char], SGI.DateTime, 108) between '00:00:00' and '23:59:59') and (Skill_Group.SkillTargetID = SGI.SkillTargetID) \r\n" + 
             "and (Skill_Group.MRDomainID = Media_Routing_Domain.MRDomainID) \r\n" +   
    "GROUP BY Skill_Group.EnterpriseName, \r\n" +
         "Skill_Group.SkillTargetID, \r\n" +
         "Media_Routing_Domain.EnterpriseName, \r\n" +
         "Media_Routing_Domain.MRDomainID, \r\n" +
         "SGI.DateTime, \r\n" +
         "CONVERT(char(10),SGI.DateTime,101), \r\n" +
         "SGI.TimeZone, \r\n" +
         "SGI.DbDateTime \r\n" +
    "ORDER BY Media_Routing_Domain.EnterpriseName, \r\n" +
         "Skill_Group.EnterpriseName, \r\n" +
         "SGI.DateTime";
		return jdbcCuicTemplate.query(sql, new CallHandleRowMapper());
	}



}
