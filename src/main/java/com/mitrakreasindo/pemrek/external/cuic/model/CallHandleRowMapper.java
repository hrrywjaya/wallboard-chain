package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CallHandleRowMapper implements RowMapper<CallHandle>{

	@Override
	public CallHandle mapRow(ResultSet rs, int rowNum) throws SQLException {
		CallHandle ch = new CallHandle();
		ch.setCallsHandled(rs.getInt("CallsHandled"));
		return ch;
	}

}
