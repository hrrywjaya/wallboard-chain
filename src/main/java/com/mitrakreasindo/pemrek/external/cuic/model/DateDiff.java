package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class DateDiff {
  // private String dropped_date;
  // private String answer_date;
  private int diff_hours;
}
