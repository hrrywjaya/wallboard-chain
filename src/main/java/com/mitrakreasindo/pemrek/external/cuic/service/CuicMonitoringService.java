package com.mitrakreasindo.pemrek.external.cuic.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.mitrakreasindo.pemrek.external.cuic.model.AbandonCall;
import com.mitrakreasindo.pemrek.external.cuic.model.Queue;
import com.mitrakreasindo.pemrek.external.cuic.model.RedirectCall;
import com.mitrakreasindo.pemrek.external.cuic.model.StatusAux;

public interface CuicMonitoringService {

	List<AbandonCall> getAbandonCall(String startPeriode, String endPeriode);
  
  List<Queue> getCallsQueue();

  List<RedirectCall> getRedirectCalls(String startPeriode, String endPeriode);

  List<RedirectCall> getRedirectCallsByAgent(String startPeriode, String endPeriode, String agentLoginName);

  List<StatusAux> statusAux(String startPeriode, String endPeriode);

  List<StatusAux> statusAuxAgent(String startPeriode, String endPeriode,String agentLoginName);
  
  List<StatusAux> statusAuxAgentNewQuery(String startPeriode, String endPeriode,String agentLoginName);
}
