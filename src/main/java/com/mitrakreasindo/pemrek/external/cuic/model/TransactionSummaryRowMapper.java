package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TransactionSummaryRowMapper implements RowMapper<TransactionSummary>{

	@Override
	public TransactionSummary mapRow(ResultSet rs, int rowNum) throws SQLException {
    TransactionSummary rc = new TransactionSummary();
    rc.setId(rs.getString("calllogid"));
    rc.setNo_ref(rs.getString("NO_REF"));
    rc.setUsername(rs.getString("USERNAME"));
    rc.setTransaksi_status(rs.getString("TRANSAKSI_STATUS"));
    rc.setTolak_message(rs.getString("TOLAK_MESSAGE"));
    rc.setTolak_md_id(rs.getString("TOLAK_MD_ID"));
    rc.setTolak_md_title(rs.getString("TOLAK_MD_TITLE"));
    rc.setTransaksi_duration(rs.getString("TRANSAKSI_DURATION"));
    rc.setTerputus_message(rs.getString("TERPUTUS_MESSAGE"));
    rc.setTerputus_md_id(rs.getString("TERPUTUS_MD_ID"));
    rc.setTerputus_md_title(rs.getString("TERPUTUS_MD_TITLE"));
    rc.setSl_message(rs.getString("SERVICE_LEVEL_MESSAGE"));
    rc.setSl_md_title(rs.getString("SERVICE_LEVEL_MD_TITLE"));;
    rc.setAnswer_date(rs.getString("ANSWER_DATE"));
    rc.setDropped_date(rs.getString("DROPPED_DATE"));
    rc.setTransaksi_status(rs.getString("TRANSAKSI_STATUS"));
    rc.setNama_nasabah(rs.getString("nama_nasabah"));
    rc.setNo_hp1(rs.getString("no_hp1"));
    rc.setNo_hp2(rs.getString("no_hp2"));
    rc.setNo_hp_mbca(rs.getString("no_hp_mbca"));
    // rc.setNo_rekening_baru(rs.getString("no_rekening_baru"));
    rc.setNo_rekening_existing(rs.getString("QR_NO_REKENING"));//NO REKENING
    rc.setAgent_name(rs.getString("agent_name"));
    rc.setProduk(rs.getString("produk"));
    rc.setAgama(rs.getString("agama"));
    rc.setNegara_asal(rs.getString("negara_asal"));
    rc.setNegara_lahir(rs.getString("negara_lahir"));
    rc.setPekerjaan(rs.getString("PEKERJAAN"));
    rc.setPekerjaan_tier2(rs.getString("PEKERJAAN_TIER2"));
    rc.setPekerjaan_tier3(rs.getString("PEKERJAAN_TIER3"));
    rc.setBidang_usaha(rs.getString("BIDANG_USAHA"));
    rc.setJabatan(rs.getString("JABATAN"));
    rc.setAlamat_usaha(rs.getString("alamat_usaha"));
    rc.setTinggal_alamat_terakhir_sejak(rs.getString("TINGGAL_ALAMAT_TERAKHIR_SEJAK"));
    rc.setNasabah_bank_lain(rs.getString("NASABAH_BANK_LAIN"));
    rc.setNama_bank_lain(rs.getString("NAMA_BANK_LAIN"));
    rc.setHubungan_usaha_luar_negeri(rs.getString("HUBUNGAN_USAHA_LUAR_NEGERI"));
    rc.setNegara_berhubungan_usaha1(rs.getString("NEGARA_BERHUBUNGAN_USAHA1"));
    rc.setNegara_berhubungan_usaha2(rs.getString("NEGARA_BERHUBUNGAN_USAHA2"));
    rc.setNegara_berhubungan_usaha3(rs.getString("NEGARA_BERHUBUNGAN_USAHA3"));
    rc.setKeterangan_edd(rs.getString("keterangan_edd"));
    rc.setCis_customer_number(rs.getString("QR_CIS_CUSTOMER_NUMBER"));
    rc.setNo_cis(rs.getString("QR_CIS_CUSTOMER_NUMBER"));
    rc.setBranch_cd(rs.getString("QR_CABANG"));
    rc.setBranch_name(rs.getString("nama_cabang"));
    rc.setPep_status(rs.getString("pep_status"));
    rc.setNrt_status(rs.getString("nrt_status"));
    rc.setChannel(rs.getString("channel"));
    rc.setRawKonfirmasiData(rs.getString("rawKonfirmasiData"));
    rc.setEform(rs.getString("eform"));
    rc.setJenis_transaksi(rs.getString("jenis_transaksi"));
    rc.setJenis_nasabah(rs.getString("jenis_nasabah"));
    rc.setStatus_identitas(rs.getString("status_identitas"));
    rc.setNo_ext(rs.getString("jabber_ext"));
    rc.setUser_spv(rs.getString("APPROVAL_USERNAME"));
    rc.setWork_ready_date(rs.getString("WORK_READY_DATE"));

		return rc;
	}

}
