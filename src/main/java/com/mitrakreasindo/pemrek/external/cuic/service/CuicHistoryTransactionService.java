package com.mitrakreasindo.pemrek.external.cuic.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.mitrakreasindo.pemrek.external.cuic.model.CallHandle;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHold;
import com.mitrakreasindo.pemrek.external.cuic.model.DurationHoldPaging;
import com.mitrakreasindo.pemrek.external.cuic.model.TransactionSummary;

public interface CuicHistoryTransactionService {
		  
  List<DurationHold> getDurationHold(String startPeriode, String endPeriode, int pageNumber, int rowPerPage);
  
  List<TransactionSummary> getCallLog(String start, String end,String agent, String dataNasabah,String dataNasabah2, String cdCabang, String trxStatus, String trxType,String trxType2,String role, String username,String title);

  DurationHoldPaging summary(String startPeriode, String endPeriode, String agent, String dataNasabah,String dataNasabah2, String cdCabang, String trxStatus, String trxType, String trxType2, String role, String username,  int pageNumber, int rowPerPage,String title); 

  DurationHoldPaging summaryByAgent(String startPeriode, String endPeriode, int pageNumber, int rowPerPage, String agentLoginName); 

	List<DurationHold> getDurationHoldByAgent(String startPeriode, String endPeriode, int pageNumber, int rowPerPage, String agentLoginName);

  List<CallHandle> getCallHandle(Timestamp startPeriode, Date endPeriode);
  

}
