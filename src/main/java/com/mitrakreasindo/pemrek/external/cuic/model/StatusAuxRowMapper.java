package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StatusAuxRowMapper implements RowMapper<StatusAux>{

	@Override
	public StatusAux mapRow(ResultSet rs, int rowNum) throws SQLException {
    StatusAux rc = new StatusAux();
    rc.setLoginName(rs.getString("LoginName"));
    rc.setReasonCode(rs.getString("ReasonCode"));
    rc.setTextReasonCode(rs.getString("textReasonCode"));
    rc.setTotalCodeDuration(rs.getInt("ReasonCodeDuration"));
    rc.setTotalLoginTime(rs.getInt("TotalLoginTime"));
    rc.setTotalNotReadyTime(rs.getInt("TotalNotReadyTime"));
    return rc;
	}

}
