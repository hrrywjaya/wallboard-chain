package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class RedirectCall {
  private int redirectCalls;
  private String loginname;
}
