package com.mitrakreasindo.pemrek.external.cuic.model;

import lombok.Data;

@Data
public class DaftarAgent {

  private String statusAgent;
  private int rona;
  private int duration;
  private int totalLogin;
  private int berhasil;
  private int jumlah;
  private String username;
	
}
