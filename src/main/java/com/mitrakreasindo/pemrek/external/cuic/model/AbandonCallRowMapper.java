package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AbandonCallRowMapper implements RowMapper<AbandonCall>{

	@Override
	public AbandonCall mapRow(ResultSet rs, int rowNum) throws SQLException {
		AbandonCall ab = new AbandonCall();
		ab.setAbandon(rs.getInt("AbandRingCalls"));
		return ab;
	}

}
