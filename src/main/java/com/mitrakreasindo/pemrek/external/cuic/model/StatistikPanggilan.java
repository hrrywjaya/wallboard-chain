package com.mitrakreasindo.pemrek.external.cuic.model;

import java.util.List;

import lombok.Data;

@Data
public class StatistikPanggilan {
  private long transaksiBerhasil;
  private long panggilanTerputus;
  private long panggilanDitolak;
  
  private int averageHandlingTime;
  private int abandonCall;
  private int rona;
  private int queue;
	
}
