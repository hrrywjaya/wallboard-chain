package com.mitrakreasindo.pemrek.external.cuic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CallLogRowMapper implements RowMapper<CallLog>{

	@Override
	public CallLog mapRow(ResultSet rs, int rowNum) throws SQLException {
    CallLog rc = new CallLog();
    rc.setId(rs.getString("ID"));
    rc.setTerputus_md_id(rs.getString("TERPUTUS_MD_ID"));
    rc.setNo_ref(rs.getString("NO_REF"));
    rc.setUsername(rs.getString("USERNAME"));
    rc.setTransaksi_status(rs.getString("TRANSAKSI_STATUS"));
    rc.setTolak_md_id(rs.getString("TOLAK_MD_ID"));
    rc.setTransaksi_duration(rs.getInt("TRANSAKSI_DURATION"));
		return rc;
	}

}
