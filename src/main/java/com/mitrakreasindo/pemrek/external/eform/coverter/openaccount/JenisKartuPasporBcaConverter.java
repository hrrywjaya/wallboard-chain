package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class JenisKartuPasporBcaConverter
{

	public enum Type {
		INSTANT,
		KONVENSIONAL
	}
	
	public static Type convert(String jenisKartuPasporBca) {
		if (jenisKartuPasporBca == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "jenis kartu paspor bca can't null");
		
		switch (jenisKartuPasporBca)
		{
		case "1":
			return Type.INSTANT;
		case "2":
			return Type.KONVENSIONAL;
		default:
			return null;
		}
	}
	

}
