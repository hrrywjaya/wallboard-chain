package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class ReligionParser {

	private Map<String, String> data = new HashMap<String, String>();
    
    public ReligionParser() {
        this.data.put("1", "Islam");
        this.data.put("2", "Katholik");
        this.data.put("3", "Kristen");
        this.data.put("4", "Hindu");
        this.data.put("5", "Budha");
        this.data.put("6", "Lainnya");
    }
   
    public String parse(String religion) {
        String result = "";
        result = this.data.get(religion);
        return result;
    }
	
}
