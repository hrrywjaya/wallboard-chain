package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class PejabatBankConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType not null");
		
		switch (accountType)
		{
		case TAHAPAN:			
			return "00000";
		case TAHAPAN_XPRESI:			
			return "00000";
		case TAHAPAN_GOLD:			
			return "00000";
		default:
			return null;
		}
	}

}
