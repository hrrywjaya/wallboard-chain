package com.mitrakreasindo.pemrek.external.eform.coverter.dukcapil;

public class DukcapilMaritalStatusConverter {

	public static String convert(String maritalStatus) {
		if (maritalStatus != null) {
			if ("belum kawin".equalsIgnoreCase(maritalStatus))
				return "1"; // lajang
			if ("kawin".equalsIgnoreCase(maritalStatus))
				return "2"; // menikah
			if ("cerai hidup".equalsIgnoreCase(maritalStatus))
				return "3";	// janda / duda
			if ("cerai mati".equalsIgnoreCase(maritalStatus))
				return "3"; // janda / duda
		}
		return "";
	}

}
