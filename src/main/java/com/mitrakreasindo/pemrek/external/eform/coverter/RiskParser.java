package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RiskParser {

	private Map<String, String> data = new HashMap<String, String>();

	public RiskParser() {
		this.data.put("013", "RISIKO TINGGI");
		this.data.put("019", "RISIKO TINGGI");
	}

	public List<String> parse(List<String> customerCodes) {
		List<String> parsed = new ArrayList<String>();
		customerCodes.forEach(code -> {
			parsed.add(this.data.get(code));
		});
		return parsed;
	}

}
