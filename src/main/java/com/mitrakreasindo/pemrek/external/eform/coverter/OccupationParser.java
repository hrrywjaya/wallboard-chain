package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class OccupationParser {

	private Map<String, String> data = new HashMap<String, String>();
    
    public OccupationParser() {
        this.data.put("1", "Pelajar / Mahasiswa");
        this.data.put("2", "Ibu Rumah Tangga");
        this.data.put("3", "Karyawan Swasta");
        this.data.put("4", "Pegawai Negeri");
        this.data.put("5", "TNI / POLRI");
        this.data.put("6", "Pejabat Negara/Daerah");
        this.data.put("7", "Pensiunan");
        this.data.put("8", "Pengusaha Pabrikan");
        this.data.put("9", "Pedagang");
        this.data.put("10", "Pengusaha Jasa");
        this.data.put("11", "Dokter");
        this.data.put("12", "Pengacara");
        this.data.put("13", "Akuntan");
        this.data.put("14", "Wartawan");
        this.data.put("15", "Seniman");
        this.data.put("16", "Notaris");
        this.data.put("17", "Profesional Lainnya");
        this.data.put("18", "Lainnya");
    }
   
    public String parse(String occupation) {
        String result = "";
        result = this.data.get(occupation);
        return result;
    }
	
}
