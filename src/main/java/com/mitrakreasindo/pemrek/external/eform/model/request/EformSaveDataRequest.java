package com.mitrakreasindo.pemrek.external.eform.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.eform.model.EformFormData;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class EformSaveDataRequest
{

	private String refNum;
	private String channelId;
	private String transId;
	private String templateVersion;
	private String status;
	private String createdBy;
	private EformFormData formData;
	
}
