package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class IdentityTypeParser {

	private Map<String, String> data = new HashMap<String, String>();
    
    public IdentityTypeParser () {
        this.data.put("1", "KTP");
        this.data.put("2", "SIM");
        this.data.put("3", "Paspor");
        this.data.put("4", "Identitas < 17th");
    }
   
    public String parse(String identityType) {
        String result = "";
        result = this.data.get(identityType);
        return result;
    }
	
}