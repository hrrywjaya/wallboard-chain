package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class OdPlanConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType not null");
		
		switch (accountType)
		{
		case TAHAPAN:			
			return "000";
		case TAHAPAN_XPRESI:			
			return "000";
		case TAHAPAN_GOLD:			
			return "000";
		default:
			return null;
		}
	}

}
