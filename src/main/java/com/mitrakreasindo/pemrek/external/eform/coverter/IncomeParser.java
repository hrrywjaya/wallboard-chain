package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class IncomeParser {

	private Map<String, String> data = new HashMap<String, String>();
    
    public IncomeParser() {
        this.data.put("1", "< 15 juta");
        this.data.put("2", "15 - 25 juta");
        this.data.put("3", "25 - 400 juta");
        this.data.put("4", "> 400 juta");
        this.data.put("5", "0 - 50 juta");
        this.data.put("6", "> 50 - 250 juta");
        this.data.put("7", "> 250 - 500 juta");
        this.data.put("8", "> 500 juta - 1.5M");
        this.data.put("9", "> 15 M");
    }
   
    public String parse(String income){
        String result = "";
        result = this.data.get(income);
        return result;
    }
	
}
