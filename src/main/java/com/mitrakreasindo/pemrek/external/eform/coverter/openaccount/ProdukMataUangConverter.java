package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class ProdukMataUangConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType not null");
		
		switch (accountType)
		{
		case TAHAPAN:			
			return "IDR";
		case TAHAPAN_XPRESI:			
			return "IDR";
		case TAHAPAN_GOLD:			
			return "IDR";
		default:
			return null;
		}
	}

}
