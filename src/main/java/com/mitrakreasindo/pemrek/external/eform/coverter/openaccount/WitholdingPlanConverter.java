package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class WitholdingPlanConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType can't null");
		
		switch (accountType)
		{
		case TAHAPAN:			
			return "020";
		case TAHAPAN_XPRESI:			
			return "020";
		case TAHAPAN_GOLD:			
			return "020";
		default:
			return null;
		}
	}

}
