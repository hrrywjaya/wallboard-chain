package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class UbahServiceChargeCodeConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType, TipeKartuPasporBcaConverter.Type tipeKartuPassporBca) {
		if (accountType == null || tipeKartuPassporBca == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType or tipeKartuPassporBca can't null");
		
		switch (accountType)
		{
		case TAHAPAN:
			return tahapan(tipeKartuPassporBca);
		case TAHAPAN_XPRESI:			
			return tahapanExpresi();
		case TAHAPAN_GOLD:			
			return tahapanGold(tipeKartuPassporBca);
		default:
			return null;
		}
	}

	private static String tahapan(TipeKartuPasporBcaConverter.Type tipeKartuPassporBca) {
		switch (tipeKartuPassporBca)
		{
		case SILVER:
			return "100";
		case GOLD:
			return "102";
		case PLATINUM:
			return "103";
		default:
			return null;
		}
	}
	
	private static String tahapanExpresi() {
		return "108";
	}
	
	private static String tahapanGold(TipeKartuPasporBcaConverter.Type tipeKartuPassporBca) {
		switch (tipeKartuPassporBca)
		{
		case SILVER:
			return "210";
		case GOLD:
			return "212";
		case PLATINUM:
			return "213";
		default:
			return null;
		}
	}
	
}
