package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.Calendar;
import java.util.Date;

public class DateParserService {

	public DateParserService() {
	}

	public Date parse(Object value, String formatAwal, String formatAkhir) { // "5/03/2018"
		formatAkhir = formatAkhir != null ? formatAkhir : "YYYY-MM-DD";
		if ((value instanceof String) && "DDMMYYYY".equalsIgnoreCase(formatAwal)) {
			String str = String.valueOf(value);

			Integer year = Integer.parseInt(str.substring(0, 2));
			Integer month = Integer.parseInt(str.substring(2, 4));
			Integer date = Integer.parseInt(str.substring(4, 8));

			Calendar cal = Calendar.getInstance();
			cal.set(year, month, date);
			return cal.getTime();
		} else if ((value instanceof String) && "".equalsIgnoreCase((String) value)) {
			return null;
		} else {
			return null;
		}
	}

}
