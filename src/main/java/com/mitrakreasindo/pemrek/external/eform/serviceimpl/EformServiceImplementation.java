package com.mitrakreasindo.pemrek.external.eform.serviceimpl;

import java.io.IOException;
import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveRequest;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformUpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformGenerateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformSaveResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformUpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.eform.repository.network.EformRepository;
import com.mitrakreasindo.pemrek.external.eform.service.EformService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class EformServiceImplementation implements EformService
{

	@Autowired
	private EformRepository eformRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${eform.user-id}")
	private String userId;
	@Autowired
	private AccountService userService;
		
	@Override
	public OutputSchema getEform (String refNumber)
	{
		
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/eform/form-transactions/chain/api/references/"+refNumber;		
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		return eformRepository.getEform(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature, 
				refNumber);
	}
	
	@Override
	public EformSaveResponse saveEform (EformSaveRequest eformRequest)
	{
		
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/eform/form-transactions/chain/api/save";		
		String time = OffsetDateTime.now().toString();
				
		try
		{
			String json = mapper.writeValueAsString(eformRequest).replace("null", new String("\"\"")); // replace null to ""
			EformSaveRequest eformRequestFormated = mapper.readValue(json, EformSaveRequest.class);
			
			String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
			return eformRepository.saveEform(
					"014D2CF3CDC3AF6F4F92C09190860E33",  
					userService.getCurrentUser().getUsername(), 
					userId,
					"localhost.com", 
					"Bearer "+token.getAccessToken(), 
					gatewayApiKey, 
					time, 
					signature, 
					eformRequestFormated);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Override
	public EformSaveResponse saveEform (String username, EformSaveRequest eformRequest)
	{
		
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "POST";
		String relativeUrl = "/eform/form-transactions/chain/api/save";		
		String time = OffsetDateTime.now().toString();
				
		try
		{
			String json = mapper.writeValueAsString(eformRequest).replace("null", new String("\"\"")); // replace null to ""
			EformSaveRequest eformRequestFormated = mapper.readValue(json, EformSaveRequest.class);
			
			String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
			return eformRepository.saveEform(
					"014D2CF3CDC3AF6F4F92C09190860E33",  
					username, 
					userId,
					"localhost.com", 
					"Bearer "+token.getAccessToken(), 
					gatewayApiKey, 
					time, 
					signature, 
					eformRequestFormated);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	@Override
	public EformUpdateStatusResponse updateStatus (String referenceNumber, EformUpdateStatusRequest eformUpdateStatusRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/eform/form-transactions/chain/api/references/"+referenceNumber;		
		String time = OffsetDateTime.now().toString();
				
		try
		{
			String json = mapper.writeValueAsString(eformUpdateStatusRequest);
			String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
			return eformRepository.updateStatus(
					"014D2CF3CDC3AF6F4F92C09190860E33", 
					userService.getCurrentUser().getUsername(), 
					userId,
					"localhost.com", 
					"Bearer "+token.getAccessToken(), 
					gatewayApiKey, 
					time, 
					signature, 
					referenceNumber,
					eformUpdateStatusRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		return null;
	}

	@Override
	public EformUpdateStatusResponse updateStatus (String username,String referenceNumber, EformUpdateStatusRequest eformUpdateStatusRequest)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "PUT";
		String relativeUrl = "/eform/form-transactions/chain/api/references/"+referenceNumber;		
		String time = OffsetDateTime.now().toString();
				
		try
		{
			String json = mapper.writeValueAsString(eformUpdateStatusRequest);
			String signature = hmac.sign(httpMethod, relativeUrl, time, json, gatewayApiSecret, token.getAccessToken());
			return eformRepository.updateStatus(
					"014D2CF3CDC3AF6F4F92C09190860E33", 
					username, 
					userId,
					"localhost.com", 
					"Bearer "+token.getAccessToken(), 
					gatewayApiKey, 
					time, 
					signature, 
					referenceNumber,
					eformUpdateStatusRequest);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}		
		return null;
	}

	// @Override
	// public EformGenerateReferenceNumberResponse generateReferenceNumber ()
	// {
	// 	GatewayToken token = gatewayService.getToken();		
	// 	String httpMethod = "GET";
	// 	String channelId = "E";
	// 	String transId = "01";
	// 	String relativeUrl = "/eform/form-transactions/chain/api/reference-number-generator?channel-id="+channelId+"&trans-id="+transId;		
	// 	String time = OffsetDateTime.now().toString();
				
	// 	String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
	// 	return eformRepository.generateReferenceNumber(
	// 			"014D2CF3CDC3AF6F4F92C09190860E33",  
	// 			userService.getCurrentUser().getUsername(), 
	// 			userId,
	// 			"localhost.com", 
	// 			"Bearer "+token.getAccessToken(), 
	// 			gatewayApiKey, 
	// 			time, 
	// 			signature, 
	// 			channelId,
	// 			transId);
	// }

}
