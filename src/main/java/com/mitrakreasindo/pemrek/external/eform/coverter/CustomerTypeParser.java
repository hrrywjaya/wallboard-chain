package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class CustomerTypeParser {
	
	private Map<String, String> data = new HashMap<String, String>();

	public CustomerTypeParser() {
        this.data.put("I", "Individu");
        this.data.put("O", "Organisasi");
    }
   
    public String parse(String customerType) {
        String result = "";
        result = this.data.get(customerType);
        return result;
    }
	
}
