package com.mitrakreasindo.pemrek.external.eform.coverter;

import com.mitrakreasindo.pemrek.external.eform.coverter.openaccount.AccountTypeConverter;

public class UpdateReferenceNumberAccountTypeConverter {

    public static String convert(AccountTypeConverter.Type accountType) {
        switch (accountType) {
            case TAHAPAN :
                return "T";
            case TAHAPAN_GOLD :
                return "G";
            case TAHAPAN_XPRESI :
                return "X";
            default :
                return "";
        }
    }
	
}
