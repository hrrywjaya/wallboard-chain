package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class UserCodeConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType, TipeKartuPasporBcaConverter.Type tipeKartuPassporBca, JenisKartuPasporBcaConverter.Type jenisKartuPassporBca) {
		if (accountType == null || tipeKartuPassporBca == null || jenisKartuPassporBca == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType or tipeKartuPassporBca or jenisKartuPassporBca can't null");
		
		switch (accountType)
		{
		case TAHAPAN:
			return tahapan(tipeKartuPassporBca, jenisKartuPassporBca);
		case TAHAPAN_XPRESI:			
			return tahapanExpresi();
		case TAHAPAN_GOLD:			
			return tahapanGold(tipeKartuPassporBca, jenisKartuPassporBca);
		default:
			return null;
		}
	}

	private static String tahapan(TipeKartuPasporBcaConverter.Type tipeKartuPassporBca, JenisKartuPasporBcaConverter.Type jenisKartuPassporBca) {
		switch (tipeKartuPassporBca)
		{
		case SILVER:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				return "0000P000000";
			else
				return null;
		case GOLD:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				return "0000E000000";
			else
				return null;
		case PLATINUM:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				// return "0000N000000"; // akan di update dari ubah user code
				return "0000P000000";
			else
				return null;
		default:
			return null;
		}
	}
	
	private static String tahapanExpresi() {
		return "0000P000000";
	}
	
	private static String tahapanGold(TipeKartuPasporBcaConverter.Type tipeKartuPassporBca, JenisKartuPasporBcaConverter.Type jenisKartuPassporBca) {
		switch (tipeKartuPassporBca)
		{
		case SILVER:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				return "0000P000000";
			else
				return null;
		case GOLD:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				return "0000E000000";
			else
				return null;
		case PLATINUM:
			if (jenisKartuPassporBca.equals(JenisKartuPasporBcaConverter.Type.INSTANT))
				// return "0000N000000"; // akan di update dari ubah user code
				return "0000P000000";
			else
				return null;
		default:
			return null;
		}
	}
	
}
