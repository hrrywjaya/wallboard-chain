package com.mitrakreasindo.pemrek.external.eform.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveRequest;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformUpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformGenerateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformSaveResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformUpdateStatusResponse;


@FeignClient(name = "eform-repository", url = "${eform.server.url}")
public interface EformRepository
{
		
	/**
	 * get eform by reference number
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param refNumber
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/eform/form-transactions/chain/api/references/{ref-number}")
	OutputSchema getEform(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("ref-number") String refNumber);
	
	/**
	 * save eform
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param eformRequest
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/eform/form-transactions/chain/api/save")
	EformSaveResponse saveEform(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestBody EformSaveRequest eformRequest);
	
	/**
	 * update eform status
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param referenceNumber
	 * @param eformRequest
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/eform/form-transactions/chain/api/references/{reference-number}")
	EformUpdateStatusResponse updateStatus(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@PathVariable("reference-number") String referenceNumber,
			@RequestBody EformUpdateStatusRequest eformRequest);
	
	/**
	 * generate reference number
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param channelId
	 * @param transId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/eform/form-transactions/chain/api/reference-number-generator")
	EformGenerateReferenceNumberResponse generateReferenceNumber(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestParam("channel-id") String channelId,
			@RequestParam("trans-id") String transId);
	
}
