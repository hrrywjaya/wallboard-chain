package com.mitrakreasindo.pemrek.external.eform.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class ErrorSchema
{

	private String errorCode;
	private ErrorMessage errorMessage;
	
}
