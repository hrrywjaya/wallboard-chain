package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class TaxIndicatorParser {

	private Map<String, String> data = new HashMap<>();

	public TaxIndicatorParser() {
		this.data.put("I", "Individu");
		this.data.put("N", "No Number");
		this.data.put("S", "Suami / Istri");
	}

	public String parse(String taxIndicator) {
		String result = "";
		result = this.data.get(taxIndicator);
		return result;
	}

}
