package com.mitrakreasindo.pemrek.external.eform.service;

import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveRequest;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformUpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformGenerateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformSaveResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformUpdateStatusResponse;

public interface EformService
{

	OutputSchema getEform(String refNumber);
	
	EformSaveResponse saveEform(EformSaveRequest eformSaveRequest);

	EformSaveResponse saveEform(String username, EformSaveRequest eformSaveRequest);
	
	EformUpdateStatusResponse updateStatus(String referenceNumber, EformUpdateStatusRequest eformUpdateStatusRequest);

	EformUpdateStatusResponse updateStatus(String username, String referenceNumber, EformUpdateStatusRequest eformUpdateStatusRequest);
	
	// EformGenerateReferenceNumberResponse generateReferenceNumber();
	
}
