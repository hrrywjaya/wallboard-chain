package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class TransIdConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "accountType not null");
		
		switch (accountType)
		{
		case TAHAPAN:			
			return "07";
		case TAHAPAN_XPRESI:			
			return "12";
		case TAHAPAN_GOLD:			
			return "07";
		default:
			return null;
		}
	}

}
