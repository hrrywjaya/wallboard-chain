package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class AccountTypeConverter
{

	public enum Type {
		TAHAPAN,
		TAHAPAN_XPRESI,
		TAHAPAN_GOLD		
	}
	
	public static Type convert(String accountType, String isTahapanGold) {
		if (accountType == null || isTahapanGold == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "account type or istahapan gold can't null");
			
		switch (accountType)
		{
		case "110":
			if ("Y".equalsIgnoreCase(isTahapanGold))
				return Type.TAHAPAN_GOLD;
			else
				return Type.TAHAPAN;
		case "108":
			return Type.TAHAPAN_XPRESI;
		default:
			return null;
		}
	}
	

}
