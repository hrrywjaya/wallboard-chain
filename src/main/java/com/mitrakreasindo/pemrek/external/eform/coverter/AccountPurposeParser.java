package com.mitrakreasindo.pemrek.external.eform.coverter;

import java.util.HashMap;
import java.util.Map;

public class AccountPurposeParser {

	private Map<String, String> data = new HashMap<String, String>();
    
	public AccountPurposeParser() {
        this.data.put("1", "Transaki");
        this.data.put("2", "Menabung");
        this.data.put("3", "Investasi");
    }
   
	public String parse(String accountPusrpose) {
        String purposed = "";
        purposed = this.data.get(accountPusrpose);
        return purposed;
    }
	
}
