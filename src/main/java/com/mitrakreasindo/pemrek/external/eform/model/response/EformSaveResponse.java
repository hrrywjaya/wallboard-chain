package com.mitrakreasindo.pemrek.external.eform.model.response;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class EformSaveResponse
{

	// response berdasarkan api bca
	
	/**
	 * akan menampilkan base_ref_num dan ref_nums jika request body yg diinput lebih dari satu
	 */
	private String baseRefNum;
	private List<String> refNums;
	
	/**
	 * hanya menampilkan ref_num jika requst body yg diinput satu
	 */
	private String refNum;
	
}
