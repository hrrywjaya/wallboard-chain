package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class StatusTahapanGoldConverter
{
	
	public static String convert(AccountTypeConverter.Type accountType) {
		if (accountType == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "account type can't null");
			
		switch (accountType)
		{
		case TAHAPAN:
			return "N";
		case TAHAPAN_XPRESI:
			return "N";
		case TAHAPAN_GOLD:
			return "Y";
		default:
			return null;
		}
	}
	

}
