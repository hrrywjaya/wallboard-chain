package com.mitrakreasindo.pemrek.external.eform.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FormData
{

	@JsonProperty("TIPE_KARTU_PASPOR_BCA")
	private String tipeKartuPasporBca;
	@JsonProperty("NAMA_TEMPAT_BEKERJA")
	private String namaTempatBekerja;
	@JsonProperty("NO_CUSTOMER_REKENING_GABUNGAN")
	private String noCustomerRekeningGabungan;
	@JsonProperty("FASILITAS_YANG_DIINGINKAN_FIN_MBCA")
	private String fasilitasYangDiinginkanFinBca;
	@JsonProperty("SERIAL_NUMBER_TOKEN")
	private String serialNumberToken;
	@JsonProperty("NAMA_JALAN")
	private String namaJalan;
	@JsonProperty("ALAMAT_KANTOR_3")
	private String almatKantor3;
	@JsonProperty("BERLAKU_SAMPAI_DENGAN")
	private String berlakuSampaiDengan;
	@JsonProperty("STATUS_PERKAWINAN")
	private String statusPerkawinan;
	@JsonProperty("PROVINSI_REKENING_KORAN")
	private String provinsiRekeningKoran;
	@JsonProperty("VALIDASI_CATATAN_BANK_KATEGORI_NASABAH")
	private String validasiCatatanBankKategoriNasabah;
	@JsonProperty("STATUS_REKENING")
	private String statusRekening;
	@JsonProperty("TIPE_IDENTITAS_LAINNYA")
	private String tipeIdentitasLainnya;
	@JsonProperty("NO_CUSTOMER")
	private String noCustomer;
	@JsonProperty("NOMOR_TELEPON_RUMAH")
	private String nomorTeleponRumah;
	@JsonProperty("AGAMA")
	private String agama;
	@JsonProperty("ALAMAT_KANTOR_1")
	private String alamatKantor1;
	@JsonProperty("ALAMAT_KANTOR_2")
	private String alamatKantor2;
	@JsonProperty("KD_PENDUDUK")
	private String kdPenduduk;
	@JsonProperty("OD_PLAN")
	private String odPlan;
	@JsonProperty("SERVICE_CHARGE_CODE")
	private String serviceChargeCode;
	@JsonProperty("FORM_PERPAJAKAN_CRS")
	private String formPerpajakanCrs;
	@JsonProperty("TEMPAT_LAHIR")
	private String tempatLahir;
	@JsonProperty("PERNYATAAN_FASILITAS_BCA")
	private String pernyataanFasilitasBca;
	@JsonProperty("PERSETUJUAN_TELEPON")
	private String persetujuanTelepon;
	@JsonProperty("JENIS_REKENING")
	private String jenisRekening;
	@JsonProperty("WITHOLDING_PLAN")
	private String withholdingPlan;
	@JsonProperty("JABATAN")
	private String jabatan;
	@JsonProperty("NOMOR_HP")
	private String nomorHp;
	@JsonProperty("NIK_REKENING_GABUNGAN")
	private String nikRekeningGabungan;
	@JsonProperty("NAMA_GEDUNG")
	private String namaGedung;
	@JsonProperty("NPWP")
	private String npwp;
	@JsonProperty("SUMBER_PENGHASILAN")
	private String sumberPenghasilan;
	@JsonProperty("FASILITAS_YANG_DIINGINKAN_MBCA")
	private String fasilitasYangDiinginkanMbca;
	@JsonProperty("KODE_AREA_TELEPON_KANTOR")
	private String kodeAreaTeleponKantor;
	@JsonProperty("PERSETUJUAN_SMS")
	private String persetujuanSms;
	@JsonProperty("NAMA_GADIS_IBU_KANDUNG")
	private String namaGadisIbuKandung;
	@JsonProperty("NAMA_DICETAK_DI_KARTU")
	private String namaDicetakDikartu;
	@JsonProperty("COST_CENTER")
	private String costCenter;
	@JsonProperty("KABUPATEN_REKENING_KORAN")
	private String kabupatenRekeningKoran;
	@JsonProperty("NAMA_REKENING_GABUNGAN")
	private String namaRekeningGabungan;
	@JsonProperty("INTEREST_PLAN")
	private String interestPlan;
	@JsonProperty("KODE_NEGARA_FAKSIMILIE")
	private String kodeNegaraFaksimilie;
	@JsonProperty("DENGAN_BUKU")
	private String denganBuku;
	@JsonProperty("SUMBER_PENGHASILAN_LAINNYA")
	private String sumberPenghasilanLainnya;
	@JsonProperty("DALAM_HAL_INI_BERTINDAK")
	private String dalamHalIniBertindak;
	@JsonProperty("FASILITAS_YANG_DIINGINKAN_KLIKBCA")
	private String fasilitasYangDiinginkanKlikbca;
	@JsonProperty("NOMOR_NPWP")
	private String nomorNpwp;
	@JsonProperty("KITAS_KITAP")
	private String kitasKitap;
	@JsonProperty("KODE_NEGARA_NOMOR_HP_2")
	private String kodeNegaraNomorHp2;
	@JsonProperty("NOMOR_FAKSIMILIE")
	private String nomorFaksimilie;
	@JsonProperty("KODE_NEGARA_NOMOR_HP")
	private String kodeNegaraNomorHp;
	@JsonProperty("PERNYATAAN_PRODUK")
	private String pernyataanProduk;
	@JsonProperty("NOMOR_REKENING_BARU")
	private String nomorRekneningBaru;
	@JsonProperty("FORM_PERPAJAKAN_FATCA")
	private String formPerpajakanFacta;
	@JsonProperty("BERLAKU_SAMPAI_DENGAN_KITAS_KITAP")
	private String berlakuSampaiDenganKitasKitap;
	@JsonProperty("KECAMATAN")
	private String kecamatan;
	@JsonProperty("PERSETUJUAN_EMAIL")
	private String persetujuanEmail;
	@JsonProperty("FASILITAS_YANG_DIINGINKAN_KEYBCA")
	private String fasilitasYangDiinginkanKeybca;
	@JsonProperty("KELURAHAN_REKENING_KORAN")
	private String kelurahanRekeningKoran;
	@JsonProperty("BAHASA_PETUNJUK_LAYAR_ATM")
	private String bahasaPetunjukLayarAtm;
	@JsonProperty("JENIS_KELAMIN")
	private String jenisKelamin;
	@JsonProperty("NEGARA_LAHIR_FATCA")
	private String negaraLahirFacta;
	@JsonProperty("PERSETUJUAN_DATA_PIHAK_KETIGA")
	private String persetujuanDataPihakKetiga;
	@JsonProperty("EMAIL")
	private String email;
	@JsonProperty("NAMA_GEDUNG_REKENING_KORAN")
	private String namaGedungRekeningKoran;
	@JsonProperty("JENIS_KARTU_PASPOR_BCA")
	private String jenisKartuPasporBca;
	@JsonProperty("TOTAL_PENGHASILAN")
	private String totalPenghasilan;
	@JsonProperty("CABANG")
	private String cabang;
	@JsonProperty("KODE_AREA_TELEPON_RUMAH")
	private String kodeAreaTeleponRumah;
	@JsonProperty("KODE_NEGARA_DATA_REKENING")
	private String kodeNegaraDataRekening;
	@JsonProperty("NOMOR_HP_MBCA")
	private String nomorHpMbca;
	@JsonProperty("KODE_NEGARA_TELEPON_KANTOR")
	private String kodeNegaraTeloponKantor;
	@JsonProperty("KECAMATAN_REKENING_KORAN")
	private String kecamatanRekeningKoran;
	@JsonProperty("PERNYATAAN_PASPOR_BCA")
	private String pernyataaanPasporBca;
	@JsonProperty("NOMOR_REKENING_EXISTING")
	private String nomorRekeningExisting;
	@JsonProperty("KODE_POS_REKENING_KORAN")
	private String kodePosRekeningKoran;
	@JsonProperty("KELURAHAN")
	private String kelurahan;
	@JsonProperty("REKENING_UNTUK")
	private String rekeningUntuk;
	@JsonProperty("WAJIB_PAJAK_NEGARA_LAIN")
	private String wajibPajakNegaraLain;
	@JsonProperty("NAMA_NASABAH")
	private String namaNasabah;
	@JsonProperty("PEKERJAAN")
	private String pekerjaan;
	@JsonProperty("NOMOR_KITAS_KITAP")
	private String nomorKitasKitap;
	@JsonProperty("PERIODE_BIAYA_ADMIN")
	private String periodeBiayaAdmin;
	@JsonProperty("PERNYATAAN_FASILITAS_BCA_DETAIL")
	private String pernyataanFasikitasBcaDetail;
	@JsonProperty("TIN_SSN")
	private String tinSsn;
	@JsonProperty("KOTA_KANTOR")
	private String kotaKantor;
	@JsonProperty("PEJABAT_BANK")
	private String pejabatBank;
	@JsonProperty("KABUPATEN")
	private String kabupaten;
	@JsonProperty("NEGARA")
	private String negara;
	@JsonProperty("TUJUAN_PEMBUKAAN_REKENING")
	private String tujuanPembukaanRekening;
	@JsonProperty("BIAYA_ADMIN")
	private String biayaAdmin;
	@JsonProperty("NEGARA_ALAMAT")
	private String negaraAlamat;
	@JsonProperty("RT_RW")
	private String rtRw;
	@JsonProperty("RT_RW_REKENING_KORAN")
	private String rtRwRekeningKoran;
	@JsonProperty("KODE_PERORANGAN_BISNIS")
	private String kodePeroranganBisnis;
	@JsonProperty("GOLONGAN_PEMILIK")
	private String golonganPemilik;
	@JsonProperty("BIDANG_USAHA")
	private String bidangUsaha;
	@JsonProperty("KODE_POS")
	private String kodePos;
	@JsonProperty("KODE_PENAMBAHAN_PAJAK")
	private String kodePenambahanPajak;
	@JsonProperty("NOMOR_HP_2")
	private String nomorHp2;
	@JsonProperty("KODE_AREA_FAKSIMILIE")
	private String kodeAreaFaksimilie;
	@JsonProperty("PROVINSI")
	private String provinsi;
	@JsonProperty("PERIODE_R_K")
	private String periodeRK;
	@JsonProperty("NEGARA_ALAMAT_REKENING_KORAN")
	private String negaraAlamatRekeningKoran;
	@JsonProperty("NIL")
	private String nil;
	@JsonProperty("USER_CODE")
	private String userCode;
	@JsonProperty("NIK")
	private String nik;
	@JsonProperty("NAMA_JALAN_REKENING_KORAN")
	private String namaJalanRekeningKoran;
	@JsonProperty("PERIODE_BUNGA")
	private String periodeBunga;
	@JsonProperty("NOMOR_TELEPON_KANTOR")
	private String nomorTeleponWajib;
	@JsonProperty("PERNYATAAN_PRODUK_DETAIL")
	private String pernyataanProdukDetail;
	@JsonProperty("TANGGAL_LAHIR")
	private String tanggalLahir;
	@JsonProperty("KODE_POS_KANTOR")
	private String kodePosKantor;
	@JsonProperty("KODE_NEGARA_TELEPON_RUMAH")
	private String kodeNegaraTeleponRumah;
	@JsonProperty("WAJIB_FATCA")
	private String wajibBaca;
	@JsonProperty("PENGIRIMAN_REKENING_KORAN")
	private String pengirimanRekeningKoran;
	@JsonProperty("NASABAH_EXISTING")
	private String nasabahExisting;

	@JsonProperty("PEKERJAAN_TIER_2")
	private String pekerjaanTier2;
	@JsonProperty("PEKERJAAN_TIER_3")
	private String pekerjaanTier3;
	@JsonProperty("STATUS_TAHAPAN_GOLD")
	private String statusTahapanGold;
	@JsonProperty("NOMOR_HP_FINMBCA")
	private String nomorHpFinMbca;

	@JsonProperty("ALASAN_RESIKO_TINGGI")
	private String alasanResikoTinggi;
	@JsonProperty("TINGGAL_ALAMAT_TERAKHIR_SEJAK")
	private String tinggalAlamatTerakhirSejak;
	@JsonProperty("NASABAH_BANK_LAIN")
	private String nasabahBankLain;
	@JsonProperty("NAMA_BANK_LAIN")
	private String namaBankLain;
	@JsonProperty("TANGGAL_BERGABUNG_BANK_LAIN")
	private String tanggalBergabungBankLain;
	@JsonProperty("HUBUNGAN_USAHA_LUAR_NEGERI")
	private String hubunganUsahaLuarNegeri;
	@JsonProperty("NEGARA_BERHUBUNGAN_USAHA_1")
	private String negaraBerhubunganUsaha1;
	@JsonProperty("NEGARA_BERHUBUNGAN_USAHA_2")
	private String negaraBerhubunganUsaha2;
	@JsonProperty("NEGARA_BERHUBUNGAN_USAHA_3")
	private String negaraBerhubunganUsaha3;
	@JsonProperty("SUMBER_KEKAYAAN_WARISAN")
	private String sumberKekayaanWarisan;
	@JsonProperty("SUMBER_KEKAYAAN_TABUNGAN")
	private String sumberKekayaanTabungan;
	@JsonProperty("SUMBER_KEKAYAAN_HASIL_USAHA")
	private String sumberKekayaanHasilUsaha;
	@JsonProperty("SUMBER_KEKAYAAN_HIBAH")
	private String sumberKekayaanHibah;
	@JsonProperty("SUMBER_KEKAYAAN_GAJI")
	private String sumberKekayaanGaji;
	@JsonProperty("SUMBER_KEKAYAAN_LAINNYA")
	private String sumberKekayaanLainnya;
	@JsonProperty("SUMBER_KEKAYAAN_LAINNYA_TEXT")
	private String sumberKekayaanLainnyaText;
	@JsonProperty("INFORMASI_LAINNYA")
	private String informasiLainnya;
}
