package com.mitrakreasindo.pemrek.external.eform.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class EformGenerateReferenceNumberResponse
{

	private String refNum;
	
}
