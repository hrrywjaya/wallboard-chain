package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

public class TipeKartuPasporBcaConverter
{

	public enum Type {
		SILVER,
		GOLD,
		PLATINUM,
		KHUSUS_COUNTER
	}
	
	public static Type convert(String tipeKartuPasporBca) {
		if (tipeKartuPasporBca == null)
			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "tipeKartuPasporBca can't null");
		
		switch (tipeKartuPasporBca)
		{
		case "1":
			return Type.SILVER;
		case "2":
			return Type.GOLD;
		case "3":
			return Type.PLATINUM;
		case "4":
			return Type.KHUSUS_COUNTER;
		default:
			return null;
		}
	}
	

}
