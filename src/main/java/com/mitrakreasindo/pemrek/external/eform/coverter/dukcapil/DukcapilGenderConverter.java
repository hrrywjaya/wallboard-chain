package com.mitrakreasindo.pemrek.external.eform.coverter.dukcapil;

public class DukcapilGenderConverter {

	public static String convert(String gender) {
		if (gender != null) {
			if ("laki-laki".equalsIgnoreCase(gender))
				return "L";
			if ("perempuan".equalsIgnoreCase(gender))
				return "P";
		}
		return "";
	}

}
