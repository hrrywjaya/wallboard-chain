package com.mitrakreasindo.pemrek.external.eform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformSaveRequest;
import com.mitrakreasindo.pemrek.external.eform.model.request.EformUpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformGenerateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformSaveResponse;
import com.mitrakreasindo.pemrek.external.eform.model.response.EformUpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.eform.service.EformService;

@RestController
@RequestMapping("/capi/eform")
public class EformController
{

	@Autowired
	private EformService eformService;
	
	@GetMapping("/{refNumber}")
	public OutputSchema getEform(@PathVariable("refNumber") String refNumber) {
		return eformService.getEform(refNumber);
	}
	
	@PostMapping
	public EformSaveResponse saveEform (@RequestBody EformSaveRequest eformRequest) {
		return eformService.saveEform(eformRequest);
	}
	
	@PutMapping("/{reference-number}/status")
	public EformUpdateStatusResponse updateStatus(
			@PathVariable("reference-number") String referenceNumber, 
			@RequestBody EformUpdateStatusRequest eformUpdateStatusRequest) {
		return eformService.updateStatus(referenceNumber, eformUpdateStatusRequest);
	}
	
	// @GetMapping("/generate/reference-number")
	// public EformGenerateReferenceNumberResponse generateReferenceNumber () {
	// 	return eformService.generateReferenceNumber();
	// }
	
}
