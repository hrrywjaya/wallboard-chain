package com.mitrakreasindo.pemrek.external.eform.coverter.dukcapil;

public class DukcapilReligionConverter {

	public static String convert(String religion) {
		if (religion != null) {
			if ("islam".equalsIgnoreCase(religion))
				return "1";
			if ("katholik".equalsIgnoreCase(religion))
				return "2";
			if ("kristen".equalsIgnoreCase(religion))
				return "3";
			if ("hindu".equalsIgnoreCase(religion))
				return "4";
			if ("budha".equalsIgnoreCase(religion))
				return "5";
			if ("khonghucu".equalsIgnoreCase(religion))
				return "6";
			if ("kepercayaan".equalsIgnoreCase(religion))
				return "6";
		}
		return "";
	}

}
