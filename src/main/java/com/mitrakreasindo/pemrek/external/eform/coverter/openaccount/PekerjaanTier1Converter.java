// package com.mitrakreasindo.pemrek.external.eform.coverter.openaccount;

// import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
// import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;

// public class PekerjaanTier1Converter
// {
	
// 	public static String convert(String pekerjaanTier1) {
// 		if (pekerjaanTier1 == null)
// 			throw new BadRequestException(BaseExceptionCode.COMMON_ERROR_CANONT_EMPTY, "pekerjaan not null");
		
// 		switch (pekerjaanTier1)
// 		{
// 		case "1":			
// 			return "Pelajar/Mahasiswa";
// 		case "2":			
// 			return "Ibu Rumah Tangga";
// 		case "3":			
// 			return "Karyawan Swasta";
// 		case "4":			
// 			return "Pegawai Negeri";
// 		case "5":			
// 			return "TNI/POLRI";
// 		case "6":			
// 			return "Pejabat Negara/Daerah";
// 		case "7":			
// 			return "Pensiunan";
// 		case "8":			
// 			return "Pengusaha Pabrikan";
// 		case "9":			
// 			return "Pedagang";
// 		case "10":			
// 			return "Pengusaha Jasa";
// 		case "11":			
// 			return "Dokter";
// 		case "12":			
// 			return "Pengacara";
// 		case "13":			
// 			return "Akuntan";
// 		case "14":			
// 			return "Wartawan";
// 		case "15":			
// 			return "Seniman";
// 		case "16":			
// 			return "Notaris";
// 		case "17":			
// 			return "Professional Lainnya";
// 		case "18":			
// 			return "Lainnya";
// 		default:
// 			return null;
// 		}
// 	}

// }
