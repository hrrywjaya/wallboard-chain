package com.mitrakreasindo.pemrek.external.eform.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchema
{

	private String refNum;
	private String status;
	private EformFormData formData;
	private String createdDate;
	private String createdTime;
	
}
