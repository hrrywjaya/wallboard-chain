package com.mitrakreasindo.pemrek.external.eform.model.request;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class EformSaveRequest
{

	private List<EformSaveDataRequest> formDataList;
	
}
