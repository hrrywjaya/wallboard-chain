package com.mitrakreasindo.pemrek.external.email.repository.network;

import com.mitrakreasindo.pemrek.external.bcacoid.model.request.MerchantTokenRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.MerchantTokenResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatusUpperCamelCase;
import com.mitrakreasindo.pemrek.model.EmailRequest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "email-repository", url = "${generate.email.url}")
public interface EmailRepository {
    
    
   
    @RequestMapping(value="/generateEmailFromTemplate/initiate", method=RequestMethod.POST)
    OutputStatusUpperCamelCase sendEmail(
      @RequestHeader("ClientID") String clientID,
      @RequestBody EmailRequest request
    );


}