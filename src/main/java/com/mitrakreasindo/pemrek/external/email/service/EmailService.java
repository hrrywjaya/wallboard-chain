package com.mitrakreasindo.pemrek.external.email.service;

import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatusUpperCamelCase;
import com.mitrakreasindo.pemrek.model.EmailRequest;

public interface EmailService {
    
  OutputStatusUpperCamelCase email(EmailRequest email);

  OutputStatusUpperCamelCase sendEmail();

}