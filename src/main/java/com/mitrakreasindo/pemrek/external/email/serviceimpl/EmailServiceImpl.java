package com.mitrakreasindo.pemrek.external.email.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.repository.network.BcaCoidRepository;
import com.mitrakreasindo.pemrek.external.bcacoid.service.BcaCoidService;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatusUpperCamelCase;
import com.mitrakreasindo.pemrek.external.email.repository.network.EmailRepository;
import com.mitrakreasindo.pemrek.external.email.service.EmailService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.Email;
import com.mitrakreasindo.pemrek.model.EmailParam;
import com.mitrakreasindo.pemrek.model.EmailRequest;
import com.mitrakreasindo.pemrek.repository.CallLogRepository;
import com.mitrakreasindo.pemrek.repository.EddApprovalRepository;
import com.mitrakreasindo.pemrek.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

  @Autowired
  private EmailRepository emailRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${din.user-id}")
  private String userId;
  @Value("${email.client-id}")
  private String clientId;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private AccountService accountService;
  @Autowired
  private CallLogRepository callLogRepository;
	
	@Override
    public OutputStatusUpperCamelCase email(EmailRequest email) {

      String json = "";
      try
      {
        json = mapper.writeValueAsString(email);
      } catch (JsonProcessingException e)
      {
        e.printStackTrace();
      }
  
      return emailRepository.sendEmail(clientId, email);
    }

  @Override
    public OutputStatusUpperCamelCase sendEmail() {
      
      String date = LocalDate.now().toString();
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      try {
			Date tanggal = formatter.parse(date);
			date = new SimpleDateFormat("dd MMMM yyyy").format(tanggal).toString();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
     
      // Long jml = callLogRepository.countByEddApprovalIsNotNull();
      List<CallLog> alledd = callLogRepository.findAllEddApproval();
      
      int jml = alledd.size();

      EmailRequest email = new EmailRequest();
      Email em = new Email();

      List<EmailParam> ep = new ArrayList<>();
      EmailParam ep1 = new EmailParam();
      ep1.setKey("param_tgl");
      ep1.setValue(date);
      
      EmailParam ep2 = new EmailParam();
      ep2.setKey("param_jml");
      ep2.setValue(String.valueOf(jml));

      EmailParam ep3 = new EmailParam();
      ep3.setKey("param_url");
      ep3.setValue("http://10.6.82.20:60023/#/");

      ep.add(ep1);
      ep.add(ep2);
      ep.add(ep3);

      em.setEmailTo("reni_septiana@bca.co.id");
      em.setEmailCustomSubject("Notifikasi Approval RBA Nasabah Risiko Tinggi di CHAIN");
      
      email.setEmailID(UUID.randomUUID().toString());
      email.setTemplateCode("NOTIF_CHAIN");
      email.setEmail(em);
      email.setParameter(ep);
    
      String json = "";
      try
      {
        json = mapper.writeValueAsString(email);
      } catch (JsonProcessingException e)
      {
        e.printStackTrace();
      }
  
      return emailRepository.sendEmail(clientId, email);
    }
}