package com.mitrakreasindo.pemrek.external.email.controller;

import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateReferenceNumberRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.request.UpdateStatusRequest;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateReferenceNumberResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.model.response.UpdateStatusResponse;
import com.mitrakreasindo.pemrek.external.bcacoid.service.BcaCoidService;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatusUpperCamelCase;
import com.mitrakreasindo.pemrek.external.email.service.EmailService;
import com.mitrakreasindo.pemrek.model.EmailRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/capi/email")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/email")
    public OutputStatusUpperCamelCase updateAccountStatus(@RequestBody EmailRequest request) {
        return emailService.email(request);
    }

    @PostMapping("/email-test")
    public OutputStatusUpperCamelCase sendEmail() {
        return emailService.sendEmail();
    }

    // @PutMapping("/status")
    // public UpdateStatusResponse updateAccountStatus(@RequestBody UpdateStatusRequest request) {
    //     return bcaCoidService.updateAccountStatus(request);
    // }

    // @PutMapping("/reference")
    // UpdateReferenceNumberResponse updateReferenceNumber(@RequestBody UpdateReferenceNumberRequest request) {
    //     return bcaCoidService.updateReferenceNumber(request);
    // }

}