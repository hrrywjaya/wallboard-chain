package com.mitrakreasindo.pemrek.external.linkcard.model;

import java.util.Dictionary;

import lombok.Data;
@Data
public class ProductDto {
	private String image;
	private String title;
	private Dictionary<String,String> table;
}
