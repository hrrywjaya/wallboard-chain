package com.mitrakreasindo.pemrek.external.linkcard.service.serviceimpl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.linkcard.model.ProductDto;
import com.mitrakreasindo.pemrek.external.linkcard.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Override
	public ProductDto getProductXpresi() throws IOException {
		String Xpresi = "http://10.20.212.217:2001/Individu/Produk/Simpanan/Paspor-BCA-tahapan-xpresi-bcamobile";
		return getCardData(Xpresi);
	}

	@Override
	public ProductDto getProductBlue() throws IOException {
		String Blue = "http://10.20.212.217:2001/Individu/Produk/Simpanan/Paspor-BCA-Blue-bcamobile";
		return getCardData(Blue);
	}

	@Override
	public ProductDto getProductGold() throws IOException {
		String Gold ="http://10.20.212.217:2001/Individu/Produk/Simpanan/Paspor-BCA-gold-bcamobile";
		return getCardData(Gold);
	}

	@Override
	public ProductDto getProductPlatinum() throws IOException {
		String Platinum = "http://10.20.212.217:2001/Individu/Produk/Simpanan/Paspor-BCA-platinum-bcamobile";
		return getCardData(Platinum);
	}
	
	public ProductDto getCardData(String hiddenLink) throws IOException {
		
		ProductDto product = new ProductDto();
		Document doc = Jsoup.connect(hiddenLink).get();
		
		// get content title
		Elements content = doc.getElementsByTag("p");
		product.setTitle(content.get(1).text());
		
		//get image base 64
		Element imageElement = doc.select("img").first();
		String link = imageElement.absUrl("src");
		URL url = new URL(link);
		InputStream in = url.openStream();
		byte[] bytes = IOUtils.toByteArray(in);
	    System.out.println("URL : "+link);
	    // Converting Image byte array into Base64 String
        String imageDataString =  Base64.encodeBase64String(bytes);;
        product.setImage(imageDataString);
	    
		//get data tabel
		Element table = doc.selectFirst("tbody");
		Elements tds = table.getElementsByTag("td");
		Dictionary<String,String> data = new Hashtable<String,String>(); 
		
		for(int i=0;i<tds.size();i=i+2) {
			data.put(tds.get(i).text(), tds.get(i+1).text());
		}
		product.setTable(data);
		return product;
	}

}
