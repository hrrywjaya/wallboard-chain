package com.mitrakreasindo.pemrek.external.linkcard.service;

import java.io.IOException;

import com.mitrakreasindo.pemrek.external.linkcard.model.ProductDto;


public interface ProductService {
	ProductDto getProductXpresi() throws IOException;
	ProductDto getProductBlue() throws IOException;
	ProductDto getProductGold() throws IOException;
	ProductDto getProductPlatinum() throws IOException;
}
