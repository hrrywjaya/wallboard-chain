package com.mitrakreasindo.pemrek.external.linkcard.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.linkcard.model.ProductDto;
import com.mitrakreasindo.pemrek.external.linkcard.service.ProductService;

@RestController
@RequestMapping("/capi/card")
public class LinkCardController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/xpresi")
	public ProductDto getCardXpresi() throws IOException{
		return productService.getProductXpresi();
	}
	
	@GetMapping("/blue")
	public ProductDto getCardBlue() throws IOException{
		return productService.getProductBlue();
	}
	
	@GetMapping("/gold")
	public ProductDto getCardGold() throws IOException{
		return productService.getProductGold();
	}
	
	@GetMapping("/platinum")
	public ProductDto getCardPlatinum() throws IOException{
		return productService.getProductPlatinum();
	}
}
