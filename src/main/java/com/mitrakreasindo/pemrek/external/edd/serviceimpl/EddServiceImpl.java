package com.mitrakreasindo.pemrek.external.edd.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.edd.model.request.EddRequest;
import com.mitrakreasindo.pemrek.external.edd.model.response.Output;
import com.mitrakreasindo.pemrek.external.edd.repository.network.EddRepository;
import com.mitrakreasindo.pemrek.external.edd.service.EddService;

@Service
public class EddServiceImpl implements EddService {

	@Autowired
	private EddRepository eddRepository;
	@Value("${edd.client-id}")
	private String eddClientId;
	@Autowired
	private ObjectMapper mapper;

	@Override
	public Output saveEdd(String cisCustomerNumber, EddRequest edd) {
		try {
			String json = mapper.writeValueAsString(edd).replace("null", new String("\"\"")); // replace null to ""
			EddRequest req = mapper.readValue(json, EddRequest.class);
			return eddRepository.saveEdd(eddClientId, cisCustomerNumber, req);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	// @Override
	// public Output updateEdd(String cisCustomerNumber, EddRequest edd) {
	// 	try {
	// 		String json = mapper.writeValueAsString(edd).replace("null", new String("\"\"")); // replace null to ""
	// 		EddRequest req = mapper.readValue(json, EddRequest.class);
	// 		return eddRepository.updateEdd(eddClientId, cisCustomerNumber, req);
	// 	} catch (JsonProcessingException e) {
	// 		e.printStackTrace();
	// 	}
	// 	return null;
	// }

}
