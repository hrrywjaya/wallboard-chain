package com.mitrakreasindo.pemrek.external.edd.service;

import com.mitrakreasindo.pemrek.external.edd.model.request.EddRequest;
import com.mitrakreasindo.pemrek.external.edd.model.response.Output;

public interface EddService
{

	Output saveEdd(String cisCustomerNumber, EddRequest edd);

	// Output updateEdd(String cisCustomerNumber, EddRequest edd);
	
}
