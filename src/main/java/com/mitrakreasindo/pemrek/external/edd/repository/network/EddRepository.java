package com.mitrakreasindo.pemrek.external.edd.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.edd.model.request.EddRequest;
import com.mitrakreasindo.pemrek.external.edd.model.response.Output;

@FeignClient(name = "edd-repository", url = "${edd.server.url}")
public interface EddRepository
{

	/**
	 * save edd
	 * @param clientId
	 * @param cisCustomerNumber
	 * @param edd
	 * @return
	 */
	@RequestMapping(method=RequestMethod.POST, value="/cis/individu/{cis-customer-number}/edd")
	Output saveEdd(
			@RequestHeader("client-id") String clientId,
			@PathVariable("cis-customer-number") String cisCustomerNumber,
			@RequestBody EddRequest edd);
	
	/**
	 * update edd
	 * @param clientId
	 * @param cisCustomerNumber
	 * @param edd
	 * @return
	 */
	@RequestMapping(method=RequestMethod.PUT, value="/cis/individu/{cis-customer-number}/edd")
	Output updateEdd(
			@RequestHeader("client-id") String clientId,
			@PathVariable("cis-customer-number") String cisCustomerNumber,
			@RequestBody EddRequest edd);

}
