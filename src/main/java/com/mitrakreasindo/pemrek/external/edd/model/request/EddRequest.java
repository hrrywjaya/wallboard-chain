package com.mitrakreasindo.pemrek.external.edd.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class EddRequest
{

	private String userId;
	private String reasonEdd;
	private String currentAddressFromMonthyear;
	private String additionalAccountOrCreditcardInstitutionName;
	private String additionalAccountOrCreditcardFromMonthyear;
	@JsonProperty("foreign_company_country_code_1")
	private String foreignCompanyCountryCode1;
	@JsonProperty("foreign_company_country_code_2")
	private String foreignCompanyCountryCode2;
	@JsonProperty("foreign_company_country_code_3")
	private String foreignCompanyCountryCode3;
	private String wealthFromHeritance;
	private String wealthFromSaving;
	private String wealthFromBusiness;
	private String wealthFromGift;
	private String wealthFromSalary;
	private String wealthFromOthers;
	private String wealthFromOthersDecription;
	private String additionalInformation;
	
}
