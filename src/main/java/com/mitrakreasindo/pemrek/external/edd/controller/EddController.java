package com.mitrakreasindo.pemrek.external.edd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;

import com.mitrakreasindo.pemrek.external.edd.model.request.EddRequest;
import com.mitrakreasindo.pemrek.external.edd.model.response.Output;
import com.mitrakreasindo.pemrek.external.edd.service.EddService;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.model.CallLog;

@RestController
@RequestMapping("/capi/edd")
public class EddController
{

	@Autowired
  private EddService eddService;
  @Autowired
  private CallLogService callLogService;
  
	
	@PostMapping("/{cis-customer-number}")
	public Output saveEdd(
			@PathVariable("cis-customer-number") String cisCustomerNumber, 
			@RequestBody EddRequest edd ) {
		return eddService.saveEdd(cisCustomerNumber, edd);
	}
  
  
	// @PutMapping("/{cis-customer-number}")
	// public Output updateEdd(
	// 		@PathVariable("cis-customer-number") String cisCustomerNumber, 
	// 		@RequestBody EddRequest edd ) {
	// 	return eddService.updateEdd(cisCustomerNumber, edd);
  // }
  @GetMapping("/getEdd")
  public List<CallLog> findAll(){
    return callLogService.findAllEdd();
  }


}
