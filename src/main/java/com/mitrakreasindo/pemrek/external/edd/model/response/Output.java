package com.mitrakreasindo.pemrek.external.edd.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaSnakeCase;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Output
{
	
	private ErrorSchemaSnakeCase errorSchema;
	private OutputSchema outputSchema;
	
}
