package com.mitrakreasindo.pemrek.external.edd.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.KebabCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(KebabCaseStrategy.class)
public class OutputSchema
{

	private String cisCustomerNumber;
	
}
