package com.mitrakreasindo.pemrek.external.customer.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request.AccountCustomerUpdateReqeust;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchemaDetail;

@FeignClient(name = "customer-account-repository", url = "${account-customer.server.url}")
public interface CustomerAccountRepository
{
	
	/**
	 * Update Service Charge & User Code
	 * @param clientId
	 * @param accountNumber
	 * @return
	 */
	@RequestMapping(method=RequestMethod.PUT, value="/deposit-accounts/account-numbers/{account-number}")
	OutputSnakeCase<OutputSchema> accountCustomerUpdate(
			@RequestHeader("client-id") String clientId, 
			@PathVariable("account-number") String accountNumber,
			@RequestBody AccountCustomerUpdateReqeust accountCustomerUpdateReqeust);
	
	/**
	 * Account Detail
	 * @param clientId
	 * @param accountNumber
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/deposit-accounts/account-numbers/{account-number}/details")
	OutputSnakeCase<OutputSchemaDetail> accountCustomerDetail(
			@RequestHeader("client-id") String clientId, 
			@PathVariable("account-number") String accountNumber);
	
}
