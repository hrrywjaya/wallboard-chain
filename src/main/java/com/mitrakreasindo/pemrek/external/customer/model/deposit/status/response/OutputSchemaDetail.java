package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaDetail {
	private String accountNumber;
	private String accountName;
	private String accountType;
	private AccountStatus accountStatus;
	private String amountTransaction;
	private String availableBalance;
	private String passbookIndicator;
	private String passbookBalance;
	private String lastPrintDate;
	private String ledgerBalance;
	private String holdAmount;
	private String openDate;
	private String taxNumber;
	private String branchCode;
	private String branchName;
	private String depositCategory;
	private String userCode;
	private String currencyCode;
	private String overdraftLimit;			
	private String overdraftPlan;
	private String interestPlan;
	private String accountMemo;
	private String accountNextDay;
	private String accountLastDay;
	private String floatAmount;
	private String floatToday;
	private String floatNextDay;
	private String floatLastDay;
	private String cardNumber;
}
