package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class AccountStatus {
	private String openIndicator;
	private String postIndicator;
	private String dormantStatus;
	private String specialStatus;
	private String miscellaneousStatus;
}
