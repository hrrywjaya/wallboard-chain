package com.mitrakreasindo.pemrek.external.customer.model.status;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Pep
{

	private String name;
	private String birthPlace;
	private String birthDate;
	private String nationality;
	private String pepNotes;
	private String otherNotes;
	private String createdDate;
	
}
