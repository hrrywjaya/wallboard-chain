package com.mitrakreasindo.pemrek.external.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request.AccountCustomerUpdateReqeust;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchemaDetail;
import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;
import com.mitrakreasindo.pemrek.external.customer.service.CustomerService;

@RestController
@RequestMapping("/capi/customer")
public class CustomerController
{

	@Autowired
	private CustomerService customerService;

	@GetMapping("/status")
	CustomerStatus getCustomerStatus (@RequestParam String nik,
			@RequestParam String customer_name, @RequestParam String birth_date, @RequestParam(required=false) String country,
			@RequestParam(required=false) String job1, @RequestParam(required=false) String job2, @RequestParam(required=false) String job3,
			@RequestParam(required=false) String phoneNumber, @RequestParam(required=false) String email)
	{
		return customerService.getCustomerStatus(nik, customer_name, birth_date, country, job1, job2, job3, phoneNumber, email);
	}

	@PutMapping("/account-numbers/{account-number}")
	public OutputSnakeCase<OutputSchema> accountCustomerUpdate (
			@PathVariable("account-number") String accountNumber, 
			@RequestBody AccountCustomerUpdateReqeust accountCustomerUpdateReqeust) {
		return customerService.accountCustomerUpdate(accountNumber, accountCustomerUpdateReqeust);
	}
	
	@GetMapping("/account-numbers/{account-number}/detail")
	public OutputSnakeCase<OutputSchemaDetail> accountDetail(
			@PathVariable("account-number") String accountNumber){
		return customerService.accountCustomerDetails(accountNumber);
	}
	
}
