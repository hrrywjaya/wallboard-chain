package com.mitrakreasindo.pemrek.external.customer.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;

@FeignClient(name = "customer-repository", url = "${customer.server.url}")
public interface CustomerRepository
{
	
	/**
	 * get customer status
	 * @param appId
	 * @param userDomain
	 * @param userId
	 * @param Origin
	 * @param authorization
	 * @param xBcaKey
	 * @param xBcaTimestamp
	 * @param xBcaSignature
	 * @param nik
	 * @param customerName
	 * @param birthDate
	 * @param country
	 * @param job1
	 * @param brachCode
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/bds/general/api/customer-status/branches/{branch-code}")
	CustomerStatus getCustomerStatus(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestHeader("nik") String nik,
			@RequestHeader("customer-name") String customerName,
			@RequestHeader("birth-date") String birthDate,
			@RequestHeader("country-code") String country,
			@RequestHeader("job-1") String job1,
			@RequestHeader("job-2") String job2,
			@RequestHeader("job-3") String job3,
			@RequestHeader("phone-number") String phoneNumber,
			@RequestHeader("email") String email,
			@PathVariable("branch-code") String branchCode);

}
