package com.mitrakreasindo.pemrek.external.customer.model.status;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerStatus
{

	private String customerName;
	private String customerPepStatus;
	private String customerDttotStatus;
	private String customerDikStatus;
	private String customerNrtStatus;
	private String customerCode;
	// private Dttot dttot;
	// private Pep pep;
}
