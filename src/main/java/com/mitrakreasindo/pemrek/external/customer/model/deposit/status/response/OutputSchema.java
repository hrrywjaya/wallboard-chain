package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.AccountCustomerUpdateKeyValue;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchema
{

	private String accountNumber;
	private List<AccountCustomerUpdateKeyValue> failedData;
	
}
