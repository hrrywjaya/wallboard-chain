package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaCardDetail {
	private String cardNumber;
	private String languageId;
	private String currentDate;
	private String openDate;
	private String productionDate;
	private String customerName;
	private List<String> customerAddress;
	private String branchCode;
	private String productionCardFlag;
	private String chargeFlag;
	private String outstanding;
	private String sendFlag;
	private String cardCompanyBrand;
	private String accountCounters;
	private List<AccountList> accountList;
}
