package com.mitrakreasindo.pemrek.external.customer.model.deposit.status;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class AccountCustomerUpdateKeyValue
{

	private String key;
	private String value;
	
}
