package com.mitrakreasindo.pemrek.external.customer.serviceimpl;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request.AccountCustomerUpdateReqeust;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.MasterDataBranchResponseDto;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchemaCardDetail;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchemaDetail;
import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;
import com.mitrakreasindo.pemrek.external.customer.repository.network.CustomerAccountRepository;
import com.mitrakreasindo.pemrek.external.customer.repository.network.CustomerRepository;
import com.mitrakreasindo.pemrek.external.customer.service.CustomerService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.repository.MasterDataBranchRepository;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class CustomerServiceImpl implements CustomerService
{

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CustomerAccountRepository customerAccountRepository;
	@Autowired
	private MasterDataBranchRepository masterDataBranchRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${account-customer.client-id}")
	private String accountCustomerClientId;
	@Value("${customer.user-id}")
	private String userId;
	@Value("${card-customer.client-id}")
	private String cardDetailClientId;
	@Value("${card-customer.server.url}")
	private String cardDetailUrl;
	@Autowired
	private AccountService userService;
	
	@Override
	public CustomerStatus getCustomerStatus (String nik, String customerName, String birthDate, String country, String job1, String job2, String job3, String phoneNumber, String email)
	{
		Account account = userService.getCurrentUser();
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/customer-status/branches/"+account.getBranchCode();	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		return customerRepository.getCustomerStatus(
				"014D2CF3CDC3AF6F4F92C09190860E33",  
				userService.getCurrentUser().getUsername(), 
				userId,
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				nik, 
				customerName, 
				birthDate, 
				country, 
				job1,
				job2,
				job3,
				phoneNumber,
				email,
				account.getBranchCode());
	}

	@Override
	public CustomerStatus getCustomerStatus(String username, String branchCode, String nik, String customerName,
			String birthDate, String country, String job1, String job2, String job3, String phoneNumber, String email) {
				GatewayToken token = gatewayService.getToken();		
				String httpMethod = "GET";
				String relativeUrl = "/bds/general/api/customer-status/branches/"+branchCode;	
				String time = OffsetDateTime.now().toString();
				String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
				
				return customerRepository.getCustomerStatus(
						"014D2CF3CDC3AF6F4F92C09190860E33",  
						username, 
						userId,
						"localhost.com", 
						"Bearer "+token.getAccessToken(), 
						gatewayApiKey, 
						time, 
						signature,
						nik, 
						customerName, 
						birthDate, 
						country, 
						job1,
						job2,
						job3,
						phoneNumber,
						email,
						branchCode);
	}
	
	@Override
	public OutputSnakeCase<OutputSchema> accountCustomerUpdate (String accountNumber, AccountCustomerUpdateReqeust accountCustomerUpdateReqeust)
	{
		return customerAccountRepository.accountCustomerUpdate(accountCustomerClientId, accountNumber, accountCustomerUpdateReqeust);
	}

	@Override
	public OutputSnakeCase<OutputSchemaDetail> accountCustomerDetails(String accountNumber) 
	{
		//get branch code
		OutputSnakeCase<OutputSchemaDetail> response = new OutputSnakeCase<OutputSchemaDetail>();
		response = customerAccountRepository.accountCustomerDetail(accountCustomerClientId, accountNumber);
		
		// check branch code
		String branchCode = response.getOutputSchema().getBranchCode();
		if(!branchCode.isEmpty()) {
			if(branchCode.length()<4) {
				int minNumber = 4;
				for(int i=branchCode.length();i<4;i++) {
					branchCode = "0"+branchCode;
				}
			}
			response.getOutputSchema().setBranchCode(branchCode);
		}
		
		//get branch name
		MasterDataBranchResponseDto branchResponse = new MasterDataBranchResponseDto(masterDataBranchRepository.findByBranchCode(response.getOutputSchema().getBranchCode()));
		response.getOutputSchema().setBranchName(branchResponse.getBranchName());
		
		//get card Number
		response.getOutputSchema().setCardNumber(getCardNumber(accountNumber));
		
		return response;
	}
	
	public String getCardNumber(String accountNumber) {
		String cardNumber ="";
		OutputSnakeCase<OutputSchemaCardDetail> out = new OutputSnakeCase<OutputSchemaCardDetail>();
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("client-id", cardDetailClientId);
		HttpEntity<?> Entity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange(cardDetailUrl+"/debit-cards/v2/card-details?card-number=&account-number="+accountNumber, HttpMethod.GET, Entity, String.class );
		try {
			if(response.getStatusCode() == HttpStatus.OK) {
				out = mapper.readValue(response.getBody(), new TypeReference<OutputSnakeCase<OutputSchemaCardDetail>>() {});
				cardNumber = out.getOutputSchema().getCardNumber();
			}
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cardNumber;
	}

}
