package com.mitrakreasindo.pemrek.external.customer.service;

import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request.AccountCustomerUpdateReqeust;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchema;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response.OutputSchemaDetail;
import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;

public interface CustomerService
{

	CustomerStatus getCustomerStatus(String nik, String customerName, String birthDate, String country, String job1, String job2, String job3, String phoneNumber, String email);

	CustomerStatus getCustomerStatus(String username, String branchCode, String nik, String customerName, String birthDate, String country, String job1, String job2, String job3, String phoneNumber, String email);
	
	OutputSnakeCase<OutputSchema> accountCustomerUpdate(String accountNumber, AccountCustomerUpdateReqeust accountCustomerUpdateReqeust);
	
	OutputSnakeCase<OutputSchemaDetail> accountCustomerDetails(String accountNumber);
	
}
