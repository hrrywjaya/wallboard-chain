package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.request;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.customer.model.deposit.status.AccountCustomerUpdateKeyValue;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class AccountCustomerUpdateReqeust
{

	private String userId;
	private List<AccountCustomerUpdateKeyValue> data;
	
}
