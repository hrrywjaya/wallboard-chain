package com.mitrakreasindo.pemrek.external.customer.model.deposit.status.response;

import com.mitrakreasindo.pemrek.model.MasterDataBranch;

import lombok.Data;

@Data
public class MasterDataBranchResponseDto {
	private String branchName;
    private String branchCode;
    private String branchType;
    private String branchInitial;
    private String branchStatus;
    private String mainBranchName;
    private String mainBranchCode;
    private String mainBranchInitial;
    private String address;
    private String city;
    
    public MasterDataBranchResponseDto(MasterDataBranch masterDataBranch) {
    	if(masterDataBranch != null) {
    		this.branchName = masterDataBranch.getBranchName();
        	this.branchCode = masterDataBranch.getBranchCode();
    	}
    }
}
