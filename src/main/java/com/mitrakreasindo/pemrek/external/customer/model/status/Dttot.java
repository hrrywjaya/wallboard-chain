package com.mitrakreasindo.pemrek.external.customer.model.status;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Dttot
{

	private String name;
	private String address;
	private String noId;
	private String source;
	private String birthDate;
	private String createdDate;
	
}
