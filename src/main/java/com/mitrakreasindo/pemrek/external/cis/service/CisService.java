package com.mitrakreasindo.pemrek.external.cis.service;

import com.mitrakreasindo.pemrek.external.cis.model.OutputCis;
import com.mitrakreasindo.pemrek.external.cis.model.OutputCisSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaCisV2;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCis;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateOutputSchema;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

public interface CisService
{

	OutputCis getCis (String cisCustomerNumber);

	OutputCisSearch getCisByNameBirthDateGender (String customerName, String birthDate, String gender);
	
	OutputCisSearch getCisByRekening (String accountNumber);
	
	OutputCisSearch getCisByTaxNumber (String taxNumber);
	
	OutputSnakeCase<RequestUpdateOutputSchema> updateCis(String cisCustomerNumber, RequestUpdateCis cis);
	
	OutputSnakeCase<OutputSchemaCisV2> getCisV2 (String cisCustomerNumber);

}
