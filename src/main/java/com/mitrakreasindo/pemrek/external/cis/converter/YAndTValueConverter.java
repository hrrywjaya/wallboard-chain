package com.mitrakreasindo.pemrek.external.cis.converter;

public class YAndTValueConverter {

    /**
     * input value Y / N
     * convert N to T
     * @param value
     * @return
     */
    public static String convert(String value) {
        if (value.equals("N")) {
            return "T";
        }
        return value;
    }

}