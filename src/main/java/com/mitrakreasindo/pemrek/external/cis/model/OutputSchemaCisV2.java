package com.mitrakreasindo.pemrek.external.cis.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.cisV2.Accounts;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaCisV2 {
	private List<Accounts> accounts;
}
