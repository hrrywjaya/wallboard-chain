package com.mitrakreasindo.pemrek.external.cis.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerMasterData
{
	
	private String religion;
	private String idNumber;
	private String idExpiredDate;
  private OtherIdentity otherIdentity;
  private String birthPlace;
  private String sex;
  private String segmentation;
  private String reference;
  private String corporateGroup;
  private String cisStatus;
  private String branchNo;
  private String branchName;
  private String citizenship;
  private String taxNo;
  private String taxIndicator;
  private String mailCode;
  private String solicit;
  private String provideDataPermission;
  private String smsAdvertisement;
  private String emailAdvertisement;
  private String telephoneAdvertisement;
  private String primaryOfficer;
  private String citizenshipCode;
  private String fatcaFormFlag;
  private String aeoiFormFlag;
  private String fatcaAssessable;
  private String aeoiAssessable;
  private String openDate;
  private String closeDate;
  private String sinceDate;
  private String fatcaTaxNo;
  private List<String> customerCode;
	private String economicSector;
	private String collectibility;
	private String creditCategory;

}
