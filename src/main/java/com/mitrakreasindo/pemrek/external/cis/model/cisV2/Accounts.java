package com.mitrakreasindo.pemrek.external.cis.model.cisV2;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Accounts {
	private String accountNumber;
	private String accountName;
	private generalDescription accountType;
	private String currency;
	private generalDescription openIndicator;
	private generalDescription postIndicator;
	private generalDescription dormantStatus;
	private String rdnFlag;
	private String joinAccountFlag;
	private String tahapanGoldFlag;
	private TahakaPurpose tahakaPurpose;
}
