package com.mitrakreasindo.pemrek.external.cis.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class Phone
{

	private String countryCode;
	private String areaCode;
	private String phoneNumber;
	
}
