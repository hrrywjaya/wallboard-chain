package com.mitrakreasindo.pemrek.external.cis.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerDemographicInformation
{

	private String income;
	private String birthDate;
	private String birthCountryCode;
	private String house_ownership;
	private String maritalStatus;
	private String education;
	private String occupation;
	private String mothersName;
	private String deathDate;
	
}
