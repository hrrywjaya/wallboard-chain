package com.mitrakreasindo.pemrek.external.cis.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.FaxPhone;
import com.mitrakreasindo.pemrek.external.cis.model.Phone;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RequestUpdateCustomerNameAndPhone
{

	 private String title;
	 private String fullName;
	 private String telexNumber;
	 private Phone homePhone;
	 private Phone officePhone;
	 private FaxPhone faxPhone;
	
}
