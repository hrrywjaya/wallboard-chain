package com.mitrakreasindo.pemrek.external.cis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.cis.model.OutputCis;
import com.mitrakreasindo.pemrek.external.cis.model.OutputCisSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaCisV2;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCis;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateOutputSchema;
import com.mitrakreasindo.pemrek.external.cis.service.CisService;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

@RestController
@RequestMapping("/capi/cis")
public class CisController
{

	@Autowired
	private CisService cisService;

	@GetMapping("/{cisCustomerNumber}")
	public OutputCis getCis (@PathVariable("cisCustomerNumber") String cisCustomerNumber)
	{
		return cisService.getCis(cisCustomerNumber);
	}

	@GetMapping("/search")
	public OutputCisSearch getCisByNameBirthDateGender (
			@RequestParam("customer_name") String customerName, @RequestParam("birth_date") String birthDate,
			@RequestParam("gender") String gender)
	{
		return cisService.getCisByNameBirthDateGender(customerName, birthDate, gender);
	}
	
	@GetMapping("/search/rekening/{account-number}")
	public OutputCisSearch getCisByRekening (@PathVariable("account-number") String accountNumber)
	{
		return cisService.getCisByRekening(accountNumber);
	}
	
	@GetMapping("/search/tax")
	public OutputCisSearch getCisByTaxNumber (@RequestParam("tax-number") String taxNumber)
	{
		return cisService.getCisByTaxNumber(taxNumber);
	}

	@PutMapping("/{cis-customer-number}")
	public OutputSnakeCase<RequestUpdateOutputSchema> updateCis (
			@PathVariable("cis-customer-number") String cisCustomerNumber, 
			@RequestBody RequestUpdateCis cis)
	{
		return cisService.updateCis(cisCustomerNumber, cis);
	}
	
	@GetMapping("/v2/{cisCustomerNumber}")
	public OutputSnakeCase<OutputSchemaCisV2> getCisV2 (@PathVariable("cisCustomerNumber") String cisCustomerNumber)
	{
		return cisService.getCisV2(cisCustomerNumber);
	}
	
}
