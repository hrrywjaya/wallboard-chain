package com.mitrakreasindo.pemrek.external.cis.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerNameAndPhone
{

	 private String title;
	 private String fullName;
	 private String firstName;
	 private String middleName;
	 private String lastName;
	 private String telexNumber;
	 private Phone homePhone;
	 private Phone officePhone;
	 private FaxPhone faxPhone;
	
}
