package com.mitrakreasindo.pemrek.external.cis.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class ContactInformation
{

	private String district;
	private String subDistrict;
	private String street;
	private String city;
	private String country;
	private String province;
	private String building;
	private String rt;
	private String rw;
	private String zipCode;
	private List<Handphone> handphone;
	private String emailAddress;
	
}
