package com.mitrakreasindo.pemrek.external.cis.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RequestUpdateCustomerRemark
{

	private String remark;
	private String remarkEffectiveDate;
	private String remarkExpiredDate;
	
}
