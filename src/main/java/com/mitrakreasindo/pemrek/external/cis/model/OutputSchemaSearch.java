package com.mitrakreasindo.pemrek.external.cis.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaSearch
{

	private List<OutputSchemaSearchItem> cisIndividu;
	private List<OutputSchemaSearchItem> cisOrganization;
	private List<CreditCard> creditCard;

}
