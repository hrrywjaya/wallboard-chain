package com.mitrakreasindo.pemrek.external.cis.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerAddress;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerComplementData;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RequestUpdateCis
{

	private String officerUserId;
	private RequestUpdateCustomerMasterData customerMasterData;
	private RequestUpdateCustomerNameAndPhone customerNameAndPhone;
	private CustomerAddress customerAddress;
	private RequestUpdateCustomerDemographicInformation customerDemographicInformation;
	private CustomerComplementData customerComplementData;
	private RequestUpdateCustomerRemark customerRemark;
	
}
