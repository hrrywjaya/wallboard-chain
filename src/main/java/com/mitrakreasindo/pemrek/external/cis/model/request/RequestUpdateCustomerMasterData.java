package com.mitrakreasindo.pemrek.external.cis.model.request;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.mitrakreasindo.pemrek.external.cis.model.OtherIdentity;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RequestUpdateCustomerMasterData
{
	
	private String religion;
	private String idNumber;
	private String idExpiredDate;
  private OtherIdentity otherIdentity;
  private String birthPlace;
  private String sex;
  private String reference;
  private String corporateGroup;
  private String groupCode;
  private String cisStatus;
  private String branchNo;
  private String citizenship;
  private String citizenshipCode;
  private String primaryOfficer;
  private String mailCode;
  private String provideDataPermission;
  private String smsAdvertisement;
  private String emailAdvertisement;
  private String telephoneAdvertisement;
  private String fatcaFormFlag;
  private String aeoiFormFlag;
  private String fatcaAssessable;
  private String aeoiAssessable;
  private String fatcaTaxNo;
  private String taxNo;
  private String taxIndicator;
  private String openDate;
  private String sinceDate;
  private List<String> customerCode;
  
}
