package com.mitrakreasindo.pemrek.external.cis.serviceimpl;

import com.mitrakreasindo.pemrek.external.cis.model.OutputCis;
import com.mitrakreasindo.pemrek.external.cis.model.OutputCisSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaCisV2;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCis;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateOutputSchema;
import com.mitrakreasindo.pemrek.external.cis.repository.network.CisRepository;
import com.mitrakreasindo.pemrek.external.cis.service.CisService;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CisServiceImplementation implements CisService {

	@Autowired
	private CisRepository cisRepository;
	@Value("${cis.client-id}")
	private String cisClientId;

	@Override
	public OutputCis getCis(String cisCustomerNumber) {
		return cisRepository.getCis(cisClientId, cisCustomerNumber, "06");
	}

	@Override
	public OutputCisSearch getCisByNameBirthDateGender(String customerName, String birthDate, String gender) {
		return cisRepository.getCisByNameBirthDateGender(cisClientId, "06", "CustomerName-BirthDate-Gender",
				customerName.toUpperCase(), birthDate, gender.toUpperCase());
	}

	@Override
	public OutputCisSearch getCisByRekening(String accountNumber) {
		return cisRepository.getCisByRekening(cisClientId, "06", "DepositAccountNumber", accountNumber);
	}

	@Override
	public OutputSnakeCase<RequestUpdateOutputSchema> updateCis(String cisCustomerNumber, RequestUpdateCis cis) {
		return cisRepository.updateCis(cisClientId, cisCustomerNumber, cis);
	}

	@Override
	public OutputSnakeCase<OutputSchemaCisV2> getCisV2(String cisCustomerNumber) {
		OutputSnakeCase<OutputSchemaCisV2> response = new OutputSnakeCase<OutputSchemaCisV2>();
		response = cisRepository.getCisV2(cisClientId, cisCustomerNumber);
		for(int i=0;i<response.getOutputSchema().getAccounts().size();i++) {
			if(response.getOutputSchema().getAccounts().get(i).getAccountNumber().length()>10) {
				String noRek = response.getOutputSchema().getAccounts().get(i).getAccountNumber().substring(1);
				response.getOutputSchema().getAccounts().get(i).setAccountNumber(noRek);
			}
		}
		return response;
	}

	@Override
	public OutputCisSearch getCisByTaxNumber(String taxNumber) {
		return cisRepository.getCisByTaxNumber(cisClientId,"01","TaxNumber", taxNumber);
	}
	
}
