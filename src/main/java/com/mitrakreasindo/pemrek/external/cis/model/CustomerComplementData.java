package com.mitrakreasindo.pemrek.external.cis.model;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerComplementData
{

	private String kitas;
	private String kitasExpiredDate;
	private String income;
	private String accountPurpose;
	private String companyName;
	private String position;
	private String businessSector;
	private ContactInformation contactInformation;
	private List<String> companyAddress;

}
