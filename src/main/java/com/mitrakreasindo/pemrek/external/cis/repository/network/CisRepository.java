package com.mitrakreasindo.pemrek.external.cis.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.cis.model.OutputCis;
import com.mitrakreasindo.pemrek.external.cis.model.OutputCisSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaCisV2;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateCis;
import com.mitrakreasindo.pemrek.external.cis.model.request.RequestUpdateOutputSchema;
import com.mitrakreasindo.pemrek.external.common.model.OutputSnakeCase;

@FeignClient(name = "cis-repository", url = "${cis.server.url}")
public interface CisRepository
{

	/**
	 * function to inquiry cis
	 * @param clientId
	 * @param cisCustomerNumber
	 * @param functionCode
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/cis/v2/db2/{cis-customer-number}")
	OutputCis getCis(
			@RequestHeader("client_id") String clientId, 
			@PathVariable("cis-customer-number") String cisCustomerNumber,
			@RequestParam("function-code") String functionCode);
	
	/**
	 * funtion to searching cis
	 * @param clientId
	 * @param cisCustomerNumber
	 * @param functionCode
	 * @param searchBy
	 * @param customerName
	 * @param birthDate
	 * @param gender
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/cis/v2/search/db2")
	OutputCisSearch getCisByNameBirthDateGender(
			@RequestHeader("client_id") String clientId, 
			@RequestParam("function-code") String functionCode,
			@RequestParam("search-by") String searchBy,
			@RequestParam("customer-name") String customerName,
			@RequestParam("birth-date") String birthDate,
			@RequestParam("gender") String gender);
	
	@RequestMapping(method = RequestMethod.GET, value = "/cis/v2/search/db2")
	OutputCisSearch getCisByRekening(
			@RequestHeader("client_id") String clientId, 
			@RequestParam("function-code") String functionCode,
			@RequestParam("search-by") String searchBy,
			@RequestParam("account-number") String accountNumber);
	
	@RequestMapping(method = RequestMethod.GET, value = "/cis/v2/search/db2")
	OutputCisSearch getCisByTaxNumber(
			@RequestHeader("client_id") String clientId, 
			@RequestParam("function-code") String functionCode,
			@RequestParam("search-by") String searchBy,
			@RequestParam("tax-number") String taxNumber);
	
	/**
	 * update cis individu by cis customer number
	 * @param clientId
	 * @param cisCustomerNumber
	 * @param cis
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/cis/v2/individu/{cis-customer-number}")
	OutputSnakeCase<RequestUpdateOutputSchema> updateCis(
			@RequestHeader("client-id") String clientId, 
			@PathVariable("cis-customer-number") String cisCustomerNumber, 
			@RequestBody RequestUpdateCis cis);
	
	/**
	 * function to inquiry cis v2
	 * @param clientId
	 * @param cisCustomerNumber
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value ="cis/v2/mdm/{cis-customer-number}/accounts")
	OutputSnakeCase<OutputSchemaCisV2> getCisV2(
			@RequestHeader("client-id") String clientId,
			@PathVariable("cis-customer-number") String cisCustomerNumber);
	
}
