package com.mitrakreasindo.pemrek.external.cis.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class CustomerAddress
{

	private String district;
	private String subDistrict;
	private String street;
	private String city;
	private String zipCode;
	private String country;
	private String province;
	private String building;
	private String rt;
	private String rw;
	
}
