package com.mitrakreasindo.pemrek.external.cis.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputSchemaSearchItem
{

	private String cisCustomerNumber;
	private String cisCustomerType;
	private String cisAccountType;
	private CustomerMasterData customerMasterData;
	private CustomerNameAndPhone customerNameAndPhone;
	private CustomerAddress customerAddress;
	private CustomerDemographicInformation customerDemographicInformation;
	private CustomerComplementData customerComplementData;
	private CustomerRemark customerRemark;
	private CisLastUpdate cisLastUpdate;

}
