package com.mitrakreasindo.pemrek.external.cis.model.cisV2;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class generalDescription {
	private String code;
	private String description ;
}
