package com.mitrakreasindo.pemrek.external.cis.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RequestUpdateCustomerDemographicInformation
{

	private String income;
	private String birthDate;
	private String birthCountryCode;
	private String house_ownership;
	private String maritalStatus;
	private String education;
	private String occupation;
	private String mothersName;
	
}
