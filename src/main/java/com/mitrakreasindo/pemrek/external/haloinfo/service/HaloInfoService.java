package com.mitrakreasindo.pemrek.external.haloinfo.service;

import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.creditcard.OutputCreditCardMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.other.OutputOtherMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.news.OutputBreakingNews;
import com.mitrakreasindo.pemrek.external.haloinfo.model.product.credit.OutputCreditProduct;

public interface HaloInfoService
{

	OutputBreakingNews getBreakingNews(int offset, int show);
	
	OutputCreditCardMarketing getCreditCardMarketing(int offset, int show);
	
	OutputOtherMarketing getOtherMarketing(int offset, int show);
	
	OutputCreditProduct getCreditProduct(int offset, int show);
	
}
