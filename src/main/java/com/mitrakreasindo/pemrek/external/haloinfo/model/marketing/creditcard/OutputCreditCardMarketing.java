package com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.creditcard;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputCreditCardMarketing
{

	private List<CreditCardMarketing> outputSchema;
	
}