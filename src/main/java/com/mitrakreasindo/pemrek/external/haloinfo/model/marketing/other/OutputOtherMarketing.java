package com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.other;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputOtherMarketing
{

	private List<OtherMarketing> outputSchema;
	
}