package com.mitrakreasindo.pemrek.external.haloinfo.model.news;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputBreakingNews
{

	private List<News> outputSchema;
	
}