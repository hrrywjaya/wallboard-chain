package com.mitrakreasindo.pemrek.external.haloinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.creditcard.OutputCreditCardMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.other.OutputOtherMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.news.OutputBreakingNews;
import com.mitrakreasindo.pemrek.external.haloinfo.model.product.credit.OutputCreditProduct;
import com.mitrakreasindo.pemrek.external.haloinfo.service.HaloInfoService;

@RestController
@RequestMapping("/capi/halo-info")
public class HaloInfoController
{

	@Autowired
	private HaloInfoService haloInfoService;
	
	@RequestMapping("/breaking-news")
	public OutputBreakingNews getBreakingNews(@RequestParam("offset") int offset, @RequestParam("show") int show) {
		return haloInfoService.getBreakingNews(offset, show);
	}
	
	@RequestMapping("/credit-card-marketing-programs")
	public OutputCreditCardMarketing getCreditCardMarketing(@RequestParam("offset") int offset, @RequestParam("show") int show) {
		return haloInfoService.getCreditCardMarketing(offset, show);
	}
	
	@RequestMapping("/other-marketing-programs")
	public OutputOtherMarketing getOtherMarketing(@RequestParam("offset") int offset, @RequestParam("show") int show) {
		return haloInfoService.getOtherMarketing(offset, show);
	}	
	
	@RequestMapping("/credit-products")
	public OutputCreditProduct getCreditProduct(@RequestParam("offset") int offset, @RequestParam("show") int show) {
		return haloInfoService.getCreditProduct(offset, show);
	}	
	
}
