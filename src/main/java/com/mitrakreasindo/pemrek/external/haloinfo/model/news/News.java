package com.mitrakreasindo.pemrek.external.haloinfo.model.news;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class News
{

	private String id;
	private String itemTitle;
	private String itemUrl;
	private String createdDate;
	
}
