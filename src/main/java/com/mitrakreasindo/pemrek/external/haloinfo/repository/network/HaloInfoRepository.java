package com.mitrakreasindo.pemrek.external.haloinfo.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.creditcard.OutputCreditCardMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.other.OutputOtherMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.news.OutputBreakingNews;
import com.mitrakreasindo.pemrek.external.haloinfo.model.product.credit.OutputCreditProduct;

@FeignClient(name = "halo-info-repository", url = "${halo-info.server.url}")
public interface HaloInfoRepository
{

	/**
	 * get breaking news
	 * @param offset
	 * @param show
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/my-bca-portal/halo-info/api/breaking-news")
	OutputBreakingNews getBreakingNews(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestHeader("X-MYBCA-APPLICATION") String mbcaApplication,
			@RequestHeader("X-MYBCA-Authorization") String mbcaAuthorization,
			@RequestParam("offset") int offset,
			@RequestParam("show") int show);
	
	/**
	 * get credit card marketing
	 * @param offset
	 * @param show
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/my-bca-portal/halo-info/api/credit-card-marketing-programs")
	OutputCreditCardMarketing getCreditCardMarketing(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestHeader("X-MYBCA-APPLICATION") String mbcaApplication,
			@RequestHeader("X-MYBCA-Authorization") String mbcaAuthorization,
			@RequestParam("offset") int offset,
			@RequestParam("show") int show);
	
	/**
	 * get other marketing
	 * @param offset
	 * @param show
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/my-bca-portal/halo-info/api/other-marketing-programs")
	OutputOtherMarketing getOtherMarketing(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestHeader("X-MYBCA-APPLICATION") String mbcaApplication,
			@RequestHeader("X-MYBCA-Authorization") String mbcaAuthorization,
			@RequestParam("offset") int offset,
			@RequestParam("show") int show);
	
	/**
	 * get other marketing
	 * @param offset
	 * @param show
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET, value="/my-bca-portal/halo-info/api/credit-products")
	OutputCreditProduct getCreditProduct(
			@RequestHeader("app-id") String appId,
			@RequestHeader("user-domain") String userDomain,
			@RequestHeader("user-id") String userId,
			@RequestHeader("Origin") String Origin,
			@RequestHeader("Authorization") String authorization,
			@RequestHeader("X-BCA-Key") String xBcaKey,
			@RequestHeader("X-BCA-Timestamp") String xBcaTimestamp,
			@RequestHeader("X-BCA-Signature") String xBcaSignature,
			@RequestHeader("X-MYBCA-APPLICATION") String mbcaApplication,
			@RequestHeader("X-MYBCA-Authorization") String mbcaAuthorization,
			@RequestParam("offset") int offset,
			@RequestParam("show") int show);
	
}
