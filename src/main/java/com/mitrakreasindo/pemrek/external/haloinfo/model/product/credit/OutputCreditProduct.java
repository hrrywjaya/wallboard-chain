package com.mitrakreasindo.pemrek.external.haloinfo.model.product.credit;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class OutputCreditProduct
{

	private List<Credit> outputSchema;
	
}