package com.mitrakreasindo.pemrek.external.haloinfo.serviceimpl;

import java.time.OffsetDateTime;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.creditcard.OutputCreditCardMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.marketing.other.OutputOtherMarketing;
import com.mitrakreasindo.pemrek.external.haloinfo.model.news.OutputBreakingNews;
import com.mitrakreasindo.pemrek.external.haloinfo.model.product.credit.OutputCreditProduct;
import com.mitrakreasindo.pemrek.external.haloinfo.repository.network.HaloInfoRepository;
import com.mitrakreasindo.pemrek.external.haloinfo.service.HaloInfoService;
import com.mitrakreasindo.pemrek.service.AccountService;

@Service
public class HaloInfoServiceImpl implements HaloInfoService
{

	@Autowired
	private HaloInfoRepository haloInfoRepository;
	@Autowired
	private GatewayService gatewayService;
	@Autowired
	private BcaSignature hmac;
	@Value("${gateway.api-key}")
	private String gatewayApiKey;
	@Value("${gateway.api-secret}")
	private String gatewayApiSecret;
	@Value("${halo-info.client-id}")
	private String haloInfoClientId;
	@Value("${halo-info.application}")
	private String haloInfoApplication;
	@Value("${halo-info.user-id}")
	private String userId;
	@Autowired
	private AccountService userService;
	
	@Override
	public OutputBreakingNews getBreakingNews (int offset, int show)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/my-bca-portal/halo-info/api/breaking-news?offset="+offset+"&"+"show="+show;	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		String authorization = "Bearer "+Base64.getEncoder().encodeToString(haloInfoClientId.getBytes());
		
		return haloInfoRepository.getBreakingNews(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				haloInfoApplication,
				authorization,
				offset,
				show);
	}
	
	@Override
	public OutputCreditCardMarketing getCreditCardMarketing (int offset, int show)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/my-bca-portal/halo-info/api/credit-card-marketing-programs?offset="+offset+"&"+"show="+show;	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		String authorization = "Bearer "+Base64.getEncoder().encodeToString(haloInfoClientId.getBytes());
		
		return haloInfoRepository.getCreditCardMarketing(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				haloInfoApplication,
				authorization,
				offset,
				show);
	}
	
	@Override
	public OutputOtherMarketing getOtherMarketing (int offset, int show)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/my-bca-portal/halo-info/api/other-marketing-programs?offset="+offset+"&"+"show="+show;	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		String authorization = "Bearer "+Base64.getEncoder().encodeToString(haloInfoClientId.getBytes());
		
		return haloInfoRepository.getOtherMarketing(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				haloInfoApplication,
				authorization,
				offset,
				show);
	}
	
	@Override
	public OutputCreditProduct getCreditProduct (int offset, int show)
	{
		GatewayToken token = gatewayService.getToken();		
		String httpMethod = "GET";
		String relativeUrl = "/my-bca-portal/halo-info/api/credit-products?offset="+offset+"&"+"show="+show;	
		String time = OffsetDateTime.now().toString();
		String signature = hmac.sign(httpMethod, relativeUrl, time, null, gatewayApiSecret, token.getAccessToken());
		
		String authorization = "Bearer "+Base64.getEncoder().encodeToString(haloInfoClientId.getBytes());
		
		return haloInfoRepository.getCreditProduct(
				"014D2CF3CDC3AF6F4F92C09190860E33", 
				userService.getCurrentUser().getUsername(), 
				userId, 
				"localhost.com", 
				"Bearer "+token.getAccessToken(), 
				gatewayApiKey, 
				time, 
				signature,
				haloInfoApplication,
				authorization,
				offset,
				show);
	}

}
