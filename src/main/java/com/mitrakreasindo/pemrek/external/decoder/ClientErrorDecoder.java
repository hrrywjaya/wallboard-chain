package com.mitrakreasindo.pemrek.external.decoder;

import com.mitrakreasindo.pemrek.core.exception.CommonResourceNotFoundException;
import com.mitrakreasindo.pemrek.core.exception.CommonServerException;

import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClientErrorDecoder implements ErrorDecoder
{

	private ErrorDecoder defaultErrorDecoder = new Default();
	
	@Override
	public Exception decode (String methodKey, Response response)
	{
		log.debug("remote response {} ", response.toString());
		if (response.status() >= 400 && response.status() <= 499)
		{
			return new CommonResourceNotFoundException("data not found", response);
		}
		if (response.status() >= 500 && response.status() <= 599)
		{
			return new CommonServerException("System tidak tersedia");
		}		
		return defaultErrorDecoder.decode(methodKey, response);
	}
	
}
