package com.mitrakreasindo.pemrek;

import com.coxautodev.graphql.tools.SchemaParser;

import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableFeignClients
@EnableZuulProxy
@EnableKafka
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ChainApplication {

	
	public static void main(String[] args) {
		Logger log = LoggerFactory.getLogger("main");

		Resource krb5File = new ClassPathResource("krb5.conf");
		String krb5Path = null;
		Resource schemaRegistry = new ClassPathResource("schema_registry.truststore.jks");
		String schemaRegistryPath = null;
		Resource kafkaBrokerFile = new ClassPathResource("kafka_broker.truststore.jks");
		String kafkaBrokerFilePath = null;
		Resource keyTabFile = new ClassPathResource("kcpappchain.keytab");
		String keyTabPath = null;
		try {
			krb5Path = krb5File.getFile().getPath();
			schemaRegistryPath = schemaRegistry.getFile().getPath();
			kafkaBrokerFilePath = kafkaBrokerFile.getFile().getPath();
			keyTabPath = keyTabFile.getFile().getPath();
			log.info("====================== krb 5 : "+krb5Path);
			log.info("====================== schema : "+schemaRegistryPath);
			log.info("====================== kafka broker : "+kafkaBrokerFilePath);
			log.info("====================== keytab : "+keyTabPath);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		//config krb5
		System.setProperty("java.security.krb5.conf", krb5Path);
		//System.setProperty("javax.net.ssl.trustStore","F:/Sts-workspace/confluent-spring-kafka-producer-1.0.0/confluent-spring-kafka-producer-1.0.0/src/main/resources/kafka_broker.truststore.jks");
		System.setProperty("javax.net.ssl.trustStore",kafkaBrokerFilePath);
		System.setProperty("javax.net.ssl.trustStorePassword","confluenttruststorepass");
		System.setProperty("chainKeyTab", keyTabPath);


		final HashMap<String, String> hmSerdeConfig = new HashMap<>();

		hmSerdeConfig.put("schema.registry.url", "https://rhddspsrg01.cp.dti.co.id:8081");

//            hmSerdeConfig.put("basic.auth.user.info", basicAuthUserInfo);

//            hmSerdeConfig.put("basic.auth.credentials.source", basicAuthCredSource);

		hmSerdeConfig.put("auto.register.schemas", "false");

		hmSerdeConfig.put("ssl.client.auth", "true");

		hmSerdeConfig.put("schema.registry.ssl.truststore.location", schemaRegistryPath);

		hmSerdeConfig.put("schema.registry.ssl.truststore.password", "confluenttruststorepass");
		
		SpringApplication.run(ChainApplication.class, args);

		// SchemaParser.Builder builder = new SchemaParser.Builder();

		// // Resource[] resources =
		// applicationContext.getResources("classpath*:*.graphqls");
		// Resource[] resources =
		// applicationContext.getResources("classpath*:**/*.graphqls");
		// if(resources.length <= 0) {
		// throw new IllegalStateException("No .graphqls files found on classpath.
		// Please add a graphql schema to the classpath or add a SchemaParser bean to
		// your application context.");
		// }
		// SchemaParser.newParser().file("schema.graphqls").file("schema-transaksi.graphqls").file("schema-role.graphqls")
		// 		.file("schema-referensi.graphqls").file("schema-menu.graphqls").file("schema-jabber.graphqls")
		// 		.file("schema-foto.graphqls").file("schema-call-log.graphqls").file("schema-account.graphqls").build();
	}
}
