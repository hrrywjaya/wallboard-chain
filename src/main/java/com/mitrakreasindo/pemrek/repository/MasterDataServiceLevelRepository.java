package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;
import com.mitrakreasindo.pemrek.model.MasterDataServiceLevel;

@Repository
public interface MasterDataServiceLevelRepository extends BaseRepository<MasterDataServiceLevel, String>
{

    @Query("select a from #{#entityName} a where lower(a.activeFlag) = lower('ACTIVE') order by a.createdDate ASC")
    List<MasterDataServiceLevel> findAll();
}
