package com.mitrakreasindo.pemrek.repository;

import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;
import com.mitrakreasindo.pemrek.model.Tolak;

@Repository
public interface TolakRepository extends BaseRepository<Tolak, String>
{
		
}
