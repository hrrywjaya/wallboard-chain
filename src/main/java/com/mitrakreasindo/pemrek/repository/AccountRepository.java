package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.model.Active;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

	@Query("select a from #{#entityName} a left join fetch a.role r")
	List<Account> findAll();

	@Query(value = "select a from #{#entityName} a left join fetch a.role r", countQuery = "select count(1) from #{#entityName} a left join a.role r")
	// @Query("from #{#entityName} a left join fetch a.role r")
	Page<Account> findAll(Pageable pageable);

	@Query("select a from #{#entityName} a left join fetch a.role r where a.username = :username and a.activeFlag = :activeFlag")
	Account findByUsernameAndActiveFlag(@Param("username") String username, @Param("activeFlag") Active active);

	// List<Account> findAllByUsername(String username);

	@Query("select a from #{#entityName} a left join fetch a.role r where a.username = :username")
	Optional<Account> findByUsername(@Param("username") String username);
	
	@Query("select a from #{#entityName} a left join fetch a.role r where a.username = :username")
	Optional<Account> findFullnameByUsername(@Param("username") String username);

	// List<Account> findByTransaksiId(String id);

	@Query(
		value = "select a from #{#entityName} a left join fetch a.role r where lower(a.username) like lower(concat('%', :username,'%')) and a.activeFlag = :activeFlag", 
		countQuery = "select count(1) from #{#entityName} a left join a.role r where lower(a.username) like lower(concat('%', :username,'%')) and a.activeFlag = :activeFlag"
	)
	Page<Account> findByUsernameAndActiveFlag(@Param("username") String username, @Param("activeFlag") Active active, Pageable pageable);

	
	@Query(
		value = "select a from #{#entityName} a left join fetch a.role r where lower(a.username) like lower(concat('%', :username,'%'))", 
		countQuery = "select count(1) from #{#entityName} a left join a.role r where  lower(a.username) like lower(concat('%', :username,'%'))"
	)
	Page<Account> findByUsername(@Param("username") String username, Pageable pageable);

	
	@Query("select a from #{#entityName} a left join fetch a.role r where a.id = :id")
	Optional<Account> findById(@Param("id") String id);

	@Query("select a, r from #{#entityName} a left join fetch a.role r where a.id = :id")
	Account getOne(@Param("id") String id);

	@Query("select a from #{#entityName} a left join fetch a.role r where r.id = :id")
	List<Account> findByRoleId(@Param("id") String id);
	
	@Query("select a from #{#entityName} a left join fetch a.role r where r.name like concat('%',:roleName,'%')")
	List<Account> findByRoleName(@Param("roleName") String roleName);
	
	@Query(
		value = "select a from #{#entityName} a left join fetch a.role r where lower(a.username) like lower(concat('%', :username,'%')) and (r.name like '%AGENT%' or r.name like '%SUPERVISOR%')", 
		countQuery = "select count(1) from #{#entityName} a left join a.role r where  lower(a.username) like lower(concat('%', :username,'%')) and (r.name like '%AGENT%' or r.name like '%SUPERVISOR%')"
	)
	Page<Account> findByUsernameAndRolename(@Param("username") String username, Pageable pageable);

	@Query(
		value = "select a from #{#entityName} a left join fetch a.role r where (lower(a.username) like lower(concat('%', :username,'%')) and a.activeFlag = :activeFlag) and (r.name like '%AGENT%' or r.name like '%SUPERVISOR%')", 
		countQuery = "select count(1) from #{#entityName} a left join a.role r where (lower(a.username) like lower(concat('%', :username,'%')) and a.activeFlag = :activeFlag) and (r.name like '%AGENT%' or r.name like '%SUPERVISOR%')"
	)
	Page<Account> findByUsernameAndActiveFlagAndRolename(@Param("username") String username, @Param("activeFlag") Active active, Pageable pageable);

}
