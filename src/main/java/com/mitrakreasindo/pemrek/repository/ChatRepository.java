package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.model.Chat;

@Repository
public interface ChatRepository extends JpaRepository<Chat, String> {


}
