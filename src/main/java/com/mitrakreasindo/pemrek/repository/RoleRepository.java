package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    @Query(
		value = "select r from #{#entityName} r where lower(r.name) like lower(concat('%',:name,'%'))", 
		countQuery = "select count(1) from #{#entityName} r where lower(r.name) like lower(concat('%',:name,'%'))"
	)
	Page<Role> findByName(@Param("name") String name, Pageable pageable);

	@Query("select r from #{#entityName} r where r.name not like '%ADMIN%' and r.name not like '%SA%'")
	List<Role> findRoleByName();
	
    @Query(
		value = "select r from #{#entityName} r where lower(r.name) like lower(concat('%',:name,'%')) and r.activeFlag = :activeFlag", 
		countQuery = "select count(1) from #{#entityName} r where lower(r.name) like lower(concat('%',:name,'%')) and r.activeFlag = :activeFlag"
	)
	Page<Role> findByNameAndActiveFlag(@Param("name") String name, @Param("activeFlag") Active activeFlag, Pageable pageable);

	// @Query("select r from #{#entityName} r left join fetch r.menus m left join fetch m.acls a")
	// @Query("select distinct r from #{#entityName} r left join fetch r.roleMenus")
	@Query("select distinct r from #{#entityName} r left join fetch r.roleMenus rm left join fetch rm.menu rmm left join fetch rm.roleMenuAcls rma left join fetch rma.acl rmaa")
	List<Role> findAll();


	@Query("select distinct r from #{#entityName} r left join fetch r.roleMenus rm left join fetch rm.roleMenuAcls rma where lower(r.name) = lower(:name)")
	Optional<Role> findOneByName(@Param("name")String name);

	@Query("select distinct r from #{#entityName} r left join fetch r.roleMenus rm left join fetch rm.menu rmm left join fetch rm.roleMenuAcls rma left join fetch rma.acl rmaa where r.id = :id") // left fetch join r.roleMenus rm
	Optional<Role> findById(@Param("id")String id);

}
