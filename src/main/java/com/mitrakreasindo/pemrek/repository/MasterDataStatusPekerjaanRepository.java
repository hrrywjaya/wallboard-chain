package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mitrakreasindo.pemrek.model.MasterDataStatusPekerjaan;

public interface MasterDataStatusPekerjaanRepository extends JpaRepository<MasterDataStatusPekerjaan, String>, JpaSpecificationExecutor<MasterDataStatusPekerjaan> {

}
