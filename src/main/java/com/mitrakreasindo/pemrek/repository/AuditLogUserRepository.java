package com.mitrakreasindo.pemrek.repository;

import com.mitrakreasindo.pemrek.model.AuditLogUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditLogUserRepository extends JpaRepository<AuditLogUser, String> {

}