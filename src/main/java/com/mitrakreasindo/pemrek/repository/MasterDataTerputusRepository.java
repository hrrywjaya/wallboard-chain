package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;
import com.mitrakreasindo.pemrek.model.MasterDataTerputus;

@Repository
public interface MasterDataTerputusRepository extends BaseRepository<MasterDataTerputus, String>
{
    
    @Query("select a from #{#entityName} a where lower(a.activeFlag) = lower('ACTIVE') order by a.createdDate ASC")
    List<MasterDataTerputus> findAll();

}
