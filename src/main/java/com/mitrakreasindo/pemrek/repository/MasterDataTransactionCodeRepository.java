package com.mitrakreasindo.pemrek.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.model.MasterDataTransactionCode;

@Repository
public interface MasterDataTransactionCodeRepository extends JpaRepository<MasterDataTransactionCode, String> {

    @Query("select tc.code from #{#entityName} tc where lower(tc.text) = lower(:text)")
    Optional<MasterDataTransactionCode> findByText(String text);

    @Query("select a from #{#entityName} a where lower(a.text) = lower(:text)")
    MasterDataTransactionCode findCode(@Param("text") String text);
  
    // String select
}
