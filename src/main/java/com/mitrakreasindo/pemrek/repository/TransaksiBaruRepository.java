// package com.mitrakreasindo.pemrek.repository;

// import org.springframework.data.jpa.repository.Query;
// import org.springframework.stereotype.Repository;

// import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;
// import com.mitrakreasindo.pemrek.model.TransaksiBaru;

// @Repository
// public interface TransaksiBaruRepository extends BaseRepository<TransaksiBaru, String> {

// 	// TransaksiBaru findTop1ByNoRefAndAgentIdOrderByCreatedDateDesc(String noRef, String agentId);

// 	// TransaksiBaru findTop1ByNoRefOrderByCreatedDateDesc(String noRef);

// 	TransaksiBaru findById(String id);

// 	// MasterDataLog findByNoRefOrderByCreatedDateDescAndViPerubahan(String noRef);
	
// 	// @Query("select u from User u where u.firstname = :name or u.lastname = :name")
// 	// TransaksiBaru findByFirstnameOrLastname(String name);
	
// 	@Query("select t from TransaksiBaru t, Referensi r where t.id = r.transaksiBaru and r.noRef = ?1")
// 	TransaksiBaru findOneByNoRef(String noRef);
// }
