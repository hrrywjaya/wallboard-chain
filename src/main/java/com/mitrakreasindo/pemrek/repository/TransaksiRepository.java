package com.mitrakreasindo.pemrek.repository;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.Transaksi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, String> {

    // @Query("from #{#entityName} t left join fetch t.zeRefs zr left join fetch
    // t.referensis r where zr.noRef = :noRef")
    // Streamable<Transaksi> findByZeRefNoRef(@Param("noRef") String noRef);

    @Query("from #{#entityName} t join fetch t.referensis r left join fetch t.fotos f where r.noRef = :noRef")
    Streamable<Transaksi> findOneTransaksiByReferensi(@Param("noRef") String noRef);

    @Override
    @Query("select t from #{#entityName} t left join fetch t.referensis r left join fetch t.fotos f")
    // @Query("select distinct t from #{#entityName} t join fetch t.zeRefs z")
    List<Transaksi> findAll();

    // @Query("from #{#entityName} t join fetch t.zeRefs zr left join fetch
    // t.referensis r where zr.id = :id")
    // Streamable<Transaksi> findByZeRefsId(@Param("id") String id);

    @Override
    @Query("select t from #{#entityName} t left join fetch t.referensis r left join fetch t.fotos f where t.id = :id")
    Optional<Transaksi> findById(@Param("id") String id);

    Optional<Transaksi> findByRefNumAndEformCreatedDate(String refNum, String createdDate);
    
}