package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.Pesan;

@Repository
public interface PesanRepository extends JpaRepository<Pesan, String>
{

	@Query("from #{#entityName} p where p.createdDate >= function('TO_DATE',concat(:date,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') AND p.createdDate <= function('TO_DATE',concat(:date,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss') order by p.createdDate desc")
	List<Pesan> findByCreatedDate(@Param("date") String date);

	// Optional<Pesan> findAllByCreatedDate(String description);
		
}
