package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;

@Repository
public interface MasterDataBranchRepository extends JpaRepository<MasterDataBranch, String>, JpaSpecificationExecutor<MasterDataBranch>
{

	@Query("select a from #{#entityName} a where a.branchName = :branchName")
	MasterDataBranch findByBranchName(@Param("branchName") String branchName);

	@Query(
		value = "select a from #{#entityName} a where lower(a.branchName) like lower(concat('%', :branchName,'%')) or lower(a.branchCode) like lower(concat('%', :branchName,'%'))", 
		countQuery = "select count(1) from #{#entityName} a where lower(a.branchName) like lower(concat('%', :branchName,'%')) or lower(a.branchCode) like lower(concat('%', :branchName,'%'))"
	)
	Page<MasterDataBranch> findByBranchName(@Param("branchName") String branchName, Pageable pageable);

	Optional<MasterDataBranch> findOneByBranchCode(String branchCode);
	
	// Page<MasterDataBranch> findByBranchNameAndActiveFlag(String branchName, Active activeFlag, Pageable pageable);
	@Query("select a from #{#entityName} a where a.branchCode = :branchCode")
	MasterDataBranch findByBranchCode(String branchCode);

	
}
