package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;

@Repository
public interface MasterDataPekerjaanRepository extends JpaRepository<MasterDataPekerjaan, String>
{

	Optional<MasterDataPekerjaan> findOneByDescription(String description);
		
}
