package com.mitrakreasindo.pemrek.repository;

import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;
import com.mitrakreasindo.pemrek.model.Terputus;

@Repository
public interface TerputusRepository extends BaseRepository<Terputus, String>
{
		
}
