package com.mitrakreasindo.pemrek.repository;

import java.util.List;

import com.mitrakreasindo.pemrek.model.EddApproval;
import com.mitrakreasindo.pemrek.model.EmailRequest;
import com.mitrakreasindo.pemrek.external.common.model.ErrorMessageUpperCamelCase;
import com.mitrakreasindo.pemrek.external.common.model.ErrorSchemaUpperCamelCase;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name="edd-approval", url = "${generate.email.url}")
public interface EddApprovalRepository{
    
  @RequestMapping(value="/generateEmailFromTemplate/initiate", method=RequestMethod.POST)
  ErrorSchemaUpperCamelCase sendEmail(
    @RequestHeader("ClientID") String clientID,
    @RequestBody EmailRequest request
  );
  
}