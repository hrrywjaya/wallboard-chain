package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mitrakreasindo.pemrek.model.RoleMenu;

@Repository
public interface RoleMenuRepository extends JpaRepository<RoleMenu, String> {

	void deleteAllByRole(String id);

	@Transactional
	void deleteAllByRoleId(String id);

}
