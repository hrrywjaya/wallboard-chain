package com.mitrakreasindo.pemrek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.model.MasterDataChannel;

@Repository
public interface MasterDataChannelRepository extends JpaRepository<MasterDataChannel, String> {


}
