package com.mitrakreasindo.pemrek.repository;

import java.util.List;

import com.mitrakreasindo.pemrek.graphql.dto.MenuFlatDto;
import com.mitrakreasindo.pemrek.model.Menu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, String> {

    // @Query("select distinct m from #{#entityName} m left join fetch m.childs c left join fetch m.acls a left join fetch c.acls ca where m.parent is null order by m.createdDate")
    // List<Menu> findAll();
    // @Query("select m from #{#entityName} m left join fetch m.childs c left join fetch m.acls a left join fetch c.acls ca order by m.createdDate")
    // List<Menu> findAll();

    // @Query("select distinct m from #{#entityName} m left join fetch m.childs c
    // left join fetch m.acls a left join fetch c.childs cc left join fetch c.acls
    // ca where m.parent is null order by m.createdDate")

    // @Query("select distinct m from #{#entityName} m left join fetch m.acls a left join fetch m.childs c left join fetch c.acls ca where m.parent is null order by m.createdDate")
    // List<Menu> findAll();
    // @Query("select distinct m from #{#entityName} m left join fetch m.acls a left join fetch m.childs c left join fetch c.parent pc left join fetch c.acls ca where m.parent is null order by m.createdDate")
    // List<Menu> findAll();

    // @Query("select t from #{#entityName} t where t.attribute = ?1")
    // List<Menu> findAllByChild(String attribute);

    // List<Menu> findByChilds_Id(String id);

    // @Query("select c from #{#entityName} m left join fetch m.acls a left join fetch m.childs c where c.id in")
    // List<Menu> findByChildsId(List<String> ids);

    // @Query("select m from #{#entityName} m left join fetch m.acls a left join fetch m.childs c where c.id in :ids")
    @Query("select m from #{#entityName} m left join fetch m.childs c left join fetch m.acls a where m.id in :ids")
    List<Menu> findByChildsIdIn(@Param("ids") List<String> ids);

    @Query("select distinct m from #{#entityName} m left join fetch m.childs c left join fetch m.acls a left join fetch c.acls ca where m.parent is null order by m.createdDate")
    List<Menu> findAll();

    @Query("select distinct m from #{#entityName} m left join fetch m.childs c left join fetch m.acls a left join fetch c.acls ca where m.parent is null order by m.createdDate")
    List<Menu> findAllNested();

    @Query("select distinct m from #{#entityName} m left join fetch m.acls a")
    List<Menu> findAllFlat();
}