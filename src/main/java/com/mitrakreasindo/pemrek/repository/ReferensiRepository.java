package com.mitrakreasindo.pemrek.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.model.Referensi;

@Repository
public interface ReferensiRepository extends JpaRepository<Referensi, String> {

	List<Referensi> findByTransaksiId(String id);
	
	@Query("SELECT r1 FROM Referensi AS r LEFT JOIN FETCH Transaksi AS tb ON tb.id = r.transaksi LEFT JOIN FETCH Referensi AS r1 ON tb.id = r1.transaksi WHERE r.noRef = :NoRef ORDER BY r1.id DESC")
	Stream<Referensi> findOneLastByNoRef(@Param("NoRef")String noref);
	
	@Query(value = "select referensi2_.id, referensi2_.active_flag, referensi2_.created_by, referensi2_.created_date, referensi2_.updated_by, referensi2_.updated_date, referensi2_.no_ref, referensi2_.transaksi_id from referensi referensi0_ left outer join transaksi transaksi1_ on (transaksi1_.id=referensi0_.transaksi_id) left outer join referensi referensi2_ on (transaksi1_.id=referensi2_.transaksi_id) where referensi0_.no_ref=:NoRef order by referensi2_.id DESC FETCH FIRST 1 ROWS ONLY", nativeQuery = true)
	Referensi findLastByNoRef(@Param("NoRef")String noref);

	@Query(value = "select referensi1_.id, referensi1_.active_flag, referensi1_.created_by, referensi1_.created_date, referensi1_.updated_by, referensi1_.updated_date, referensi1_.no_ref, referensi1_.transaksi_id from referensi referensi1_ inner join transaksi transaksi1_ on (transaksi1_.id=referensi1_.transaksi_id) where transaksi1_.id=(select referensi2_.transaksi_id from referensi referensi2_ left join transaksi transaksi2_ on (transaksi2_.id=referensi2_.transaksi_id) where referensi2_.no_ref=:NoRef) order by referensi1_.created_date DESC FETCH FIRST 1 ROWS ONLY", nativeQuery = true)
	Referensi findLastByNoRef2(@Param("NoRef")String noref);
}
