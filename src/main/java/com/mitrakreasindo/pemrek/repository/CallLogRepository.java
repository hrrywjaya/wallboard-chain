package com.mitrakreasindo.pemrek.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitrakreasindo.pemrek.expose.transaction.model.TransaksiStatusDetailProjection;
import com.mitrakreasindo.pemrek.model.CallLog;

@Repository
public interface CallLogRepository extends JpaRepository<CallLog, String> {

  @Query("select cl from #{#entityName} cl where lower(cl.noRef) = lower(:noRef) and cl.tolakMdId is not null order by cl.createdDate desc")
  List<CallLog> findByNoRef(@Param("noRef") String noRef);

  @Query("select cl from #{#entityName} cl where lower(cl.qrNik) = lower(:qrNik) and cl.tolakMdId is not null order by cl.createdDate desc")
  List<CallLog> findAllTolakByQrNik(@Param("qrNik") String qrNik);

  // List<CallLog> findByTransaksiId(String id);

  // @Query("select t from #{#entityName} t where t.noRef = :noRef and
  // t.rawKonfirmasiData is not null order by t.createdDate ")
  // List<CallLog> getLogPerubahan(@Param("noRef") String noRef);

  // @Query("select t from #{#entityName} t where t.noRef = :noRef and
  // t.rawKonfirmasiData is not null order by t.createdDate ")
  // List<CallLog> getLogPerubahanByParam(@Param("param") String param);

  @Query("select t from #{#entityName} t where t.noRef = :noRef and t.perubahanData is not null order by t.createdDate ")
  List<CallLog> getLogPerubahan(@Param("noRef") String noRef);

  // @Query("select t from #{#entityName} t where t.createdDate >=
  // TO_DATE(:createdDate 00:00:00, 'yyyy-mm-dd hh24:mi:ss') AND CREATED_DATE <=
  // TO_DATE(':createdDate 23:59:00', 'yyyy-mm-dd hh24:mi:ss') and
  // t.rawKonfirmasiData is not null order by t.createdDate ")
  // List<CallLog> getLogPerubahanByDate(@Param("createdDate") String
  // createdDate);

  @Query("select t from #{#entityName} t where t.perubahanData is not null and t.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and t.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')  order by t.createdDate ")
  List<CallLog> getLogPerubahanByDate(@Param("startDate") String startDate, @Param("endDate") String endDate);

  @Query("select t from #{#entityName} t where t.perubahanData like %:param% and t.perubahanData is not null and t.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and t.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')  order by t.createdDate ")
  List<CallLog> getLogPerubahanByParam(@Param("param") String param, @Param("startDate") String startDate,
      @Param("endDate") String endDate);

  @Query("select cl from #{#entityName} cl where lower(cl.noRef) = lower(:noRef) and cl.terputusMdId is not null order by cl.createdDate desc")
  List<CallLog> findAllTerputusByNoRef(String noRef);

  // @Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and cl.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')" +
  //       "and cl.eddApproval is not null " +
  //       "and ((cl.qrChannel = :channel or :channel is null) ")
  // List<CallLog> findApprovalReport(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("channel") String channel,
  //     @Param("rekening") String rekening, @Param("cabang") String cabang, @Param("agent") String agent, @Param("status") String status);
  

  
  @Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.transaksiStatus = 'SUCCESS' and cl.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and cl.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')"+
  // "and ea.id is not null"+
  "and (cl.qrChannel = :channel or :channel is null)"+
  "and (cl.qrNoRekening = :rekening or :rekening is null)"+
  "and (cl.qrCabang = :cabang or :cabang is null)"+
  "and (lower(cl.fullName) like lower(concat('%',:agent,'%')) or :agent is null)"+
  "and (ea.approvalStatus = :status or :status is null) and ea.id is not null"
  )  
List<CallLog> findApprovalReport(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("channel") String channel,
@Param("rekening") String rekening, @Param("cabang") String cabang, @Param("agent") String agent, @Param("status") String status);

//waiting
@Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.qrCustomerNrtStatus != 'S' and cl.qrCustomerNrtStatus is not null and cl.transaksiStatus = 'SUCCESS' and ea.id is null and cl.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and cl.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')"+
  "and (cl.qrChannel = :channel or :channel is null)"+
  "and (cl.qrNoRekening = :rekening or :rekening is null)"+
  "and (cl.qrCabang = :cabang or :cabang is null)"+
  "and (lower(cl.fullName) like lower(concat('%',:agent,'%')) or :agent is null)"
  )  
List<CallLog> findApprovalReportStatusWaiting(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("channel") String channel,
@Param("rekening") String rekening, @Param("cabang") String cabang, @Param("agent") String agent);

//all
@Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.qrCustomerNrtStatus != 'S' and cl.qrCustomerNrtStatus is not null and cl.transaksiStatus = 'SUCCESS' and cl.createdDate >= function('TO_DATE',concat(:startDate,'00:00:00') ,'yyyy-mm-dd hh24:mi:ss') and cl.createdDate <= function('TO_DATE',concat(:endDate,'23:59:00') ,'yyyy-mm-dd hh24:mi:ss')"+
  "and (cl.qrChannel = :channel or :channel is null)"+
  "and (cl.qrNoRekening = :rekening or :rekening is null)"+
  "and (cl.qrCabang = :cabang or :cabang is null)"+
  "and (lower(cl.fullName) like lower(concat('%',:agent,'%')) or :agent is null)"
  )  
List<CallLog> findApprovalReportStatusAll(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("channel") String channel,
@Param("rekening") String rekening, @Param("cabang") String cabang, @Param("agent") String agent);


  @Query("select new com.mitrakreasindo.pemrek.expose.transaction.model.TransaksiStatusDetailProjection( cl.noRef, t.eformCreatedDate, cl.createdDate, cl.qrChannel, cl.qrJenisTransaksi, cl.qrNoRekening, cl.oldRawKonfirmasiData, cl.rawKonfirmasiData, cl.transaksiStatus, cl.fullName, cl.approvalFullname, cl.tolakMdId, cl.tolakMessage, cl.droppedDate, cl.workReadyDate, cl.terputusMessage, cl.serviceLevelMdId, cl.serviceLevelMessage ) from #{#entityName} cl inner join Transaksi t on cl.transaksiId = t.id and cl.noRef = ?1 and t.eformCreatedDate = ?2 ORDER BY cl.createdDate DESC")
  List<TransaksiStatusDetailProjection> findTransaksiStatusDetail(String nomorReferensi, String tanggal);
  
      // @Query("")
  // Page<TransaksiStatusProjection> findTransaksiStatus(String handphone, String
  // status, String tanggal, Pageable pageable);

  // edd approval list

  @Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.qrCustomerNrtStatus != 'S' and cl.qrCustomerNrtStatus is not null and cl.transaksiStatus = 'SUCCESS' and ea.id is null")
  List<CallLog> findAllEddApproval();

  // @Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.qrCustomerNrtStatus != 'S' and cl.qrCustomerNrtStatus is not null and cl.transaksiStatus = 'PENDING' and ea.id is null")
  // List<CallLog> findApprovalReport(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("channel") String channel,
  // @Param("rekening") String rekening, @Param("cabang") String cabang, @Param("agent") String agent, @Param("status") String status);

    
  @Query("select cl from #{#entityName} cl left join fetch cl.eddApproval ea where cl.id = :id")
  Optional<CallLog> findById(String id);

  @Query("select cl from #{#entityName} cl where cl.qrCustomerNrtStatus != 'S' and cl.qrCustomerNrtStatus is not null and cl.transaksiStatus = 'PENDING' ")
  List<CallLog> findAllEdd();


  Long countByEddApprovalIsNotNull();

  
  // @Query("select * from ( select cl from #{#entityName} cl left join Transaksi t on cl.transaksiId = t.id and cl.noRef = ?1 and t.eformCreatedDate = ?2) where rownum = 1")
  @Query(value = "SELECT * FROM ( SELECT * from  call_log cl inner join Transaksi t on cl.transaksi_id = t.id and cl.no_ref = ?1 and t.eform_created_date = ?2 ORDER BY cl.CREATED_DATE DESC) WHERE rownum = 1", nativeQuery = true)
  Optional<CallLog> findLastByNoRefAndNoRefCreatedDate(String noref, String norefCreatedDate);
  
  // @Override
  // @Query("select cl from #{#entityName} cl")
  // // @Query("select distinct t from #{#entityName} t join fetch t.zeRefs z join
  // fetch t.callLogs cl")
  // List<CallLog> findAll();

}