package com.mitrakreasindo.pemrek.repository;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.Jabber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JabberRepository extends JpaRepository<Jabber, String> {

	Optional<Jabber> findByIpAddress(String id);
	
}