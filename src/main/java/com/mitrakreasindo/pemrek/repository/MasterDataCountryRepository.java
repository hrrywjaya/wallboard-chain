package com.mitrakreasindo.pemrek.repository;

import java.util.Optional;

import com.mitrakreasindo.pemrek.model.MasterDataCountry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterDataCountryRepository extends JpaRepository<MasterDataCountry, String> {

    Optional<MasterDataCountry> findOneByCountryCodeIso(String iso);
}
