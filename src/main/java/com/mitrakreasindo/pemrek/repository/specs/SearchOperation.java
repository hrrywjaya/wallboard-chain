package com.mitrakreasindo.pemrek.repository.specs;

public enum SearchOperation {
    GREATER_THAN, LESS_THAN, GREATER_THAN_EQUAL, LESS_THAN_EQUAL, NOT_EQUAL, EQUAL, MATCH, MATCH_START, MATCH_END, IN,
    NOT_IN

    // public String toString() {
    // switch (this) {
    // case GREATER_THAN:
    // return "GREATER_THAN";
    // case LESS_THAN:
    // return "LESS_THAN";
    // case GREATER_THAN_EQUAL:
    // return "GREATER_THAN_EQUAL";
    // case LESS_THAN_EQUAL:
    // return "LESS_THAN_EQUAL";
    // case NOT_EQUAL:
    // return "NOT_EQUAL";
    // case EQUAL:
    // return "EQUAL";
    // case MATCH:
    // return "MATCH";
    // case MATCH_START:
    // return "MATCH_START";
    // case MATCH_END:
    // return "MATCH_END";
    // case IN:
    // return "IN";
    // case NOT_IN:
    // return "NOT_IN";
    // }
    // return null;
    // }

    // public static SearchOperation xxxOf(String value) {
    // if (value.equalsIgnoreCase(GREATER_THAN.toString()))
    // return SearchOperation.GREATER_THAN;
    // else if (value.equalsIgnoreCase(LESS_THAN.toString()))
    // return SearchOperation.LESS_THAN;
    // else if (value.equalsIgnoreCase(GREATER_THAN_EQUAL.toString()))
    // return SearchOperation.GREATER_THAN_EQUAL;
    // else
    // return null;
    // }
}