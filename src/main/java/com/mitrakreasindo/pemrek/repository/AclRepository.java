package com.mitrakreasindo.pemrek.repository;


import com.mitrakreasindo.pemrek.model.Acl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AclRepository extends JpaRepository<Acl, String> {

}