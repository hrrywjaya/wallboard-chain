package com.mitrakreasindo.pemrek.repository;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.Foto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FotoRepository extends JpaRepository<Foto, String> {

	List<Foto> findByTransaksiId(String id);

	Optional<Foto> findFirstByTransaksiIdOrderByCreatedDateDesc(String id);

}