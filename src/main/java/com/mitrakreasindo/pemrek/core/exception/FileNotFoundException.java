package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class FileNotFoundException extends RuntimeException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9183996805035203966L;
	
	public int code;
	
	public FileNotFoundException(String locationId)
	{
		super("File by location id "+locationId+ " not found");
		this.code = BaseExceptionCode.COMMON_ERROR_FILE_NOT_FOUND;
	}

}
