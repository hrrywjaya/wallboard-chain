package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class ResourceIsExistsException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4699006924920155053L;
	
	public int code;
	
	public ResourceIsExistsException(String message)
	{
		super(message);
		this.code = BaseExceptionCode.COMMON_ERROR_IS_EXIST;
	}

}
