package com.mitrakreasindo.pemrek.core.exception;

public class BaseExceptionCode
{
	
	public static final int COMMON_ERROR_ID_NOT_SPESIFIED = 4101;
	public static final int COMMON_ERROR_RESOURCE_NOT_FOUND = 4102;
	public static final int COMMON_ERROR_ID_NOT_MATCH = 4103;
	public static final int COMMON_ERROR_FILE_NOT_FOUND = 4104;
	public static final int COMMON_ERROR_CANONT_EMPTY = 4105;
	public static final int COMMON_ERROR_IS_EXIST = 4106;
	
	public static final int COMMON_SERVER = 5000;
	
}
