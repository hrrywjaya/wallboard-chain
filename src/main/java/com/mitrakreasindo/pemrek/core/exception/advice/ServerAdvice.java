package com.mitrakreasindo.pemrek.core.exception.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mitrakreasindo.pemrek.core.exception.CommonServerException;
import com.mitrakreasindo.pemrek.core.model.ErrorModel;

@RestControllerAdvice
public class ServerAdvice
{

	@ExceptionHandler({CommonServerException.class})
	public ResponseEntity<ErrorModel> handleException(Exception e)
	{
		
		if (e instanceof CommonServerException)
		{
			CommonServerException ex = (CommonServerException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}
			
		return null;
	}
	
}
