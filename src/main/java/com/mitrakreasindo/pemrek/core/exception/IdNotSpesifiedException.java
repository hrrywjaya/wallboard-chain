package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class IdNotSpesifiedException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6259752705747517417L;
	
	public int code;
	
	public IdNotSpesifiedException()
	{
		super("Id not spesified");
		this.code = BaseExceptionCode.COMMON_ERROR_ID_NOT_SPESIFIED;
	}

}
