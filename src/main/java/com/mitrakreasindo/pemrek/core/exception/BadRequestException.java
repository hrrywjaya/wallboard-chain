package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class BadRequestException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6259752705747517417L;
	
	public int code;
	
	public BadRequestException(int code, String message)
	{
		super(message);
		this.code = code;
	}

}
