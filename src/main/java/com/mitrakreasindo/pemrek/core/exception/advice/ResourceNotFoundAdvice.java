package com.mitrakreasindo.pemrek.core.exception.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mitrakreasindo.pemrek.core.exception.CommonResourceNotFoundException;
import com.mitrakreasindo.pemrek.core.exception.FileNotFoundException;
import com.mitrakreasindo.pemrek.core.exception.ResourceNotFoundException;
import com.mitrakreasindo.pemrek.core.model.ErrorModel;

@RestControllerAdvice
public class ResourceNotFoundAdvice
{

	@ExceptionHandler({CommonResourceNotFoundException.class, ResourceNotFoundException.class, FileNotFoundException.class})
	public ResponseEntity<ErrorModel> handleException(Exception e)
	{
		
		if (e instanceof CommonResourceNotFoundException)
		{
			CommonResourceNotFoundException ex = (CommonResourceNotFoundException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.NOT_FOUND);
		}
		
		if (e instanceof ResourceNotFoundException)
		{
			ResourceNotFoundException ex = (ResourceNotFoundException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.NOT_FOUND);
		}
		
		if (e instanceof FileNotFoundException)
		{
			FileNotFoundException ex = (FileNotFoundException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.NOT_FOUND);
		}
				
		return null;
	}
	
}
