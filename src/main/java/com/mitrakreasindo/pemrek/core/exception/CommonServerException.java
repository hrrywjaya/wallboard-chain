package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class CommonServerException extends RuntimeException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6343147051813303504L;

	public int code;
	
	public CommonServerException(String message)
	{
		super(message);
		this.code = BaseExceptionCode.COMMON_SERVER;
	}

}
