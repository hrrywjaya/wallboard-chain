package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class ResourceNotFoundException extends RuntimeException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6343147051813303504L;

	public int code;
	
	public ResourceNotFoundException(String id)
	{
		super("Resource by id "+id+ " not found");
		this.code = BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND;
	}

}
