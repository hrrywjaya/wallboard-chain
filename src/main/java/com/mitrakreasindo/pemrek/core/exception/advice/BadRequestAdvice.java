package com.mitrakreasindo.pemrek.core.exception.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.IdNotMatchException;
import com.mitrakreasindo.pemrek.core.exception.IdNotSpesifiedException;
import com.mitrakreasindo.pemrek.core.model.ErrorModel;

@RestControllerAdvice
public class BadRequestAdvice
{

	@ExceptionHandler({BadRequestException.class, IdNotMatchException.class, IdNotSpesifiedException.class})
	public ResponseEntity<ErrorModel> handleException(Exception e)
	{
		if (e instanceof BadRequestException)
		{
			BadRequestException ex = (BadRequestException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.BAD_REQUEST);
		}
		
		if (e instanceof IdNotMatchException)
		{
			IdNotMatchException ex = (IdNotMatchException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.BAD_REQUEST);
		}
		

		if (e instanceof IdNotSpesifiedException)
		{
			IdNotSpesifiedException ex = (IdNotSpesifiedException) e;
			ErrorModel model = new ErrorModel(ex.code, ex.getMessage());
			return new ResponseEntity<ErrorModel>(model, HttpStatus.BAD_REQUEST);
		}
		
		return null;
	}
	
}
