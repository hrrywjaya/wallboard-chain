package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class CommonResourceNotFoundException extends RuntimeException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6343147051813303504L;

	public int code;
	public Object data;
	
	public CommonResourceNotFoundException(String message)
	{
		super(message);
		this.code = BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND;
	}

	public CommonResourceNotFoundException(String message, Object data)
	{
		super(message);
		this.code = BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND;
		this.data = data;
	}

}
