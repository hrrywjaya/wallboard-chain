package com.mitrakreasindo.pemrek.core.exception;

/**
 * @author miftakhul
 *
 */
public class IdNotMatchException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int code;
	
	public IdNotMatchException()
	{
		super("Id doesn't match");
		this.code = BaseExceptionCode.COMMON_ERROR_ID_NOT_MATCH;
	}

}
