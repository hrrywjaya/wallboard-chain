package com.mitrakreasindo.pemrek.core.model;

public class ErrorModel
{

	public int code;
	public String message;
	
	public ErrorModel(int code, String message)
	{
		this.code = code;
		this.message = message;
	}
		
}
