package com.mitrakreasindo.pemrek.core.model;


public enum Active {
    ACTIVE, 
    DELETED,
    ENABLED,
    ALL;

    @Override
    public String toString() {
        return this == ALL ? "" : this.name();
    }
}

// public enum Active {
//     ACTIVE("ACTIVE"), 
//     DELETED("DELETED"), 
//     ENABLED("ENABLED"),
//     ALL("");
//     // ED("STOP"), GREEN("GO"), ORANGE("SLOW DOWN"); 
//     // declaring private variable for getting values 
//     private String action; 
  
//     // // getter method 
//     public String getAction() 
//     { 
//         return this.action; 
//     } 
  
//     // // enum constructor - cannot be public or protected 
//     private Active(String action) 
//     { 
//         this.action = action; 
//     } 
// }