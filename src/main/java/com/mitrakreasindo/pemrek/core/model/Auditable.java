package com.mitrakreasindo.pemrek.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

// @Getter(AccessLevel.PROTECTED)
// @Setter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

    // @CreatedBy
    // @Column(name = "created_by")
    // private U createdBy;


    // @CreatedDate
    // @Column(name = "created_date")
    // private Date createdDate;

    // @LastModifiedBy
    // @Column(name = "last_modified_by")
    // private U updatedBy;


    // @LastModifiedDate
    // @Column(name = "last_modified_date")
    // private Date updatedDate;

    @CreatedBy
    private U createdBy;

    @CreatedDate
    private Date createdDate;

    @LastModifiedBy
    private U updatedBy;

    @LastModifiedDate
    private Date updatedDate;
    
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Active activeFlag = Active.ACTIVE;
}