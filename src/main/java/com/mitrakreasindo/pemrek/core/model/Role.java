package com.mitrakreasindo.pemrek.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mitrakreasindo.pemrek.core.model.base.BaseModel;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.RoleMenu;
import com.mitrakreasindo.pemrek.model.Transaksi;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author miftakhul
 *
 */
@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
// @EqualsAndHashCode(exclude = { "accounts", "menus" })
@EqualsAndHashCode(exclude = { "accounts","roleMenus" })
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Role extends Auditable<String> implements Serializable {

	public static enum Type {
		ADMIN, USER
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2304597535286185712L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
	// @JsonManagedReference
	// @JsonBackReference
	private Set<Account> accounts = new HashSet<>();

	// @NotNull
	// @ManyToMany(fetch = FetchType.LAZY)
	// @JoinTable(name = "role_menus", joinColumns = @JoinColumn(name = "role_id"),
	// inverseJoinColumns = @JoinColumn(name = "menu_id"))
	// @JsonManagedReference
	// private Set<Menu> menus = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
	// @JsonManagedReference
	// @JsonBackReference
	private Set<RoleMenu> roleMenus = new HashSet<>();

	@NotNull
	@Column(unique = true)
	private String name;
	private String description;

	

	public void addRoleMenus(RoleMenu r) {
		r.setRole(this);
		roleMenus.add(r);
	}
	// public void removeRoleMenus(RoleMenu r) {
	// 	// r.setRole(this);
	// 	roleMenus.remove(r);
	// }
	// public void removeAllRoleMenus(Set<RoleMenu> rs) {
	// 	// r.setRole(this);
	// 	roleMenus.removeAll(rs);
	// }
	public void removeRoleMenus(RoleMenu child) {
        roleMenus.remove(child);
        child.setRole(null);
    }

}
