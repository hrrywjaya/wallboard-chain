package com.mitrakreasindo.pemrek.core.model.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mitrakreasindo.pemrek.core.model.Active;

import lombok.Data;


/**
 * @author miftakhul
 *
 */
@Data
@MappedSuperclass
public class BaseModel implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;
	@CreatedDate
	private Date createdDate;
	@CreatedBy
	private String createdBy;
	@LastModifiedDate
	private Date updatedDate;
	@LastModifiedBy
	private String updatedBy;
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Active activeFlag = Active.ACTIVE;
	
}