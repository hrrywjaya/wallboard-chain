package com.mitrakreasindo.pemrek.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@EnableWebMvc
@Configuration
public class CorsConfig 
//implements WebMvcConfigurer
{
	
//	@Override
//	public void addCorsMappings (CorsRegistry registry)
//	{
//		registry.addMapping("/")
//		.allowCredentials(true)
//		.allowedOrigins("*")
//		.allowedMethods("*")
//		.allowedHeaders("*")
//		.maxAge(30);
//	}

//	@Bean
//	public CorsConfigurationSource corsConfiguration() {
//		CorsConfiguration configuration = new CorsConfiguration();
//		configuration.applyPermitDefaultValues();
////		configuration.setAllowCredentials(true);
////		configuration.setAllowedOrigins(Arrays.asList("*"));
////		configuration.addAllowedOrigin("http://10.20.234.94:8082");
////		configuration.addAllowedOrigin("http://10.5.41.116:4200");
////		configuration.addAllowedOrigin("http://127.0.0.1:8082");
////		configuration.addAllowedOrigin("*");
////		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
////		configuration.setAllowedHeaders(Arrays.asList("X-Request-With", "Origin", "Content-Type", "Accept", "Authorization"));
////		configuration.addAllowedOrigin("*");
////		configuration.addAllowedMethod("*");
////		configuration.addAllowedHeader("*");
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", configuration);
//		return source;
//	}
	
//	@Bean
//	public FilterRegistrationBean corsFilter() {
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		CorsConfiguration config = new CorsConfiguration();
//		config.setAllowCredentials(true);
//		config.addAllowedOrigin("http://127.0.0.1:8082");
//		config.addAllowedOrigin("*");
//		config.addAllowedHeader("*");
//		config.addAllowedMethod("*");
//		config.addAllowedMethod(HttpMethod.OPTIONS);
//		config.addAllowedMethod(HttpMethod.GET);
//		config.addAllowedMethod(HttpMethod.POST);
//		config.addAllowedMethod(HttpMethod.PUT);
//		config.addAllowedMethod(HttpMethod.PATCH);
//		config.addAllowedMethod(HttpMethod.DELETE);
//		source.registerCorsConfiguration("/**", config);
//		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
//		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return bean;
//	} 
	
	
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/**")
//				.allowCredentials(true)
//				.allowedHeaders("*")
//				.allowedMethods("*")
//				.allowedOrigins("*");
//				
//			}
//		};
//	}
	
	
	@Bean
	public CorsFilter corsFilter() {

	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    CorsConfiguration config = new CorsConfiguration();
	    config.setAllowCredentials(true);
		config.addAllowedOrigin("http://localhost:4200");
		config.addAllowedOrigin("http://10.20.234.94:8082");
		config.addAllowedOrigin("http://192.168.1.104:9700");
		config.addAllowedOrigin("http://localhost:9700");
		config.addAllowedOrigin("http://10.5.41.116:4200");
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("PATCH");
		config.addAllowedMethod("DELETE");
	    source.registerCorsConfiguration("/**", config);
	    return new CorsFilter(source);
	}

	
}
