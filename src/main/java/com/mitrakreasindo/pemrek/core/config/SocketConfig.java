package com.mitrakreasindo.pemrek.core.config;

import java.net.URISyntaxException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.JsonNode;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class SocketConfig
{
	
	@Value("${socket.server.url}")
	private String socketServerUrl;
	private String username = "socket-backend";
	private String role = "sytem";
	
	
	
	@Bean
	public Socket socket() {
		try
		{
			Socket socket = IO.socket(socketServerUrl);
			socket
			.on(Socket.EVENT_CONNECTING, new Emitter.Listener()
			{
				
				@Override
				public void call(Object... args)
				{
					log.debug("socket connecting");
				}
			}).on(Socket.EVENT_CONNECT, new Emitter.Listener()
			{
				
				@Override
				public void call(Object... args)
				{
					log.debug("socket connected");
					login();
				}
			}).on(Socket.EVENT_RECONNECT, new Emitter.Listener()
			{
				
				@Override
				public void call(Object... args)
				{
					log.debug("socket reconnected");
				}
			}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener()
			{
				
				@Override
				public void call (Object... args)
				{
					log.debug("socket disconnected");
				}
			});
			socket.connect();
			return socket;
		} catch (URISyntaxException e)
		{
			log.error("socket error : {}", e.getMessage());
		}
		
		return null;
	}
	
	private void login() {
		if (socket() == null)
			return;
		
		JSONObject user = new JSONObject();
		try
		{
			user.put("username", username);
			user.put("role", role);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		log.debug("sending socket login");
		socket().emit("add user", user);		
	}
	
	

}
