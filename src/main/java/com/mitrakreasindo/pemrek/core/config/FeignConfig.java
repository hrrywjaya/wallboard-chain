package com.mitrakreasindo.pemrek.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mitrakreasindo.pemrek.external.decoder.ClientErrorDecoder;

import feign.Logger;

@Configuration
public class FeignConfig
{

	@Bean
	public ClientErrorDecoder decoder()
	{
		return new ClientErrorDecoder();
	}
	
//	@Bean
//	public Encoder encoder(ObjectFactory<HttpMessageConverters> converters) {
//		return new SpringFormEncoder(new SpringEncoder(converters));
//	}
	
	
	@Bean
	public Logger.Level feignLoggerLevel() {
		return Logger.Level.FULL;
	}
	
//	@Bean
//	public Client feignClient()
//	{
//	    Client trustSSLSockets = new Client.Default(getSSLSocketFactory(), new NoopHostnameVerifier());
////	    log.info("feignClient called"); 
//	    return trustSSLSockets;
//	}
//
//
	
//	@Bean
//	public Client feignClient()
//	{
//		return null;
//	    Client client = new Client.Default(new Nai, hostnameVerifier)
//	}
	
//	private SSLSocketFactory getSSLSocketFactory() {
//	    try {
//	    TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
//	        @Override
//	        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//	            //Do your validations
//	            return true;
//	        }
//	        };
//
//	        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
//	        return sslContext.getSocketFactory();
//	    } catch (Exception exception) {
//	        throw new RuntimeException(exception);
//	    }
//	}
	
}
