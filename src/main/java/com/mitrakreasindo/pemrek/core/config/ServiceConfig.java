package com.mitrakreasindo.pemrek.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author miftakhul
 *
 */
@Configuration
@EnableTransactionManagement
public class ServiceConfig
{

}
