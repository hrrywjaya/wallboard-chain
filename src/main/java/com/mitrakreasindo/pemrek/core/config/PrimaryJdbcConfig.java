package com.mitrakreasindo.pemrek.core.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class PrimaryJdbcConfig {
	
	@Value("${spring.datasource.url}")
	private String springUrl;
	@Value("${spring.datasource.username}")
	private String springUsername;
	@Value("${spring.datasource.password}")
	private String springPassword;

	@Bean
	@Primary
	public DataSource datasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(springUrl);
		dataSource.setUsername(springUsername);
		dataSource.setPassword(springPassword);
		return dataSource;
	}

  @Bean
  @Primary
	public JdbcTemplate chainJdbc () {
		return new JdbcTemplate(datasource());
	}

}
