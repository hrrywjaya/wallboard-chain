package com.mitrakreasindo.pemrek.core.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class CuicJdbcConfig {
	
	@Value("${cuic.datasource.url}")
	private String cuicUrl;
	@Value("${cuic.datasource.username}")
	private String cuicUsername;
	@Value("${cuic.datasource.password}")
	private String cuicPassword;

	@Bean(name = "cuicDatasource")
	public DataSource cuicDatasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		dataSource.setUrl(cuicUrl);
		dataSource.setUsername(cuicUsername);
		dataSource.setPassword(cuicPassword);
		return dataSource;
	}
	
	@Bean(name = "cuicJdbc")
	public JdbcTemplate cuicJdbc () {
		return new JdbcTemplate(cuicDatasource());
	}
	
}
