package com.mitrakreasindo.pemrek.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mitrakreasindo.pemrek.core.security.TripleDesEncription;

@Configuration
public class EncryptorConfig
{

	@Value("${des.key}")
	private String desKey;
	
	@Bean
	public TripleDesEncription desEncription() {
		return new TripleDesEncription(desKey);
	}
	
}
