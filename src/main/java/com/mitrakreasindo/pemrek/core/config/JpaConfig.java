package com.mitrakreasindo.pemrek.core.config;

import org.hibernate.collection.spi.PersistentCollection;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NamingConventions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.mitrakreasindo.pemrek.graphql.dto.CallLogInput;
import com.mitrakreasindo.pemrek.model.CallLog;

@Configuration
// @EnableJpaAuditing
public class JpaConfig {
    @Bean
    @Primary
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        // return new ModelMapper();
        // modelMapper.getConfiguration()
        // .setPropertyCondition(context -> !(context.getSource() instanceof
        // PersistentCollection));
        // modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());

        modelMapper.getConfiguration().setFieldMatchingEnabled(true).setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldAccessLevel(AccessLevel.PRIVATE).setSkipNullEnabled(true)
                .setSourceNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
        PropertyMap<CallLogInput, CallLog> callLogInputMap = new PropertyMap<CallLogInput, CallLog>() {
            protected void configure() {
                // map(source.getId()).setTransaksiId(null);
                // map().setTransaksiId(null);
                // map().setId(null);
                // map().setCallId(source.getCallId());
                when(Conditions.isNull()).skip().setTransaksiId(null);
                when(Conditions.isNull()).skip().setId(null);
            }
        };
        modelMapper.addMappings(callLogInputMap);
        return modelMapper;
    }

    @Bean({ "lazy" })
    public ModelMapper modelMapperLazy() {
        ModelMapper modelMapper = new ModelMapper();
        // return new ModelMapper();
        modelMapper.getConfiguration()
                .setPropertyCondition(context -> !(context.getSource() instanceof PersistentCollection));
        return modelMapper;
    }

    @Bean({ "mapperPatch" })
    public ModelMapper modelMapperPatch() {
        ModelMapper mapper = new ModelMapper();
        // modelMapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(AccessLevel.PRIVATE);
        // Condition<?, ?> isNotNullAndEmpty = new AbstractCondition<Object, Object>() {
        //     @Override
        //     public boolean applies(MappingContext<Object, Object> context) {
        //         if (context.getSource() instanceof Collection) {
        //             Collection<?> collection = (Collection<?>) context.getSource();
        //             return collection != null;
        //         }
        //         return true;
        //     }
        // };
        // mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // mapper.getConfiguration().setPropertyCondition(isNotNullAndEmpty);
        // mapper.getConfiguration().setFieldMatchingEnabled(true);
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper;
    }

    @Bean({ "mapperNormal" })
    public ModelMapper modelMapperNormal() {
        ModelMapper mapper = new ModelMapper();;
        return mapper;
    }

    // modelMapper.getConfiguration().setPropertyCondition(new Condition<Object,
    // Object>() {
    // public boolean applies(MappingContext<Object, Object> context) {
    // return !(context.getSource() instanceof PersistentCollection);
    // }
    // });

}
