package com.mitrakreasindo.pemrek.core.component;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelPatcher {

    @Autowired
    private ObjectMapper objectMapper;
    
    @SuppressWarnings("unchecked")
    public <T, U> T convert(T source, U dto, Class<T> sourceTypeClass) {

        Map<String, Object> mapSource = objectMapper.convertValue(source, Map.class);
        Map<String, Object> mapDto = objectMapper.convertValue(dto, Map.class);
        // Map<String, Object> mapDto2 = mapDto.entrySet()
        // mapDto.entrySet()
        // .stream().map(e -> {
        //         // System.out.println("key: " + e.getKey() + ", val: " + e.getValue());
        //         return e;
        //     }).collect(Collectors.toList())
        // .removeIf(e -> {
        //     // return !!"terputusMessage".equalsIgnoreCase(e.getKey());
        //         System.out.println("key: " + e.getKey() + ", val: " + e.getValue());
        //     if (e.getKey().equalsIgnoreCase("terputusMessage")) {
        //         return false;
        //     }
        //     return true;
        //     // return !e.getKey().equalsIgnoreCase("terputusMessage") && Objects.isNull(e.getValue())
        // });
        // .removeIf(e -> e.getValue().equals(null));
        // mapDto.values().stream().map(e -> {
        //     System.out.println("objectMapper -> " + e);
        //     return e;
        // }).collect(Collectors.toList())
        mapDto.values().removeIf(Objects::isNull);
        mapSource.putAll(mapDto);

        T sourceUpdated = objectMapper.convertValue(mapSource, sourceTypeClass);

        return sourceUpdated;
    }

}