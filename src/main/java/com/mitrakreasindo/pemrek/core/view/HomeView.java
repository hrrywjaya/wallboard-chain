package com.mitrakreasindo.pemrek.core.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeView
{

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home()
	{
		return "home";
	}
	
}
