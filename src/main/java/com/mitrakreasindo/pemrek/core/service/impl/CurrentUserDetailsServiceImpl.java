package com.mitrakreasindo.pemrek.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.model.CurrentPrincipal;
import com.mitrakreasindo.pemrek.service.AccountService;

/**
 * @author miftakhul
 *
 */
@Service
public class CurrentUserDetailsServiceImpl implements UserDetailsService
{

	// @Autowired
	// private UserService userService;
	@Autowired
	private AccountService userService;
	
	@Override
	public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException
	{
		Account user = userService.findByUsername(username);
		
		if (user == null)
			throw new UsernameNotFoundException("username "+username+" not found");

		return new CurrentPrincipal(user);
	}

}
