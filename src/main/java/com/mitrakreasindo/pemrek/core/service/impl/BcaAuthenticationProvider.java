package com.mitrakreasindo.pemrek.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;

@Service
public class BcaAuthenticationProvider implements AuthenticationProvider
{

	@Autowired
	private AdGatewayService adGatewayService;
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	public Authentication authenticate (Authentication authentication) throws AuthenticationException
	{
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		UserDetails user = userDetailsService.loadUserByUsername(username);
		
		try {
			OutputStatus status = adGatewayService.login(username, password);
			if (status.getOutputSchema() != null)
				return new UsernamePasswordAuthenticationToken(username, password, user.getAuthorities());
		} catch (Exception e) {
			throw new UsernameNotFoundException("failed login to ad");
		}
		
		throw new UsernameNotFoundException("failed login to ad");
	}

	@Override
	public boolean supports (Class<?> authentication)
	{
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
