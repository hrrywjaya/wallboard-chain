package com.mitrakreasindo.pemrek.core.service.base;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitrakreasindo.pemrek.core.exception.IdNotMatchException;
import com.mitrakreasindo.pemrek.core.exception.IdNotSpesifiedException;
import com.mitrakreasindo.pemrek.core.exception.ResourceNotFoundException;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.base.BaseModel;
import com.mitrakreasindo.pemrek.core.repository.base.BaseRepository;

/**
 * @author miftakhul
 *
 * @param <T>
 */
@Transactional
public abstract class BaseServiceImpl<T extends BaseModel> implements BaseService<T>
{
	
	@Autowired
	private BaseRepository<T, String> repository;

	public List<T> findAll()
	{
		return repository.findByActiveFlag(Active.ACTIVE);
	}
	
	public Page<T> findAll(Pageable pageable)
	{
		return repository.findByActiveFlag(pageable, Active.ACTIVE);
	}
	
	@Override
	public T findById (String id)
	{
		T data = repository.findByIdAndActiveFlag(id, Active.ACTIVE);
		if (data == null)
			throw new ResourceNotFoundException(id.toString());
		
		return data;
	}
	
	public T findByIdNullable (String id)
	{
		T data = repository.findByIdAndActiveFlag(id, Active.ACTIVE);
		return data;
	}	

	@Override
	public T save (T data)
	{
		data.setId(null);
		data.setActiveFlag(Active.ACTIVE);
		return repository.save(data);
	}

	@Override
	public T update (String id, T data)
	{
		if (id == null || data.getId() == null)
			throw new IdNotSpesifiedException();
		
		// parameter id and id in data doesn't match
		if (!id.equals(data.getId()))
			throw new IdNotMatchException();

		T dataInResource = findByIdNullable(id);
		if (dataInResource == null)
			throw new ResourceNotFoundException(id.toString());		
		
		BeanUtils.copyProperties(data, dataInResource);

		dataInResource.setActiveFlag(Active.ACTIVE);
		return repository.save(dataInResource);
	}

	@Override
	public void delete (String id)
	{
		if (id == null)
			throw new IdNotSpesifiedException();		
		
		T data = findByIdNullable(id);
		if (data == null)
			throw new ResourceNotFoundException(id.toString());		
		
		data.setActiveFlag(Active.DELETED);
		update(id, data);
	}
	
	@Override
	public boolean isEmpty ()
	{
		return repository.count() > 0 ? true : false;
	}
		
}
