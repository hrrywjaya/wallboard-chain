package com.mitrakreasindo.pemrek.core.service.impl;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatedUserAwareImpl implements AuditorAware<String>
{

	@Override
	public Optional<String> getCurrentAuditor ()
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication != null ? Optional.of(authentication.getName()) : Optional.empty();
	}

}
