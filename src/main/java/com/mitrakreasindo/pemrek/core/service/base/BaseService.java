package com.mitrakreasindo.pemrek.core.service.base;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitrakreasindo.pemrek.core.model.base.BaseModel;

/**
 * @author miftakhul
 *
 * @param <T>
 */
public interface BaseService<T extends BaseModel>
{

	List<T> findAll();
	
	Page<T> findAll(Pageable pageable);
	
	T findById (String id);

	T save (T data);

	T update (String id, T data);
	
	void delete(String id);
	
	boolean isEmpty();

}
