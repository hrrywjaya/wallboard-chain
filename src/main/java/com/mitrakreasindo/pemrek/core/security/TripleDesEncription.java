package com.mitrakreasindo.pemrek.core.security;

public class TripleDesEncription
{
	
	private String password;
	
	public TripleDesEncription(String password) {
		this.password = password;
	}
	
	public String bcaEencrypt(String cleanData)
	    throws Exception
	  {
	    try
	    {
	      EnDeCipherDESTriple DES = new EnDeCipherDESTriple(password.getBytes());
	      return DES.encrypt(cleanData);
	    }
	    catch (Exception ex)
	    {
	      throw new Exception("ESB-12 encrypt error : " + ex.getMessage());
	    }
	  }
		
}
