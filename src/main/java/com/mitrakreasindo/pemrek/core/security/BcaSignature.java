package com.mitrakreasindo.pemrek.core.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class BcaSignature
{

	@Autowired
	private ObjectMapper mapper;
	
	public String sign(String httpMethod, String relativeUrl, String time, String json, String secret, String token) {
		String data = httpMethod+":"+relativeUrl+":"+token+":"+sha256(json)+":" + time;
		return hmacSignarture(secret, data);
	}
	
	public String hmacSignarture(String key, String data) {
		try
		{
			Mac sha256Hmac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
			sha256Hmac.init(secretKey);
			return Hex.encodeHexString(sha256Hmac.doFinal(data.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		} catch (InvalidKeyException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String sha256Emty() {
		String bodyString = "";
		String hashed = DigestUtils.sha256Hex(bodyString);
		return hashed;
	}
	
	public String sha256(Object data) {
		if (data == null)
			return sha256Emty();
		
		String bodyString = null;
		try
		{ 
			bodyString = mapper.writeValueAsString(data);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		String hashed = DigestUtils.sha256Hex(bodyString);
		return hashed;
	}
	
	public String sha256(String data) {
		if (data == null)
			return sha256Emty();
		
		data = data.replace(" ", "");
		String hashed = DigestUtils.sha256Hex(data);
		return hashed;
	}
		
}
