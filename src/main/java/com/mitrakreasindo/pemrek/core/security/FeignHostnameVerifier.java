// package com.mitrakreasindo.pemrek.core.security;

// import java.util.Arrays;
// import java.util.Collections;
// import java.util.HashSet;
// import java.util.Set;

// import javax.net.ssl.HostnameVerifier;
// import javax.net.ssl.HttpsURLConnection;
// import javax.net.ssl.SSLSession;

// public class FeignHostnameVerifier implements HostnameVerifier
// {

// 	private Set<String> trustedHostname;
// 	private HostnameVerifier hostVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
	
// 	public FeignHostnameVerifier(String... trusted)
// 	{
// 		this.trustedHostname = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(trusted)));
// 	}
	
// 	@Override
// 	public boolean verify (String hostname, SSLSession sslSession)
// 	{
// 		return this.trustedHostname.contains(hostname) || this.hostVerifier.verify(hostname, sslSession);
// 	}

// }
