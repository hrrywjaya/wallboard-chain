package com.mitrakreasindo.pemrek.core.security;

public class Util
{
	
	public static byte[] fromHexString(String s)
  {
    byte[] bytes = new byte[s.length() / 2];
    for (int i = 0; i < s.length() / 2; i++) {
      bytes[i] = ((byte)Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16));
    }
    return bytes;
  }
	
	public static String toHexString(byte[] b)
  {
    if ((b == null) || (b.length == 0)) {
      return "";
    }
    return toHexString(b, 0, b.length);
  }
  
  private static String toHexString(byte[] b, int off, int len)
  {
    StringBuffer s = new StringBuffer();
    for (int i = off; i < off + len; i++)
    {
      s.append("0123456789ABCDEF".charAt((b[i] & 0xFF) >> 4));
      s.append("0123456789ABCDEF".charAt(b[i] & 0xF));
    }
    return s.toString();
  }

}
