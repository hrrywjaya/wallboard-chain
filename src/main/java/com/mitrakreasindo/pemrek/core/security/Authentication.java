package com.mitrakreasindo.pemrek.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mitrakreasindo.pemrek.core.service.impl.BcaAuthenticationProvider;

//import com.mitrakreasindo.pemrek.core.service.impl.BcaAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class Authentication extends WebSecurityConfigurerAdapter
{

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private BcaAuthenticationProvider authenticationProvider;
	
	@Bean
	public PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}
		
	@Override
	protected void configure (AuthenticationManagerBuilder auth) throws Exception
	{
		auth.authenticationProvider(authenticationProvider);
//		auth
//			.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean () throws Exception
	{
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure (HttpSecurity http) throws Exception
	{
		http.cors().and().authorizeRequests().antMatchers("/login").permitAll().antMatchers("/oauth/**").permitAll().anyRequest()
				.authenticated().and().formLogin().permitAll();
	}

}
