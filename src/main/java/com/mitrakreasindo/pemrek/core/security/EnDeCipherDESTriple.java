package com.mitrakreasindo.pemrek.core.security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class EnDeCipherDESTriple
{
  private SecretKey desKey;
  
  public EnDeCipherDESTriple(byte[] key)
    throws Exception
  {
    createCipher(key);
  }
  
  public void createCipher(byte[] desKeyData)
    throws Exception
  {
    if ((desKeyData.length != 16) && (desKeyData.length != 24)) {
      throw new Exception("Length not valid :" + desKeyData.length);
    }
    byte[] key = new byte[24];
    if (desKeyData.length == 16)
    {
      for (int za = 0; za < 16; za++) {
        key[za] = desKeyData[za];
      }
      for (int za = 0; za < 8; za++) {
        key[(za + 16)] = desKeyData[za];
      }
    }
    if (desKeyData.length == 24) {
      for (int za = 0; za < 24; za++) {
        key[za] = desKeyData[za];
      }
    }
    DESedeKeySpec desKeySpec = new DESedeKeySpec(key);
    SecretKeyFactory keyFactory = null;
    keyFactory = SecretKeyFactory.getInstance("DESede");
    this.desKey = keyFactory.generateSecret(desKeySpec);
  }
  
  public byte[] encryptECB(byte[] cleartext)
  {
    byte[] ciphertext = null;
    try
    {
      Cipher desCipher = Cipher.getInstance("DESede/ECB/NoPadding");
      desCipher.init(1, this.desKey);
      ciphertext = desCipher.doFinal(cleartext);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}catch (InvalidKeyException localInvalidKeyException) {}catch (NoSuchPaddingException localNoSuchPaddingException) {}catch (BadPaddingException localBadPaddingException) {}catch (IllegalBlockSizeException localIllegalBlockSizeException) {}catch (IllegalStateException localIllegalStateException) {}
    return ciphertext;
  }
  
  public byte[] decryptECB(byte[] ciphertext)
  {
    byte[] cleartext = null;
    try
    {
      Cipher desCipher = Cipher.getInstance("DESede/ECB/NoPadding");
      desCipher.init(2, this.desKey);
      cleartext = desCipher.doFinal(ciphertext);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}catch (InvalidKeyException localInvalidKeyException) {}catch (NoSuchPaddingException localNoSuchPaddingException) {}catch (BadPaddingException localBadPaddingException) {}catch (IllegalBlockSizeException localIllegalBlockSizeException) {}catch (IllegalStateException localIllegalStateException) {}
    return cleartext;
  }
  
  public byte[] encryptCBC(byte[] cleartext)
    throws Exception
  {
    byte[] ciphertext = null;
    byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0 };
    AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
    
    Cipher desCipher = Cipher.getInstance("DESede/CBC/NoPadding");
    if (this.desKey == null) {
      throw new Exception("desKey = NULL");
    }
    desCipher.init(1, this.desKey, paramSpec);
    ciphertext = desCipher.doFinal(cleartext);
    iv = desCipher.getIV();
    return ciphertext;
  }
  
  public byte[] decryptCBC(byte[] ciphertext)
    throws Exception
  {
    byte[] cleartext = null;
    byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0 };
    
    IvParameterSpec paramSpec = new IvParameterSpec(iv);
    
    Cipher desCipher = Cipher.getInstance("DESede/CBC/NoPadding");
    if (this.desKey == null) {
      throw new Exception("desKey = NULL");
    }
    desCipher.init(2, this.desKey, paramSpec);
    cleartext = desCipher.doFinal(ciphertext);
    iv = paramSpec.getIV();
    return cleartext;
  }
  
  public String cleanData(String orig)
  {
    StringBuffer buff = new StringBuffer();
    char[] chars = orig.toCharArray();
    for (int za = 0; za < chars.length; za++)
    {
      char tmp = chars[za];
      if (tmp != 0) {
        buff.append(tmp);
      }
    }
    return buff.toString();
  }
  
  public String paddData(String orig)
  {
    StringBuffer buff = new StringBuffer();
    buff.append(orig);
    int paddSize = 0;
    if (orig.length() % 8 != 0) {
      paddSize = 8 - orig.length() % 8;
    }
    for (int za = 0; za < paddSize; za++) {
      buff.append('\000');
    }
    return buff.toString();
  }
  
  public String encrypt(String data)
  {
    String retval = "";
    byte[] encrypted = encryptECB(paddData(data).getBytes());
    retval = Util.toHexString(encrypted);
    return retval;
  }
  
  public String decrypt(String data)
  {
    String retval = "";
    byte[] decrypted = decryptECB(Util.fromHexString(data));
    retval = cleanData(new String(decrypted));
    return retval;
  }
}