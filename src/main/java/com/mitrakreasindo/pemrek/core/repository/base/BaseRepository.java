package com.mitrakreasindo.pemrek.core.repository.base;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import com.mitrakreasindo.pemrek.core.model.Active;

/**
 * @author miftakhul
 *
 * @param <T>
 */
@NoRepositoryBean
public interface BaseRepository<T, ID> extends Repository<T, ID>
{
	
	List<T> findByActiveFlag(Active active);
	
	Page<T> findByActiveFlag(Pageable pageable, Active active);
	
	T findByIdAndActiveFlag(ID id, Active active);
	
	T save(T data);
	
	Iterable<T> saveAll(Iterable<T> datas);
			
	boolean existsByIdAndActiveFlag(ID id, Active active);
	
	Long count();
	
}
