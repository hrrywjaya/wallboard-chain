package com.mitrakreasindo.pemrek.core.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.service.AccountService;

@RestController
@RequestMapping("/capi/users")
public class UserController
{
	
//	@Autowired
//	private UserService userService;
	

	@Autowired
	private AccountService userService;
	
	@GetMapping("/me")
	public Principal me(Principal principal)
	{
		return principal;
	}
	
	@GetMapping("/me/info")
	public Account user(Principal principal) {
		return userService.findByUsername(principal.getName());
	}
	
	@GetMapping("/me/info/test")
	public Account usertest() {
		return userService.getCurrentUser();
	}
	
//	@GetMapping
//	public Page<Account> accounts(Pageable pageable) {
//		return userService.findAll(pageable);
//	}
//	
//	@PostMapping
//	public Account saveAccount(@RequestBody @Valid Account account) {
//		return userService.save(account);
//	}
//	
//	@PutMapping("/{id}")
//	public Account updateAccount(@PathVariable("id") String id, @RequestBody @Valid Account account) {
//		return userService.update(id, account);
//	}
	
	
}
