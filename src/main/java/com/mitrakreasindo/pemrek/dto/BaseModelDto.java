package com.mitrakreasindo.pemrek.dto;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mitrakreasindo.pemrek.core.model.Active;

import lombok.Data;

@Data
public class BaseModelDto {

	private String id;
	@CreatedDate
	private Date createdDate;
	@CreatedBy
	private String createdBy;
	@LastModifiedDate
	private Date updatedDate;
	@LastModifiedBy
	private String updatedBy;
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Active activeFlag = Active.ACTIVE;

}
