package com.mitrakreasindo.pemrek.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class MasterDataTolakDto extends BaseModelDto {

	// @JsonIgnore
	// private List<TolakDto> tolakDtos;

	private String title;
	
	private String description;

}
