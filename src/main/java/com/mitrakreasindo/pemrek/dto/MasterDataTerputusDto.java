package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class MasterDataTerputusDto extends BaseModelDto {

	private String title;
	
	private String description;

}
