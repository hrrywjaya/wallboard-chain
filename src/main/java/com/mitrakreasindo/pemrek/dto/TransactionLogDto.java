package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class TransactionLogDto
{

	private String noRef;
	private VerificationIdentityLogDto vi;
	
}
