package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class TransactionLogLastAgentDto
{

	private String noRef;
	private String agentName; 
	private MasterDataLogDataDto vi;
	
}
