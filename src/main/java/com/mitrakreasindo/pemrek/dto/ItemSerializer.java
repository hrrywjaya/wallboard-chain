package com.mitrakreasindo.pemrek.dto;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class ItemSerializer extends StdSerializer<BaseModelDto> {

	private static final long serialVersionUID = 1L;

	public ItemSerializer() {
        this(null);
    }
   
    public ItemSerializer(Class<BaseModelDto> t) {
        super(t);
    }
 
    @Override
    public void serialize(
		BaseModelDto value, JsonGenerator jgen, SerializerProvider provider) 
		throws IOException, JsonProcessingException {
  
        jgen.writeStartObject();
		// jgen.writeNumberField("id", value.id);
		jgen.writeStringField("id", value.getId());
		// jgen.writeString(value.getId());
        jgen.writeEndObject();
    }

	
}
