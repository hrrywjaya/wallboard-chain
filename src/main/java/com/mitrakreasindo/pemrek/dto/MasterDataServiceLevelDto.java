package com.mitrakreasindo.pemrek.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class MasterDataServiceLevelDto extends BaseModelDto {

	private String title;
	
	private String description;

}
