package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class MasterDataLogDataDto
{

	private MasterDataLogDto foto;
	private MasterDataLogDto ktp;
	private MasterDataLogDto tandaTangan;
	private MasterDataLogDto npwp;
	
}
