package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class MasterDataLogChecksumDto 
{
	
	private MasterDataLogDataDto data;
	private String checksum;

}
