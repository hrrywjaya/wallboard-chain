package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class VerificationIdentityLogDto
{

	private MasterDataLogChecksumDto awal;
	private MasterDataLogChecksumDto akhir;
	private MasterDataLogDataDto perubahan;
	
}
