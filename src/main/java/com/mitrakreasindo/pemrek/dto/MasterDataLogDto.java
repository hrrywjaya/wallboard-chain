package com.mitrakreasindo.pemrek.dto;

import lombok.Data;

@Data
public class MasterDataLogDto
{

	private String title;
	private String data;
	private String checked;
	
}
