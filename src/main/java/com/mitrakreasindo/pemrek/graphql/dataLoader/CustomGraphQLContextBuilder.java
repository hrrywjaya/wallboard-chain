package com.mitrakreasindo.pemrek.graphql.dataLoader;

import org.dataloader.BatchLoaderContextProvider;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderOptions;
import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.kickstart.execution.context.DefaultGraphQLContext;
import graphql.kickstart.execution.context.GraphQLContext;
// import graphql.servlet.context.DefaultGraphQLContext;
import graphql.servlet.context.DefaultGraphQLServletContext;
import graphql.servlet.context.DefaultGraphQLWebSocketContext;
// import graphql.servlet.context.GraphQLContext;
// import graphql.servlet.context.GraphQLContextBuilder;
import graphql.servlet.context.GraphQLServletContextBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;

import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.graphql.datafetcher.GraphQLDataFetchers;
import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.service.TransaksiService;

@Component
// @RequiredArgsConstructor
public class CustomGraphQLContextBuilder implements GraphQLServletContextBuilder, BatchLoaderContextProvider {

    private final TransaksiService transaksiService;

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    public CustomGraphQLContextBuilder(TransaksiService ts) {
        this.transaksiService = ts;
    }

    // @Override
    // public GraphQLContext build(HttpServletRequest req, HttpServletResponse response) {
    //     return DefaultGraphQLServletContext.createServletContext(buildDataLoaderRegistry(), null).with(req)
    //             .with(response).build();
    // }

    // @Override
    // public GraphQLContext build() {
    //     return new DefaultGraphQLContext(buildDataLoaderRegistry(), null);
    // }

    // @Override
    // public GraphQLContext build(Session session, HandshakeRequest request) {
    //     return DefaultGraphQLWebSocketContext.createWebSocketContext(buildDataLoaderRegistry(), null).with(session)
    //             .with(request).build();
    // }

    @Override
    public GraphQLContext build(HttpServletRequest req, HttpServletResponse response) {
        return DefaultGraphQLServletContext.createServletContext(buildDataLoaderRegistry(), null).with(req)
                .with(response).build();
    }

    @Override
    public GraphQLContext build() {
        return new DefaultGraphQLContext(buildDataLoaderRegistry(), null);
    }

    @Override
    public GraphQLContext build(Session session, HandshakeRequest request) {
        return DefaultGraphQLWebSocketContext.createWebSocketContext(buildDataLoaderRegistry(), null).with(session)
                .with(request).build();
    }

    private DataLoaderRegistry buildDataLoaderRegistry() {
        DataLoaderRegistry dataLoaderRegistry = new DataLoaderRegistry();
        
        DataLoaderOptions loaderOptions = DataLoaderOptions.newOptions().setBatchLoaderContextProvider(this);
        // DataLoader<String, ZeRef> zeRefLoader = DataLoader.newDataLoader(graphQLDataFetchers.zeRefBatchLoader(), loaderOptions);
        // dataLoaderRegistry.register("zeRefsDataLoader", zeRefLoader);
        DataLoader<String, Foto> fotoLoader = DataLoader.newDataLoader(graphQLDataFetchers.fotoBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("fotosDataLoader", fotoLoader);

        DataLoader<String, Referensi> referensiLoader = DataLoader.newDataLoader(graphQLDataFetchers.referensiBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("referensisDataLoader", referensiLoader);

        DataLoader<String, Account> accountLoader = DataLoader.newDataLoader(graphQLDataFetchers.accountBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("accountsDataLoader", accountLoader);

        DataLoader<String, Acl> aclLoader = DataLoader.newDataLoader(graphQLDataFetchers.aclBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("aclsDataLoader", aclLoader);

        DataLoader<String, Menu> childLoader = DataLoader.newDataLoader(graphQLDataFetchers.childBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("childsDataLoader", childLoader);

        DataLoader<String, Menu> childHaveParentLoader = DataLoader.newDataLoader(graphQLDataFetchers.childHaveParentBatchLoader(), loaderOptions);
        dataLoaderRegistry.register("childsHaveParentDataLoader", childHaveParentLoader);
        return dataLoaderRegistry;
    }

    static class SecurityContext {

        static SecurityContext newSecurityContext() {
            return null;
        }

        Object getToken() {
            return null;
        }
    }

    SecurityContext securityCtx = SecurityContext.newSecurityContext();

    BatchLoaderContextProvider contextProvider = new BatchLoaderContextProvider() {
        @Override
        public Object getContext() {
            return securityCtx;
        }
    };

    @Override
    public Object getContext() {
        // TODO Auto-generated method stub
        return null;
    }

    
}