package com.mitrakreasindo.pemrek.graphql.resolver;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.graphql.datafetcher.GraphQLDataFetchers;
import com.mitrakreasindo.pemrek.graphql.dto.MenuFlatDto;
import com.mitrakreasindo.pemrek.graphql.dto.PaginationDto;
import com.mitrakreasindo.pemrek.graphql.dto.TransaksiDto;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.EddApproval;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Jabber;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;
import com.mitrakreasindo.pemrek.model.MasterDataChannel;
import com.mitrakreasindo.pemrek.model.MasterDataCountry;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.model.MasterDataTransactionCode;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.Pesan;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.repository.specs.MasterDataBranchSpecification;
import com.mitrakreasindo.pemrek.repository.specs.SearchCriteria;
import com.mitrakreasindo.pemrek.repository.specs.SearchOperation;
import com.mitrakreasindo.pemrek.service.AccountService;
import com.mitrakreasindo.pemrek.service.AuditLogUserService;
import com.mitrakreasindo.pemrek.service.CallLogService;
// import com.mitrakreasindo.pemrek.service.EddApprovalService;
import com.mitrakreasindo.pemrek.service.FotoService;
import com.mitrakreasindo.pemrek.service.JabberService;
import com.mitrakreasindo.pemrek.service.MasterDataBranchService;
import com.mitrakreasindo.pemrek.service.MasterDataChannelService;
import com.mitrakreasindo.pemrek.service.MasterDataCountryService;
import com.mitrakreasindo.pemrek.service.MasterDataPekerjaanService;
import com.mitrakreasindo.pemrek.service.MasterDataTransactionCodeService;
import com.mitrakreasindo.pemrek.service.MenuService;
import com.mitrakreasindo.pemrek.service.PesanService;
import com.mitrakreasindo.pemrek.service.RoleService;
import com.mitrakreasindo.pemrek.service.TransaksiService;
// import graphql.servlet.context.GraphQLContext;
import graphql.servlet.context.GraphQLServletContext;

import org.dataloader.DataLoader;
import org.modelmapper.ModelMapper;
// import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetcher;
import lombok.RequiredArgsConstructor;
import graphql.schema.DataFetchingEnvironment;

@Component
@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {

    @Resource
    private final TransaksiService transaksiService;

    @Resource
    private final FotoService fotoService;

    @Resource
    private final RoleService roleService;

    @Resource
    private final CallLogService callLogService;

    @Resource
    private final AccountService accountService;

    @Resource
    private final MenuService menuService;
    @Resource
    private final JabberService jabberService;
    @Resource
    private final MasterDataPekerjaanService masterDataPekerjaanService;
    @Resource
    private final PesanService pesanService;
    @Resource
    private final MasterDataBranchService masterDataBranchService;
    @Resource
    private final AuditLogUserService auditLogUserService;
    // @Resource
    // private final EddApprovalService eddApprovalService;
 
    @Resource
    private final MasterDataCountryService masterDataCountryService;
    @Resource
    private final MasterDataChannelService masterDataChannelService;
    @Resource
    private final MasterDataTransactionCodeService masterDataTransactionCodeService;

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    @Value("${jabber.domain}")
    private String jabberDomain;
    @Value("${jabber.cucm}")
    private String jabberCucm;
    @Value("${jabber.cti}")
    private String jabberCti;
    @Value("${jabber.tftp}")
    private String jabberTftp;
    @Value("${jabber.password}")
    private String jabberPassword;
    @Value("${jabber.finessePassword}")
    private String finessePassword;

    @Autowired
    @Qualifier("mapperNormal")
    private ModelMapper modelMapperNormal;

    public List<Transaksi> allTransaksis() {
        return transaksiService.findAll();
        // return graphQLDataFetchers.allTransaksisDataFetcher;
    }

    // public List<TransaksiDto> allTransaksis() {

    // // return transaksiService.findAll();

    // List<TransaksiDto> tbDtos = new ArrayList<TransaksiDto>();
    // // transaksiService.findAll().stream().parallel().forEach(e -> {
    // // TransaksiDto tbDto = modelMapper.map(e, TransaksiDto.class);
    // // tbDtos.add(tbDto);
    // // });
    // List<Transaksi> ts =
    // transaksiService.findAll().stream().parallel().collect(Collectors.toList());
    // ts.forEach(e -> {
    // TransaksiDto tbDto = modelMapper.map(e, TransaksiDto.class);
    // tbDtos.add(tbDto);
    // });
    // return tbDtos;
    // }

    // public CompletableFuture<List<Transaksi>>
    // allTransaksis(DataFetchingEnvironment dfe) {
    // List<Transaksi> t = transaksiService.findAll();
    // List<String> transaksiIds = t.stream().map(e ->
    // e.getId()).collect(Collectors.toList());
    // final DataLoader<String, Transaksi> dataLoader = ((GraphQLContext)
    // dfe.getContext())
    // .getDataLoaderRegistry().get()
    // .getDataLoader("allTransaksiis");

    // return dataLoader.loadMany(transaksiIds);
    // // return graphQLDataFetchers.zeRefsDataFetcher;
    // }

    // public List<ZeRef> allZeRefs() {
    // return zeRefService.findAll();
    // }

    public List<Role> allRoles() {
        return roleService.findAll();
    }

    public List<Role> findRoleByName() {
        return roleService.findRoleByName();
    }

    // public Optional<Transaksi> findOneTransaksiByZeRef(String noRef,
    // DataFetchingEnvironment env) {
    // // DatabaseSecurityCtx ctx = env.getContext();

    // return transaksiService.findOneByZeRefNoRef(noRef);
    // }

    public List<CallLog> allCallLogs() {
        return callLogService.findAll();
        // return graphQLDataFetchers.allTransaksisDataFetcher;
    }

    public Optional<Foto> findLastPhotoByTransaksiId(String id, DataFetchingEnvironment env) {
        // DatabaseSecurityCtx ctx = env.getContext();

        return fotoService.findFirstByTransaksiIdOrderByCreatedDateDesc(id);
    }

    public Optional<Transaksi> findOneTransaksiByReferensi(String noRef, DataFetchingEnvironment env) {

        return transaksiService.findOneTransaksiByReferensi(noRef);
    }

    public List<Account> allAccounts() {
        return accountService.findAll();
    }

    public Optional<Account> findOneAccountByUsername(String username) {
        return accountService.findOneByUsername(username);
    }
    
    public Optional<Account> findOneAccountFullnameByUsername(String username) {
        return accountService.findOneFullnameByUsername(username);
    }

    public Optional<Transaksi> transaksi(String id) {
        return transaksiService.findById(id);
    }

    public Account me(DataFetchingEnvironment env) {
        // SecurityContext context = SecurityContextHolder.getContext();
        // Authentication auth = context.getAuthentication();
        // Principal principal = ((GraphQLContext)
        // context).getHttpServletRequest().get().getUserPrincipal();

        // DatabaseSecurityCtx ctx = env.getContext();

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return accountService.findByUsername(username);
    }

    // public Page<Account> allAccountsCursor(String username, Active activeFlag,
    // Optional<Integer> page,
    // Optional<Integer> size, Optional<String> orderBy, Optional<String> direction,
    // DataFetchingEnvironment env) {
    // Integer _page = page.isPresent() ? page.get() : 0;
    // Integer _size = size.isPresent() ? size.get() : 10;
    // // String _orderBy = orderBy.orElse("username");
    // Direction _direction = direction
    // // .map(v -> v.toLowerCase())
    // .map(v -> v.equalsIgnoreCase("desc") ? Direction.DESC :
    // Direction.ASC).orElse(Direction.ASC);
    // Sort _sort = Sort.by(_direction, orderBy.orElse("username"));
    // // Sort _sort = Sort.by(Direction.ASC, "username");
    // // , Optional<Integer> sort
    // // Defining sort expressions
    // // Sort.by("firstname").ascending().and(Sort.by("lastname").descending());
    // // Defining sort expressions using the type-safe API
    // // TypedSort<Person> person = Sort.sort(Person.class);
    // // TypedSort<Person> sort = person.by(Person::getFirstname).ascending()
    // // .and(person.by(Person::getLastname).descending());
    // // Sort sort = Sort.by("role.name").ascending();
    // Pageable pageable = PageRequest.of(_page, _size, _sort);
    // return accountService.pagination(username, activeFlag, pageable);
    // }

    public Page<Account> allAccountsCursor(String username, Active activeFlag, Optional<Integer> page,
            Optional<Integer> size, Optional<String> orderBy, Optional<String> direction, DataFetchingEnvironment env) {
        Integer _page = page.isPresent() ? page.get() : 0;
        Integer _size = size.isPresent() ? size.get() : 10;

        Direction _direction = Optional.ofNullable(direction).isPresent()
                ? (direction.get().equalsIgnoreCase("desc") ? Direction.DESC : Direction.ASC)
                : Direction.ASC;
        String _orderBy = Optional.ofNullable(orderBy).isPresent() ? orderBy.get() : "username";
        Sort _sort = Sort.by(_direction, _orderBy);
        Pageable pageable = PageRequest.of(_page, _size, _sort);
        return accountService.pagination(username, activeFlag, pageable);
    }

    public Page<Account> allAccountsCursors(String username, Active activeFlag, Optional<Integer> page,
            Optional<Integer> size, Optional<String> orderBy, Optional<String> direction, DataFetchingEnvironment env) {
        Integer _page = page.isPresent() ? page.get() : 0;
        Integer _size = size.isPresent() ? size.get() : 10;

        Direction _direction = Optional.ofNullable(direction).isPresent()
                ? (direction.get().equalsIgnoreCase("desc") ? Direction.DESC : Direction.ASC)
                : Direction.ASC;
        String _orderBy = Optional.ofNullable(orderBy).isPresent() ? orderBy.get() : "username";
        Sort _sort = Sort.by(_direction, _orderBy);
        Pageable pageable = PageRequest.of(_page, _size, _sort);
        return accountService.paginations(username, activeFlag, pageable);
    }
    
    public Account account(String id, DataFetchingEnvironment env) {
        Account a = accountService.findById(id).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));
        return a;
    }

    // public Page<Role> allRolesCursor(String name, Optional<Integer> page,
    // Optional<Integer> size,
    // Optional<String> orderBy, Optional<String> direction, DataFetchingEnvironment
    // env) {
    // Integer _page = page.isPresent() ? page.get() : 0;
    // Integer _size = size.isPresent() ? size.get() : 10;
    // Direction _direction = direction.map(v -> v.equalsIgnoreCase("desc") ?
    // Direction.DESC : Direction.ASC)
    // .orElse(Direction.ASC);
    // Sort _sort = Sort.by(_direction, orderBy.orElse("name"));
    // Pageable pageable = PageRequest.of(_page, _size, _sort);
    // return roleService.pagination(name, pageable);
    // }

    public Page<Role> allRolesCursor(String name, Optional<Integer> page, Optional<Integer> size,
            Optional<String> orderBy, Optional<String> direction, DataFetchingEnvironment env) {
        Integer _page = page.isPresent() ? page.get() : 0;
        Integer _size = size.isPresent() ? size.get() : 10;
        Direction _direction = Optional.ofNullable(direction).isPresent()
                ? (direction.get().equalsIgnoreCase("desc") ? Direction.DESC : Direction.ASC)
                : Direction.ASC;
        String _orderBy = Optional.ofNullable(orderBy).isPresent() ? orderBy.get() : "name";
        Sort _sort = Sort.by(_direction, _orderBy);
        Pageable pageable = PageRequest.of(_page, _size, _sort);
        return roleService.pagination(name, pageable);
    }

    public List<Menu> allMenus() {
        return menuService.findAll();
    }

    public List<MenuFlatDto> allFlatMenus() {
        // Stream<Menu> userEntityStream =
        // StreamSupport.stream(menuService.findAllFlat().spliterator(), false);
        // List<MenuFlatDto> activeUserListDTOs =
        List<MenuFlatDto> results = StreamSupport.stream(menuService.findAllFlat().spliterator(), false)
                .map(MenuFlatDto::new).collect(Collectors.toList());
        // menuService.findAllFlat().stream()
        // .
        return results;
    }

    public Optional<Role> findOneRoleByName(String name) {
        return roleService.findOneByName(name);
    }

    public Optional<Role> role(String id, DataFetchingEnvironment env) {
        return roleService.findById(id);
    }

    public List<CallLog> allTolaksByNoRef(String noRef) {
        return callLogService.allCallLogsByNoRef(noRef);
    }

    public List<CallLog> allTerputusesByNoRef(String noRef) {
        return callLogService.allTerputusesByNoRef(noRef);
    }

    public List<Account> allAccountByRoleId(String id, DataFetchingEnvironment env) {

        return accountService.allAccountByRoleId(id);
    }

    public Optional<Jabber> jabber(String ipAddress, DataFetchingEnvironment env) {
        // // GraphQLContext ctx = env.getContext();
        // GraphQLServletContext ctx = (GraphQLServletContext) env.getContext();
        // HttpServletRequest request = ctx.getHttpServletRequest();
        // String remoteAddr = request.getHeader("X-Forwarded-For");
        // // String remoteAddr = request.getRemoteAddr() != null ? request.getRemoteAddr() : request.getRemoteHost();
        // // String remoteHost = request.getRemoteHost();
        // // String remoteUser = request.getRemoteUser();

        // // Optional<Jabber> jabber = jabberService.findByIpAddress(remoteAddr);
        Optional<Jabber> jabber = jabberService.findByIpAddress(ipAddress);
        jabber.ifPresent((e) -> {
            e.setDomain(this.jabberDomain);
            e.setCucm(this.jabberCucm);
            e.setCti(this.jabberCti);
            e.setTftp(this.jabberTftp);
            e.setPassword(this.jabberPassword);
            e.setFinessePassword(this.finessePassword);
        });

        return jabber;
    }

    public Optional<MasterDataPekerjaan> findOneMasterDataPekerjaanByDescription(String description) {
        return masterDataPekerjaanService.findOneByDescription(description);
    }

    public List<Pesan> allPesan() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        // return pesanService.findByCreatedDate(new Date());
        return pesanService.findByCreatedDate(df.format(new Date()));
    }

    // public Page<MasterDataBranch> allBranchsCursor(String branchName, Optional<Integer> page, Optional<Integer> size,
    //         Optional<String> orderBy, Optional<String> direction, DataFetchingEnvironment env) {
    //     Integer _page = page.isPresent() ? page.get() : 0;
    //     Integer _size = size.isPresent() ? size.get() : 10;
    //     // Direction _direction = Optional.ofNullable(direction).isPresent()
    //     // ? (direction.get().equalsIgnoreCase("desc") ? Direction.DESC : Direction.ASC)
    //     // : Direction.ASC;
    //     // String _orderBy = Optional.ofNullable(orderBy).isPresent() ? orderBy.get() :
    //     // "branchName";
    //     // String _orderBy = orderBy.orElse("branchName");
    //     // Sort _sort = Sort.by(_direction, _orderBy);
    //     Sort _sort = Sort.by(Optional.ofNullable(direction).filter(x -> x.get().equalsIgnoreCase("desc"))
    //             .map(x -> Direction.DESC).orElse(Direction.ASC),
    //             Optional.ofNullable(orderBy).map(x -> x.get()).orElse("branchName"));
    //     Pageable pageable = PageRequest.of(_page, _size, _sort);
    //     return masterDataBranchService.pagination(branchName, pageable);
    // }

    public PaginationDto<MasterDataBranch> allBranchsCursor(final String branchName, final Optional<Integer> page,
            final Optional<Integer> size, final Optional<String> orderBy, final Optional<String> direction,
            final DataFetchingEnvironment env) {
        final Integer _page = page.isPresent() ? page.get() : 0;
        final Integer _size = size.isPresent() ? size.get() : 10;
        final Sort _sort = Sort.by(Optional.ofNullable(direction).filter(x -> x.get().equalsIgnoreCase("desc"))
                .map(x -> Direction.DESC).orElse(Direction.ASC),
                Optional.ofNullable(orderBy).map(x -> x.get()).orElse("branchName"));
        final Pageable pageable = PageRequest.of(_page, _size, _sort);

        final Page<MasterDataBranch> entityPage = masterDataBranchService.pagination(branchName, pageable);
        LinkedHashMap<String, String> orders = entityPage.getSort()
            .stream()
            // .forEach((Order key) -> result.put(key.getProperty(),key.getDirection().name()));
            .collect(Collectors.toMap(
                        order -> order.getProperty(), //KeyMapper
                        order -> order.getDirection().name(), //Value Mapper
                        (first, second) -> first, //Merge function
                        LinkedHashMap::new)); //LinkedHashMap
    
        final List<MasterDataBranch> b = entityPage.getContent().stream().collect(Collectors.toList());
        return new PaginationDto<>(entityPage, b, orders);
        // .collect(Collectors.toList())
    }

    public Optional<MasterDataBranch> findOneMasterDataBranchByBranchCode(final String branchCode) {
        return masterDataBranchService.findOneByBranchCode(branchCode);
    }

    public List<CallLog> allTolaksByQrNik(String qrNik) {
        return callLogService.allTolaksByQrNik(qrNik);
    }

    public List<CallLog> allEddApprovals() {
        return callLogService.allEddApprovals();
    }

    public List<CallLog> findApprovalReportx(String startDate,String endDate,String channel,String rekening,String cabang,String agent,String status){
      return callLogService.findApprovalReport(startDate, endDate, channel, rekening, cabang, agent, status);
    }

    public List<CallLog> findApprovalReportStatusWaiting(String startDate,String endDate,String channel,String rekening,String cabang,String agent){
      return callLogService.findApprovalReportStatusWaiting(startDate, endDate, channel, rekening, cabang, agent);
    }
    public List<CallLog> findApprovalReportStatusAll(String startDate,String endDate,String channel,String rekening,String cabang,String agent){
        return callLogService.findApprovalReportStatusAll(startDate, endDate, channel, rekening, cabang, agent);
    }
    // public List<CallLog> b(String startDate,String endDate,String channel,String rekening,String cabang,String agent,String status){
    //   return callLogService.findApprovalReport(startDate, endDate, channel, rekening, cabang, agent, status);
    // }

    public PaginationDto<MasterDataBranch> allBranchsCustomCursor(final List<LinkedHashMap<String, Object>> criteria,
            final Optional<Integer> page, final Optional<Integer> size, final Optional<String> orderBy,
            final Optional<String> direction, final DataFetchingEnvironment env) {
        final Integer _page = page.isPresent() ? page.get() : 0;
        final Integer _size = size.isPresent() ? size.get() : 10;

        final Sort _sort = Sort.by(Optional.ofNullable(direction).filter(x -> x.get().equalsIgnoreCase("desc"))
                .map(x -> Direction.DESC).orElse(Direction.ASC),
                Optional.ofNullable(orderBy).map(x -> x.get()).orElse("branchName"));
        final Pageable pageable = PageRequest.of(_page, _size, _sort);

        MasterDataBranchSpecification masterDataBranchSpecification = new MasterDataBranchSpecification();

        criteria.stream().forEach(property -> {

            SearchOperation operation = SearchOperation.valueOf(property.get("operation").toString().toUpperCase());
            masterDataBranchSpecification.add(
                    new SearchCriteria(property.get("property").toString(), property.get("value").toString(), operation));
        });

        final Page<MasterDataBranch> entityPage = masterDataBranchService
                .paginationCustom(masterDataBranchSpecification, pageable);

        LinkedHashMap<String, String> orders = entityPage.getSort().stream()
                .collect(Collectors.toMap(order -> order.getProperty(), // KeyMapper
                        order -> order.getDirection().name(), // Value Mapper
                        (first, second) -> first, // Merge function
                        LinkedHashMap::new)); // LinkedHashMap

        final List<MasterDataBranch> b = entityPage.getContent().stream().collect(Collectors.toList());
        return new PaginationDto<>(entityPage, b, orders);
    }

    public List<MasterDataCountry> allMasterDataCountries() {
        return masterDataCountryService.findAll();
    }    

    public Optional<MasterDataCountry> findOneMasterDataCountryByCountryCodeIso(final String countryCodeIso) {
        return masterDataCountryService.findOneByCountryCodeIso(countryCodeIso);
    }

    public List<MasterDataChannel> allMasterDataChannels() {
        return masterDataChannelService.findAll();
    }    

    public List<MasterDataTransactionCode> allMasterDataTransactionCodes() {
        return masterDataTransactionCodeService.findAll();
    }    
    
}