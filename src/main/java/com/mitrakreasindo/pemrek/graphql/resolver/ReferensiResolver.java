package com.mitrakreasindo.pemrek.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.mitrakreasindo.pemrek.graphql.datafetcher.GraphQLDataFetchers;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.TransaksiService;
import org.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
// import graphql.servlet.context.GraphQLContext;
// import graphql.kickstart.execution.context.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class ReferensiResolver implements GraphQLResolver<Referensi> {

    // @Resource
    private TransaksiService transaksiService;

    public ReferensiResolver(TransaksiService ts) {
        this.transaksiService = ts;
    }

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

}