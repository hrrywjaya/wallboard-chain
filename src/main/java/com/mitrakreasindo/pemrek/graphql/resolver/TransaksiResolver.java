package com.mitrakreasindo.pemrek.graphql.resolver;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.mitrakreasindo.pemrek.graphql.datafetcher.GraphQLDataFetchers;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.ReferensiService;
import org.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
// import graphql.servlet.context.GraphQLContext;
import graphql.kickstart.execution.context.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;

@Component
public class TransaksiResolver implements GraphQLResolver<Transaksi> {

    @Resource
    private ReferensiService referensiService;

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    // public List<ZeRef> zeRefs(Transaksi transaksi) {
    // return zeRefService.findAllTransaksi?F
    // }

    // public CompletableFuture<List<ZeRef>> zeRefs(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // // return
    // //
    // this.transaksiRepository.findAll().stream().limit(count).collect(Collectors.toList());
    // // return zeRefService.findByTransaksiId(transaksi.getId());
    // // return zeRefService.findByTransaksiId(transaksi.getId());
    // final DataLoader<String, ZeRef> dataloader = ((GraphQLContext)
    // dfe.getContext()).getDataLoaderRegistry().get()
    // .getDataLoader("zeRefsDataLoader");

    // List<String> zeRefsId =
    // zeRefService.findByTransaksiId(transaksi.getId()).stream().map(e ->
    // e.getId())
    // .collect(Collectors.toList());
    // // List<String> zeRefsId = new ArrayList<String>();
    // // zeRefsId.addAll(
    // //
    // transaksi.getZeRefs().stream().map(e->e.getId()).collect(Collectors.toList())
    // // );

    // return dataloader.loadMany(zeRefsId);
    // }

    // public CompletableFuture<List<ZeRef>> zeRefs2(Transaksi transaksi, DataFetchingEnvironment dfe) {
        
    //     Transaksi t = dfe.getSource();
    //     List<String> zeRefsIds = t.getZeRefs().stream().map(e -> e.getId()).collect(Collectors.toList());
    //     // Context ctx = environment.getContext();
    //     // Context ctx = environment.getContext();
    //     final DataLoader<String, ZeRef> dataLoader = dfe.getDataLoader("zeRefsDataLoader");

    //     return dataLoader.loadMany(zeRefsIds);
    // }

    // public CompletableFuture<List<ZeRef>> zeRefs(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // // Transaksi t = dfe.getSource();
    // // Transaksi t = dfe.getRoot();
    // // List<String> zeRefsIds = t.getZeRefs().stream().map(e ->
    // // e.getId()).collect(Collectors.toList());
    // List<String> zeRefsIds =
    // zeRefService.findByTransaksiId(transaksi.getId()).stream().parallel()
    // .map(e -> e.getId()).collect(Collectors.toList());
    // final DataLoader<String, ZeRef> dataLoader = ((GraphQLContext)
    // dfe.getContext()).getDataLoaderRegistry().get()
    // .getDataLoader("countries");

    // return dataLoader.loadMany(zeRefsIds);
    // // return graphQLDataFetchers.zeRefsDataFetcher;
    // }s

    // public CompletableFuture<String> getName(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // final DataLoader<String, String> dataloader = ((GraphQLContext)
    // dfe.getContext()).getDataLoaderRegistry().get()
    // .getDataLoader("zeRefsDataLoader");

    // return dataloader.load(transaksi.getId());
    // }

    // public CompletableFuture<Transaksi> getName(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // final DataLoader<String, Transaksi> dataloader = ((GraphQLContext)
    // dfe.getContext())
    // .getDataLoaderRegistry().get()
    // .getDataLoader("transaksiDataLoader");

    // return dataloader.load(transaksi.getId());
    // }

    // public CompletableFuture<List<ZeRef>> zeRefs(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // final DataLoader<String, ZeRef> dataloader = ((GraphQLContext)
    // dfe.getContext()).getDataLoaderRegistry().get()
    // .getDataLoader("transaksiDataLoader");

    // List<String> ss = transaksi.getZeRefs().stream().map(e ->
    // e.getId()).collect(Collectors.toList());

    // return dataloader.loadMany(ss);
    // }

    // public CompletableFuture<List<ZeRef>> zeRefs(Transaksi transaksi, DataFetchingEnvironment dfe) {
    //     final DataLoader<String, ZeRef> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
    //             .getDataLoader("zeRefsDataLoader");

                
    //     Transaksi t = dfe.getSource();
    //     List<String> ids = t.getZeRefs().stream().map(e -> e.getId()).collect(Collectors.toList());
    //     // List<String> ids =  new ArrayList<String>();
    //     // List<String> ids = t.getZeRefs().stream().map(e -> e.getId()).collect(Collectors.toList());

    //     // List<Transaksi> transaksis = dfe.getSource();
    //     // List<String> ids = transaksis.stream().map(e -> e.getId()).collect(Collectors.toList());
    //     // List<String> ss = transaksi.getZeRefs().stream().map(e ->
    //     // e.getId()).collect(Collectors.toList());
    //     return dataloader.loadMany(ids);
    // }

    // public CompletableFuture<List<String>> getName(Transaksi transaksi,
    // DataFetchingEnvironment dfe) {
    // // final DataLoader<Integer, String> dataloader = ((GraphQLContext)
    // // dfe.getContext())
    // // .getDataLoaderRegistry().get()
    // // .getDataLoader("customerDataLoader");

    // // return dataloader.load(transaksi.getId());
    // // List<Article> articles = DataFetchingEnvironment.getSource(); // source is
    // a list
    // List<Transaksi> articles = dfe.getSource(); // source is a lists
    // List<String> authorIds = articles.stream().map(article ->
    // article.getId()).collect(Collectors.toList());

    // return authorIds;
    // }

    // @Provides
    // DataLoaderRegistry dataLoaderRegistry(BookServiceGrpc.BookServiceFutureStub
    // bookService) {

    // // TODO: Use multibinder to modularize this, or automate this somehow
    // BatchLoader<String, Book> bookBatchLoader =
    // keys -> {
    // ListenableFuture<List<Book>> listenableFuture =
    // Futures.transform(
    // bookService.listBooks(
    // ListBooksRequest.newBuilder()
    // .addAllIds(keys)
    // .setPageSize(keys.size())
    // .build()),
    // resp -> resp.getBooksList(),
    // MoreExecutors.directExecutor());
    // return FutureConverter.toCompletableFuture(listenableFuture);
    // };

    // DataLoaderRegistry registry = new DataLoaderRegistry();
    // registry.register("books", new DataLoader<>(bookBatchLoader));
    // return registry;
    // }

    // BatchLoader<String, Transaksi> userBatchLoader = new BatchLoader<String,
    // Transaksi>() {
    // @Override
    // public CompletionStage<List<Transaksi>> load(List<String> userIds) {
    // return CompletableFuture.supplyAsync(() -> {
    // return userManager.loadUsersById(userIds);
    // });
    // }
    // };

    // //
    // // use this data loader in the data fetchers associated with characters and
    // put them into
    // // the graphql schema (not shown)
    // //
    // DataFetcher heroDataFetcher = new DataFetcher() {
    // @Override
    // public Object get(DataFetchingEnvironment environment) {
    // DataLoader<String, Object> dataLoader =
    // environment.getDataLoader("character");
    // return dataLoader.load("2001"); // R2D2
    // }
    // };

    // DataFetcher friendsDataFetcher = new DataFetcher() {
    // @Override
    // public Object get(DataFetchingEnvironment environment) {
    // StarWarsCharacter starWarsCharacter = environment.getSource();
    // List<String> friendIds = starWarsCharacter.getFriendIds();
    // DataLoader<String, Object> dataLoader =
    // environment.getDataLoader("character");
    // return dataLoader.loadMany(friendIds);
    // }
    // };

    // //
    // // this instrumentation implementation will dispatch all the data loaders
    // // as each level of the graphql query is executed and hence make batched
    // objects
    // // available to the query and the associated DataFetchers
    // //
    // // In this case we use options to make it keep statistics on the batching
    // efficiency
    // //
    // DataLoaderDispatcherInstrumentationOptions options =
    // DataLoaderDispatcherInstrumentationOptions
    // .newOptions().includeStatistics(true);

    // DataLoaderDispatcherInstrumentation dispatcherInstrumentation
    // = new DataLoaderDispatcherInstrumentation(options);

    // //
    // // now build your graphql object and execute queries on it.
    // // the data loader will be invoked via the data fetchers on the
    // // schema fields
    // //
    // GraphQL graphQL = GraphQL.newGraphQL(buildSchema())
    // .instrumentation(dispatcherInstrumentation)
    // .build();

    // //
    // // a data loader for characters that points to the character batch loader
    // //
    // // Since data loaders are stateful, they are created per execution request.
    // //
    // DataLoader<String, Object> characterDataLoader =
    // DataLoader.newDataLoader(characterBatchLoader);

    // //
    // // DataLoaderRegistry is a place to register all data loaders in that needs
    // to be dispatched together
    // // in this case there is 1 but you can have many.
    // //
    // // Also note that the data loaders are created per execution request
    // //
    // DataLoaderRegistry registry = new DataLoaderRegistry();
    // registry.register("character", characterDataLoader);

    // ExecutionInput executionInput = newExecutionInput()
    // .query(getQuery())
    // .dataLoaderRegistry(registry)
    // .build();

    // ExecutionResult executionResult = graphQL.execute(executionInput);

    // DataLoader<String, Transaksi> userLoader = new DataLoader<>(userBatchLoader);

    // CompletionStage<Transaksi> load1 = userLoader.load(1L);

    // userLoader.load(1L).thenAccept(user -> {
    // System.out.println("user = " + user);
    // userLoader.load(user.getInvitedByID()).thenAccept(invitedBy -> {
    // System.out.println("invitedBy = " + invitedBy);
    // });
    // });

    // userLoader.load(2L).thenAccept(user -> {
    // System.out.println("user = " + user);
    // userLoader.load(user.getInvitedByID()).thenAccept(invitedBy -> {
    // System.out.println("invitedBy = " + invitedBy);
    // });
    // });

    // userLoader.dispatchAndJoin();

    // public CompletableFuture<List<CallLog>> callLogs(Transaksi transaksi, DataFetchingEnvironment dfe) {
    //     final DataLoader<String, CallLog> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
    //             .getDataLoader("callLogsDataLoader");
                
    //     Transaksi t = dfe.getSource();
    //     List<String> ids = t.getCallLogs().stream().map(e -> e.getId()).collect(Collectors.toList());

    //     return dataloader.loadMany(ids);
    // }

    

    public CompletableFuture<List<Foto>> fotos(Transaksi transaksi, DataFetchingEnvironment dfe) {
        final DataLoader<String, Foto> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
                .getDataLoader("fotosDataLoader");
                
        Transaksi t = dfe.getSource();
        List<String> ids = t.getFotos().stream().map(e -> e.getId()).collect(Collectors.toList());
        
        return dataloader.loadMany(ids);
    }

    public CompletableFuture<List<Referensi>> referensis(Transaksi transaksi, DataFetchingEnvironment dfe) {
        final DataLoader<String, Referensi> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
                .getDataLoader("referensisDataLoader");
                
        Transaksi t = dfe.getSource();
        List<String> ids = t.getReferensis().stream().map(e -> e.getId()).collect(Collectors.toList());
      
        return dataloader.loadMany(ids);
    }

}