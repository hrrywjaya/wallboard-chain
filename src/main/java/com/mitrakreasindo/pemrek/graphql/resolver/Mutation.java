package com.mitrakreasindo.pemrek.graphql.resolver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.component.ModelPatcher;
import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.graphql.dto.AccountDto;
import com.mitrakreasindo.pemrek.graphql.dto.AccountInput;
import com.mitrakreasindo.pemrek.graphql.dto.AccountUpdate;
import com.mitrakreasindo.pemrek.graphql.dto.AuditLogUserInput;
import com.mitrakreasindo.pemrek.graphql.dto.CallLogInput;
import com.mitrakreasindo.pemrek.graphql.dto.CallLogUpdate;
import com.mitrakreasindo.pemrek.graphql.dto.FotoInput;
import com.mitrakreasindo.pemrek.graphql.dto.PesanInput;
import com.mitrakreasindo.pemrek.graphql.dto.RoleInput;
import com.mitrakreasindo.pemrek.graphql.dto.RoleReplace;
import com.mitrakreasindo.pemrek.graphql.dto.TransaksiInput;
import com.mitrakreasindo.pemrek.graphql.dto.TransaksiUpdate;
import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.model.AuditLogUser;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.Chat;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Pesan;
import com.mitrakreasindo.pemrek.model.RoleMenuAcl;
import com.mitrakreasindo.pemrek.model.RoleMenu;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.AccountService;
import com.mitrakreasindo.pemrek.service.AuditLogUserService;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.service.ChatService;
import com.mitrakreasindo.pemrek.service.FotoService;
import com.mitrakreasindo.pemrek.service.PesanService;
import com.mitrakreasindo.pemrek.service.RoleMenuService;
import com.mitrakreasindo.pemrek.service.RoleService;
import com.mitrakreasindo.pemrek.service.TransaksiService;

import org.dataloader.DataLoader;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// import graphql.servlet.context.GraphQLContext;
// import graphql.kickstart.execution.context.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;

@Component
public class Mutation implements GraphQLMutationResolver {

    @Resource
    private TransaksiService transaksiService;

    @Resource
    private CallLogService callLogService;

    @Resource
    private FotoService fotoService;

    @Resource
    private AccountService accountService;

    @Resource
    private RoleService roleService;

    @Resource
    private RoleMenuService roleMenuService;
    
    @Resource
    private PesanService pesanService;
    
    @Resource
    private AuditLogUserService auditLogUserService;
    
    @Resource
    private ChatService chatService;


    // // @Autowired
    // // private ModelMapper modelMapper;
    // @Autowired
    // @Qualifier("mapperPatch")
    // private ModelMapper modelMapper;

    @Autowired
    @Qualifier("mapperNormal")
    private ModelMapper modelMapperNormal;

    // @Autowired
    // private ModelPatcher modelPatcher;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Transaksi addTransaksi(TransaksiInput ti) {
        // Transaksi transaksi = new Transaksi();
        // transaksi = modelMapper.map(ti, Transaksi.class);

        // return transaksiService.save(transaksi);
        Transaksi transaksi = new Transaksi();
        transaksi = modelMapperNormal.map(ti, Transaksi.class);
        return transaksiService.save(transaksi);
    }

    public Optional<CallLog> addCallLog(CallLogInput cli, DataFetchingEnvironment dfe) {
        // dfe.getSource();
        CallLog callLog = new CallLog();
        // callLog.setCallId(cli.getCallId());
        // modelMapper.addMappings(new PropertyMap<CallLog, CallLogInput>() {
        // @Override
        // protected void configure() {
        // map().setCallId(source.getCallId());
        // }
        // });
        // ModelMapper modelMapper = new ModelMapper();
        // CallLog callLog = modelMapper.map(cli, CallLog.class);

        // callLog = modelMapper.map(cli, CallLog.class);
        callLog = modelMapperNormal.map(cli, CallLog.class);
        return callLogService.save(callLog);

    }

    // public Optional<CallLog> updateCallLog(CallLogUpdate clu,
    // DataFetchingEnvironment dfe) {
    // CallLog cl = callLogService.findById(clu.getId()).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // not found"));

    // // // cl = modelMapper.map(clu, CallLog.class);
    // // Map<String, Object> mapCl = objectMapper.convertValue(cl, Map.class);
    // // Map<String, Object> mapUpdateClu = objectMapper.convertValue(clu,
    // Map.class);
    // // mapUpdateClu.values().removeIf(Objects::isNull);
    // // mapCl.putAll(mapUpdateClu);

    // // // CallLog clUpdated = objectMapper.convertValue(mapCl, CallLog.class);
    // CallLog clUpdated = modelPatcher.convert(cl, clu, CallLog.class);
    // if (Objects.nonNull(clUpdated.getTerputusMessage())) {
    // if ("".equals(clUpdated.getTerputusMessage()))
    // clUpdated.setTerputusMessage(null);
    // }
    // // if (Optional.ofNullable(clUpdated.getTerputusMessage()).isPresent()) {
    // // if ("".equals(clUpdated.getTerputusMessage()))
    // // clUpdated.setTerputusMessage(null);
    // // }
    // return callLogService.save(clUpdated);
    // }

    public Foto addFoto(String transaksiId, FotoInput fi, DataFetchingEnvironment dfe) {

        Transaksi t = transaksiService.findById(transaksiId).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));
        Foto foto = new Foto();
        // foto = modelMapper.map(fi, Foto.class);
        foto = modelMapperNormal.map(fi, Foto.class);
        foto.setTransaksi(t);
        return fotoService.add(foto);

    }

    // public Optional<Transaksi> updateTransaksi(TransaksiUpdate tu,
    // DataFetchingEnvironment dfe) {
    // Transaksi t = transaksiService.findById(tu.getId()).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // not found"));

    // // // cl = modelMapper.map(clu, CallLog.class);
    // // Map<String, Object> mapCl = objectMapper.convertValue(cl, Map.class);
    // // Map<String, Object> mapUpdateClu = objectMapper.convertValue(clu,
    // Map.class);
    // // mapUpdateClu.values().removeIf(Objects::isNull);
    // // mapCl.putAll(mapUpdateClu);

    // // // CallLog clUpdated = objectMapper.convertValue(mapCl, CallLog.class);
    // Transaksi tUpdated = modelPatcher.convert(t, tu, Transaksi.class);

    // return transaksiService.update(tUpdated);
    // }

    // public Account addAccount(AccountInput ai, DataFetchingEnvironment dfe) {

    // // Transaksi t = transaksiService.findById(transaksiId).orElseThrow(
    // // () -> new
    // // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // // not found"));
    // // Account a = new Account();
    // // a = modelMapperNormal.map(ai, Account.class);
    // // return accountService.add(a);
    // // if (!opt.isPresent()) {
    // // }
    // // roleService.findById(ai.getRole().getId()).orElseThrow(
    // // () -> new
    // // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // // not found"));
    // Role role = roleService.findById(ai.getRole().getId()).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "role
    // not found"));
    // // final DataLoader<String, Transaksi> dataLoader = ((GraphQLContext)
    // // dfe.getContext()).getDataLoaderRegistry()
    // // .get();
    // Account a = new Account();
    // a = modelMapperNormal.map(ai, Account.class);
    // // a.setRole(role);
    // return accountService.add(a);

    // }

    @SuppressWarnings("unchecked")
    public Optional<Transaksi> updateTransaksi(LinkedHashMap<String, Object> tu, DataFetchingEnvironment dfe) {

        // Transaksi t = transaksiService.findById(((Account) tu).getId()).orElseThrow(
        // () -> new
        // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
        // not found"));
        // Transaksi s1 = objectMapper.convertValue(tu, Transaksi.class);
        // String id = tu.get("id").toString();
        String id = Optional.ofNullable(tu.get("id").toString()).orElse("");
        Transaksi t = transaksiService.findById(id).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));

        // Transaksi tUpdated = modelPatcher.convert(t, tu, Transaksi.class);
        LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(t, LinkedHashMap.class);
        mapSource.putAll(tu);
        Transaksi tUpdated = objectMapper.convertValue(mapSource, Transaksi.class);

        return transaksiService.update(tUpdated);
    }

    @SuppressWarnings("unchecked")
    public Optional<CallLog> updateCallLog(LinkedHashMap<String, Object> clu, DataFetchingEnvironment dfe) {
        // String s = Optional.ofNullable(clu.getId()).orElse("");
        String id = Optional.ofNullable(clu.get("id").toString()).orElse("");

        CallLog cl = callLogService.findById(id).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));

        // CallLog clUpdated = modelPatcher.convert(cl, clu, CallLog.class);
        // if (Objects.nonNull(clUpdated.getTerputusMessage())) {
        // if ("".equals(clUpdated.getTerputusMessage()))
        // clUpdated.setTerputusMessage(null);
        LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(cl, LinkedHashMap.class);
        mapSource.putAll(clu);
        CallLog clUpdated = objectMapper.convertValue(mapSource, CallLog.class);
        return callLogService.save(clUpdated);
    }

    // @SuppressWarnings("unchecked")
    // public Optional<Account> updateAccount(LinkedHashMap<String, Object> au,
    // DataFetchingEnvironment dfe) {

    // String id = Optional.of(au.get("id").toString()).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // not found"));

    // Account a = accountService.findById(id).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // not found"));

    // // Optional<Role> optRoleId = Optional.of(accountUpdate.get("role")).map(v ->
    // {
    // // LinkedHashMap<String, Object> roleM =
    // objectMapper.convertValue(accountUpdate.get("role"),
    // // LinkedHashMap.class);
    // // String roleId = roleM.get("id").toString();
    // // // Role role = roleService.findById(roleId).orE
    // // return roleId;
    // // }).map(v -> roleService.findById(v)).orElse(null);
    // // LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(a,
    // LinkedHashMap.class);
    // // mapSource.putAll(accountUpdate);
    // // Account aUpdated = objectMapper.convertValue(mapSource, Account.class);
    // // // if (optRoleId.isPresent())
    // // // aUpdated.setRole(optRoleId.get());
    // // return accountService.update(aUpdated);

    // LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(a,
    // LinkedHashMap.class);
    // mapSource.putAll(au);
    // Account tUpdated = objectMapper.convertValue(mapSource, Account.class);

    // return accountService.update(tUpdated);
    // }

    public Account addAccount(AccountInput ai, DataFetchingEnvironment dfe) {

        Role role = roleService.findById(ai.getRole().getId()).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "role not found"));

        Account a = new Account();
        a = modelMapperNormal.map(ai, Account.class);
        // a.setRole(role);
        return accountService.add(a);

    }

    @SuppressWarnings("unchecked")
    public Optional<Account> updateAccount(LinkedHashMap<String, Object> updateSource, DataFetchingEnvironment dfe) {

        String id = Optional.ofNullable(updateSource.get("id").toString()).orElse("");
        Optional<Active> activeFlag = Optional.ofNullable(Active.valueOf(updateSource.get("activeFlag").toString()));

        Optional.ofNullable(updateSource.get("password")).map(v -> passwordEncoder.encode(v.toString()))
                .ifPresent(v -> {
                    updateSource.put("password", v);
                });
        // if (updateSource.get("password").toString()) {
        // String password = passwordEncoder.encode(account.getPassword());
        // account.setPassword(password);
        // }

        Account account = accountService.findById(id).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));
        AccountUpdate accountUpdate = modelMapperNormal.map(account, AccountUpdate.class);
        // Map<String, String> triggerMap = objectMapper.convertValue(accountUpdate, new
        // TypeReference<HashMap<String, Object>>() {
        // });
        // JsonNode fea = objectMapper.convertValue(accountUpdate, JsonNode.class);
        LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(accountUpdate, LinkedHashMap.class);
        mapSource.putAll(updateSource);
        Account clUpdated = objectMapper.convertValue(mapSource, Account.class);
        activeFlag.ifPresent((v) -> {
            clUpdated.setActiveFlag(v);
        });
        return accountService.update(clUpdated);
    }

    // public Role addRole(RoleInput ai, DataFetchingEnvironment dfe) {

    // // Role role = roleService.findById(ai.getRole().getId()).orElseThrow(
    // // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "role
    // not found"));

    // Role a = new Role();
    // a = modelMapperNormal.map(ai, Role.class);
    // // a.setRole(role);
    // return roleService.add(a);

    // }

    // public Role addRole(RoleInput ai, DataFetchingEnvironment dfe) {

    // // Role role = roleService.findById(ai.getRole().getId()).orElseThrow(
    // // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "role
    // not found"));

    // Role a = new Role();
    // a = modelMapperNormal.map(ai, Role.class);
    // // a.setRole(role);
    // return roleService.add(a);

    // }

    // @SuppressWarnings("unchecked")
    // public Optional<Role> addRole(LinkedHashMap<String, Object> source,
    // DataFetchingEnvironment dfe) {

    // Role create = new Role();
    // LinkedHashMap<String, Object> mapSource = objectMapper.convertValue(create,
    // LinkedHashMap.class);
    // mapSource.putAll(source);
    // Role update = objectMapper.convertValue(mapSource, Role.class);

    // return roleService.add(update);

    // }

    // @SuppressWarnings("unchecked")
    public Optional<Role> addRole(RoleInput source, DataFetchingEnvironment dfe) {

        // Role update = new Role();
        Role update = modelMapperNormal.map(source, Role.class);
        update.getRoleMenus().stream().forEach(e -> {

            e.getRoleMenuAcls().stream().forEach(acl -> {
                // Acl newAcl = new Acl();
                // newAcl.setActiveFlag(Active.ACTIVE);
                // newAcl.setA
                // RoleMenuAcl rma = new RoleMenuAcl();
                // rma = modelMapperNormal.map(acl, RoleMenuAcl.class);
                // rma.setRoleMenu(e);
                acl.setRoleMenu(e);
                // e.addRoleMenuAcls(acl);
            });
            // e.getRoleMenuAcls().stream().forEach(System.out::println);
            // // e.getRoleMenuAcls().stream().map(acl -> {
            // // Acl c = new Acl();
            // // acl.setRoleMenu(e);
            // // return acl;
            // // });
            // Set<RoleMenuAcl> r = e.getRoleMenuAcls().stream().map(rma -> {
            // RoleMenuAcl b = new RoleMenuAcl();
            // // b = modelMapperNormal.map(rma, RoleMenuAcl.class);
            // b.setRoleMenu(e);
            // return b;
            // }).collect(Collectors.toSet());
            // e.setRoleMenuAcls(r);
            e.setRole(update);
        });

        return roleService.add(update);

    }

    // @SuppressWarnings("unchecked")
    // public Optional<Role> replaceRole(LinkedHashMap<String, Object> updateSource,
    // DataFetchingEnvironment dfe) {

    // String id =
    // Optional.ofNullable(updateSource.get("id").toString()).orElse("");

    // roleMenuService.deleteAllByRoleId(id);
    // Role role = roleService.findById(id).orElseThrow(
    // () -> new
    // BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id
    // not found"));

    // RoleInput roleUpdate = modelMapperNormal.map(role, RoleInput.class);
    // // Map<String, String> triggerMap = objectMapper.convertValue(accountUpdate,
    // new
    // // TypeReference<HashMap<String, Object>>() {
    // // });
    // // JsonNode fea = objectMapper.convertValue(accountUpdate, JsonNode.class);
    // LinkedHashMap<String, Object> mapSource =
    // objectMapper.convertValue(roleUpdate, LinkedHashMap.class);
    // mapSource.putAll(updateSource);
    // Role clUpdated = objectMapper.convertValue(mapSource, Role.class);
    // // activeFlag.ifPresent((v) -> {
    // // clUpdated.setActiveFlag(v);
    // // });
    // clUpdated.getRoleMenus().stream().forEach(e -> {
    // e.getRoleMenuAcls().stream().forEach(acl -> {
    // acl.setRoleMenu(e);
    // });
    // e.setRole(clUpdated);
    // });
    // // final DataLoader<String, Foto> dataloader = ((GraphQLContext)
    // dfe.getContext()).getDataLoaderRegistry().get()
    // // .getDataLoader("fotosDataLoader");

    // Object t = dfe.getSource();
    // // List<String> ids = t.getFotos().stream().map(e ->
    // e.getId()).collect(Collectors.toList());

    // // return dataloader.loadMany(ids);
    // // dfe.getContext()
    // // roleMenuService.deleteAllByRoleId(id);

    // return roleService.replace(clUpdated);
    // // Long role2 = roleService.deleteRoleMenus(id);
    // // return Optional.ofNullable(role);

    // }

    @SuppressWarnings("unchecked")
    public Optional<Role> replaceRole(RoleReplace source, DataFetchingEnvironment dfe) {
        // Role update = new Role();
        roleMenuService.deleteAllByRoleId(source.getId());
        Role role = roleService.findById(source.getId()).orElseThrow(
                () -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "id not found"));

        Set<RoleMenu> rm = new HashSet<>();
        Set<RoleMenuAcl> a = new HashSet<>();
        Role update = modelMapperNormal.map(source, Role.class);
        update.getRoleMenus().forEach(e -> {

            e.getRoleMenuAcls().stream().forEach(acl -> {
                acl.setRoleMenu(e);
            });
            e.setRole(update);
            rm.add(e);

        });
        role.setName(update.getName());
        role.setDescription(update.getDescription());
        role.setRoleMenus(rm);

        return roleService.replace(role);
        // Long role2 = roleService.deleteRoleMenus(id);
        // return Optional.ofNullable(role);

    }
    public String removeRole(String id, DataFetchingEnvironment dfe) {
        return roleService.remove(id);
    }



    public Pesan addPesan(PesanInput pi, DataFetchingEnvironment dfe) {

        Pesan pesan = new Pesan();
        // foto = modelMapper.map(fi, Foto.class);
        pesan = modelMapperNormal.map(pi, Pesan.class);
        pesan.setActiveFlag(Active.ACTIVE);
        return pesanService.add(pesan);

    }
    
    public Optional<AuditLogUser> addAuditLogUser(AuditLogUserInput cli, DataFetchingEnvironment dfe) {

        AuditLogUser auditLogUser = new AuditLogUser();
        auditLogUser = modelMapperNormal.map(cli, AuditLogUser.class);
        return auditLogUserService.save(auditLogUser);

    }
    
    public List<Chat> addChats(List<Chat> chatInput, DataFetchingEnvironment dfe) {
        return chatService.saveAll(chatInput);
    }

}