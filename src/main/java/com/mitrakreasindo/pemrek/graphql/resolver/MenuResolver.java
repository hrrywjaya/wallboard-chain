package com.mitrakreasindo.pemrek.graphql.resolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.mitrakreasindo.pemrek.graphql.datafetcher.GraphQLDataFetchers;
import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.model.Menu;

import org.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
// import graphql.servlet.context.GraphQLContext;
import graphql.kickstart.execution.context.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;

@Component
public class MenuResolver implements GraphQLResolver<Menu> {

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    public CompletableFuture<List<Acl>> acls(Menu menu, DataFetchingEnvironment dfe) {
        final DataLoader<String, Acl> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
                .getDataLoader("aclsDataLoader");

        Menu t = dfe.getSource();
        // Optional<Menu> parent = Optional.ofNullable(t.getParent());
        List<String> ids = new ArrayList<>();
        // if (parent.isPresent()) {
        // Optional<Set<Acl>> acl = Optional.ofNullable(t.getAcls());
        // if (acl.isPresent()) {
        // ids = t.getAcls().stream().map(e -> e.getId()).collect(Collectors.toList());
        // }
        // // ids = t.getParent().getAcls().stream().map(e ->
        // e.getId()).collect(Collectors.toList());
        // } else {
        ids = t.getAcls().stream().map(e -> e.getId()).collect(Collectors.toList());
        // }

        // Optional<Menu> parent = Optional.ofNullable(t.getParent());
        // if (parent.isPresent()) {
        // ids = t.getAcls().stream().map(e -> e.getId()).collect(Collectors.toList());
        // }

        // Optional<Integer> acl = Optional.ofNullable(t.getAcls().size());
        // if (acl.isPresent()) {
        //     ids = t.getAcls().stream().map(e -> e.getId()).collect(Collectors.toList());
        // }

        return dataloader.loadMany(ids);
    }

    public CompletableFuture<List<Menu>> childs(Menu menu, DataFetchingEnvironment dfe) {
        final DataLoader<String, Menu> dataloader = ((GraphQLContext) dfe.getContext()).getDataLoaderRegistry().get()
                .getDataLoader("childsDataLoader");
        final DataLoader<String, Menu> dataloaderHaveParent = ((GraphQLContext) dfe.getContext())
                .getDataLoaderRegistry().get().getDataLoader("childsHaveParentDataLoader");

        Menu t = dfe.getSource();
        // List<String> ids = t.getChilds().stream().map(e ->
        // e.getId()).collect(Collectors.toList());

        List<String> ids = new ArrayList<>();
        // Optional<Set<Menu>> child = Optional.ofNullable(t.getChilds());
        // if (child.isPresent()) {
        // ids = t.getChilds().stream().map(e ->
        // e.getId()).collect(Collectors.toList());
        // }

        // return dataloader.loadMany(ids);

        Optional<Menu> parent = Optional.ofNullable(t.getParent());
        if (parent.isPresent()) {
            ids = t.getChilds().stream().map(e -> e.getId()).collect(Collectors.toList());
            return dataloaderHaveParent.loadMany(ids);
        } else {
            ids = t.getChilds().stream().map(e -> e.getId()).collect(Collectors.toList());
            return dataloader.loadMany(ids);
        }
    }

}