package com.mitrakreasindo.pemrek.graphql.datafetcher;

import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.graphql.dto.TransaksiDto;
import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.service.AccountService;
import com.mitrakreasindo.pemrek.service.AclService;
import com.mitrakreasindo.pemrek.service.CallLogService;
import com.mitrakreasindo.pemrek.service.FotoService;
import com.mitrakreasindo.pemrek.service.MenuService;
import com.mitrakreasindo.pemrek.service.ReferensiService;
import com.mitrakreasindo.pemrek.service.RoleService;
import com.mitrakreasindo.pemrek.service.TransaksiService;

import java.util.stream.Collectors;

import javax.annotation.Resource;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dataloader.BatchLoader;
import org.dataloader.BatchLoaderEnvironment;
import org.dataloader.BatchLoaderWithContext;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Component
@RequiredArgsConstructor
public class GraphQLDataFetchers {

    private static final Logger LOG = LoggerFactory.getLogger(GraphQLDataFetchers.class);

    @Resource
    private final FotoService fotoService;
    @Resource
    private final ReferensiService referensiService;
    @Resource
    private final CallLogService callLogService;
    @Resource
    private final TransaksiService transaksiService;
    @Resource
    private final AccountService accountService;
    @Resource
    private final RoleService roleService;
    @Resource
    private final AclService aclService;
    @Resource
    private final MenuService menuService;

    // public DataFetcher zeRefsDataFetcher = environment -> {
    // Transaksi transaksi = environment.getSource();
    // List<String> zeRefsIds = transaksi.getZeRefs().stream().map(e ->
    // e.getId()).collect(Collectors.toList());
    // // Context ctx = environment.getContext();
    // // Context ctx = environment.getContext();
    // DataLoader<String, ZeRef> dataLoader =
    // environment.getDataLoader("zeRefsDataLoader");
    // return dataLoader.loadMany(zeRefsIds);
    // };

    // public BatchLoader<String, ZeRef> countryBatchLoader() {
    // return keys -> CompletableFuture.supplyAsync(() -> {
    // List<ZeRef> zeRefs = zeRefService.findByIds(keys);
    // LOG.info("Returning {} countries.", zeRefs.size());
    // return zeRefs;
    // });
    // }

    public BatchLoader<String, Transaksi> allTransaksiBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Transaksi> transaksis = transaksiService.findAllById(keys).stream().collect(Collectors.toList());
            LOG.info("Returning {} transaksis.", transaksis.size());
            return transaksis;
        });
    }

    // public BatchLoader<String, ZeRef> zeRefBatchLoader() {
    // return keys -> CompletableFuture.supplyAsync(() -> {
    // List<ZeRef> zeRefs = zeRefService.findByIds(keys);
    // LOG.info("Returning {} zeRefs.", zeRefs.size());
    // return zeRefs;
    // });
    // }

    public BatchLoader<String, Foto> fotoBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Foto> fotos = fotoService.findByIds(keys);
            LOG.info("Returning {} fotos.", fotos.size());
            return fotos;
        });
    }

    public BatchLoader<String, Referensi> referensiBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Referensi> referensis = referensiService.findByIds(keys);
            LOG.info("Returning {} referensis.", referensis.size());
            return referensis;
        });
    }

    public BatchLoader<String, Account> accountBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Account> accounts = accountService.findByIds(keys);
            LOG.info("Returning {} accounts.", accounts.size());
            return accounts;
        });
    }

    public BatchLoader<String, Acl> aclBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Acl> acls = aclService.findByIds(keys);
            LOG.info("Returning {} acls.", acls.size());
            return acls;
        });
    }

    public BatchLoader<String, Menu> childBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            // List<Menu> childs = menuService.findByChildsIdIn(keys).stream().map(e -> new
            // Menu())
            // .collect(Collectors.toList());
            List<Menu> childs = menuService.findByChildsIdIn(keys);
            LOG.info("Returning {} childs.", childs.size());
            return childs;
        });
    }

    public BatchLoader<String, Menu> childHaveParentBatchLoader() {
        return keys -> CompletableFuture.supplyAsync(() -> {
            List<Menu> childHaveParents = menuService.findByChildsIdIn(keys);
            LOG.info("Returning {} childHaveParents.", childHaveParents.size());
            return childHaveParents;
        });
    }

    // public BatchLoader<String, CallLog> callLogBatchLoader() {
    // return keys -> CompletableFuture.supplyAsync(() -> {
    // List<CallLog> callLogs = callLogService.findByIds(keys);
    // LOG.info("Returning {} callLogs.", callLogs.size());
    // return callLogs;
    // });
    // }

    // public DataFetcher allTransaksisDataFetcher = environment -> {
    // DataFetchingFieldSelectionSet selectionSet = environment.getSelectionSet();
    // if (selectionSet.contains("zeRefs")) {
    // List<Transaksi> data = getAllIssuesWithComments(environment,
    // selectionSet.getFields());
    // return data;
    // } else {
    // List<Transaksi> issues = getAllIssuesWitNoComments(environment);
    // return issues;
    // }
    // };

    public DataFetcher allTransaksisDataFetcher = new DataFetcher<List<Transaksi>>() {
        @Override
        public List<Transaksi> get(DataFetchingEnvironment environment) {
            TransaksiService ctx = environment.getContext();

            List<Transaksi> products;
            // String match = environment.getArgument("match");
            // if (match != null) {
            // products = fetchProductsFromDatabaseWithMatching(ctx, match);
            // } else {
            // products = fetchAllProductsFromDatabase(ctx);
            // }
            products = ctx.findAll();
            return products;
        }
    };

    // BatchLoaderWithContext<String, Object> batchLoaderWithCtx = new
    // BatchLoaderWithContext<String, Object>() {

    // @Override
    // public CompletionStage<List<Object>> load(List<String> keys,
    // BatchLoaderEnvironment loaderContext) {
    // //
    // // we can have an overall context object
    // SecurityContext securityCtx = loaderContext.getContext();
    // //
    // // and we can have a per key set of context objects
    // Map<Object, Object> keysToSourceObjects = loaderContext.getKeyContexts();

    // return CompletableFuture
    // .supplyAsync(() -> getTheseCharacters(securityCtx.getToken(), keys,
    // keysToSourceObjects));
    // }

    // @Override
    // public CompletionStage<List<Object>> load(List<String> keys,
    // BatchLoaderEnvironment environment) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    // };

    public DataFetcher getRoleByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            String roleId = dataFetchingEnvironment.getArgument("id");
            RoleService ctx = dataFetchingEnvironment.getContext();
            return ctx.findById(roleId).orElse(null);
            // return books.stream().filter(book ->
            // book.get("id").equals(bookId)).findFirst().orElse(null);
        };
    }

}