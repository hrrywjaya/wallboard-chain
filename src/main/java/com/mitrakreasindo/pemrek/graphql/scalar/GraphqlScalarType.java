package com.mitrakreasindo.pemrek.graphql.scalar;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;

@Component
public class GraphqlScalarType {

    @Bean
    public GraphQLScalarType object() {
        return ExtendedScalars.Object;
    }
}