package com.mitrakreasindo.pemrek.graphql.dto;



import lombok.Data;

@Data
public class FotoInput {


	private String base64;
	
	private String username;
	private String role;
}