package com.mitrakreasindo.pemrek.graphql.dto;

import com.mitrakreasindo.pemrek.core.model.Role;

import lombok.Data;

@Data
public class AccountInput {
	
	private String username;
	private String password;
	private String fullName;
	private String activeFlag;
	private RoleInputWithId role;

	private String title;
	private String email;
	
	private String branchCode;
	
	private String finesseAgentId;

	private String division;

	// private String finesseUsername;
	// private String finessePassword;
	// private String finesseAgentId;
	// private String jabberUsername;
	// private String jabberPassword;
	// private String jabberExtension;

}