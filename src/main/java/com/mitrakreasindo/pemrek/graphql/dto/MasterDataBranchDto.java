package com.mitrakreasindo.pemrek.graphql.dto;

import lombok.Data;

@Data
public class MasterDataBranchDto {
    private String id;

    private String branchName;
    private String branchCode;
    private String branchType;
    private String branchInitial;
    private String city;
    private String address;
    private String branchStatus;

    private String mainBranchName;
    private String mainBranchCode;    
    private String mainBranchInitial;
}
