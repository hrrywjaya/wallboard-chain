package com.mitrakreasindo.pemrek.graphql.dto;



import lombok.Data;

@Data
public class PesanDto {	
	private String message;
	private String author;
}
