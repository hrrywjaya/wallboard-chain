package com.mitrakreasindo.pemrek.graphql.dto;


import lombok.Data;

@Data
public class ChatInput {

	private String callLogId;    
    private String type;    
    private String username;    
    private String role;    
    private String message;
    private String timestamp;

}