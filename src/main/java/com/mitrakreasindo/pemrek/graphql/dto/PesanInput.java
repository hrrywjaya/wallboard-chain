package com.mitrakreasindo.pemrek.graphql.dto;



import lombok.Data;

@Data
public class PesanInput {	
	private String message;
	private String author;
}
