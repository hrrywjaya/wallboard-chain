package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.RoleMenu;

import lombok.Data;

@Data
public class MenuFlatDto {
	public MenuFlatDto() {
    }
 
    public MenuFlatDto(Menu menu) {
 
        this.id = menu.getId();
        this.path = menu.getPath();
        this.name = menu.getName();
        this.acls = menu.getAcls().stream().map(AclDto::new).collect(Collectors.toList());
		this.activeFlag = menu.getActiveFlag().toString();
		this.frontend = menu.getFrontend();
   }
	
	private String id;
	private String path;
	private String name;
    private List<AclDto> acls = new ArrayList<>();
	private String activeFlag;
	private String frontend;

}