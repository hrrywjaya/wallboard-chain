package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import com.mitrakreasindo.pemrek.core.model.Role;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Data
public class PaginationDto<T> {

	public PaginationDto(final Page<?> page, final List<T> entities, final LinkedHashMap<String, String> orders) {
        // this.currentIndex = page.getNumber() + 1;
        // this.beginIndex = Math.max(1, currentIndex - 5);
		// this.endIndex = Math.min(beginIndex + 10, page.getTotalPages());

        this.content = entities;
		this.pageable = page.getPageable();
		this.totalPages = page.getTotalPages();
		this.totalElements = page.getTotalElements();
		this.last = page.isLast();
		this.size = page.getSize();
		// this.number = page.getNumber();
		this.number = (page.getNumber() + 1 >= page.getTotalPages()) ? (page.getTotalPages() - 1) : page.getNumber();
		this.sort = page.getSort();
		this.numberOfElements = page.getNumberOfElements();
		this.first = page.isFirst();
		// this.orders.putAll(orders);
		this.orders = orders;
    }
	// private String content: Content[];
	private List<T> content;
	private Pageable pageable;
	private Integer totalPages;
	private long totalElements;
	private Boolean last;
	private Integer size;
	private Integer number;
	private Sort sort;
	private Integer numberOfElements;
	private Boolean first;
	private LinkedHashMap<String, String> orders = new LinkedHashMap<>();

}