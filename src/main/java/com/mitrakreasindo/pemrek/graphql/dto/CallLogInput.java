package com.mitrakreasindo.pemrek.graphql.dto;



import lombok.Data;

@Data
public class CallLogInput {


	private String callId;

	private String noRef;
	
	private String username;
	private String fullName;
	private String role;

	// start call
	private String alertingDate;
	private String activeDate;
	
	private String incomingDuration;
	private String answerDate;
	private String answerDuration;

	private String droppedDate;
	
	private String transaksiDuration;
	private String transaksiStatus;
    
	private String cti;
	private String cucm;
	private String tftp;
	private String finesseUsername;
	private String finessePassword;
	private String finesseAgentId;
	private String jabberUsername;
	private String jabberPassword;
	private String jabberExtension;

	// ----- start terputus ------
	private String terputusMdId;
	private String terputusMdTitle;
	private String terputusMessage; // kalo terputusMdTitle in [lainnya...]
	// ----- end terputus ------
	
	
	// ----- start tolak ------
	private String tolakMdId;
	private String tolakMdTitle;
	private String tolakMessage; // kalo tolakMdTitle in [lainnya...]
	// ----- end tolak ------

	// ----- start service level ------
	private String serviceLevelMdId;
	private String serviceLevelMdTitle;
	private String serviceLevelMessage; // kalo tolakMdTitle in [lainnya...]
	// ----- end service level ------

	private String foto; //base64
	
	private String oldRawKonfirmasiData;

	private String rawKonfirmasiData;

	// ----- start addtional field for query ------
	private String qrChannel;
	private String qrKodeTransaksiTemp;
	private String qrJenisTransaksi;
	private String qrCabang; 
	private String qrNoRekening;
	private String qrCisCustomerNumber;
	private String qrNamaNasabah; 
	private String qrJenisRekening; // utk di frontend page transaksi-berhasil

  private String qrTipeKartuPasporBcaTemp;

	private String qrJenisNasabah;
	private String qrStatusIdentitas;
	private String qrStatusRba;
	private String qrNamaCabang;

	private String qrCustomerName;
	private String qrCustomerPepStatus;
	private String qrCustomerDttotStatus;
	private String qrCustomerDikStatus;
	private String qrCustomerNrtStatus;
	private String qrCustomerCode;
	// ----- end addtional field for query ------

	// ----- start approval spv ------
	private String approvalUsername;
	private String approvalFullname;
	private String approvalRole;
	private String approvalStartDate;
	private String approvalEndDate;
	private String approvalDuration; 
	// ----- end approval spv ------
	
	private String workReadyDate;
	private String workDate;
		
	private String qrNik;	
	private String qrNomorHp;

	private String qrMbankStatus;
	
	private String perubahanData;

	private String qrSumber;
	
	// ----- start channel ------
	private String channelMdId;
	private String channelMdCode;
	private String channelMdText;
	// ----- end channel ------
	
	// ----- start transactioncode ------
	private String transactionCodeMdId;
	private String transactionCodeMdCode;
	private String transactionCodeMdText;
	// ----- end transactioncode ------
}