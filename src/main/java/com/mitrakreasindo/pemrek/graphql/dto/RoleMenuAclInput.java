package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.List;

import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.model.Menu;

import lombok.Data;

@Data
public class RoleMenuAclInput {
	
	private AclInputWithId acl;
	private String activeFlag;

}