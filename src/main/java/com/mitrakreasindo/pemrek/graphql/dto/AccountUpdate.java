package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.Date;

import lombok.Data;

@Data
public class AccountUpdate {
	
	private String id;
	
	private String username;
	private String password;
	private String fullName;
	private String activeFlag;
	private RoleUpdate role;

	private String title;
	private String email;
	
	private String branchCode;
	
	private String finesseAgentId;

	private String division;

	// private String finesseUsername;
	// private String finessePassword;
	// private String finesseAgentId;
	// private String jabberUsername;
	// private String jabberPassword;
	// private String jabberExtension;
	
    private String createdBy;

    private Date createdDate;

}