package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RoleReplace {
	
	private String id;
	private String name;
	private String description;
    private List<RoleMenuInput> roleMenus = new ArrayList<>();

}