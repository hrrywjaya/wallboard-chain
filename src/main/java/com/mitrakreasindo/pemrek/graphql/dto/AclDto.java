package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.List;

import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.model.Menu;

import lombok.Data;

@Data
public class AclDto {

	
	public AclDto() {
    }
 
    public AclDto(Acl acl) {
 
        this.id = acl.getId();
        this.name = acl.getName();
        this.description = acl.getDescription();
        this.enabled = acl.getEnabled();
        this.activeFlag = acl.getActiveFlag().toString();
   }
	
	private String id;
	private String name;
	private String description;
	private String enabled;
	private String activeFlag;

}