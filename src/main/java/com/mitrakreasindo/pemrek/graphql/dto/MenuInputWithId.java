package com.mitrakreasindo.pemrek.graphql.dto;

import com.mitrakreasindo.pemrek.core.model.Role;

import lombok.Data;

@Data
public class MenuInputWithId {
	
	private String id;
	private String name;
	private String description;
	private String path;
	private String frontend;

}