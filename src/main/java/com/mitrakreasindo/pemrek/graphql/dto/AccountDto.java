package com.mitrakreasindo.pemrek.graphql.dto;

import com.mitrakreasindo.pemrek.core.model.Role;

import lombok.Data;

@Data
public class AccountDto {
	
	private String id;
	
	private String username;
	private String password;
	private String fullName;
	private RoleInputWithId role;

	private String title;
	private String email;
	private String branchCode;
	private String finesseAgentId;

	private String division;

}