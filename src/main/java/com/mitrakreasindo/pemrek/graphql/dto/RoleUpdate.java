package com.mitrakreasindo.pemrek.graphql.dto;

import com.mitrakreasindo.pemrek.core.model.Role;

import lombok.Data;

@Data
public class RoleUpdate {
	
	private String id;
	private String name;
	private String description;

}