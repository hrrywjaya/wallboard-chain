package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mitrakreasindo.pemrek.core.model.Active;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
// @NoArgsConstructor
// @AllArgsConstructor
public class FotoDto  {
	
    private TransaksiDto transaksi;

    private String id;

    private String base64;
    
	private String username;
	private String role;

	@CreatedDate
	private Date createdDate;
	@CreatedBy
	private String createdBy;
	@LastModifiedDate
	private Date updatedDate;
	@LastModifiedBy
	private String updatedBy;
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Active activeFlag = Active.ACTIVE;
}
