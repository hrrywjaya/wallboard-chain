package com.mitrakreasindo.pemrek.graphql.dto;



import lombok.Data;

@Data
public class AuditLogUserInput {

	private String action;
	private String oldRawUser;
	private String rawUser;
	private String perubahanData;
	
	private String username;
	private String fullName;
	private String role;
	
}