package com.mitrakreasindo.pemrek.graphql.dto;

import java.util.ArrayList;
import java.util.List;

import com.mitrakreasindo.pemrek.model.RoleMenu;

import lombok.Data;

@Data
public class RoleInput {
	
	private String name;
	private String description;
    private List<RoleMenuInput> roleMenus = new ArrayList<>();

}