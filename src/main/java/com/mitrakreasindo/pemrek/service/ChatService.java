package com.mitrakreasindo.pemrek.service;

import java.util.List;
import com.mitrakreasindo.pemrek.model.Chat;
import com.mitrakreasindo.pemrek.repository.ChatRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatService {

	@Autowired
    private ChatRepository chatRepository;
    

    public List<Chat> saveAll(List<Chat> ChatInputs) {
		return chatRepository.saveAll(ChatInputs);
	}

}
