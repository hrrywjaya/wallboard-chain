package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;

import com.mitrakreasindo.pemrek.graphql.dto.MenuFlatDto;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.model.RoleMenu;
import com.mitrakreasindo.pemrek.repository.MenuRepository;
import com.mitrakreasindo.pemrek.repository.RoleMenuRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuService
{

	@Autowired
	private RoleMenuRepository roleMenuRepository;

	


	public List<RoleMenu> findAll() {
		return roleMenuRepository.findAll();
	}


	public List<RoleMenu> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		
		return roleMenuRepository.findAllById(keys);
		
	}


	public void deleteAllByRoleId(String id) {
		// String id = "A";
		roleMenuRepository.deleteAllByRoleId(id);
		
	}
}
