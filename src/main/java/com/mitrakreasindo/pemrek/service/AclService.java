package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;

import com.mitrakreasindo.pemrek.model.Acl;
import com.mitrakreasindo.pemrek.repository.AclRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AclService
{

	@Autowired
	private AclRepository aclRepository;

	


	public List<Acl> findAll() {
		return aclRepository.findAll();
	}


	public List<Acl> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return aclRepository.findAllById(keys);
		
	}
	
}
