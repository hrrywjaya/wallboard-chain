package com.mitrakreasindo.pemrek.service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.MasterDataTolak;

/**
 * @author miftakhul
 *
 */
public interface MasterDataTolakService extends BaseService<MasterDataTolak>
{
    List<MasterDataTolak> findAll();
}
