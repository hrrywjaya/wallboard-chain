package com.mitrakreasindo.pemrek.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.ReportLog;
import com.mitrakreasindo.pemrek.model.ReportLogPerubahan;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.model.TransaksiLogPerubahan;
import com.mitrakreasindo.pemrek.model.TransaksiLogPerubahanEform;
import com.mitrakreasindo.pemrek.repository.RoleRepository;
import com.mitrakreasindo.pemrek.repository.TransaksiRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportService {
  @Autowired
  private CallLogService callLogService;

  @Autowired
  ObjectMapper objMapper;

  public ReportLogPerubahan getReport(String noRef) {

    List<CallLog> trx = callLogService.getLogPerubahan(noRef);

    List<ReportLog> rp = trx.stream().map(e -> {
      ReportLog log = new ReportLog();
      try {
        TransaksiLogPerubahan trxLogPerubahan = objMapper.readValue(e.getRawKonfirmasiData(),
            TransaksiLogPerubahan.class);
        log.setTrxLog(trxLogPerubahan);

        TransaksiLogPerubahan trxLogPerubahanOld = objMapper.readValue(e.getOldRawKonfirmasiData(),
            TransaksiLogPerubahan.class);
        log.setTrxLogOld(trxLogPerubahanOld);
        if (e.getCreatedDate() != null) {
          log.setCreatedDate(e.getCreatedDate().toString());
        }
        if (e.getUsername() != null) {
          log.setUsername(e.getUsername());
        }
        if (e.getNoRef() != null) {
          log.setNoRef(e.getNoRef());
        }

        
        // if(e.getApprovalDate() != null){
        // log.setApproveDate(e.getApprovalDate());
        // }
        if (e.getApprovalEndDate() != null) {
          log.setApproveDate(e.getApprovalEndDate());
        }
        if (e.getApprovalUsername() != null) {
          log.setUsernameSPV(e.getApprovalUsername());
        }
      } catch (IOException er) {
      }
      return log;
    }).collect(Collectors.toList());

    ReportLogPerubahan rpl = new ReportLogPerubahan();
    rpl.setLog(rp);

    return rpl;

  }

  public ReportLogPerubahan getReportByParam(String param, String startDate, String endDate) {

    List<CallLog> trx = callLogService.getLogPerubahanByParam(param, startDate, endDate);

    List<ReportLog> rp = trx.stream().map(e -> {
      ReportLog log = new ReportLog();
      try {
       
        TransaksiLogPerubahan trxLogPerubahan = objMapper.readValue(e.getPerubahanData(),
            TransaksiLogPerubahan.class);
        log.setTrxLog(trxLogPerubahan);//perubahan data

        TransaksiLogPerubahan trxLogPerubahanOld = objMapper.readValue(e.getOldRawKonfirmasiData(),
            TransaksiLogPerubahan.class);
        log.setTrxLogOld(trxLogPerubahanOld);

        
        if (e.getCreatedDate() != null) {
          log.setCreatedDate(e.getCreatedDate().toString());
        }
        if(e.getActiveDate() != null){
          log.setWaktuPanggilan(e.getActiveDate());
        }
        if(e.getJabberExtension() != null){
          log.setExtention(e.getJabberExtension());
        }        
        if(e.getTransaksiStatus() != null){
          log.setStatus(e.getTransaksiStatus());
        }
        if(e.getTolakMdId() != null){
          log.setTolakMdId(e.getTolakMdId());
        }
        if(e.getDroppedDate() != null){
          log.setDropped_date(e.getDroppedDate());
        }
        if(e.getQrNoRekening() != null){
          log.setNoRekening(e.getQrNoRekening());
        }
        if (e.getUsername() != null) {
          log.setUsername(e.getUsername());
        }
        if (e.getNoRef() != null) {
          log.setNoRef(e.getNoRef());
        }
        if (e.getApprovalEndDate() != null) {
          log.setApproveDate(e.getApprovalEndDate());
        }
        if (e.getApprovalUsername() != null) {
          log.setUsernameSPV(e.getApprovalUsername());
        }
      } catch (IOException er) {
      }
      return log;
    }).collect(Collectors.toList());

    ReportLogPerubahan rpl = new ReportLogPerubahan();
    rpl.setLog(rp);

    return rpl;

  }

  
  public ReportLogPerubahan getReportByDate(String startDate, String endDate) {

    List<CallLog> trx = callLogService.getLogPerubahanByDate(startDate, endDate);

    List<ReportLog> rp = trx.stream().map(e -> {
      ReportLog log = new ReportLog();
      try {
        // TransaksiLogPerubahan trxLogPerubahan = objMapper.readValue(e.getRawKonfirmasiData(),
        //     TransaksiLogPerubahan.class);
        // log.setTrxLog(trxLogPerubahan);

        TransaksiLogPerubahan trxLogPerubahan = objMapper.readValue(e.getPerubahanData(),
            TransaksiLogPerubahan.class);
        log.setTrxLog(trxLogPerubahan);//perubahan data

        TransaksiLogPerubahan trxLogPerubahanOld = objMapper.readValue(e.getOldRawKonfirmasiData(),
            TransaksiLogPerubahan.class);
        log.setTrxLogOld(trxLogPerubahanOld);

        
        if (e.getCreatedDate() != null) {
          log.setCreatedDate(e.getCreatedDate().toString());
        }
        if(e.getActiveDate() != null){
          log.setWaktuPanggilan(e.getActiveDate());
        }
        if(e.getJabberExtension() != null){
          log.setExtention(e.getJabberExtension());
        }        
        if(e.getTransaksiStatus() != null){
          log.setStatus(e.getTransaksiStatus());
        }
        if(e.getTolakMdId() != null){
          log.setTolakMdId(e.getTolakMdId());
        }
        if(e.getDroppedDate() != null){
          log.setDropped_date(e.getDroppedDate());
        }
        if(e.getQrNoRekening() != null){
          log.setNoRekening(e.getQrNoRekening());
        }
        if (e.getUsername() != null) {
          log.setUsername(e.getUsername());
        }
        if (e.getNoRef() != null) {
          log.setNoRef(e.getNoRef());
        }
        if (e.getApprovalEndDate() != null) {
          log.setApproveDate(e.getApprovalEndDate());
        }
        if (e.getApprovalUsername() != null) {
          log.setUsernameSPV(e.getApprovalUsername());
        }
      } catch (IOException er) {
      }
      return log;
    }).collect(Collectors.toList());

    ReportLogPerubahan rpl = new ReportLogPerubahan();
    rpl.setLog(rp);

    return rpl;

  }

}