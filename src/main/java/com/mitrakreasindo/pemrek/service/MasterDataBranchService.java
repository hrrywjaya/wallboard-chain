package com.mitrakreasindo.pemrek.service;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.model.MasterDataBranch;
import com.mitrakreasindo.pemrek.repository.MasterDataBranchRepository;
import com.mitrakreasindo.pemrek.repository.specs.MasterDataBranchSpecification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MasterDataBranchService {

	@Autowired
	private MasterDataBranchRepository masterDataBranchRepository;

	public List<MasterDataBranch> findAll() {
		return masterDataBranchRepository.findAll();
	}

	public List<MasterDataBranch> findAll(MasterDataBranchSpecification masterDataBranchSpecification) {
		return masterDataBranchRepository.findAll(masterDataBranchSpecification);
	}	

	public Page<MasterDataBranch> paginationCustom(MasterDataBranchSpecification masterDataBranchSpecification, Pageable pageable) {
		return masterDataBranchRepository.findAll(masterDataBranchSpecification, pageable);
	}

	public Page<MasterDataBranch> pagination(String branchName, Pageable pageable) {
		return masterDataBranchRepository.findByBranchName(branchName, pageable);
	}

	public Optional<MasterDataBranch> findById(String id) {
		return masterDataBranchRepository.findById(id);
	}

	public Optional<MasterDataBranch> update(MasterDataBranch masterDataBranch) {
		String id = Optional.of(masterDataBranchRepository.save(masterDataBranch)).map(v -> v.getId()).get();
		return masterDataBranchRepository.findById(id);
	}

	public Optional<MasterDataBranch> findOneByBranchCode(String branchCode) {
		return masterDataBranchRepository.findOneByBranchCode(branchCode);
	}

}
