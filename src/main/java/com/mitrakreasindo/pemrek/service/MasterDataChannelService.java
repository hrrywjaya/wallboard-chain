package com.mitrakreasindo.pemrek.service;

import java.util.List;

import com.mitrakreasindo.pemrek.model.MasterDataChannel;
import com.mitrakreasindo.pemrek.repository.MasterDataChannelRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataChannelService {

	@Autowired
	private MasterDataChannelRepository masterDataChannelRepository;

	public List<MasterDataChannel> findAll() {
		return masterDataChannelRepository.findAll();
	}

}
