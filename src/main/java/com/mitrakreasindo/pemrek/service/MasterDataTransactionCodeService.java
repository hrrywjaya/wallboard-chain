package com.mitrakreasindo.pemrek.service;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.MasterDataTransactionCode;
import com.mitrakreasindo.pemrek.repository.MasterDataTransactionCodeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataTransactionCodeService {

	@Autowired
    private MasterDataTransactionCodeRepository masterDataTransactionCodeRepository;

	public List<MasterDataTransactionCode> findAll() {
		return masterDataTransactionCodeRepository.findAll();
	}

	public Optional<MasterDataTransactionCode> findByText(String text){
		return masterDataTransactionCodeRepository.findByText(text);
  }
  
  public MasterDataTransactionCode findCode(String text){
		return masterDataTransactionCodeRepository.findCode(text);
	}

}
