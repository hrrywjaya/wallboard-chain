package com.mitrakreasindo.pemrek.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.mitrakreasindo.pemrek.graphql.dto.FotoInput;
import com.mitrakreasindo.pemrek.graphql.dto.TransaksiDto;
import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.repository.TransaksiRepository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import graphql.schema.DataFetcher;

// public interface TransaksiService {

// 	List<Transaksi> getAllTransaksis(int count);

// 	Optional<Transaksi> getTransaksi(String id);

// 	Transaksi createTransaksi(String type, String modelCode, String brandName, String launchDate);

// }

@Service
public class TransaksiService {

	private static final Logger logger = Logger.getLogger(TransaksiService.class.getName());

	@Autowired
	private TransaksiRepository transaksiRepository;


	// @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
	// private int batchSize;

	// @PersistenceContext
	// private EntityManager entityManager;

	// public static Specification<Transaksi> customerHasBirthday() {
	// 	return (root, query, cb) -> {
	// 		return cb.equal(root.get("Transaksi_.birthday"), today);
	// 	};
	// }

	// public static Specification<Transaksi> isLongTermCustomer() {
	// return (root, query, cb) -> {
	// return cb.lessThan(root.get("Transaksi_.createdAt"), new
	// LocalDate.minusYears(2));
	// };
	// }

	public List<Transaksi> findAll() {
		return transaksiRepository.findAll();
	}

	// public List<TransaksiDto> findAll() {
	// 	// List<Transaksi> tbDto = new ArrayList<Transaksi>();
	// 	// tbDto.addAll(transaksiRepository.findAll());
	// 	// return transaksiRepository.findAll();
	// 	List<TransaksiDto> tbDtos = new ArrayList<TransaksiDto>();
	// 	transaksiRepository.findAll()
	// 	.stream().parallel()
	// 	.forEach(e -> {
	// 		TransaksiDto tbDto = modelMapper.map(e, TransaksiDto.class);
	// 		tbDtos.add(tbDto);
	// 	});
	// 	// .collect(Collectors.toList());
	// 	return tbDtos;
	// 	// return tbDto;
	// }


	public List<Transaksi> findAllById(List<String> keys) {
		// List<Transaksi> tbDto = new ArrayList<Transaksi>();
		// tbDto.addAll(transaksiRepository.findAll());
		// return transaksiRepository.findAll();
		return transaksiRepository.findAllById(keys);
		// return transaksiRepository.findAllById(keys).stream().map(e -> modelMapper.map(e, TransaksiDto.class)).collect(Collectors.toList());
		// return tbDto;
	}

	// public Optional<TransaksiDto> findOneByZeRefNoRef(String noRef) {
	// 	// Example<Transaksi> example = Example.of(Transaksi.from());

	// 	// Optional<Transaksi> actual = transaksiRepository.findOne(example);
	// 	// return transaksiRepository.findByZeRefsNoRef(noRef).stream().findFirst();
	// 	// return transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst();

	// 	// List<Transaksi> t = transaksiRepository.findByZeRefNoRef("AAAAAAAA");
	// 	Type listType = new TypeToken<List<TransaksiDto>>() {
	// 	}.getType();
	// 	List<Transaksi> t = transaksiRepository.findByZeRefNoRef("AAAAAAAA").stream().collect(Collectors.toList());
	// 	List<TransaksiDto> tbDto = new ArrayList<TransaksiDto>();
	// 	// final List<TransaksiDto> tbDto = modelMapper.map(t, listType);
	// 	tbDto.addAll(modelMapper.map(t, listType));
	// 	Optional<TransaksiDto> s = tbDto.stream().findFirst();
	// 	// logger.log(Level.INFO, "Flushing the EntityManager containing {0} entities
	// 	// ...", 222);
	// 	return s;
	// }

	// public Optional<Transaksi> findOneByZeRefNoRef(String noRef) {
	// 	// Optional<TransaksiDto> tbDto = transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst().map(e -> modelMapper.map(e, TransaksiDto.class));
	// 	// Optional<Transaksi> t = transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst();
	// 	return transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst();
	// 	// return tbDto;
	// }

	// public Optional<Transaksi> findOneByZeRefId(String id) {
	// 	// Optional<TransaksiDto> tbDto = transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst().map(e -> modelMapper.map(e, TransaksiDto.class));
	// 	// Optional<Transaksi> t = transaksiRepository.findByZeRefNoRef(noRef).stream().findFirst();
	// 	return transaksiRepository.findByZeRefsId(id).stream().findFirst();
	// 	// return tbDto;
	// }

	public Transaksi save(Transaksi transaksi) {
		Transaksi t = new Transaksi();
		// transaksi.getZeRefs().stream().forEach(x -> {
		// 	ZeRef zr = new ZeRef();
		// 	zr.setNoRef(x.getNoRef());
		// 	t.addZeRefs(zr);
		// });
		transaksi.getReferensis().stream().forEach(x -> {
			Referensi r = new Referensi();
			r.setNoRef(x.getNoRef());
			t.addReferensis(r);
		});
		return transaksiRepository.save(t);
	}

	public Optional<Transaksi> findById(String id) {
		return transaksiRepository.findById(id);
	}

	// public Transaksi updateTransaksi(Transaksi t) {
	// 	// Transaksi t = transaksiRepository.findById(transaksiId).get();
	// 	// Foto foto = new Foto();
	// 	// t.addFotos(foto);
	// 	return transaksiRepository.save(t);
		
	// 	// return transaksiRepository.findByZeRefsId(id).stream().findFirst();
	// 	// return tbDto;
	// }


	public Optional<Transaksi> update(Transaksi transaksi) {
		return Optional.of(transaksiRepository.save(transaksi));
	}

	public Optional<Transaksi> findOneTransaksiByReferensi(String noRef) {
		return transaksiRepository.findOneTransaksiByReferensi(noRef).stream().findFirst();
  }
  
 
  
  

}