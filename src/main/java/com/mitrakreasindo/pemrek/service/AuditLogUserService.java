package com.mitrakreasindo.pemrek.service;

import java.util.List;
import java.util.Optional;


import com.mitrakreasindo.pemrek.model.AuditLogUser;
import com.mitrakreasindo.pemrek.repository.AuditLogUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AuditLogUserService {

	@Autowired
	private AuditLogUserRepository auditLogUserRepository;

	public List<AuditLogUser> findAll() {
		return auditLogUserRepository.findAll();
	}

	public Optional<AuditLogUser> save(AuditLogUser auditLogUser) {
		return Optional.of(auditLogUserRepository.save(auditLogUser));
	}
}