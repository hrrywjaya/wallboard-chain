package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.transaction.Transactional;

import com.mitrakreasindo.pemrek.model.Foto;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.repository.FotoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import graphql.schema.DataFetchingEnvironment;

@Service
public class FotoService {

	@Autowired
	private FotoRepository fotoRepository;

	// public List<Foto> findAll() {
	// 	return fotoRepository.findAll();
	// }

	// // public List<Foto> findAllFotosForTransaksi(Transaksi transaksi) {
	// // List<Foto> z = fotoRepository.findByTransaksiId(transaksi.getId());

	// // return z.stream().map(e ->
	// // e).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

	// // }

	// public List<Foto> findByTransaksiId(String transaksiId) {
	// 	// return
	// 	// this.transaksiRepository.findAll().stream().limit(count).collect(Collectors.toList());
	// 	return fotoRepository.findByTransaksiId(transaksiId)
	// 	.parallelStream().collect(Collectors.toList());
	// }

	// public List<Foto> loadFotosById(List<String> userIds) {
	// 	return fotoRepository.findAllById(userIds);
	// }

	// // @Transactional
	// // public List<Foto> findByIds(final List<Integer> userIds, DataFetchingEnvironment env) {
	// // 	if (userIds.size() == 0) {
	// // 		return Collections.emptyList();
	// // 	}
	// // 	CriteriaBuilder builder = getSession().getCriteriaBuilder();
	// // 	// CriteriaBuilder builder = env.getContext().getCriteriaBuilder();
	// // 	CriteriaQuery<Foto> query = builder.createQuery(Foto.class);
	// // 	Root<Foto> root = query.from(Foto.class);
	// // 	// SingularAttribute a

	// // 	// SetJoin<Foto, Transaksi> itemNode = cq.from(Foto.class).join(Order_.items);
	// // 	env.getQueryDirectives();

	// // 	query.where(root.get(Foto_.id).in(userIds));
	// // 	return query.createQuery(query).getResultList();
	// // }


	public List<Foto> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return fotoRepository.findAllById(keys);
		
	}

	public Optional<Foto> findFirstByTransaksiIdOrderByCreatedDateDesc(String id) {
		return fotoRepository.findFirstByTransaksiIdOrderByCreatedDateDesc(id);
	}

	public Foto add(Foto foto) {
		// Foto f = new Foto();
		// foto.setTransaksi(transaksi);
		return fotoRepository.save(foto);
	}
}