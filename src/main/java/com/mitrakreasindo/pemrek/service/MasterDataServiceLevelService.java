package com.mitrakreasindo.pemrek.service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.MasterDataServiceLevel;

/**
 * @author miftakhul
 *
 */
public interface MasterDataServiceLevelService extends BaseService<MasterDataServiceLevel>
{

    List<MasterDataServiceLevel> findAll();
}
