package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.repository.MasterDataPekerjaanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataPekerjaanService {

	@Autowired
	private MasterDataPekerjaanRepository mdPekerjaanRepository;

	public Optional<MasterDataPekerjaan> findOneByDescription(String description) {
		return mdPekerjaanRepository.findOneByDescription(description);
	}
}

