package com.mitrakreasindo.pemrek.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.core.exception.BadRequestException;
import com.mitrakreasindo.pemrek.core.exception.BaseExceptionCode;
import com.mitrakreasindo.pemrek.core.model.Account;
import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.CurrentPrincipal;
import com.mitrakreasindo.pemrek.repository.AccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public Account findByUsername(String username) {
		return accountRepository.findByUsernameAndActiveFlag(username, Active.ACTIVE);
	}

	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	// public List<Account> findByIds(List<String> keys) {
	// if (keys.size() == 0) {
	// return Collections.emptyList();
	// }
	// return accountRepository.findAllById(keys);

	// }

	public Account add(Account account) {
		String password = passwordEncoder.encode(account.getPassword());
		account.setPassword(password);
		// return accountRepository.save(account);
		String id = Optional.of(accountRepository.save(account)).map(v -> v.getId()).get();
		return accountRepository.findById(id).get();
	}

	public Optional<Account> findOneByUsername(String username) {
		return accountRepository.findByUsername(username);
	}
	
	public Optional<Account> findOneFullnameByUsername(String username) {
		return accountRepository.findByUsername(username);
	}

	public List<Account> findByIds(List<String> keys) {
		if (keys.size() == 0) {
			return Collections.emptyList();
		}
		return accountRepository.findAllById(keys);

	}

	public Page<Account> pagination(String username, Active activeFlag, Pageable pageable) {
		// Pageable pageable = PageRequest.of(0, 10);
		// Page<Account> page = accountRepository.findAll(pageable);
		// return new ArrayList<Account>();
		// return accountRepository.findAll(pageable);
		if (activeFlag.equals(Active.ALL))
			return accountRepository.findByUsername(username, pageable);
		else
			return accountRepository.findByUsernameAndActiveFlag(username, activeFlag, pageable);
	}

	public Page<Account> paginations(String username, Active activeFlag, Pageable pageable) {
		// Pageable pageable = PageRequest.of(0, 10);
		// Page<Account> page = accountRepository.findAll(pageable);
		// return new ArrayList<Account>();
		// return accountRepository.findAll(pageable);
		if (activeFlag.equals(Active.ALL))
			return accountRepository.findByUsernameAndRolename(username, pageable);
		else
			return accountRepository.findByUsernameAndActiveFlagAndRolename(username, activeFlag, pageable);
	}
	
	public Optional<Account> findById(String id) {
		return accountRepository.findById(id);
	}

	public Optional<Account> update(Account account) {
		// Account a = Optional.of(accountRepository.save(account)).orElseThrow(
		// 	() -> new BadRequestException(BaseExceptionCode.COMMON_ERROR_RESOURCE_NOT_FOUND, "update error")
		// );
		// Optional<Account> aa = accountRepository.findById(a.getId());
		// accountRepository.getOne(account.getId)
		// return aa;
		String id = Optional.of(accountRepository.save(account)).map(v -> v.getId()).get();
		return accountRepository.findById(id);
	}

	public Account getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
//			CurrentPrincipal principal = (CurrentPrincipal) auth.getPrincipal();
//			return principal.getAccount();
			return findByUsername(auth.getName());
		}
		return null;
	}

	public List<Account> allAccountByRoleId(String id) {
		return accountRepository.findByRoleId(id);
	}
	
	public List<Account> allAccountByRoleName(String roleName) {
		return accountRepository.findByRoleName(roleName);
	}


}
