package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.MasterDataPekerjaan;
import com.mitrakreasindo.pemrek.model.Pesan;
import com.mitrakreasindo.pemrek.repository.PesanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PesanService {

	@Autowired
	private PesanRepository pesanRepository;

	public List<Pesan> findByCreatedDate(String date) {
		return pesanRepository.findByCreatedDate(date);
	}

	public Pesan add(Pesan pesan) {
		return pesanRepository.save(pesan);
	}
}

