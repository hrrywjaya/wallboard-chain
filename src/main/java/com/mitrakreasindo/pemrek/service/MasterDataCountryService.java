package com.mitrakreasindo.pemrek.service;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.MasterDataCountry;
import com.mitrakreasindo.pemrek.repository.MasterDataCountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataCountryService {

	@Autowired
	private MasterDataCountryRepository masterDataCountryRepository;

	public List<MasterDataCountry> findAll() {
		return masterDataCountryRepository.findAll();
	}

	public Optional<MasterDataCountry> findById(String id) {
		return masterDataCountryRepository.findById(id);
	}

	public Optional<MasterDataCountry> findOneByCountryCodeIso(String iso) {
		return masterDataCountryRepository.findOneByCountryCodeIso(iso);
	}

}
