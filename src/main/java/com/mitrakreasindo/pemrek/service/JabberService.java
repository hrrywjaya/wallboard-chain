package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.model.Jabber;
import com.mitrakreasindo.pemrek.repository.JabberRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JabberService {

	@Autowired
	private JabberRepository jabberRepository;


	public List<Jabber> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return jabberRepository.findAllById(keys);
		
	}

	public Optional<Jabber> findByIpAddress(String ipAddress) {
		return jabberRepository.findByIpAddress(ipAddress);
	}
}