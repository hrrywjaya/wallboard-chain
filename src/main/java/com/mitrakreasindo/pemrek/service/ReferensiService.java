package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.repository.ReferensiRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

@Service
public class ReferensiService
{

	@Autowired
	private ReferensiRepository referensiRepository;

	
	public Referensi create(Referensi referensi) {
		return referensiRepository.save(referensi);
	}

	public List<Referensi> findAll() {
		return referensiRepository.findAll();
	}

	public List<Referensi> findByTransaksiId(String transaksiId) {
		// return
		// this.transaksiRepository.findAll().stream().limit(count).collect(Collectors.toList());
		return referensiRepository.findByTransaksiId(transaksiId)
		.parallelStream().collect(Collectors.toList());
	}

	public List<Referensi> loadZeRefsById(List<String> userIds) {
		return referensiRepository.findAllById(userIds);
	}

	public List<Referensi> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return referensiRepository.findAllById(keys);
		
	}
	
	public Referensi findOneLastReferensiByNoRef(String noRef) {
		return referensiRepository.findOneLastByNoRef(noRef).findFirst().orElse(null);
	}
	
}
