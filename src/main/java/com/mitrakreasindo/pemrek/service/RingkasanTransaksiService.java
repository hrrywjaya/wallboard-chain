package com.mitrakreasindo.pemrek.service;

import java.util.List;

import com.mitrakreasindo.pemrek.model.RingkasanTransaksiData;
import com.mitrakreasindo.pemrek.model.TransactionMessage.TransactionMessageStatus;
import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.transaction.dto.RetryOutputSchema;

public interface RingkasanTransaksiService
{
	
	RingkasanTransaksiData process(String noRef, String callLogId, String username);

	void processAsync(RingkasanTransaksiData ringkasanTransaksiData, String callLogId);

	Output<RetryOutputSchema> processRetry(String noRef, String norRefCretedDate, String username);
	
	List<TransactionMessageStatus> buildStatusProgress(String noRef, boolean isNasabahExisting, boolean isNrt, boolean isChangeProduct);
		
}
