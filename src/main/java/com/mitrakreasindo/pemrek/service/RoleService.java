package com.mitrakreasindo.pemrek.service;

import java.util.List;
import java.util.Optional;

import com.mitrakreasindo.pemrek.core.model.Active;
import com.mitrakreasindo.pemrek.core.model.Role;
import com.mitrakreasindo.pemrek.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public List<Role> findRoleByName() {
        return roleRepository.findRoleByName();
    }
    
    public Optional<Role> findById(String id) {
        return roleRepository.findById(id);
    }

    public Page<Role> pagination(String name, Pageable pageable) {
        return roleRepository.findByName(name, pageable);
    }

    // public Role add(Role role) {
    // return roleRepository.save(role);
    // }

    public Optional<Role> add(Role role) {
        return Optional.of(roleRepository.save(role));
    }

    public Optional<Role> findOneByName(String name) {
        return roleRepository.findOneByName(name);
    }

    // @Transactional
    // public Optional<Role> replace(Role role) {
    //     String id = Optional.of(roleRepository.save(role)).map(v -> v.getId()).get();
    //     return roleRepository.findById(id);
    // }

    // @Transactional
    public Optional<Role> replace(Role role) {
        // String id = Optional.of(roleRepository.save(role)).map(v -> v.getId()).get();
        return Optional.of(roleRepository.save(role));
    }

    public Optional<Role> save(Role role) {
        return Optional.ofNullable(roleRepository.save(role));
    }

	public String remove(String id) {
        roleRepository.deleteById(id);
        return id;
	}

    // public Long deleteRoleMenus(String id) {
    //     // String id = Optional.of(roleRepository.save(role)).map(v -> v.getId()).get();
    //     return roleRepository.deleteRoleMenusById(id);
    // }

}