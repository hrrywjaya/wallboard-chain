package com.mitrakreasindo.pemrek.service;

import java.util.List;

import com.mitrakreasindo.pemrek.core.service.base.BaseService;
import com.mitrakreasindo.pemrek.model.MasterDataTerputus;

/**
 * @author miftakhul
 *
 */
public interface MasterDataTerputusService extends BaseService<MasterDataTerputus>
{
    List<MasterDataTerputus> findAll();
}
