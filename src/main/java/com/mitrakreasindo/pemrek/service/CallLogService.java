package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.mitrakreasindo.pemrek.model.CallLog;
import com.mitrakreasindo.pemrek.repository.CallLogRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import graphql.schema.DataFetchingEnvironment;

@Service
public class CallLogService {

	@Autowired
	private CallLogRepository callLogRepository;

	public List<CallLog> findAll() {
		return callLogRepository.findAll();
	}

	public List<CallLog> findByIds(List<String> keys) {
		if (keys.size() == 0) {
			return Collections.emptyList();
		}
		return callLogRepository.findAllById(keys);

	}

	public Optional<CallLog> save(CallLog callLog) {
		return Optional.of(callLogRepository.save(callLog));
	}

	public Optional<CallLog> findById(String id) {
		return callLogRepository.findById(id);
	}

	public List<CallLog> allCallLogsByNoRef(String noRef) {
		return callLogRepository.findByNoRef(noRef);
	}

	public List<CallLog> allTolaksByQrNik(String qrNik) {
		return callLogRepository.findAllTolakByQrNik(qrNik);
	}

	public List<CallLog> getLogPerubahan(String noRef) {
		return callLogRepository.getLogPerubahan(noRef);
  }

  public List<CallLog> getLogPerubahanByParam(String param, String startDate, String endDate) {
		return callLogRepository.getLogPerubahanByParam(param, startDate, endDate);
	}

	public List<CallLog> getLogPerubahanByDate(String startDate, String endDate) {
		return callLogRepository.getLogPerubahanByDate(startDate, endDate);
	}

	public List<CallLog> allTerputusesByNoRef(String noRef) {
		return callLogRepository.findAllTerputusByNoRef(noRef);
	}

  public List<CallLog> findAllEdd(){
    return callLogRepository.findAllEdd();
  }
  
  public List<CallLog> allEddApprovals() {
	  return callLogRepository.findAllEddApproval();
  }

  public List<CallLog> findApprovalReport(String startDate,String endDate,String channel,String rekening,String cabang,String agent,String status){
    return callLogRepository.findApprovalReport(startDate, endDate, channel, rekening, cabang, agent, status);
  }

  public List<CallLog> findApprovalReportStatusWaiting(String startDate,String endDate,String channel,String rekening,String cabang,String agent){
    return callLogRepository.findApprovalReportStatusWaiting(startDate, endDate, channel, rekening, cabang, agent);
  }

  public List<CallLog> findApprovalReportStatusAll(String startDate,String endDate,String channel,String rekening,String cabang,String agent){
    return callLogRepository.findApprovalReportStatusAll(startDate, endDate, channel, rekening, cabang, agent);
  }

  // public List<CallLog> findApprovalReport(String startDate,String endDate, String channel,String rekening,String cabang){
  //   return callLogRepository.findApprovalReport(startDate, endDate, channel,rekening,cabang);
  // }

  public Optional<CallLog> findLastByNoRefAndNoRefCreatedDate(String noref, String norefCreatedDate) {
	  return callLogRepository.findLastByNoRefAndNoRefCreatedDate(noref, norefCreatedDate);
  }

}