package com.mitrakreasindo.pemrek.service;

import java.util.Collections;
import java.util.List;

import com.mitrakreasindo.pemrek.graphql.dto.MenuFlatDto;
import com.mitrakreasindo.pemrek.model.Menu;
import com.mitrakreasindo.pemrek.repository.MenuRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuService
{

	@Autowired
	private MenuRepository menuRepository;	

	public List<Menu> findAll() {
		return menuRepository.findAll();
	}

	public List<Menu> findByIds(List<String> keys) {
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return menuRepository.findAllById(keys);
		
	}

	public List<Menu> findByChildsIdIn(List<String> keys) {
		// if (id == null) {
        //     return Collections.emptyList();
		// }
		if (keys.size() == 0) {
            return Collections.emptyList();
		}
		return menuRepository.findByChildsIdIn(keys);
		
	}
	
	public List<Menu> findAllFlat() {
		return menuRepository.findAllFlat();
	}
	
}
