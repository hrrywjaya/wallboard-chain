package com.mitrakreasindo.pemrek.service;

import org.springframework.stereotype.Service;

@Service
public class ReferenceNumberExtractor {

    public static enum ChannelType {
        BCA_CO_ID, BCA_MOBILE
    }

    public ChannelType parseToChannel(String referenceNumber) {
        String channel = referenceNumber.substring(0, 1);
        if ("M".equals(channel))
            return ChannelType.BCA_MOBILE;
        else if ("A".equals(channel))
            return ChannelType.BCA_CO_ID;
        
            return null;
    }

    public String parseToChannelId(String referenceNumber) {
        return referenceNumber.substring(0, 1);
    }

}