package com.mitrakreasindo.pemrek.model;

import java.util.List;

import lombok.Data;

@Data
public class ReportLogPerubahan {
  private List<ReportLog> log;

} 