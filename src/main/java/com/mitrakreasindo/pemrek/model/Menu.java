package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mitrakreasindo.pemrek.core.model.Auditable;
import com.mitrakreasindo.pemrek.core.model.Role;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
// @EqualsAndHashCode(exclude = { "childs", "acls", "roles" })
@EqualsAndHashCode(exclude = { "childs", "acls", "roleMenus" })
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Menu extends Auditable<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "transaksi_id")
    // @JsonBackReference
    // private Transaksi transaksi;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    // @JsonBackReference
    // @JsonManagedReference
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private Menu parent;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "menu_acls", joinColumns = @JoinColumn(name = "menu_id"), inverseJoinColumns = @JoinColumn(name = "acl_id"))
    // @JsonManagedReference
    // @JsonBackReference
    private Set<Acl> acls = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent", cascade = CascadeType.ALL)
    // @JsonManagedReference
    // @JsonBackReference
    private Set<Menu> childs = new HashSet<Menu>();

    // @ManyToMany(fetch = FetchType.LAZY,mappedBy = "menus")
    // @JsonBackReference
    // private Set<Role> roles = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "menu", cascade = CascadeType.ALL)
    // @JsonManagedReference
    // @JsonBackReference
    private Set<RoleMenu> roleMenus = new HashSet<>();

    private String path;
    private String name;
    private String description;
    private String frontend;
}
