package com.mitrakreasindo.pemrek.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mitrakreasindo.pemrek.core.model.base.BaseModel;

import lombok.Data;

@Data
@Table(name="MD_PEMREK_SERVICE_LEVEL")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class MasterDataServiceLevel extends BaseModel {

	private static final long serialVersionUID = 1L;	

	// @JsonIgnore
	// @OneToMany(mappedBy = "masterDataServiceLevel")
	// private List<ServiceLevel> serviceLevels;

	private String title;
	
	private String description;

}
