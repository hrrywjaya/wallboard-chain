// package com.mitrakreasindo.pemrek.model;

// import java.util.ArrayList;
// import java.util.List;

// import javax.persistence.CascadeType;
// import javax.persistence.Column;
// import javax.persistence.Entity;
// import javax.persistence.EntityListeners;
// import javax.persistence.EnumType;
// import javax.persistence.Enumerated;
// import javax.persistence.Lob;
// import javax.persistence.ManyToOne;
// import javax.persistence.OneToMany;
// import javax.persistence.OneToOne;
// import javax.persistence.PrePersist;
// import javax.persistence.PreUpdate;
// import javax.validation.constraints.NotNull;

// import org.springframework.data.jpa.domain.support.AuditingEntityListener;

// import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
// import com.mitrakreasindo.pemrek.core.model.Account;
// import com.mitrakreasindo.pemrek.core.model.base.BaseModel;

// import lombok.Data;

// @Data
// @Entity
// @EntityListeners(AuditingEntityListener.class)
// public class TransaksiBaru extends BaseModel {

// 	private static final long serialVersionUID = 1L;
	
// 	public static enum TransactionStatus {
// 		UPDATE_STATUS_IN_PROGRESS,
// 		GENERATE_EFORM,
// 		SAVE_EFORM,
// 		UPDATE_NO_REF_TO_CHANNEL,
// 		UPDATE_NO_REF_TO_PREDIN,
// 		UPDATE_CIS,
// 		FIRST_ACCOUNT,
// 		SECOND_ACCOUNT,
// 		SAVE_EDD,
// 		UPDATE_EDD,
// 		SAVE_PREDIN_AND_DIR,
// 		UPDATE_COBRAND,
// 		DIGITAL_CARD_REGISTRATION		
// 	}

// 	@Column(nullable = false)
// 	@NotNull
// 	private String noTransaksi;

// 	private String callId;
// 	private String lastStatus;
// 	private String lastApiAction;
// 	private String lastStateAction;
// 	private String lastState;

// //	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<Referensi> referensis = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<ViLog> viLogs = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<Tolak> tolaks = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<Terputus> terputuses = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<CallHistory> callHistories = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<RingkasanPanggilan> ringkasanPanggilans = new ArrayList<>();

// 	@JsonIgnore
// 	@OneToOne(mappedBy="transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	// @JoinColumn(name = "service_level_id", referencedColumnName = "id")
// 	// @OneToOne(cascade=CascadeType.ALL,mappedBy="transaksiBaru")
// 	private ServiceLevel serviceLevel;

// 	@JsonIgnore
// 	@OneToMany(mappedBy = "transaksiBaru", cascade = CascadeType.ALL, orphanRemoval = true)
// 	private List<ChatLog> chatLogs = new ArrayList<>();

// 	@ManyToOne
// 	@JsonIgnoreProperties({ "channel", "roles", "createdBy", "createdDate", "updatedBy", "updatedDate" })
// 	private Account agent;
	
// //	@Lob
// 	@Column(columnDefinition="CLOB")
// 	private String rawEform;

// //	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	@Column(columnDefinition="CLOB")
// 	private String rawCis;
	
// 	@Column(columnDefinition="CLOB")
// 	private String rawKonfirmasiData;

// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] predinFoto;
// 	private String predinFotoChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] predinKtp;
// 	private String predinKtpChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] predinTtd;
// 	private String predinTtdChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] predinNpwp;
// 	private String predinNpwpChecked;

// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] dinFoto;
// 	private String dinFotoChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] dinKtp;
// 	private String dinKtpChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] dinTtd;
// 	private String dinTtdChecked;
// 	@Lob
// //	@Type(type = "org.hibernate.type.TextType")
// 	private byte[] dinNpwp;
// 	private String dinNpwpChecked;

// 	private Boolean cisIsExisting;
// 	private String cisType;
// 	private String cisCustomerNumber;
// 	// private String cisNamaNasabah;
// 	private String cisTanggalLahir;
// 	private String cisTempatLahir;
// 	private String cisNamaIbuKandung;
// 	private String cisStatusTempatTinggal;
// 	private String cisNamaJalan;
// 	private String cisKodePos;
// 	private String cisProvinsi;
// 	private String cisKabupaten;
// 	private String cisKecamatan;
// 	private String cisKelurahan;

// 	// private String nik;
// 	// private String namaNasabah;
// 	// private String tempatLahir;
// 	// private String tanggalLahir;
// 	// private String jenisKelamin;
// 	// private String namaIbukandung;
// 	// private String statusKawin;
// 	// private String agama;

// 	// private String namaJalan;
// 	// private String namaTempat;
// 	// private String kodePos;
// 	// private String kecamatan;
// 	// private String kelurahan;

// 	// private String pekerjaan;
// 	// private String sumberPenghasilan;
// 	// private String sumberPenghasilan1;
// 	// private String totalPenghasilan;
// 	// private String namaKantor;
// 	// private String jabatan;
// 	// private String bidangUsaha;

// 	// private String tujuanPemRek;
// 	// private String jenisRek;
// 	// private String tipeKartu;
// 	// private String jenisKartu;
// 	// private String fasilitas_klikbca;
// 	// private String fasilitas_keybca_ib;
// 	// private String fasilitas_mbca;
// 	// private String fasilitas_aktivasi_fin_mbca;
// 	// private String fasilitas_bbp;

// 	// private String kodeNegara;
// 	// private String nomorHp;
// 	// private String email;
// 	// private String cabang;
// 	// private String layananBahasa;

// 	// private String form_fatca;
// 	// private String form_aeoi;
// 	// private String wajib_fatca;
// 	// private String wp_negara_lain;
// 	// private String tin;
// 	// private String persetujuan;
// 	// private String media_info_sms;
// 	// private String media_info_mail;
// 	// private String media_info_phone;

// 	// private String negaraLahir;
// 	// private String nomorHpLuar;
// 	// private String negaraLuar;

// 	private String eddUserId;
// 	private String eddReasonEdd;
// 	private String eddCurrentAddressFromMonthyear;
// 	private String eddAdditionalAccountOrCreditcardInstitutionName;
// 	private String eddAdditionalAccountOrCreditcardFromMonthyear;
// 	private String eddForeignCompanyCountryCode1;
// 	private String eddForeignCompanyCountryCode2;
// 	private String eddForeignCompanyCountryCode3;
// 	private String eddWealthFromHeritance;
// 	private String eddWealthFromSaving;
// 	private String eddWealthFromBusiness;
// 	private String eddWealthFromGift;
// 	private String eddWealthFromSalary;
// 	private String eddWealthFromOthers;
// 	private String eddWealthFromOthersDecription;
// 	private String eddAdditionalInformation;

// 	// private String eddBlnAlamat;
// 	// private String eddThnAlamat;
// 	// private Boolean eddRekLain;
// 	// private String eddNamaRekLain;
// 	// private String eddBlnRekLain;
// 	// private String eddThnRekLain;
// 	// private String eddUsahaLn;
// 	// private String eddNegaraUsahaLn;
// 	// private Boolean eddSumberWarisan;
// 	// private Boolean eddSumberTabungan;
// 	// private Boolean eddSumberHasilUsaha;
// 	// private Boolean eddSumberHibah;
// 	// private Boolean eddSumberGaji;
// 	// private Boolean eddSumberLainnya;
// 	// private String eddPEP;
// 	// private String eddInfoLainnya;

// 	private Boolean isFinish;

// 	// private String ref_num;
// 	// private String status;
// 	// private String TIPE_KARTU_PASPOR_BCA;
// 	// private String NAMA_TEMPAT_BEKERJA;
// 	// private String NO_CUSTOMER_REKENING_GABUNGAN;
// 	// private String FASILITAS_YANG_DIINGINKAN_FIN_MBCA;
// 	// private String SERIAL_NUMBER_TOKEN;
// 	// private String NAMA_JALAN;
// 	// private String ALAMAT_KANTOR_3;
// 	// private String BERLAKU_SAMPAI_DENGAN;
// 	// private String STATUS_PERKAWINAN;
// 	// private String PROVINSI_REKENING_KORAN;
// 	// private String VALIDASI_CATATAN_BANK_KATEGORI_NASABAH;
// 	// private String STATUS_REKENING;
// 	// private String TIPE_IDENTITAS_LAINNYA;
// 	// private String NO_CUSTOMER;
// 	// private String NOMOR_TELEPON_RUMAH;
// 	// private String AGAMA;
// 	// private String ALAMAT_KANTOR_1;
// 	// private String ALAMAT_KANTOR_2;
// 	// private String KD_PENDUDUK;
// 	// private String OD_PLAN;
// 	// private String SERVICE_CHARGE_CODE;
// 	// private String FORM_PERPAJAKAN_CRS;
// 	// private String TEMPAT_LAHIR;
// 	// private String PERNYATAAN_FASILITAS_BCA;
// 	// private String PERSETUJUAN_TELEPON;
// 	// private String WITHOLDING_PLAN;
// 	// private String JABATAN;
// 	// private String NOMOR_HP;
// 	// private String NIK_REKENING_GABUNGAN;
// 	// private String NAMA_GEDUNG;
// 	// private String NPWP;
// 	// private String SUMBER_PENGHASILAN;
// 	// private String FASILITAS_YANG_DIINGINKAN_MBCA;
// 	// private String KODE_AREA_TELEPON_KANTOR;
// 	// private String PERSETUJUAN_SMS;
// 	// private String NAMA_GADIS_IBU_KANDUNG;
// 	// private String NAMA_DICETAK_DI_KARTU;
// 	// private String COST_CENTER;
// 	// private String KABUPATEN_REKENING_KORAN;
// 	// private String NAMA_REKENING_GABUNGAN;
// 	// private String INTEREST_PLAN;
// 	// private String KODE_NEGARA_FAKSIMILIE;
// 	// private String DENGAN_BUKU;
// 	// private String SUMBER_PENGHASILAN_LAINNYA;
// 	// private String DALAM_HAL_INI_BERTINDAK;
// 	// private String FASILITAS_YANG_DIINGINKAN_KLIKBCA;
// 	// private String NOMOR_NPWP;
// 	// private String KITAS_KITAP;
// 	// private String KODE_NEGARA_NOMOR_HP_2;
// 	// private String NOMOR_FAKSIMILIE;
// 	// private String KODE_NEGARA_NOMOR_HP;
// 	// private String PERNYATAAN_PRODUK;
// 	// private String NOMOR_REKENING_BARU;
// 	// private String FORM_PERPAJAKAN_FATCA;
// 	// private String BERLAKU_SAMPAI_DENGAN_KITAS_KITAP;
// 	// private String KECAMATAN;
// 	// private String PERSETUJUAN_EMAIL;
// 	// private String FASILITAS_YANG_DIINGINKAN_KEYBCA;
// 	// private String KELURAHAN_REKENING_KORAN;
// 	// private String BAHASA_PETUNJUK_LAYAR_ATM;
// 	// private String JENIS_KELAMIN;
// 	// private String NEGARA_LAHIR_FATCA;
// 	// private String PERSETUJUAN_DATA_PIHAK_KETIGA;
// 	// private String EMAIL;
// 	// private String NAMA_GEDUNG_REKENING_KORAN;
// 	// private String JENIS_KARTU_PASPOR_BCA;
// 	// private String TOTAL_PENGHASILAN;
// 	// private String CABANG;
// 	// private String KODE_AREA_TELEPON_RUMAH;
// 	// private String KODE_NEGARA_DATA_REKENING;
// 	// private String NOMOR_HP_MBCA;
// 	// private String KODE_NEGARA_TELEPON_KANTOR;
// 	// private String KECAMATAN_REKENING_KORAN;
// 	// private String PERNYATAAN_PASPOR_BCA;
// 	// private String NOMOR_REKENING_EXISTING;
// 	// private String KODE_POS_REKENING_KORAN;
// 	// private String KELURAHAN;
// 	// private String REKENING_UNTUK;
// 	// private String WAJIB_PAJAK_NEGARA_LAIN;
// 	// private String NAMA_NASABAH;
// 	// private String PEKERJAAN;
// 	// private String NOMOR_KITAS_KITAP;
// 	// private String PERIODE_BIAYA_ADMIN;
// 	// private String PERNYATAAN_FASILITAS_BCA_DETAIL;
// 	// private String TIN_SSN;
// 	// private String KOTA_KANTOR;
// 	// private String PEJABAT_BANK;
// 	// private String KABUPATEN;
// 	// private String NEGARA;
// 	// private String TUJUAN_PEMBUKAAN_REKENING;
// 	// private String BIAYA_ADMIN;
// 	// private String NEGARA_ALAMAT;
// 	// private String RT_RW;
// 	// private String RT_RW_REKENING_KORAN;
// 	// private String KODE_PERORANGAN_BISNIS;
// 	// private String GOLONGAN_PEMILIK;
// 	// private String BIDANG_USAHA;
// 	// private String KODE_POS;
// 	// private String KODE_PENAMBAHAN_PAJAK;
// 	// private String NOMOR_HP_2;
// 	// private String KODE_AREA_FAKSIMILIE;
// 	// private String PROVINSI;
// 	// private String PERIODE_R_K;
// 	// private String NEGARA_ALAMAT_REKENING_KORAN;
// 	// private String NIL;
// 	// private String USER_CODE;
// 	// private String NIK;
// 	// private String NAMA_JALAN_REKENING_KORAN;
// 	// private String PERIODE_BUNGA;
// 	// private String NOMOR_TELEPON_KANTOR;
// 	// private String PERNYATAAN_PRODUK_DETAIL;
// 	// private String TANGGAL_LAHIR;
// 	// private String KODE_POS_KANTOR;
// 	// private String KODE_NEGARA_TELEPON_RUMAH;
// 	// private String WAJIB_FATCA;
// 	// private String PENGIRIMAN_REKENING_KORAN;

// 	private String refNum;
// 	private String status;
// 	private String eformCreatedDate;
// 	private String eformCreatedTime;
// 	private String tipeKartuPasporBca;
// 	private String namaTempatBekerja;
// 	private String noCustomerRekeningGabungan;
// 	private String fasilitasYangDiinginkanFinMbca;
// 	private String serialNumberToken;
// 	private String namaJalan;
// 	private String alamatKantor3;
// 	private String berlakuSampaiDengan;
// 	private String statusPerkawinan;
// 	private String provinsiRekeningKoran;
// 	private String validasiCatatanBankKategoriNasabah;
// 	private String statusRekening;
// 	private String tipeIdentitasLainnya;
// 	private String noCustomer;
// 	private String nomorTeleponRumah;
// 	private String agama;
// 	private String alamatKantor1;
// 	private String alamatKantor2;
// 	private String kdPenduduk;
// 	private String odPlan;
// 	private String serviceChargeCode;
// 	private String formPerpajakanCrs;
// 	private String tempatLahir;
// 	private String pernyataanFasilitasBca;
// 	private String persetujuanTelepon;
// 	private String jenisRekening;
// 	private String witholdingPlan;
// 	private String jabatan;
// 	private String nomorHp;
// 	private String nikRekeningGabungan;
// 	private String namaGedung;
// 	private String npwp;
// 	private String sumberPenghasilan;
// 	private String fasilitasYangDiinginkanMbca;
// 	private String kodeAreaTeleponKantor;
// 	private String persetujuanSms;
// 	private String namaGadisIbuKandung;
// 	private String namaDicetakDiKartu;
// 	private String costCenter;
// 	private String kabupatenRekeningKoran;
// 	private String namaRekeningGabungan;
// 	private String interestPlan;
// 	private String kodeNegaraFaksimilie;
// 	private String denganBuku;
// 	private String sumberPenghasilanLainnya;
// 	private String dalamHalIniBertindak;
// 	private String fasilitasYangDiinginkanKlikbca;
// 	private String nomorNpwp;
// 	private String kitasKitap;
// 	private String kodeNegaraNomorHp2;
// 	private String nomorFaksimilie;
// 	private String kodeNegaraNomorHp;
// 	private String pernyataanProduk;
// 	private String nomorRekeningBaru;
// 	private String formPerpajakanFatca;
// 	private String berlakuSampaiDenganKitasKitap;
// 	private String kecamatan;
// 	private String persetujuanEmail;
// 	private String fasilitasYangDiinginkanKeybca;
// 	private String kelurahanRekeningKoran;
// 	private String bahasaPetunjukLayarAtm;
// 	private String jenisKelamin;
// 	private String negaraLahirFatca;
// 	private String persetujuanDataPihakKetiga;
// 	private String email;
// 	private String namaGedungRekeningKoran;
// 	private String jenisKartuPasporBca;
// 	private String totalPenghasilan;
// 	private String cabang;
// 	private String kodeAreaTeleponRumah;
// 	private String kodeNegaraDataRekening;
// 	private String nomorHpMbca;
// 	private String kodeNegaraTeleponKantor;
// 	private String kecamatanRekeningKoran;
// 	private String pernyataanPasporBca;
// 	private String nomorRekeningExisting;
// 	private String kodePosRekeningKoran;
// 	private String kelurahan;
// 	private String rekeningUntuk;
// 	private String wajibPajakNegaraLain;
// 	private String namaNasabah;
// 	private String pekerjaan;
// 	private String pekerjaanTier2;
// 	private String pekerjaanTier3;
// 	private String nomorKitasKitap;
// 	private String periodeBiayaAdmin;
// 	private String pernyataanFasilitasBcaDetail;
// 	private String tinSsn;
// 	private String kotaKantor;
// 	private String pejabatBank;
// 	private String kabupaten;
// 	private String negara;
// 	private String tujuanPembukaanRekening;
// 	private String biayaAdmin;
// 	private String negaraAlamat;
// 	private String rtRw;
// 	private String rtRwRekeningKoran;
// 	private String kodePeroranganBisnis;
// 	private String golonganPemilik;
// 	private String bidangUsaha;
// 	private String kodePos;
// 	private String kodePenambahanPajak;
// 	private String nomorHp2;
// 	private String kodeAreaFaksimilie;
// 	private String provinsi;
// 	private String periodeRK;
// 	private String negaraAlamatRekeningKoran;
// 	private String nil;
// 	private String userCode;
// 	private String nik;
// 	private String namaJalanRekeningKoran;
// 	private String periodeBunga;
// 	private String nomorTeleponKantor;
// 	private String pernyataanProdukDetail;
// 	private String tanggalLahir;
// 	private String kodePosKantor;
// 	private String kodeNegaraTeleponRumah;
// 	private String wajibFatca;
// 	private String pengirimanRekeningKoran;
// 	private String nasabahExisting;
// 	private String statusTahapanGold;

// 	private String nomorHpFinMbca;
// 	private String alasanResikoTinggi;
// 	private String tinggalAlamatTerakhirSejak;
// 	private String nasabahBankLain;
// 	private String namaBankLain;
// 	private String tanggalBergabungBankLain;
// 	private String hubunganUsahaLuarNegeri;
// 	private String negaraBerhubunganUsaha1;
// 	private String negaraBerhubunganUsaha2;
// 	private String negaraBerhubunganUsaha3;
// 	private String sumberKekayaanWarisan;
// 	private String sumberKekayaanTabungan;
// 	private String sumberKekayaanHasilUsaha;
// 	private String sumberKekayaanHibah;
// 	private String sumberKekayaanGaji;
// 	private String sumberKekayaanLainnya;
// 	private String sumberKekayaanLainnyaText;
// 	private String informasiLainnya;
	
// 	// *** ditanyakan
// 	private String formAeoi;
	
//   private String spvApproveKtp;
//   private String spvApproveFoto;
//   private String spvApproveTtd;
//   private String spvApproveNpwp;

//   	private Boolean isRefNumRegenerated;
// 	private Boolean isProdukEdited;
// 	private String pemrekAccountNumber;
// 	private String pemrekCisCustomerNumber;
// 	@Enumerated(EnumType.STRING)
// 	private TransactionStatus transactionStatus;
// 	private Boolean isTransactionStepFailed;
	
// 	@Column(columnDefinition="CLOB")
// 	private String transactionStatusMap;
	
// 	@PrePersist
// 	public void onPrePersist() {
// 		// for (Referensi r : this.referensis) {
// 		// 	r.setTransaksiBaru(this);
// 		// }
// 	}

// 	@PreUpdate
// 	public void onPreUpdate() {
// 		// for (Referensi r : this.referensis) {
// 		// 	r.setTransaksiBaru(this);
// 		// }
// 	}

// 	// @PreRemove
// 	// public void onPreRemove() { ... }

// 	public void addReferensi(Referensi referensi) {
// 		referensis.add(referensi);
// 		referensi.setTransaksiBaru(this);
// 	}

// 	public void removeReferensi(Referensi referensi) {
// 		referensis.remove(referensi);
// 		referensi.setTransaksiBaru(null);
// 	}

// 	public void addTolak(Tolak tolak) {
// 		tolaks.add(tolak);
// 		tolak.setTransaksiBaru(this);
// 	}

// 	public void removeTolak(Tolak tolak) {
// 		tolaks.remove(tolak);
// 		tolak.setTransaksiBaru(null);
// 	}

// 	public void addCallHistory(CallHistory ch) {
// 		callHistories.add(ch);
// 		ch.setTransaksiBaru(this);
// 	}

// 	public void removeCallHistory(CallHistory ch) {
// 		callHistories.remove(ch);
// 		ch.setTransaksiBaru(null);
// 	}
	
// 	public void addViLog(ViLog vl) {
// 		viLogs.add(vl);
// 		vl.setTransaksiBaru(this);
// 	}

// 	public void removeViLog(ViLog vl) {
// 		viLogs.remove(vl);
// 		vl.setTransaksiBaru(null);
// 	}
	
// 	public void addRingkasanPanggilan(RingkasanPanggilan rp) {
// 		ringkasanPanggilans.add(rp);
// 		rp.setTransaksiBaru(this);
// 	}

// 	public void removeRingkasanPanggilan(RingkasanPanggilan rp) {
// 		ringkasanPanggilans.remove(rp);
// 		rp.setTransaksiBaru(null);
// 	}

// 	public void addTerputus(Terputus terputus) {
// 		terputuses.add(terputus);
// 		terputus.setTransaksiBaru(this);
// 	}

// 	public void removeTerputus(Terputus terputus) {
// 		terputuses.remove(terputus);
// 		terputus.setTransaksiBaru(null);
// 	}
	
// 	public void addChatLog(ChatLog c) {
// 		chatLogs.add(c);
// 		c.setTransaksiBaru(this);
// 	}

// 	public void removeChatLog(ChatLog c) {
// 		chatLogs.remove(c);
// 		c.setTransaksiBaru(null);
// 	}
// }
