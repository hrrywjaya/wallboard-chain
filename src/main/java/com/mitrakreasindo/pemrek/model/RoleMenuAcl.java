package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.hibernate.annotations.GenericGenerator;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "role_menu_alc")
// @IdClass(RoleMenuAcl.class)
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class RoleMenuAcl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;


	// @NotNull
    // @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "acl_id", referencedColumnName = "id")
    // @JsonBackReference
    // @JsonManagedReference
    private Acl acl;


	// @NotNull
    // @Id
    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "role_menu_id", referencedColumnName = "id")
    // // @JsonBackReference
    // // @JsonManagedReference
    // private RoleMenu roleMenu;

    // @NotNull
    // @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_menu_id", referencedColumnName = "id")
    // @JsonBackReference(value = "menu-roleMenu")
    // @JsonManagedReference
    private RoleMenu roleMenu;

	// @NotNull
    private String activeFlag;
}