package com.mitrakreasindo.pemrek.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.external.cis.model.CisLastUpdate;
import com.mitrakreasindo.pemrek.external.cis.model.ContactInformation;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerAddress;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerComplementData;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerDemographicInformation;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerMasterData;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerNameAndPhone;
import com.mitrakreasindo.pemrek.external.cis.model.CustomerRemark;
import com.mitrakreasindo.pemrek.external.cis.model.FaxPhone;
import com.mitrakreasindo.pemrek.external.cis.model.OtherIdentity;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaSearch;
import com.mitrakreasindo.pemrek.external.cis.model.OutputSchemaSearchItem;
import com.mitrakreasindo.pemrek.external.cis.model.Phone;
import com.mitrakreasindo.pemrek.external.eform.model.EformFormData;
import com.mitrakreasindo.pemrek.external.eform.model.OutputSchema;

import lombok.Data;

@Data
public class CompletData
{

//	private String refNum;
//	private String status;
//	private String eformCreatedDate;
//	private String eformCreatedTime;
//	private String tipeKartuPasporBca;
//	private String namaTempatBekerja;
//	private String noCustomerRekeningGabungan;
//	private String fasilitasYangDiinginkanFinMbca;
//	private String serialNumberToken;
//	private String namaJalan;
//	private String alamatKantor3;
//	private String berlakuSampaiDengan;
//	private String statusPerkawinan;
//	private String provinsiRekeningKoran;
//	private String validasiCatatanBankKategoriNasabah;
//	private String statusRekening;
//	private String tipeIdentitasLainnya;
//	private String noCustomer;
//	private String nomorTeleponRumah;
//	private String agama;
//	private String alamatKantor1;
//	private String alamatKantor2;
//	private String kdPenduduk;
//	private String odPlan;
//	private String serviceChargeCode;
//	private String formPerpajakanCrs;
//	private String tempatLahir;
//	private String pernyataanFasilitasBca;
//	private String persetujuanTelepon;
//	private String jenisRekening;
//	private String witholdingPlan;
//	private String jabatan;
//	private String nomorHp;
//	private String nikRekeningGabungan;
//	private String namaGedung;
//	private String npwp;
//	private String sumberPenghasilan;
//	private String fasilitasYangDiinginkanMbca;
//	private String kodeAreaTeleponKantor;
//	private String persetujuanSms;
//	private String namaGadisIbuKandung;
//	private String namaDicetakDiKartu;
//	private String costCenter;
//	private String kabupatenRekeningKoran;
//	private String namaRekeningGabungan;
//	private String interestPlan;
//	private String kodeNegaraFaksimilie;
//	private String denganBuku;
//	private String sumberPenghasilanLainnya;
//	private String dalamHalIniBertindak;
//	private String fasilitasYangDiinginkanKlikbca;
//	private String nomorNpwp;
//	private String kitasKitap;
//	private String kodeNegaraNomorHp2;
//	private String nomorFaksimilie;
//	private String kodeNegaraNomorHp;
//	private String pernyataanProduk;
//	private String nomorRekeningBaru;
//	private String formPerpajakanFatca;
//	private String berlakuSampaiDenganKitasKitap;
//	private String kecamatan;
//	private String persetujuanEmail;
//	private String fasilitasYangDiinginkanKeybca;
//	private String kelurahanRekeningKoran;
//	private String bahasaPetunjukLayarAtm;
//	private String jenisKelamin;
//	private String negaraLahirFatca;
//	private String persetujuanDataPihakKetiga;
//	private String email;
//	private String namaGedungRekeningKoran;
//	private String jenisKartuPasporBca;
//	private String totalPenghasilan;
//	private String cabang;
//	private String kodeAreaTeleponRumah;
//	private String kodeNegaraDataRekening;
//	private String nomorHpMbca;
//	private String kodeNegaraTeleponKantor;
//	private String kecamatanRekeningKoran;
//	private String pernyataanPasporBca;
//	private String nomorRekeningExisting;
//	private String kodePosRekeningKoran;
//	private String kelurahan;
//	private String rekeningUntuk;
//	private String wajibPajakNegaraLain;
//	private String namaNasabah;
//	private String pekerjaan;
//	private String pekerjaanTier2;
//	private String pekerjaanTier3;
//	private String nomorKitasKitap;
//	private String periodeBiayaAdmin;
//	private String pernyataanFasilitasBcaDetail;
//	private String tinSsn;
//	private String kotaKantor;
//	private String pejabatBank;
//	private String kabupaten;
//	private String negara;
//	private String tujuanPembukaanRekening;
//	private String biayaAdmin;
//	private String negaraAlamat;
//	private String rt;
//	private String rw;
//	private String rtRw;
//	private String rtRekeningKoran;
//	private String RwRekeningKoran;
//	private String rtRwRekeningKoran;
//	private String kodePeroranganBisnis;
//	private String golonganPemilik;
//	private String bidangUsaha;
//	private String kodePos;
//	private String kodePenambahanPajak;
//	private String nomorHp2;
//	private String kodeAreaFaksimilie;
//	private String provinsi;
//	private String periodeRK;
//	private String negaraAlamatRekeningKoran;
//	private String nil;
//	private String userCode;
//	private String nik;
//	private String namaJalanRekeningKoran;
//	private String periodeBunga;
//	private String nomorTeleponKantor;
//	private String pernyataanProdukDetail;
//	private String tanggalLahir;
//	private String kodePosKantor;
//	private String kodeNegaraTeleponRumah;
//	private String wajibFatca;
//	private String pengirimanRekeningKoran;
//	private String nasabahExisting;
//	private String statusTahapanGold;
//	private String nomorHpFinMbca;
//	private String alasanResikoTinggi;
//	private String tinggalAlamatTerakhirSejak;
//	private String nasabahBankLain;
//	private String namaBankLain;
//	private String tanggalBergabungBankLain;
//	private String hubunganUsahaLuarNegeri;
//	private String negaraBerhubunganUsaha1;
//	private String negaraBerhubunganUsaha2;
//	private String negaraBerhubunganUsaha3;
//	private String sumberKekayaanWarisan;
//	private String sumberKekayaanTabungan;
//	private String sumberKekayaanHasilUsaha;
//	private String sumberKekayaanHibah;
//	private String sumberKekayaanGaji;
//	private String sumberKekayaanLainnya;
//	private String sumberKekayaanLainnyaText;
//	private String informasiLainnya;
//	private String formAeoi;
		
	private Transaksi transaksi;
	private OutputSchemaSearch cis;	
	private OutputSchema eform;
	private EformFormData confirmedEformData;
	
	private OutputSchemaSearchItem cisItem = null;
	private Boolean isRefNumRegenerated;
	private Boolean isProdukEdited;
	// private Boolean cisHavePhone = false;
	// private Boolean cisHaveEmail = false;
	
	public CompletData () {
		
	}
	
//	public CompletData(TransaksiBaru transaksi, OutputEktp ektp, OutputSchema eform, OutputSchemaSearch cis) {
//		
//		// check selected cis
//		OutputSchemaSearchItem cisItem = null;
//		List<String> cisType = Arrays.asList("A", "B");
//		if (cisType.contains(transaksi.getCisType())) {
//			cisItem = cis.getCisIndividu().stream().filter(c -> c.getCisCustomerNumber() == transaksi.getCisCustomerNumber()).findFirst().orElse(null);
//		} else {
//			// add empty data
//			cisItem = new OutputSchemaSearchItem();
//			CustomerMasterData customerMasterData = new CustomerMasterData();
//			OtherIdentity otherIdentity = new OtherIdentity();
//			customerMasterData.setOtherIdentity(otherIdentity);
//			cisItem.setCustomerMasterData(customerMasterData);
//			CustomerNameAndPhone customerNameAndPhone = new CustomerNameAndPhone();
//			customerNameAndPhone.setHomePhone(new Phone());
//			customerNameAndPhone.setOfficePhone(new Phone());
//			customerNameAndPhone.setFaxPhone(new FaxPhone());
//			cisItem.setCustomerNameAndPhone(customerNameAndPhone);
//			cisItem.setCustomerAddress(new CustomerAddress());
//			cisItem.setCustomerDemographicInformation(new CustomerDemographicInformation());
//			CustomerComplementData customerComplementData = new CustomerComplementData();
//			customerComplementData.setContactInformation(new ContactInformation());
//			cisItem.setCustomerComplementData(customerComplementData);
//			cisItem.setCustomerRemark(new CustomerRemark());
//			cisItem.setCisLastUpdate(new CisLastUpdate());
//		}
//		
//		if (cisItem.getCustomerComplementData().getContactInformation().getHandphone() != null && cisItem.getCustomerComplementData().getContactInformation().getHandphone().size() > 0) {
//			setCisHavePhone(true);
//		}		
//		if (cisItem.getCustomerComplementData().getContactInformation().getEmailAddress() != null && !cisItem.getCustomerComplementData().getContactInformation().getEmailAddress().isEmpty()) {
//			setCisHaveEmail(true);
//		}
//
////		setNik(Stream.of(transaksi.getNik(),eform.getFormData().getNik(), cisItem.getCustomerMasterData().getOtherIdentity().getIdNumber()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaNasabah(Stream.of(transaksi.getNamaNasabah(),ektp.getOutputSchema().getPersonalInformation().getFullName(), eform.getFormData().getNamaNasabah(), cisItem.getCustomerNameAndPhone().getFullName()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTempatLahir(Stream.of(transaksi.getTempatLahir(),ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthPlace() ,eform.getFormData().getTempatLahir(),cisItem.getCustomerMasterData().getBirthPlace()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTanggalLahir(Stream.of(transaksi.getTanggalLahir(),ektp.getOutputSchema().getPersonalInformation().getBirthInformation().getBirthDate() ,eform.getFormData().getTanggalLahir(), cisItem.getCustomerDemographicInformation().getBirthDate()).filter(Objects::nonNull).findFirst().orElse(""));
////		setJenisKelamin(Stream.of(transaksi.getJenisKelamin(), DukcapilGenderConverter.convert(ektp.getOutputSchema().getPersonalInformation().getSex()) ,eform.getFormData().getJenisKelamin(), cisItem.getCustomerMasterData().getSex()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaGadisIbuKandung(Stream.of(transaksi.getNamaGadisIbuKandung(), eform.getFormData().getNamaGadisIbuKandung(),cisItem.getCustomerDemographicInformation().getMothersName()).filter(Objects::nonNull).findFirst().orElse(""));
////		setStatusPerkawinan(Stream.of(transaksi.getStatusPerkawinan(), eform.getFormData().getStatusPerkawinan(), DukcapilMaritalStatusConverter.convert(ektp.getOutputSchema().getMaritalInformation().getMarriageStatus()), cisItem.getCustomerDemographicInformation().getMaritalStatus()).filter(Objects::nonNull).findFirst().orElse(""));
////		setAgama(Stream.of(transaksi.getAgama(), eform.getFormData().getAgama(), DukcapilReligionConverter.convert(ektp.getOutputSchema().getPersonalInformation().getReligion()), cisItem.getCustomerMasterData().getReligion()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraNomorHp(Stream.of(transaksi.getKodeNegaraNomorHp(), eform.getFormData().getKodeNegaraNomorHp(), cisItem.getCustomerNameAndPhone().getOfficePhone().getCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorHp(Stream.of(transaksi.getNomorHp(), eform.getFormData().getNomorHp(), cisItem.getCustomerNameAndPhone().getHomePhone().getPhoneNumber()).filter(Objects::nonNull).findFirst().orElse(""));
////		setEmail(Stream.of(transaksi.getEmail(), eform.getFormData().getEmail(), cisItem.getCustomerComplementData().getContactInformation().getEmailAddress()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaJalanRekeningKoran(Stream.of(transaksi.getNamaJalanRekeningKoran(),ektp.getOutputSchema().getAddress().getStreet() ,eform.getFormData().getNamaJalanRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaGedungRekeningKoran(Stream.of(transaksi.getNamaGedungRekeningKoran(), eform.getFormData().getNamaGedungRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getBuilding()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodePosRekeningKoran(Stream.of(transaksi.getKodePosRekeningKoran(),ektp.getOutputSchema().getAddress().getPostCode(), eform.getFormData().getKodePosRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getZipCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKecamatanRekeningKoran(Stream.of(transaksi.getKecamatanRekeningKoran(),ektp.getOutputSchema().getAddress().getSubdistrictName() ,eform.getFormData().getKecamatanRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKelurahanRekeningKoran(Stream.of(transaksi.getKelurahanRekeningKoran(),ektp.getOutputSchema().getAddress().getVillageName() ,eform.getFormData().getKelurahanRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getSubDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
////		setRtRwRekeningKoran(Stream.of(transaksi.getRtRwRekeningKoran(),ektp.getOutputSchema().getAddress().getRt() ,eform.getFormData().getRtRwRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getRt()).filter(Objects::nonNull).findFirst().orElse(""));
////		setRtRwRekeningKoran(Stream.of(transaksi.getRtRwRekeningKoran(),ektp.getOutputSchema().getAddress().getRw() ,eform.getFormData().getRtRwRekeningKoran(), cisItem.getCustomerComplementData().getContactInformation().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKabupatenRekeningKoran(Stream.of(transaksi.getKabupatenRekeningKoran(),ektp.getOutputSchema().getAddress().getRegencyName() ,eform.getFormData().getKabupatenRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCity()).filter(Objects::nonNull).findFirst().orElse(""));
////		setProvinsiRekeningKoran(Stream.of(transaksi.getProvinsiRekeningKoran(),ektp.getOutputSchema().getAddress().getProvinceName(),eform.getFormData().getProvinsiRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getProvince()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraAlamatRekeningKoran(Stream.of(transaksi.getNegaraAlamatRekeningKoran(), eform.getFormData().getNegaraAlamatRekeningKoran(),cisItem.getCustomerComplementData().getContactInformation().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPekerjaan(Stream.of(transaksi.getPekerjaan(), eform.getFormData().getPekerjaan(), cisItem.getCustomerDemographicInformation().getOccupation()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPekerjaanTier2(Stream.of(transaksi.getPekerjaanTier2(), eform.getFormData().getPekerjaanTier2()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPekerjaanTier3(Stream.of(transaksi.getPekerjaanTier3(), eform.getFormData().getPekerjaanTier3()).filter(Objects::nonNull).findFirst().orElse(""));
////		setBidangUsaha(Stream.of(transaksi.getBidangUsaha(), eform.getFormData().getBidangUsaha(),cisItem.getCustomerComplementData().getBusinessSector()).filter(Objects::nonNull).findFirst().orElse(""));
////		setJabatan(Stream.of(transaksi.getJabatan(), eform.getFormData().getJabatan(),cisItem.getCustomerComplementData().getPosition()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberPenghasilan(Stream.of(transaksi.getSumberPenghasilan(), eform.getFormData().getSumberPenghasilan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberPenghasilanLainnya(Stream.of(transaksi.getSumberPenghasilanLainnya(), eform.getFormData().getSumberPenghasilanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTotalPenghasilan(Stream.of(transaksi.getTotalPenghasilan(), eform.getFormData().getTotalPenghasilan(), cisItem.getCustomerComplementData().getIncome()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaTempatBekerja(Stream.of(transaksi.getNamaTempatBekerja(), eform.getFormData().getNamaTempatBekerja(), cisItem.getCustomerComplementData().getCompanyName()).filter(Objects::nonNull).findFirst().orElse(""));
////		if (cisItem.getCustomerComplementData().getCompanyAddress() != null) {
////			String alamat1 = cisItem.getCustomerComplementData().getCompanyAddress().get(0) != null ? cisItem.getCustomerComplementData().getCompanyAddress().get(0) : "";
////			String alamat2 = cisItem.getCustomerComplementData().getCompanyAddress().get(1) != null ? cisItem.getCustomerComplementData().getCompanyAddress().get(1) : "";
////			String alamat3 = cisItem.getCustomerComplementData().getCompanyAddress().get(2) != null ? cisItem.getCustomerComplementData().getCompanyAddress().get(2) : "";
////			
////			setAlamatKantor1(Stream.of(transaksi.getAlamatKantor1(), eform.getFormData().getAlamatKantor1(),alamat1).filter(Objects::nonNull).findFirst().orElse(""));
////			setAlamatKantor2(Stream.of(transaksi.getAlamatKantor2(), eform.getFormData().getAlamatKantor2(),alamat2).filter(Objects::nonNull).findFirst().orElse(""));
////			setAlamatKantor3(Stream.of(transaksi.getAlamatKantor3(), eform.getFormData().getAlamatKantor3(),alamat3).filter(Objects::nonNull).findFirst().orElse(""));
////		} else {
////			setAlamatKantor1(Stream.of(transaksi.getAlamatKantor1(), eform.getFormData().getAlamatKantor1()).filter(Objects::nonNull).findFirst().orElse(""));
////			setAlamatKantor2(Stream.of(transaksi.getAlamatKantor2(), eform.getFormData().getAlamatKantor2()).filter(Objects::nonNull).findFirst().orElse(""));
////			setAlamatKantor3(Stream.of(transaksi.getAlamatKantor3(), eform.getFormData().getAlamatKantor3()).filter(Objects::nonNull).findFirst().orElse(""));
////		}
////		setKotaKantor(Stream.of(transaksi.getKotaKantor(), eform.getFormData().getKotaKantor()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodePosKantor(Stream.of(transaksi.getKodePosKantor(), eform.getFormData().getKodePosKantor()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraTeleponKantor(Stream.of(transaksi.getKodeNegaraTeleponKantor(),eform.getFormData().getKodeNegaraTeleponKantor(), cisItem.getCustomerNameAndPhone().getOfficePhone().getCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeAreaTeleponKantor(Stream.of(transaksi.getKodeAreaTeleponKantor(), eform.getFormData().getKodeAreaTeleponKantor(), cisItem.getCustomerNameAndPhone().getOfficePhone().getAreaCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorTeleponKantor(Stream.of(transaksi.getNomorTeleponKantor(), eform.getFormData().getNomorTeleponKantor(),cisItem.getCustomerNameAndPhone().getOfficePhone().getPhoneNumber()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNpwp(Stream.of(transaksi.getNpwp(), eform.getFormData().getNpwp(), cisItem.getCustomerMasterData().getTaxIndicator()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNpwp(Stream.of(transaksi.getNomorNpwp(), eform.getFormData().getNomorNpwp(), cisItem.getCustomerMasterData().getTaxNo()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTujuanPembukaanRekening(Stream.of(transaksi.getTujuanPembukaanRekening(), eform.getFormData().getTujuanPembukaanRekening(), cisItem.getCustomerComplementData().getAccountPurpose()).filter(Objects::nonNull).findFirst().orElse(""));
////		setJenisRekening(Stream.of(transaksi.getJenisRekening() , eform.getFormData().getJenisRekening()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTipeKartuPasporBca(Stream.of(transaksi.getTipeKartuPasporBca(), eform.getFormData().getTipeKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setJenisKartuPasporBca(Stream.of(transaksi.getJenisKartuPasporBca(), eform.getFormData().getJenisKartuPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setCabang(Stream.of(transaksi.getCabang(), eform.getFormData().getCabang()).filter(Objects::nonNull).findFirst().orElse(""));
////		setBahasaPetunjukLayarAtm(Stream.of(transaksi.getBahasaPetunjukLayarAtm(), eform.getFormData().getBahasaPetunjukLayarAtm()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFormPerpajakanFatca(Stream.of(transaksi.getFormPerpajakanFatca(), eform.getFormData().getFormPerpajakanFatca(),cisItem.getCustomerMasterData().getFatcaFormFlag()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFormPerpajakanCrs(Stream.of(transaksi.getFormPerpajakanCrs(), eform.getFormData().getFormPerpajakanCrs(),cisItem.getCustomerMasterData().getAeoiFormFlag()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraLahirFatca(Stream.of(transaksi.getNegaraLahirFatca(), eform.getFormData().getNegaraLahirFatca(),cisItem.getCustomerDemographicInformation().getBirthCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegara(Stream.of(transaksi.getNegara(), eform.getFormData().getNegara(), cisItem.getCustomerMasterData().getCitizenshipCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setWajibFatca(Stream.of(transaksi.getWajibFatca(), eform.getFormData().getWajibFatca(), cisItem.getCustomerMasterData().getFatcaAssessable()).filter(Objects::nonNull).findFirst().orElse(""));
////		setWajibPajakNegaraLain(Stream.of(transaksi.getWajibPajakNegaraLain(), eform.getFormData().getWajibPajakNegaraLain()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPersetujuanDataPihakKetiga(Stream.of(transaksi.getPersetujuanDataPihakKetiga(), eform.getFormData().getPersetujuanDataPihakKetiga(),cisItem.getCustomerMasterData().getProvideDataPermission()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPersetujuanSms(Stream.of(transaksi.getPersetujuanSms(), eform.getFormData().getPersetujuanSms(),cisItem.getCustomerMasterData().getSmsAdvertisement()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPersetujuanEmail(Stream.of(transaksi.getPersetujuanEmail(), eform.getFormData().getPersetujuanEmail(),cisItem.getCustomerMasterData().getEmailAdvertisement()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPersetujuanTelepon(Stream.of(transaksi.getPersetujuanTelepon(), eform.getFormData().getPersetujuanTelepon(),cisItem.getCustomerMasterData().getTelephoneAdvertisement()).filter(Objects::nonNull).findFirst().orElse(""));
////
////		/*Tidak ditemukan data di response*/
////		//Start Missing Property
////		setAlasanResikoTinggi(Stream.of(transaksi.getAlasanResikoTinggi(), eform.getFormData().getAlasanResikoTinggi()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTinggalAlamatTerakhirSejak(Stream.of(transaksi.getTinggalAlamatTerakhirSejak(), eform.getFormData().getTinggalAlamatTerakhirSejak()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNasabahBankLain(Stream.of(transaksi.getNasabahBankLain(), eform.getFormData().getNasabahBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaBankLain(Stream.of(transaksi.getNamaBankLain(), eform.getFormData().getNamaBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTanggalBergabungBankLain(Stream.of(transaksi.getTanggalBergabungBankLain(), eform.getFormData().getTanggalBergabungBankLain()).filter(Objects::nonNull).findFirst().orElse(""));
////		setHubunganUsahaLuarNegeri(Stream.of(transaksi.getHubunganUsahaLuarNegeri(), eform.getFormData().getHubunganUsahaLuarNegeri()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraBerhubunganUsaha1(Stream.of(transaksi.getNegaraBerhubunganUsaha1(), eform.getFormData().getNegaraBerhubunganUsaha1()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraBerhubunganUsaha2(Stream.of(transaksi.getNegaraBerhubunganUsaha2(), eform.getFormData().getNegaraBerhubunganUsaha2()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraBerhubunganUsaha3(Stream.of(transaksi.getNegaraBerhubunganUsaha3(), eform.getFormData().getNegaraBerhubunganUsaha3()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanWarisan(Stream.of(transaksi.getSumberKekayaanWarisan(), eform.getFormData().getSumberKekayaanWarisan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanTabungan(Stream.of(transaksi.getSumberKekayaanTabungan(), eform.getFormData().getSumberKekayaanTabungan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanHasilUsaha(Stream.of(transaksi.getSumberKekayaanHasilUsaha(), eform.getFormData().getSumberKekayaanHasilUsaha()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanHibah(Stream.of(transaksi.getSumberKekayaanHibah(), eform.getFormData().getSumberKekayaanHibah()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanGaji(Stream.of(transaksi.getSumberKekayaanGaji(), eform.getFormData().getSumberKekayaanGaji()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanLainnya(Stream.of(transaksi.getSumberKekayaanLainnya(), eform.getFormData().getSumberKekayaanLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSumberKekayaanLainnyaText(Stream.of(transaksi.getSumberKekayaanLainnyaText(), eform.getFormData().getSumberKekayaanLainnyaText()).filter(Objects::nonNull).findFirst().orElse(""));
////		setInformasiLainnya(Stream.of(transaksi.getInformasiLainnya(), eform.getFormData().getInformasiLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
////		//End Missing Property
////
////		/*Yang di excel di hitamkan*/
////		setNamaJalan(Stream.of(transaksi.getNamaJalan(), eform.getFormData().getNamaJalan(),cisItem.getCustomerAddress().getStreet()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaGedung(Stream.of(transaksi.getNamaGedung(), eform.getFormData().getNamaGedung(),cisItem.getCustomerAddress().getBuilding()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodePos(Stream.of(transaksi.getKodePos(), eform.getFormData().getKodePos(), cisItem.getCustomerAddress().getZipCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKabupaten(Stream.of(transaksi.getKabupaten(), eform.getFormData().getKabupaten(), cisItem.getCustomerAddress().getCity()).filter(Objects::nonNull).findFirst().orElse(""));
////		setProvinsi(Stream.of(transaksi.getProvinsi(), eform.getFormData().getProvinsi(),cisItem.getCustomerAddress().getProvince()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKecamatan(Stream.of(transaksi.getKecamatan(), eform.getFormData().getKecamatan(),cisItem.getCustomerAddress().getDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKelurahan(Stream.of(transaksi.getKelurahan(), eform.getFormData().getKelurahan(),cisItem.getCustomerAddress().getSubDistrict()).filter(Objects::nonNull).findFirst().orElse(""));
////		setRtRw(Stream.of(transaksi.getRtRw(), eform.getFormData().getRtRw(), cisItem.getCustomerAddress().getRt()).filter(Objects::nonNull).findFirst().orElse(""));
////		setRtRw(Stream.of(transaksi.getRtRw(), eform.getFormData().getRtRw(), cisItem.getCustomerAddress().getRw()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNegaraAlamat(Stream.of(transaksi.getNegaraAlamat(), eform.getFormData().getNegaraAlamat(), cisItem.getCustomerAddress().getCountry()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraTeleponRumah(Stream.of(transaksi.getKodeNegaraTeleponRumah(), eform.getFormData().getKodeNegaraTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getCountryCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeAreaTeleponRumah(Stream.of(transaksi.getKodeAreaTeleponRumah(), eform.getFormData().getKodeAreaTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getAreaCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorTeleponRumah(Stream.of(transaksi.getNomorTeleponRumah(), eform.getFormData().getNomorTeleponRumah(), cisItem.getCustomerNameAndPhone().getHomePhone().getPhoneNumber()).filter(Objects::nonNull).findFirst().orElse(""));
////
////
////		/*Yang Tidak ada di excel*/
////		setRefNum(Stream.of(transaksi.getRefNum(), eform.getRefNum()).filter(Objects::nonNull).findFirst().orElse(""));
////		setStatus(Stream.of(transaksi.getStatus(), eform.getStatus()).filter(Objects::nonNull).findFirst().orElse(""));
////		setEformCreatedDate(Stream.of(transaksi.getEformCreatedDate(), eform.getCreatedDate()).filter(Objects::nonNull).findFirst().orElse(""));
////		setEformCreatedTime(Stream.of(transaksi.getEformCreatedTime(), eform.getCreatedTime()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNoCustomerRekeningGabungan(Stream.of(transaksi.getNoCustomerRekeningGabungan(), eform.getFormData().getNoCustomerRekeningGabungan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFasilitasYangDiinginkanFinMbca(Stream.of(transaksi.getFasilitasYangDiinginkanFinMbca(), eform.getFormData().getFasilitasYangDiinginkanFinMbca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setSerialNumberToken(Stream.of(transaksi.getSerialNumberToken(), eform.getFormData().getSerialNumberToken()).filter(Objects::nonNull).findFirst().orElse(""));
////		setBerlakuSampaiDengan(Stream.of(transaksi.getBerlakuSampaiDengan(), eform.getFormData().getBerlakuSampaiDengan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setValidasiCatatanBankKategoriNasabah(Stream.of(transaksi.getValidasiCatatanBankKategoriNasabah(), eform.getFormData().getValidasiCatatanBankKategoriNasabah()).filter(Objects::nonNull).findFirst().orElse(""));
////		setStatusRekening(Stream.of(transaksi.getStatusRekening(), eform.getFormData().getStatusRekening()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTipeIdentitasLainnya(Stream.of(transaksi.getTipeIdentitasLainnya(), eform.getFormData().getTipeIdentitasLainnya()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNoCustomer(Stream.of(transaksi.getNoCustomer(), eform.getFormData().getNoCustomer()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKdPenduduk(Stream.of(transaksi.getKdPenduduk(), eform.getFormData().getKdPenduduk()).filter(Objects::nonNull).findFirst().orElse(""));
////		setOdPlan(Stream.of(transaksi.getOdPlan(), eform.getFormData().getOdPlan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setServiceChargeCode(Stream.of(transaksi.getServiceChargeCode(), eform.getFormData().getServiceChargeCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPernyataanFasilitasBca(Stream.of(transaksi.getPernyataanFasilitasBca(), eform.getFormData().getPernyataanFasilitasBca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setWitholdingPlan(Stream.of(transaksi.getWitholdingPlan(), eform.getFormData().getWitholdingPlan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNikRekeningGabungan(Stream.of(transaksi.getNikRekeningGabungan(), eform.getFormData().getNikRekeningGabungan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFasilitasYangDiinginkanMbca(Stream.of(transaksi.getFasilitasYangDiinginkanMbca(), eform.getFormData().getFasilitasYangDiinginkanMbca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaDicetakDiKartu(Stream.of(transaksi.getNamaDicetakDiKartu(), eform.getFormData().getNamaDicetakDiKartu()).filter(Objects::nonNull).findFirst().orElse(""));
////		setCostCenter(Stream.of(transaksi.getCostCenter(), eform.getFormData().getCostCenter()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNamaRekeningGabungan(Stream.of(transaksi.getNamaRekeningGabungan(), eform.getFormData().getNamaRekeningGabungan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setInterestPlan(Stream.of(transaksi.getInterestPlan(), eform.getFormData().getInterestPlan()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraFaksimilie(Stream.of(transaksi.getKodeNegaraFaksimilie(), eform.getFormData().getKodeNegaraFaksimilie()).filter(Objects::nonNull).findFirst().orElse(""));
////		setDenganBuku(Stream.of(transaksi.getDenganBuku(), eform.getFormData().getDenganBuku()).filter(Objects::nonNull).findFirst().orElse(""));
////		setDalamHalIniBertindak(Stream.of(transaksi.getDalamHalIniBertindak(), eform.getFormData().getDalamHalIniBertindak()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFasilitasYangDiinginkanKlikbca(Stream.of(transaksi.getFasilitasYangDiinginkanKlikbca(), eform.getFormData().getFasilitasYangDiinginkanKlikbca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorNpwp(Stream.of(transaksi.getNomorNpwp(), eform.getFormData().getNomorNpwp()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKitasKitap(Stream.of(transaksi.getKitasKitap(), eform.getFormData().getKitasKitap()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraNomorHp2(Stream.of(transaksi.getKodeNegaraNomorHp2(), eform.getFormData().getKodeNegaraNomorHp2()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorFaksimilie(Stream.of(transaksi.getNomorFaksimilie(), eform.getFormData().getNomorFaksimilie()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPernyataanProduk(Stream.of(transaksi.getPernyataanProduk(), eform.getFormData().getPernyataanProduk()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorRekeningBaru(Stream.of(transaksi.getNomorRekeningBaru(), eform.getFormData().getNomorRekeningBaru()).filter(Objects::nonNull).findFirst().orElse(""));
////		setBerlakuSampaiDenganKitasKitap(Stream.of(transaksi.getBerlakuSampaiDenganKitasKitap(), eform.getFormData().getBerlakuSampaiDenganKitasKitap()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFasilitasYangDiinginkanKeybca(Stream.of(transaksi.getFasilitasYangDiinginkanKeybca(), eform.getFormData().getFasilitasYangDiinginkanKeybca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeNegaraDataRekening(Stream.of(transaksi.getKodeNegaraDataRekening(), eform.getFormData().getKodeNegaraDataRekening()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorHpMbca(Stream.of(transaksi.getNomorHpMbca(), eform.getFormData().getNomorHpMbca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPernyataanPasporBca(Stream.of(transaksi.getPernyataanPasporBca(), eform.getFormData().getPernyataanPasporBca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorRekeningExisting(Stream.of(transaksi.getNomorRekeningExisting(), eform.getFormData().getNomorRekeningExisting()).filter(Objects::nonNull).findFirst().orElse(""));
////		setRekeningUntuk(Stream.of(transaksi.getRekeningUntuk(), eform.getFormData().getRekeningUntuk()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorKitasKitap(Stream.of(transaksi.getNomorKitasKitap(), eform.getFormData().getNomorKitasKitap()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPeriodeBiayaAdmin(Stream.of(transaksi.getPeriodeBiayaAdmin(), eform.getFormData().getPeriodeBiayaAdmin()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPernyataanFasilitasBcaDetail(Stream.of(transaksi.getPernyataanFasilitasBcaDetail(), eform.getFormData().getPernyataanFasilitasBcaDetail()).filter(Objects::nonNull).findFirst().orElse(""));
////		setTinSsn(Stream.of(transaksi.getTinSsn(), eform.getFormData().getTinSsn()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPejabatBank(Stream.of(transaksi.getPejabatBank(), eform.getFormData().getPejabatBank()).filter(Objects::nonNull).findFirst().orElse(""));
////		setBiayaAdmin(Stream.of(transaksi.getBiayaAdmin(), eform.getFormData().getBiayaAdmin()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodePeroranganBisnis(Stream.of(transaksi.getKodePeroranganBisnis(), eform.getFormData().getKodePeroranganBisnis()).filter(Objects::nonNull).findFirst().orElse(""));
////		setGolonganPemilik(Stream.of(transaksi.getGolonganPemilik(), eform.getFormData().getGolonganPemilik()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodePenambahanPajak(Stream.of(transaksi.getKodePenambahanPajak(), eform.getFormData().getKodePenambahanPajak()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorHp2(Stream.of(transaksi.getNomorHp2(), eform.getFormData().getNomorHp2()).filter(Objects::nonNull).findFirst().orElse(""));
////		setKodeAreaFaksimilie(Stream.of(transaksi.getKodeAreaFaksimilie(), eform.getFormData().getKodeAreaFaksimilie()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPeriodeRK(Stream.of(transaksi.getPeriodeRK(), eform.getFormData().getPeriodeRK()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNil(Stream.of(transaksi.getNil(), eform.getFormData().getNil()).filter(Objects::nonNull).findFirst().orElse(""));
////		setUserCode(Stream.of(transaksi.getUserCode(), eform.getFormData().getUserCode()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPeriodeBunga(Stream.of(transaksi.getPeriodeBunga(), eform.getFormData().getPeriodeBunga()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPernyataanProdukDetail(Stream.of(transaksi.getPernyataanProdukDetail(), eform.getFormData().getPernyataanProdukDetail()).filter(Objects::nonNull).findFirst().orElse(""));
////		setPengirimanRekeningKoran(Stream.of(transaksi.getPengirimanRekeningKoran(), eform.getFormData().getPengirimanRekeningKoran()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNasabahExisting(Stream.of(transaksi.getNasabahExisting(), eform.getFormData().getNasabahExisting()).filter(Objects::nonNull).findFirst().orElse(""));
////		setStatusTahapanGold(Stream.of(transaksi.getStatusTahapanGold(), eform.getFormData().getStatusTahapanGold()).filter(Objects::nonNull).findFirst().orElse(""));
////		setNomorHpFinMbca(Stream.of(transaksi.getNomorHpFinMbca(), eform.getFormData().getNomorHpFinMbca()).filter(Objects::nonNull).findFirst().orElse(""));
////		setFormAeoi(Stream.of(transaksi.getFormAeoi(), eform.getFormData().getFormPerpajakanCrs()).filter(Objects::nonNull).findFirst().orElse(""));
//		
////		setIsRefNumRegenerated(transaksi.getIsRefNumRegenerated() == null ? false : transaksi.getIsRefNumRegenerated());
//		// kode tahapan 110
//		// kode tahapan gold 110
//		// jika berubah dari tahapan ke tahapan gold, maka tidak dianggap berubah
//		// jika berubah ke xpresi, maka dianggap berubah
////		if (transaksi.getJenisRekening() != null) {
////			if (eform.getFormData().getJenisRekening().equals(transaksi.getJenisRekening()))
////				setIsProdukEdited(false);
////			else
////				setIsProdukEdited(true);
////		} else {
////			setIsProdukEdited(transaksi.getJenisRekening() != null ? true : false);
////		}
//	}
//	
	
	private OutputSchemaSearchItem emptyCisItem() {
		OutputSchemaSearchItem cisItem = new OutputSchemaSearchItem();
		CustomerMasterData customerMasterData = new CustomerMasterData();
		OtherIdentity otherIdentity = new OtherIdentity();
		customerMasterData.setOtherIdentity(otherIdentity);
		cisItem.setCustomerMasterData(customerMasterData);
		CustomerNameAndPhone customerNameAndPhone = new CustomerNameAndPhone();
		customerNameAndPhone.setHomePhone(new Phone());
		customerNameAndPhone.setOfficePhone(new Phone());
		customerNameAndPhone.setFaxPhone(new FaxPhone());
		cisItem.setCustomerNameAndPhone(customerNameAndPhone);
		cisItem.setCustomerAddress(new CustomerAddress());
		cisItem.setCustomerDemographicInformation(new CustomerDemographicInformation());
		CustomerComplementData customerComplementData = new CustomerComplementData();
		customerComplementData.setContactInformation(new ContactInformation());
		cisItem.setCustomerComplementData(customerComplementData);
		cisItem.setCustomerRemark(new CustomerRemark());
		cisItem.setCisLastUpdate(new CisLastUpdate());
		return cisItem;
	}
	
	public void setData(Transaksi transaksi, OutputSchema eform, EformFormData updatedEformData, OutputSchemaSearch cis) {
		this.transaksi = transaksi;
		this.eform = eform;
		this.confirmedEformData = updatedEformData;
		this.cis = cis;
		
		OutputSchemaSearchItem cisItem = null;
		List<String> cisType = Arrays.asList("A", "B");
		if (cis != null && cis.getCisIndividu() != null && cisType.contains(transaksi.getCisType())) {
			cisItem = cis.getCisIndividu().stream().filter(c -> c.getCisCustomerNumber() == transaksi.getCisCustomerNumber()).findFirst().orElse(emptyCisItem());			
		} else {
			// add empty data
			cisItem = emptyCisItem();
		}
		
		
		
		
		System.out.println("complement data " +cisItem.getCustomerComplementData() != null ? "not null" : "null");
		System.out.println("contact information " +cisItem.getCustomerComplementData().getContactInformation() != null ? "not null" : "null");
		System.out.println("hadphone " +cisItem.getCustomerComplementData().getContactInformation().getHandphone() != null ? "not null" : "null");
		
		// Optional.ofNullable(cisItem.getCustomerComplementData().getContactInformation().getHandphone())
		// .ifPresent(handphones -> {
		// 	if (handphones.size() > 0)
		// 		setCisHavePhone(true);
		// });
		
		// Optional.ofNullable(cisItem.getCustomerComplementData().getContactInformation().getEmailAddress())
		// .ifPresent(email -> {
		// 	if (!email.isEmpty())
		// 		setCisHaveEmail(true);
		// });
		
//		if (cisItem.getCustomerComplementData().getContactInformation().getHandphone() != null && cisItem.getCustomerComplementData().getContactInformation().getHandphone().size() > 0) {
//			setCisHavePhone(true);
//		}		
//		if (cisItem.getCustomerComplementData().getContactInformation().getEmailAddress() != null && !cisItem.getCustomerComplementData().getContactInformation().getEmailAddress().isEmpty()) {
//			setCisHaveEmail(true);
//		}
		
		setIsRefNumRegenerated(transaksi.getIsRefNumRegenerated() == null ? false : transaksi.getIsRefNumRegenerated());
		// kode tahapan 110
		// kode tahapan gold 110
		// jika berubah dari tahapan ke tahapan gold, maka tidak dianggap berubah
		// jika berubah ke xpresi, maka dianggap berubah
		if (transaksi.getJenisRekening() != null) {
			if (eform.getFormData().getJenisRekening().equals(transaksi.getJenisRekening()))
				setIsProdukEdited(false);
			else
				setIsProdukEdited(true);
		} else {
			setIsProdukEdited(transaksi.getJenisRekening() != null ? true : false);
		}
	}
		
}
