package com.mitrakreasindo.pemrek.model;

import com.mitrakreasindo.pemrek.external.customer.model.status.CustomerStatus;

import lombok.Data;

@Data
public class RingkasanTransaksiData {

	private String username;
	private String branchCode;
	private String currentNoRef;
	// private Referensi referensi;
	private Transaksi transaksi;
	private CustomerStatus customerStatus;
	private CompletData data;
	private Boolean isNasabahExisting;
	private Boolean isNrt;
	private Boolean isChangeProduct;
	private CallLog callLog;
	
}
