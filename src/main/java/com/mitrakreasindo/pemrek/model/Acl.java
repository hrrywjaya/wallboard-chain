package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mitrakreasindo.pemrek.core.model.Auditable;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
// @EqualsAndHashCode(exclude = { "menus","roleMenus" })
@EqualsAndHashCode(exclude = { "menus","roleMenuAcls" })
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Acl extends Auditable<String> implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "acls")
    // @JsonBackReference
    // @JsonManagedReference
    private Set<Menu> menus = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "acl", cascade = CascadeType.ALL)
    // @JsonManagedReference
    // @JsonBackReference
    private Set<RoleMenuAcl> roleMenuAcls = new HashSet<>();
    
    // @OneToMany(fetch = FetchType.LAZY, mappedBy = "acl", cascade = CascadeType.ALL)
    // private Set<RoleMenu> roleMenus = new HashSet<>();
    
	private String name;
	private String description;
	private String enabled;
}
