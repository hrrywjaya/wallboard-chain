package com.mitrakreasindo.pemrek.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.mitrakreasindo.pemrek.core.model.base.BaseModel;

import lombok.Data;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Tolak extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	// @ManyToOne(fetch = FetchType.LAZY)
	// private TransaksiBaru transaksiBaru;

	@ManyToOne(fetch = FetchType.LAZY)
	private MasterDataTolak masterDataTolak;

//	@ManyToOne
//	private CallHistory callHistory;

	private String noRef;
	
	private String agentName;

	private String message;

}
