package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mitrakreasindo.pemrek.core.model.Role;

import org.hibernate.annotations.GenericGenerator;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(exclude = { "roleMenuAcls" })
// @EqualsAndHashCode
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="role_menu",  uniqueConstraints={
    @UniqueConstraint(columnNames={"role_id", "menu_id"}),
    // @UniqueConstraint(columnNames={"anotherField", "uid"})
 })
// @IdClass(RoleMenu.class)
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class RoleMenu implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

	@NotNull
    // @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    // @JsonBackReference
    // @JsonManagedReference
    private Role role;


	@NotNull
    // @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "menu_id", referencedColumnName = "id")
    // @JsonBackReference(value = "menu-roleMenu")
    // @JsonManagedReference
    private Menu menu;

    // @OneToMany(fetch = FetchType.LAZY, mappedBy = "roleMenu", cascade = CascadeType.ALL)
    // // @JsonManagedReference
    // // @JsonBackReference
    // private Set<RoleMenuAcl> roleMenuAcls = new HashSet<>();
    
    // @OneToMany(fetch = FetchType.LAZY, mappedBy = "roleMenu")
    // private Set<RoleMenuAcl> roleMenuAcls = new HashSet<>();
    
    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "acl_id", referencedColumnName = "id")
    // // @JsonBackReference(value = "menu-roleMenu")
    // // @JsonManagedReference
    // private Acl acl;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roleMenu", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RoleMenuAcl> roleMenuAcls = new HashSet<>();

	@NotNull
    private String activeFlag;

    public void addRoleMenuAcls(RoleMenuAcl r) {
		r.setRoleMenu(this);
		roleMenuAcls.add(r);
	}
}