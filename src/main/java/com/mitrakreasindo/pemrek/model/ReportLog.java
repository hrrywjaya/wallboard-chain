package com.mitrakreasindo.pemrek.model;

import java.util.List;

import lombok.Data;

@Data
public class ReportLog {
  private TransaksiLogPerubahan trxLog;
  private TransaksiLogPerubahan trxLogOld;
  private String createdDate;
  private String username;
  private String noRef;
  private String approveDate;
  private String usernameSPV;
  private String waktuPanggilan;
  private String extention;
  private String status;
  private String dropped_date;
  private String noRekening;
  private String tolakMdId;


  
} 