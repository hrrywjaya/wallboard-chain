package com.mitrakreasindo.pemrek.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(UpperCamelCaseStrategy.class)
public class Email {
  
  private String emailTo;
  private String emailCc;
  private String emailBcc;
  private String emailCustomSubject;
  


}