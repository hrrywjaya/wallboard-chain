package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mitrakreasindo.pemrek.core.model.Auditable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
// @EqualsAndHashCode
@EqualsAndHashCode(exclude = { "eddApproval" })
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CallLog extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "edd_approval_id", referencedColumnName = "id")
    private EddApproval eddApproval;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "transaksi_id")
	// private Transaksi transaksi;
	
	@Column(nullable = false)
	@NotNull
	private String callId;

	private String transaksiId;

	private String noRef;
  
	private String username;
	private String fullName;
	private String role;

	// start call
	private String alertingDate;
	private String activeDate;
	
	private String incomingDuration;
	private String answerDate;
	private String answerDuration;

	private String droppedDate;

	private String transaksiDuration;
	// end call
	private String transaksiStatus;

	// start cti
	private String cti;
	private String cucm;
	private String tftp;
	private String finesseUsername;
	private String finessePassword;
	private String finesseAgentId;
	private String jabberUsername;
	private String jabberPassword;
	private String jabberExtension;
	// end cti

	// ----- start terputus ------
	private String terputusMdId;
	private String terputusMdTitle;
	private String terputusMessage; // kalo terputusMdTitle in [lainnya...]
	// ----- end terputus ------

	
	// ----- start tolak ------
	private String tolakMdId;
	private String tolakMdTitle;
	private String tolakMessage; // kalo tolakMdTitle in [lainnya...]
	// ----- end tolak ------

	// ----- start service level ------
	private String serviceLevelMdId;
	private String serviceLevelMdTitle;
	private String serviceLevelMessage; // kalo tolakMdTitle in [lainnya...]
	// ----- end service level ------

	@Column(columnDefinition="CLOB")
	private String foto; //base64

	@Column(columnDefinition="CLOB")
	private String oldRawKonfirmasiData;

	@Column(columnDefinition="CLOB")
	private String rawKonfirmasiData;

	// ----- start addtional field for query ------
	private String qrChannel;
	private String qrKodeTransaksiTemp;
	private String qrJenisTransaksi;
	private String qrCabang; 
	private String qrNoRekening;
	private String qrCisCustomerNumber;
	private String qrNamaNasabah; 
	private String qrJenisRekening; // utk di frontend page transaksi-berhasil

  private String qrTipeKartuPasporBcaTemp;

	private String qrJenisNasabah;
	private String qrStatusIdentitas;
	private String qrStatusRba;
	private String qrNamaCabang;

	private String qrCustomerName;
	private String qrCustomerPepStatus;
	private String qrCustomerDttotStatus;
	private String qrCustomerDikStatus;
	private String qrCustomerNrtStatus;
	private String qrCustomerCode;
	// ----- end addtional field for query ------

	// ----- start approval spv ------
	private String approvalUsername;
	private String approvalFullname;
	private String approvalRole;
	private String approvalStartDate;
	private String approvalEndDate;
	private String approvalDuration; 
	// ----- end approval spv ------
	
	private String workReadyDate;
	private String workDate;
		
	private String qrNik;	
	private String qrNomorHp;

	private String qrMbankStatus;

	@Column(columnDefinition="CLOB")
	private String perubahanData;

	private String qrSumber;
	
	// ----- start channel ------
	private String channelMdId;
	private String channelMdCode;
	private String channelMdText;
	// ----- end channel ------
	
	// ----- start transactioncode ------
	private String transactionCodeMdId;
	private String transactionCodeMdCode;
	private String transactionCodeMdText;
	// ----- end transactioncode ------

}
