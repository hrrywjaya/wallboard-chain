package com.mitrakreasindo.pemrek.model;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionMessage {
	public static enum Status {
		SUCCESS,
		FAILED_RETRAY_ABLE,
		FAILED_NOT_RETRAY_ABLE,
		NOT_RUNNING,
		ON_PROGRESS
	}
	
	private String username;
	private List<TransactionMessageStatus> stepMap;
	private Status status;

	@Data
	@Builder
	public static class TransactionMessageStatus {
		private TransactionStatus name;
		private Status status;
		private int order;
		private String description;
	}
}