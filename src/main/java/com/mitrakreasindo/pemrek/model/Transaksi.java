package com.mitrakreasindo.pemrek.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mitrakreasindo.pemrek.core.model.Auditable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(exclude={"fotos", "referensis"})
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Transaksi extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "transaksi", cascade = CascadeType.ALL)	
	@JsonManagedReference
	private Set<Foto> fotos = new HashSet<>();

	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaksi", cascade = CascadeType.ALL)	
	// @JsonManagedReference
	// private Set<Terputus> terputuses = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "transaksi", cascade = CascadeType.ALL)	
	@JsonManagedReference
	private Set<Referensi> referensis = new HashSet<>();

	private String callId;

	@Column(columnDefinition="CLOB")
	private String rawKonfirmasiData;

	@Column(columnDefinition="CLOB")
	private String rawEform;

	@Column(columnDefinition="CLOB")
	private String rawCis;

	private Boolean predinFotoChecked;
	private Boolean predinKtpChecked;
	private Boolean predinTtdChecked;
	private Boolean predinNpwpChecked;

	private String dinFotoChecked;
	private String dinKtpChecked;
	private String dinTtdChecked;
	private String dinNpwpChecked;

	private String cisType;
	private String cisCustomerNumber;

	private String refNum;
	private String status;
	private String eformCreatedDate;
	private String eformCreatedTime;
	private String tipeKartuPasporBca;
	private String namaTempatBekerja;
	private String noCustomerRekeningGabungan;
	private String fasilitasYangDiinginkanFinMbca;
	private String serialNumberToken;
	private String namaJalan;
	private String alamatKantor3;
	private String berlakuSampaiDengan;
	private String statusPerkawinan;
	private String provinsiRekeningKoran;
	private String validasiCatatanBankKategoriNasabah;
	private String statusRekening;
	private String tipeIdentitasLainnya;
	private String noCustomer;
	private String nomorTeleponRumah;
	private String agama;
	private String alamatKantor1;
	private String alamatKantor2;
	private String kdPenduduk;
	private String odPlan;
	private String serviceChargeCode;
	private String formPerpajakanCrs;
	private String tempatLahir;
	private String pernyataanFasilitasBca;
	private String persetujuanTelepon;
	private String jenisRekening;
	private String witholdingPlan;
	private String jabatan;
	private String nomorHp;
	private String nikRekeningGabungan;
	private String namaGedung;
	private String npwp;
	private String sumberPenghasilan;
	private String fasilitasYangDiinginkanMbca;
	private String kodeAreaTeleponKantor;
	private String persetujuanSms;
	private String namaGadisIbuKandung;
	private String namaDicetakDiKartu;
	private String costCenter;
	private String kabupatenRekeningKoran;
	private String namaRekeningGabungan;
	private String interestPlan;
	private String kodeNegaraFaksimilie;
	private String denganBuku;
	private String sumberPenghasilanLainnya;
	private String dalamHalIniBertindak;
	private String fasilitasYangDiinginkanKlikbca;
	private String nomorNpwp;
	private String kitasKitap;
	private String kodeNegaraNomorHp2;
	private String nomorFaksimilie;
	private String kodeNegaraNomorHp;
	private String pernyataanProduk;
	private String nomorRekeningBaru;
	private String formPerpajakanFatca;
	private String berlakuSampaiDenganKitasKitap;
	private String kecamatan;
	private String persetujuanEmail;
	private String fasilitasYangDiinginkanKeybca;
	private String kelurahanRekeningKoran;
	private String bahasaPetunjukLayarAtm;
	private String jenisKelamin;
	private String negaraLahirFatca;
	private String persetujuanDataPihakKetiga;
	private String email;
	private String namaGedungRekeningKoran;
	private String jenisKartuPasporBca;
	private String totalPenghasilan;
	private String cabang;
	private String kodeAreaTeleponRumah;
	private String kodeNegaraDataRekening;
	private String nomorHpMbca;
	private String kodeNegaraTeleponKantor;
	private String kecamatanRekeningKoran;
	private String pernyataanPasporBca;
	private String nomorRekeningExisting;
	private String kodePosRekeningKoran;
	private String kelurahan;
	private String rekeningUntuk;
	private String wajibPajakNegaraLain;
	private String namaNasabah;
	private String pekerjaan;
	private String nomorKitasKitap;
	private String periodeBiayaAdmin;
	private String pernyataanFasilitasBcaDetail;
	private String tinSsn;
	private String kotaKantor;
	private String pejabatBank;
	private String kabupaten;
	private String negara;
	private String tujuanPembukaanRekening;
	private String biayaAdmin;
	private String negaraAlamat;
	private String rtRw;
	private String rtRwRekeningKoran;
	private String kodePeroranganBisnis;
	private String golonganPemilik;
	private String bidangUsaha;
	private String kodePos;
	private String kodePenambahanPajak;
	private String nomorHp2;
	private String kodeAreaFaksimilie;
	private String provinsi;
	private String periodeRK;
	private String negaraAlamatRekeningKoran;
	private String nil;
	private String userCode;
	private String nik;
	private String namaJalanRekeningKoran;
	private String periodeBunga;
	private String nomorTeleponKantor;
	private String pernyataanProdukDetail;
	private String tanggalLahir;
	private String kodePosKantor;
	private String kodeNegaraTeleponRumah;
	private String wajibFatca;
	private String pengirimanRekeningKoran;
	private String nasabahExisting;

	private String formAeoi;

	private String statusTahapanGold;

	private String pekerjaanTier2;
	private String pekerjaanTier3;

	private String alasanResikoTinggi;
	private String tinggalAlamatTerakhirSejak;
	private String nasabahBankLain;
	private String namaBankLain;
	private String tanggalBergabungBankLain;
	private String hubunganUsahaLuarNegeri;
	private String negaraBerhubunganUsaha1;
	private String negaraBerhubunganUsaha2;
	private String negaraBerhubunganUsaha3;
	private String sumberKekayaanWarisan;
	private String sumberKekayaanTabungan;
	private String sumberKekayaanHasilUsaha;
	private String sumberKekayaanHibah;
	private String sumberKekayaanGaji;
	private String sumberKekayaanLainnya;
	private String sumberKekayaanLainnyaText;
	private String informasiLainnya;

	private String spvApproveKtp;
	private String spvApproveFoto;
	private String spvApproveTtd;
	private String spvApproveNpwp;

	private String qrJenisNasabah;

	private Boolean isRefNumRegenerated;
	private String newRefNumber;
	private Boolean isProdukEdited;
	private String pemrekAccountNumber;
	private String pemrekCisCustomerNumber;
	@Enumerated(EnumType.STRING)
	private TransactionStatus transactionStatus;
	private Boolean isTransactionStepFailed;
	
	@Column(columnDefinition="CLOB")
	private String transactionStatusMap;
	// chekc is process still running
	private Boolean processRunning;

	// @Column(name = "launch_date")
	// private LocalDate launchDate;

	// private transient String formattedDate;

	// Getter and setter
	// public String getFormattedDate() {
	// return getLaunchDate().toString();
	// }

	public void addFotos(Foto f) {
		f.setTransaksi(this);
		fotos.add(f);
	}

	// public void addTerputuses(Terputus t) {
	// 	t.setTransaksi(this);
	// 	terputuses.add(t);
	// }

	public void addReferensis(Referensi r) {
		r.setTransaksi(this);
		referensis.add(r);
	}
}