package com.mitrakreasindo.pemrek.expose.reference.service;

import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.reference.model.ReferenceNumberOutputSchema;

public interface ReferenceService
{

	Output<ReferenceNumberOutputSchema> getLatestReferenceNumber(String referenceNumber);
	
}
