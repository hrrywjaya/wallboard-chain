package com.mitrakreasindo.pemrek.expose.reference.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import com.mitrakreasindo.pemrek.expose.common.model.ErrorMessage;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorSchema;
import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.reference.ReferenceErrorCode;
import com.mitrakreasindo.pemrek.expose.reference.model.ReferenceNumberOutputSchema;
import com.mitrakreasindo.pemrek.expose.reference.service.ReferenceService;
import com.mitrakreasindo.pemrek.model.Referensi;
import com.mitrakreasindo.pemrek.service.ReferensiService;

@Service
@Transactional
public class ReferenceServiceImpl implements ReferenceService
{

	@Autowired
	private ReferensiService referensiService;
	
	@Override
	public Output<ReferenceNumberOutputSchema> getLatestReferenceNumber (String referenceNumber)
	{
		Referensi reference = referensiService.findOneLastReferensiByNoRef(referenceNumber);
		
		Output<ReferenceNumberOutputSchema> output = new Output<>();
		ErrorMessage errorMessage = new ErrorMessage();
		ErrorSchema errorSchema = new ErrorSchema();
		ReferenceNumberOutputSchema ouputSchema = new ReferenceNumberOutputSchema(); 
		if (reference != null) {
			ouputSchema.setReferenceNumber(reference.getNoRef());
			errorSchema.setErrorCode(ReferenceErrorCode.LATEST_REFERENCE_SUCCESS);
			errorMessage.setEnglish("success");
			errorMessage.setIndonesian("Berhasil");
		} else {
			errorSchema.setErrorCode(ReferenceErrorCode.LATEST_REFERENCE_NOT_FOUND);
			errorMessage.setEnglish("Not found");
			errorMessage.setIndonesian("Tidak ditemukan");
		}
		
		errorSchema.setErrorMessage(errorMessage);
		output.setErrorSchema(errorSchema);
		output.setOutputSchema(ouputSchema);
		return output;
	}

}
