package com.mitrakreasindo.pemrek.expose.reference.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.reference.model.ReferenceNumberOutputSchema;
import com.mitrakreasindo.pemrek.expose.reference.service.ReferenceService;

@RestController
@RequestMapping("/chain/reference-number")
public class ReferenceController
{
	
	@Autowired
	private ReferenceService referenceService;

	@GetMapping("/latest/{reference-number}")
	public Output<ReferenceNumberOutputSchema> getLatestReferenceNumber (@PathVariable("reference-number") String referenceNumber) {
		return referenceService.getLatestReferenceNumber(referenceNumber);
	}
	
}
