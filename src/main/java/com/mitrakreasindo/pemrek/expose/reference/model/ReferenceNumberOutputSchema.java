package com.mitrakreasindo.pemrek.expose.reference.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class ReferenceNumberOutputSchema
{

	private String referenceNumber;
	
}
