package com.mitrakreasindo.pemrek.expose.transaction.service;
import java.util.List;

import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.transaction.dto.ServiceStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatusDetail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransactionService {

    Output<List<TransactionStatus>> findTransactionStatusByHandphone(String handphone);

    Output<List<TransactionStatus>> findTransactionStatusByStatus(String status, String startDate, String endDate);

    Output<List<ServiceStatus>> findServiceStatus(String refNum, String createdDate);

    Output<TransactionStatusDetail> findTransactionStatusDetail(String nomorReferensi, String tanggal);
}
