package com.mitrakreasindo.pemrek.expose.transaction.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(SnakeCaseStrategy.class)
public class TransactionStatus {
	
    private Long callTime;
    private String referenceNumber;
    private String referenceNumberDate;
    private String customerName;
    private String birthDate;
    private String gender;
    private String phoneNumber;
    private String channel;
    private String transactionType;
    private String transactionStatus;
}
