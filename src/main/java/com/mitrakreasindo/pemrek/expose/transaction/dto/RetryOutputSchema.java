package com.mitrakreasindo.pemrek.expose.transaction.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class RetryOutputSchema {
	private String referenceNumber;
	private String status;
}	
