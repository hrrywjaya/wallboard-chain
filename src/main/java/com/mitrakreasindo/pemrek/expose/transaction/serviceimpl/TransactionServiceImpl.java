package com.mitrakreasindo.pemrek.expose.transaction.serviceimpl;

import java.io.IOException;
import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorMessage;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorSchema;
import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.transaction.TransactionErrorCode;
import com.mitrakreasindo.pemrek.expose.transaction.converter.ServiceStatusConverter;
import com.mitrakreasindo.pemrek.expose.transaction.converter.TransactionStatusConverter;
import com.mitrakreasindo.pemrek.expose.transaction.dto.ServiceStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatusDetail;
import com.mitrakreasindo.pemrek.expose.transaction.model.TransaksiStatusDetailProjection;
import com.mitrakreasindo.pemrek.expose.transaction.model.TransaksiStatusProjection;
import com.mitrakreasindo.pemrek.expose.transaction.service.TransactionService;
import com.mitrakreasindo.pemrek.repository.CallLogRepository;
import com.mitrakreasindo.pemrek.repository.TransaksiRepository;
import com.mitrakreasindo.pemrek.model.Transaksi;
import com.mitrakreasindo.pemrek.model.TransactionMessage.TransactionMessageStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransaksiRepository transactionRepository;
    @Autowired
    private CallLogRepository callLogRepository;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private JdbcTemplate jdbcChainTemplate;

    @Override
    public Output<List<TransactionStatus>> findTransactionStatusByHandphone(String handphone) {   
        Output<List<TransactionStatus>> output = new Output<>();
		ErrorSchema errorSchema = new ErrorSchema();
        ErrorMessage errorMessage = new ErrorMessage();
             
        String sqlQuery = "SELECT * FROM (\r\n" + 
        		"	SELECT\r\n" + 
        		"		calllog0_.created_date AS createdDate,\r\n" + 
        		"		calllog0_.no_ref AS noRef,\r\n" + 
        		"		transaksi1_.eform_created_date AS eformCreatedDate,\r\n" + 
        		"		calllog0_.qr_channel AS qrChannel,\r\n" + 
        		"		calllog0_.qr_jenis_transaksi AS qrJenisTransaksi,\r\n" + 
        		"		calllog0_.transaksi_status AS transaksiStatus,\r\n" + 
        		"		calllog0_.old_raw_konfirmasi_data AS oldRawKonfirmasiData,\r\n" + 
        		"		calllog0_.raw_konfirmasi_data AS rawKonfirmasiData,\r\n" + 
                "		calllog0_.tolak_md_id AS tolakMdId,\r\n" + 
                "		calllog0_.work_ready_date AS workReadyDate,\r\n" + 
                "		calllog0_.dropped_date AS droppedDate,\r\n" + 
                "		ROW_NUMBER() over(PARTITION BY calllog0_.no_ref ORDER BY calllog0_.created_date DESC) AS rn\r\n" +
        		"	FROM\r\n" + 
        		"		call_log calllog0_\r\n" + 
        		"	INNER JOIN transaksi transaksi1_ ON\r\n" + 
        		"		(calllog0_.transaksi_id = transaksi1_.id\r\n" + 
        		"		AND calllog0_.qr_nomor_hp = '"+handphone+"')\r\n" + 
        		"	ORDER BY calllog0_.CREATED_DATE DESC\r\n" + 
        		") t WHERE t.rn = 1 and t.workReadyDate is not null";

        List<TransaksiStatusProjection> result = jdbcChainTemplate.query(
        		sqlQuery, 
        		(rs, rowNum) -> new TransaksiStatusProjection(
        				rs.getTimestamp("createdDate"), 
        				rs.getString("noRef"), 
        				rs.getString("eformCreatedDate"), 
        				rs.getString("qrChannel"), 
        				rs.getString("qrJenisTransaksi"), 
        				rs.getString("transaksiStatus"), 
        				rs.getString("oldRawKonfirmasiData"), 
                        rs.getString("rawKonfirmasiData"),
                        rs.getString("workReadyDate"), 
        				rs.getString("tolakMdId")));

            List<TransactionStatus> resultList = result.stream().map(trx -> { 
                JsonNode konfirmasiData = parseKonfirmasiData(trx.getOldRawKonfirmasiData(), trx.getRawKonfirmasiData());
                TransactionStatus trxStatus  = new TransactionStatus(
                                            trx.getCreatedDate().getTime(),
                                            trx.getNoRef(),
                                            trx.getEformCreatedDate(), 
                                            getStringContent(konfirmasiData, "namaNasabah"), 
                                            getStringContent(konfirmasiData, "tanggalLahir"), 
                                            getStringContent(konfirmasiData, "jenisKelamin"),
                                            getStringContent(konfirmasiData, "nomorHp"), 
                                            trx.getQrChannel(), 
                                            trx.getQrJenisTransaksi(), 
                                            TransactionStatusConverter.convert(trx.getTransaksiStatus(), trx.getTolakMdId(), trx.getWorkReadyDate()).name());
                return trxStatus;
            })
            .collect(Collectors.toList());

        errorMessage.setEnglish("Success");
        errorMessage.setIndonesian("Berhasil");
        errorSchema.setErrorCode(TransactionErrorCode.SUCCESS);				
        errorSchema.setErrorMessage(errorMessage);
        output.setErrorSchema(errorSchema);
        output.setOutputSchema(resultList);
        
        printLog(output);

        return output;

    }

    @Override
    public Output<List<TransactionStatus>> findTransactionStatusByStatus(String status, String startDate, String endDate) {
        Output<List<TransactionStatus>> output = new Output<>();
		ErrorSchema errorSchema = new ErrorSchema();
        ErrorMessage errorMessage = new ErrorMessage();

        String byStatus = "";
     
        switch (status) {
        case "Berhasil":
            byStatus = " and t.transaksiStatus = 'SUCCESS' ";
            break;
        case "Terputus":
            byStatus = " and t.tolakMdId is null and t.transaksiStatus is null and t.workReadyDate is not null";
            break;
        case "Ditolak":
            byStatus = " and t.tolakMdId is not null ";
            break;
        case "Pending":
            byStatus = " and t.transaksiStatus = 'PENDING' ";
            break;
        }
  
        String sqlQuery = "SELECT\r\n" + 
    		"	*\r\n" + 
    		"FROM\r\n" + 
    		"	(\r\n" + 
    		"	SELECT\r\n" + 
    		"		calllog0_.created_date AS createdDate,\r\n" + 
    		"		calllog0_.no_ref AS noRef,\r\n" + 
    		"		transaksi1_.eform_created_date AS eformCreatedDate,\r\n" + 
    		"		calllog0_.qr_channel AS qrChannel,\r\n" + 
    		"		calllog0_.qr_jenis_transaksi AS qrJenisTransaksi,\r\n" + 
    		"		calllog0_.transaksi_status AS transaksiStatus,\r\n" + 
    		"		calllog0_.old_raw_konfirmasi_data AS oldRawKonfirmasiData,\r\n" + 
    		"		calllog0_.raw_konfirmasi_data AS rawKonfirmasiData,\r\n" + 
            "		calllog0_.tolak_md_id AS tolakMdId,\r\n" + 
            "		calllog0_.work_ready_date AS workReadyDate,\r\n" + 
    		"		calllog0_.dropped_date AS droppedDate,\r\n" + 
    		"		ROW_NUMBER() over(PARTITION BY calllog0_.no_ref ORDER BY calllog0_.created_date DESC) AS rn\r\n" + 
    		"	FROM\r\n" + 
    		"		call_log calllog0_\r\n" + 
    		"	INNER JOIN transaksi transaksi1_ ON\r\n" + 
    		"		(calllog0_.transaksi_id = transaksi1_.id\r\n" + 
    		"		AND (calllog0_.created_date >= to_date('"+startDate+"', 'yyyy-mm-dd') AND calllog0_.created_date < to_date('"+endDate+"', 'yyyy-mm-dd')+1))\r\n" +
    		"	ORDER BY calllog0_.created_date DESC) t\r\n" +
    		"WHERE\r\n" + 
            "	t.rn = 1\r\n" +
            "   "+byStatus+"";
        
        List<TransaksiStatusProjection> result = jdbcChainTemplate.query(
        		sqlQuery, 
        		(rs, rowNum) -> new TransaksiStatusProjection(
        				rs.getTimestamp("createdDate"), 
        				rs.getString("noRef"), 
        				rs.getString("eformCreatedDate"), 
        				rs.getString("qrChannel"), 
        				rs.getString("qrJenisTransaksi"), 
        				rs.getString("transaksiStatus"), 
        				rs.getString("oldRawKonfirmasiData"), 
                        rs.getString("rawKonfirmasiData"), 
                        rs.getString("workReadyDate"),
        				rs.getString("tolakMdId")));

                        List<TransactionStatus> resultList =  result.stream().map(trx -> { 
            JsonNode konfirmasiData = parseKonfirmasiData(trx.getOldRawKonfirmasiData(), trx.getRawKonfirmasiData());
            TransactionStatus trxStatus  = new TransactionStatus(
            							trx.getCreatedDate().getTime(),
                                        trx.getNoRef(),
                                        trx.getEformCreatedDate(), 
                                        getStringContent(konfirmasiData, "namaNasabah"), 
                                        getStringContent(konfirmasiData, "tanggalLahir"), 
                                        getStringContent(konfirmasiData, "jenisKelamin"),
                                        getStringContent(konfirmasiData, "nomorHp"), 
                                        trx.getQrChannel(), 
                                        trx.getQrJenisTransaksi(), 
                                        TransactionStatusConverter.convert(trx.getTransaksiStatus(), trx.getTolakMdId(), trx.getWorkReadyDate()).name());
            return trxStatus;
        }).collect(Collectors.toList());

        errorMessage.setEnglish("Success");
        errorMessage.setIndonesian("Berhasil");
        errorSchema.setErrorCode(TransactionErrorCode.SUCCESS);				
        errorSchema.setErrorMessage(errorMessage);
        output.setErrorSchema(errorSchema);
        output.setOutputSchema(resultList);

        printLog(output);

        return output;
    }

    @Override
    public Output<List<ServiceStatus>> findServiceStatus(String refNum, String createdDate) {
        Output<List<ServiceStatus>> output = new Output<>();
		ErrorSchema errorSchema = new ErrorSchema();
        ErrorMessage errorMessage = new ErrorMessage();

        List<ServiceStatus> status = null;
        Optional<Transaksi> transaction = transactionRepository.findByRefNumAndEformCreatedDate(refNum, createdDate);
        if (transaction.isPresent()) {
            Transaksi transaksi = transaction.get();
            String jsonStatusMap = transaksi.getTransactionStatusMap();
            if (jsonStatusMap != null) {
                List<TransactionMessageStatus> val = null;

                try {
                    val = mapper.readValue(jsonStatusMap, new TypeReference<List<TransactionMessageStatus>>() {
                    });
                    status = val.stream().map(s -> new ServiceStatus(s.getName().name(), s.getDescription(),
                            ServiceStatusConverter.convert(s.getStatus()))).collect(Collectors.toList());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        errorMessage.setEnglish("Success");
        errorMessage.setIndonesian("Berhasil");
        errorSchema.setErrorCode(TransactionErrorCode.SUCCESS);				
        errorSchema.setErrorMessage(errorMessage);
        output.setErrorSchema(errorSchema);
        output.setOutputSchema(status);
        
        printLog(output);

        return output;
    }

    @Override
    public Output<TransactionStatusDetail> findTransactionStatusDetail(String nomorReferensi, String tanggal) {
        Output<TransactionStatusDetail> output = new Output<>();
		ErrorSchema errorSchema = new ErrorSchema();
        ErrorMessage errorMessage = new ErrorMessage();

        TransactionStatusDetail resultDetail = callLogRepository.findTransaksiStatusDetail(nomorReferensi, tanggal)
            .stream()
            .findFirst()
            .map(projection -> {
            TransactionStatusDetail detail = new TransactionStatusDetail();

            JsonNode konfirmasiData = parseKonfirmasiData(projection.getOldRawKonfirmasiData(), projection.getRawKonfirmasiData());

            String catatanTransaksi = null;
            switch (TransactionStatusConverter.convert(projection.getTransaksiStatus(),
                    projection.getTolakMdId(), projection.getWorkReadyDate())) {
                case Terputus:
                    catatanTransaksi = projection.getTerputusMessage();
                    break;
                case Ditolak:
                    catatanTransaksi = projection.getTolakMessage();
                    break;
                case Berhasil:
                    catatanTransaksi = projection.getServiceLevelMessage();
                    break;  
            }

            detail.setReferenceNumber(projection.getNoRef());
            detail.setReferenceNumberDate(projection.getEformCreatedDate());
            detail.setCallTime(projection.getCreatedDate().getTime());
            detail.setCustomerName(getStringContent(konfirmasiData, "namaNasabah"));
            detail.setBirthPlace(getStringContent(konfirmasiData, "tempatLahir"));
            detail.setBirthDate(getStringContent(konfirmasiData, "tanggalLahir"));
            detail.setGender(getStringContent(konfirmasiData, "jenisKelamin"));
            detail.setMotherName(getStringContent(konfirmasiData, "namaGadisIbuKandung"));
            detail.setPhoneNumber(getStringContent(konfirmasiData, "nomorHp"));
            detail.setEmail(getStringContent(konfirmasiData, "email"));
            detail.setChannel(projection.getQrChannel());
            detail.setTransactionType(projection.getQrJenisTransaksi());
            detail.setAccountNumber(projection.getQrNoRekening());
            detail.setProduct(getStringContent(konfirmasiData, "jenisRekening") + " - "+ getStringContent(konfirmasiData, "jenisRekeningTemp"));
            detail.setCardType(getStringContent(konfirmasiData, "tipeKartuPasporBcaTemp"));
            detail.setBranch(getStringContent(konfirmasiData, "cabang") + " - " + getStringContent(konfirmasiData, "cabangTemp"));
            detail.setTransactionStatus(TransactionStatusConverter.convert(projection.getTransaksiStatus(), projection.getTolakMdId(), projection.getWorkReadyDate()).name());
            detail.setCsoName(projection.getFullName());
            detail.setSpvName(projection.getApprovalFullname());
            detail.setTransactionNote(catatanTransaksi);

            return detail;
        }).orElse(null);
        
        
        errorMessage.setEnglish("Success");
        errorMessage.setIndonesian("Berhasil");
        errorSchema.setErrorCode(TransactionErrorCode.SUCCESS);				
        errorSchema.setErrorMessage(errorMessage);
        output.setErrorSchema(errorSchema);
        output.setOutputSchema(resultDetail);

        printLog(output);

        return output;
    }

    private JsonNode parseKonfirmasiData(String oldRawKonfirmasiData, String rawKonfirmasiData) {
    	if (oldRawKonfirmasiData == null && oldRawKonfirmasiData == null)
    		return null;
    	try {
            return mapper.readTree(rawKonfirmasiData != null ? rawKonfirmasiData : oldRawKonfirmasiData);
        } catch (JsonMappingException e) {
            System.out.println("failed parsing konfirmasi data");
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            System.out.println("failed parsing konfirmasi data");
            e.printStackTrace();
        }
        return null;        
    }
    
    private String getStringContent(JsonNode jsonNode, String field) {
    	return jsonNode == null ? "" : jsonNode.get(field).asText();
    }

    private void printLog(Object object) {
        try {
            String json = mapper.writeValueAsString(object);
            log.debug("response : "+json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
    }

}
