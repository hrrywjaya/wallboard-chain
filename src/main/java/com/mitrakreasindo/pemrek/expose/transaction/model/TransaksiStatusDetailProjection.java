package com.mitrakreasindo.pemrek.expose.transaction.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransaksiStatusDetailProjection {

    private String noRef;
    private String eformCreatedDate;
    private Date createdDate; // call log created date / waktu panggilan
    private String qrChannel;
    private String qrJenisTransaksi;
    private String qrNoRekening;    
    private String oldRawKonfirmasiData;
    private String rawKonfirmasiData;
    private String transaksiStatus;
    private String fullName;
    private String approvalFullname;
    private String tolakMdId;
    private String tolakMessage;
    private String droppedDate;
    private String workReadyDate;
    private String terputusMessage;
    private String serviceLevelMdId;
    private String serviceLevelMessage;
}

