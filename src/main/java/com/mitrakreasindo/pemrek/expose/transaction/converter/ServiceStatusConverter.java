package com.mitrakreasindo.pemrek.expose.transaction.converter;

import com.mitrakreasindo.pemrek.model.TransactionMessage.Status;

public class ServiceStatusConverter {
	
	public static int convert(Status status) {
        if (status == Status.NOT_RUNNING) {
            return 0;
        } else if (status == Status.SUCCESS) {
            return 1;
        } else if (status == Status.FAILED_RETRAY_ABLE) {
            return 2;
        } else if (status == Status.FAILED_NOT_RETRAY_ABLE) {
            return 3;
        } else if (status == Status.ON_PROGRESS) {
            return 4;
        }

        throw new RuntimeException("unknown transaction status");
    }
}
