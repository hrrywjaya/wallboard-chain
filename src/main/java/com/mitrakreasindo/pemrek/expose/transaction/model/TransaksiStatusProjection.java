package com.mitrakreasindo.pemrek.expose.transaction.model;

import java.sql.Timestamp;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransaksiStatusProjection {

    // call log created date
    private Timestamp createdDate;
    private String noRef;
    private String eformCreatedDate;
    private String qrChannel;
    private String qrJenisTransaksi;
    private String transaksiStatus;
    private String oldRawKonfirmasiData;
    private String rawKonfirmasiData;
    private String workReadyDate;
    private String tolakMdId;
    // private String droppedDate;    
}