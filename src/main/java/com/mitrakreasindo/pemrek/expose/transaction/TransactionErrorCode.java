package com.mitrakreasindo.pemrek.expose.transaction;

public class TransactionErrorCode {
	
	public static String SUCCESS = "CH-TRC-000";
	public static String FAILED = "CH-TRC-001";

	public static String RETRY_SUCCESS = "CH-TRCR-000";
	public static String RETRY_ERROR_TRANSACTION_IN_PROCCESS = "CH-TRCR-001";
	public static String RETRY_ERROR_TRANSACTION_FAILED = "CH-TRCR-002";
	public static String RETRY_ERROR_TRANSACTION_NOT_MATCH = "CH-TRCR-003";
	public static String RETRY_ERROR_TRANSACTION_NOT_FOUND = "CH-TRCR-004";
}
