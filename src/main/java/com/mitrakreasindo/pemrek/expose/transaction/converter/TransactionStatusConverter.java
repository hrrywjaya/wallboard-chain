package com.mitrakreasindo.pemrek.expose.transaction.converter;

import com.mitrakreasindo.pemrek.expose.transaction.model.TransaksiStatusProjection;

public class TransactionStatusConverter {

    public enum Status {
        Ditolak, Berhasil, Terputus, Pending
    }

    public static Status convert(String transaksiStatus, String tolakMdId, String workReadyDate) {
        if (tolakMdId != null)
            return Status.Ditolak;
        else if ("SUCCESS".equals(transaksiStatus))
            return Status.Berhasil;
        // else if (terputusMdId != null && transaksiStatus != null)
        //     return Status.Pending;
        else if ("PENDING".equals(transaksiStatus))
            return Status.Pending;
        else if (tolakMdId == null && transaksiStatus == null & workReadyDate != null)
            return Status.Terputus;
        return null;
    }
    
}