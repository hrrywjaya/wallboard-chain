package com.mitrakreasindo.pemrek.expose.transaction.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(SnakeCaseStrategy.class)
public class TransactionStatusDetail {
	
	 private String referenceNumber;
	    private String referenceNumberDate;
	    private Long callTime; // call log created date / waktu panggilan
	    private String customerName;
	    private String birthPlace;
	    private String birthDate;
	    private String gender;
		private String motherName;
	    private String phoneNumber;
		private String email;
	    private String channel;
	    private String transactionType;
	    private String accountNumber;
	    private String product;
	    private String cardType;
	    private String branch;
	    private String transactionStatus;
	    private String csoName;
	    private String spvName;
	    private String transactionNote;
}
