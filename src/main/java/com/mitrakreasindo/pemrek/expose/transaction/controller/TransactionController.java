package com.mitrakreasindo.pemrek.expose.transaction.controller;

import java.util.List;

import com.mitrakreasindo.pemrek.expose.common.model.ErrorMessage;
import com.mitrakreasindo.pemrek.expose.common.model.ErrorSchema;
import com.mitrakreasindo.pemrek.expose.common.model.Output;
import com.mitrakreasindo.pemrek.expose.transaction.TransactionErrorCode;
import com.mitrakreasindo.pemrek.expose.transaction.dto.RetryOutputSchema;
import com.mitrakreasindo.pemrek.expose.transaction.dto.ServiceStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatus;
import com.mitrakreasindo.pemrek.expose.transaction.dto.TransactionStatusDetail;
import com.mitrakreasindo.pemrek.expose.transaction.service.TransactionService;
import com.mitrakreasindo.pemrek.service.RingkasanTransaksiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chain/transactions")
public class TransactionController {
	@Autowired
    private TransactionService transactionService;
    @Autowired
    private RingkasanTransaksiService ringkasanTransaksiService;

    @GetMapping("/status")
    public Output<List<TransactionStatus>> findTransactionStatus(
        @RequestParam(value = "handphone", required = false) String handphone, 
        @RequestParam(value = "status", required = false) String status, 
        @RequestParam(value = "start-date", required = false) String startDate,
        @RequestParam(value = "end-date", required = false) String endDate) {
            Output<List<TransactionStatus>> output = new Output<>();
            ErrorSchema errorSchema = new ErrorSchema();
            ErrorMessage errorMessage = new ErrorMessage();

            if ((handphone == null && status == null) ||  (handphone != null && status != null)) {
                errorMessage.setEnglish("Determine filters based on phone or status");
                errorMessage.setIndonesian("Tentukan filter berdasarkan handphone atau status");
                errorSchema.setErrorCode(TransactionErrorCode.FAILED);				
                errorSchema.setErrorMessage(errorMessage);
                output.setErrorSchema(errorSchema);
                output.setOutputSchema(null);
                return output;
            } else if (handphone != null && status == null) {
                return transactionService.findTransactionStatusByHandphone(handphone);
            } else if (handphone == null && status != null) {
                if (startDate == null || endDate == null) {
                    errorMessage.setEnglish("start-date and end-date must be filled");
                    errorMessage.setIndonesian("start-date dan tanggal akhir harus diisi");
                    errorSchema.setErrorCode(TransactionErrorCode.FAILED);				
                    errorSchema.setErrorMessage(errorMessage);
                    output.setErrorSchema(errorSchema);
                    output.setOutputSchema(null);
                    return output;
                }
                return transactionService.findTransactionStatusByStatus(status, startDate, endDate);
            }

        return null;
    }

    @GetMapping("/status/services")
    public Output<List<ServiceStatus>> findServiceStatus(
        @RequestParam("reference-number") String refNum, 
        @RequestParam("created-date") String createdDate) {
        return transactionService.findServiceStatus(refNum, createdDate);
    }
        
    @GetMapping("/status/details")
    public Output<TransactionStatusDetail> findTransactionStatusDetail(
        @RequestParam("reference-number") String referenceNumber,
        @RequestParam("created-date") String tanggal) {
        return transactionService.findTransactionStatusDetail(referenceNumber, tanggal);
    }

    @PostMapping("/retry")
    public Output<RetryOutputSchema> processRetry(
        @RequestParam("reference-number") String noRef, 
        @RequestParam("created-date") String norRefCretedDate, 
        @RequestParam("username") String username) {
        return ringkasanTransaksiService.processRetry(noRef, norRefCretedDate, username);
    }
}
