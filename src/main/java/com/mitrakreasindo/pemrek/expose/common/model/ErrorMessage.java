package com.mitrakreasindo.pemrek.expose.common.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class ErrorMessage
{

	private String english;
	private String indonesian;
	
}
