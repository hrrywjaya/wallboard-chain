package com.mitrakreasindo.pemrek.unit.controller;
//package com.mitrakreasindo.chain.unit.controller;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.mockito.ArgumentMatchers.isA;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.Pageable;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.mitrakreasindo.chain.controller.ProjectController;
//import com.mitrakreasindo.chain.model.Project;
//import com.mitrakreasindo.chain.service.ProjectService;
//
///**
// * @author miftakhul
// *
// */
//@RunWith(SpringRunner.class)
//@WebMvcTest(ProjectController.class)
//public class ProjectControllerTest
//{
//
//	@Autowired
//	private MockMvc mvc;
//
//	@MockBean
//	private ProjectService service;
//
//	@Test
//	public void getAllProjectWithPage () throws Exception
//	{
//		Project project = new Project();
//		project.name = "project1";
//
//		Page<Project> page = new PageImpl<>(Arrays.asList(project));
//		given(service.findAll(isA(Pageable.class))).willReturn(page);
//
//		mvc.perform(get("/projects")).andDo(print()).andExpect(status().isOk())
//				.andExpect(jsonPath("$.content[0].name", equalTo(project.name)));
//	}
//	
//}
