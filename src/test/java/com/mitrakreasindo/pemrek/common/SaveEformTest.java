package com.mitrakreasindo.pemrek.common;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.OffsetDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SaveEformTest
{


	@Autowired
	private GatewayService gat;
	@Autowired
	private AdGatewayService ad;
	@Autowired
	private BcaSignature hmac;
	
//	@Test
	public void saveEform ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "POST";
		String relativeUrl = "/eform/form-transactions/api/save";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		String json = "{\r\n" + 
				"\"form_data_list\" : [\r\n" + 
				"{\r\n" + 
				"\"ref_num\" : \"\",\r\n" + 
				"\"channel_id\" : \"E\",\r\n" + 
				"\"trans_id\" : \"01\",\r\n" + 
				"\"template_version\" : \"\",\r\n" + 
				"\"status\" : \"\",\r\n" + 
				"\"created_by\" : \"U057101\",\r\n" + 
				"\"form_data\": {\r\n" + 
				"\"TIPE_KARTU_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"NAMA_TEMPAT_BEKERJA\": \"PT BCA TBK\",\r\n" + 
				"        \"NO_CUSTOMER_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_FIN_MBCA\": \"0\",\r\n" + 
				"        \"SERIAL_NUMBER_TOKEN\": \"\",\r\n" + 
				"        \"NAMA_JALAN\": \"NGLAMPENG\",\r\n" + 
				"        \"ALAMAT_KANTOR_3\": \"Jakarta barat/Indonesia\",\r\n" + 
				"        \"BERLAKU_SAMPAI_DENGAN\": \"2026-04-30\",\r\n" + 
				"        \"STATUS_PERKAWINAN\": \"1\",\r\n" + 
				"        \"PROVINSI_REKENING_KORAN\": \"DKI Jakarta\",\r\n" + 
				"        \"VALIDASI_CATATAN_BANK_KATEGORI_NASABAH\": \"\",\r\n" + 
				"        \"STATUS_REKENING\": \"\",\r\n" + 
				"        \"TIPE_IDENTITAS_LAINNYA\": \"1\",\r\n" + 
				"        \"NO_CUSTOMER\": \"\",\r\n" + 
				"        \"NOMOR_TELEPON_RUMAH\": \"0211234544\",\r\n" + 
				"        \"AGAMA\": \"3\",\r\n" + 
				"        \"ALAMAT_KANTOR_1\": \"BCA Wisma asia 2 lt 12 B\",\r\n" + 
				"        \"ALAMAT_KANTOR_2\": \"jl Brigjen Katamso No 1\",\r\n" + 
				"        \"KD_PENDUDUK\": \"\",\r\n" + 
				"        \"OD_PLAN\": \"\",\r\n" + 
				"        \"SERVICE_CHARGE_CODE\": \"\",\r\n" + 
				"        \"FORM_PERPAJAKAN_CRS\": \"Y\",\r\n" + 
				"        \"TEMPAT_LAHIR\": \"JAKARTA\",\r\n" + 
				"        \"PERNYATAAN_FASILITAS_BCA\": \"0\",\r\n" + 
				"        \"PERSETUJUAN_TELEPON\": \"Y\",\r\n" + 
				"        \"JENIS_REKENING\": \"110\",\r\n" + 
				"        \"WITHOLDING_PLAN\": \"\",\r\n" + 
				"        \"JABATAN\": \"Wakil Presiden\",\r\n" + 
				"        \"NOMOR_HP\": \"000021820555440\",\r\n" + 
				"        \"NIK_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"NAMA_GEDUNG\": \"Apartemen\",\r\n" + 
				"        \"NPWP\": \"I\",\r\n" + 
				"        \"SUMBER_PENGHASILAN\": \"1\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_MBCA\": \"0\",\r\n" + 
				"        \"KODE_AREA_TELEPON_KANTOR\": \"62\",\r\n" + 
				"        \"PERSETUJUAN_SMS\": \"Y\",\r\n" + 
				"        \"NAMA_GADIS_IBU_KANDUNG\": \"KAORI MIYAZONO\",\r\n" + 
				"        \"NAMA_DICETAK_DI_KARTU\": \"\",\r\n" + 
				"        \"COST_CENTER\": \"\",\r\n" + 
				"        \"KABUPATEN_REKENING_KORAN\": \"Jakarta\",\r\n" + 
				"        \"NAMA_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"INTEREST_PLAN\": \"\",\r\n" + 
				"        \"KODE_NEGARA_FAKSIMILIE\": \"62\",\r\n" + 
				"        \"DENGAN_BUKU\": \"Y\",\r\n" + 
				"        \"SUMBER_PENGHASILAN_LAINNYA\": \"\",\r\n" + 
				"        \"DALAM_HAL_INI_BERTINDAK\": \"000\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_KLIKBCA\": \"0\",\r\n" + 
				"        \"NOMOR_NPWP\": \"314124134314134\",\r\n" + 
				"        \"KITAS_KITAP\": \"\",\r\n" + 
				"        \"KODE_NEGARA_NOMOR_HP_2\": \"62\",\r\n" + 
				"        \"NOMOR_FAKSIMILIE\": \"0211234547\",\r\n" + 
				"        \"KODE_NEGARA_NOMOR_HP\": \"52\",\r\n" + 
				"        \"PERNYATAAN_PRODUK\": \"1\",\r\n" + 
				"        \"NOMOR_REKENING_BARU\": \"\",\r\n" + 
				"        \"FORM_PERPAJAKAN_FATCA\": \"Y\",\r\n" + 
				"        \"BERLAKU_SAMPAI_DENGAN_KITAS_KITAP\": \"\",\r\n" + 
				"        \"KECAMATAN\": \"KLATEN SELATAN\",\r\n" + 
				"        \"PERSETUJUAN_EMAIL\": \"Y\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_KEYBCA\": \"0\",\r\n" + 
				"        \"KELURAHAN_REKENING_KORAN\": \"Jakarta Utara\",\r\n" + 
				"        \"BAHASA_PETUNJUK_LAYAR_ATM\": \"1\",\r\n" + 
				"        \"JENIS_KELAMIN\": \"L\",\r\n" + 
				"        \"NEGARA_LAHIR_FATCA\": \"ID\",\r\n" + 
				"        \"PERSETUJUAN_DATA_PIHAK_KETIGA\": \"Y\",\r\n" + 
				"        \"EMAIL\": \"CRM2@BCA.COM\",\r\n" + 
				"        \"NAMA_GEDUNG_REKENING_KORAN\": \"\",\r\n" + 
				"        \"JENIS_KARTU_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"TOTAL_PENGHASILAN\": \"1\",\r\n" + 
				"        \"CABANG\": \"0008\",\r\n" + 
				"        \"KODE_AREA_TELEPON_RUMAH\": \"62\",\r\n" + 
				"        \"KODE_NEGARA_DATA_REKENING\": \"\",\r\n" + 
				"        \"NOMOR_HP_MBCA\": \"\",\r\n" + 
				"        \"KODE_NEGARA_TELEPON_KANTOR\": \"62\",\r\n" + 
				"        \"KECAMATAN_REKENING_KORAN\": \"Jl pademangan raya no 19\",\r\n" + 
				"        \"PERNYATAAN_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"NOMOR_REKENING_EXISTING\": \"\",\r\n" + 
				"        \"KODE_POS_REKENING_KORAN\": \"15492\",\r\n" + 
				"        \"KELURAHAN\": \"RT 02 RW 03 JETIS\",\r\n" + 
				"        \"REKENING_UNTUK\": \"Pribadi\",\r\n" + 
				"        \"WAJIB_PAJAK_NEGARA_LAIN\": \"N\",\r\n" + 
				"        \"NAMA_NASABAH\": \"KAMASHWANEE RIZELS\",\r\n" + 
				"        \"PEKERJAAN\": \"6\",\r\n" + 
				"        \"NOMOR_KITAS_KITAP\": \"\",\r\n" + 
				"        \"PERIODE_BIAYA_ADMIN\": \"\",\r\n" + 
				"        \"PERNYATAAN_FASILITAS_BCA_DETAIL\": \"\",\r\n" + 
				"        \"TIN_SSN\": \"\",\r\n" + 
				"        \"KOTA_KANTOR\": \"\",\r\n" + 
				"        \"PEJABAT_BANK\": \"\",\r\n" + 
				"        \"KABUPATEN\": \"KLATEN SELATAN\",\r\n" + 
				"        \"NEGARA\": \"ID\",\r\n" + 
				"        \"TUJUAN_PEMBUKAAN_REKENING\": \"1\",\r\n" + 
				"        \"BIAYA_ADMIN\": \"\",\r\n" + 
				"        \"NEGARA_ALAMAT\": \"INDONESIA\",\r\n" + 
				"        \"RT_RW\": \"022033\",\r\n" + 
				"        \"RT_RW_REKENING_KORAN\": \"002003\",\r\n" + 
				"        \"KODE_PERORANGAN_BISNIS\": \"\",\r\n" + 
				"        \"GOLONGAN_PEMILIK\": \"\",\r\n" + 
				"        \"BIDANG_USAHA\": \"Pemerintahan\",\r\n" + 
				"        \"KODE_POS\": \"57421\",\r\n" + 
				"        \"KODE_PENAMBAHAN_PAJAK\": \"\",\r\n" + 
				"        \"NOMOR_HP_2\": \"000008151215151\",\r\n" + 
				"        \"KODE_AREA_FAKSIMILIE\": \"62\",\r\n" + 
				"        \"PROVINSI\": \"DKI\",\r\n" + 
				"        \"PERIODE_R_K\": \"\",\r\n" + 
				"        \"NEGARA_ALAMAT_REKENING_KORAN\": \"Indonesia\",\r\n" + 
				"        \"NIL\": \"1311032908900002\",\r\n" + 
				"        \"USER_CODE\": \"\",\r\n" + 
				"        \"NIK\": \"\",\r\n" + 
				"        \"NAMA_JALAN_REKENING_KORAN\": \"Indonesia\",\r\n" + 
				"        \"PERIODE_BUNGA\": \"\",\r\n" + 
				"        \"NOMOR_TELEPON_KANTOR\": \"0211234546\",\r\n" + 
				"        \"PERNYATAAN_PRODUK_DETAIL\": \"Tahapan\",\r\n" + 
				"        \"TANGGAL_LAHIR\": \"1971-01-01\",\r\n" + 
				"        \"KODE_POS_KANTOR\": \"\",\r\n" + 
				"        \"KODE_NEGARA_TELEPON_RUMAH\": \"62\",\r\n" + 
				"        \"WAJIB_FATCA\": \"N\",\r\n" + 
				"        \"PENGIRIMAN_REKENING_KORAN\": \"1\"\r\n" + 
				"}\r\n" + 
				"},\r\n" + 
				"{\r\n" + 
				"\"ref_num\" : \"\",\r\n" + 
				"\"channel_id\" : \"E\",\r\n" + 
				"\"trans_id\" : \"01\",\r\n" + 
				"\"template_version\" : \"\",\r\n" + 
				"\"status\" : \"\",\r\n" + 
				"\"created_by\" : \"U057101\",\r\n" + 
				"\"form_data\": {\r\n" + 
				"\"TIPE_KARTU_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"NAMA_TEMPAT_BEKERJA\": \"PT BCA TBK\",\r\n" + 
				"        \"NO_CUSTOMER_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_FIN_MBCA\": \"0\",\r\n" + 
				"        \"SERIAL_NUMBER_TOKEN\": \"\",\r\n" + 
				"        \"NAMA_JALAN\": \"NGLAMPENG\",\r\n" + 
				"        \"ALAMAT_KANTOR_3\": \"Jakarta barat/Indonesia\",\r\n" + 
				"        \"BERLAKU_SAMPAI_DENGAN\": \"2026-04-30\",\r\n" + 
				"        \"STATUS_PERKAWINAN\": \"1\",\r\n" + 
				"        \"PROVINSI_REKENING_KORAN\": \"DKI Jakarta\",\r\n" + 
				"        \"VALIDASI_CATATAN_BANK_KATEGORI_NASABAH\": \"\",\r\n" + 
				"        \"STATUS_REKENING\": \"\",\r\n" + 
				"        \"TIPE_IDENTITAS_LAINNYA\": \"1\",\r\n" + 
				"        \"NO_CUSTOMER\": \"\",\r\n" + 
				"        \"NOMOR_TELEPON_RUMAH\": \"0211234544\",\r\n" + 
				"        \"AGAMA\": \"3\",\r\n" + 
				"        \"ALAMAT_KANTOR_1\": \"BCA Wisma asia 2 lt 12 B\",\r\n" + 
				"        \"ALAMAT_KANTOR_2\": \"jl Brigjen Katamso No 1\",\r\n" + 
				"        \"KD_PENDUDUK\": \"\",\r\n" + 
				"        \"OD_PLAN\": \"\",\r\n" + 
				"        \"SERVICE_CHARGE_CODE\": \"\",\r\n" + 
				"        \"FORM_PERPAJAKAN_CRS\": \"Y\",\r\n" + 
				"        \"TEMPAT_LAHIR\": \"JAKARTA\",\r\n" + 
				"        \"PERNYATAAN_FASILITAS_BCA\": \"0\",\r\n" + 
				"        \"PERSETUJUAN_TELEPON\": \"Y\",\r\n" + 
				"        \"JENIS_REKENING\": \"110\",\r\n" + 
				"        \"WITHOLDING_PLAN\": \"\",\r\n" + 
				"        \"JABATAN\": \"Wakil Presiden\",\r\n" + 
				"        \"NOMOR_HP\": \"000021820555440\",\r\n" + 
				"        \"NIK_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"NAMA_GEDUNG\": \"Apartemen\",\r\n" + 
				"        \"NPWP\": \"I\",\r\n" + 
				"        \"SUMBER_PENGHASILAN\": \"1\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_MBCA\": \"0\",\r\n" + 
				"        \"KODE_AREA_TELEPON_KANTOR\": \"62\",\r\n" + 
				"        \"PERSETUJUAN_SMS\": \"Y\",\r\n" + 
				"        \"NAMA_GADIS_IBU_KANDUNG\": \"KAORI MIYAZONO\",\r\n" + 
				"        \"NAMA_DICETAK_DI_KARTU\": \"\",\r\n" + 
				"        \"COST_CENTER\": \"\",\r\n" + 
				"        \"KABUPATEN_REKENING_KORAN\": \"Jakarta\",\r\n" + 
				"        \"NAMA_REKENING_GABUNGAN\": \"\",\r\n" + 
				"        \"INTEREST_PLAN\": \"\",\r\n" + 
				"        \"KODE_NEGARA_FAKSIMILIE\": \"62\",\r\n" + 
				"        \"DENGAN_BUKU\": \"Y\",\r\n" + 
				"        \"SUMBER_PENGHASILAN_LAINNYA\": \"\",\r\n" + 
				"        \"DALAM_HAL_INI_BERTINDAK\": \"000\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_KLIKBCA\": \"0\",\r\n" + 
				"        \"NOMOR_NPWP\": \"314124134314134\",\r\n" + 
				"        \"KITAS_KITAP\": \"\",\r\n" + 
				"        \"KODE_NEGARA_NOMOR_HP_2\": \"62\",\r\n" + 
				"        \"NOMOR_FAKSIMILIE\": \"0211234547\",\r\n" + 
				"        \"KODE_NEGARA_NOMOR_HP\": \"52\",\r\n" + 
				"        \"PERNYATAAN_PRODUK\": \"1\",\r\n" + 
				"        \"NOMOR_REKENING_BARU\": \"\",\r\n" + 
				"        \"FORM_PERPAJAKAN_FATCA\": \"Y\",\r\n" + 
				"        \"BERLAKU_SAMPAI_DENGAN_KITAS_KITAP\": \"\",\r\n" + 
				"        \"KECAMATAN\": \"KLATEN SELATAN\",\r\n" + 
				"        \"PERSETUJUAN_EMAIL\": \"Y\",\r\n" + 
				"        \"FASILITAS_YANG_DIINGINKAN_KEYBCA\": \"0\",\r\n" + 
				"        \"KELURAHAN_REKENING_KORAN\": \"Jakarta Utara\",\r\n" + 
				"        \"BAHASA_PETUNJUK_LAYAR_ATM\": \"1\",\r\n" + 
				"        \"JENIS_KELAMIN\": \"L\",\r\n" + 
				"        \"NEGARA_LAHIR_FATCA\": \"ID\",\r\n" + 
				"        \"PERSETUJUAN_DATA_PIHAK_KETIGA\": \"Y\",\r\n" + 
				"        \"EMAIL\": \"CRM2@BCA.COM\",\r\n" + 
				"        \"NAMA_GEDUNG_REKENING_KORAN\": \"\",\r\n" + 
				"        \"JENIS_KARTU_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"TOTAL_PENGHASILAN\": \"1\",\r\n" + 
				"        \"CABANG\": \"0008\",\r\n" + 
				"        \"KODE_AREA_TELEPON_RUMAH\": \"62\",\r\n" + 
				"        \"KODE_NEGARA_DATA_REKENING\": \"\",\r\n" + 
				"        \"NOMOR_HP_MBCA\": \"\",\r\n" + 
				"        \"KODE_NEGARA_TELEPON_KANTOR\": \"62\",\r\n" + 
				"        \"KECAMATAN_REKENING_KORAN\": \"Jl pademangan raya no 19\",\r\n" + 
				"        \"PERNYATAAN_PASPOR_BCA\": \"1\",\r\n" + 
				"        \"NOMOR_REKENING_EXISTING\": \"\",\r\n" + 
				"        \"KODE_POS_REKENING_KORAN\": \"15492\",\r\n" + 
				"        \"KELURAHAN\": \"RT 02 RW 03 JETIS\",\r\n" + 
				"        \"REKENING_UNTUK\": \"Pribadi\",\r\n" + 
				"        \"WAJIB_PAJAK_NEGARA_LAIN\": \"N\",\r\n" + 
				"        \"NAMA_NASABAH\": \"KAMASHWANEE RIZELS\",\r\n" + 
				"        \"PEKERJAAN\": \"6\",\r\n" + 
				"        \"NOMOR_KITAS_KITAP\": \"\",\r\n" + 
				"        \"PERIODE_BIAYA_ADMIN\": \"\",\r\n" + 
				"        \"PERNYATAAN_FASILITAS_BCA_DETAIL\": \"\",\r\n" + 
				"        \"TIN_SSN\": \"\",\r\n" + 
				"        \"KOTA_KANTOR\": \"\",\r\n" + 
				"        \"PEJABAT_BANK\": \"\",\r\n" + 
				"        \"KABUPATEN\": \"KLATEN SELATAN\",\r\n" + 
				"        \"NEGARA\": \"ID\",\r\n" + 
				"        \"TUJUAN_PEMBUKAAN_REKENING\": \"1\",\r\n" + 
				"        \"BIAYA_ADMIN\": \"\",\r\n" + 
				"        \"NEGARA_ALAMAT\": \"INDONESIA\",\r\n" + 
				"        \"RT_RW\": \"022033\",\r\n" + 
				"        \"RT_RW_REKENING_KORAN\": \"002003\",\r\n" + 
				"        \"KODE_PERORANGAN_BISNIS\": \"\",\r\n" + 
				"        \"GOLONGAN_PEMILIK\": \"\",\r\n" + 
				"        \"BIDANG_USAHA\": \"Pemerintahan\",\r\n" + 
				"        \"KODE_POS\": \"57421\",\r\n" + 
				"        \"KODE_PENAMBAHAN_PAJAK\": \"\",\r\n" + 
				"        \"NOMOR_HP_2\": \"000008151215151\",\r\n" + 
				"        \"KODE_AREA_FAKSIMILIE\": \"62\",\r\n" + 
				"        \"PROVINSI\": \"DKI\",\r\n" + 
				"        \"PERIODE_R_K\": \"\",\r\n" + 
				"        \"NEGARA_ALAMAT_REKENING_KORAN\": \"Indonesia\",\r\n" + 
				"        \"NIL\": \"1311032908900002\",\r\n" + 
				"        \"USER_CODE\": \"\",\r\n" + 
				"        \"NIK\": \"\",\r\n" + 
				"        \"NAMA_JALAN_REKENING_KORAN\": \"Indonesia\",\r\n" + 
				"        \"PERIODE_BUNGA\": \"\",\r\n" + 
				"        \"NOMOR_TELEPON_KANTOR\": \"0211234546\",\r\n" + 
				"        \"PERNYATAAN_PRODUK_DETAIL\": \"Tahapan\",\r\n" + 
				"        \"TANGGAL_LAHIR\": \"1971-01-01\",\r\n" + 
				"        \"KODE_POS_KANTOR\": \"\",\r\n" + 
				"        \"KODE_NEGARA_TELEPON_RUMAH\": \"62\",\r\n" + 
				"        \"WAJIB_FATCA\": \"N\",\r\n" + 
				"        \"PENGIRIMAN_REKENING_KORAN\": \"1\"\r\n" + 
				"}\r\n" + 
				"}\r\n" + 
				"]\r\n" + 
				"}";
		
		
		String canonJson = json.replace(" ", "").replace("\n", "").replace("\r", "").replace("	", "");

		String lowerRequestBody = hmac.sha256(canonJson).toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		String normalJson = json.toLowerCase();
		System.out.println("========== json "+normalJson);
		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}

}
