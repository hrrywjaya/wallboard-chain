package com.mitrakreasindo.pemrek.common;

import java.util.Base64;

import org.junit.Test;

public class Base64Test
{

//	@Test
	public void test ()
	{
		String data = "A2E5C7EA-6E18-463D-A1F6-1E757C667D20";
		System.out.println("============= "+Base64.getEncoder().encodeToString(data.getBytes()));
		
		String data1 = "7752e4ef-3c21-4f4d-950a-dd0cbbc09b83";
		System.out.println("============= "+Base64.getEncoder().encodeToString(data1.getBytes()));
		
		String data2 = "8C361D488B1132BDE05400144FFBD319";
		System.out.println("============= "+Base64.getEncoder().encodeToString(data2.getBytes()));
		
		
		String clientid = "4c44ae98";
		String secret = "acf26382ccd95033ab2fbc0f9d5dd79f";
		String res = Base64.getEncoder().encodeToString(new String(clientid+":"+secret).getBytes());
		System.out.println("===== res "+res);
		
		
	}

}
