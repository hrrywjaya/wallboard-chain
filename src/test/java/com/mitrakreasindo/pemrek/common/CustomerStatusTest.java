package com.mitrakreasindo.pemrek.common;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.OffsetDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerStatusTest
{


	@Autowired
	private GatewayService gat;
	@Autowired
	private AdGatewayService ad;
	@Autowired
	private BcaSignature hmac;
	
//	@Test
	public void customerStatus ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/customer-status/branches/0008/identities/1234567891";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String lowerRequestBody = hmac.sha256("").toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}
	
	@Test
	public void customerStatus1 ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/customer-status/branches/0008";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String lowerRequestBody = hmac.sha256("").toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}

}
