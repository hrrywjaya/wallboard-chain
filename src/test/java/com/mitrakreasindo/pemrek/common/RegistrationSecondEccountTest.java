package com.mitrakreasindo.pemrek.common;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.OffsetDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationSecondEccountTest
{


	@Autowired
	private GatewayService gat;
	@Autowired
	private AdGatewayService ad;
	@Autowired
	private BcaSignature hmac;
	
//	@Test
	public void firstAccountRegistration ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/customers/mdm/branches/0008/second-accounts/registration";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		
		String jsonSecond = "{\r\n" + 
				"    \"customer_number\": \"00000008464\",\r\n" + 
				"    \"account_number\": \"\",\r\n" + 
				"    \"reference_number\": \"E07H9GWJJ6\",\r\n" + 
				"    \"action\": \"\",\r\n" + 
				"    \"spv_id\": \"\",\r\n" + 
				"    \"customer_pekerjaan_desc_1\": \"pejabat negara/daerah\",\r\n" + 
				"    \"customer_pekerjaan_desc_2\": \"pejabat tinggi negara\",\r\n" + 
				"    \"customer_pekerjaan_desc_3\": \"presiden\",\r\n" + 
				"    \"account_information\": {\r\n" + 
				"        \"customer_gaji_utama\": \"1\",\r\n" + 
				"        \"customer_cabang\": \"0021\",\r\n" + 
				"        \"customer_no_rek_baru\": \"\",\r\n" + 
				"        \"account_type\": \"110\",\r\n" + 
				"        \"cost_center\": \"0000000\",\r\n" + 
				"        \"kode_negara\": \"ID\",\r\n" + 
				"        \"produk_mata_uang\": \"IDR\",\r\n" + 
				"        \"service_charge_code\": \"100\",\r\n" + 
				"        \"beban_biaya_admin\": \"1\",\r\n" + 
				"        \"user_code\": \"0000P000000\",\r\n" + 
				"        \"periode_biaya_admin\": \"005\",\r\n" + 
				"        \"periode_rk\": \"031\",\r\n" + 
				"        \"periode_bunga\": \"031\",\r\n" + 
				"        \"interest_plan\": \"110\",\r\n" + 
				"        \"od_plan\": \"000\",\r\n" + 
				"        \"golongan_pemilik\": \"886\",\r\n" + 
				"        \"witholding_plan\": \"020\",\r\n" + 
				"        \"kode_pembebanan_pajak\": \"Y\",\r\n" + 
				"        \"kode_perorangan_bisnis\": \"P\",\r\n" + 
				"        \"kode_penduduk_nonpenduduk\": \"R\",\r\n" + 
				"        \"customer_rekening_untuk\": \"2\",\r\n" + 
				"        \"status_rekening_gabungan\": \"2\",\r\n" + 
				"        \"rek_gabungan_nama\": \"LELI NUNIK\",\r\n" + 
				"        \"rek_gabungan_no_customer\": \"154270\",\r\n" + 
				"        \"rek_gabungan_nama_tercetak_pada_kartu\": \"\",\r\n" + 
				"        \"rek_gabungan_no_tanda_pengenal\": \"9213479123647164\",\r\n" + 
				"        \"buku_tabungan\": \"Y\",\r\n" + 
				"        \"produk_paspor_bca_jenis\": \"1\",\r\n" + 
				"        \"produk_paspor_bca_tipe\": \"1\",\r\n" + 
				"        \"produk_paspor_bca_instant_no\": \"601900133565481\",\r\n" + 
				"        \"produk_paspor_bca_petunjuk_layar\": \"1\",\r\n" + 
				"        \"fasilitas_klikbca\": \"Y\",\r\n" + 
				"        \"fasilitas_keybca_ib\": \"Y\",\r\n" + 
				"        \"fasilitas_keybca_sn\": \"1234567891\",\r\n" + 
				"        \"fasilitas_klikbca_hp\": \"085693344774\",\r\n" + 
				"        \"fasilitas_mbca\": \"Y\",\r\n" + 
				"        \"fasilitas_aktivasi_fin_mbca\": \"Y\",\r\n" + 
				"        \"fasilitas_mbca_hp\": \"085693344774\",\r\n" + 
				"        \"fasilitas_bbp\": \"Y\"\r\n" + 
				"    }\r\n" + 
				"}";
		
		String canonJson = jsonSecond.replace(" ", "").replace("\n", "").replace("\r", "").replace("	", "");

		String lowerRequestBody = hmac.sha256(canonJson).toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}

}
