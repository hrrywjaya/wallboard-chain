package com.mitrakreasindo.pemrek.common;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.OffsetDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationTest
{


	@Autowired
	private GatewayService gat;
	@Autowired
	private AdGatewayService ad;
	@Autowired
	private BcaSignature hmac;
	
//	@Test
	public void firstAccountRegistration ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/customers/mdm/branches/0008/accounts/registration";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String json = "{\r\n" + 
				"	\"reference_number\": \"W07A4UREH3\",\r\n" + 
				"	\"action\": \"\",\r\n" + 
				"	\"spv_id\": \"\",\r\n" + 
				"	\"customer_information\": {\r\n" + 
				"		\"customer_nama\": \"Stanley sutedy\",\r\n" + 
				"		\"customer_alias\": \"unyu\",\r\n" + 
				"		\"customer_nama_ref\": \"BAKULJKTOVO\",\r\n" + 
				"		\"customer_agama\": \"6\",\r\n" + 
				"		\"customer_agama_lainnya\": \"Kong hu cu\",\r\n" + 
				"		\"customer_tanda_pengenal_no\": \"1234567891234561\",\r\n" + 
				"		\"customer_tanda_pengenal_berlaku_sampai\": \"22042032\",\r\n" + 
				"		\"customer_tanda_pengenal\": \"1\",\r\n" + 
				"		\"customer_tanda_pengenal_no_lainnya\": \"1234567891234561\",\r\n" + 
				"		\"customer_tempat_lahir\": \"jakarta\",\r\n" + 
				"		\"customer_tanggal_lahir\": \"1992-10-08\",\r\n" + 
				"		\"negara_lahir\": \"jp\",\r\n" + 
				"		\"negara_wn\": \"ID\",\r\n" + 
				"		\"customer_jenis_kelamin\": \"L\",\r\n" + 
				"		\"customer_nama_ibu_kandung\": \"sri mulyani\",\r\n" + 
				"		\"customer_kewarganegaraan\": \"2\",\r\n" + 
				"		\"customer_kitas_kitap\": \"2\",\r\n" + 
				"		\"customer_kitas_kitap_no\": \"17236478908967329467\",\r\n" + 
				"		\"customer_kitas_kitap_berlaku_sampai\": \"2022-04-14\",\r\n" + 
				"		\"id_kecamatan\": \"koja\",\r\n" + 
				"		\"id_kelurahan\": \"pademangan timur\",\r\n" + 
				"		\"id_nama_jalan\": \"Jl Pademangan Raya No 21\",\r\n" + 
				"		\"id_kota\": \"jakarta utara\",\r\n" + 
				"		\"id_kode_pos\": \"14422\",\r\n" + 
				"		\"id_negara\": \"indonesia\",\r\n" + 
				"		\"id_provinsi\": \"dki jakarta\",\r\n" + 
				"		\"id_komplek_gedung\": \"Komplek Pademangan\",\r\n" + 
				"		\"id_rt\": \"023\",\r\n" + 
				"		\"id_rw\": \"033\",\r\n" + 
				"		\"kode_negara_telp_rumah\": \"62\",\r\n" + 
				"		\"kode_area_telp_rumah\": \"21\",\r\n" + 
				"		\"telp_rumah\": \"862222222\",\r\n" + 
				"		\"domisili_kecamatan\": \"pakulonan\",\r\n" + 
				"		\"domisili_kelurahan\": \"curug sangereng\",\r\n" + 
				"		\"domisili_nama_jalan\": \"jl kelapa puan xvi\",\r\n" + 
				"		\"domisili_kota\": \"kab tangerang\",\r\n" + 
				"		\"domisili_negara\": \"indonesia\",\r\n" + 
				"		\"domisili_provinsi\": \"banten\",\r\n" + 
				"		\"domisili_komplek_gedung\": \"sektor 1a\",\r\n" + 
				"		\"domisili_rw\": \"003\",\r\n" + 
				"		\"domisili_rt\": \"023\",\r\n" + 
				"		\"domisili_kode_pos\": \"15535\",\r\n" + 
				"		\"customer_pekerjaan\": \"1\",\r\n" + 
				"		\"customer_pekerjaan_lainnya\": \"\",\r\n" + 
				"		\"nama_kantor\": \"PT BCA TBK\",\r\n" + 
				"		\"jabatan\": \"Pelajar/Mahasiswa\",\r\n" + 
				"		\"bidang_usaha\": \"pendidikan\",\r\n" + 
				"		\"email\": \"STANLEY@GMAIL.COM\",\r\n" + 
				"		\"alamat_kerja_1\": \"BCA WISMA ASIA II\",\r\n" + 
				"		\"alamat_kerja_2\": \"JL BRIGJEN KATAMSO NO.2\",\r\n" + 
				"		\"alamat_kerja_3\": \"SLIPI KEMANGGISAN\",\r\n" + 
				"		\"kode_negara_telp_kantor\": \"62\",\r\n" + 
				"		\"kode_area_telp_kantor\": \"21\",\r\n" + 
				"		\"telp_kantor\": \"896634887\",\r\n" + 
				"		\"kode_negara_fax_kantor\": \"62\",\r\n" + 
				"		\"kode_area_fax_kantor\": \"62\",\r\n" + 
				"		\"fax_kantor\": \"99989999\",\r\n" + 
				"		\"customer_npwp\": \"N\",\r\n" + 
				"		\"customer_npwp_no\": \"\",\r\n" + 
				"		\"customer_sumber_penghasilan\": \"4\",\r\n" + 
				"		\"customer_gaji_total\": \"2\",\r\n" + 
				"		\"customer_sumber_penghasilan_lainnya\": \"\",\r\n" + 
				"		\"customer_tujuan_buka_rek\": \"4\",\r\n" + 
				"		\"customer_tujuan_buka_rek_lainnya\": \"Buat judi\",\r\n" + 
				"		\"kd_negara_hp_1\": \"62\",\r\n" + 
				"		\"hp_1\": \"85693344774\",\r\n" + 
				"		\"kd_negara_hp_2\": \"62\",\r\n" + 
				"		\"hp_2\": \"85693344774\",\r\n" + 
				"		\"customer_status_rumah\": \"4\",\r\n" + 
				"		\"customer_status_kawin\": \"1\",\r\n" + 
				"		\"customer_pendidikan_terakhir\": \"5\",\r\n" + 
				"		\"kode_customer_resiko_tinggi\": \"\",\r\n" + 
				"		\"mail_code\": \"1\",\r\n" + 
				"		\"persetujuan\": \"Y\",\r\n" + 
				"		\"media_info_sms\": \"Y\",\r\n" + 
				"		\"media_info_mail\": \"N\",\r\n" + 
				"		\"media_info_phone\": \"Y\",\r\n" + 
				"		\"form_fatca\": \"Y\",\r\n" + 
				"		\"form_aeoi\": \"Y\",\r\n" + 
				"		\"wajib_fatca\": \"Y\",\r\n" + 
				"		\"wp_negara_lain\": \"N\",\r\n" + 
				"		\"tin\": \"235234524\",\r\n" + 
				"		\"pejabat_bank\": \"00000\"\r\n" + 
				"	},\r\n" + 
				"	\"account_information\": {\r\n" + 
				"		\"customer_no_rek_baru\": \"\",\r\n" + 
				"		\"account_type\": \"110\",\r\n" + 
				"		\"cost_center\": \"0000000\",\r\n" + 
				"		\"kode_negara\": \"ID\",\r\n" + 
				"		\"produk_mata_uang\": \"IDR\",\r\n" + 
				"		\"service_charge_code\": \"100\",\r\n" + 
				"		\"beban_biaya_admin\": \"1\",\r\n" + 
				"		\"user_code\": \"0000P000000\",\r\n" + 
				"		\"periode_biaya_admin\": \"005\",\r\n" + 
				"		\"periode_rk\": \"031\",\r\n" + 
				"		\"periode_bunga\": \"031\",\r\n" + 
				"		\"interest_plan\": \"110\",\r\n" + 
				"		\"od_plan\": \"000\",\r\n" + 
				"		\"golongan_pemilik\": \"886\",\r\n" + 
				"		\"witholding_plan\": \"020\",\r\n" + 
				"		\"kode_pembebanan_pajak\": \"Y\",\r\n" + 
				"		\"kode_perorangan_bisnis\": \"P\",\r\n" + 
				"		\"kode_penduduk_nonpenduduk\": \"R\",\r\n" + 
				"		\"customer_rekening_untuk\": \"2\",\r\n" + 
				"		\"status_rekening_gabungan\": \"2\",\r\n" + 
				"		\"rek_gabungan_nama\": \"LELI NUNIK\",\r\n" + 
				"		\"rek_gabungan_no_customer\": \"154270\",\r\n" + 
				"		\"rek_gabungan_nama_tercetak_pada_kartu\": \"\",\r\n" + 
				"		\"rek_gabungan_no_tanda_pengenal\": \"9213479123647164\",\r\n" + 
				"		\"buku_tabungan\": \"Y\",\r\n" + 
				"		\"produk_paspor_bca_jenis\": \"1\",\r\n" + 
				"		\"produk_paspor_bca_tipe\": \"1\",\r\n" + 
				"		\"produk_paspor_bca_instant_no\": \"601900133565481\",\r\n" + 
				"		\"produk_paspor_bca_petunjuk_layar\": \"1\",\r\n" + 
				"		\"fasilitas_klikbca\": \"Y\",\r\n" + 
				"		\"fasilitas_keybca_ib\": \"Y\",\r\n" + 
				"		\"fasilitas_keybca_sn\": \"1234567891\",\r\n" + 
				"		\"fasilitas_klikbca_hp\": \"085693344774\",\r\n" + 
				"		\"fasilitas_mbca\": \"Y\",\r\n" + 
				"		\"fasilitas_aktivasi_fin_mbca\": \"Y\",\r\n" + 
				"		\"fasilitas_mbca_hp\": \"085693344774\",\r\n" + 
				"		\"fasilitas_bbp\": \"Y\",\r\n" + 
				"		\"customer_cabang\": \"0008\",\r\n" + 
				"		\"customer_gaji_utama\": \"7\"\r\n" + 
				"	},\r\n" + 
				"	\"customer_pekerjaan_desc_1\": \"pejabat negara/daerah\",\r\n" + 
				"	\"customer_pekerjaan_desc_2\": \"pejabat tinggi negara\",\r\n" + 
				"	\"customer_pekerjaan_desc_3\": \"presiden\"\r\n" + 
				"} \r\n" + 
				"";
		
		
		String canonJson = json.replace(" ", "").replace("\n", "").replace("\r", "").replace("	", "");

		String lowerRequestBody = hmac.sha256(canonJson).toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}

}
