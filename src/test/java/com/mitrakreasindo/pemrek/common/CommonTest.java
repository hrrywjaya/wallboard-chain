package com.mitrakreasindo.pemrek.common;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrakreasindo.pemrek.core.security.BcaSignature;
import com.mitrakreasindo.pemrek.external.adgateway.service.AdGatewayService;
import com.mitrakreasindo.pemrek.external.common.model.OutputStatus;
import com.mitrakreasindo.pemrek.external.gateway.model.GatewayToken;
import com.mitrakreasindo.pemrek.external.gateway.repository.network.GatewayRepository;
import com.mitrakreasindo.pemrek.external.gateway.service.GatewayService;
import com.mitrakreasindo.pemrek.external.predin.model.Predin;
import com.mitrakreasindo.pemrek.external.predin.repository.network.PredinRepository;
import com.mitrakreasindo.pemrek.external.registration.model.card.CardRegistrationRequest;
import com.mitrakreasindo.pemrek.external.registration.repository.network.RegistrationRepository;
import com.mitrakreasindo.pemrek.external.registration.service.RegistrationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonTest
{
	@Autowired
	private AdGatewayService ad;
	@Autowired
	private BcaSignature hmac;
	@Autowired
	private GatewayService gat;
	@Autowired
	private GatewayRepository gr;

//	@Test
	public void testCopyProperty ()
	{

		BCryptPasswordEncoder b = new BCryptPasswordEncoder();
		System.out.println("================= " + b.encode("admin"));
		System.out.println("================= " + b.encode("admin"));
	}

//	@Test
	public void testIpaddress ()
	{
		String ipAddress = "";
		try
		{
			InetAddress inet = InetAddress.getLocalHost();
			ipAddress = inet.getHostAddress();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		System.out.println("=============== " + ipAddress);
	}

//	@Test
//	public void logintest() {
//		OutputStatus ou = ad.login("appemas12", "46C573B3CC59EA7C");
//	}

//	@Test
	public void hmac ()
	{
		String access_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYbE9wbEVrdGg5QnNqTnQ4VUtHTnViblk3MHloYkMtMjg0YklhOTY3dGswIn0.eyJqdGkiOiIzNTMyN2UyZS03ZGQ0LTQ4ZjktYjQwZS00NjNkNTRhZGZmM2UiLCJleHAiOjE1NTUwNjgyODcsIm5iZiI6MCwiaWF0IjoxNTU1MDY0Njg3LCJpc3MiOiJodHRwczovL3Nzby5hcHBzLm9jcC5kdGkuY28uaWQvYXV0aC9yZWFsbXMvM3NjYWxlLWRldiIsImF1ZCI6IjRjNDRhZTk4Iiwic3ViIjoiNThlNzdmYzEtMmI3YS00NzE0LWJkYTQtNDk1ZTVlNWE0ZDRhIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiNGM0NGFlOTgiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJjNTc0NzUwZi04OWY3LTRhNGMtOWYyOS1mZGRkOTM5N2E4NTQiLCJhY3IiOiIxIiwiY2xpZW50X3Nlc3Npb24iOiIzNmY4OWQwMi02MjA2LTQwZDUtODYwYy1jNTEzNWE2MmM2NmYiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50Iiwidmlldy1wcm9maWxlIl19fSwiY2xpZW50SWQiOiI0YzQ0YWU5OCIsImNsaWVudEhvc3QiOiIxMC41LjQxLjUzIiwibmFtZSI6IiIsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC00YzQ0YWU5OCIsImNsaWVudEFkZHJlc3MiOiIxMC41LjQxLjUzIiwiZW1haWwiOiJzZXJ2aWNlLWFjY291bnQtNGM0NGFlOThAcGxhY2Vob2xkZXIub3JnIn0.MJAbybeWxKweBJDWxn_pHUdEKc9JuWJIqJnwzhWsdPh-wNCSNBETR4rBefB7RnzHIDDYMISzxnNBqGF1QO_nNtuM96Hey9CKDLiMGM7vjmwOAdJYi8hU_BuiXfN8sqCUt_PHdqTgYMaKlTRopKSGy3XNL7G9DC8UPP2BSWhyxV8RYI3N5ecr2mq-ESEkm1eXdBBIet5GXQVlPvcU4gT102P3Rw97K-K_3__KKqsoE-FqBypuZcO0rCVBRuvjQ-NHlUV0F6e2PPxWIq6KL2ga8c_8z5WTEH5k4gk-Zj73zZr8DC5Y55h_cIs4tYWKlcbr_eIzJVXLM4gW2az-URnAKg";
		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/pre-din/branches/0008/references/H93152";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String lowerRequestBody = hmac.sha256Emty().toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + access_token + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========= data " + data);
		System.out.println("========= hmac : " + result);
	}

	@Value("${gateway.client-id}")
	private String gatewayId;
	@Value("${gateway.client-secret}")
	private String gatewaySecret;

//	@Test
	public void testGateway ()
	{
		gat.getToken();
	}

	@Autowired
	private PredinRepository predinRepository;

//	@Test
	public void getPredin ()
	{
		GatewayToken token = gat.getToken();

		String httpMethod = "GET";
		String relativeUrl = "/bds/general/api/pre-din/branches/0008/references/E072589684";

		String time = OffsetDateTime.now().toString();

		String lowerRequestBody = hmac.sha256Emty().toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== time " + time);
		System.out.println("========= data " + data);
		System.out.println("========= hmac : " + result);

//		return new Predin();
		predinRepository.getPredin("014D2CF3CDC3AF6F4F92C09190860E33", "u012345", "010T", "http://localhost.com",
				token.getAccessToken(), "7752e4ef-3c21-4f4d-950a-dd0cbbc09b83", time, result, "0008", "W129597777");
		// H93152
		// W07ENGZWEZ
	}

	@Test
	public void hmacEform ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "GET";
		String relativeUrl = "/eform/form-transactions/api/references/M129118040";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String lowerRequestBody = hmac.sha256Emty().toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== token Bearer "+token.getAccessToken());
		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========= data " + data);
		System.out.println("========= hmac : " + result);
	}

//@Test
	public void hmacPostEform ()
	{
		GatewayToken token = gat.getToken();
//String access_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYbE9wbEVrdGg5QnNqTnQ4VUtHTnViblk3MHloYkMtMjg0YklhOTY3dGswIn0.eyJqdGkiOiJmMzlhODgxYy1iMTBiLTQzYTEtOGYxYi1kZTkwZTI4YTE3OTUiLCJleHAiOjE1NTUzOTY2NjQsIm5iZiI6MCwiaWF0IjoxNTU1MzkzMDY0LCJpc3MiOiJodHRwczovL3Nzby5hcHBzLm9jcC5kdGkuY28uaWQvYXV0aC9yZWFsbXMvM3NjYWxlLWRldiIsImF1ZCI6IjRjNDRhZTk4Iiwic3ViIjoiNThlNzdmYzEtMmI3YS00NzE0LWJkYTQtNDk1ZTVlNWE0ZDRhIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiNGM0NGFlOTgiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkZjMzNjA0NC02MjgzLTQwYjMtOTM1Ny1mNmE3NTYwMTUzMGYiLCJhY3IiOiIxIiwiY2xpZW50X3Nlc3Npb24iOiI0Y2VjNjdhMS03NTI0LTQ0ZGQtYTA2OC00YWVmYjk4Yzg1NGMiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50Iiwidmlldy1wcm9maWxlIl19fSwiY2xpZW50SWQiOiI0YzQ0YWU5OCIsImNsaWVudEhvc3QiOiIxMC41LjQxLjUzIiwibmFtZSI6IiIsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC00YzQ0YWU5OCIsImNsaWVudEFkZHJlc3MiOiIxMC41LjQxLjUzIiwiZW1haWwiOiJzZXJ2aWNlLWFjY291bnQtNGM0NGFlOThAcGxhY2Vob2xkZXIub3JnIn0.GhgJEBQEOiHU2PWoeSzV7cc0yinCrmnz2yNnz5vJPM_AtSe556AbykSXr8yPErwk_em6baiVoIF-AibTuAb4-hutu60IY2qoBaBjwYY1FT-xUXr04GepePES7lkupiIsQclUrWNHTbu2LPY97hQ42d51Ie6-PtMWGnmXa8sBcduHfChKlZNMHJepxdf_Mwd23kCP_dfQeTJ1dS-DOLmXPYx9wcPWwjITQZdbCngrWD0DFJjxFeyxbycorivtuTNwWOxULPj9KAomqWdtFZsArJas4XQUzEpWT4ayez45t3gRKWAZmH2HMnyedOTpwge8zEol9HWq48zrDP8VB8cdUQ";
		String httpMethod = "POST";
		String relativeUrl = "/eform/form-transactions/api/save";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		String body = "{\"form_data_list\" : [\n" + "{\n" + "\"ref_num\" : \"\",\n" + "\"channel_id\" : \"E\",\n"
				+ "\"trans_id\" : \"01\",\n" + "\"template_version\" : \"\",\n" + "\"status\" : \"\",\n"
				+ "\"created_by\" : \"U057101\",\n" + "\"form_data\": {\n" + "\"JUMLAH_TRANSAKSI\" : \"100000\",\n"
				+ "\"JENIS_IDENTITAS\" : \"KTP\",\n" + "\"NOMOR_PONSEL\" : \"02846226634556\",\n"
				+ "\"NAMA_PENYETOR\" : \"TestingSetor\",\n" + "\"NOMOR_REKENING_PENYETOR\" : \"5278569321\",\n"
				+ "\"STATUS_VERIFIKASI\" : \"\",\n" + "\"SUMBER_DANA\" : \"\",\n" + "\"EMAIL\" : \"testing@email.com\",\n"
				+ "\"TUJUAN_TRANSAKSI\" : \"\",\n" + "\"BERITA\" : \"\",\n" + "\"NOMOR_REKENING_PENERIMA\" : \"5278965123\",\n"
				+ "\"MATA_UANG\" : \"IDR\",\n" + "\"SIMPAN_DENGAN_NAMA\" : \"\",\n" + "\"ALAMAT_PENYETOR\" : \"Jalan xxx\",\n"
				+ "\"NAMA_PENERIMA\" : \"TestingTerima\",\n" + "\"NOMOR_IDENTITAS_PENYETOR\" : \"123456479687654634\"\n" + "}\n"
				+ "},\n" + "{\n" + "\"ref_num\" : \"\",\n" + "\"channel_id\" : \"E\",\n" + "\"trans_id\" : \"01\",\n"
				+ "\"template_version\" : \"\",\n" + "\"status\" : \"\",\n" + "\"created_by\" : \"U057101\",\n"
				+ "\"form_data\": {\n" + "\"JUMLAH_TRANSAKSI\" : \"100000\",\n" + "\"JENIS_IDENTITAS\" : \"KTP\",\n"
				+ "\"NOMOR_PONSEL\" : \"02846226634556\",\n" + "\"NAMA_PENYETOR\" : \"TestingSetor\",\n"
				+ "\"NOMOR_REKENING_PENYETOR\" : \"5278569321\",\n" + "\"STATUS_VERIFIKASI\" : \"\",\n"
				+ "\"SUMBER_DANA\" : \"\",\n" + "\"EMAIL\" : \"testing@email.com\",\n" + "\"TUJUAN_TRANSAKSI\" : \"\",\n"
				+ "\"BERITA\" : \"\",\n" + "\"NOMOR_REKENING_PENERIMA\" : \"5278965123\",\n" + "\"MATA_UANG\" : \"IDR\",\n"
				+ "\"SIMPAN_DENGAN_NAMA\" : \"\",\n" + "\"ALAMAT_PENYETOR\" : \"Jalan xxx\",\n"
				+ "\"NAMA_PENERIMA\" : \"TestingTerima\",\n" + "\"NOMOR_IDENTITAS_PENYETOR\" : \"123456479687654634\"\n" + "}\n"
				+ "}\n" + "]\n" + "}";
		String lowerRequestBody = hmac.sha256(body).toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========= data " + data);
		System.out.println("========= hmac : " + result);
	}

//	@Test
	public void hmacPostCardRegistration ()
	{
		GatewayToken token = gat.getToken();
		String httpMethod = "POST";
		String relativeUrl = "/bds/general/api/digital-card/registration";
		String encodedUrl = "";

		String time = OffsetDateTime.now().toString();
		try
		{
			encodedUrl = URLEncoder.encode(relativeUrl, "UTF-8");

		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		CardRegistrationRequest cardReg = new CardRegistrationRequest();
		cardReg.setAccountNumber("0210000551");
		cardReg.setUserId("011T");
		cardReg.setBranchCode("0940");
//		cardReg.setBranchName("SENTRA LAYANAN DIGITAL");
		cardReg.setSpvId("901S");
		
		ObjectMapper mapper = new ObjectMapper();
		
		String j = "";
		try
		{
			j = mapper.writeValueAsString(cardReg).replace(" ", "");
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}

		String lowerRequestBody = hmac.sha256(j).toLowerCase();

		String data = httpMethod + ":" + relativeUrl + ":" + token.getAccessToken() + ":" + lowerRequestBody + ":" + time;
		String result = hmac.hmacSignarture("ec2a259f-0cbf-433f-ac54-bb85a46a59f2", data);

		System.out.println("========== relative url " + relativeUrl);
		System.out.println("========== Bearer " + token.getAccessToken());
		System.out.println("========== encoded url " + encodedUrl);
		System.out.println("========== time " + time);
		System.out.println("========== data " + data);
		System.out.println("========== hmac : " + result);
	}

}
